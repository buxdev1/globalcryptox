const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let currency = new Schema({
	currencyName:{
		type: String, default: ''
	},
	currencySymbol:{
		type: String, default: ''
	},
	fee:{
		type: Number, default: 0
	},
	minimum:{
		type: Number, default: 0
	},
	type:{
		type: String, default: 'Crypto'
	},
	minABI:{
		type: String, default: ''
	},
	contractAddress:{
		type: String, default: ''
	},
	decimals:{
		type:Number
	},
	status:{
		type: String, default: 1, // 0 - deactive, 1-active
	},
	currencyimage:{
		type:String
	}
});

module.exports = mongoose.model('currency',currency,'currency');
