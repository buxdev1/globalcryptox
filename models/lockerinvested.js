const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let lockerinvested = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
    index: true,
  },
  totalinvest: {
    type: Number,
  },
  projectId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "lockers",
    index: true,
  },
  projectduration: {
    type: Number,
  },
  status: {
    type: String,
    default: "Invest", // Return, Invested
  },
  createddate:{
    type: Date,
    default: Date.now,
  },
});
module.exports = mongoose.model(
  "lockerinvested",
  lockerinvested,
  "lockerinvested"
);
