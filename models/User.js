const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  name: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    // required: true
  },
  liq_lock: {
    type: Boolean,
    default: false,
  },
  userid: {
    type: String,
  },
  password: {
    type: String,
    required: true
  },
  moderator:{
    type:String,
    default:'0' //0-normal user, 1-moderator 2-admin,
  },
  phonenumber: {
    type: String,
    default: ""
  },
  otp: {
    type: String,
    default: ""
  },
  ipblocktime: {
    type: Date,
  },
  ipblockcode: {
    type: String,
  },
  blocktime: {
    type: Date,
  },
  blockhours: {
    type: String,
  },
  otptime: {
    type: Date,
    default: ''
  },
  expTime: {
    type: Date,
    default: ''
  },
  currency: {
    type: String,
    default: ""
  },
  profile: {
    type: String,
    default: ""
  },
  role: {
    type: String,
    default: "user"
  },
  active: {
    type: String,
    default: ""
  },
  referaluserid: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  referencecode: {
    type: String,
  },
  windoworder: {
    type: String,
    default: "false"
  },
  mobilesite: {
    type: String,
    default: "false"
  },
  position: {
    type: String,
    default: "false"
  },
  animation: {
    type: String,
    default: "false"
  },
  sms: {
    type: String,
    default: "Not Verified"
  },
  google: {
    type: String,
    default: "Disabled"
  },
  googlesecretcode: {
    type: String,
  },
  status: {
    type: String,
    default: 1 //1-Completed
  },
  date: {
    type: Date,
    default: Date.now
  },
  loginhistory: [{
    countryCode: {
      type: String,
      default: ''
    },
    countryName: {
      type: String,
      default: ''
    },
    regionName: {
      type: String,
      default: ''
    },
    ipaddress: {
      type: String,
      default: ''
    },
    broswername: {
      type: String,
      default: ''
    },
    ismobile: {
      type: String,
      default: ''
    },
    os: {
      type: String,
      default: ''
    },
    status: {
      type: String,
      default: 'Success' // success / failure
    },
    createdDate: {
      type: Date,
      default: "" // success / failure
    },
  }],
  apiKeydetails: [{
    remarkname: {
      type: String,
      default: ''
    },
    ipaddress: {
      type: String,
      default: ''
    },
    keypermission: {
      type: String,
      default: ''
    },
    readOnly: {
      type: String,
      default: ''
    },
    applicationName: {
      type: String,
      default: ''
    },
    apikey: {
      type: String,
      default: ''
    },
    secretkey: {
      type: String,
      default: ''
    },
    createdDate: {
      type: Date,
      default: Date.now
    },
    expiredDate: {
      type: Date,
      default: ''
    },

  }]

});

UserSchema.virtual('id').get(function () {
  return this._id.toHexString();
});

UserSchema.set('toJSON', {
  virtuals: true
});

module.exports = User = mongoose.model("users", UserSchema);
