const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const btcusdschema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    index: true
  },
  fromcurrency: {
    type: String
  },
  tocurrency: {
    type: String
  },
  beforefrombalance: {
    type: String
  },
  beforetobalance: {
    type: String
  },
  afterfrombalance: {
    type: String
  },
  aftertobalance: {
    type: String
  },

  created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = btcusdconversion = mongoose.model("btcusdconversion", btcusdschema);
