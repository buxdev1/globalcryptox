const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let lockerhistory = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
    index: true,
  },
  referaluserid: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  Investamount: {
    type: Number,
  },
  projectId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "lockers",
    index: true,
  },
  projectname: {
    type: String,
  },

  projectduration: {
    type: Number,
  },
  investid: {
    type: String,
  },
  termpercentage: {
    type: Number,
  },
  status: {
    type: String,
    default: "Invest", // Return, Invested
  },
  totalnoofreturn: {
    type: Number,
  },
  noofreturn: {
    type: Number,
    default: 0,
  },
  lastreturndate: {
    type: Date,
  },
  // returndates: [
  //   {
  //     type: Date,
  //   },
  // ],
  returndates: [{
    returnstatus: {
      type: String,
      default: 'Pending'// Pending / Completed
    },
    returnamount: {
      type: Number,
    },
    returnweekdate: {
      type: Date,
      default: "" 
    },
  }],
  type: {
    type: String,
  },
  createddate: {
    type: Date,
    default: Date.now,
  },
  termstart: {
    type: Date,
    default: Date.now,
  },
  termend: {
    type: Date,
  },
});
module.exports = mongoose.model(
  "lockerhistory",
  lockerhistory,
  "lockerhistory"
);
