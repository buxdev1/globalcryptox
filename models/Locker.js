const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let lockers = new Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  returnpercentagemonth: {
    type: String,
  },
//   returnterms:[{
//   	type:String
//   }],
  // totalnoofreturn:{
  //   type:Number
  // },
  // rerturndates:[{
  //   type:String
  // }],
  returnterm: {
    type: String,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
  TotalInvested: {
    type: Number,
    default:0
  },
  mininvestment: {
    type: Number,
    default:0
  },
  maxinvestment: {
    type: Number,
    default:0
  },
  Returntype:{
		type: String, default: 'Day'   ///Days  Week
	},
});
module.exports = mongoose.model("lockers", lockers, "lockers");
