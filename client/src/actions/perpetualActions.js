import axios from "axios";
import {
    GET_ERRORS,
    PERPETUAL_ADD,
    PERPETUAL_UPDATE
} from "./types";
import keys from "./config";
const url = keys.baseUrl;
export const addperpetual = (faqData) => dispatch => {
    axios
        .post(url+"api/perpetual-add", faqData)
        .then(res =>
            dispatch({
                type: PERPETUAL_ADD,
                payload: res,
            })
        ).catch(err =>
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};


export const updatePerpetual = (faqData) => dispatch => {
    axios
        .post(url+"api/perpetual-update", faqData)
        .then(res =>
            dispatch({
                type: PERPETUAL_UPDATE,
                payload: res,
            })
        ).catch(err =>
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};

export const addpair = (faqData) => dispatch => {
    axios
        .post(url+"api/pair-add", faqData)
        .then(res =>
            dispatch({
                type: PERPETUAL_ADD,
                payload: res,
            })
        ).catch(err =>
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};


export const updatepair = (faqData) => dispatch => {
    axios
        .post(url+"api/pair-update", faqData)
        .then(res =>
            dispatch({
                type: PERPETUAL_UPDATE,
                payload: res,
            })
        ).catch(err =>
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};
