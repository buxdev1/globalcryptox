import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import ReactDatatable from '@ashvin27/react-datatable';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import { ToastContainer } from "react-toastify";
import TemplateUpdateModal from "../partials/withdrawModal";
import keys from "../../actions/config";
import { Modal, Button } from 'react-bootstrap/';
import $ from 'jquery';

const url = keys.baseUrl;
class Pmwithdrawlist extends Component {

    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "email",
                text: "Email",
                className: "email",
                align: "left",
                sortable: true
            },
            {
                key: "name",
                text: "Name",
                className: "name",
                align: "left",
                sortable: true
            },
            {
                key: "amount",
                text: "Amount",
                className: "amount",
                align: "left",
                sortable: true
            },
            {
                key: "accountnumber",
                text: "Account Number",
                className: "accountnumber",
                align: "left",
                sortable: true
            },
            {
                key: "fees",
                text: "Fees",
                className: "accountnumber",
                align: "left",
                sortable: true
            }
            ,
            {
                key: "useramount",
                text: "User Received",
                className: "accountnumber",
                align: "left",
                sortable: true
            },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                    return (
                        <Fragment>
                            <button
                                data-toggle="modal"
                                data-target="#update-template-modal"
                                className="btn btn-primary btn-sm"
                                onClick={() => this.editRecord(record)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-edit"></i>
                            </button>
                            {/* <button
                                className="btn btn-danger btn-sm"
                                onClick={() => this.deleteRecord(record)}>
                                <i className="fa fa-trash"></i>
                            </button> */}
                        </Fragment>
                    );
                }
            }
        ];

        this.config = {
            page_size: 10,
            length_menu: [10, 20, 50],
            filename: "Users",
            no_data_text: 'No user found!',
            sort: { column: "Created date", order: "desc" },
            language: {
                length_menu: "Show _MENU_ result per page",
                filter: "Filter in records...",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                pagination: {
                    first: "First",
                    previous: "Previous",
                    next: "Next",
                    last: "Last"
                }
            },
            show_length_menu: true,
            show_filter: true,
            show_pagination: true,
            show_info: true,
            defaultSortAsc: true,
        };

        this.state = {
            currentRecord: {
                records: [],
                email: '',
                modalshow: false,
                modalshow1: false,
                responsive: true,
            }
        };

        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
       
        this.getData()
    };

    editRecord(record) {
        console.log(record,"recordrecordrecordrecordrecordrecordrecord");
        this.setState({ currentRecord: record});
        if(this.state.currentRecord) {
            $("#update-template-modal").find(".text-danger").hide();
        }
       
        
   }

    componentWillReceiveProps(nextProps) {
        this.getData()
    }

    getData() {
        axios
            .get(url + "adminapi/pm-withdraw-request-data")
            .then(res => {
                console.log("New response", res.data.data);
                this.setState({ records: res.data.data })
            })
            .catch()
    }

    pageChange(pageData) {
        console.log("OnPageChange", pageData);
    }

    render() {
        return (
            <div>
                <Navbar />
                <div className="d-flex" id="wrapper">
                    <Sidebar />
                    <TemplateUpdateModal record={this.state.currentRecord}/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            {/* <button className="btn btn-outline-primary float-right mt-3 mr-2" data-toggle="modal" data-target="#add-user-modal"><FontAwesomeIcon icon={faPlus}/> add</button> */}
                            <h3 className="mt-2 text-secondary">Perfect Withdraw Management</h3>
                            <ReactDatatable
                                responsive={this.state.responsive}
                                config={this.config}
                                records={this.state.records}
                                columns={this.columns}
                                onPageChange={this.pageChange.bind(this)}
                            />
                        </div>
                    </div>
                    <ToastContainer />
                </div>
               
            </div>

        );
    }

}

Pmwithdrawlist.propTypes = {
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    records: state.records
});

export default connect(
    mapStateToProps
)(Pmwithdrawlist);
