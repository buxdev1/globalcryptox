import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import classnames from "classnames";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import { newsletteremail } from "../../actions/userActions";
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer} from "react-toastify";
import keys from "../../actions/config";
import { withRouter } from "react-router-dom";
import { Editor } from '@tinymce/tinymce-react';
import Multiselect from 'multiselect-dropdown-react';
const url = keys.baseUrl;
class Newsletter extends Component {
    constructor(props) {
        super(props);
        this.state = {
          
            email_assigned:false,
            email:{},
            message: "",
            errors: {},
        };
        console.log(this.state,'state');
         
        this.handleEditorChange = this.handleEditorChange.bind(this);
        }

        componentDidMount() {
            this.getData()  
        };

    
     componentWillReceiveProps(nextProps) {
         console.log(nextProps,'nextprops');
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
        if (nextProps.auth !== undefined
            && nextProps.auth.profile !== undefined
            && nextProps.auth.profile.data !== undefined
            && nextProps.auth.profile.data.message !== undefined) {
            toast(nextProps.auth.profile.data.message, {
                position: toast.POSITION.TOP_CENTER
            });
            nextProps.auth.profile = undefined;
        }
    }

    result=params=>{
        console.log(params);
     this.setState({
         email1: params
        });

       console.log(this.setState,'emailssss');   
    }

    onChange = e => {
      if (e.target.id === 'message') {
         this.setState({ message: e.target.value });
         };
       console.log(this.state,'statessss'); 
    }

    getData() {
           axios
           .get(url+"api/news-letter")
            .then(res => {   
                var arremail =[]; 
                  res.data.map((item,i)=>{
                    const name = item.email;
                    const value = item.email;         
                    const obj = {'name':name, 'value':value};
                  arremail.push(obj);
                   });
                  console.log(arremail.length,'length');
                   this.setState({email:arremail,email_assigned:true});
             })
            .catch()
        console.log(this.props,'authget1');
    }
 
    handleEditorChange(message, editor) {
      this.setState({ message });
    }
   
    onemailsend = e => {
        e.preventDefault();
        const newsletteremail = {
            email: this.state.email1, 
            message: this.state.message
        };
        const data = new FormData();
        data.append('message', this.state.message);
        data.append('email', this.state.email1);
        
        this.props.newsletteremail(newsletteremail);
        //console.log(newsletteremail,'newsletteremailzz');
        // axios.post(url+"api/profileupload", data, { // receive two parameter endpoint url ,form data 
        // })
        // .then(res => { // then print response status
        //     console.log(res.statusText)
        // })

    };

    render() {
        const { errors } = this.state;
        var email_try = this.state.email[0];

        if(typeof email_try == 'object'){}
        if(this.state.email_assigned){
            Object.keys(email_try).forEach(function(key) {
           
            });
        }
       
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <h3 className="mt-2 text-secondary">Newletter Details</h3>
                            <form noValidate onSubmit={this.onemailsend} id="send-email">
                                    
                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="email">Email</label>
                                        </div>
                                        <div className="col-md-9">
                                           {this.state.email_assigned?
                                           <Multiselect options={this.state.email} onSelectOptions={this.result} />
                                           :''
                                       }
                                               
                                               
                                             
                                            <span className="text-danger">{errors.email}</span>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="content">Message</label>
                                        </div>
                                        <div className="col-md-9">
                                        <Editor apiKey='5vk89nvvi2zckrb2lp2ctyyolewhq1v3pzdiwb7at68h40a5'
                                           initialValue={this.state.message}
                                           id="message"
                                           value={this.state.message}  onEditorChange={this.handleEditorChange}
                                           init={{
                                             height: 500,
                                             menubar: false,
                                             plugins: [
                                               'advlist autolink lists link image charmap print preview anchor',
                                               'searchreplace visualblocks code fullscreen',
                                               'insertdatetime media table paste code help wordcount'
                                             ],
                                             toolbar:
                                               'undo redo code | formatselect | bold italic backcolor | \
                                               alignleft aligncenter alignright alignjustify | \
                                               bullist numlist outdent indent | removeformat | help'
                                           }}
                                         />
                                           
                                        </div>
                                    </div>
                                    
                                    
                                </form>
                                    <br />
                               <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button
                                    form="send-email"
                                    type="submit"
                                    className="btn btn-primary">
                                    Send
                                </button>
                            </div>
                         </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}

Newsletter.propTypes = {
    newsletteremail: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { newsletteremail }
)(withRouter(Newsletter));
