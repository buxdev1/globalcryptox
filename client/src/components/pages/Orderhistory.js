import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from '@ashvin27/react-datatable';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import SupportReplyModal from "../partials/SupportReplyModal";
import { toast, ToastContainer} from "react-toastify";
import keys from "../../actions/config";
const url = keys.baseUrl;
class tradehistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            records: [],
        };
        this.columns = [
            {
                key: "pairName",
                text: "Contracts",
                className: "pairName",
                align: "left",
                sortable: true,
                width:200,
            },
            {
                key: "name",
                text: "Name",
                className: "name",
                align: "left",
                sortable: true,
            },
             {
                key: "quantity",
                text: "Qty",
                className: "quantity",
                align: "left",
                sortable: true,
                width:200,
            },
             {
                key: "quantity",
                text: "Filled remaining",
                className: "Filled",
                align: "left",
                sortable: true,
                width:200,
                cell: record => {
                var filledAmount = record.filledAmount?record.filledAmount:0;
                var Remaining = parseFloat(record.quantity) - parseFloat(record.filledAmount);
                return (
                        <Fragment>
                                {parseFloat(filledAmount).toFixed(8)+"/"+parseFloat(Remaining).toFixed(8)}
                        </Fragment>
                    );
                }
            },

             {
                key: "e_price",
                text: "Exec price",
                className: "e_price",
                align: "left",
                sortable: true,
                width:200,
                
            },
             {
                key: "price",
                text: "Order price",
                className: "price",
                align: "left",
                sortable: true,
                width:200,
                
            },
             {
                key: "orderValue",
                text: "Order value",
                className: "orderValue",
                align: "left",
                sortable: true,
                width:200,
                
                
            },
            {
                key: "orderType",
                text: "Type",
                className: "orderType",
                align: "left",
                sortable: true,
                width:200,
                
            },
            {
                key: "status",
                text: "Status",
                className: "status",
                align: "left",
                sortable: true,
                width:200,
                
                
            }, {
                key: "date",
                text: "Date",
                className: "date",
                align: "left",
                sortable: true,
                width:200,
                
                
            },
            
           
            
        ];

        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
            filename: "Order",
            no_data_text: 'No Records found!',
            language: {
                length_menu: "Show _MENU_ result per page",
                filter: "Filter in records...",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                pagination: {
                    first: "First",
                    previous: "Previous",
                    next: "Next",
                    last: "Last"
                }
            },
            show_length_menu: true,
            show_filter: true,
            show_pagination: true,
            show_info: true,
        };
}
   componentDidMount() {
            this.getData()        
        };

        pageChange(pageData) {
        console.log("OnPageChange", pageData);
    }


    getData() {
        axios
            .post(url+"api/order_history")
            .then(res => {
                this.setState({ records: res.data.data})
            })
            .catch()
                }

    render() {
    
            const {records} = this.state
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <h3 className="mt-2 text-secondary">Order History</h3>
                          {/*<table id="assetsTable" className="table">
                                                  <thead>
                                                      <tr className="wow flipInX" data-wow-delay=".5s;">
                                                          <th>Contracts</th>
                                                          <th>Name</th>
                                                          <th>Qty</th>
                                                          <th>Filled remaining</th>
                                                          <th>Exec price</th>
                                                          <th>Order price</th>
                                                          <th>Order value</th>
                                                          <th>Type</th>
                                                          <th>Status</th>
                                                          <th>Order#</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                   {
                                                    records.map((item,i)=>{
                                                var pairName = item.pairName?item.pairName:'';
                                                var quantity = item.quantity?item.quantity:0;
                                                 var name = item.userId.email.split('@')[0]?item.userId.email.split('@')[0]:0;
                                                var price = item.quantity?item.price:0;
                                                var orderValue = item.orderValue?item.orderValue:0;
                                                var orderType = item.orderType?item.orderType:0;
                                                var orderDate = item.orderDate?item.orderDate:0;
                                                var classs = item.buyorsell=='buy'?'greenText':'pinkText';
                                                var _id = item._id?item._id:0;
                                                var status1 = item.status;
                                                var e_price = item.filled.length>0?item.filled[0].Price:0;
                                                var filledAmount = item.filledAmount?item.filledAmount:0;
                                                var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                                var data1 = new Date(orderDate);
                                                let date12 = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
                                                    return <tr className="wow flipInX" data-wow-delay=".5s;">
                                                    <td>{pairName}</td>
                                                    <td>{name}</td>
                                                    <td>{quantity}</td>
                                                    <td className="text-center">{filledAmount+'/'+Remaining}</td>
                                                    <td>{e_price}</td>
                                                    <td>{price}</td>
                                                    <td>{orderValue}</td>
                                                    <td>{orderType}</td>
                                                           {(status1 == 0)?
                                                            <td className="text-center">New</td>
                                                             :''}
                                                             {(status1 == 1)?
                                                            <td className="text-center">Completed</td>
                                                             :''}
                                                             {(status1 == 2)?
                                                            <td className="text-center">Partial</td>
                                                             :''}
                                                             {(status1 == 3)?
                                                            <td className="text-center">Cancel</td>
                                                             :''}
                                                      <td>{_id}</td> 
                                                      </tr>
                                                       })
                                                }
                                                  </tbody>
                                              </table>*/}
                                                <ReactDatatable
                                config={this.config}
                                records={this.state.records}
                                columns={this.columns}
                                onPageChange={this.pageChange.bind(this)}
                            />
                        </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}

tradehistory.propTypes = {
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    records: state.records
});

export default connect(
    mapStateToProps
)(tradehistory);
