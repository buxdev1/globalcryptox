import React, { Component } from 'react';
import ReactDatatable from '@ashvin27/react-datatable';

class ServerSideData extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "name",
                text: "Name",
                className: "name",
                sortable: true
            },
            {
                key: "_id",
                text: "Id",
                className: "name",
                sortable: true
            },
            
            {
                key: "email",
                text: "Email",
                sortable: true
            }
         
        ];
        this.config = {
            page_size: 10,
            length_menu: [10, 20, 50],
            show_filter: true,
            show_pagination: true,
            button: {
                excel: false,
                print: false
            }
        }
        this.state = {
            total_pages: 0,
            records: [],
            count:0
        }
    }

    componentDidMount(){
        this.getData();
    }

    getData = (queryString = "") => {
        let url = "http://localhost:5000/api/testtable?" + queryString

        fetch(url, {
            headers: {
                "Accept": "application/json"
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log("Response",res)
            this.setState({
                total: res.total,
                records: res.result,
                count:res.totalcount
            })
        })

    }

    tableChangeHandler = data => {
        let queryString = Object.keys(data).map((key) => {
            if(key === "sort_order" && data[key]){
                return encodeURIComponent("sort_order") + '=' + encodeURIComponent(data[key].order) + '&' + encodeURIComponent("sort_column") + '=' + encodeURIComponent(data[key].column)
            } else {
                return encodeURIComponent(key) + '=' + encodeURIComponent(data[key])
            }
            
        }).join('&');

        this.getData(queryString);
    }

    render() {
        return (
            <ReactDatatable
                config={this.config}
                records={this.state.records}
                columns={this.columns}
                dynamic={true}
                total_record={this.state.count}
                onChange={this.tableChangeHandler}/>
        );
    }
}

export default ServerSideData;  