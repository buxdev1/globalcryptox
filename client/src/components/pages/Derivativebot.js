import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import classnames from "classnames";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import { derivativeBotfunction ,DerivativeTradeOrderplacing} from "../../actions/userActions";
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer} from "react-toastify";
import keys from "../../actions/config";
import { withRouter } from "react-router-dom";
import Select from 'react-select';
const url = keys.baseUrl;
var options = [];
const options1 = [ {'value':"buy", 'label':"Buy"},{'value':"sell", 'label':"Sell"}];
const options2= [ {'value':"Limit", 'label':"Limit"},{'value':"Market", 'label':"Market"}];
class Tradingbot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id : "",
            contact_person: "",
            email: "",
            sitename: "",
            site_description: "",
            phone_number : "",
            mobile_number : "",
            address : "",
            google_analytics : "",
            social_link1 : "",
            social_link2 : "",
            social_link3 : "",
            social_link4 : "",
            social_link5 : "",
            reg_code : "",
            company_info_link:"",
            license_info_link:"",
            copyright_text : "",
            sitelogo : "",
            sitelogourl: "",
            liveprice:0,
            perparray:[],
            ordertypechange:"",
            tradeprice:0,

            errors: {}
        };
    }

    componentDidMount() {
        this.getData()
    };

     componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
        if (nextProps.auth !== undefined
            && nextProps.auth.updatesettings !== undefined
            && nextProps.auth.updatesettings.data !== undefined
            && nextProps.auth.updatesettings.data.message !== undefined) {
            toast(nextProps.auth.updatesettings.data.message, {
                position: toast.POSITION.TOP_CENTER
            });
            nextProps.auth.updatesettings = undefined;
            this.getData();
            this.setState({errors : {}})
        }
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
    handleChange = (event) => {
        this.setState({
            sitelogourl: URL.createObjectURL(event.target.files[0]),
            sitelogo: event.target.files[0]
        })
    }

    handleselectChange = selectedOption => {
    this.setState({pair : selectedOption });
  var perpaarray=this.state.perparray
  var index1 = perpaarray.findIndex(
    (x) => x.tiker_root === selectedOption.value
  );
  console.log("index of the handle",index1);
  var liveprice=this.state.perparray[index1].markprice
  this.setState({liveprice:liveprice})
     };

     handleselectbuyChange = selectedOption => {
    this.setState({buyorsell : selectedOption });
  //  console.log(`Option selected:`, selectedOption );
     };

     handleselectOrdertypeChange = selectedOption => {
    this.setState({ordertypechange : selectedOption });
   // console.log(`Option selected:`, selectedOption );
     };
    getData() {
        axios
            .post(url+"api/perpetual-data")
            .then(res => {
                // this.setState();
                if(res.data)
                {
                  this.setState({perparray:res.data.data})
                    for(var i=0;i<res.data.data.length;i++)
                    {
                        var opt = {"value":res.data.data[i].tiker_root,"label":res.data.data[i].tiker_root}
                        options.push(opt);
                    }
                }
            })
            .catch()
    }

    onDeleteBot =e =>{
      e.preventDefault();
      const newCategory = {
        buyorsell: this.state.buyorsell,
        pairname:this.state.pair
      };
      console.log(newCategory, "newCategory");
      axios
      .post(url+"api/derivativebot-delete", newCategory)
      .then(res =>{
          if (res.status) {
              toast(res.data.message, {
                  position: toast.POSITION.TOP_CENTER,
              })
           }
      }).catch();

    }
    onTradeOrders =e =>{
      e.preventDefault();
      const data = {};
      data['tradeprice'] = this.state.tradeprice;
      data['tradequantity'] = this.state.tradequantity;
      data['buyorsell'] = this.state.buyorsell;
      data['pair'] = this.state.pair;
      data['ordertypechange'] = this.state.ordertypechange;
      this.props.DerivativeTradeOrderplacing(data)
    }

    onSettingsUpdate = e => {
        e.preventDefault();
        const data = {};
        console.log("liveprice",this.state.liveprice);
        console.log("live pricerangestart",this.state.pricerangestart);
        console.log("live pricerangeend",this.state.pricerangeend);
        if(this.state.buyorsell.value =="buy"){
          if(parseFloat(this.state.pricerangeend) < parseFloat(this.state.liveprice) &&
          parseFloat(this.state.pricerangeend)> parseFloat(this.state.pricerangestart)&&
            parseFloat(this.state.quantityrangeend)> parseFloat(this.state.quantityrangestart)&&
            parseFloat(this.state.quantityrangestart) >=0
          ){
            console.log(" live inside");
              data['pricerangestart'] = this.state.pricerangestart;
              data['pricerangeend'] = this.state.pricerangeend;
              data['quantityrangestart'] = this.state.quantityrangestart;
              data['quantityrangeend'] = this.state.quantityrangeend;
              data['buyorsell'] = this.state.buyorsell;
              data['ordercount'] = this.state.ordercount;
              data['pair'] = this.state.pair;
              this.props.derivativeBotfunction(data);
          }else{
            toast("Please check the Values Entered", {
                position: toast.POSITION.TOP_CENTER
            });
          }
        }  else{
          if(parseFloat(this.state.pricerangestart) > parseFloat(this.state.liveprice)&&
          parseFloat(this.state.pricerangeend)> parseFloat(this.state.pricerangestart)&&
            parseFloat(this.state.quantityrangeend)> parseFloat(this.state.quantityrangestart)&&
            parseFloat(this.state.quantityrangestart) >=0
         ){
            console.log("live inside");
              data['pricerangestart'] = this.state.pricerangestart;
              data['pricerangeend'] = this.state.pricerangeend;
              data['quantityrangestart'] = this.state.quantityrangestart;
              data['quantityrangeend'] = this.state.quantityrangeend;
              data['buyorsell'] = this.state.buyorsell;
              data['ordercount'] = this.state.ordercount;
              data['pair'] = this.state.pair;
              this.props.derivativeBotfunction(data);
          }else{
            toast("Please check the Values Entered", {
                position: toast.POSITION.TOP_CENTER
            });
          }
        }
        //
        // data['pricerangestart'] = this.state.pricerangestart;
        // data['pricerangeend'] = this.state.pricerangeend;
        // data['quantityrangestart'] = this.state.quantityrangestart;
        // data['quantityrangeend'] = this.state.quantityrangeend;
        // data['buyorsell'] = this.state.buyorsell;
        // data['ordercount'] = this.state.ordercount;
        // data['pair'] = this.state.pair;
        // this.props.derivativeBotfunction(data);
    };

    render() {
        const { errors } = this.state;
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>

                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <button className="btn mt-3" id="menu-toggle"><FontAwesomeIcon icon={faList}/></button>
                            <h3 className="mt-2 text-secondary">Derivative Trading bot</h3>
                            <form noValidate onSubmit={this.onSettingsUpdate} id="update-settings">
                            <div className="row mt-2">
                               <div className="col-md-3">
                                   <label htmlFor="copyright_text">Pair</label>
                               </div>
                               <div className="col-md-6">
                                     <Select
                                       value={this.state.pair}
                                       onChange={this.handleselectChange}
                                       options={options}
                                   />
                               </div>
                           </div>

                           <div className="row mt-2">
                              <div className="col-md-3">
                                  <label htmlFor="copyright_text">Live Price</label>
                              </div>
                              <div className="col-md-6">
                                  {this.state.liveprice}
                              </div>
                          </div>

                             <div className="row mt-2">
                               <div className="col-md-3">
                                   <label htmlFor="copyright_text">Buy/Sell</label>
                               </div>
                               <div className="col-md-6">
                                     <Select
                                       value={this.state.buyorsell}
                                       onChange={this.handleselectbuyChange}
                                       options={options1}
                                   />
                               </div>
                           </div>

                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="pricerangestart">Price range start </label>
                                        </div>
                                        <div className="col-md-6">
                                            <input
                                                onChange={this.onChange}
                                                value={this.state.pricerangestart}
                                                id="pricerangestart"
                                                type="number"
                                                error={errors.pricerangestart}
                                                className={classnames("form-control", {
                                                    invalid: errors.pricerangestart
                                                })}/>
                                            <span className="text-danger">{errors.pricerangestart}</span>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="email">Price range End</label>
                                        </div>
                                        <div className="col-md-6">
                                             <input
                                                onChange={this.onChange}
                                                value={this.state.pricerangeend}
                                                id="pricerangeend"
                                                type="number"
                                                error={errors.pricerangeend}
                                                className={classnames("form-control", {
                                                    invalid: errors.pricerangeend
                                                })}/>
                                            <span className="text-danger">{errors.pricerangeend}</span>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="quantityrangestart">Quantity range start</label>
                                        </div>
                                        <div className="col-md-6">
                                             <input
                                                onChange={this.onChange}
                                                value={this.state.quantityrangestart}
                                                id="quantityrangestart"
                                                type="number"
                                                error={errors.quantityrangestart}
                                                className={classnames("form-control", {
                                                    invalid: errors.quantityrangestart
                                                })}/>
                                            <span className="text-danger">{errors.quantityrangestart}</span>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="quantityrangeend">Quantity range end</label>
                                        </div>
                                        <div className="col-md-6">
                                             <input
                                                onChange={this.onChange}
                                                value={this.state.quantityrangeend}
                                                id="quantityrangeend"
                                                type="number"
                                                error={errors.quantityrangeend}
                                                className={classnames("form-control", {
                                                    invalid: errors.quantityrangeend
                                                })}/>
                                            <span className="text-danger">{errors.quantityrangeend}</span>
                                        </div>
                                    </div>
                                     <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="ordercount">Order count</label>
                                        </div>
                                        <div className="col-md-6">
                                             <input
                                                onChange={this.onChange}
                                                value={this.state.ordercount}
                                                id="ordercount"
                                                type="number"
                                                error={errors.ordercount}
                                                className={classnames("form-control", {
                                                    invalid: errors.ordercount
                                                })}/>
                                            <span className="text-danger">{errors.ordercount}</span>
                                        </div>
                                    </div>



                                </form>
                                    <br />
                                <button
                                    form="update-settings"
                                    type="submit"
                                    className="btn btn-primary">
                                    Place Derivative Order
                                </button>




                                <h3 className="mt-2 text-secondary">Delete Derivative Orders</h3>
                                <form noValidate onSubmit={this.onDeleteBot} id="delete-bot">
                                <div className="row mt-2">
                                   <div className="col-md-3">
                                       <label htmlFor="copyright_text">Pair</label>
                                   </div>
                                   <div className="col-md-6">
                                         <Select
                                           value={this.state.pair}
                                           onChange={this.handleselectChange}
                                           options={options}
                                       />
                                   </div>
                                </div>
                                 <div className="row mt-2">
                                   <div className="col-md-3">
                                       <label htmlFor="copyright_text">Buy/Sell</label>
                                   </div>
                                   <div className="col-md-6">
                                         <Select
                                           value={this.state.buyorsell}
                                           onChange={this.handleselectbuyChange}
                                           options={options1}
                                       />
                                   </div>
                                </div>

                                    </form>
                                        <br />
                                    <button
                                        form="delete-bot"
                                        type="submit"
                                        className="btn btn-primary">
                                        Delete Derivative Order
                                    </button>


                                    <h3 className="mt-2 text-secondary">Place Derivative Trade Orders</h3>
                                    <form noValidate onSubmit={this.onTradeOrders} id="Trade-bot">
                                    <div className="row mt-2">
                                       <div className="col-md-3">
                                           <label htmlFor="copyright_text">Pair</label>
                                       </div>
                                       <div className="col-md-6">
                                             <Select
                                               value={this.state.pair}
                                               onChange={this.handleselectChange}
                                               options={options}
                                           />
                                       </div>
                                    </div>
                                    <div className="row mt-2">
                                       <div className="col-md-3">
                                           <label htmlFor="copyright_text">Live Price</label>
                                       </div>
                                       <div className="col-md-6">
                                           {this.state.liveprice}
                                       </div>
                                   </div>
                                     <div className="row mt-2">
                                       <div className="col-md-3">
                                           <label htmlFor="copyright_text">Buy/Sell</label>
                                       </div>
                                       <div className="col-md-6">
                                             <Select
                                               value={this.state.buyorsell}
                                               onChange={this.handleselectbuyChange}
                                               options={options1}
                                           />
                                       </div>
                                    </div>
                                    <div className="row mt-2">
                                      <div className="col-md-3">
                                          <label htmlFor="copyright_text">Limit/Market</label>
                                      </div>
                                      <div className="col-md-6">
                                            <Select
                                              value={this.state.ordertypechange}
                                              onChange={this.handleselectOrdertypeChange}
                                              options={options2}
                                          />
                                      </div>
                                   </div>

                                   {this.state.ordertypechange.value=="Limit"?(
                                     <div className="row mt-2">
                                         <div className="col-md-3">
                                             <label htmlFor="email">Price </label>
                                         </div>
                                         <div className="col-md-6">
                                              <input
                                                 onChange={this.onChange}
                                                 value={this.state.tradeprice}
                                                 id="tradeprice"
                                                 type="number"
                                                 error={errors.tradeprice}
                                                 className={classnames("form-control", {
                                                     invalid: errors.tradeprice
                                                 })}/>
                                             <span className="text-danger">{errors.tradeprice}</span>
                                         </div>
                                     </div>

                                   ):(" ")}

                                   <div className="row mt-2">
                                       <div className="col-md-3">
                                           <label htmlFor="tradequantity">Quantity </label>
                                       </div>
                                       <div className="col-md-6">
                                            <input
                                               onChange={this.onChange}
                                               value={this.state.tradequantity}
                                               id="tradequantity"
                                               type="number"
                                               error={errors.tradequantity}
                                               className={classnames("form-control", {
                                                   invalid: errors.tradequantity
                                               })}/>
                                           <span className="text-danger">{errors.tradequantity}</span>
                                       </div>
                                   </div>

                                        </form>
                                            <br />
                                        <button
                                            form="Trade-bot"
                                            type="submit"
                                            className="btn btn-primary">
                                            Place Derivative Single Order
                                        </button>




                        </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}


Tradingbot.propTypes = {
    Botfunction: PropTypes.func.isRequired,
    DerivativeTradeOrderplacing: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { derivativeBotfunction,DerivativeTradeOrderplacing }
)(withRouter(Tradingbot));
