const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserdepositSchema = new Schema({
  	userId : {
		type: mongoose.Schema.Types.ObjectId, ref: 'users',index: true
	},
	amount:{
		type  : String, index: true
	},
	accountnumber:{
		type  : String
	},
	useramount:{
		type  : Number
	},
	fees:{
		type  : Number
	},
	status:{
		type:String,default:1  //1 user submit request 2 accept 3 reject
	},
	batch:{
		type:String
	},
	created_date:{
		type:Date,default:Date.now
	}
});

module.exports  = mongoose.model("pmwithdraw",UserdepositSchema,"pmwithdraw");
