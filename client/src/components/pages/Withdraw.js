import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from '@ashvin27/react-datatable';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Select from "react-select";
import {Modal,Button} from 'react-bootstrap/';
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer} from "react-toastify";
import $ from 'jquery';
import keys from "../../actions/config";
import Loader from 'react-loader-spinner'
// import "../../react-loader-spinner/dist/loader/css/react-spinner-loader.css"
const url = keys.baseUrl;

class Withdraw extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                key: "receiveraddress",
                text: "To Address",
                className: "receiveraddress",
                align: "left",
                sortable: true,
                width:200,

            },
            {
                key: "cryptoType",
                text: "Coin",
                className: "cryptoType",
                align: "left",
                sortable: true
            },
            {
                key: "userId",
                text: "User Name",
                className: "userId",
                align: "left",
                sortable: true,

            },
            {
                key: "transferamount",
                text: "Transfer Amount",
                className: "transferamount",
                align: "left",
                sortable: true
            },
            {
                key: "status",
                text: "Status",
                className: "status",
                align: "left",
                sortable: true
            },
             {
                key: "created_date",
                text: "Created date",
                className: "created_date",
                align: "left",
                sortable: true
            },
            // {
            //     key: "transactionId",
            //     text: "transactionId",
            //     className: "transactionId",
            //     align: "left",
            //     sortable: true
            // },
            {
                key: "action",
                text: "Action",
                className: "action",
                width: 100,
                align: "left",
                sortable: false,
                cell: record => {
                        if(record.status=="Confirmed")
                        {
                           return (
                        <Fragment>
                            <button
                                data-toggle="modal"
                                data-target="#show-withdraw-modal"
                                className="fa fa-stack-exchange"
                                onClick={() => this.showtransactionid(record)}
                                style={{marginRight: '5px'}}>
                            </button>
                            <button
                              data-toggle="modal"
                              data-target="#update-faq-modal"
                              className="btn btn-primary btn-sm"
                              onClick={() => this.editRecord(record)}
                              style={{ marginRight: "5px" }}
                            >
                              <i className="fa fa-edit"></i>
                            </button>
                        </Fragment>
                    );
                        }
                        if(record.status=="Pending")
                        {

                    return (
                        <Fragment>
                            <button
                                data-toggle="modal"
                                data-target="#show-withdraw-modal"
                                className="btn btn-primary btn-sm"
                                onClick={() => this.showDetails(record)}
                                style={{marginRight: '5px'}}>
                                <i className="fa fa-check"></i>
                            </button>
                        </Fragment>
                    );
                        }
                }
            }
        ];

        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
            filename: "Withdraw",
            no_data_text: 'No Withdraw found!',
            sort:{column: "Created date", order: "desc"},
            language: {
                length_menu: "Show _MENU_ result per page",
                filter: "Filter in records...",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                pagination: {
                    first: "First",
                    previous: "Previous",
                    next: "Next",
                    last: "Last"
                }
            },
            show_length_menu: true,
            show_filter: true,
            show_pagination: true,
            show_info: true,
        };

        this.state = {
                records: [],
                showDetails : false,
                showReason  : false,
                showDetails1 : false,
                id: '',
                status : '',
                transferamount: '',
                cryptoType: '',
                userId: '',
                receiveraddress: '',
                transactionId: '',
                tagid: '',
                errors: '',
                showloader:false,
                editDetailshow:false,
                transactionIdedited:"",
                rejectreason:""

        };
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getData()
    };

    onChange = (e) => {
      this.setState({ [e.target.id]: e.target.value });
    };
    showtransactionid = (record) => {
       this.setState({transactionId:record.transactionId})
       this.setState({showDetails1:true})
    }
    showDetails = (record) => {
      console.log(record,'record')
      this.setState({receiveraddress:record.receiveraddress})
      this.setState({tagid:''})
      this.setState({id:record._id})
      this.setState({cryptoType:record.cryptoType})
      this.setState({transferamount:record.transferamount})
      this.setState({status:record.status})
      this.setState({showDetails:true})
    }

    editRecord = (record) => {
      console.log(record,'recordedit')
      this.setState({receiveraddress:record.receiveraddress})
      this.setState({tagid:''})
      this.setState({id:record._id})
      this.setState({transactionId:record.transactionId})
      this.setState({cryptoType:record.cryptoType})
      this.setState({transferamount:record.transferamount})
      this.setState({status:record.status})
      this.setState({editDetailshow:true})
    }
    EditTransactionId =() =>{
      this.setState({editDetailshow:false,transactionId:""})
      var id = this.state.id;
      var passVal = {id:id,transactionId:this.state.transactionIdedited};
      // console.log("passavla0",passVal);
      axios
          .post(url+"api/updatetransactionid",passVal)
          .then(res => {
            toast(res.data.message, {
                position: toast.POSITION.TOP_CENTER
            });
            this.getData();
          })
          .catch()
    }
    confirmSubmit = () => {
      var id = this.state.id;
      var passVal = {id:id,status:"Confirmed",transactionId:this.state.transactionId};

      if(this.state.cryptoType=="USD"){
        if(this.state.transactionId==""){
          return  toast("Please Enter the Transaction ID", {
                position: toast.POSITION.TOP_CENTER
            });
        }
      }
            this.setState({showloader:true});

      axios
          .post(url+"api/updatewithdraw",passVal)
          .then(res => {
            if(res.data.status){
              toast(res.data.message, {
                position: toast.POSITION.TOP_CENTER
            });
            this.setState({showDetails:false,showloader:false});
            this.getData();
          }else{
                 toast(res.data.message, {
                position: toast.POSITION.TOP_CENTER
            });
          }
        })
          .catch()
          }
    

    rejectReason = () => {
        this.setState({showReason:true});    
        this.setState({showDetails:false})
                
    }
    rejectSubmit = () => {
        this.setState({showloader:true});
        var id = this.state.id;
        var passVal = {id:id,status:"Rejected",Reason:this.state.rejectreason};
        axios
            .post(url+"api/updatewithdraw",passVal)
            .then(res => {
              this.setState({showloader:false});
              toast(res.data.message, {
                  position: toast.POSITION.TOP_CENTER
              });
              this.setState({showDetails:false});
              this.getData();
            })
            .catch()
      }
      handleShowReason = (record)  => {
        this.setState({showReason:false})

      }
    handleClosedetails = (record) => {
      this.setState({showDetails:false})
    }
     handleClosedetails1 = (record) => {
      this.setState({showDetails1:false})
    }
    handleClosedetails2 = (record) => {
     this.setState({editDetailshow:false,transactionId:""})
   }
    getData() {
        axios
            .post(url+"api/withdraw-data")
            .then(res => {
              console.log(res);
                this.setState({ records: res.data})
            })
            .catch()
    }


    pageChange(pageData) {
        console.log("OnPageChange", pageData);
    }

    render() {
        return (
            <div>


 <Modal show={this.state.showReason} onHide={this.handleShowReason}  aria-labelledby="contained-modal-title-vcenter" centered>
         <Modal.Header closeButton>
         <Modal.Title>Reason</Modal.Title>
         </Modal.Header>
    <Modal.Body>
    <div className="col-md-3">
                      <input  type="text"
                    onChange={this.onChange}
                    value={this.state.rejectreason}
                    id="rejectreason"
     
                 />
                    <div><center><Button variant="success btnDefaultNew"  onClick={this.rejectSubmit}>
                    Submit
                    </Button>
                    </center></div>
                  </div>
                  </Modal.Body>
</Modal>

            <Modal show={this.state.editDetailshow} onHide={this.handleClosedetails2}  aria-labelledby="contained-modal-title-vcenter" centered>
              <Modal.Header closeButton>
              <Modal.Title>Edit Transaction Id</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <div className="popUpSpace">
              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">To address</label>
                  </div>
                  <div className="col-md-9" style={{"word-wrap":"break-word"}}>
                      {this.state.receiveraddress}
                  </div>
              </div>
              {this.state.transactionId==""?(
                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="description">TransactionId</label>
                  </div>
                  <div className="col-md-9">
                  <input
                    onChange={this.onChange}
                    defaultValue={this.state.transactionId}
                    value={this.state.transactionIdedited}
                    id="transactionIdedited"
                    type="text"
                  />
                  </div>
                </div>
              ):(
                <div className="row mt-2">
                    <div className="col-md-3">
                        <label htmlFor="answer">TransactionId</label>
                    </div>
                    <div className="col-md-9" style={{"word-wrap":"break-word"}}>
                        {this.state.transactionId}
                    </div>
                </div>
              )}


              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Transfer Coin</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.cryptoType}
                  </div>
              </div>
              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Transfer Amount</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.transferamount}
                  </div>
              </div>
              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Status</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.status}
                  </div>
              </div>
              </div>
              </Modal.Body>
              <Modal.Footer>

              {this.state.transactionId==""?(
                <div><center>
                <span> <Button onClick={this.EditTransactionId} variant="success btnDefaultNew" >
                Update
                </Button></span>
                </center></div>
              ):("")}
              </Modal.Footer>
            </Modal>



            <Modal show={this.state.showDetails} onHide={this.handleClosedetails}  aria-labelledby="contained-modal-title-vcenter" centered>
      				<Modal.Header closeButton>
      				<Modal.Title>Details</Modal.Title>
      				</Modal.Header>
      				<Modal.Body>
      				<div className="popUpSpace">
              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">To address</label>
                  </div>
                  <div className="col-md-9" style={{"word-wrap":"break-word"}}>
                      {this.state.receiveraddress}
                  </div>
              </div>

              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Tag id/Memo</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.tagid}
                  </div>
              </div>
              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Transfer Coin</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.cryptoType} 
                  </div>
              </div>
              
                    <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Transfer Amount</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.transferamount}
                  </div>
              </div>

                  {
                (this.state.cryptoType == "USD" )?(
                    <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Transaction ID</label>
                  </div>
                  <div className="col-md-9">
                  <input
                    type="text"
                    value={this.state.transactionId}
                    name="transactionId"
                    id="transactionId"
                    onChange={this.onChange}
                     />
                  </div>
              </div>
              ):("")}
       
            
            
              <div className="row mt-2">
                  <div className="col-md-3">
                      <label htmlFor="answer">Status</label>
                  </div>
                  <div className="col-md-9">
                      {this.state.status}
                  </div>
              </div>
            	</div>
      				</Modal.Body>
      				<Modal.Footer>

                {
                (this.state.showloader)?
                <div style={{width:"250px"}}>
                    <Loader
                    type="Circles"
                    color="#00BFFF"
                    height={50}
                    width={50}
                    />
                  </div>
                :
                    <div><center><Button variant="danger btnDefaultNewBlue" onClick={this.rejectReason}>
                    Reject
                    </Button>
                    <span> <Button onClick={this.confirmSubmit} variant="success btnDefaultNew" >
                    Confirm
                    </Button></span>
                    <p>*Note Confirm option should take some time please wait</p>
                    </center></div>
                }

      				</Modal.Footer>
      			</Modal>
            <Modal show={this.state.showDetails1} onHide={this.handleClosedetails1}  aria-labelledby="contained-modal-title-vcenter" centered>
              <Modal.Header closeButton>
              <Modal.Title>Transaction Id</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <div className="popUpSpace">
              <div className="row mt-2">

                  <div className="col-md-12" style={{"word-wrap":"break-word"}}>
                      {this.state.transactionId}
                  </div>
              </div>


              </div>
              </Modal.Body>
              <Modal.Footer>
              </Modal.Footer>
            </Modal>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>

                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <h3 className="mt-2 text-secondary">Withdraw List</h3>
                            <ReactDatatable
                                config={this.config}
                                records={this.state.records}
                                columns={this.columns}
                                onPageChange={this.pageChange.bind(this)}
                            />
                        </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}

Withdraw.propTypes = {
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    records: state.records
});

export default connect(
    mapStateToProps
)(Withdraw);
