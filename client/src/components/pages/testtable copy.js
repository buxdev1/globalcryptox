import React from 'react';
import axios from 'axios';
// import './App.css';


import {
  Table,
  Pagination
} from "react-bootstrap";

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      totalPage: 0,
      pageOfItems: [],


    };

    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  componentDidMount() {
    this.handleLoadMore(1);
  }

  async handleLoadMore(page) {
      console.log("respo pafge",page)

    try {

      let response = await axios.get('http://localhost:5000/api/testtable?page=' + page + '&limit='+5);


      console.log("response", response)
      if (response.data) {
        this.setState({
          currentPage: response.data.page,
          totalPage: response.data.pages,
        //   pageOfItems: response.data.result,
                  pageOfItems: response.data.events,
        })
      }

    } catch (err) {
      console.log("error", err);
    }

  }

  render() {

    const { pageOfItems, currentPage, totalPage } = this.state;
    console.log("respo totalPage,",totalPage)
    console.log("respo currentPage,",currentPage)

    return (
      <div>


        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th>Email</th>
              {/* <th>Action</th> */}
            </tr>
          </thead>
          <tbody>
            {
              pageOfItems && pageOfItems.length > 0 && pageOfItems.map((el) => {
                return (
                  <tr>
                    <td>{el.id}</td>
                    <td>{el.name}</td>
                    <td>{el.email}</td>
                    {/* <td></td> */}
                  </tr>
                )
              })
            }

          </tbody>
        </Table>

        <Pagination>
          <Pagination.Item active >{currentPage}</Pagination.Item>
          {
            totalPage > parseInt(currentPage) + 1 && <Pagination.Item onClick={() => this.handleLoadMore(parseInt(currentPage) + 1)}>{parseInt(currentPage) + 1}</Pagination.Item>
          }

          {
            totalPage > parseInt(currentPage) + 2 && parseInt(currentPage) + 2 > 0 && <Pagination.Item onClick={() => this.handleLoadMore(parseInt(currentPage) + 2)}>{parseInt(currentPage) + 2}</Pagination.Item>
          }

          {
            totalPage > parseInt(currentPage) + 3 && parseInt(currentPage) + 3 > 0 && <Pagination.Item onClick={() => this.handleLoadMore(parseInt(currentPage) + 3)}>{parseInt(currentPage) + 3}</Pagination.Item>
          }

          {
            totalPage > parseInt(currentPage) && <Pagination.Ellipsis />
          }



          {
            totalPage > parseInt(currentPage) && <Pagination.Item onClick={() => this.handleLoadMore(parseInt(totalPage))}>{totalPage}</Pagination.Item>
          }

        </Pagination>

      </div>
    )
  }
}

export default App;
