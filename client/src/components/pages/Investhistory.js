import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from '@ashvin27/react-datatable';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Select from "react-select";
import {Modal,Button} from 'react-bootstrap/';
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer} from "react-toastify";
import $ from 'jquery';
import keys from "../../actions/config";
const url = keys.baseUrl;

class Deposit extends Component {
    constructor(props) {
        super(props);
        this.columns = [
        
            {
                key: "email",
                text: "Email",
                className: "toaddress",
                align: "left",
                sortable: true,
            },     
            
            {
                key: "Investamount",
                text: "Invest Amount",
                className: "amount",
                align: "left",
                sortable: true
            },
            //  {
            //     key: "txid",
            //     text: "Transaction Id",
            //     className: "txid",
            //     align: "left",
            //     sortable: true,
            //     width:100,
            //     cell: record => { 
            //         return (
            //             <Fragment>
            //                 <p
            //                     style={{"word-wrap": 'break-word'}}>
            //                     {record.txid}
            //                 </p>
                           
            //             </Fragment>
            //         );
            //     }
            // },
            {
                key: "created_date",
                text: "Created date",
                className: "created_date",
                align: "left",
                sortable: true
            },
            
        ];

        this.config = {
            page_size: 10,
            length_menu: [ 10, 20, 50 ],
            filename: "Withdraw",
            no_data_text: 'No Records found!',
            sort:{column: "Created date", order: "desc"},
            language: {
                length_menu: "Show _MENU_ result per page",
                filter: "Filter in records...",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                pagination: {
                    first: "First",
                    previous: "Previous",
                    next: "Next",
                    last: "Last"
                }
            },
            show_length_menu: true,
            show_filter: true,
            show_pagination: true,
            show_info: true,
        };

        this.state = {
                records: [],
                showDetails : false,
                id: '',
                status : '',
                transferamount: '',
                cryptoType: '',
                userId: '',
                receiveraddress: '',
                tagid: '',
                errors: ''

        };
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getData()
    };

    getData() {
        axios
            .get(url+"api/invest-data")
            .then(res => {
                console.log("ress",res.data)
                this.setState({ records: res.data})
            })
            .catch()
    }


    pageChange(pageData) {
        console.log("OnPageChange", pageData);
    }

    render() {
        return (
            <div>
             <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <h3 className="mt-2 text-secondary">Invest List</h3>
                            <ReactDatatable
                                config={this.config}
                                records={this.state.records}
                                columns={this.columns}
                                onPageChange={this.pageChange.bind(this)}
                            />
                        </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}

Deposit.propTypes = {
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    records: state.records
});

export default connect(
    mapStateToProps
)(Deposit);
