import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addcurrency } from "../../actions/currency";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import Select from "react-select";
import $ from "jquery";
import { Editor } from "@tinymce/tinymce-react";

import "react-toastify/dist/ReactToastify.css";

const options = [
  { value: "Crypto", label: "Crypto" },
  { value: "Token", label: "Token" },
  { value: "Fiat", label: "Fiat" },
];

class CurrencyAddModal extends React.Component {
  constructor() {
    super();
    this.state = {
      currencyName: "",
      currencySymbol: "",
      fee: "",
      minimum: "",
      status: "",
      currencyType: "",
      contractaddress: "",
      minabi: "",
      errors: {},
      curimage:null,
      attachment:"",
      imageerror:"",
      curimagesize:0,
      decimals:0
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      $("#add-currency-modal").find(".text-danger").show();
      this.setState({
        errors: nextProps.errors,
      });
    } else {
      this.setState({
        errors: "",
      });
    }
    //console.log(nextProps.auth.currencyadd.data,'shdbjksfcgdsyfvfdsuy');
    if (
      nextProps.auth !== undefined &&
      nextProps.auth.currencyadd !== undefined &&
      nextProps.auth.currencyadd.data !== undefined &&
      nextProps.auth.currencyadd.data.message !== undefined
    ) {
      $("#add-currency-modal").modal("hide");
      this.setState({
        errors: "",
      });
      toast(nextProps.auth.currencyadd.data.message, {
        position: toast.POSITION.TOP_CENTER,
      });
      nextProps.auth.currencyadd = undefined;
      this.setState({
        errors: "",
      });
    }
  }
  handleChange = (optionsTerms) => {
    this.setState({ currencyType: optionsTerms.value });
  };

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };


  handleChangeefile = (event) => {
if(event.target.files[0]){
    var filesize= event.target.files[0].size
    if(filesize>20000){
      this.setState({curimagesize:filesize})
      this.setState({imageerror:"Image size should be less than  20 Kb"})
      toast("Image Size should be less than 20 Kb", {
        position: toast.POSITION.TOP_CENTER,
      });
    }else{
    this.setState({
      curimage: URL.createObjectURL(event.target.files[0]),
      attachment: event.target.files[0],
    });
  }
}

  };

  onCurrencyAdd = (e) => {
    e.preventDefault();
    if(this.state.curimagesize<20000){
console.log("this.state.minabi",this.state.minabi)
    const newCurrency = {
      currencyName: this.state.currencyName,
      currencySymbol: this.state.currencySymbol,
      fee: this.state.fee,
      currencyType: this.state.currencyType,
      contractaddress: this.state.contractaddress,
      minabi: this.state.minabi,
    };
    const data = new FormData();
    data.append("currencyName", this.state.currencyName);
    data.append("currencySymbol", this.state.currencySymbol);
    data.append("fee", this.state.fee);
    data.append("contractaddress", this.state.contractaddress);
    data.append("currencyType", this.state.currencyType);
    data.append("minabi", this.state.minabi);
    data.append("decimals", this.state.decimals);

    data.append("file", this.state.attachment);

    this.props.addcurrency(data);
  }else{
    toast("Image Size should be less than 20 Kb", {
      position: toast.POSITION.TOP_CENTER,
    });
  }
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <div className="modal fade" id="add-currency-modal" data-reset="true">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add Currency</h4>
                <button type="button" className="close" data-dismiss="modal">
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <form
                  noValidate
                  onSubmit={this.onCurrencyAdd}
                  id="add-currency"
                >
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencyName">Currency Type</label>
                    </div>
                    <div className="col-md-9">
                      <Select
                        // value={selectedTerms}
                        onChange={this.handleChange}
                        options={options}
                        label="Single select"
                      />
                      <span className="text-danger">{errors.currencyType}</span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencyName">Currency Name</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.currencyName}
                        id="currencyName"
                        type="text"
                        error={errors.currencyName}
                        className={classnames("form-control", {
                          invalid: errors.currencyName,
                        })}
                      />
                      <span className="text-danger">{errors.currencyName}</span>
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencySymbol">Currency Symbol</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.currencySymbol}
                        error={errors.currencySymbol}
                        id="currencySymbol"
                        type="text"
                        className={classnames("form-control", {
                          invalid: errors.currencySymbol,
                        })}
                      />
                      <span className="text-danger">
                        {errors.currencySymbol}
                      </span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencySymbol">Decimals</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.decimals}
                        error={errors.decimals}
                        id="decimals"
                        type="number"
                        className={classnames("form-control", {
                          invalid: errors.decimals,
                        })}
                      />
                      <span className="text-danger">
                        {errors.decimals}
                      </span>
                    </div>
                  </div>

                  {this.state.currencyType == "Token" ? (
                    <div>
                      <div className="row mt-2">
                        <div className="col-md-3">
                          <label htmlFor="currencyName">Contract Address</label>
                        </div>
                        <div className="col-md-9">
                          <input
                            onChange={this.onChange}
                            value={this.state.contractaddress}
                            error={errors.contractaddress}
                            id="contractaddress"
                            type="text"
                            className={classnames("form-control", {
                              invalid: errors.contractaddress,
                            })}
                          />
                          <span className="text-danger">
                            {errors.contractaddress}
                          </span>
                        </div>
                      </div>

                      <div className="row mt-2">
                        <div className="col-md-3">
                          <label htmlFor="currencyName">Min ABI</label>
                        </div>
                        <div className="col-md-9">
                          <textarea
                            onChange={this.onChange}
                            value={this.state.minabi}
                            error={errors.minabi}
                            id="minabi"
                            type="text"
                            className={classnames("form-control", {
                              invalid: errors.minabi,
                            })}
                          />
                          <span className="text-danger">{errors.minabi}</span>
                        </div>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="fee">Withdrawal Fee</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.fee}
                        error={errors.fee}
                        id="fee"
                        type="text"
                        className={classnames("form-control", {
                          invalid: errors.fee,
                        })}
                      />
                      <span className="text-danger">{errors.fee}</span>
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="minimum">Minimum Withdrawal</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.minimum}
                        error={errors.minimum}
                        id="minimum"
                        type="text"
                        className={classnames("form-control", {
                          invalid: errors.minimum,
                        })}
                      />
                      <span className="text-danger">{errors.minimum}</span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="minimum">Currency ICON</label>
                    </div>
                    <div className="col-md-9">
                    <input
                            type="file"
                            accept="image/x-png,image/gif,image/jpeg"
                            onChange={this.handleChangeefile}
                            name="file1"
                            id="exampleInputFile"
                            aria-describedby="fileHelp"
                          />
                          <span className="text-danger">{this.state.imageerror}</span>
                           <img
                          className="img-fluid proofThumb"
                          src={this.state.curimage}
                        />
                    </div>
                  </div>

                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  form="add-currency"
                  type="submit"
                  className="btn btn-primary"
                >
                  Add currency
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CurrencyAddModal.propTypes = {
  addcurrency: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { addcurrency })(
  withRouter(CurrencyAddModal)
);
