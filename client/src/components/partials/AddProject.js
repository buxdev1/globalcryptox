import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addproject } from "../../actions/faqActions";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import $ from "jquery";
import { Editor } from "@tinymce/tinymce-react";
import axios from "axios";
import "react-toastify/dist/ReactToastify.css";
import Select from "react-select";
import MultiSelect from "@khanacademy/react-multi-select";

import keys from "../../actions/config";

const url = keys.baseUrl;

const options = [
  { value: "Week", label: "Week" },
  { value: "Day", label: "Day" },
];

class FaqAddModal extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      description: "",
      returnpercentage: "",
      lockerduration: "",
      pdf: null,
      errors: {},
      selectedTerms: null,
      selected: [],
      validation:[],
      mininvestment:"",
      maxinvestment:"",
      currencyType:""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      $("#add-faq-modal").find(".text-danger").show();
      this.setState({
        errors: nextProps.errors,
      });
    }
    console.log(nextProps);
    if (
      nextProps.auth !== undefined &&
      nextProps.auth.faq !== undefined &&
      nextProps.auth.faq.data !== undefined &&
      nextProps.auth.faq.data.message !== undefined
    ) {
      $("#add-faq-modal").modal("hide");
      toast(nextProps.auth.faq.data.message, {
        position: toast.POSITION.TOP_CENTER,
      });
      this.setState({
        name: "",
        description: "",
        returnpercentage: "",
        lockerduration: "",
      })
      nextProps.auth.faq = undefined;
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  handleChange = (optionsTerms) => {
    this.setState({ currencyType: optionsTerms.value });
  };


  handleTermsChange = (selectedTerms) => {
    this.setState({ selectedTerms });
    console.log(`Option selected:`, selectedTerms);
  };

  onFaqAdd = (e) => {
    e.preventDefault();
      // let fields = {};
      // fields["returnpercentage"] = "";
      // fields["lockerduration"] = "";
      // this.setState({ validation: fields });
    const data = {
      name: this.state.name,
      description: this.state.description,
      returnpercentage: this.state.returnpercentage,
      lockerduration: this.state.lockerduration,
      mininvestment:this.state.mininvestment,
      maxinvestment:this.state.maxinvestment,
      currencyType: this.state.currencyType,
    };
        this.props.addproject(data);
  };

  render() {
    const { errors, selectedTerms, selected } = this.state;
    return (
      <div>
        <div className="modal fade" id="add-faq-modal" data-reset="true">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add Finance</h4>
                <button type="button" className="close" data-dismiss="modal">
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <form noValidate onSubmit={this.onFaqAdd} id="add-faq">
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        name="name"
                        onChange={this.onChange}
                        value={this.state.name}
                        error={errors.name}
                        id="name"
                        type="text"
                        name="name"
                        className={classnames("form-control", {
                          invalid: errors.name,
                        })}
                      />
                      <span className="text-danger">{errors.name}</span>
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="description">Description</label>
                    </div>
                    <div className="col-md-9">
                      <textarea
                        onChange={this.onChange}
                        value={this.state.description}
                        error={errors.description}
                        id="description"
                        type="text"
                        className={classnames("form-control", {
                          invalid: errors.description,
                        })}
                      />
                      <span className="text-danger">{errors.description}</span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="returnpercentage">
                         Percentage
                      </label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}

                        value={this.state.returnpercentage}
                        error={errors.returnpercentage}
                        id="returnpercentage"
                        type="number"

                        className={classnames("form-control", {
                          invalid: errors.returnpercentage,
                        })}
                      />
                      <span className="text-danger">
                        {errors.returnpercentage}
                      </span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencyName">Return Type</label>
                    </div>
                    <div className="col-md-9">
                      <Select
                        // value={selectedTerms}
                        onChange={this.handleChange}
                        options={options}
                        label="Single select"
                      />
                      <span className="text-danger">{errors.currencyType}</span>
                    </div>
                  </div>


                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="lockerduration">
                        Finance Duration
                      </label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.lockerduration}
                        error={errors.lockerduration}
                        id="lockerduration"
                        type="number"
                        className={classnames("form-control", {
                          invalid: errors.lockerduration,
                        })}
                      />

                      <span className="text-danger">{errors.lockerduration}</span>
                    </div>
                  </div>


                                    <div className="row mt-2">
                                      <div className="col-md-3">
                                        <label htmlFor="lockerduration">
                                          Min Investment Amount
                                        </label>
                                      </div>
                                      <div className="col-md-9">
                                        <input
                                          onChange={this.onChange}
                                          value={this.state.mininvestment}
                                          error={errors.mininvestment}
                                          id="mininvestment"
                                          type="number"
                                          className={classnames("form-control", {
                                            invalid: errors.mininvestment,
                                          })}
                                        />

                                        <span className="text-danger">{errors.mininvestment}</span>
                                      </div>
                                    </div>


                                    <div className="row mt-2">
                                      <div className="col-md-3">
                                        <label htmlFor="lockerduration">
                                          Max Investment Amount
                                        </label>
                                      </div>
                                      <div className="col-md-9">
                                        <input
                                          onChange={this.onChange}
                                          value={this.state.maxinvestment}
                                          error={errors.maxinvestment}
                                          id="maxinvestment"
                                          type="number"
                                          className={classnames("form-control", {
                                            invalid: errors.maxinvestment,
                                          })}
                                        />

                                        <span className="text-danger">{errors.maxinvestment}</span>
                                      </div>
                                    </div>



                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  form="add-faq"
                  type="submit"
                  className="btn btn-primary"
                >
                  Add Finance
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FaqAddModal.propTypes = {
  addproject: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { addproject })(
  withRouter(FaqAddModal)
);
