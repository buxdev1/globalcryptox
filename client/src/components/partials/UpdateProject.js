import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateproject } from "../../actions/faqActions";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import $ from "jquery";
import { Editor } from "@tinymce/tinymce-react";
import "react-toastify/dist/ReactToastify.css";
import Select from "react-select";


const options = [
  { value: "Week", label: "Week" },
  { value: "Day", label: "Day" },
];
class FaqUpdateModal extends React.Component {
  constructor(props) {
    super(props);
    $("#update-faq-modal").find(".text-danger").hide();
    this.state = {
      id: "",
      name: "",
      description: "",
      returnpercentage: "",
      returnterm: "",
      pdffile: "",
      pdf: null,
      type: {
        label: this.props.record.type,
        value: this.props.record.type,
      },
      mininvestment:"",
      maxinvestment:"",
      errors: {},
      errors1: {},

    };

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.record) {
      this.setState({
        id: nextProps.record._id,
        name: nextProps.record.name,
        description: nextProps.record.description,
        returnpercentage: nextProps.record.returnpercentagemonth,
        returnterm: nextProps.record.returnterm,
        type: {
          label:nextProps.record.Returntype,
          value: nextProps.record.Returntype,
        },
        mininvestment:nextProps.record.mininvestment,
        maxinvestment:nextProps.record.maxinvestment
        // pdffile: nextProps.record.pdffile,
      });
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }

    if (
      nextProps.auth !== undefined &&
      nextProps.auth.updatefaq !== undefined &&
      nextProps.auth.updatefaq.data !== undefined &&
      nextProps.auth.updatefaq.data.message !== undefined &&
      nextProps.auth.updatefaq.data.success
    ) {
      $("#update-faq-modal").modal("hide");
      toast(nextProps.auth.updatefaq.data.message, {
        position: toast.POSITION.TOP_CENTER,
      });
      nextProps.auth.updatefaq = "";
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  handleChange = (selectedOption) => {
    this.setState({ type: selectedOption });
    // this.setState({currencyType:selectedOption.value})
    //console.log(`Option selected:`, selectedOption );
  };


  onFaqUpdate = (e) => {
    e.preventDefault();
    $("#update-faq-modal").find(".text-danger").show();

    // const data = new FormData();
    // data.append("id",this.state.id)
    // data.append("name", this.state.name);
    // data.append("description", this.state.description);
    // data.append("returnpercentage", this.state.returnpercentage);
    // data.append("returnterm", this.state.returnterm);
    // if (this.state.pdf) {
    //   data.append("file", this.state.pdf);
    // }
    const data = {
      name: this.state.name,
      id: this.state.id,
      description: this.state.description,
      returnpercentage: this.state.returnpercentage,
      lockerduration: this.state.returnterm,
      currencyType: this.state.type.value,
      mininvestment:this.state.mininvestment,
      maxinvestment:this.state.maxinvestment
    };
    this.props.updateproject(data);
  };

  render() {
    const { errors ,errors1} = this.state;
    return (
      <div>
        <div className="modal fade" id="update-faq-modal">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Update Finance</h4>
                <button type="button" className="close" data-dismiss="modal">
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <form noValidate onSubmit={this.onFaqUpdate} id="update-faq">
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        name="name"
                        onChange={this.onChange}
                        value={this.state.name}
                        error={errors.name}
                        id="name"
                        type="text"
                        name="name"
                        className={classnames("form-control", {
                          invalid: errors.name,
                        })}
                      />
                      <span className="text-danger">{errors.name}</span>
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="description">Description</label>
                    </div>
                    <div className="col-md-9">
                      <textarea
                        onChange={this.onChange}
                        value={this.state.description}
                        error={errors.description}
                        id="description"
                        type="text"
                        className={classnames("form-control", {
                          invalid: errors.description,
                        })}
                      />
                      <span className="text-danger">{errors.description}</span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="returnpercentage">
                        Percentage
                      </label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.returnpercentage}
                        error={errors.returnpercentage}
                        id="returnpercentage"
                        type="number"
                        className={classnames("form-control", {
                          invalid: errors.returnpercentage,
                        })}
                      />
                      <span className="text-danger">
                        {errors.returnpercentage}
                      </span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencyName">Return Type</label>
                    </div>
                    <div className="col-md-9">
                      {this.state.type ? (
                        <Select
                          value={this.state.type}
                          defaultValue={{
                            label: this.state.type.label,
                            value: this.state.type.label,
                          }}
                          onChange={this.handleChange}
                          options={options}
                        />
                      ) : (
                        ""
                      )}
                      <span className="text-danger">{errors.type}</span>
                    </div>
                  </div>

                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="returnterm">
                        {" "}
                        Finance Duration
                      </label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.returnterm}
                        error={errors.returnterm}
                        id="returnterm"
                        type="number"
                        className={classnames("form-control", {
                          invalid: errors.returnterm,
                        })}
                      />
                      <span className="text-danger">{errors.returnterm}</span>
                    </div>
                  </div>


                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="returnterm">
                        {" "}
                        Minimum Investment Amount
                      </label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.mininvestment}
                        error={errors.mininvestment}
                        id="mininvestment"
                        type="number"
                        className={classnames("form-control", {
                          invalid: errors.mininvestment,
                        })}
                      />
                      <span className="text-danger">{errors.mininvestment}</span>
                    </div>
                  </div>


                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="returnterm">
                        {" "}
                        Max Investment Amount
                      </label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        value={this.state.maxinvestment}
                        error={errors.maxinvestment}
                        id="maxinvestment"
                        type="number"
                        className={classnames("form-control", {
                          invalid: errors.maxinvestment,
                        })}
                      />
                      <span className="text-danger">{errors.maxinvestment}</span>
                    </div>
                  </div>





                </form>
              </div>


              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  form="update-faq"
                  type="submit"
                  className="btn btn-primary"
                >
                  Update Finance
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FaqUpdateModal.propTypes = {
  updateproject: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { updateproject })(
  withRouter(FaqUpdateModal)
);
