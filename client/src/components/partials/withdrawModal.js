import React from 'react'
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateWithdrawPM } from "../../actions/templateActions";
import { withRouter } from "react-router-dom";
import { toast } from 'react-toastify';
import $ from 'jquery';
import { Editor } from '@tinymce/tinymce-react';
import 'react-toastify/dist/ReactToastify.css';
class TemplateUpdateModal extends React.Component {
    constructor(props) {
        super(props);
         $("#update-template-modal").find(".text-danger").hide();
       
        this.state = {
            id: this.props.record._id,
            accountnumber: this.props.record.accountnumber,
            amount: this.props.record.amount,
            name: this.props.record.name,
            email: this.props.record.email,
            test: this.props.record.status,
            batchexist: this.props.record.batch,
            errors: {},
            batch: "",
            status : 2,
        };
    }


    componentWillReceiveProps(nextProps) {
        console.log(nextProps,"sdfsdgfmdlgjkdhgjkdhgjkdfghjkdfghkj");
        if (nextProps.record) {
            this.setState({
                id: nextProps.record._id,
                accountnumber: nextProps.record.accountnumber,
                amount: nextProps.record.amount,
                name: nextProps.record.name,
                email: nextProps.record.email,
                test: nextProps.record.status,
                batchexist: nextProps.record.batch,
            })
        }
        if (nextProps.errors) {
             $("#update-template-modal").find(".text-danger").show();
            this.setState({
                errors: nextProps.errors
            });
        }
        if (nextProps.auth !== undefined
            && nextProps.auth.updatetemplate !== undefined
            && nextProps.auth.updatetemplate.data !== undefined
            && nextProps.auth.updatetemplate.data.Message !== undefined
            && nextProps.auth.updatetemplate.data.success) {
            $('#update-template-modal').modal('hide');
            toast(nextProps.auth.updatetemplate.data.Message, {
                position: toast.POSITION.TOP_CENTER
            });
            nextProps.auth.updatetemplate = "";
        }
    }

    onChange = e => {
         //$("#update-template-modal").find(".text-danger").show();
        if (e.target.id === 'batch') {
            this.setState({ batch: e.target.value });
        }
        if (e.target.id === 'status') {
            this.setState({ status: e.target.value });
        }
    };

    onTemplateUpdate = e => {
        e.preventDefault();
         $("#update-template-modal").find(".text-danger").show();
        const newTemplate = {
            id: this.props.record._id,
            batch: this.state.batch,
            status: this.state.status,
        };
        
        this.props.updateWithdrawPM(newTemplate);
    };

    render() {
        const { errors } = this.state;
        return (
            <div>
                <div className="modal fade" id="update-template-modal">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Update status</h4>
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                            <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="identifier">Email</label>
                                        </div>
                                        <div className="col-md-9">
                                           {this.state.email}
                                        </div>
                              </div>  
                              <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="identifier">Name</label>
                                        </div>
                                        <div className="col-md-9">
                                           {this.state.name}
                                        </div>
                              </div>  
                              <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="identifier">Account Number</label>
                                        </div>
                                        <div className="col-md-9">
                                           {this.state.accountnumber}
                                        </div>
                              </div>  
                              <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="identifier">Amount</label>
                                        </div>
                                        <div className="col-md-9">
                                           {this.state.amount}
                                        </div>
                              </div> 
                              {(this.state.test =="2" ) && 
                                        <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="identifier">Status</label>
                                        </div>
                                        <div className="col-md-9">
                                           Approved
                                        </div>
                              </div> 
                                    }
                                    {(this.state.test =="3" ) && 
                                       <div className="row mt-2">
                                       <div className="col-md-3">
                                           <label htmlFor="identifier">Status</label>
                                       </div>
                                       <div className="col-md-9">
                                          Rejected
                                       </div>
                             </div> 
                                    }

                                    {(this.state.batchexist) && 
                                       <div className="row mt-2">
                                            <div className="col-md-3">
                                                <label htmlFor="identifier">Batch Number</label>
                                            </div>
                                            <div className="col-md-9">
                                            {this.state.batchexist}
                                            </div>
                                       </div> 
                                    }
                              {(this.state.test =="1") &&   
                                <form noValidate onSubmit={this.onTemplateUpdate} id="update-template">
                                <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="batch">Batch Number / Reason</label>
                                        </div>
                                        <div className="col-md-9">
                                            <input
                                                onChange={this.onChange}
                                                error={errors.batch}
                                                id="batch"
                                                type="text"
                                                className={classnames("form-control", {
                                                    invalid: errors.batch
                                                })}
                                            />
                                            <span className="text-danger">{errors.batch}</span>
                                        </div>
                                    </div>   
                                    <div className="row mt-2">
                                        <div className="col-md-3">
                                            <label htmlFor="status">Status</label>
                                        </div>
                                        <div className="col-md-9">
                                        <select name="status" id="status" onChange={this.onChange}   className={classnames("form-control", {
                                                    invalid: errors.status
                                                })}>
                                            <option value="2">Completed</option>
                                            <option value="3">Rejected</option>
                                        </select>
                                            <span className="text-danger">{errors.status}</span>
                                        </div>
                                    </div>
                                </form>
                                }
                                
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                {(this.state.test =="1") && 
                                <button
                                    form="update-template"
                                    type="submit"
                                    className="btn btn-primary">
                                    Update status
                                </button>
                                    }  
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

        )
    }
}

TemplateUpdateModal.propTypes = {
    updateWithdrawPM: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { updateWithdrawPM }
)(withRouter(TemplateUpdateModal));
