const express                     = require('express');
const router                      = express.Router();
const bcrypt                      = require('bcryptjs');
const jwt                         = require('jsonwebtoken');
const keys                        = require('../../config/keys');
const async                       = require("async");
const validateTradeInput          = require('../../validation/frontend/trade');
const validatemobRegisterInput    = require('../../validation/frontend/mobregister');
const validateLoginInput          = require('../../validation/login');
const validatemobLoginInput       = require('../../validation/moblogin');
const validateUpdateUserInput     = require('../../validation/frontend/updateUser');
const validateEmailtemplateInput  = require('../../validation/emailtemplate');
const validateForgotInput         = require('../../validation/forgot');
const validateCmsInput            = require('../../validation/cms');
const validateFaqInput            = require('../../validation/faq');
const validateUpdateSettingsInput = require('../../validation/settings');
const validateResetInput          = require('../../validation/frontend/resetpassword');
const validatetfaInput            = require('../../validation/frontend/tfainput');
const validateContactInput        = require('../../validation/frontend/contact_us');
const tradeTable                  = require('../../models/tradeTable');
const Assets                      = require('../../models/Assets');
const position_table              = require('../../models/position_table');
const currency                    = require('../../models/currency');
const User                        = require('../../models/User');
const Emailtemplates              = require('../../models/emailtemplate');
const exchangePrices              = require('../../models/exchangePrices');
const nodemailer                  = require('nodemailer');
const multer                      = require('multer');
var node2fa                       = require('node-2fa');
var CryptoJS                      = require("crypto-js");
var moment                      = require("moment");
const perpetual                   = require('../../models/perpetual');
const cryptoRandomString          = require('crypto-random-string');
const client                      = require('twilio')(
  keys.TWILIO_ACCOUT_SID,
  keys.TWILIO_AUTH_TOKEN
);
const mongoose                    = require('mongoose');
const url                    = require('url');
const ObjectId                    = mongoose.Types.ObjectId;
var symbolsDatabase  = require("../symbols_database"),
RequestProcessor     = require("../request-processor").RequestProcessor;
var requestProcessor = new RequestProcessor(symbolsDatabase);

var cron = require('node-cron');

var request = require('request');


const rp = require('request-promise')


let user_list  = [{"Id":1 },{"Id":2 },{"Id":3 },{"Id":4 },{"Id":5 }];
let URL = "https://jsonplaceholder.typicode.com/users/"

const WebSocket = require('ws');

//Kraken API section
const ws = new WebSocket('wss://ws.kraken.com/');
ws.on('message', function incoming(data) {
  // console.log(JSON.parse(data),'hsdkfhksdhfkshdfkhsdf');
  // console.log(JSON.parse(data)[3],'hsdkfhksdhfkshdfkhsdf');
  var low   = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].l:'';
  var last  = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].a:'';
  var high  = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].h:'';
  var open  = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].o:'';
  var close = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].c:'';
  var volume = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].v:'';
  var pairname = typeof(JSON.parse(data)[3])!='undefined'?JSON.parse(data)[3].replace("/",""):'';
  if(typeof(JSON.parse(data)[3])!='undefined')
  {
  	var updatedata = {
      "low"          : (low)?low[0]:0,
      "high"         : (high)?high[0]:0,
      "exchangename" : "Kraken",
      "last"         : (last)?last[0]:0,
      "pairname"     : (pairname=='XBTUSD')?'BTCUSD':pairname
    };

    var pairname = (pairname=='XBTUSD')?'BTCUSD':pairname;

    updatebaldata = {"volume":(volume)?volume[0]:0,}

	exchangePrices.findOneAndUpdate({exchangename:"Kraken","pairname":pairname},{"$set": updatedata ,"$inc": updatebaldata } , {new:true,"fields": {exchangename:1} } ,function(balerr,baldata){
		console.log(balerr);
		console.log(baldata,'baldata');
	});
  }
});

ws.on('open', function open() {
  ws.send(JSON.stringify({
  "event": "subscribe",
  "pair": ["XBT/USD", "ETH/USD", "XRP/USD", "BCH/USD", "LTC/USD"],
  "subscription": {"name": "ticker"}
}))
});

//Coinbase pro API section
const ws1 = new WebSocket("wss://ws-feed.pro.coinbase.com");

ws1.onopen = function () {
  ws1.send(JSON.stringify({
    "type": "subscribe",
    "product_ids": [
        "BTC-USD",
        "ETH-USD",
        "LTC-USD",
        "XRP-USD",
        "BCH-USD",
    ],
    "channels": [
        "level2",
        "heartbeat",
        {
            "name": "ticker",
            "product_ids": [
                "BTC-USD",
                "ETH-USD",
                "LTC-USD",
                "XRP-USD",
                "BCH-USD",
            ]
        }
    ]
}));
};

ws1.on('message', function incoming(data) {
  // console.log(JSON.parse(data),'gemini');
  var result = JSON.parse(data);
  if(typeof result.type!='undefined' && result.type=='l2update')
  {
		var product_id = typeof result.product_id != 'undefined'?result.product_id:'';
		var changes = typeof result.changes != 'undefined'?result.changes[0]:'';
		var updatedata = {"exchangename" : "Coinbasepro","last" : (changes[1])?changes[1]:0,"pairname" : product_id?product_id.replace("-",""):''};

		updatebaldata = {"volume":(changes[2])?changes[2]:0}

		exchangePrices.findOneAndUpdate({exchangename:"Coinbasepro","pairname":product_id?product_id.replace("-",""):''},{"$set": updatedata ,"$inc": updatebaldata } , {new:true,"fields": {exchangename:1} } ,function(balerr,baldata){
		});
  }

});

//Bitstamp API section
const ws2 = new WebSocket("wss://ws.bitstamp.net");

var subscribeMsg = {
        "event": "bts:subscribe",
        "data": {
            "channel": "live_trades_btcusd"
        }
    };

var subscribeMsg1 = {
        "event": "bts:subscribe",
        "data": {
            "channel": "live_trades_ethusd"
        }
    };
var subscribeMsg2 = {
        "event": "bts:subscribe",
        "data": {
            "channel": "live_trades_xrpusd"
        }
    };
var subscribeMsg3 = {
        "event": "bts:subscribe",
        "data": {
            "channel": "live_trades_bchusd"
        }
    };
    var subscribeMsg4 = {
        "event": "bts:subscribe",
        "data": {
            "channel": "live_trades_ltcusd"
        }
    };

ws2.onopen = function () {
  ws2.send(JSON.stringify(subscribeMsg));
  ws2.send(JSON.stringify(subscribeMsg1));
  ws2.send(JSON.stringify(subscribeMsg2));
  ws2.send(JSON.stringify(subscribeMsg3));
  ws2.send(JSON.stringify(subscribeMsg4));
};

ws2.on('message', function incoming(data) {
// console.log(JSON.parse(data),'dddddddddd');
  var result = JSON.parse(data).data;
  if(typeof result != 'undefined' && typeof result.amount_str != 'undefined')
  {
      var channel = typeof JSON.parse(data).channel!= 'undefined'?JSON.parse(data).channel:'';
      var splitar = channel.split('_');
      
      var updatedata = {
      "exchangename" : "Bitstamp",
      "last"         : (result.price)?result.price:0,
      "pairname"     : typeof splitar[2]!='undefined'?splitar[2].toUpperCase():''
      };

      updatebaldata = {"volume":(result.amount_str)?result.amount_str:0}

      exchangePrices.findOneAndUpdate({exchangename:"Bitstamp","pairname":typeof splitar[2]!='undefined'?splitar[2].toUpperCase():''},{"$set": updatedata ,"$inc": updatebaldata } , {new:true,"fields": {exchangename:1} } ,function(balerr,baldata){
		});
  }
});




function make_api_call(id,exchange){
  if(exchange=='Bitstamp')
  {
    console.log('bit here');
    return rp({
          url : 'https://www.bitstamp.net/api/v2/ticker/'+id.toLowerCase(),
          method : 'GET',
          json : true
      })
  }

  if(exchange=='Kraken')
  {
    var id = (id=='BTCUSD')?'XBTUSD':(id=='ETHUSD')?'ETHUSD':(id=='BCHUSD')?'BCHUSD':'';
    console.log(id,'pairdetails');
    return rp({
          url : 'https://api.kraken.com/0/public/Ticker?pair='+id,
          method : 'GET',
          json : true
      })
  }

  if(exchange=='Coinbasepro')
  {
    var id = (id=='BTCUSD')?'BTC-USD':(id=='ETHUSD')?'ETH-USD':'';
    console.log(id,'pairdetails');
    return rp({
          url : 'https://api.pro.coinbase.com/products/'+id+'/ticker',
          method : 'GET',
          json : true
      })
  }

  if(exchange=='Gemini')
  {
    return rp({
          url : 'https://api.gemini.com/v1/pubticker/'+id.toLowerCase(),
          method : 'GET',
          json : true
      })
  }

}

async function processUsers(contractdetails,exchange){
  console.log(exchange);
  console.log(contractdetails.length,'length of contract');
    if(contractdetails.length>0)
    {
      for(var i=0;i<contractdetails.length;i++)
      {
        if(contractdetails[i].tiker_root=='ETHUSD' || contractdetails[i].tiker_root=='BTCUSD' || contractdetails[i].tiker_root=='BCHUSD')
        {
          result = await make_api_call(contractdetails[i].tiker_root,exchange);
          if(exchange=='Bitstamp')
          {
              const newrecord = new exchangePrices({
                  "low"          : (result.low)?result.low:0,
                  "high"         : (result.high)?result.high:0,
                  "exchangename" : exchange,
                  "last"         : (result.last)?result.last:0,
                  "volume"       : (result.volume)?result.volume:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                  console.log("success1");
                  });
          }

          if(exchange=='Kraken')
          {
            
            var result = (contractdetails[i].tiker_root=='BTCUSD')?result.result.BTCUSD:(contractdetails[i].tiker_root=='ETHUSD')?result.result.XETHZUSD:(contractdetails[i].tiker_root=='BCHUSD')?result.result.XXBTZUSD:[];
              const newrecord = new exchangePrices({
                  "low"          : (result.l[0])?result.l[0]:0,
                  "high"         : (result.h[0])?result.h[0]:0,
                  "exchangename" : exchange,
                  "last"         : (result.p[0])?result.p[0]:0,
                  "volume"       : (result.v[0])?result.v[0]:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                  console.log("success1");
                  });
          }

           if(exchange=='Coinbasepro')
          {
              const newrecord = new exchangePrices({
                  "low"          : (result.ask)?result.ask:0,
                  "high"         : (result.bid)?result.bid:0,
                  "exchangename" : exchange,
                  "last"         : (result.price)?result.price:0,
                  "volume"       : (result.volume)?result.volume:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                  console.log("success1");
                  });
          }

           if(exchange=='Gemini')
          {
              const newrecord = new exchangePrices({
                  "low"          : (result.ask)?result.ask:0,
                  "high"         : (result.bid)?result.bid:0,
                  "exchangename" : exchange,
                  "last"         : (result.last)?result.last:0,
                  "volume"       : (result.volume)?result.volume[contractdetails[i].first_currency]:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                    console.log("success1");
                  });
          }

        }
    }
  }

}



router.get('/balance', (req, res) => {
  // res.json({statue:"success"});


   User.find({}, function(err, userdetails) {
      if (userdetails) {
        userdetails.forEach(function(res) {
          var userId = res._id;
          currency.find({}, function(err, currencydetails) {
            currencydetails.forEach(function(cur){
              var insertobj = {
                "balance":0,
                "currency":cur._id,
                "currencySymbol":cur.currencySymbol
              };

              const newContact = new Assets({
               "balance":0,
                "currency":cur._id,
                "currencySymbol":cur.currencySymbol,
                "userId":userId
            });
           newContact.save(function(err,data) {
            console.log("success");
           });

            });
          });
        });
        res.send('success');

      }
    })
});
function gettradedata(firstCurrency,secondCurrency,io)
{
    var findObj = {
        firstCurrency:firstCurrency,
        secondCurrency:secondCurrency
    };
    var pair = firstCurrency + secondCurrency;
    var result = {};
    // tradeTable.find(findObj,function(err,tradeTableAll){
    async.parallel({
      buyOrder : function(cb) {
        var sort = {'price':-1};
        tradeTable.aggregate([
        {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:firstCurrency,secondCurrency:secondCurrency,buyorsell:'buy'}},
        {
          $group : {
            '_id' : '$price',
            'quantity' : { $sum : '$quantity' },
            'filledAmount' : { $sum : '$filledAmount' }
          }
        },
        {$sort:sort},
        {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      sellOrder : function(cb) {
        var sort = {'price':1};
        tradeTable.aggregate([
        {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:firstCurrency,secondCurrency:secondCurrency,buyorsell:'sell'}},
        {
          $group : {
            '_id' : '$price',
            'quantity' : { $sum : '$quantity' },
            'filledAmount' : { $sum : '$filledAmount' }
          }
        },
        {$sort:sort},
        {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      contractdetails : function(cb) {
         perpetual.findOne({first_currency:firstCurrency,second_currency:secondCurrency},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1}).exec(cb)
      },
      Rescentorder : function(cb) {
      tradeTable.aggregate([
      {$match:{'pairName': pair,'status':'1'}},
      {$unwind:"$filled"},
      {$project:{"filled":1}},
      {$group:{_id:{"buyuserId":'$filled.buyuserId',"selluserId":'$filled.selluserId',"sellId":"$filled.sellId","buyId":"$filled.buyId"},"created_at":{ $first:"$filled.created_at" },"Type":{$first:"$filled.Type"},"filledAmount":{$first:"$filled.filledAmount"},"pairname":{$first:"$filled.pairname"},"Price":{$first:"$filled.Price"}}},
     {$limit: 50},
     ]).exec(cb)
   },
    },(err,results) => {
        if(err){
            result.status      = false;
            result.message     = 'Error occured.';
            result.err         = err;
            result.notify_show = 'no';
            // res.json(result);
        } else if(results){
           var sellOrder = results.sellOrder;
           var buyOrder  = results.buyOrder;

            if(buyOrder.length>0)
            {
              var sumamount = 0
              for(i=0;i<buyOrder.length;i++)
              {
                  var quantity      = buyOrder[i].quantity;
                  var _id           = buyOrder[i]._id;
                  sumamount         = parseFloat(sumamount) + parseFloat(quantity);
                  buyOrder[i].total = sumamount;
              }
            }

            if(sellOrder.length>0)
            {
              var sumamount = 0
              for(i=0;i<sellOrder.length;i++)
              {
                  var quantity       = sellOrder[i].quantity;
                  var _id            = sellOrder[i]._id;
                  sumamount          = parseFloat(sumamount) + parseFloat(quantity);
                  sellOrder[i].total = sumamount;
              }
            }

            sellOrder = sellOrder.reverse();

            result.status       = true;
            result.message      = 'tradeTableAll';
            result.buyOrder     = results.buyOrder;
            result.sellOrder    = results.sellOrder;
            result.contractdetails    = results.contractdetails;
            result.notify_show  = 'no';
            result.Rescentorder = results.Rescentorder;
             // res.json(result);
             console.log(result);
             if(typeof io != 'undefined')
             {
                io.emit('TRADE', result);
             }
        } else {
            result.status = false;
            result.message = 'Error occured.';
            result.err = '';
            result.notify_show = 'no';
            // res.json(result);
        }
    });
}
router.post('/getTradeData', (req, res) => {
    var findObj = {
        firstCurrency:req.body.firstCurrency,
        secondCurrency:req.body.secondCurrency
    };
    var pair = req.body.firstCurrency + req.body.secondCurrency;
    var result = {};
    // tradeTable.find(findObj,function(err,tradeTableAll){
    async.parallel({
      buyOrder : function(cb) {
        var sort = {'price':1};
        tradeTable.aggregate([
        {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:req.body.firstCurrency,secondCurrency:req.body.secondCurrency,buyorsell:'buy'}},
        {
          $group : {
            '_id' : '$price',
            'quantity' : { $sum : '$quantity' },
            'filledAmount' : { $sum : '$filledAmount' }
          }
        },
        {$sort:sort},
        {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      sellOrder : function(cb) {
        var sort = {'price':1};
        tradeTable.aggregate([
        {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:req.body.firstCurrency,secondCurrency:req.body.secondCurrency,buyorsell:'sell'}},
        {
          $group : {
            '_id' : '$price',
            'quantity' : { $sum : '$quantity' },
            'filledAmount' : { $sum : '$filledAmount' }
          }
        },
        {$sort:sort},
        {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },

      Assetdetails : function(cb) {
         Assets.find({userId:ObjectId(req.body.userid)}).exec(cb)
      },
      contractdetails : function(cb) {
         perpetual.findOne({first_currency:req.body.firstCurrency,second_currency:req.body.secondCurrency},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1}).exec(cb)
      },
      Rescentorder : function(cb) {
      tradeTable.aggregate([
      {$match:{'pairName': pair,'status':'1'}},
      {$unwind:"$filled"},
      {$project:{"filled":1}},
      {$group:{_id:{"buyuserId":'$filled.buyuserId',"selluserId":'$filled.selluserId',"sellId":"$filled.sellId","buyId":"$filled.buyId"},"created_at":{ $first:"$filled.created_at" },"Type":{$first:"$filled.Type"},"filledAmount":{$first:"$filled.filledAmount"},"pairname":{$first:"$filled.pairname"},"Price":{$first:"$filled.Price"}}},
     {$limit: 50},
     ]).exec(cb)
   },
    },(err,results) => {
        if(err){
            result.status      = false;
            result.message     = 'Error occured.';
            result.err         = err;
            result.notify_show = 'no';
            res.json(result);
        } else if(results){
            var sellOrder = results.sellOrder;
            var buyOrder   = results.buyOrder;

            if(buyOrder.length>0)
            {
              var sumamount = 0
              for(i=0;i<buyOrder.length;i++)
              {
                  var quantity = buyOrder[i].quantity;
                  var _id = buyOrder[i]._id;
                  sumamount = parseFloat(sumamount) + parseFloat(quantity);
                  buyOrder[i].total = sumamount;
              }
            }

            if(sellOrder.length>0)
            {
              var sumamount = 0
              for(i=0;i<sellOrder.length;i++)
              {
                  var quantity = sellOrder[i].quantity;
                  var _id = sellOrder[i]._id;
                  sumamount = parseFloat(sumamount) + parseFloat(quantity);
                  sellOrder[i].total = sumamount;
              }
            }

            sellOrder = sellOrder.reverse();

            result.status          = true;
            result.message         = 'tradeTableAll';
            result.buyOrder        = results.buyOrder;
            result.sellOrder       = results.sellOrder;
            result.contractdetails = results.contractdetails;
            result.notify_show     = 'no';
            result.assetdetails    = results.Assetdetails;
            result.Rescentorder    = results.Rescentorder;
             res.json(result);
        } else {
            result.status = false;
            result.message = 'Error occured.';
            result.err = '';
            result.notify_show = 'no';
            res.json(result);
        }
    });
});
function cancel_trade(tradeid,userid)
{
  update      = { status : '3'}
  tradeTable.aggregate([
    {$match:{'_id' : ObjectId(tradeid),'status':{ $ne: '3' }}},
  ]).exec((tradeerr,tradedata) => {
    if(tradedata.length > 0) {
      var type                =  tradedata[0].buyorsell;
      var trade_ids           =  tradedata[0]._id;
      var userId              =  tradedata[0].userId;
      var filledAmt           =  tradedata[0].filledAmount;
      var status              =  tradedata[0].status;
      var quantity            =  tradedata[0].quantity;
      var price               =  tradedata[0].price;
      var t_firstcurrencyId   =  tradedata[0].firstCurrency;
      var t_secondcurrencyId  =  tradedata[0].secondCurrency;
      var leverage            =  tradedata[0].leverage;

      quantity = parseFloat(quantity) - parseFloat(filledAmt);

      var leverage1       = parseFloat(100/leverage);

      var initial_margin = ((parseFloat(quantity)/parseFloat(price))*leverage1)/100;
      var feetoopen      = (quantity/price)*0.075/100;
      var Bankruptcy     = price * leverage /(parseFloat(leverage) + 1);
      var feetoclose     = (quantity/Bankruptcy) * 0.075/100;
      var order_value    = parseFloat(quantity/price).toFixed(8);
      var order_cost     = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);



      async.parallel({
      // update balance
      data1: function(cb){
      var updatebaldata = {};
        updatebaldata["balance"] = order_cost;
        console.log(order_cost,'order cost');
        Assets.findOneAndUpdate({currencySymbol:t_firstcurrencyId,userId:ObjectId(userId)},{"$inc": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){

        });
      },
      data2: function(cb){

        var updatedata = {"status": '3' }
        tradeTable.findOneAndUpdate({_id: ObjectId(tradeid)},{"$set":updatedata},{new:true,"fields": {balance:1} },function(upErr,upRes){
            if(upRes) {
              //res.json({status:true,message:"Your Order cancelled successfully.",notify_show:'yes'});
              // getrateandorders(pair,userid);
              // open_ordersdata(pair,userid);
            }
            else {
              //res.json({status:false,message:"Due to some error occurred,While Order cancelling"});
            }
        });
      }
      },function(err, results){

      });
    }
    else  {
      console.log({status:false,message:"Your Order already cancelled"});
    }
  });
}
router.post('/cancelTrade', (req, res) => {

  var tradeid = req.body.id;
  var userid  = req.body.userid;
  update      = { status : '3'}
  tradeTable.aggregate([
    {$match:{'_id' : ObjectId(tradeid),'status':{ $ne: '3' }}},
  ]).exec((tradeerr,tradedata) => {
    if(tradedata.length > 0) {
      var type               = tradedata[0].buyorsell;
      var trade_ids          = tradedata[0]._id;
      var userId             = tradedata[0].userId;
      var filledAmt          = tradedata[0].filledAmount;
      var status             = tradedata[0].status;
      var quantity           = tradedata[0].quantity;
      var price              = tradedata[0].price;
      var t_firstcurrencyId  = tradedata[0].firstCurrency;
      var t_secondcurrencyId = tradedata[0].secondCurrency;
      var beforeBalance      = tradedata[0].beforeBalance;
      var afterBalance       = tradedata[0].afterBalance;
      var beforebonusBalance = tradedata[0].beforebonusBalance;
      var afterbonusBalance  = tradedata[0].afterbonusBalance;
      var leverage           = tradedata[0].leverage;

      quantity = parseFloat(quantity) - parseFloat(filledAmt);

      var leverage1       = parseFloat(100/leverage);

      var initial_margin = ((parseFloat(quantity)/parseFloat(price))*leverage1)/100;
      var feetoopen      = (quantity/price)*0.075/100;
      var Bankruptcy     = price * leverage /(parseFloat(leverage) + 1);
      var feetoclose     = (quantity/Bankruptcy) * 0.075/100;
      var order_value    = parseFloat(quantity/price).toFixed(8);
      var order_cost     = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);

      async.parallel({
      // update balance
      data1: function(cb){
          var updatebaldata = {};
          if(t_firstcurrencyId=='BTC')
          {
              if(beforeBalance==0)
              {
                  updatebaldata["tempcurrency"] = order_cost;
              }
              else if(beforebonusBalance==0)
              {
                  updatebaldata["balance"] = order_cost;
              }
              else
              {
                if(filledAmt==0)
                {
                    var newbonbalance = parseFloat(beforebonusBalance) - parseFloat(afterbonusBalance);
                    updatebaldata["tempcurrency"] = newbonbalance;
                    if(beforeBalance!=0 && afterBalance!=0)
                    {
                      var newbonbalance = parseFloat(beforeBalance) - parseFloat(afterBalance);
                        updatebaldata["balance"] = newbonbalance;
                    }
                }
                else
                {
                    var newbal = parseFloat(beforeBalance) - parseFloat(afterBalance);
                    if(parseFloat(order_cost)>=parseFloat(newbal))
                    {
                        var bonusbalance = parseFloat(order_cost)-parseFloat(newbal)
                        updatebaldata["balance"] = newbal;
                        updatebaldata["tempcurrency"] = bonusbalance;
                    }
                    else
                    {
                        var bonusbalance = parseFloat(order_cost)-parseFloat(newbal)
                        updatebaldata["balance"] = order_cost;
                        updatebaldata["tempcurrency"] = 0;
                    }
                }
              }
            }
            else
            {
              updatebaldata["balance"] = order_cost;
            }
          console.log(order_cost,'order cost');
          Assets.findOneAndUpdate({currencySymbol:t_firstcurrencyId,userId:ObjectId(userId)},{"$inc": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){

          });
      },
      data2: function(cb){
        var updatedata = {"status": '3' }
        tradeTable.findOneAndUpdate({_id: ObjectId(tradeid)},{"$set":updatedata},{new:true,"fields": {balance:1} },function(upErr,upRes){
            if(upRes) {
              res.json({status:true,message:"Your Order cancelled successfully.",notify_show:'yes'});
              // getrateandorders(pair,userid);
              // open_ordersdata(pair,userid);
            }
            else {
              res.json({status:false,message:"Due to some error occurred,While Order cancelling"});
            }
        });
      }
      },function(err, results){

      });
    }
    else  {
      res.json({status:false,message:"Your Order already cancelled"});
    }
  });

});

router.post('/getPricevalue', (req, res) => {
  var result = {};
  async.parallel({
      lastpricedet : function(cb) {
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
          {
            $match:
            {
              status:'1',
              firstCurrency:req.body.firstCurrency,
              secondCurrency:req.body.secondCurrency,
              //"createdAt": { $gt: new Date(Date.now() - 24*60*60 * 1000) }
            }
          },
          {$sort:sort},
          {$limit: 1},
        ]).allowDiskUse(true).exec(cb)
      },
      pricedet : function(cb) {
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
           {
            $match:
            {
              status:'1',
            }
          },
          {$unwind:"$filled"},
          {
            $match:
            {
              'filled.pairname': req.body.firstCurrency+req.body.secondCurrency
            }
          },
          {
            $group:
            {
              _id: "$item",
              low: { $min: "$filled.Price" },
              high: { $max: "$filled.Price" },
              volume :{ $sum: "$filled.order_cost"}
            }
          }

        ]).allowDiskUse(true).exec(cb)
      },
      rates : function(cb) {
          tradeTable.aggregate([
              {$unwind:"$filled"},
              {
                  $match : {
                    "filled.created_at": {
                        $gte: new Date(new Date().setDate(new Date().getDate()-1)),
                        $lte: new Date()
                    },
                    'filled.pairname': req.body.firstCurrency+req.body.secondCurrency
                  }
              },
              {
                $sort : { 'filled.created_at' : 1 }
              },
              {
                  $project : {
                      "filled.filledAmount" : 2,
                      "filled.Price" : 1,
                  }
              },
              {
                  $group : {
                      _id    : null,
                      first  : { $first : '$filled.Price'},
                      last   : { $last : '$filled.Price'},
                      high   : { $max : '$filled.Price' },
                      low    : { $min : '$filled.Price' },
                      volume : { $sum : '$filled.filledAmount' }
                  }
              }
          ]).allowDiskUse(true).exec(cb)
      },
      change : function(cb) {
          tradeTable.aggregate([
              {$unwind:"$filled"},
              {$match : { "filled.created_at": {
                  $gte: new Date(new Date().setDate(new Date().getDate()-1)),
                  $lte: new Date()
              },'filled.pairname': req.body.firstCurrency+req.body.secondCurrency}},
              {
                  $sort: {
                  "filled.created_at": 1,
                  }
              },
              {
                  $group: {
                      _id: null,
                      pairname: {$first:"$filled.pair"},
                      lastRate: { $last: '$filled.Price'},
                      oldRate: { $first: '$filled.Price'},
                  }
              },
              {
                  $project: {
                      _id: 1,
                      pairname: 1,
                      oldRate : {$cond: [ { $eq: [ "$oldRate", null ] }, 0, '$oldRate' ]},
                      lastRate : {$cond: [ { $eq: [ "$lastRate", null ] }, 0, '$lastRate' ]},
                      change  : { $multiply: [{ $subtract: [ 1, { $divide: [ {$cond: [ { $eq: [ "$oldRate", null ] }, 0, '$oldRate' ]}, {$cond: [ { $eq: [ "$lastRate", null ] }, 0, '$lastRate' ]} ] } ] }, 100]}
                  }
              },
          ]).allowDiskUse(true).exec(cb)
      },
    },(err,results) => {

        if(err){
            result.status       = false;
            result.message      = 'Error occured.';
            result.err          = err;
            result.notify_show  = 'no';
            res.json(result);
        } else if(results){
            if(results.rates.length>0)
            {
                var low          = results.rates[0].low;
                var high         = results.rates[0].high;
                var last         = results.rates[0].last;
                var volume       = results.pricedet[0].volume;
                var total_volume = results.rates[0].volume;
            }
            var change = (results.change.length>0)?results.change[0].change:0;
            perpetual.findOneAndUpdate({ "tiker_root":req.body.firstCurrency+req.body.secondCurrency },{ "$set": {"low": low,"high": high,"last": last,"volume": volume,"total_volume": total_volume,"change":change}},{multi: true}).exec(function(err, resUpdate){
                if(resUpdate)
                {
                  console.log(resUpdate);
                }
            });

            result.status       = true;
            result.message      = 'tradeTableAll';
            result.pricedet     = results.pricedet;
            result.lastpricedet = results.lastpricedet;
            result.change       = results.change;
            result.notify_show  = 'no';
            res.json(result);
        } else {
            result.status       = false;
            result.message      = 'Error occured.';
            result.err          = '';
            result.notify_show  = 'no';
            res.json(result);
        }
    });
});

function getusertradedata(userId,firstCurrency,secondCurrency)
{
    var result = {};
    async.parallel({
      orderHistory : function(cb) {
        var sort = {'_id':-1};
        tradeTable.aggregate([
          {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:firstCurrency,secondCurrency:secondCurrency,userId:ObjectId(userId)}},
          {$sort:sort},
          {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      Histroydetails : function(cb) {
        tradeTable.find({userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      Filleddetails : function(cb) {
        tradeTable.find({status:1,userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      Conditional_details : function(cb) {
        tradeTable.find({status:'4',stopstatus:{$ne:'1'},userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      position_details : function(cb) {
        var pair = firstCurrency+secondCurrency;
          tradeTable.aggregate([
          { "$match": { status:'1',userId:ObjectId(userId),"pairName": pair } },
          {$unwind:"$filled"},
          { "$match": { "filled.position_status":'1'} },
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
          ]).exec(cb)
      },
      daily_details : function(cb) {
        var pair = firstCurrency+secondCurrency;
        var start = new Date();
        start.setHours(0,0,0,0);
        // console.log(start,'start');
        var end = new Date();
        end.setHours(23,59,59,999);
        // console.log(end,'start');
        tradeTable.aggregate([
          { "$match": { status:'1',position_status:'1',userId:ObjectId(userId),"pairName": pair } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$match": {"filled.created_at": {$gte: new Date(start), $lt: new Date(end)}} },
          { "$group": { "_id": null,"price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"Fees" :{ "$sum": "$filled.Fees" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
          ]).exec(cb)
      },
      lastpricedet : function(cb) {
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
          {
            $match:
            {
              status:'1',
              firstCurrency:firstCurrency,
              secondCurrency:secondCurrency
            }
          },
          {$sort:sort},
          {$limit: 1},
        ]).allowDiskUse(true).exec(cb)
      },
      Assetdetails : function(cb) {
         Assets.find({userId:ObjectId(userId)}).exec(cb)
      },
      contractdetails : function(cb) {
         perpetual.findOne({first_currency:firstCurrency,second_currency:secondCurrency},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1}).exec(cb)
      },
      closed_positions : function(cb) {
         position_table.find({userId:ObjectId(userId),pairname:firstCurrency+secondCurrency}).exec(cb)
      },
    },(err,results) => {
      // console.log(results.position_details,'position_details');
        if(err){
            result.status      = false;
            result.message     = 'Error occured.';
            result.err         = err;
            result.notify_show = 'no';
            // res.json(result);
        } else if(results){
            result.status               = true;
            result.message              = 'tradeTableAll';
            result.buyOrder             = results.buyOrder;
            result.sellOrder            = results.sellOrder;
            result.orderHistory         = results.orderHistory;
            result.Histroydetails       = results.Histroydetails;
            result.Conditional_details  = results.Conditional_details;
            result.Filleddetails        = results.Filleddetails;
            result.lastpricedet         = results.lastpricedet;
            result.assetdetails         = results.Assetdetails;
            result.contractdetails      = results.contractdetails;
            result.position_details     = results.position_details;
            result.closed_positions     = results.closed_positions;
            result.daily_details        = results.daily_details;
            result.notify_show          = 'no';
            if(typeof io != 'undefined')
            {
                io.sockets.in(userId).emit('USERTRADE',result);
            }
        } else {
            result.status = false;
            result.message = 'Error occured.';
            result.err = '';
            result.notify_show = 'no';
            // res.json(result);
        }
    });

}
router.post('/changeopenpositions', (req, res) => {
  console.log('call here');
    tradeTable.findOneAndUpdate({'filled.user_id':ObjectId(req.body.user_id),'filled.position_status':1},{ "$set": {"leverage":req.body.leverage}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
        if(selltempData)
        {
            res.json({"status":true,"message":"Position updated successfully","notify_show":"yes"});
        }
        else
        {
            res.json({"status":true,"message":"Position updated successfully","notify_show":"yes"});
        }
    });
});

router.post('/getuserTradeData', (req, res) => {
   var userId = req.body.userid;
   var status = req.body.status;
    var result = {};
    // tradeTable.find(findObj,function(err,tradeTableAll){
    async.parallel({
      orderHistory : function(cb) {
        var sort = {'_id':-1};
        tradeTable.aggregate([
          {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:req.body.firstCurrency,secondCurrency:req.body.secondCurrency,userId:ObjectId(userId)}},
          {$sort:sort},
          {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      Histroydetails : function(cb) {
        tradeTable.find({userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      Filleddetails : function(cb) {
        tradeTable.find({status:1,userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      Conditional_details : function(cb) {
        tradeTable.find({status:'4',stopstatus:{$ne:'1'},userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      position_details : function(cb) {
        var pair = req.body.firstCurrency+req.body.secondCurrency;
          tradeTable.aggregate([
          { "$match": { status:'1',userId:ObjectId(userId),"pairName": pair } },
          {$unwind:"$filled"},
          { "$match": { "filled.position_status":'1'} },
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
          ]).exec(cb)
      },
      daily_details : function(cb) {
        var pair = req.body.firstCurrency+req.body.secondCurrency;
        var start = new Date();
        start.setHours(0,0,0,0);
        // console.log(start,'start');
        var end = new Date();
        end.setHours(23,59,59,999);
        // console.log(end,'start');
        tradeTable.aggregate([
          { "$match": { status:'1',position_status:'1',userId:ObjectId(userId),"pairName": pair } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$match": {"filled.created_at": {$gte: new Date(start), $lt: new Date(end)}} },
          { "$group": { "_id": null,"price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"Fees" :{ "$sum": "$filled.Fees" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
          ]).exec(cb)
      },
      lastpricedet : function(cb) {
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
          {
            $match:
            {
              status:'1',
              firstCurrency:req.body.firstCurrency,
              secondCurrency:req.body.secondCurrency
            }
          },
          {$sort:sort},
          {$limit: 1},
        ]).allowDiskUse(true).exec(cb)
      },
      Assetdetails : function(cb) {
         Assets.find({userId:ObjectId(req.body.userid)}).exec(cb)
      },
      contractdetails : function(cb) {
         perpetual.findOne({first_currency:req.body.firstCurrency,second_currency:req.body.secondCurrency},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1}).exec(cb)
      },
      closed_positions : function(cb) {
         position_table.find({userId:ObjectId(req.body.userid),pairname:req.body.firstCurrency+req.body.secondCurrency}).exec(cb)
      },
      allposition_details : function(cb) {
         tradeTable.aggregate([
          { "$match": { status:'1',userId:ObjectId(req.body.userid) } },
          {$unwind:"$filled"},
          { "$match": { "filled.position_status":'1'} },
          {$project:{"filled":1,leverage:1,firstCurrency:1}},
          { "$group": { "_id": "$pairName","price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } ,"firstCurrency" :{ "$first": "$firstCurrency" } } }
          ]).exec(cb)
      },
    },(err,results) => {
      // console.log(results.position_details,'position_details');
        if(err){
            result.status      = false;
            result.message     = 'Error occured.';
            result.err         = err;
            result.notify_show = 'no';
            res.json(result);
        } else if(results){
            result.status               = true;
            result.message              = 'tradeTableAll';
            result.buyOrder             = results.buyOrder;
            result.sellOrder            = results.sellOrder;
            result.orderHistory         = results.orderHistory;
            result.Histroydetails       = results.Histroydetails;
            result.Conditional_details  = results.Conditional_details;
            result.Filleddetails        = results.Filleddetails;
            result.lastpricedet         = results.lastpricedet;
            result.assetdetails         = results.Assetdetails;
            result.contractdetails      = results.contractdetails;
            result.position_details     = results.position_details;
            result.allposition_details  = results.allposition_details;
            result.closed_positions     = results.closed_positions;
            result.daily_details        = results.daily_details;
            result.notify_show          = 'no';
            res.json(result);
        } else {
            result.status = false;
            result.message = 'Error occured.';
            result.err = '';
            result.notify_show = 'no';
            res.json(result);
        }
    });
});
function order_placing(ordertype,buyorsell,price,quantity,leverage,pairname,userid,trigger_price=0,trigger_type=null,id=0,typeorder='Conditional',trailstopdistance=0)
{
    console.log(trigger_type,'trigger_type');
      var leverage       = parseFloat(100/leverage);
      var initial_margin = ((parseFloat(quantity)/parseFloat(price))*leverage)/100;
      var feetoopen      = (quantity/price)*0.075/100;
      var Bankruptcy     = price * leverage /(parseFloat(leverage) + 1);
      var feetoclose     = (quantity/Bankruptcy) * 0.075/100;
      var order_value    = parseFloat(quantity/price).toFixed(8);
      var order_cost     = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);
      order_cost         = parseFloat(order_cost).toFixed(8);

       async.parallel({
      position_details : function(cb) {
          tradeTable.aggregate([
          { "$match": { status:'1',position_status:'1',userId:ObjectId(userid),"pairName": pairname } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"quantity" :{ "$sum": "$filled.filledAmount" }} }
          ]).exec(cb)
      },

    },(err,results) => {
      var position_details = (results.position_details.length>0)?results.position_details[0].quantity:0;
          perpetual.findOne({tiker_root:pairname},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){
                var mainmargin = contractdetails.maint_margin/100;
                var balance_check = true;
            if(buyorsell=='buy')
            {
                if(position_details<0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                var Liqprice = price*leverage/((leverage+1)-(mainmargin*leverage));
            }
            else
            {
               if(position_details>0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                quantity = parseFloat(quantity)*-1;
                var Liqprice = price*leverage/((leverage-1)+(mainmargin*leverage));
            }
            if(err){
              res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
            }
            else
            {
              Assets.findOne({userId:ObjectId(userid),currencySymbol:contractdetails.first_currency},function(err,assetdetails){
                if(err){
                  res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                } else if(assetdetails){
                  var firstcurrency  = contractdetails.first_currency;
                  var secondcurrency = contractdetails.second_currency;
                  var curbalance     = assetdetails.balance;
                  if(curbalance<order_cost && balance_check==true)
                  {
                    res.json({status:false,message:"Due to insuffient balance order cannot be placed",notify_show:'yes'})
                  } else {

                    var before_reduce_bal = curbalance;
                    var after_reduce_bal = curbalance-(balance_check)?order_cost:0;

                    var updateObj = {balance:after_reduce_bal};

                    // Assets.findByIdAndUpdate(assetdetails._id, updateObj, {new: true}, function(err, changed) {
                    //   if (err) {
                    //     res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                    //   } else if(changed){
                      console.log(typeorder,'triggertyrp')
                      if(typeorder=='trailingstop')
                      {
                           const newtradeTable = new tradeTable({
                          quantity          : quantity,
                          price             : price,
                          trigger_price     : trigger_price,
                          orderCost         : order_cost,
                          orderValue        : order_value,
                          leverage          : leverage,
                          userId            : userid,
                          pair              : contractdetails._id,
                          pairName          : pairname,
                          beforeBalance     : before_reduce_bal,
                          afterBalance      : after_reduce_bal,
                          firstCurrency     : firstcurrency,
                          secondCurrency    : secondcurrency,
                          Liqprice          : Liqprice,
                          orderType         : ordertype,
                          trigger_type      : trigger_type,
                          stopstatus        : '0',
                          buyorsell         : buyorsell,
                          pairid            : id,
                          trigger_ordertype : typeorder,
                          trailstop         : '1',
                          trailstopdistance : trailstopdistance,
                          status            : 4 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                        });
                           newtradeTable
                        .save()
                        .then(curorder => {
                          if(typeof io != 'undefined')
                          {
                              io.sockets.in(userid).emit('NOTIFICATION',"Trail stop order created successfully");
                          }
                          tradematching(curorder);
                        }).catch(err => { console.log(err,'error'); res.json({status:false,message:"Your order not placed.",notify_show:'yes'})});``
                      }
                      else
                      {
                         const newtradeTable = new tradeTable({
                          quantity          : quantity,
                          price             : price,
                          trigger_price     : trigger_price,
                          orderCost         : order_cost,
                          orderValue        : order_value,
                          leverage          : leverage,
                          userId            : userid,
                          pair              : contractdetails._id,
                          pairName          : pairname,
                          beforeBalance     : before_reduce_bal,
                          afterBalance      : after_reduce_bal,
                          firstCurrency     : firstcurrency,
                          secondCurrency    : secondcurrency,
                          Liqprice          : Liqprice,
                          orderType         : ordertype,
                          trigger_type      : trigger_type,
                          stopstatus        : (typeorder!='Conditional')?'1':'0',
                          buyorsell         : buyorsell,
                          pairid            : id,
                          trigger_ordertype : typeorder,
                          status            : (trigger_type!=null)?4:0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                        });
                         newtradeTable
                        .save()
                        .then(curorder => {


                          tradematching(curorder);
                        }).catch(err => { console.log(err,'error'); res.json({status:false,message:"Your order not placed.",notify_show:'yes'})});``
                      }

                    //   }
                    // })
                    // insert trade tab
                  }
                } else {

                }
              });
            }

          });
    });


}
router.post('/triggerstop', (req, res) => {
    var takeprofitcheck      = req.body.takeprofitcheck;
    var stopcheck            = req.body.stopcheck;
    var quantity             = req.body.quantity;
    var takeprofit           = req.body.takeprofit;
    var ordertype            = req.body.ordertype;
    var buyorsell            = req.body.buyorsell;
    var price                = req.body.price;
    var leverage             = req.body.leverage;
    var trailingstopdistance = req.body.trailingstopdistance;

    if(takeprofitcheck)
    {
        var trigger_price = takeprofit;
        var tptrigger_type = req.body.tptrigger_type;
        order_placing(ordertype,buyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,tptrigger_type,0,'takeprofit');
    }
    if(stopcheck)
    {
        var stoptrigger_type = req.body.stoptrigger_type;
        var trigger_price = stopprice;
        var newbuyorsell = (buyorsell=='buy')?'sell':'buy';
        order_placing(ordertype,newbuyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,stoptrigger_type,0,'stop');
    }
    if(trailingstopdistance!='' && trailingstopdistance!= 0)
    {

        var trigger_price = (buyorsell=='buy')?parseFloat(price)+parseFloat(trailingstopdistance):parseFloat(price)-parseFloat(trailingstopdistance);
        // var newbuyorsell = (buyorsell=='buy')?'sell':'buy';
        order_placing(ordertype,buyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,'Last',0,'trailingstop',trailingstopdistance);
    }
});
router.post('/orderPlacing', (req, res) => {
  if(req.body.price<1 || req.body.quantity <1)
  {
    return res.json({
      status:false,
      message:"Price or Quantity of contract must not be lesser than 1",
      notify_show:'yes'
    });
  }
  //Checking the takeprofit price
  if(req.body.takeprofitcheck)
  {
    if(req.body.buyorsell=='buy')
    {
        if(req.body.takeprofit<req.body.price)
        {
         return res.json({
            status:false,
            message:"Take-Profit price should be higher than Last Traded Price",
            notify_show:'yes'
          });
        }
    }
    else
    {
        if(req.body.takeprofit>req.body.price)
        {
            return res.json({
                status:false,
                message:"Take-Profit price should be lower than Last Traded Price",
                notify_show:'yes'
            });
        }
    }
  }
   //Checking the stop loss price
  if(req.body.stopcheck)
  {
    if(req.body.buyorsell=='buy')
    {
        if(req.body.stopprice>req.body.price)
        {
          return res.json({
            status:false,
            message:"Stop-Loss price should be higher than Liquidation Price but lower than Last Traded Price",
            notify_show:'yes'
          });
        }
    }
    else
    {
        if(req.body.stopprice<req.body.price)
        {
           return res.json({
                status:false,
                message:"Stop-Loss price should be lower than Liquidation Price but higher than Last Traded Price",
                notify_show:'yes'
            });
        }
    }
  }



    const { errors, isValid } = validateTradeInput(req.body);
    if (!isValid) {
      res.json({
        status:false,
        message:"Error occured, please fill all required fields.",
        errors:errors,
        notify_show:'yes'
      });
    } else {

      var post_only       = req.body.post_only;
      var reduce_only     = req.body.reduce_only;
      var ordertype       = req.body.ordertype;
      var buyorsell       = req.body.buyorsell;
      var price           = req.body.price;
      var timeinforcetype = req.body.timeinforcetype;
      var trigger_price   = req.body.trigger_price;
      var trigger_type    = req.body.trigger_type;
      var quantity        = req.body.quantity;
      var takeprofitcheck = req.body.takeprofitcheck;
      var stopcheck       = req.body.stopcheck;
      var takeprofit      = req.body.takeprofit;
      var stopprice       = req.body.stopprice;
      var leverage        = parseFloat(100/req.body.leverage);
      var initial_margin  = ((parseFloat(quantity)/parseFloat(price))*leverage)/100;
      var feetoopen       = (quantity/price)*0.075/100;
      var Bankruptcy      = price * req.body.leverage /(parseFloat(req.body.leverage) + 1);
      var feetoclose      = (quantity/Bankruptcy) * 0.075/100;
      var order_value     = parseFloat(quantity/price).toFixed(8);
      var order_cost      = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);
      order_cost          = parseFloat(order_cost).toFixed(8);


       async.parallel({
      position_details : function(cb) {
        var pair = req.body.pairname;
          tradeTable.aggregate([
          { "$match": { status:'1',position_status:'1',userId:ObjectId(req.body.userid),"pairName": pair } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"quantity" :{ "$sum": "$filled.filledAmount" }} }
          ]).exec(cb)
      },

    },(err,results) => {
      var position_details = (results.position_details.length>0)?results.position_details[0].quantity:0;
          perpetual.findOne({tiker_root:req.body.pairname},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){
                var mainmargin = contractdetails.maint_margin/100;
                var balance_check = true;
            if(req.body.buyorsell=='buy')
            {
                if(position_details<0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                var Liqprice = price*req.body.leverage/((req.body.leverage+1)-(mainmargin*req.body.leverage));
            }
            else
            {
               if(position_details>0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                quantity = parseFloat(quantity)*-1;
                var Liqprice = price*req.body.leverage/((req.body.leverage-1)+(mainmargin*req.body.leverage));
            }
            if(err){
              res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
            }
            else
            {
              Assets.findOne({userId:ObjectId(req.body.userid),currencySymbol:contractdetails.first_currency},function(err,assetdetails){
                if(err){
                  res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                } else if(assetdetails){
                  var firstcurrency  = contractdetails.first_currency;
                  var secondcurrency = contractdetails.second_currency;
                  var curbalance     = (firstcurrency=='BTC')?parseFloat(assetdetails.balance)+parseFloat(assetdetails.tempcurrency):assetdetails.balance;
                  if(curbalance<order_cost && balance_check==true)
                  {
                    res.json({status:false,message:"Due to insuffient balance order cannot be placed",notify_show:'yes'})
                  } else {

                    var before_reduce_bal = assetdetails.balance;
                    if(firstcurrency=='BTC')
                    {
                        var bonus = assetdetails.tempcurrency;
                        if(order_cost<=assetdetails.tempcurrency)
                        {
                            var bonusreduce = parseFloat(assetdetails.tempcurrency)-parseFloat(order_cost);
                            var after_reduce_bal = assetdetails.balance;
                        }
                        else
                        {
                            var bonusreduce = 0;
                            var redbalance = parseFloat(order_cost)-parseFloat(assetdetails.tempcurrency);
                            var after_reduce_bal = parseFloat(assetdetails.balance)-parseFloat(redbalance);
                        }
                        var updateObj = {balance:after_reduce_bal,tempcurrency:bonusreduce};
                    }
                    else
                    {

                        var bonus = 0;
                        var bonusreduce = 0;
                        var after_reduce_bal = (balance_check)?(curbalance-order_cost):curbalance;
                        var updateObj = {balance:after_reduce_bal};
                    }

                    var userid = req.body.userid;


                    Assets.findByIdAndUpdate(assetdetails._id, updateObj, {new: true}, function(err, changed) {
                      if (err) {
                        res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                      } else if(changed){

                        const newtradeTable = new tradeTable({
                          quantity           : quantity,
                          price              : price,
                          trigger_price      : trigger_price,
                          orderCost          : order_cost,
                          orderValue         : order_value,
                          leverage           : req.body.leverage,
                          userId             : req.body.userid,
                          pair               : contractdetails._id,
                          pairName           : req.body.pairname,
                          postOnly           : post_only,
                          reduceOnly         : reduce_only,
                          beforeBalance      : before_reduce_bal,
                          afterBalance       : after_reduce_bal,
                          beforebonusBalance : bonus,
                          afterbonusBalance  : bonusreduce,
                          timeinforcetype    : timeinforcetype,
                          firstCurrency      : firstcurrency,
                          secondCurrency     : secondcurrency,
                          Liqprice           : Liqprice,
                          orderType          : ordertype,
                          trigger_type       : trigger_type,
                          buyorsell          : buyorsell,
                          status             : (trigger_type!=null)?4:0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                        });
                        newtradeTable
                        .save()
                        .then(curorder => {
                          var io = req.app.get('socket');
                          if(typeof io != 'undefined')
                          {
                              io.sockets.in(req.body.userid).emit('TRADE', curorder);
                           }
                          res.json({status:true,message:"Your order placed successfully.",notify_show:'yes'});
                          if(takeprofitcheck)
                          {
                              var trigger_price = takeprofit;
                              var tptrigger_type = req.body.tptrigger_type;
                              order_placing(ordertype,buyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,tptrigger_type,curorder._id,'takeprofit');
                          }
                          if(stopcheck)
                          {
                              var stoptrigger_type = req.body.stoptrigger_type;
                              var trigger_price = stopprice;
                              var newbuyorsell = (buyorsell=='buy')?'sell':'buy';
                              order_placing(ordertype,newbuyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,stoptrigger_type,curorder._id,'stop');
                          }
                          tradematching(curorder,io);
                        }).catch(err => { console.log(err,'error'); res.json({status:false,message:"Your order not placed.",notify_show:'yes'})});``
                      }
                    })
                    // insert trade tab
                  }
                } else {
                  res.json({status:false,message:"Error occured.",err:'no res 2',notify_show:'yes'});
                }
              });
            }

          });
    });
    }
  });
function selldetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,buyprice,maker_rebate,io)
{

  async.waterfall([
    function(callback){
     tradeTable.findOneAndUpdate({_id:ObjectId(buyorderid)},{"$set":{"status":buyUpdate.status,"filled":tempdata},"$inc":{"filledAmount" : parseFloat(buyUpdate.filledAmt)}},{new:true,"fields": {status:1,filled:1}},function(buytemp_err,buytempData1){
            if(buytempData1)
            {
                 callback(null, buytempData1);
            }
      });
    },
    function(data,callback){

        var fee_amount        = parseFloat(sellUpdate.filledAmt/buyprice)*parseFloat(maker_rebate)/100;
        tempdata.Type         = "sell";
        tempdata.user_id      = ObjectId(selluserid);
        tempdata.Fees         = parseFloat(fee_amount).toFixed(8);
        tempdata.filledAmount = +(sellUpdate.filledAmt).toFixed(8)*-1;
        tradeTable.findOneAndUpdate({_id:ObjectId(sellorderid)},{"$set":{"status":sellUpdate.status,"filled":tempdata},"$inc":{"filledAmount" : parseFloat(sellUpdate.filledAmt * -1)}},{new:true,"fields": {status:1,filled:1} },function(buytemp_err,selltempData){
                if(selltempData)
                {
                  positionmatching(data.filled[data.filled.length-1]);
                  positionmatching(selltempData.filled[selltempData.filled.length-1]);
                   callback(null, selltempData);
                }
        });
    },
], function (err,result) {

    //socket call
    gettradedata(result.filled[0].firstCurrency,result.filled[0].secondCurrency,io);
    getusertradedata(result.filled[0].selluserId,result.filled[0].firstCurrency,result.filled[0].secondCurrency,io);
    getusertradedata(result.filled[0].buyuserId,result.filled[0].firstCurrency,result.filled[0].secondCurrency,io);


    tradeTable.findOneAndUpdate({pairid:(buyorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){

    });

    tradeTable.findOneAndUpdate({pairid:(sellorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){

    });

 tradeTable.find({status:'4',trigger_type:'Last'},function(buytemp_err,buytempData){
              if(buytempData.length)
              {
                  for (var i=0; i < buytempData.length; i++) {
                      var _id           = buytempData[i]._id;
                      var price         = buytempData[i].price;
                      var trigger_price = buytempData[i].trigger_price;
                      var userId        = buytempData[i].userId;
                      var pairName      = buytempData[i].pairName;
                      var leverage      = buytempData[i].leverage;
                      var quantity      = buytempData[i].quantity;
                      var buyorsell     = buytempData[i].buyorsell;
                      var orderType     = buytempData[i].orderType;
                      var trailstop     = buytempData[i].trailstop;
                      var different     = parseFloat(price)-parseFloat(trigger_price);
                      console.log(trigger_price,'trigger_price');
                      console.log(buyprice,'buyprice');
                      if(different>0)
                      {
                          if(trailstop=='0' && parseFloat(trigger_price)>parseFloat(buyprice))
                          {
                              // order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
                                tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                          }
                      }
                      else
                      {
                          if(trailstop=='0' && parseFloat(trigger_price)<parseFloat(buyprice))
                          {
                              //order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
                               tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){

                                    console.log(buytemp_err,'trigger error');
                                });
                          }
                      }
                      //trailing stop trigger
                      if(trailstop=='1' && buyorsell=='buy' && parseFloat(price)>parseFloat(buyprice))
                      {
                          var addprice = (parseFloat(buyprice)-parseFloat(price))
                          var newtriggerprice = parseFloat(trigger_price)+parseFloat(addprice);
                          tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"price":newtriggerprice,"trigger_price":newtriggerprice}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                 console.log(buytemp_err,'trigger error');
                          });
                      }
                      if(trailstop=='1' && buyorsell=='buy' && parseFloat(trigger_price)<parseFloat(buyprice))
                      {
                          tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                      }
                      if(trailstop=='1' && buyorsell=='sell' && parseFloat(price)<parseFloat(buyprice))
                      {
                            var addprice = (parseFloat(price)-parseFloat(buyprice))
                            var newtriggerprice = parseFloat(trigger_price)-parseFloat(addprice);
                            tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"price":newtriggerprice,"trigger_price":newtriggerprice}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                 console.log(buytemp_err,'trigger error');
                            });
                      }
                      if(trailstop=='1' && buyorsell=='sell' && parseFloat(trigger_price)>parseFloat(buyprice))
                      {
                          tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                      }
                  }
              }
          });

});
}
function buydetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,buyprice,maker_rebate,io)
{

  async.waterfall([
    function(callback){
      tradeTable.findOneAndUpdate({_id:ObjectId(buyorderid)},{ "$set": {"filled":tempdata,"status":buyUpdate.status},"$inc":{"filledAmount":buyUpdate.filledAmt}},{new:true,"fields": {filled:1} },function(buytemp_err,buytempData){
            if(buytempData)
            {
                 callback(null, buytempData);
            }
      });
    },
    function(data,callback){
        var fee_amount        = parseFloat(sellUpdate.filledAmt/buyprice)*parseFloat(maker_rebate)/100;
        tempdata.Type         = "sell";
        tempdata.user_id      = ObjectId(selluserid);
        tempdata.filledAmount = (sellUpdate.filledAmt) * -1;
        tempdata.Fees         = parseFloat(fee_amount).toFixed(8);

        tradeTable.findOneAndUpdate({_id:ObjectId(sellorderid)},{ "$set": {"filled":tempdata,"status":sellUpdate.status},"$inc":{"filledAmount":parseFloat(sellUpdate.filledAmt)*-1}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
              positionmatching(data.filled[data.filled.length-1]);
              positionmatching(selltempData.filled[selltempData.filled.length-1]);
              callback(null, selltempData);
        });
    },
], function (err,result) {

    //socket call
    gettradedata(result.filled[0].firstCurrency,result.filled[0].secondCurrency,io);
    getusertradedata(result.filled[0].selluserId,result.filled[0].firstCurrency,result.filled[0].secondCurrency,io);
    getusertradedata(result.filled[0].buyuserId,result.filled[0].firstCurrency,result.filled[0].secondCurrency,io);

    tradeTable.findOneAndUpdate({pairid:(buyorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){

    });

    tradeTable.findOneAndUpdate({pairid:(sellorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){

    });

    tradeTable.find({status:'4',trigger_type:'Last'},function(buytemp_err,buytempData){
              if(buytempData.length)
              {
                  for (var i=0; i < buytempData.length; i++) {
                      var _id           = buytempData[i]._id;
                      var price         = buytempData[i].price;
                      var trigger_price = buytempData[i].trigger_price;
                      var userId        = buytempData[i].userId;
                      var pairName      = buytempData[i].pairName;
                      var leverage      = buytempData[i].leverage;
                      var quantity      = buytempData[i].quantity;
                      var buyorsell     = buytempData[i].buyorsell;
                      var orderType     = buytempData[i].orderType;
                      var trailstop     = buytempData[i].trailstop;
                      var different     = parseFloat(price)-parseFloat(trigger_price);
                      console.log(trigger_price,'trigger_price');
                      console.log(buyprice,'buyprice');
                      if(different>0)
                      {
                          if(trailstop=='0' && parseFloat(trigger_price)>parseFloat(buyprice))
                          {
                              // order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
                                tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                          }
                      }
                      else
                      {
                          if(trailstop=='0' && parseFloat(trigger_price)<parseFloat(buyprice))
                          {
                              //order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
                               tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){

                                    console.log(buytemp_err,'trigger error');
                                });
                          }
                      }
                      //trailing stop trigger
                      if(trailstop=='1' && buyorsell=='buy' && parseFloat(price)>parseFloat(buyprice))
                      {
                          var addprice = (parseFloat(buyprice)-parseFloat(price))
                          var newtriggerprice = parseFloat(trigger_price)+parseFloat(addprice);
                          tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"price":newtriggerprice,"trigger_price":newtriggerprice}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                 console.log(buytemp_err,'trigger error');
                          });
                      }
                      if(trailstop=='1' && buyorsell=='buy' && parseFloat(trigger_price)<parseFloat(buyprice))
                      {
                          tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                      }
                      if(trailstop=='1' && buyorsell=='sell' && parseFloat(price)<parseFloat(buyprice))
                      {
                            var addprice = (parseFloat(price)-parseFloat(buyprice))
                            var newtriggerprice = parseFloat(trigger_price)-parseFloat(addprice);
                            tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"price":newtriggerprice,"trigger_price":newtriggerprice}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                 console.log(buytemp_err,'trigger error');
                            });
                      }
                      if(trailstop=='1' && buyorsell=='sell' && parseFloat(trigger_price)>parseFloat(buyprice))
                      {
                          tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                      }
                  }
              }
          });

});
}
async function buymatchingprocess(curorder,tradedata,pairData,io)
{
    console.log("buy");
    var buyOrder  = curorder,
    buyAmt        = rounds(buyOrder.quantity),
    buyorderid    = buyOrder._id,
    buyuserid     = buyOrder.userId,
    forceBreak    = false,
    buyeramount   = rounds(buyOrder.quantity),
    buyeramount1  = rounds(buyOrder.quantity),
    buytempamount = 0;
    buy_res_len   = tradedata.length;

    // tradedata.forEach(function(data_loop){
        for (var i = 0; i < tradedata.length; i++) {
        var data_loop = tradedata[i];
        buyAmt = rounds(buyOrder.quantity-buytempamount);
        if(buyAmt == 0 || forceBreak == true){
          console.log("break");
          return;
        }
        else{
            var ii = i,
            sellOrder   = data_loop,
            sellorderid = sellOrder._id,
            selluserid  = sellOrder.userId,
            sellAmt     = rounds(+sellOrder.quantity - +sellOrder.filledAmount),
            silentBreak = false,
            buyUpdate   = {},
            sellUpdate  = {},
            buyerBal    = 0,
            sellerBal   = 0,
            orderSocket = {}
            buyeramount = buyeramount-sellAmt;
            console.log(buyAmt,"buyAmt");
            console.log(Math.abs(sellAmt),"sellAmt");
            if (buyAmt == Math.abs(sellAmt)) {
                console.log("amount eq");
                buyUpdate = {
                  status: '1',
                  filledAmt: Math.abs(sellAmt)
                }
                sellUpdate =  {
                  status: '1',
                  filledAmt: buyAmt
                }
                forceBreak = true
            } else if (buyAmt > Math.abs(sellAmt)) {
                console.log("else buy gt");
                buyUpdate = {
                  status: '2',
                  filledAmt: Math.abs(sellAmt)
                }
                sellUpdate = {
                  status: '1',
                  filledAmt: Math.abs(sellAmt)
                }
                buyAmt = rounds(+buyAmt - +sellAmt)
            } else if (buyAmt < Math.abs(sellAmt)) {
                console.log("else sell gt");
                buyUpdate = {
                  status: '1',
                  filledAmt: buyAmt
                }
                sellUpdate = {
                  status: '2',
                  filledAmt: buyAmt
                }
                forceBreak = true
            } else {
                silentBreak = true
            }

            if (silentBreak == false) {
                console.log("si brk");
                var taker_fee = pairData.taker_fees;
                var fee_amount = parseFloat(buyUpdate.filledAmt/buyOrder.price)*parseFloat(pairData.taker_fees)/100;
                var tempdata = {
                    "pair"           : ObjectId(pairData._id),
                    "firstCurrency"  : pairData.first_currency,
                    "secondCurrency" : pairData.second_currency,
                    "buyuserId"      : ObjectId(buyuserid),
                    "user_id"        : ObjectId(buyuserid),
                    "selluserId"     : ObjectId(selluserid),
                    "sellId"         : ObjectId(sellorderid),
                    "buyId"          : ObjectId(buyorderid),
                    "filledAmount"   : +(buyUpdate.filledAmt).toFixed(8),
                    "Price"          : +buyOrder.price,
                    "pairname"       : curorder.pairName,
                    "order_cost"     : (+(buyUpdate.filledAmt)/+buyOrder.price).toFixed(8),
                    "Fees"           : parseFloat(fee_amount).toFixed(8),
                    "status"         : "filled",
                    "Type"           : "buy",
                    "created_at"     : new Date(),
                }
                buytempamount += +buyUpdate.filledAmt

                await buydetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,buyOrder.price,pairData.maker_rebate,io);
                if(tradedata.length==i && forceBreak!=true && curorder.timeinforcetype=='ImmediateOrCancel')
                {
                    cancel_trade(curorder._id,curorder.userId);
                }
                // positionmatching(data_loop);
                if(forceBreak == true) {
                    console.log('forceBreak')
                    return true;
                }
            }
        }
    }

}

async function sellmatchingprocess(curorder,tradedata,pairData,io)
{
  var sellOrder = curorder,
  sellorderid    = sellOrder._id,
  selluserid     = sellOrder.userId,
  sellAmt        = rounds(Math.abs(sellOrder.quantity)),
  forceBreak     = false,
  selleramount   = rounds(sellOrder.quantity)
  selleramount1  = rounds(sellOrder.quantity)
  selltempamount = 0;
  sell_res_len   = tradedata.length;
  for (var i = 0; i < tradedata.length; i++) {
    var data_loop = tradedata[i];
    sellAmt        = rounds(Math.abs(sellOrder.quantity)-selltempamount);
    console.log('loop starting',i);
    if(sellAmt == 0 || forceBreak == true)
    return

    var ii = i,
    buyOrder     = data_loop,
    buyorderid   = buyOrder._id,
    buyuserid    = buyOrder.userId,
    buyAmt       = rounds(buyOrder.quantity - buyOrder.filledAmount),
    silentBreak  = false,
    buyUpdate    = {},
    sellUpdate   = {},
    buyerBal     = 0,
    sellerBal    = 0,
    orderSocket  = {}
    selleramount = selleramount-buyAmt;

    console.log(Math.abs(sellAmt),'sellamount');
    console.log(buyAmt,'buyamount');
    if(Math.abs(sellAmt) == buyAmt) {
        buyUpdate = {
          status    : 1,
          filledAmt : Math.abs(sellAmt)
        }
        sellUpdate = {
          status    : 1,
          filledAmt : buyAmt
        }
        forceBreak = true
    } else if(Math.abs(sellAmt) > buyAmt) {
        buyUpdate = {
          status    : 1,
          filledAmt : buyAmt
        }
        sellUpdate = {
          status    : 2,
          filledAmt : buyAmt
        }
        sellAmt = rounds(+sellAmt - +buyAmt)
    } else if(Math.abs(sellAmt) < buyAmt) {
        buyUpdate = {
          status    : 2,
          filledAmt : Math.abs(sellAmt)
        }
        sellUpdate = {
          status    : 1,
          filledAmt : Math.abs(sellAmt)
        }
        forceBreak = true
    } else {
      silentBreak = true
    }
    var returnbalance = 0;
    if(+buyOrder.price > +sellOrder.price)
    {
      var return_price = +buyOrder.price - +sellOrder.price;
      returnbalance    = +buyUpdate.filledAmt* +return_price;
      returnbalance    = parseFloat(returnbalance).toFixed(8);
    }

    if(silentBreak == false) {
      var taker_fee = pairData.taker_fees;
      var fee_amount = parseFloat(buyUpdate.filledAmt/buyOrder.price)*parseFloat(pairData.taker_fees)/100;
        var tempdata = {
        "pair"           : ObjectId(pairData._id),
        "firstCurrency"  : pairData.first_currency,
        "secondCurrency" : pairData.second_currency,
        "buyuserId"      : ObjectId(buyuserid),
        "user_id"        : ObjectId(buyuserid),
        "selluserId"     : ObjectId(selluserid),
        "sellId"         : ObjectId(sellorderid),
        "buyId"          : ObjectId(buyorderid),
        "filledAmount"   : +(buyUpdate.filledAmt).toFixed(8),
        "Price"          : +sellOrder.price,
        "pairname"       : curorder.pairName,
        "order_cost"     : (+(buyUpdate.filledAmt)/+sellOrder.price).toFixed(8),
        "status"         : "filled",
        "Type"           : "buy",
        "Fees"           : parseFloat(fee_amount).toFixed(8),
        "created_at"     : new Date()
        }
        selltempamount += +sellUpdate.filledAmt;
        console.log(tempdata,'before sell update');
        await selldetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,sellOrder.price,pairData.maker_rebate,io);
        if(tradedata.length==i && forceBreak!=true && curorder.timeinforcetype=='ImmediateOrCancel')
        {
            cancel_trade(curorder._id,curorder.userId);
        }
        if(forceBreak == true) {
            console.log("true");
            // getrateandorders(curorder.pair,userid);
            return true
        }
    }
    if(forceBreak == true) {
        // getrateandorders(curorder.pair,userid);
        return true
    }
  }
}
function tradematching(curorder,io)
{
  //Fill or kill order type
  if(curorder.timeinforcetype=="FillOrKill")
  {
    console.log('filleorkill');
      var datas = {
        '$or' : [{"status" : '2'},{"status" : '0'}],
        'userId' : { $ne : ObjectId(curorder.userId) },
        'pairName': (curorder.pairName)
      },sort;

      if(curorder.buyorsell == 'buy') {
        datas['buyorsell'] = 'sell'
        datas['price'] = { $lte : curorder.price }
        sort = { "price" : 1 }
      } else {
        datas['buyorsell'] = 'buy'
        datas['price'] = { $gte : curorder.price }
        sort = { "price" : -1 }
      }
        tradeTable.aggregate([
            {$match:datas},
            {
                $group : {
                    "_id"          : null,
                    'quantity'     : { $sum : '$quantity' },
                    'filledAmount' : { $sum : '$filledAmount' }
                }
            },
            {$sort:sort},
            {$limit: 10},
        ]).exec((tradeerr,tradedata) => {
          if(tradedata.length>0)
          {
              var quantity      = tradedata[0].quantity;
              var filledAmount  = tradedata[0].filledAmount;
              var pendingamount = parseFloat(quantity) - parseFloat(filledAmount);
              if(curorder.quantity>pendingamount)
              {
                  cancel_trade(curorder._id,curorder.userId);
              }
          }
          else
          {
            cancel_trade(curorder._id,curorder.userId);
          }
        });
  }
  var datas = {
      '$or' : [{"status" : '2'},{"status" : '0'}],
      'userId' : { $ne : ObjectId(curorder.userId) },
      'pairName': (curorder.pairName)
    },sort;

    if(curorder.buyorsell == 'buy') {
      datas['buyorsell'] = 'sell'
      datas['price'] = { $lte : curorder.price }
      sort = { "price" : 1 }
    } else {
      datas['buyorsell'] = 'buy'
      datas['price'] = { $gte : curorder.price }
      sort = { "price" : -1 }
    }
      // console.log(datas,'datas');
    tradeTable.aggregate([
      {$match:datas},
      {$sort:sort},
    ]).exec((tradeerr,tradedata) => {
    perpetual.findOne({_id:ObjectId(curorder.pair)}).exec(function(pairerr,pairData){
        console.log('perpetual');
        if(tradeerr)
            console.log({status:false,message:tradeerr});
        else
            if(tradedata.length>0){
                if(curorder.postOnly)
                {
                  cancel_trade(curorder._id,curorder.userId);
                }
                var i=0;
                if (curorder.buyorsell == 'buy') {
                   buymatchingprocess(curorder,tradedata,pairData,io);
                } else if (curorder.buyorsell == 'sell') {
                    sellmatchingprocess(curorder,tradedata,pairData,io);
                }
            }
            else
            {
              if(curorder.timeinforcetype=="ImmediateOrCancel")
              {
                  cancel_trade(curorder._id,curorder.userId);
              }
              gettradedata(curorder.firstCurrency,curorder.secondCurrency,io);
            }
            // positionmatching(curorder);
    });
  });
}
function positionmatching(curorder)
{
  console.log('positionmatching',curorder);
    var datas = {
        'filled.user_id'         : ObjectId(curorder.user_id) ,
        'filled.pairname'        : (curorder.pairname),
        "filled.position_status" : '1',
        "filled.status"          : 'filled'
    },sort;
    if(curorder.Type=='buy')
    {
      datas['filled.Type'] = 'sell';
    }
    else
    {
      datas['filled.Type'] = 'buy';
    }
    tradeTable.aggregate([
        {$unwind:"$filled"},
        {$project:{"filled":1}},
        {$match:datas},
    ]).exec((tradeerr,tradedata) => {

        if(tradedata.length>0)
        {
          var buytemp =0;
          var selltemp =0;
            for(var i=0;i<tradedata.length;i++)
            {
                if(curorder.Type=='buy')
                {
                  console.log(curorder.filledAmount,'curqauntity');
                  var sellorderid   = tradedata[i].filled.sellId;
                  var firstCurrency = curorder.firstCurrency;
                  var buyorderid    = curorder.buyId;
                  var user_id       = curorder.user_id;
                  var buyFees       = curorder.Fees;
                  var sellFees      = tradedata[i].filled.Fees;
                  var buypairName   = curorder.pairname;
                  var buyuserId     = curorder.buyuserId;
                  var buypair       = curorder.pair;
                  var sellquantity  = Math.abs(tradedata[i].filled.filledAmount);
                  var buyqauntity   = parseFloat(curorder.filledAmount)-buytemp;
                  var buyprice      = curorder.Price;
                  var sellprice     = tradedata[i].filled.Price;
                  var buyupdate     = {};
                  var sellupdate    = {};
                  var forceBreak    = false;
                  if(forceBreak == true) {
                      break;
                  }
                  console.log(buyqauntity,'buyqauntity');
                  console.log(sellquantity,'sellquantity');
                  if(sellquantity==buyqauntity)
                  {
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }
                  else if(sellquantity<buyqauntity)
                  {
                    buyupdate['position_status']  = 2;
                    buyupdate['positionFilled']   = sellquantity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = sellquantity;
                  }
                  else{
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 2;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }

                  buytemp += buyupdate.positionFilled;
                  tradeTable.findOneAndUpdate({'filled.buyId':ObjectId(buyorderid),'filled.user_id':ObjectId(user_id)},{ "$set": {"filled.$.position_status":buyupdate.position_status}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
                    console.log(selltemp_err,'sell error');
                    console.log(selltempData,'sell result');
                        if(selltempData)
                        {
                            var totalfees   = parseFloat(buyFees)+parseFloat(sellFees);
                            var profitnloss = [ (1/parseFloat(sellprice))  - (1/parseFloat(buyprice)) ] * parseFloat(buyupdate.positionFilled);
                            profitnloss     = parseFloat(profitnloss)-parseFloat(totalfees);

                            const newposition = new position_table({
                            "pairname"          : buypairName,
                            "pair"              : buypair,
                            "userId"            : buyuserId,
                            "closing_direction" : "Closed short",
                            "quantity"          : buyupdate.positionFilled,
                            "exit_price"        : buyprice,
                            "entry_price"       : sellprice,
                            "profitnloss"       : profitnloss,
                            });
                            newposition.save(function(err,data) {
                              console.log("success");
                              var updatebaldata = {};


                             Assets.find({currencySymbol:firstCurrency,userId:ObjectId(user_id)},function(balerr,userbalance){
                                console.log(userbalance,'userbalance');
                              })


                             console.log(profitnloss,'profitnloss1');
                              Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(user_id)},{"$inc": {"balance":parseFloat(profitnloss).toFixed(8)} } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
                                console.log(balerr,'buybalance error');
                                console.log(baldata,'buybaldata');
                              });

                            });
                        }
                  });

                  tradeTable.findOneAndUpdate({'filled.sellId':ObjectId(sellorderid),'filled.user_id':ObjectId(user_id)},{ "$set": {"filled.$.position_status":sellupdate.position_status}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
                        if(selltempData)
                        {
                            console.log(selltemp_err,'buy');
                            console.log(selltempData,'buy');
                        }
                  });

                }
                else
                {
                  var buyorderid    = tradedata[i].filled.buyId;
                  var buyprice      = tradedata[i].filled.Price;
                  var sellorderid   = curorder.sellId;
                  var firstCurrency = curorder.firstCurrency;
                  var selluserId    = curorder.userId;
                  var user_id       = curorder.user_id;
                  var sellpair      = curorder.pair;
                  var sellprice     = curorder.Price;
                  var buyFees       = curorder.Fees;
                  var sellFees      = tradedata[i].filled.Fees;
                  var sellpairName  = curorder.pairname;
                  var buyqauntity   = Math.abs(tradedata[i].filled.filledAmount);
                  var sellquantity  = Math.abs(curorder.filledAmount)-selltemp;
                  var buyupdate     = {};
                  var sellupdate    = {};
                  var forceBreak    = false;
                  if(forceBreak == true) {
                      break;
                  }
                  console.log(buyqauntity,'buyqauntity1');
                  console.log(sellquantity,'sellquantity1');
                  if(sellquantity==buyqauntity)
                  {
                    console.log('equal');
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }
                  else if(sellquantity<buyqauntity)
                  {
                    buyupdate['position_status']  = 2;
                    buyupdate['positionFilled']   = sellquantity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = sellquantity;
                  }
                  else{
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 2;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }
                   console.log('after else');
                  buytemp += buyupdate.positionFilled;
                  tradeTable.findOneAndUpdate({'filled.buyId':ObjectId(buyorderid),'filled.user_id':ObjectId(user_id)},{ "$set": {"filled.$.position_status":buyupdate.position_status}},{new:true,"fields": {filled:1} },function(buytemp_err,buytempData){
                    console.log(buytemp_err,'posbuytemp_err');
                    console.log(buytempData,'posbuytempData');
                        if(buytempData)
                        {
                            var totalfees   = parseFloat(buyFees)+parseFloat(sellFees);
                            var profitnloss = [ (1/parseFloat(buyprice))  - (1/parseFloat(sellprice)) ] * parseFloat(buyupdate.positionFilled);
                            profitnloss     = parseFloat(profitnloss)-parseFloat(totalfees);

                            const newposition = new position_table({
                            "pairname"          : sellpairName,
                            "pair"              : sellpair,
                            "userId"            : user_id,
                            "closing_direction" : "Closed long",
                            "quantity"          : buyupdate.positionFilled,
                            "exit_price"        : sellprice,
                            "entry_price"       : buyprice,
                            "profitnloss"       : profitnloss,
                            });
                            newposition.save(function(err,data) {
                            console.log(err);
                            console.log('success');
                            console.log(profitnloss,'profitnloss');
                                Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(user_id)},{"$inc": {"balance":parseFloat(profitnloss).toFixed(8)} } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
                                  console.log(balerr,'sellbalerr')
                                  console.log(baldata,'sellbaldata')
                                });

                            });
                        }

                  });

                  tradeTable.findOneAndUpdate({'filled.sellId':ObjectId(sellorderid),'filled.user_id':ObjectId(user_id)},{ "$set": {"filled.$.position_status":sellupdate.position_status}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
                            console.log(selltemp_err,'possell');
                            console.log(selltempData,'possell');
                        if(selltempData)
                        {
                        }
                  });

                }
            }
        }
    });
}

function rounds(n) {
  var roundValue = (+n).toFixed(8)
  return parseFloat(roundValue)
}

router.post('/user-activate', (req, res) => {
    var userid = req.body.userid;
    var updateObj = {active:"Activated"}
    User.findByIdAndUpdate(userid, updateObj, {new: true}, function(err, user) {
      if (user) {
          return res.status(200).json({ message: 'Your Account activated successfully' });
      }
    })
});

router.post('/perpetual-data/', (req, res) => {

    perpetual.find({}).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result});
        }
    });
});

router.post('/order-history/', (req, res) => {
   tradeTable.find({userId:ObjectId(req.body.userid)}).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result,type:"orderhistory"});
        }
    });
});


router.post('/searchorder-history/', (req, res) => {
  console.log(req.body,'postdetails');
  var userid    = req.body.userid;
  var contract  = req.body.contract;
  var type      = req.body.type;
  var startDate = req.body.startDate;
  var endDate   = req.body.endDate;
  var match = {};
  match['userId'] = userid;
  if(contract!='All')
  {
    match['pairName'] = contract;
  }
  if(type!='All')
  {
    match['buyorsell'] = type.toLowerCase();
  }
  if(startDate!='' && endDate!='')
  {
    match['orderDate'] = {$gte:startDate,$lte:endDate};
  }
  else if(startDate!='')
  {
    match['orderDate'] = {$gte:startDate};
  }
  else if(endDate!='')
  {
    match['orderDate'] = {$lte:endDate};
  }
  console.log(match,'search data');
   tradeTable.find(match).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result,type:"orderhistory"});
        }
    });
});


router.post('/searchtrade-history/', (req, res) => {
  var userid    = req.body.userid;
  var contract  = req.body.contract;
  var type      = req.body.type;
  var startDate = req.body.startDate;
  var endDate   = req.body.endDate;
  var match = {};
  match['userId'] = userid;
  match['status'] = '1';
  if(contract!='All')
  {
    match['pairName'] = contract;
  }
  if(type!='All')
  {
    match['buyorsell'] = type.toLowerCase();
  }
  if(startDate!='' && endDate!='')
  {
    match['orderDate'] = {$gte:startDate,$lte:endDate};
  }
  else if(startDate!='')
  {
    match['orderDate'] = {$gte:startDate};
  }
  else if(endDate!='')
  {
    match['orderDate'] = {$lte:endDate};
  }
  console.log(match,'search data');
   tradeTable.find(match).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result,type:"orderhistory"});
        }
    });
});

router.post('/trade-history/', (req, res) => {
    tradeTable.find({status:1,userId:ObjectId(req.body.userid)}).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result,type:"tradehistory"});
        }
    });
});


router.post('/closedposhistory/', (req, res) => {
    position_table.find({userId:ObjectId(req.body.userid)}).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result,type:"closedposhistory"});
        }
    });
});

router.post('/pnlSearchdata/', (req, res) => {
  var userid    = req.body.userid;
  var contract  = req.body.contract;
  var startDate = req.body.startDate;
  var endDate   = req.body.endDate;
  var match = {};
  match['userId'] = userid;
  if(contract!='All')
  {
    match['pairname'] = contract;
  }
  if(startDate!='' && endDate!='')
  {
    match['createdDate'] = {$gte:startDate,$lte:endDate};
  }
  else if(startDate!='')
  {
    match['createdDate'] = {$gte:startDate};
  }
  else if(endDate!='')
  {
    match['createdDate'] = {$lte:endDate};
  }
  console.log(match,'search data');
    position_table.find(match).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result,type:"closedposhistory"});
        }
    });
});


// cron.schedule('3 * * * * *', (req,res) => {
// request
//   .get(keys.baseUrl+'cryptoapi/indexprice_calculation')
//   .on('response', function(response) {
//     console.log(response.statusCode) // 200
//   })

// });

cron.schedule('3 * * * * *', (req,res) => {
	console.log('cronworking');
exchangePrices.find({})
            .skip(10000)
            .sort({createdAt: 'desc'})
            .exec(function(err, result) {
            if (err) {
              next(err);
              console.log(err,'error');
            }
            if (result) {
              // console.log(result);
              result.forEach( function (doc) {
                 doc.remove();
               });
            }
          });
});

cron.schedule('0 */8 * * *', () => {
  fundingAmount();
});

cron.schedule('0 22 * * *', () => {
  interestfeefunction();
});

cron.schedule('0 22 * * *', () => {
  perpetual.find({},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){

 }).then((contractdetails)=> {

// processUsers(contractdetails,'Bitstamp');
// processUsers(contractdetails,'Kraken');
// processUsers(contractdetails,'Coinbasepro');
// processUsers(contractdetails,'Gemini');
  });
});

function forced_liquidation()
{
    perpetual.findOne({tiker_root:'BTCUSD'},function(pererr,perdata){
      if(perdata)
      {
          var lastprice = perdata.last;
          var mainmargin = perdata.mainmargin;
            tradeTable.aggregate([
            { "$match": { status:'1',"pairName": perdata.tiker_root } },
            {$unwind:"$filled"},
            { "$match": { "filled.position_status":'1'} },
            {$project:{"filled":1,leverage:1}},
            { "$group": { "_id": "$filled.user_id","price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
            ]).exec(function(err,result){
                for(i=0;i<result.length;i++)
                {
                    var pos_pairName       = (result[i].pairName)?result[i].pairName:0;
                    var user_id            = (result[i]._id)?result[i]._id:0;
                    var pos_quantity       = (result[i].quantity)?result[i].quantity:0;
                    var pos_price          = (result[i].price)?result[i].price:0;
                    var pos_leverage       = (result[i].leverage)?result[i].leverage:0;

                    //calculate the initial margin
                    var pos_initial_margin = (parseFloat(pos_quantity)/parseFloat(pos_price))*((100/parseFloat(pos_leverage))/100);
                    var profitnloss        = [ (1/parseFloat(pos_price))  - (1/parseFloat(lastprice)) ] * parseFloat(pos_quantity);
                    var profitnlossper     = (parseFloat(profitnloss)/parseFloat(pos_initial_margin))*100;
                    var profitnlossusd     = (profitnloss*parseFloat(lastprice));


                    var mainmarginwithleverage = parseFloat(mainmargin)*parseFloat(pos_leverage);
                    var pos_liqprice       = (parseFloat(pos_price)*parseFloat(pos_leverage))/((parseFloat(pos_leverage)+1)-mainmarginwithleverage);
                    if(pos_quantity>0 && pos_liqprice>lastprice-1)
                    {
                         var orderType = "Market";
                        order_placing(orderType,'sell',lastprice,pos_quantity,pos_leverage,pos_pairName,user_id);
                    }
                    if(pos_quantity<0 && pos_liqprice<lastprice+1)
                    {
                         var orderType = "Market";
                        order_placing(orderType,'buy',lastprice,pos_quantity,pos_leverage,pos_pairName,user_id);
                    }
                }
            });
      }
    });

    //  tradeTable.find({Liqprice:{$gt:lastprice-1,$lte:lastprice},buyorsell:'buy','$or' : [{"status" : '0'},{"status" : '2'}]}).then(result => {
    //     if(result) {
    //         console.log(result);
    //         if(result.length>0)
    //         {
    //             for(var i=0;i<result.length;i++)
    //             {
    //               var quantity  = parseFloat(result[i].quantity) - parseFloat(result[i].filledAmount);
    //               var leverage  = result[i].leverage;
    //               var pairname  = result[i].pairName;
    //               var userId    = result[i].userId;
    //               var orderType = "Market";
    //               order_placing(orderType,'sell',lastprice,quantity,leverage,pairname,userId);
    //             }
    //         }
    //     }
    // });

    // tradeTable.find({Liqprice:{$lt:lastprice+1,$gte:lastprice},buyorsell:'sell',status:{'$or' : [{"status" : '0'},{"status" : '2'}]}}).then(result => {
    //     if(result) {
    //         console.log(result);
    //         if(result.length>0)
    //         {
    //             for(var i=0;i<result.length;i++)
    //             {
    //               var quantity  = parseFloat(result[i].quantity) - parseFloat(result[i].filledAmount);
    //               var leverage  = result[i].leverage;
    //               var pairname  = result[i].pairName;
    //               var userId    = result[i].userId;
    //               var orderType = "Market";
    //               order_placing(orderType,'buy',lastprice,quantity,leverage,pairname,userId);
    //             }
    //         }
    //     }
    // });
}

function interestfeefunction()
{
      var datas = {
        "filled.position_status" : '1',
        "filled.status"          : 'filled'
    },sort;
    perpetual.findOneAndUpdate({tiker_root:pair},{"$set": {"markprice":mark_price,"index_price":index_price} } , {new:true,"fields": {tiker_root:1} } ,function(pererr,perdata){
          console.log(perdata);
          tradeTable.aggregate([
          {$unwind:"$filled"},
          {$project:{"filled":1}},
          {$match:datas},
          ]).exec((tradeerr,tradedata) => {
                if(tradedata.length>0)
                {
                    var quantity   = tradedata.filled.filledAmount;
                    var Price      = tradedata.filled.Price;
                    var order_cost = tradedata.filled.order_cost;
                    var pairname    = tradedata.filled.pairname;
                    var firstCurrency    = tradedata.filled.firstCurrency;
                    var user_id    = tradedata.filled.user_id;
                    var index = perdata.findIndex(x => (x.tiker_root) === pairname);
                    if(index!=-1)
                    {
                        var dailyinterest = perdata[index].dailyinterest;
                        var markprice = perdata[index].markprice;

                        var intrestinfirstcur = order_cost*dailyinterest;
                        var interest = intrestinfirstcur*markprice;
                        var updatebaldata = {};
                        updatebaldata["balance"] = interest;
                        Assets.findOneAndUpdate({currencySymbol:firstCurrency,user_id:ObjectId(user_id)},{"$dec": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){

                        });
                    }

                }
          });
    });
}

router.get('/indexprice_calculation/', (req, res) => {
   var io = req.app.get('socket');
  // funding_rate();
  // forced_liquidation();
  indexprice_calculation('BTCUSD',io);
  indexprice_calculation('ETHUSD',io);
  // indexprice_calculation('BCHUSD');
  // fundingAmount();
  // interestfeefunction();
});
function getimpactbidprice(type,pair)
{
  console.log(pair,'fsdfsdfsdfsdfsdfsdf')
  return tradeTable.aggregate([
    // { "$match": { "orderDate": { $gt:new Date(Date.now() - 8*60*60 * 1000) },"status":'1',"buyorsell":type,"pairname": pair} },
    { "$match": { "status":'1',"buyorsell":type,"pairname": pair } },
    { "$group": { "_id": null,"price" :{ "$avg": "$price" } } }
]);
}

function getavgtradevolume(type,pair)
{
  return exchangePrices.aggregate([
    { "$match": { "createdAt":{
        $gte: (new Date((new Date()).getTime() - (10 * 24 * 60 * 60 * 1000)))
    },"pairname":pair,"exchangename":type } },
    // { "$match": { "status":'1',"buyorsell":type } },
    { "$group": { "_id": null,"volume" :{ "$avg": "$volume" } } }
]);
}

function getspotprice(pair,exchange)
{
  return exchangePrices.findOne({pairname:pair,exchangename:exchange}).sort({'createdAt':-1}).select('last');
}

async function indexprice_calculation(pair,io=null)
{
    var exc_A_avgtradevolumefor_month = await getavgtradevolume('Bitstamp',pair);
    var exc_B_avgtradevolumefor_month = await getavgtradevolume('Kraken',pair);
    var exc_C_avgtradevolumefor_month = await getavgtradevolume('Gemini',pair);

    console.log(exc_A_avgtradevolumefor_month);
    console.log(exc_B_avgtradevolumefor_month);
    console.log(exc_C_avgtradevolumefor_month);
    exc_A_avgtradevolumefor_month = (typeof exc_A_avgtradevolumefor_month[0].volume != 'undefined')?exc_A_avgtradevolumefor_month[0].volume:0;
    exc_B_avgtradevolumefor_month = (typeof exc_B_avgtradevolumefor_month[0].volume != 'undefined')?exc_B_avgtradevolumefor_month[0].volume:0;
    exc_C_avgtradevolumefor_month = (typeof exc_C_avgtradevolumefor_month[0].volume != 'undefined')?exc_C_avgtradevolumefor_month[0].volume:0;

    console.log(exc_A_avgtradevolumefor_month);
    console.log(exc_B_avgtradevolumefor_month);
    console.log(exc_C_avgtradevolumefor_month);
    // return false;

    // var exc_A_avgtradevolumefor_month =  2722988075;
    // var exc_B_avgtradevolumefor_month =  6031867375;
    // var exc_C_avgtradevolumefor_month =  3951095106;

    Totalexcvol = exc_A_avgtradevolumefor_month + exc_B_avgtradevolumefor_month + exc_C_avgtradevolumefor_month;
    //Weightage calculation

    var Wt_A = parseFloat(exc_A_avgtradevolumefor_month) / parseFloat(Totalexcvol);
    var Wt_B = parseFloat(exc_A_avgtradevolumefor_month) / parseFloat(Totalexcvol);
    var Wt_C = parseFloat(exc_A_avgtradevolumefor_month) / parseFloat(Totalexcvol);

    var exchange_A_ticker = await getspotprice(pair,'Bitstamp');
    var exchange_B_ticker = await getspotprice(pair,'Kraken');
    var exchange_C_ticker = await getspotprice(pair,'Coinbasepro');

    console.log(exchange_A_ticker,'indexprice_calculation');
    // return false;
    // console.log(exchange_B_ticker,'ticker details');
    var exchange_A_spotprice = exchange_A_ticker?exchange_A_ticker.last:0;
    var exchange_B_spotprice = exchange_B_ticker?exchange_B_ticker.last:0;
    var exchange_C_spotprice = exchange_C_ticker?exchange_C_ticker.last:0;

    console.log(exchange_A_spotprice,'A price');
    console.log(exchange_B_spotprice,'B price');
    console.log(exchange_C_spotprice,'C price');

    var total = Wt_A+Wt_B+Wt_C;

    var awei = (Wt_A/total)*100;
    var bwei = (Wt_B/total)*100;
    var cwei = (Wt_C/total)*100;


    var EBTC  =  parseFloat(awei/100) * parseFloat(exchange_A_spotprice) + parseFloat(bwei/100) * parseFloat(exchange_B_spotprice) + parseFloat(cwei/100) * parseFloat(exchange_C_spotprice);

    var pricespreadofA = Math.abs(parseFloat(exchange_A_spotprice) - parseFloat(EBTC));
    var pricespreadofB = Math.abs(parseFloat(exchange_B_spotprice) - parseFloat(EBTC));
    var pricespreadofC = Math.abs(parseFloat(exchange_C_spotprice) - parseFloat(EBTC));


    var asquare = 1/(parseFloat(pricespreadofA) * parseFloat(pricespreadofA));
    var bsquare = 1/(parseFloat(pricespreadofB) * parseFloat(pricespreadofB));
    var csquare = 1/(parseFloat(pricespreadofC) * parseFloat(pricespreadofC));

    var totalsquar = parseFloat(asquare) + parseFloat(bsquare) + parseFloat(csquare);

    var weight_A   =   parseFloat(asquare) / parseFloat(totalsquar);
    var weight_B   =   parseFloat(bsquare) / parseFloat(totalsquar);
    var weight_C   =   parseFloat(csquare) / parseFloat(totalsquar);


    var index_price  =  (parseFloat(weight_A) * parseFloat(exchange_A_spotprice)) + (parseFloat(weight_B) * parseFloat(exchange_B_spotprice)) + (parseFloat(weight_C) * parseFloat(exchange_C_spotprice))

      console.log(index_price,'indexprice');
    var Interest_Quote_Index = 1/100;
    var Interest_Base_Index  = 0.25/100;
    var fundingrate          = (Interest_Quote_Index*Interest_Base_Index)/3;

    var timeuntillfunding    = 5;  //need to calculate
    var fundinbasis          = fundingrate * (timeuntillfunding/3);
    var mark_price           = index_price * (1 + fundinbasis);

    console.log(mark_price,'mark price');

    if(typeof index_price != 'undefined' && typeof mark_price != 'undefined')
    {
        perpetual.findOneAndUpdate({tiker_root:pair},{"$set": {"markprice":mark_price,"index_price":index_price} } , {new:true,"fields": {tiker_root:1,last:1,markprice:1} } ,function(pererr,perdata){
            console.log(perdata);
            io.emit('PRICEDETAILS', perdata);
        });
    }

    var Interest_Quote_Index = 1/100;
    var Interest_Base_Index  = 0.25/100;
    // var mark_price        = 9260

    var Interest_Rate        = ((Interest_Quote_Index-Interest_Base_Index)/3) * 100;

    var imapactbidprice      = await getimpactbidprice('buy',pair);
    var imapactaskprice      = await getimpactbidprice('sell',pair);
    console.log(imapactbidprice,'imapactbidprice');

    var exchange_A_ticker    = await make_api_call(pair,'Bitstamp');
    var exchange_A_spotprice = exchange_A_ticker?exchange_A_ticker.last:0;

    if(typeof imapactbidprice=='undefined' && imapactbidprice.length>0 && typeof imapactbidprice[0].price == 'undefined')
    {
      return false;
    }
    imapactbidprice          = (imapactbidprice.length>0)?imapactbidprice[0].price:0;
    imapactaskprice          = (imapactaskprice.length>0)?imapactaskprice[0].price:0;

    console.log(imapactbidprice);
    console.log(imapactaskprice);

    var midprice      = Math.round((imapactbidprice+imapactaskprice)/2);
    console.log(midprice,'midprice');
    var premium_index = Math.max(0, parseFloat(imapactbidprice) - parseFloat(mark_price)) -  Math.max(0, parseFloat(mark_price) - parseFloat(imapactaskprice))/exchange_A_spotprice;
    console.log(premium_index,'premium_index');

    var fairbasis     = (parseFloat(midprice)/parseFloat(index_price)-1)/(30/365);
    console.log(fairbasis,'fairbasis');
    premium_index     = parseFloat(premium_index) + fairbasis;
    console.log(premium_index,'premium_index');
    console.log(Interest_Rate,'Interest_Rate');

    const clamp = (min, max) => (value) =>
      value < min ? min : value > max ? max : value;

    var Funding_Rate = premium_index + clamp(0.05, -0.05)(Interest_Rate - premium_index);
    console.log(Funding_Rate,'Funding_Rate');
    if(typeof Funding_Rate != 'undefined' && typeof mark_price != 'undefined')
    {
        perpetual.findOneAndUpdate({tiker_root:pair},{"$set": {"funding_rate":Funding_Rate,"markprice":mark_price,"index_price":index_price} } , {new:true,"fields": {tiker_root:1,last:1,markprice:1} } ,function(pererr,perdata){
            console.log(perdata);
        });
    }

}
function updatefunction(payerid,receiverid,amount,firstCurrency)
{
  updatebaldata["balance"] = amount;
    Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(receiverid)},{"$inc": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
    });

    Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(payerid)},{"$dec": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){

    });
}
async function calculatingfun(tradeDetails,pairDetails)
{
    if(tradeDetails.length>0)
    {
      var buyuserIdarr  = [];
      var selluserIdarr = [];
      var sellIdarr     = [];
      var buyIdarr      = [];
      var pairarr       = [];
      for(var i=0; i<tradeDetails.length; i++)
      {
        var quantity      = tradeDetails[i].quantity;
        var price         = tradeDetails[i].price;
        var markprice     = tradeDetails[i].pair.markprice;
        var funding_rate  = tradeDetails[i].pair.funding_rate;
        var filled        = tradeDetails[i].pair.filled;
        var leverage      = tradeDetails[i].pair.leverage;
        var firstCurrency = tradeDetails[i].pair.firstCurrency;

        if(filled)
        {
          for(var j=0; j<filled.length; j++)
          {
              var buyuserId    = filled[j].buyuserId;
              var selluserId   = filled[j].selluserId;
              var sellId       = filled[j].sellId;
              var buyId        = filled[j].buyId;
              var pair         = filled[j].pair;
              var pairname     = filled[j].pairname;
              var filledAmount = filled[j].filledAmount;
              var Price        = filled[j].Price;
              if(sellIdarr.includes(sellId) && buyIdarr.includes(buyId) && pairarr.includes(pair) && buyuserIdarr.includes(buyuserId) && selluserIdarr.includes(selluserId))
              {
                continue;
              }
              else
              {
                  sellIdarr.push(sellId);
                  buyIdarr.push(buyId);
                  pairarr.push(pair);
                  buyuserIdarr.push(buyuserId);
                  selluserIdarr.push(selluserId);
                  var index = pairDetails.findIndex(x => (x.tiker_root) === pairname);
                  if(index!=-1)
                  {
                    var markprice      = pairDetails[index].markprice;
                    var funding_rate   = pairDetails[index].funding_rate;
                    var position_value = quantity / markprice;
                    var fundingamount  = position_value * (funding_rate/100);
                    if(funding_rate>=0)
                    {
                        await updatefunction(buyuserId,selluserId,fundingamount,firstCurrency);
                    }
                    else
                    {
                        await updatefunction(selluserId,buyuserId,fundingamount,firstCurrency);
                    }
                  }

              }


          }

        }
      }
    }
}
async function fundingAmount()
{
    console.log('funding amourn');
    perpetual.find({},{tiker_root,funding_rate,markprice}).populate('pair').
    exec(function (pairerr, pairDetails) {
        if(pairDetails)
        {
            tradeTable.find({status:'1'}).populate('pair').exec(function (err, tradeDetails) {
                calculatingfun(tradeDetails,pairDetails);
            });
        }
    });
}

router.get('/apitest', (req, res) => {
// console.log('sdjfsjdflsjdf');
//   var sDate =  "1970-01-01 00:00:0.000Z";
//   var eDate = "2020-03-09 05:21:16.843Z";
//   tradeTable.aggregate([
//       {
//         $unwind : '$filled'
//       },
//       {
//       $match : {
//           "filled.created_at": {
//           // "$gte": new Date(sDate),
//            "$lt": new Date(eDate)
//          },
//           }
//       },
//   ]).exec(function(err,result){
//   console.log(err);
//   console.log(result);
//   });
  perpetual.find({},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){

 }).then((contractdetails)=> {

processUsers(contractdetails,'Bitstamp');
processUsers(contractdetails,'Kraken');
processUsers(contractdetails,'Coinbasepro');
processUsers(contractdetails,'Gemini');
  });

});

router.get('/markets', (req,res) => {
  perpetual.aggregate([
     {
      $project : {
        _id : 0,
        name : "$tiker_root",
        type : "crypto",
        exchange : "Alwin",
      }
    }
    ]).exec(function(err,pairdata){
    res.json(pairdata);
  });
});

router.get('/chart/:config', (request,response) => {
  var uri = url.parse(request.url, true);
  // console.log(uri.query,'querydfjsldjflsdjflsdkfjldfs');
  var action = uri.pathname;
  // console.log(action,'action');
  switch (action) {
    case '/chart/config':
      action = '/config';
      break;
    case '/chart/time':
      action = '/time';
      break;
    case '/chart/symbols':
      symbolsDatabase.initGetAllMarketsdata();
      action = '/symbols';
      break;
    case '/chart/history':
      action = '/history';
      break;
  }
  return requestProcessor.processRequest(action, uri.query, response);
});

router.get('/chartData', (req,res) =>{
// console.log('callhere chart');
  var url              = require('url');
  var url_parts        = url.parse(req.url, true);
  var query            = url_parts.query;


  var pair       = req.query.market;
  var start_date = req.query.start_date;
  var end_date   = req.query.end_date;
  var resol      = req.query.resolution;
  var spl        = pair.split("_");
  var first      = spl[0];
  var second     = spl[1];
  var pattern    = /^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/;
  var _trProject;
  var _trGroup;
  var _exProject;
  var _exGroup;
  if(start_date)
  {
    if(!pattern.test(start_date))
    {
        res.json({"message" : "Start date is not a valid format"});
        return false;
    }
  }
  else
  {
        res.json({"message" : "Start date parameter not found"});
        return false;
  }
  if(end_date)
  {
    if(!pattern.test(end_date))
    {
        res.json({"message" : "End date is not a valid format"});
        return false;
    }
  }
  else
  {
        res.json({"message" : "End date parameter not found"});
        return false;
  }
    var sDate = start_date+' 00:00:0.000Z';
    var eDate = end_date+' 00:00:0.000Z';
    // console.log(start_date,'start_date');
    // console.log(end_date,'end_date');

    // console.log(sDate,'start_date');
    // console.log(eDate,'end_date');
    if(sDate > eDate){
      res.json({"message" : "Please ensure that the End Date is greater than or equal to the Start Date"});
     }
    perpetual.find({tiker_root:pair}).select("_id").select("tiker_root").exec(function(err,pairdata){
      try
      {
        if(pairdata.length > 0)
        {
          var pairId   = pairdata[0]._id;
          var pairname = pairdata[0].tiker_root;
          // console.log(pairname);
          var limits ;
                  var project = {Date : "$Date",pair : "$pair",low : "$low",high : "$high",open : "$open",close : "$close",volume : "$volume",exchange : "GlobalCryptoX"};


              if(resol)
              {
                if(resol!=1 && resol!=5 && resol!=15 && resol!=30 && resol!=60 && resol!='d' && resol!='2d' && resol!='3d' && resol!='w' && resol!='1w' && resol!='3w' && resol!='m' && resol!='6m')
                {
                  res.json({"message" : "Resolution value is not valid"});
                  return false;
                }
                else
                {
                 if(resol == 'd'){
                  resol = 1440;
                   _trProject = {
                      "yr": {
                          "$year": "$filled.created_at"
                      },
                      "mn": {
                          "$month": "$filled.created_at"
                      },
                      "dt": {
                          "$dayOfMonth": "$filled.created_at"
                      },
                       "hour": {
                           "$hour": "$filled.created_at"
                       },
                       "minute": { "$minute": "$filled.created_at" },
                      "filled.filledAmount" : 1,
                      "filled.Price" : 1,
                      pair : "$filled.pairname",
                      modifiedDate : '$filled.created_at',
                    }
                    _trGroup = {
                      "_id": {
                          "year": "$yr",
                          "month": "$mn",
                          "day": "$dt",
                          "hour": "$hour",
                          "minute": {
                                 $add:
                                      [
                                         {$subtract:
                                         [
                                            {$minute: "$modifiedDate"},
                                            {$mod: [{$minute: "$modifiedDate"}, +resol]}
                                         ]},
                                         +resol
                                      ]
                            }
                          },
                        count: {
                          "$sum": 1
                      },
                      Date : { $last : "$modifiedDate" },
                      pair : { $first : '$pair' },
                      low : { $min : '$filled.Price' },
                      high : { $max : '$filled.Price' },
                      open : { $first : '$filled.Price' },
                      close : { $last : '$filled.Price' } ,
                      volume : { $sum : '$filled.filledAmount' }

                    }

                  }
                  else if(resol == 'w')
                  {
                    _trProject = {
                         "week": { "$week": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : '$filled.pairname',
                        modifiedDate : '$filled.created_at',
                    }
                    _trGroup = {
                      "_id": {
                          "week": "$week",
                          },
                        count: {
                          "$sum": 1
                      },
                      Date : { $last : "$modifiedDate" },
                      pair : { $first : '$pair' },
                      low : { $min : '$filled.Price' },
                      high : { $max : '$filled.Price' },
                      open : { $first : '$filled.Price' },
                      close : { $last : '$filled.Price' } ,
                      volume : { $sum : '$filled.filledAmount' }
                    }

                    resol = 10080;
                  }
                  else if(resol == 'm')
                  {

                      _trProject = {
                      "month": { "$month": "$filled.created_at" },
                      "filled.filledAmount" : 1,
                      "filled.Price" : 1,
                      pair : "$filled.pairname",
                      modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                            "month": "$month",
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$filled.created_at" },
                        pair : { $first : '$filled.pairname' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }
                      }

                      resol = 43200;
                  }
                  else if(resol == 1)
                  {
                     resol = 1440;
                     _trProject = {
                         "yr": {
                            "$year": "$filled.created_at"
                        },
                        "mn": {
                            "$month": "$filled.created_at"
                        },
                        "dt": {
                            "$dayOfMonth": "$filled.created_at"
                        },
                         "hour": { "$hour": "$filled.created_at" },
                         "minute": { "$minute": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : "$filled.pairname",
                        modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                            "year": "$yr",
                            "month": "$mn",
                            "day": "$dt",
                            "hour": "$hour",
                            "minute": "$minute"
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$modifiedDate" },
                        pair : { $first : '$pair' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }

                      }
                  }
                  else if(resol == 5)
                  {
                     resol = 1440;
                     _trProject = {
                      "yr": {
                            "$year": "$filled.created_at"
                        },
                        "mn": {
                            "$month": "$filled.created_at"
                        },
                        "dt": {
                            "$dayOfMonth": "$filled.created_at"
                        },
                         "hour": { "$hour": "$filled.created_at" },
                         "minute": { "$minute": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : "filled.pairname",
                        modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                            "year": "$yr",
                            "month": "$mn",
                            "day": "$dt",
                            "hour": "$hour",
                            "minute": {
                                "$subtract": [
                                  { "$minute": "$modifiedDate" },
                                  { "$mod": [{ "$minute": "$modifiedDate"}, 5] }
                                ]
                              }
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$modifiedDate" },
                        pair : { $first : '$pair' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }

                      }
                  }
                  else if(resol == 30)
                  {
                     resol = 1440;
                     _trProject = {
                         "yr": {
                            "$year": "$filled.created_at"
                        },
                        "mn": {
                            "$month": "$filled.created_at"
                        },
                        "dt": {
                            "$dayOfMonth": "$filled.created_at"
                        },
                         "hour": { "$hour": "$filled.created_at" },
                         "minute": { "$minute": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : "$filled.pairname",
                        modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                             "year": "$yr",
                            "month": "$mn",
                            "day": "$dt",
                            "hour": "$hour",
                            "minute": {
                                "$subtract": [
                                  { "$minute": "$modifiedDate" },
                                  { "$mod": [{ "$minute": "$modifiedDate"}, 30] }
                                ]
                              }
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$modifiedDate" },
                        pair : { $first : '$pair' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }

                      }
                  }
                  else if(resol == 60)
                  {
                     resol = 1440;
                     _trProject = {
                      "yr": {
                            "$year": "$filled.created_at"
                        },
                        "mn": {
                            "$month": "$filled.created_at"
                        },
                        "dt": {
                            "$dayOfMonth": "$filled.created_at"
                        },
                         "hour": { "$hour": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : "$filled.pairname",
                        modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                          "year": "$yr",
                            "month": "$mn",
                            "day": "$dt",
                            "hour": "$hour"
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$modifiedDate" },
                        pair : { $first : '$pair' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }

                      }
                  }
                  else if(resol == 15)
                  {
                     resol = 1440;
                     _trProject = {
                         "yr": {
                            "$year": "$filled.created_at"
                        },
                        "mn": {
                            "$month": "$filled.created_at"
                        },
                        "dt": {
                            "$dayOfMonth": "$filled.created_at"
                        },
                         "hour": { "$hour": "$filled.created_at" },
                         "minute": { "$minute": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : "$filled.pairname",
                        modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                            "year": "$yr",
                            "month": "$mn",
                            "day": "$dt",
                            "hour": "$hour",
                            "minute": {
                                "$subtract": [
                                  { "$minute": "$modifiedDate" },
                                  { "$mod": [{ "$minute": "$modifiedDate"}, 15] }
                                ]
                              }
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$modifiedDate" },
                        pair : { $first : '$pair' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }

                      }
                  }
                  else
                  {
                     resol = 1440;
                     _trProject = {
                        "yr": {
                            "$year": "$filled.created_at"
                        },
                        "mn": {
                            "$month": "$filled.created_at"
                        },
                        "dt": {
                            "$dayOfMonth": "$filled.created_at"
                        },
                         "hour": {
                             "$hour": "$filled.created_at"
                         },
                         "minute": { "$minute": "$filled.created_at" },
                        "filled.filledAmount" : 1,
                        "filled.Price" : 1,
                        pair : "$filled.pairname",
                        modifiedDate : '$filled.created_at',
                      }
                      _trGroup = {
                        "_id": {
                            "year": "$yr",
                            "month": "$mn",
                            "day": "$dt",
                            "hour": "$hour",
                            "minute": {
                                   $add:
                                        [
                                           {$subtract:
                                           [
                                              {$minute: "$modifiedDate"},
                                              {$mod: [{$minute: "$modifiedDate"}, +resol]}
                                           ]},
                                           +resol
                                        ]
                              }
                            },
                          count: {
                            "$sum": 1
                        },
                        Date : { $last : "$modifiedDate" },
                        pair : { $first : '$pair' },
                        low : { $min : '$filled.Price' },
                        high : { $max : '$filled.Price' },
                        open : { $first : '$filled.Price' },
                        close : { $last : '$filled.Price' } ,
                        volume : { $sum : '$filled.filledAmount' }

                      }

                  }
                }
              }
              // console.log(end_date,'eDate');
              // console.log(moment(end_date).format(),'eDate');
               tradeTable.aggregate([
                {
                  $unwind : '$filled'
                },
                {
                  $match : {
                    "filled.pair": ObjectId(pairId),
                    "filled.status":"filled",
                    "filled.created_at": {
                       "$lt": new Date(moment(end_date).format()),
                      "$gte": new Date(moment(start_date).format())
                     },
                  }
                },
                {
                  $project : _trProject
                },
                {
                  "$group": _trGroup
              },
               {
                  $project : project,
                },
                {
                  $sort: {
                    "filled.created_at": 1,

                  }
                }
              ]).exec(function(err,result){
                  res.json(result);
                });

        }
        else
        {
            // console.log("no pair data");
            res.json({"message" : "No Pair Found"});
        }
      }
      catch (e)
      {
        console.log("no pair",e);
      }
      // console.log(pairdata);
    });

});


module.exports = router;
