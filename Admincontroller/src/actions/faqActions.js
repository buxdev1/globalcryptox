import axios from "axios";
import {
    GET_ERRORS,
    FAQ_ADD,
    FAQ_UPDATE
} from "./types";
import keys from "./config";
const url = keys.baseUrl;
export const addFaq = (faqData) => dispatch => {
    axios
        .post(url+"api/faq-add", faqData)
        .then(res =>
            dispatch({
                type: FAQ_ADD,
                payload: res,
            })
        ).catch(err =>
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};


export const updateFaq = (faqData) => dispatch => {
    axios
        .post(url+"api/faq-update", faqData)
        .then(res =>
            dispatch({
                type: FAQ_UPDATE,
                payload: res,
            })
        ).catch(err =>
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};
