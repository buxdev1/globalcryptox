import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import {
    GET_ERRORS,
    SET_CURRENT_USER,
    FORGOT,
    USER_LOADING,

} from "./types";
import keys from "./config";
const url = keys.baseUrl;
export const registerUser = (userData, history) => dispatch => {
    axios
        .post(url+"api/adminadd", userData)
        .then(res => history.push("/login"))
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

export const loginUser = userData => dispatch => {
    axios
        .post(url+"api/adminlogin", userData)
        .then(res => {
            const { token } = res.data;
            localStorage.setItem("jwtToken", token);
            setAuthToken(token);
            const decoded = jwt_decode(token);
            dispatch(setCurrentUser(decoded));
        })
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

export const Otplogin = phonenumberdata => dispatch => {
    axios
        .post(url+"api/otplogin", phonenumberdata)
        .then(res => {
            const { token } = res.data;
            localStorage.setItem("jwtToken", token);
            setAuthToken(token);
            const decoded = jwt_decode(token);
            dispatch(setCurrentUser(decoded));
        })
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
};

export const forgotUser = (forgotData,history) => dispatch => {
  axios
      .post(url+"api/controforgot", forgotData)
      .then(res =>
          dispatch({
              type: FORGOT,
              payload: res,
          })
      ).catch(err =>
          dispatch({
              type: GET_ERRORS,
              payload: err.response.data
          })
      );
 };

export const resetChangepattern = (resetData,history) => dispatch => {
  axios
      .post(url+"api/resetChangepattern", resetData)
      .then(res =>
          dispatch({
              type: FORGOT,
              payload: res,
          })
      ).catch(err =>
          dispatch({
              type: GET_ERRORS,
              payload: err.response.data
          })
      );
 };

export const setCurrentUser = decoded => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    };
};

export const setUserLoading = () => {
    return {
        type: USER_LOADING
    };
};

export const logoutUser = () => dispatch => {
    localStorage.removeItem("jwtToken");
    setAuthToken(false);
    dispatch(setCurrentUser({}));
};
