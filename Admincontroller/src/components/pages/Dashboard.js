import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import {Link} from "react-router-dom";
import {faUserAlt} from "@fortawesome/free-solid-svg-icons/faUserAlt";
import TradingViewWidget, { Themes } from 'react-tradingview-widget';
import keys from "../../actions/config";
import axios from "axios";
var CanvasJSReact = require('../../canvasjs.react');
const url = keys.baseUrl;
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      btcbalance: 0,
      ltcbalance: 0,
      bchbalance: 0,
      xrpbalance: 0,
      ethbalance: 0,

      usdtbalance: 0,
      spcbalance: 0,
      busdbalance: 0,
      scnbalance: 0,
      depositamount:"",
      currentRecord: {
        id: "",
        categoryName: "",
        status: ""
      },
      errors: {}
    };

    // this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.getData('BTC');
    this.getData('LTC');
    this.getData('BCH');
    this.getData('XRP');
    this.getData('ETH');
    this.gettokendata('USDT');
    this.gettokendata('SPC');
    this.gettokendata('BUSD');
    this.gettokendata('SCN');
  }
  gettokendata(cur){
    axios
      .post(url + "api/getlivetokenbalance",{currency:cur})
      .then(res => {
        if(res.data.status)
        {
          if(cur == 'USDT'){ this.setState({ usdtbalance : res.data.result }); }
          if(cur == 'SPC'){ this.setState({ spcbalance : res.data.result }); }
          if(cur == 'BUSD'){ this.setState({ busdbalance : res.data.result }); }
          if(cur == 'SCN'){ this.setState({ scnbalance : res.data.result }); }
        }
      })
      .catch();
  }

  getData(cur) {
    axios
      .post(url + "api/getlivebalance",{currency:cur})
      .then(res => {
        if(res.data.status)
        {
          if(cur == 'BTC'){ this.setState({ btcbalance : res.data.result }); }
          if(cur == 'LTC'){ this.setState({ ltcbalance : res.data.result }); }
          if(cur == 'BCH'){ this.setState({ bchbalance : res.data.result }); }
          if(cur == 'XRP'){ this.setState({ xrpbalance : res.data.result }); }
          if(cur == 'ETH'){ this.setState({ ethbalance : res.data.result }); }

        }
      })
      .catch();
    //  console.log(this.props,'authget1');
  }
    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {

        //const { user } = this.props.auth;
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <button className="btn" id="menu-toggle"><FontAwesomeIcon icon={faList}/></button>
                            <h3 className="mt-2 text-secondary">Dashboard</h3>
                            <div className="row px-2">
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor : "cornflowerblue"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Ethereum</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.ethbalance).toFixed(8)}.</p>
                                              </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "cadetblue"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Litecoin</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.ltcbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "grey"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Bitcoin</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.btcbalance).toFixed(8)}.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "red"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Ripple</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.xrpbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "violet"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">BitcoinCash</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.bchbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "violet"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">USDT Balance</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.usdtbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "violet"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">SPC Balance</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.spcbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "violet"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">BUSD balance</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.busdbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "violet"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">SCN Balance</h5>
                                            <p className="card-text">Balance : {parseFloat(this.state.scnbalance).toFixed(8)}.</p>
                                            </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

Dashboard.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(Dashboard);
