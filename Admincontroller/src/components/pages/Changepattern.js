import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import classnames from "classnames";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import { updateChangepattern } from "../../actions/userActions";
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer} from "react-toastify";
import keys from "../../actions/config";
import { withRouter } from "react-router-dom";
import PatternLock from "react-pattern-lock";
const url = keys.baseUrl;
class Changepattern extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id : "",
            oldpattern: [],
            pattern: [],
            pattern2: [],
            errors: {},
        };
    }

    componentDidMount() {

    };

     componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
        if (nextProps.auth !== undefined
            && nextProps.auth.Changepassword !== undefined
            && nextProps.auth.Changepassword.data !== undefined
            && nextProps.auth.Changepassword.data.message !== undefined) {
            toast(nextProps.auth.Changepassword.data.message, {
                position: toast.POSITION.TOP_CENTER
            });
            nextProps.auth.Changepassword = undefined;
        }
    }
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };

     onChangepatternUpdate = e => {
        e.preventDefault();
        const updatechangepattern = {
            _id: this.props.auth.user.id,
            oldpattern: this.state.oldpattern.join(""),
            pattern: this.state.pattern.join(""),
            pattern2: this.state.pattern2.join("")
        };
        this.props.updateChangepattern(updatechangepattern);
    };
    onReset = () => {
      this.setState({
        oldpattern: [],
        success: false,
        error: false,
        disabled: false
      });
    };

    onReset1 = () => {
      this.setState({
        pattern: [],
        success: false,
        error: false,
        disabled: false
      });
    };

    onReset2 = () => {
      this.setState({
        pattern2: [],
        success: false,
        error: false,
        disabled: false
      });
    };

    render() {
        const { errors } = this.state;
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <button className="btn mt-3" id="menu-toggle"><FontAwesomeIcon icon={faList}/>
                            </button>
                            <h3 className="mt-2 text-secondary">Change Pattern</h3>
                            <form noValidate onSubmit={this.onChangepatternUpdate} id="update-Changepassword">
                                    <div className="row mt-2">
                                        <div className="col-md-4 text-center">
                                          <label htmlFor="name">Old pattern</label>
                                            <PatternLock
                                              width={ 300 }
                                              pointSize={ 15 }
                                              size={ 3 }
                                              path={ this.state.oldpattern }
                                              onChange={ (pattern) => {
                                                  this.setState({ oldpattern : pattern });
                                              }}
                                              onFinish={() => {
                                                console.log("patternn",this.state.oldpattern.join(""))
                                                  // check if the pattern is correct
                                              }}
                                          />
                                            <span className="text-danger">{errors.oldpattern}</span>
                                            <button className="btn btnBlue"
                                               onClick={this.onReset}
                                             >
                                               Click here to reset
                                             </button>
                                        </div>

                                   
                                        <div className="col-md-4 text-center">
                                          <label htmlFor="password">New Pattern</label>
                                        <PatternLock
                                          width={ 300 }
                                          pointSize={ 15 }
                                          size={ 3 }
                                          path={ this.state.pattern }
                                          onChange={ (pattern) => {
                                              this.setState({ pattern : pattern });
                                          }}
                                          onFinish={() => {
                                            console.log("pattern",this.state.pattern.join(""))
                                              // check if the pattern is correct
                                          }}
                                      />
                                            <span className="text-danger">{errors.pattern}</span>
                                            <button className="btn btnBlue"
                                               onClick={this.onReset1}
                                             >
                                               Click here to reset
                                             </button>
                                        </div>
                                  
                                        <div className="col-md-4 text-center">
                                           <label htmlFor="password2">Confirm pattern</label>
                                        <PatternLock
                                          width={ 300 }
                                          pointSize={ 15 }
                                          size={ 3 }
                                          path={ this.state.pattern2 }
                                          onChange={ (pattern) => {
                                              this.setState({ pattern2 : pattern });
                                          }}
                                          onFinish={() => {
                                            console.log("pattern",this.state.pattern2.join(""))
                                              // check if the pattern is correct
                                          }}
                                      />
                                            <span className="text-danger">{errors.pattern2}</span>
                                            <button className="btn btnBlue"
                                               onClick={this.onReset2}
                                             >
                                               Click here to reset
                                             </button>
                                        </div>
                                    </div>

                                </form>
                                    <p className="text-center mt-4">
                                <button
                                    form="update-Changepassword"
                                    type="submit"
                                    className="btn btnYellow">
                                    Update Pattern
                                </button>
                                </p>
                         </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}

Changepattern.propTypes = {
    updateChangepattern: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { updateChangepattern }
)(withRouter(Changepattern));
