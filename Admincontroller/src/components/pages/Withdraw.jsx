import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList } from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from "@ashvin27/react-datatable";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer } from "react-toastify";
import $ from "jquery";
import Select from "react-select";
import {Modal,Button} from 'react-bootstrap/';

import keys from "../../actions/config";
import axios from "axios";
const url = keys.baseUrl;
class Deposit extends Component {
  constructor(props) {
    super(props);
    this.state = {
    first_currency:{label:'ETH',value:'ETH'},
    withdrawamount:"",
    toaddress:"",
    otpnumber:"",
    show:false,
    errors: {}
    };

    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  componentWillReceiveProps(nextProps) {
    this.getData();
  }

  getData() {
    axios
      .get(url + "api/asset-data-first")
      .then(res => {
        console.log("datassss", res.data);
        var currencyarray = [];
        res.data.map((item, i) => {
          const name = item.currencySymbol;
          const value = item.currencySymbol;
          const obj = { value: name, label: value };
          currencyarray.push(obj);
        });
        this.setState({ first_currency1: currencyarray, email_assigned: true });
      })
      .catch();
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  handleChange = selectedOption => {
    this.setState({ first_currency: selectedOption   });
     // console.log(`Option selected:`, selectedOption );
  };

  handleClose = e => {
    this.setState({ show: false   });
  };

  pageChange(pageData) {
    console.log("OnPageChange", pageData);
  }

  submitwithdraw = e => {
    e.preventDefault();
    if(this.state.withdrawamount=='')
    {
      toast("Enter withdraw amount", {
          position: toast.POSITION.TOP_CENTER,
      });
      return false;
    }
    if(this.state.toaddress=='')
    {
      toast("Enter To address", {
          position: toast.POSITION.TOP_CENTER,
      });
      return false;
    }
    const inputdetails = {
      first_currency: this.state.first_currency.value,
      withdrawamount:this.state.withdrawamount,
    };
    axios
      .post(url + "api/sendotp")
      .then(res => {
        toast(res.data.message, {
            position: toast.POSITION.TOP_CENTER,
        })
        this.setState({ show: true   });
      })
      .catch();
  };

  confirmwithdraw = e => {
    e.preventDefault();
    if(this.state.otpnumber=='' || this.state.otpnumber.length != 6)
    {
      toast("OTP is not valid", {
          position: toast.POSITION.TOP_CENTER,
      });
      return false;
    }
    const inputdetails = {
      first_currency: this.state.first_currency.value,
      withdrawamount:this.state.withdrawamount,
      toaddress:this.state.toaddress,
      otpnumber:this.state.otpnumber,
    };
    axios
      .post(url + "api/checkotp",inputdetails)
      .then(res => {
        toast(res.data.message, {
            position: toast.POSITION.TOP_CENTER,
        })
        this.setState({ show: false,otpnumber:''   });
      })
      .catch();
  };

  render() {
    const { selectedOption } = this.state.first_currency;

    return (
      <div>
        <Navbar />
        <div className="d-flex" id="wrapper">
          <Sidebar />
          <div id="page-content-wrapper">
          <h3 class="mt-2 text-secondary">Withdraw section</h3>
            <div className="container-fluid mt-2">
              <form
                noValidate
                onSubmit={this.submitwithdraw}
                id="add-perpetual"
              >
                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="first_currency">Currency Name</label>
                  </div>
                  <div className="col-md-9">
                    <Select
                      value={this.state.first_currency}
                      onChange={this.handleChange}
                      options={this.state.first_currency1}
                    />
                    <span className="text-danger"></span>
                  </div>
                </div>

                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="currencyName">To address</label>
                  </div>
                  <div className="col-md-9">
                    <input
                      onChange={this.onChange}
                      value={this.state.toaddress}
                      id="toaddress"
                      className="form-control"
                      type="text"
                    />
                  </div>
                </div>
                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="currencyName">Withdraw Amount</label>
                  </div>
                  <div className="col-md-9">
                    <input
                      onChange={this.onChange}
                      value={this.state.withdrawamount}
                        className="form-control"
                      id="withdrawamount"
                      type="text"
                    />
                    <button
                  form="add-perpetual"
                  type="submit"
                  className="btn btnBlue mt-3"
                >
                  Withdraw
                </button>
                  </div>
                </div>
                <div>

              </div>
              </form>
            </div>
          </div>
          <ToastContainer />
          <Modal show={this.state.show} onHide={this.handleClose}  aria-labelledby="contained-modal-title-vcenter" centered>
           <Modal.Header closeButton>
             <Modal.Title>OTP Verification</Modal.Title>
           </Modal.Header>
           <Modal.Body>


               <div className="form-group">
                 <div className="row">
                   <div className="col-md-4">
                     <div className="checkbox pt-2"><label>OTP</label></div>
                   </div>
                   <div className="col-md-8">
                     <div className="def-number-input number-input safari_only">
                       <input className="quantity" min="0" name="otpnumber" id="otpnumber" value={this.state.otpnumber} type="number" onChange={this.onChange}/>

                     </div>

                   </div>
                 </div>
               </div>

           </Modal.Body>
           <Modal.Footer>
             <Button variant="secondary btnDefaultNewBlue" onClick={this.handleClose}>
               Cancel
             </Button>
             <Button variant="primary btnDefaultNew" onClick={this.confirmwithdraw}>
               Confirm
             </Button>
           </Modal.Footer>
         </Modal>
        </div>
      </div>
    );
  }
}

Deposit.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  records: state.records
});

export default connect(mapStateToProps)(Deposit);
