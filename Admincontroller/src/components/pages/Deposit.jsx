import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList } from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from "@ashvin27/react-datatable";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer } from "react-toastify";
import $ from "jquery";
import keys from "../../actions/config";
import Select from "react-select";
var QRCode = require('qrcode.react');

const url = keys.baseUrl;
class Deposit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      depositamount:"",
      assetdetails:[],
      first_currency:{label:'ETH',value:'ETH'},
      cryptoAddress:'',
      currentRecord: {
        id: "",
        categoryName: "",
        status: ""
      },
      errors: {}
    };

    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.getData();
    this.getData1();
  }

  componentWillReceiveProps(nextProps) {
    this.getData();
  }

  getData() {
    axios
      .get(url + "api/asset-data-first")
      .then(res => {
        console.log("datassss", res.data);
        var currencyarray = [];
        res.data.map((item, i) => {
          const name = item.currencySymbol;
          const value = item.currencySymbol;
          const obj = { value: name, label: value };
          currencyarray.push(obj);
        });
        // console.log(currencyarray,'currencyarray');
        this.setState({ first_currency1: currencyarray, email_assigned: true });
      })
      .catch();
    //  console.log(this.props,'authget1');
  }

  getData1() {
    axios
      .get(url + "api/getadminassetdetails")
      .then(res => {
        console.log("datassss", res);
        var currencyarray = [];
        var index = res.data.data.findIndex(x => (x.currencySymbol) ==='ETH');
        var cryptoAddress = index!=-1?res.data.data[index].currencyAddress:'';
        this.setState({ assetdetails: res.data.data,"cryptoAddress":cryptoAddress });
      })
      .catch();
    //  console.log(this.props,'authget1');
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  handleChange = selectedOption => {
    // console.log(selectedOption,'assetdtali');
    var index = this.state.assetdetails.findIndex(x => (x.currencySymbol) ===selectedOption.value);
    var cryptoAddress = index!=-1?this.state.assetdetails[index].currencyAddress:"";
    if(selectedOption.value=="USDT"|| selectedOption.value =="💲PC" || selectedOption.value=="SCN"||selectedOption.value=="BUSD" ){
      cryptoAddress="0x5BbCcf95E8a7F951803A3046c2B3A9F9BFC96e2f"
    }
    this.setState({ first_currency: selectedOption,cryptoAddress: cryptoAddress});
    //  console.log(`Option selected:`, selectedOption );
  };

  pageChange(pageData) {
    console.log("OnPageChange", pageData);
  }

  ondepositadd = e => {
    e.preventDefault();
    const newcontract = {
      first_currency: this.state.first_currency,
      depositamount:this.state.depositamount

    };
    console.log(newcontract);
    // this.props.addasset(newcontract);
  };

  render() {
    const { selectedOption } = this.state.first_currency;

    return (
      <div>
        <Navbar />
        <div className="d-flex" id="wrapper">
          <Sidebar />

          <div id="page-content-wrapper">
          <div>
          <h3 class="mt-2 text-secondary">Deposit section</h3>
          </div>
            <div className="container-fluid mt-2">
              <form
                noValidate
                onSubmit={this.ondepositadd}
                id="add-perpetual"
              >
                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="first_currency">Currency Name</label>
                  </div>
                  <div className="col-md-9">
                    <Select
                      value={this.state.first_currency}
                      onChange={this.handleChange}
                      options={this.state.first_currency1}
                    />
                    <span className="text-danger"></span>
                  </div>
                </div>

                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="currencyName">Deposit Address</label>
                  </div>
                  <div className="col-md-9">
                  {
                    (this.state.cryptoAddress)?
                    <QRCode value={this.state.cryptoAddress} />
                    :''
                  }
                  <div>
                  {this.state.cryptoAddress}

                  </div>
                  </div>
                </div>
                <div>

              </div>
              </form>
            </div>
          </div>
          <ToastContainer />
        </div>
      </div>
    );
  }
}

Deposit.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  records: state.records
});

export default connect(mapStateToProps)(Deposit);
