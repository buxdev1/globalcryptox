import React, { Component } from "react";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";
import {logoutUser} from "../../actions/authActions";
import {Link} from "react-router-dom";

class Sidebar extends Component {

    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        const { user } = this.props.auth.user;
        console.log(this.props.auth.user,'fsdlfsdlkfjsldkjflskdjflsdf');
        return (
            <div className="border-right h-100" id="sidebar-wrapper">

                <div className="list-group list-group-flush">
                    {
                    (this.props.auth.user.moderator=='1')?
                    <div>
                        <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                        <Link to="/chat" className="list-group-item list-group-item-action">Chat</Link>
                    </div> :
                    <div>
                        <Link to="/dashboard" className="list-group-item list-group-item-action">Dashboard</Link>
                        <Link to="/admindeposit" className="list-group-item list-group-item-action">Admin Deposit</Link>
                        <Link to="/adminwithdraw" className="list-group-item list-group-item-action">Admin Withdraw</Link>
                        <Link to="/Transactions/BTC" className="list-group-item list-group-item-action">BTC Transactions</Link>
                        <Link to="/Transactions/LTC" className="list-group-item list-group-item-action">LTC Transactions</Link>
                        <Link to="/Transactions/BCH" className="list-group-item list-group-item-action">BCH Transactions</Link>
                        <Link to="/Transactions/ETH" className="list-group-item list-group-item-action">ETH Transactions</Link>
                        <Link to="/Transactions/XRP" className="list-group-item list-group-item-action">XRP Transactions</Link>



                    </div>}

                    <button className="list-group-item list-group-item-action" onClick={this.onLogoutClick}>Logout <FontAwesomeIcon icon={faSignOutAlt} /></button>
                </div>
            </div>
        );
    }
}

Sidebar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(Sidebar);
