import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { forgotUser,resetChangepattern } from "../../actions/authActions";
import classnames from "classnames";
import PhoneInput from 'react-phone-number-input'
import axios from "axios";
import { store } from 'react-notifications-component';
import keys from "../../actions/config";
import setAuthToken from "../../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import PatternLock from "react-pattern-lock";
import { toast, ToastContainer} from "react-toastify";
import Logo from "../../images/Logo-small.png"
import '../../css/style-custom.css'

const url = keys.baseUrl;

class Forgot extends Component {
    constructor() {
        super();
        this.state = {
            phone: "",
            otp: "",
            email:"",
            pattern:[],
            pattern2:[],
            resetform: false,
            errors: {}
        };
    }

    componentDidMount() {

        if (this.props.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }
    };

    componentWillReceiveProps(nextProps) {
      console.log(nextProps.auth,'nextProps');
      if (nextProps.auth.forgot!== undefined && nextProps.auth.forgot.data!== undefined && nextProps.auth.forgot.data.success!== undefined && nextProps.auth.forgot.data.success && nextProps.auth.forgot.data.type!== undefined) {
        toast(nextProps.auth.forgot.data.message, {
          position: toast.POSITION.TOP_CENTER
        });
        nextProps.auth.forgot = '';
        this.props.history.push("/login");
        // this.setState({resetform:true})
        // console.log('shown here')
      }
        if (nextProps.auth.forgot!== undefined && nextProps.auth.forgot.data!== undefined && nextProps.auth.forgot.data.success!== undefined && nextProps.auth.forgot.data.success && nextProps.auth.forgot.data.type=== undefined) {
            // this.props.history.push("/dashboard");
            toast("OTP verified successfully", {
                position: toast.POSITION.TOP_CENTER
            });
            this.setState({resetform:true})
            nextProps.auth.forgot = '';
            // console.log('shown here')
        }


        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    onsendOtp = e => {
        e.preventDefault();
      console.log("phone",this.state.phone);
      // if(typeof this.state.phone != "undefined" && this.state.phone!=""){
        const phonenumberdata = {
          _id: this.props.auth.user.id,
          email: this.state.email
        };
        axios
            .post(url+"api/sendotpadminlogin",phonenumberdata)
            .then(res => {
              if(res.data.success){
                toast(res.data.message, {
                    position: toast.POSITION.TOP_CENTER
                });

                this.setState({errors: ""})
              }else{
                toast(res.data.message, {
                    position: toast.POSITION.TOP_CENTER
                });

              }
            })
            .catch();
      // }else{
      //   store.addNotification({
      //     title: "Error!",
      //     message: "Phone Number should not be empty",
      //     type: "danger",
      //     insert: "top",
      //     container: "top-right",
      //     animationIn: ["animated", "fadeIn"],
      //     animationOut: ["animated", "fadeOut"],
      //     dismiss: {
      //       duration: 1500,
      //       onScreen: true
      //     }
      //   });
      // }
     }


    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };

    onSubmit = e => {
        e.preventDefault();
        const userData = {
            // phone: this.state.phone,
            email:this.state.email,
            otp: this.state.otp,
        };
        this.props.forgotUser(userData);
    };

    onChangepatternUpdate = e => {
       e.preventDefault();
       const updatechangepattern = {
           email: this.state.email,
           pattern: this.state.pattern.join(""),
           pattern2: this.state.pattern2.join("")
       };
       this.props.resetChangepattern(updatechangepattern);
   };
   onReset = () => {
     this.setState({
       oldpattern: [],
       success: false,
       error: false,
       disabled: false
     });
   };

   onReset1 = () => {
     this.setState({
       pattern: [],
       success: false,
       error: false,
       disabled: false
     });
   };

   onReset2 = () => {
     this.setState({
       pattern2: [],
       success: false,
       error: false,
       disabled: false
     });
   };

    render() {
        const { errors } = this.state;
        return (
            <div className="container">
                <div className="row mt-5">
                    <div className="col-md-5 mx-auto mt-0 mb-4 card shadow-lg">
                        <div className="card-body p-1 text-center">
                        <img className="text-center text-primary mt-3" src={Logo} />
                            <h2 className="text-center titleLogin mt-4 mb-3">Forgot Pattern</h2>

                            {this.state.resetform?
                              <form noValidate onSubmit={this.onChangepatternUpdate} id="update-Changepassword">

                                      <div className="row mt-2">
                                          <div className="">
                                              <label htmlFor="password" style={{"margin":"0 auto"}}>New Pattern</label>
                                          </div>

                                          <PatternLock
                                            width={ 300 }
                                            pointSize={ 15 }
                                            size={ 3 }
                                            path={ this.state.pattern }
                                            onChange={ (pattern) => {
                                                this.setState({ pattern : pattern });
                                            }}
                                            onFinish={() => {
                                              console.log("pattern",this.state.pattern.join(""))
                                                // check if the pattern is correct
                                            }}
                                            style={{"margin":"0 auto"}}
                                        />
                                              <span className="text-danger">{errors.pattern}</span>

                                      </div>
                                      <div className="row mt-2">
                                          <div className="col-md-3">

                                          </div>
                                          <div className="col-md-6">
                                          <p>
                                          <button className="btn btnBlue"
                                          onClick={this.onReset1}
                                          >
                                          Click here to reset
                                          </button>
                                          </p>

                                          </div>

                                      </div>
                                      <div className="row mt-2">
                                          <div className="col-md-3">
                                              <label htmlFor="password2">Confirm pattern</label>
                                          </div>

                                          <PatternLock
                                            width={ 300 }
                                            pointSize={ 15 }
                                            size={ 3 }
                                            style={{"margin":"0 auto"}}
                                            path={ this.state.pattern2 }
                                            onChange={ (pattern) => {
                                                this.setState({ pattern2 : pattern });
                                            }}
                                            onFinish={() => {
                                              console.log("pattern",this.state.pattern2.join(""))
                                                // check if the pattern is correct
                                            }}
                                        />
                                              <span className="text-danger">{errors.pattern2}</span>

                                      </div>

                                      <div className="row mt-2">
                                          <div className="col-md-3">

                                          </div>
                                          <div className="col-md-6">
                                           <p>
                                          <button className="btn btnBlue"
                                          onClick={this.onReset2}
                                          >
                                          Click here to reset
                                          </button>
                                          </p>
                                          </div>

                                      </div>


                                      <br />
                                      <button
                                      form="update-Changepassword"
                                      type="submit"
                                      className="btn btn-primary">
                                      Reset Pattern
                                      </button>
                                  </form>
                              :

                            <form noValidate onSubmit={this.onSubmit} className="white">
                                <div className="form-group input-group mobNumber text-center">
                            {/*}    <label>Mobile Number</label>
                                <PhoneInput
                                  placeholder="Enter phone number"
                                  country={'IN'}
                                  value={this.state.phone}
                                  onChange={phone => this.setState({ phone })}
                                /> */}
                                <label htmlFor="email">Email</label>
                                <input
                                    onChange={this.onChange}
                                    value={this.state.email}
                                    error={errors.email}
                                    id="email"
                                    type="email"
                                    className={classnames("form-control", {
                                        invalid: errors.email
                                    })}
                                />
                                <p className="text-center pb-0 mt-2 d-block w-100">
                                <button  form="otp-form" className="btn btnBlue mt-2 mx-auto" onClick={this.onsendOtp}>Send OTP</button>
                                </p>
                                <span className="text-danger">{errors.phone}</span>
                                </div>
                                <div className="form-group">
                                <label>Enter 6 Digit OTP</label>
                                    <input type="text" id="otp"
                                    onChange={this.onChange}
                                    value={this.state.otp} className="form-control" />
                                    <span className="text-danger">{errors.otp}</span>
                                </div>
                                <p className="text-center pb-0 mt-2">
                                    <button
                                        type="submit"
                                        className="btn btnYellow">
                                        Submit
                                    </button>
                                </p>
                                <p className="text-white">
                                    Back to Login <Link to="/login">Login</Link>
                                </p>
                            </form>
                          }
                        </div>
                          <ToastContainer/>
                    </div>
                </div>
            </div>
        );
    }
}

Forgot.propTypes = {
    resetChangepattern: PropTypes.func.isRequired,
    forgotUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    { forgotUser,resetChangepattern }
)(Forgot);
