import React, { Component } from "react";
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser, Otplogin } from "../../actions/authActions";
import classnames from "classnames";
import 'react-phone-number-input/style.css'
import axios from "axios";
import { store } from 'react-notifications-component';
import keys from "../../actions/config";
import setAuthToken from "../../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import PatternLock from "react-pattern-lock";
import Logo from "../../images/Logo-small.png"
import '../../css/style-custom.css'
import { toast, ToastContainer} from "react-toastify";



import PhoneInput from 'react-phone-number-input'


const url = keys.baseUrl;

class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            phone  : "",
            otp  : "",
            errors: {},
            path: [],
            isLoading: false,
            error: false,
            success: false,
            disabled: false,
            size: 3
        };
    }

    errorTimeout = 0;

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }

        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    onChange1 = e => {
        this.setState({ [e.target.id]: e.target.value });
    };

    passlogin = e => {
        e.preventDefault();
        const userData = {
            email: this.state.email,
            password: this.state.password
        };
        this.props.loginUser(userData);
    };
    onsendOtp = e => {
        e.preventDefault();
      console.log("phone",this.state.phone);
      // if(typeof this.state.phone != "undefined" && this.state.phone!=""){
        const phonenumberdata = {
          _id: this.props.auth.user.id,
          email: this.state.email
        };
        axios
            .post(url+"api/sendotpadminlogin",phonenumberdata)
            .then(res => {
              if(res.data.success){
                toast(res.data.message, {
                position: toast.POSITION.TOP_CENTER
                });
                this.setState({errors: ""})
              }else{
                toast(res.data.message, {
                position: toast.POSITION.TOP_CENTER
                });
              }
            })
            .catch();
      // }else{
      //   store.addNotification({
      //     title: "Error!",
      //     message: "Phone Number should not be empty",
      //     type: "danger",
      //     insert: "top",
      //     container: "top-right",
      //     animationIn: ["animated", "fadeIn"],
      //     animationOut: ["animated", "fadeOut"],
      //     dismiss: {
      //       duration: 1500,
      //       onScreen: true
      //     }
      //   });
      // }
     }

     Otplogin = e => {
        e.preventDefault();
      console.log(e);
      if(typeof this.state.otp != "undefined" && this.state.otp!=""){
        const otpdata = {
          _id: this.props.auth.user.id,
          otp: this.state.otp,
          pattern:this.state.path.join(""),
          email: this.state.email
        };
        console.log("otpdata",otpdata);

        this.props.Otplogin(otpdata);

      }else{
        store.addNotification({
          title: "Error!",
          message: "OTP should not be empty",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
      }
     }




  onReset = () => {
    this.setState({
      path: [],
      success: false,
      error: false,
      disabled: false
    });
  };

  onChange = path => {
    this.setState({ path: [...path] });
  };

  // onFinish = () => {
  //   this.setState({ isLoading: true });
  //   // an imaginary api call
  //   // setTimeout(() => {
  //   //   if (this.state.path.join("-") === "0-1-2") {
  //   //     this.setState({ isLoading: false, success: true, disabled: true });
  //   //   } else {
  //   //     this.setState({ disabled: true, error: true });
  //   //     this.errorTimeout = window.setTimeout(() => {
  //   //       this.setState({
  //   //         disabled: false,
  //   //         error: false,
  //   //         isLoading: false,
  //   //         path: []
  //   //       });
  //   //     }, 2000);
  //   //   }
  //   // }, 1000);
  // };





    render() {
        const { size,errors, path, disabled, success, error, isLoading } = this.state;

        return (
            <div className="container">
                <div className="row mt-5">
                    <div className="col-md-5 mx-auto mt-0 mb-4 card shadow-lg">
                        <div className="card-body p-1 text-center">
                        <img className="text-center text-primary mt-3" src={Logo} />
                            <h2 className="text-center titleLogin mt-4 mb-3">Login</h2>



                            {/* <form noValidate onSubmit={this.passlogin} className="white">
                                <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input
                                    onChange={this.onChange}
                                    value={this.state.email}
                                    error={errors.email}
                                    id="email"
                                    type="email"
                                    className={classnames("form-control", {
                                        invalid: errors.email
                                    })}
                                />
                                <span className="text-danger d-block">{errors.email}</span>
                               </div>
                                <label htmlFor="password">Password</label>
                                <input
                                    onChange={this.onChange}
                                    value={this.state.password}
                                    error={errors.password}
                                    id="password"
                                    type="password"
                                    className={classnames("form-control", {
                                        invalid: errors.password
                                    })}
                                />
                                <span className="text-danger">{errors.password}</span>
                                <p className="text-center pb-0 mt-2">
                                    <button
                                        type="submit"
                                        className="btn btn-large btn-primary mt-2 px-5">
                                        Login
                                    </button>
                                </p>
                            </form> */}

                            <form noValidate onSubmit={this.onsendOtp} id="otp-form">
                              <div className="form-group mobNumber">
                              {/*}  <label>Mobile Number</label>
                            <PhoneInput
                            placeholder="Enter phone number"
                            country={'IN'}
                            value={this.state.phone}
                            onChange={phone => this.setState({ phone })}
                                /> */}
                                <label htmlFor="email">Email</label>
                                <input
                                    onChange={this.onChange1}
                                    value={this.state.email}
                                    error={errors.email}
                                    id="email"
                                    type="email"
                                    className={classnames("form-control", {
                                        invalid: errors.email
                                    })}
                                />

                                <p className="text-center pb-0 mt-2">
                            <button  form="otp-form" className="btn btnBlue mt-2">Send OTP</button>
                                </p>
                            </div>
                        </form>
                        <form className="" noValidate onSubmit={this.Otplogin} id="otp-login">


                            <div className="form-group mt-4">
                            <label>Enter 6 Digit OTP</label>
                                <input type="text" id="otp"
                                onChange={this.onChange1}
                                value={this.state.otp} className="form-control" />
                                <span className="text-danger">{errors.otp}</span>
                            </div>

                            <PatternLock
                              width={ 300 }
                              pointSize={ 15 }
                              size={ 3 }
                              path={ this.state.path }
                              onChange={ (pattern) => {
                                  this.setState({ path : pattern });
                              }}
                              onFinish={() => {
                                console.log("patternn",this.state.path.join(""))
                                  // check if the pattern is correct
                              }}
                          />
                          <span className="text-danger">{errors.pattern}</span>
                          <p>
                           <button className="btn btnBlue"
                                  onClick={this.onReset}
                                >
                                  Click here to reset
                                </button>
                                </p>

                            <div>
                                <div className="form-group">
                                <label className="d-none d-md-block invisible">L</label>
                                <button form="otp-login" className="btn btnYellow">Login</button>
                                </div>
                            </div>
                        </form>
                        <p className="grey-text text-darken-1">
                                     <Link to="/forgot"> Forgot Pattern?</Link>
                                </p>



                        </div>
                        <ToastContainer/>
                    </div>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    { loginUser,Otplogin }
)(withRouter(Login));
