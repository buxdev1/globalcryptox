const express = require('express');
const router = express.Router();
const async = require("async");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');


const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');
const Admincontrol =require ('../../models/Admincontrol')

router.get('/test', (req, res) => {
    res.json({statue:"success"});
});

router.post('/admin_add',(req,res)=>{
    const { errors, isValid } = validateRegisterInput(req.body,'register');
    if (!isValid) {
        return res.status(400).json(errors);
    }
    Admincontrol.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: 'Email already exists' });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then(user => {
                            return res.status(200).json({message: 'Admin added successfully. Refreshing data...'})
                        }).catch(err => console.log(err));
                });
            });
        }
    });
})






module.exports = router;