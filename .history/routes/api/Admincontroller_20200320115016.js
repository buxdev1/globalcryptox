const express = require('express');
const router = express.Router();
const async = require("async");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const multer = require('multer');
const {ObjectId} = require('mongodb');


const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');
const Admincontrol =require ('../../models/Admincontrol')

router.get('/test', (req, res) => {
    res.json({statue:"success"});
});

router.post('/adminadd',(req,res)=>{
    const { errors, isValid } = validateRegisterInput(req.body,'register');
    if (!isValid) {
        return res.status(400).json(errors);
    }
    Admincontrol.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: 'Email already exists' });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then(user => {
                            return res.status(200).json({message: 'Admin added successfully. Refreshing data...'})
                        }).catch(err => console.log(err));
                });
            });
        }
    });
})

router.get('/adminget/:id', (req, res) => {
    const id = req.params.id;
    Admincontrol.findById(id).then(user => {
        if (user) {
            return res.status(200).send(user);
        }
    });
});



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' +file.originalname )
    }
})

var upload = multer({ storage: storage });

router.post('/adminprofileupload', upload.single('file'), (req, res) => {
    const file = req.file; // file passed from client
    const meta = req.body; // all other values passed from the client, like name, etc..
    const { errors, isValid } = validateUpdateUserInput(req.body,'profile');
    if (!isValid) {
        return res.status(400).json(errors);
    }
    console.log(meta);
    console.log(file);
    let update = {};
    if(file!="" && file!=undefined){
       const profile = req.file.filename;
         update = {'name': req.body.name, 'phonenumber': req.body.phonenumber,"profile" : profile };
    }else{
         update = {'name': req.body.name, 'phonenumber': req.body.phonenumber};
    }
    console.log(update);
    const _id = req.body._id;

    User.update({ _id: _id}, {$set: update}, function(err, result) {
        if (err) {
            return res.status(400).json({ message: 'Unable to update user.' });
        } else {
            return res.status(200).json({ message: 'User updated successfully. Refreshing data...', success: true });
        }
    });

})






module.exports = router;