const express = require('express');
const router = express.Router();
const async = require("async");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const multer = require('multer');
const {ObjectId} = require('mongodb');

const keys = require('../../config/keys');


const Emailtemplates = require('../../models/emailtemplate');
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');
const Admincontrol =require ('../../models/Admincontrol')

router.get('/test', (req, res) => {
    res.json({statue:"success"});
});


router.post('/adminlogin', (req, res) => {
    console.log(req.body)
    const { errors, isValid } = validateLoginInput(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const email = req.body.email;
    const password = req.body.password;
    Admincontrol.findOne({ email:req.body.email }).then(user => {
        if (!user) {
            return res.status(404).json({ email: 'Email not found' });
        }
        console.log("user",user)

        bcrypt.compare(password, user.password).then(isMatch => {
            if (isMatch) {
                const payload = {
                    id        : user.id,
                    name      : user.name,
                    moderator : user.moderator,
                };
                jwt.sign(
                    payload,
                    keys.secretOrKey,
                    {
                        expiresIn: 31556926 // 1 year in seconds
                    },
                    (err, token) => {
                        res.json({
                            success: true,
                            token: 'Bearer ' + token
                        });
                    }
                );
            } else {
                return res
                    .status(400)
                    .json({ password: 'Password incorrect' });
            }
        });
    });
});


router.post('/adminadd',(req,res)=>{
    const { errors, isValid } = validateRegisterInput(req.body,'register');
    if (!isValid) {
        return res.status(400).json(errors);
    }
    Admincontrol.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: 'Email already exists' });
        } else {
            const newUser = new Admincontrol({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then(user => {
                            return res.status(200).json({message: 'Admin added successfully. Refreshing data...'})
                        }).catch(err => console.log(err));
                });
            });
        }
    });
})

router.get('/adminget/:id', (req, res) => {
    const id = req.params.id;
    Admincontrol.findById(id).then(user => {
        if (user) {
            return res.status(200).send(user);
        }
    });
});



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' +file.originalname )
    }
})

var upload = multer({ storage: storage });

router.post('/adminprofileupload', upload.single('file'), (req, res) => {
    const file = req.file; // file passed from client
    const meta = req.body; // all other values passed from the client, like name, etc..
    const { errors, isValid } = validateUpdateUserInput(req.body,'profile');
    if (!isValid) {
        return res.status(400).json(errors);
    }
    console.log(meta);
    console.log(file);
    let update = {};
    if(file!="" && file!=undefined){
       const profile = req.file.filename;
         update = {'name': req.body.name, 'phonenumber': req.body.phonenumber,"profile" : profile };
    }else{
         update = {'name': req.body.name, 'phonenumber': req.body.phonenumber};
    }
    console.log(update);
    const _id = req.body._id;

    Admincontrol.update({ _id: _id}, {$set: update}, function(err, result) {
        if (err) {
            return res.status(400).json({ message: 'Unable to update user.' });
        } else {
            return res.status(200).json({ message: 'User updated successfully. Refreshing data...', success: true });
        }
    });

})



router.post('/forgot', (req, res) => {
    const { errors, isValid } = validateForgotInput(req.body);
    if (!isValid) {
        return res.status(400).json(errors);
    }
    Admincontrol.findOne({ email: req.body.email }).then(user => {
        if (!user) {
            return res.status(400).json({ email: 'Email not exists' });
        } else {
          async.waterfall([
            function (done) {
            var jsonfilter = {identifier:"User_forgot"};
            var logo = keys.baseUrl+"Logo-small.png";
            Emailtemplates.findOne(jsonfilter,{_id : 0 } , function (err, templates) {
                if(templates.content) {
                  templateData = templates;
                  templateData.content = templateData.content.replace(/##templateInfo_name##/g, user.name);
                  templateData.content = templateData.content.replace(/##templateInfo_appName##/g, keys.siteName);
                  templateData.content = templateData.content.replace(/##templateInfo_logo##/g, logo);
                  var link_html = keys.frontUrl+"resetpassword/"+user._id;
                  templateData.content = templateData.content.replace(/##templateInfo_url##/g, link_html);
                  done();
                }
             });
          },
          function (done) {
           var smtpConfig = {
                    // service: keys.serverName,
                    host: keys.host, // Amazon email SMTP hostname
                    auth: {
                      user: keys.email,
                      pass: keys.password
                    }
                  };
           var transporter = nodemailer.createTransport(smtpConfig);
  
           var mailOptions = {
              from: keys.fromName+ '<'+keys.fromemail +'>', // sender address
              to: req.body.email, // list of receivers
              subject: templateData.subject, // Subject line
              html: templateData.content // html body
           };
           transporter.sendMail(mailOptions, function(error, info){
              if(error)
              {
                 return console.log(error);
              }
              else
              {
                return res.status(200).json({message: 'Reset Password link sent to Registered Mail I...'})
              }
           });
          }
        ], function (err) {
        });
  
        }
    });
  });
  





module.exports = router;