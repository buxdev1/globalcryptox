import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import {Link} from "react-router-dom";
import {faUserAlt} from "@fortawesome/free-solid-svg-icons/faUserAlt";
import TradingViewWidget, { Themes } from 'react-tradingview-widget';
var CanvasJSReact = require('../../canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
 

class Dashboard extends Component {

    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        
        //const { user } = this.props.auth;
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                            <button className="btn" id="menu-toggle"><FontAwesomeIcon icon={faList}/></button>
                            <h3 className="mt-2 text-secondary">Dashboard</h3>
                            <div className="row px-2">
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor : "cornflowerblue"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Users</h5>
                                            <p className="card-text">With supporting text below as a natural lead-in to
                                                additional content.</p>
                                            <Link to="/users" className="btn btn-light"><FontAwesomeIcon className="text-primary" icon={faUserAlt}/> Go to Users</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "cadetblue"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Enquires</h5>
                                            <p className="card-text">With supporting text below as a natural lead-in to
                                                additional content.</p>
                                            <Link to="/contactus" className="btn btn-light"><FontAwesomeIcon className="text-primary" icon={faUserAlt}/> Go to Contact</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "grey"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Enquires</h5>
                                            <p className="card-text">With supporting text below as a natural lead-in to
                                                additional content.</p>
                                            <Link to="/contactus" className="btn btn-light"><FontAwesomeIcon className="text-primary" icon={faUserAlt}/> Go to Contact</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "cadetblue"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Enquires</h5>
                                            <p className="card-text">With supporting text below as a natural lead-in to
                                                additional content.</p>
                                            <Link to="/contactus" className="btn btn-light"><FontAwesomeIcon className="text-primary" icon={faUserAlt}/> Go to Contact</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-3 p-sm-2">
                                    <div className="card text-white shadow-lg" style={{backgroundColor: "cadetblue"}}>
                                        <div className="card-body">
                                            <h5 className="card-title">Enquires</h5>
                                            <p className="card-text">With supporting text below as a natural lead-in to
                                                additional content.</p>
                                            <Link to="/contactus" className="btn btn-light"><FontAwesomeIcon className="text-primary" icon={faUserAlt}/> Go to Contact</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        );
    }
}

Dashboard.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(Dashboard);
