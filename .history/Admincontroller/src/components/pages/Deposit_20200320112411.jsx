import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList } from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from "@ashvin27/react-datatable";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { toast, ToastContainer } from "react-toastify";
import $ from "jquery";
import keys from "../../actions/config";
import Select from "react-select";

const url = keys.baseUrl;
class Deposit extends Component {
  constructor(props) {
    super(props);

    // this.state = {
    //     first_currency: "",

    //     records: []
    // };

    this.state = {
      first_currency: "",
      depositamount:"",
      currentRecord: {
        id: "",
        categoryName: "",
        status: ""
      },
      errors: {}
    };

    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  componentWillReceiveProps(nextProps) {
    this.getData();
  }

  getData() {
    axios
      .get(url + "api/asset-data-first")
      .then(res => {
        console.log("datassss", res.data);
        var currencyarray = [];
        res.data.map((item, i) => {
          const name = item.currencySymbol;
          const value = item.currencySymbol;
          const obj = { value: name, label: value };
          currencyarray.push(obj);
        });
        // console.log(currencyarray,'currencyarray');
        this.setState({ first_currency1: currencyarray, email_assigned: true });
      })
      .catch();
    //  console.log(this.props,'authget1');
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  handleChange = selectedOption => {
    this.setState({ first_currency: selectedOption.value });
    //  console.log(`Option selected:`, selectedOption );
  };

  pageChange(pageData) {
    console.log("OnPageChange", pageData);
  }

  ondepositadd = e => {
    e.preventDefault();
    const newcontract = {
      second_currency: this.state.second_currency,
      first_currency: this.state.first_currency,
      single_min_limit: this.state.single_min_limit,
      single_max_limit: this.state.single_max_limit,
      full_min_limit: this.state.full_min_limit,
      full_max_limit: this.state.full_max_limit,
      trade_fee: this.state.trade_fee,
      statusoption: this.state.status.statusoption
    };
    // console.log(newcontract);
    this.props.addasset(newcontract);
  };

  render() {
    const { selectedOption } = this.state.first_currency;

    return (
      <div>
        <Navbar />
        <div className="d-flex" id="wrapper">
          <Sidebar />
          <div id="page-content-wrapper">
            <div className="container-fluid">
              <form
                noValidate
                onSubmit={this.ondepositadd}
                id="add-currency"
              >
                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="first_currency">Currency Name</label>
                  </div>
                  <div className="col-md-9">
                    <Select
                      value={selectedOption}
                      onChange={this.handleChange}
                      options={this.state.first_currency1}
                    />
                    <span className="text-danger"></span>
                  </div>
                </div>

                <div className="row mt-2">
                  <div className="col-md-3">
                    <label htmlFor="currencyName">Deposit Amount</label>
                  </div>
                  <div className="col-md-9">
                    <input
                      onChange={this.onChange}
                      value={this.state.depositamount}
                      id="depositamount"
                      type="text"
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
          <ToastContainer />
        </div>
      </div>
    );
  }
}

Deposit.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  records: state.records
});

export default connect(mapStateToProps)(Deposit);
