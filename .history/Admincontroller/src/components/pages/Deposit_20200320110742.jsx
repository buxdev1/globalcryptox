import React, { Component, Fragment } from "react";
import Navbar from "../partials/Navbar";
import Sidebar from "../partials/Sidebar";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faList} from "@fortawesome/free-solid-svg-icons/faList";
import ReactDatatable from '@ashvin27/react-datatable';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import axios from "axios";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import CategoryAddModal from "../partials/CategoryAddModal.jsx";
import CategoryUpdateModal from "../partials/CategoryUpdateModal.jsx";
import { toast, ToastContainer} from "react-toastify";
import $ from 'jquery';
import keys from "../../actions/config";
import Select from "react-select";

const url = keys.baseUrl;
class Faq extends Component {
    constructor(props) {
        super(props);


        this.state = {
            records: []
        };

        this.state = {
            currentRecord: {
                id:"",
                categoryName: '',
                status:''
            },
            errors : {}
            
        };

        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getData()
    };

    componentWillReceiveProps(nextProps) {
        this.getData()
    }

    getData() {
        axios
            .get(url+"api/category")
            .then(res => {
                console.log("Responmse category data",res)
                this.setState({ records: res.data})
            })
            .catch()
    }

    editRecord(record) {
         $("#update-category-modal").find(".text-danger").hide();
          $("#add-category-modal").find(".text-danger").hide();
        this.setState({ currentRecord: record,errors:{}});
    }
     addRecord() {
        $("#add-category-modal").find(".text-danger").hide();
    }


    deleteRecord(record) {
      console.log(record);
       if(!window.confirm('Are you sure you want to delete this category?')){ return false; }
        axios
            .post(url+"api/category-delete", {_id: record._id})
            .then(res => {
                if (res.status === 200) {
                   toast(res.data.message, {
                       position: toast.POSITION.TOP_CENTER,
                   })
                }
            })
            .catch();
        this.getData();
    }

    pageChange(pageData) {
        console.log("OnPageChange", pageData);
    }

    render() {
        return (
            <div>
                <Navbar/>
                <div className="d-flex" id="wrapper">
                    <Sidebar/>
                    <CategoryAddModal/>
                    <CategoryUpdateModal record={this.state.currentRecord}/>
                    <div id="page-content-wrapper">
                        <div className="container-fluid">
                           

                        <form
                  noValidate
                  onSubmit={this.onsubCategoryAdd}
                  id="add-currency"
                >
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="currencyName">Sub Category Name</label>
                    </div>
                    <div className="col-md-9">
                      <input
                        onChange={this.onChange}
                        // value={this.state.subcategoryName}
                        id="subcategoryName"
                        type="text"
                        // error={errors.currencyName}
                      
                      />
                    </div>
                  </div>
                  <div className="row mt-2">
                    <div className="col-md-3">
                      <label htmlFor="first_currency">Main Category</label>
                    </div>
                    <div className="col-md-9">
                      <Select
                        value={selectedOption}
                        // defaultValue={{
                        //   label: this.state.maincategoryId,
                        //   value: this.state.maincategoryId
                        // }}
                        // onChange={this.handleChange}
                        // options={this.state.categoryName1}
                      />
                      <span className="text-danger">
                        
                      </span>
                    </div>
                  </div>
                </form>



                        </div>
                    </div>
                    <ToastContainer/>
                </div>
            </div>
        );
    }

}

Faq.propTypes = {
    auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    records: state.records
});

export default connect(
    mapStateToProps
)(Faq);
