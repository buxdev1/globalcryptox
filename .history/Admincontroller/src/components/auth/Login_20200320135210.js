import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";
import classnames from "classnames";
import 'react-phone-number-input/style.css'
import axios from "axios";
import { store } from 'react-notifications-component';
import keys from "../../actions/config";



import PhoneInput from 'react-phone-number-input'


const url = keys.baseUrl;

class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            phone  : "",
            otp  : "",
            errors: {}
        };
    }

    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.props.history.push("/dashboard");
        }

        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };

    onSubmit = e => {
        e.preventDefault();
        const userData = {
            email: this.state.email,
            password: this.state.password
        };
        this.props.loginUser(userData);
    };
    onsendOtp = e => {
        e.preventDefault();
      console.log("phone",this.state.phone);
      if(typeof this.state.phone != "undefined" && this.state.phone!=""){
        const phonenumberdata = {
          _id: this.props.auth.user.id,
          phone: this.state.phone
        };
        axios
            .post(url+"api/sendotpadminlogin",phonenumberdata)
            .then(res => {
              if(res.data.success){
                store.addNotification({
                  title: "Wonderful!",
                  message: res.data.message,
                  type: "success",
                  insert: "top",
                  container: "top-right",
                  animationIn: ["animated", "fadeIn"],
                  animationOut: ["animated", "fadeOut"],
                  dismiss: {
                    duration: 1500,
                    onScreen: true
                  }
                });
                this.setState({errors: ""})
              }else{
                store.addNotification({
                  title: "Sorry!",
                  message: res.data.message,
                  type: "danger",
                  insert: "top",
                  container: "top-right",
                  animationIn: ["animated", "fadeIn"],
                  animationOut: ["animated", "fadeOut"],
                  dismiss: {
                    duration: 1500,
                    onScreen: true
                  }
                });
              }
            })
            .catch();    
      }else{
        store.addNotification({
          title: "Error!",
          message: "Phone Number should not be empty",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
      }
     }

     checkOtp = e => {
        e.preventDefault();
      console.log(e);
      if(typeof this.state.otp != "undefined" && this.state.otp!=""){
        const otpdata = {
          _id: this.props.auth.user.id,
          otp: this.state.otp
        };
        axios
            .post(url+"api/otplogin",otpdata)
            .then(res => {
              if(res.data.success){
                store.addNotification({
                  title: "Wonderful!",
                  message: res.data.message,
                  type: "success",
                  insert: "top",
                  container: "top-right",
                  animationIn: ["animated", "fadeIn"],
                  animationOut: ["animated", "fadeOut"],
                  dismiss: {
                    duration: 1500,
                    onScreen: true
                  }
                });
                const { token } = res.data;
                localStorage.setItem("jwtToken", token);
                setAuthToken(token);
                const decoded = jwt_decode(token);
                dispatch(setCurrentUser(decoded));
                this.setState({errors: ""})
              }else{
                store.addNotification({
                  title: "Sorry!",
                  message: res.data.message,
                  type: "danger",
                  insert: "top",
                  container: "top-right",
                  animationIn: ["animated", "fadeIn"],
                  animationOut: ["animated", "fadeOut"],
                  dismiss: {
                    duration: 1500,
                    onScreen: true
                  }
                });
              }
            })
            .catch();
    
    
    
      }else{
        store.addNotification({
          title: "Error!",
          message: "OTP should not be empty",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
      }
     }
    






    render() {
        const { errors } = this.state;
        return (
            <div className="container">
                <div className="row mt-5">
                    <div className="col-md-4 mx-auto mt-5 card shadow-lg">
                        <div className="card-body p-1">
                        <img style={{
                            paddingLeft:"15%",

                        }} className="text-center text-primary mt-3" src="http://3.15.233.234:5000/Logo-small.png" />
                            <h2 className="text-center text-primary mt-3">Login</h2>
                            <form noValidate onSubmit={this.onSubmit} className="white">
                                <label htmlFor="email">Email</label>
                                <input
                                    onChange={this.onChange}
                                    value={this.state.email}
                                    error={errors.email}
                                    id="email"
                                    type="email"
                                    className={classnames("form-control", {
                                        invalid: errors.email
                                    })}
                                />
                                <span className="text-danger">{errors.email}</span>
                                <br/>
                                <label htmlFor="password">Password</label>
                                <input
                                    onChange={this.onChange}
                                    value={this.state.password}
                                    error={errors.password}
                                    id="password"
                                    type="password"
                                    className={classnames("form-control", {
                                        invalid: errors.password
                                    })}
                                />
                                <span className="text-danger">{errors.password}</span>
                                <p className="text-center pb-0 mt-2">
                                    <button
                                        type="submit"
                                        className="btn btn-large btn-primary mt-2 px-5">
                                        Login
                                    </button>
                                </p>
                            </form>

                            <form noValidate onSubmit={this.onsendOtp} id="otp-form">
                              <div className="form-group input-group mobNumber">
                                <label>Mobile Number</label>
                            <PhoneInput
                            placeholder="Enter phone number"
                            country={'IN'}
                            value={this.state.phone}
                            onChange={phone => this.setState({ phone })}
                                />
                                <p className="text-center pb-0 mt-2">
                            <button className="btn btn-large btn-primary mt-2 px-5">Send OTP</button>
                                </p>
                            </div>
                        </form>
                        <form className="col-xl-8 col-lg-8 col-md-8" noValidate onSubmit={this.checkOtp} id="otp-form">
                   
                            <div className="form-group">
                                <label>Enter 6 Digit OTP</label>
                            <input type="text" id="otp"
                                                onChange={this.onChange}
                            value={this.state.otp} className="form-control" />
                            </div>
                            
                            <div className="col-xl-4 col-lg-4 col-md-2 col-sm-12">
                                <div className="form-group">
                                <label className="d-none d-md-block invisible">L</label>
                                <button className="btn btn-large btn-primary mt-2 px-5">Login</button>
                                </div>
                            </div>
                        </form>



                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    { loginUser }
)(Login);
