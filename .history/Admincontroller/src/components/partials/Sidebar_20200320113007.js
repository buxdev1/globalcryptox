import React, { Component } from "react";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";
import {logoutUser} from "../../actions/authActions";
import {Link} from "react-router-dom";

class Sidebar extends Component {

    onLogoutClick = e => {
        e.preventDefault();
        this.props.logoutUser();
    };

    render() {
        const { user } = this.props.auth.user;
        console.log(this.props.auth.user,'fsdlfsdlkfjsldkjflskdjflsdf');
        return (
            <div className="border-right h-100" id="sidebar-wrapper">

                <div className="list-group list-group-flush">
                    {
                    (this.props.auth.user.moderator=='1')?
                    <div>
                        <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                        <Link to="/chat" className="list-group-item list-group-item-action">Chat</Link>  
                    </div> :
                    <div>
                        <Link to="/dashboard" className="list-group-item list-group-item-action">Dashboard</Link>
                        <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                        <Link to="/chat" className="list-group-item list-group-item-action">Chat</Link>
                        <Link to="/assets" className="list-group-item list-group-item-action">Assets</Link>
                        <Link to="/emailtemplates" className="list-group-item list-group-item-action">Email Templates</Link>
                        <Link to="/cms" className="list-group-item list-group-item-action">CMS Pages</Link>
                        <Link to="/faq" className="list-group-item list-group-item-action">FAQ</Link>
                        <Link to="/withdraw" className="list-group-item list-group-item-action">Withdraw List</Link> 
                        <Link to="/contactus" className="list-group-item list-group-item-action">Contact Us</Link>
                        <Link to="/newsletter" className="list-group-item list-group-item-action">Newsletter</Link>    
                        <Link to="/support" className="list-group-item list-group-item-action">Support</Link>    
                        <Link to="/currency" className="list-group-item list-group-item-action">Currency</Link>    
                        <Link to="/" className="list-group-item list-group-item-action">Trade Pair</Link>
                        <Link to="/perpetual" className="list-group-item list-group-item-action">Perpetual contract</Link>
                        <Link to="/orderhistory" className="list-group-item list-group-item-action">Order History</Link>
                        <Link to="/tradehistory" className="list-group-item list-group-item-action">Trade History</Link>
                        <Link to="/closedpositions" className="list-group-item list-group-item-action">Closed positions</Link>
                        <Link to="/" className="list-group-item list-group-item-action">Transaction History</Link>
                        <Link to="/" className="list-group-item list-group-item-action">User Audit</Link>
                        <Link to="/feesettings" className="list-group-item list-group-item-action">Fee Settings</Link>
                        <Link to="/category" className="list-group-item list-group-item-action">Help Center Main Category</Link>
                        <Link to="/subcategory" className="list-group-item list-group-item-action">Help Center Sub Category</Link>
                        <Link to="/article" className="list-group-item list-group-item-action">Help Center Article</Link>
                        <Link to="/admindeposit" className="list-group-item list-group-item-action">Admin Deposit</Link>
                        <Link to="/adminwithdraw" className="list-group-item list-group-item-action">Admin Withdraw</Link>
                        
                        
                    </div>}

                    <button className="list-group-item list-group-item-action" onClick={this.onLogoutClick}>Logout <FontAwesomeIcon icon={faSignOutAlt} /></button>
                </div>
            </div>
        );
    }
}

Sidebar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(Sidebar);
