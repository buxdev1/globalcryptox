import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "./components/pages/Dashboard";
import React, { Component } from 'react';
import Login from "./components/auth/Login";
import Forgot from "./components/auth/Forgot";
import NotFound from "./components/layout/NotFound";
import { Provider } from "react-redux";
import PrivateRoute from "./components/private-route/PrivateRoute";
import Register from "./components/auth/Register";
import store from "./store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootstrap/dist/js/bootstrap';
import '../node_modules/font-awesome/css/font-awesome.css';
import '../node_modules/jquery/dist/jquery.min';
import '../node_modules/popper.js/dist/popper';
import User from "./components/pages/Users";
import Emailtemplates from "./components/pages/Emailtemplates";
import Cms from "./components/pages/Cms";
import Profile from "./components/pages/Profile.jsx";
import Settings from "./components/pages/Settings";
import Changepassword from "./components/pages/Changepassword";
import Faq from "./components/pages/Faq";
import Withdraw from "./components/pages/Withdraw";
import Perpetual from "./components/pages/Perpetual";
import Contactus from "./components/pages/Contactus";
import Chat from "./components/pages/Chat";
import Newsletter from "./components/pages/Newsletter";
import Support from "./components/pages/Support";
import Support_reply from "./components/partials/SupportReplyModal";
import Currency from "./components/pages/Currency";
import TradeHistory from "./components/pages/Tradehistory";
import Closedpositions from "./components/pages/Closedpositionshistory";
import OrderHistory from "./components/pages/Orderhistory";
import FeeSettings from "./components/pages/Feesettings.jsx"
import Assets from "./components/pages/AssetsExchange.jsx"
import Category from "./components/pages/category.jsx"
import Subcategory from "./components/pages/Subcategory.jsx"
import Article from "./components/pages/Article.jsx"
import AdminDeposit from "./components/pages/Deposit.jsx"
import AdminWithdraw from "./components/pages/Withdraw.jsx"
if (localStorage.jwtToken) {
    const token = localStorage.jwtToken;
    setAuthToken(token);
    const decoded = jwt_decode(token);
    store.dispatch(setCurrentUser(decoded));
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        store.dispatch(logoutUser());
        window.location.href = "./login";
    }
}

class App extends Component {
    render () {
        return (
            <Provider store={store}>
                <Router basename={'/administrator'}>
                    <div className="App">
                        <Switch>
                            <Route exact path="/" component={Login} />
                            <Route exact path="/register" component={Register} />
                            <Route exact path="/login" component={Login} />
                            <Route exact path="/forgot" component={Forgot} />
                            <Switch>
                                <PrivateRoute exact path="/dashboard" component={Dashboard} />
                                <PrivateRoute exact path="/users" component={User} />
                                <PrivateRoute exact path="/assets" component={Assets} />
                                <PrivateRoute exact path="/emailtemplates" component={Emailtemplates} />
                                <PrivateRoute exact path="/cms" component={Cms} />
                                <PrivateRoute exact path="/profile" component={Profile} />
                                <PrivateRoute exact path="/settings" component={Settings} />
                                <PrivateRoute exact path="/changepassword" component={Changepassword} />
                                <PrivateRoute exact path="/faq" component={Faq} />
                                <PrivateRoute exact path="/perpetual" component={Perpetual} />
                                <PrivateRoute exact path="/contactus" component={Contactus} />
                                <PrivateRoute exact path="/chat" component={Chat} />
                                <PrivateRoute exact path="/newsletter" component={Newsletter} />
                                <PrivateRoute exact path="/support" component={Support} />
                                <PrivateRoute exact path="/support_reply/:id" component={Support_reply} />
                                <PrivateRoute exact path="/currency" component={Currency} />
                                <PrivateRoute exact path="/tradehistory" component={TradeHistory} />
                                <PrivateRoute exact path="/closedpositions" component={Closedpositions} />
                                <PrivateRoute exact path="/orderhistory" component={OrderHistory} />
                                <PrivateRoute exact path="/feesettings" component={FeeSettings} />
                                <PrivateRoute exact path="/withdraw" component={Withdraw} />
                                <PrivateRoute exact path="/category" component={Category} />
                                <PrivateRoute exact path="/subcategory" component={Subcategory} />
                                <PrivateRoute exact path="/article" component={Article} />
                                <PrivateRoute exact path="/admindeposit" component={AdminDeposit} />

                            </Switch>
                            <Route exact path="*" component={NotFound} />
                        </Switch>
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
