const express       = require('express');
const path          = require('path');
const mongoose      = require('mongoose');
const bodyParser    = require('body-parser');
const passport      = require('passport');
var http            = require("http");
var https           = require("https");
const users         = require('./routes/api/users');
const helpcenter    = require('./routes/api/helpcenter')
const newsletter    = require('./routes/api/newsletter');
const trade         = require('./routes/api/trade');
const frontusers    = require('./routes/cryptoapi/frontusers');
const support       = require('./routes/cryptoapi/support');
const support_bk    = require('./routes/api/support_bk');
const fronttrade    = require('./routes/cryptoapi/fronttrade');
const homepage      = require('./routes/cryptoapi/homepage');
const frontreferral = require('./routes/cryptoapi/frontreferral')
const frontassets = require('./routes/cryptoapi/frontassets');
// const exchangePrices              = require('../models/exchangePrices');

require('./config/passport')(passport);
const app = express();
const cors = require('cors');



app.use(cors());
// var socket = require('socket.io');
var ip = require('ip');
var fs = require('fs');
var myip = ip.address();

  console.log(myip,'ip');

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

//app.listen(9000);

const db = require('./config/keys').mongoURI;

mongoose.connect(db, {
        useNewUrlParser: true
    })
    .then(() =>
        console.log('MongoDB successfully connected.')
    ).catch(err => console.log(err));

app.use(passport.initialize());
// app.use(express.static('public'));
var uploadsDir = './public/';
app.use(express.static(path.join(__dirname, uploadsDir)));

app.use('/api', users);
app.use('/api', newsletter);
app.use('/api', support);
app.use('/api', trade);
app.use('/api', helpcenter);
app.use('/api',Admincontroller)
//frontend
app.use('/cryptoapi', frontusers);
app.use('/cryptoapi', fronttrade);
app.use('/cryptoapi', homepage);
app.use('/cryptoapi', frontreferral)
app.use('/cryptoapi', frontassets)
console.log('dfsdfsdfsdfsdf');
app.use(express.static(path.join(__dirname, 'client/build')));

// app.get('*', function (req, res) {
//     res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
// });


const port = process.env.PORT || 5000;
  var server = http.createServer(app);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.get('/', function (req, res) {
    res.json({status:true});
});

//app.listen(port, () => console.log(`Server up and running on port ${port} !`));
server = server.listen(port, function () {
    console.log('server is running on port 5000')
});

var io = require('socket.io')(server);

io.on('connection', (socket) => {
    console.log(socket.id,'socket id');

    socket.on('CREATEROOM', function(userid){
        socket.join(userid);
    });

    socket.on('SEND_MESSAGE', function (data) {
        console.log("Server");
        console.log(data);
        io.emit('RECEIVE_MESSAGE', data);
    })
});
socketio = io;
app.set('socket',io);
