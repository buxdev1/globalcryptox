import React, { Component } from 'react'
import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
// import Factor2AImg from "../images/scanCopy.png"
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import 'react-phone-number-input/style.css'
import { store } from 'react-notifications-component';
import { profileUser,tfaFormsubmit,CreateApiKey,deleteApiKey } from "../actions/authActions";
import PhoneInput from 'react-phone-number-input'
import classnames from "classnames";
import keys from "../actions/config";
import Popup from "reactjs-popup";
import {Modal,Button} from 'react-bootstrap/';
import Select from 'react-select';

const url = keys.baseUrl;

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: '1px dotted pink',
    color: state.isSelected ? 'red' : 'blue',
    padding: 20,
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />

  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';
    return { ...provided, opacity, transition };
  }
}


const options = [
  { value: 'Active Order and Positions', label: 'Active Order and Positions' },
  { value: 'Positions Only', label: 'Positions Only' },
  { value: 'Active Order Only', label: 'Active Order Only' },
];

const options1 = [
  { value: 'Beshare', label: 'Beshare' },
  { value: '3Commas', label: '3Commas' },
  { value: 'AIcoin', label: 'AIcoin' },
  { value: 'alpha-algo', label: 'alpha-algo' },
  { value: 'Cornix', label: 'Cornix' },
  { value: '合约帝', label: '合约帝' },
];



class Settings extends Component {

  constructor() {
			super();
			this.state = {
          userid          : "",
          name            : "",
          id              : "",
          currency        : "",
          oldpassword     : "",
          password        : "",
          password2       : "",
          loginpassword   : "",
          secretcode      : "",
          googleauth      : "",
          onecode         : "",
          remarkname      : "",
          ipaddress       : "",
          keypermission   : "",
          applicationName : "",
          readOnly        : "",
          twofactorkey    : "",
          notifications   : {},
          errors          : {},
          windoworder     : "false",
          mobilesite      : "false",
          position        : "false",
          animation       : "false",
          phone           : "",
          secretcode      : "",
          show            : false,
          type1           : true,
          selectedOption: null,
          type2           : false,
          otp             : "",
          sms             : "Not Verified",
          APItype         : 1,
          loginhistory    : {},
          apiKeydetails   : {},
			};
	}
  onChange = e => {
			this.setState({ [e.target.id]: e.target.value });
	};

  handleChange = keypermission => {
    this.setState({ keypermission });
    console.log(`Option selected:`, keypermission);
  };

   handleChange1 = applicationName => {
    this.setState({ applicationName });
    console.log(`Option selected:`, applicationName);
  };
    handleClose = () => {
      this.setState({show:false,remarkname      : "",
          ipaddress       : "",
          keypermission   : "",
          applicationName : "",
          readOnly        : "",
          twofactorkey    : "",});
    }

    handleShow = () => {
      this.setState({show:true});
    }

    handleOnChange = (e) => {
      console.log('selected option', e.target.value);
      this.setState({ APItype: e.target.value});
      if(e.target.value==1)
      {
        console.log('type1');
        this.setState({ type1: true, type2 :false});
        console.log(this.state.type2);
      }
      else
      {
        console.log('type2');
        this.setState({ type2: true, type1 :false});
        this.setState({ type2: true, type1 :false});
      }
    }

    handleSelect = (e) => {
      console.log('selected option', e.target.id);
      this.setState({ APItype: e.target.value});
    }

  deleteKey = (e) => {
    let userid = this.props.auth.user.id;
    if(window.confirm('Are you sure you want to delete this key?'))
    console.log('selected option', e.target.id);
      e.preventDefault();
      const apiData = {
          userid          : userid,
          id      : e.target.id,
      };
      console.log(apiData);
      this.props.deleteApiKey(apiData);

  }

  componentDidMount() {
      this.getData()
  }
  getData() {
    let userid = this.props.auth.user.id;
      axios
          .get(url+"cryptoapi/userget/"+userid)
          .then(res => {
            console.log(res,'resultttttttttttt');
            console.log(res.data.apiKeydetails);
            this.props.auth.user.name = res.data.name;
              this.setState({
                userid        : res.data.userid,
                name          : res.data.name,
                currency      : res.data.currency,
                mobilesite    : res.data.mobilesite,
                windoworder   : res.data.windoworder,
                position      : res.data.position,
                animation     : res.data.animation,
                loginhistory  : res.data.loginhistory,
                apiKeydetails : res.data.apiKeydetails,
                sms           : res.data.sms,
                secretcode    : res.data.newSecret.secret,
                Factor2AImg   : "https://chart.googleapis.com/chart?chs=130x130&chld=L|0&cht=qr&chl=otpauth://totp/GlobalcryptoX%3Fsecret="+res.data.newSecret.secret
              })
              if(res.data.google!='Disabled')
              {
                this.setState({googleauth:'Verified'});
              }
              this.loginHistory();
          })
          .catch()

  }

  componentWillReceiveProps(nextProps) {
			if (nextProps.errors) {
					this.setState({
							errors: nextProps.errors
					});
			}
      else
      {
        this.setState({
              errors: {}
          });
      }
      console.log(nextProps.auth,'nextProps');
      if (nextProps.auth.updateprofile !== undefined
					&& nextProps.auth.updateprofile.data !== undefined
					&& nextProps.auth.updateprofile.data.message !== undefined ) {
        if(nextProps.auth.updateprofile.tfastatus!== undefined)
        {
          console.log(nextProps.auth.updateprofile.tfastatus,'tfastatusssssssssss')
            (nextProps.auth.updateprofile.tfastatus=='active')?this.setState({googleauth:'Verified'}):this.setState({googleauth:''});
        }
					store.addNotification({
            title        : "Wonderful!",
            message      : nextProps.auth.updateprofile.data.message,
            type         : "success",
            insert       : "top",
            container    : "top-right",
            animationIn  : ["animated", "fadeIn"],
            animationOut : ["animated", "fadeOut"],
            dismiss      : {
            duration     : 1500,
            onScreen     : true
					  }
					});
          this.getData();

      		nextProps.auth.updateprofile = "";
    	}
	}


  setCurrency(event) {
     console.log(event.target.value);
     this.setState({currency : event.target.value})
   }


	onProfileSubmit = e => {
			e.preventDefault();
			const userData = {
					userid: this.state.userid,
					name: this.state.name,
					id: this.props.auth.user.id
			};
			this.props.profileUser(userData);
	};

  CreateApiKey = e => {
     let userid = this.props.auth.user.id;
      e.preventDefault();
      const apiData = {
          userid          : userid,
          remarkname      : this.state.remarkname,
          APItype         : this.state.APItype,
          ipaddress       : this.state.ipaddress,
          keypermission   : (this.state.keypermission!='')?this.state.keypermission.value:'',
          readOnly        : this.state.readOnly,
          twofactorkey    : this.state.twofactorkey,
          applicationName : (this.state.applicationName!='')?this.state.applicationName.value:'',
      };
      console.log(apiData);
      // this.props.CreateApiKey(apiData);
       axios
         .post(url+"cryptoapi/CreateApiKey",apiData)
         .then(res => {
           if(res.data.success){
             store.addNotification({
               title: "Wonderful!",
               message: res.data.message,
               type: "success",
               insert: "top",
               container: "top-right",
               animationIn: ["animated", "fadeIn"],
               animationOut: ["animated", "fadeOut"],
               dismiss: {
                 duration: 1500,
                 onScreen: true
               }
             });
             this.setState({errors: ""});
               this.handleClose();
               this.getData();
           }else{
            if(res.data.message != undefined)
            {
                store.addNotification({
                title: "Error!",
                message: res.data.message,
                type: "danger",
                insert: "top",
                container: "top-right",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                duration: 1500,
                onScreen: true
                }
                });
            }
            if(res.data.errors != undefined)
             this.setState({errors: res.data.errors});
           }
         })
         .catch();
  };

  tfaFormsubmit = e => {
      e.preventDefault();
      const userData = {
          userid: this.state.userid,
          loginpassword: this.state.loginpassword,
          secretcode: this.state.secretcode,
          onecode: this.state.onecode,
          id: this.props.auth.user.id
      };
      this.props.tfaFormsubmit(userData);
  };


	onCurrencySubmit = e => {
    let userid = this.props.auth.user.id;
			e.preventDefault();
			const currencyData = {
					currency: this.state.currency,
					id: this.props.auth.user.id
			};
      axios
          .post(url+"cryptoapi/updatecurrency",currencyData)
          .then(res => {
            if(res.data.success){
              store.addNotification({
                title: "Wonderful!",
                message: "Currency updated Successfully",
                type: "success",
                insert: "top",
                container: "top-right",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                  duration: 1500,
                  onScreen: true
                }
              });
              this.setState({errors: ""})
            }else{
              this.setState({errors: res.data.errors})
            }
          })
          .catch();
	};

  checkBoxClicked(event) {
     this.setState({ [event.target.id]: event.target.checked.toString() });
   }




   apikeydetails = () => {
     let table = [];
     if(this.state.apiKeydetails.length>0)
     {
       for (var i = 0; i < this.state.apiKeydetails.length; i++) {
        var createdDate = this.state.apiKeydetails[i].createdDate.split;
          table.push(<tr><td>{i+1}</td><td>{this.state.apiKeydetails[i].createdDate}</td><td>{this.state.apiKeydetails[i].expiredDate?this.state.apiKeydetails[i].expiredDate:'permanent'}</td><td>{this.state.apiKeydetails[i].apikey}</td><td>{this.state.apiKeydetails[i].remarkname?this.state.apiKeydetails[i].remarkname:this.state.apiKeydetails[i].applicationName}</td><td>{this.state.apiKeydetails[i].secretkey}</td><td>{this.state.apiKeydetails[i].keypermission}</td><td>{this.state.apiKeydetails[i].ipaddress}</td><td><a onClick={this.deleteKey} id={this.state.apiKeydetails[i]._id}>delete</a></td></tr>)
       }
     }
     else
     {
        table.push(<tr><td colspan="9"><center>No data Found</center></td></tr>)
     }
     return table;
   }


   loginHistory = () => {
     let table = [];
     for (var i = 0; i < this.state.loginhistory.length; i++) {
        table.push(<tr><td>{i+1}</td><td>{this.state.loginhistory[i].ipaddress}</td><td>{this.state.loginhistory[i].os}/{this.state.loginhistory[i].broswername}</td><td>{this.state.loginhistory[i].countryName}/{this.state.loginhistory[i].regionName}</td><td>{this.state.loginhistory[i].ismobile}</td></tr>)
     }
     return table;
   }
  onChangepasswordUpdate = e => {
     e.preventDefault();
     const updatechangepassword = {
         _id: this.props.auth.user.id,
         oldpassword: this.state.oldpassword,
         password: this.state.password,
         password2: this.state.password2
     };

     axios
         .post(url+"cryptoapi/updatepassword",updatechangepassword)
         .then(res => {
           if(res.data.success){
             store.addNotification({
               title: "Wonderful!",
               message: "Password updated Successfully",
               type: "success",
               insert: "top",
               container: "top-right",
               animationIn: ["animated", "fadeIn"],
               animationOut: ["animated", "fadeOut"],
               dismiss: {
                 duration: 1500,
                 onScreen: true
               }
             });
             this.setState({errors: ""})
           }else{
             this.setState({errors: res.data.errors})
           }
         })
         .catch();
 };
 checkOtp = e => {
    e.preventDefault();
  console.log(e);
  if(typeof this.state.otp != "undefined" && this.state.otp!=""){
    const otpdata = {
      _id: this.props.auth.user.id,
      otp: this.state.otp
    };
    axios
        .post(url+"cryptoapi/checkotp",otpdata)
        .then(res => {
          if(res.data.success){
            store.addNotification({
              title: "Wonderful!",
              message: res.data.message,
              type: "success",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 1500,
                onScreen: true
              }
            });
            this.setState({errors: ""})
          }else{
            store.addNotification({
              title: "Sorry!",
              message: res.data.message,
              type: "danger",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 1500,
                onScreen: true
              }
            });
          }
        })
        .catch();



  }else{
    store.addNotification({
      title: "Error!",
      message: "OTP should not be empty",
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 1500,
        onScreen: true
      }
    });
  }
 }


 onsendOtp = e => {
    e.preventDefault();
  console.log(e);
  if(typeof this.state.phone != "undefined" && this.state.phone!=""){
    const phonenumberdata = {
      _id: this.props.auth.user.id,
      phone: this.state.phone
    };
    axios
        .post(url+"cryptoapi/sendotp",phonenumberdata)
        .then(res => {
          if(res.data.success){
            store.addNotification({
              title: "Wonderful!",
              message: res.data.message,
              type: "success",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 1500,
                onScreen: true
              }
            });
            this.setState({errors: ""})
          }else{
            store.addNotification({
              title: "Sorry!",
              message: res.data.message,
              type: "danger",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 1500,
                onScreen: true
              }
            });
          }
        })
        .catch();



  }else{
    store.addNotification({
      title: "Error!",
      message: "Phone Number should not be empty",
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 1500,
        onScreen: true
      }
    });
  }
 }
 onChangenotificationUpdate = e => {
   e.preventDefault();
   const updatenotification= {
       _id: this.props.auth.user.id,
       windoworder: this.state.windoworder.toString(),
       mobilesite: this.state.mobilesite.toString(),
       position: this.state.position.toString(),
       animation: this.state.animation.toString()
   };

   axios
       .post(url+"cryptoapi/updatenotification",updatenotification)
       .then(res => {
         if(res.data.success){
           store.addNotification({
             title: "Wonderful!",
             message: "Notification updated Successfully",
             type: "success",
             insert: "top",
             container: "top-right",
             animationIn: ["animated", "fadeIn"],
             animationOut: ["animated", "fadeOut"],
             dismiss: {
               duration: 1500,
               onScreen: true
             }
           });
           this.setState({errors: ""})
         }else{
           this.setState({errors: res.data.errors})
         }
       })
       .catch();

 };


  render() {
      const { errors } = this.state;
      const { user } = this.props.auth;
       const { keypermission,applicationName } = this.state;

    return (
    	<div>
    		<TradeHeader />
    		<div className="container-fluid">
    		<section className="tradeMain">
			  <div className="row">
			    <div className="col-md-12">

			     <div id="accordionPersonal" className="accordianComman">
			                          <div className="card">
			                            <div className="card-header" id="headingTwo">
			                              <h5 className="mb-0">
			                                <button className="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span className="question">Profile</span> <i className="fas fa-chevron-down"></i></button>
			                              </h5>
			                            </div>
			                            <div id="collapseTwo" className="collapse show" aria-labelledby="headingTwo" data-parent="#accordionPersonal">
			                              <div className="card-body">
                                    <form className="stoForm" noValidate onSubmit={this.onProfileSubmit} >
			                               <div className="row">
			                                 <div className="col-lg-3 col-md-4 col-sm-6">
                                         <div className="form-group">
			                                     <label>User ID</label>
                                           <input
                           										onChange={this.onChange}
                           										value={this.state.userid}
                           										error={errors.userid}
                           										id="userid"
                           										type="text"
                                              readOnly
                                        			className={classnames("form-control", {
                           												invalid: errors.userid
                           										})}
                           								/>
                           								<span className="text-danger">{errors.userid}</span>
			                                   </div>
			                                 </div>
			                                 <div className="col-lg-3 col-md-4 col-sm-6">
			                                   <div className="form-group">
			                                     <label>Username</label>
                                           <input
                           										onChange={this.onChange}
                           										value={this.state.name}
                           										error={errors.name}
                           										id="name"
                           										type="text"
                                    					className={classnames("form-control", {
                           												invalid: errors.name
                           										})}
                           								/>
                           								<span className="text-danger">{errors.name}</span>
			                                   </div>
			                                 </div>
			                                 <div className="col-lg-3 col-md-2 col-sm-12">
			                                   <div className="form-group">
			                                      <label className="d-none d-md-block invisible">Label Space</label>
			                                     <button className="btn btnDefault px-4">Update</button>
			                                   </div>
			                                 </div>
			                               </div>
                                     </form>
			                              </div>
			                            </div>
			                          </div>
			                          
			                          <div className="card">
			                            <div className="card-header" id="headingFour">
			                              <h5 className="mb-0">
			                                <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><span className="question">Security</span> <i className="fas fa-chevron-down"></i></button>
			                              </h5>
			                            </div>
			                            <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordionPersonal">
			                              <div className="card-body" id="child1">
			                                <div className="row">
			                                  <div className="col-md-12">
			                                    <div className="accordianSubGroup">
			                                      <h3 className="groupTitle">1. Email Authentication: <small className="userMailId">{user.email}</small> <small className="textVerifyGreen"><i className="far fa-check-circle"></i> Verified</small></h3>
			                                    </div>
			                                     <div className="accordianSubGroup">
			                                      <h3 className="groupTitle">2. SMS Authentication:
                                            {this.state.sms == "Not Verified" &&
                                                <small className="textUnVerifyRed">
                                                <i className="far fa-times-circle"></i>
                                                {this.state.sms}</small>

                                             }
                                             {this.state.sms == "Verified" &&
                                             <span>
                                              <small className="userMailId"></small>
                                                 <small className="textVerifyGreen"><i className="far fa-check-circle"></i> {this.state.sms}</small>
                                              </span>
                                              }


                                            </h3>

                                            <div className="row mt-3">
			                                      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                            <form noValidate onSubmit={this.onsendOtp} id="otp-form">
                                              <div className="form-group input-group mobNumber">
			                                            <label>Mobile Number</label>
                                                  <PhoneInput
                                                    placeholder="Enter phone number"
                                                    country={'IN'}
                                                    value={this.state.phone}
                                                    onChange={phone => this.setState({ phone })}
                                                      />
			                                            <div className="input-group-append securityphone">
                                                   <button className="btn btnDefault">Send OTP</button>
			                                            </div>
			                                        </div>
                                              </form>
			                                      </div>
                                            <form className="col-xl-8 col-lg-8 col-md-8" noValidate onSubmit={this.checkOtp} id="otp-form">
                                            <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
			                                        <div className="form-group">
			                                            <label>Enter 6 Digit OTP</label>
                                                  <input type="text" id="otp"
                               										  onChange={this.onChange}
                                                  value={this.state.otp} className="form-control" />
			                                        </div>
			                                      </div>
			                                      <div className="col-xl-4 col-lg-4 col-md-2 col-sm-12">
			                                         <div className="form-group">
			                                            <label className="d-none d-md-block invisible">L</label>
			                                           <button className="btn btnDefault px-4">Save</button>
			                                         </div>
			                                       </div>
                                             </form>
			                                    </div>
			                                    </div>
			                                    <div className="accordianSubGroup">
			                                      <h3 className="groupTitle">3. Google Authentication:
                                            {
                                              (this.state.googleauth == '')?
                                              <small className="textUnVerifyRed"><i className="far fa-times-circle"></i> Disabled</small>
                                              :
                                              <span>
                                              <small className="userMailId"></small><small className="textVerifyGreen"><i className="far fa-check-circle"></i> Verified</small>
                                              </span>
                                            } </h3>
			                                      <div className="row mt-3">
			                                      <div className="col-xl-4 col-lg-4 col-md-6 col-10">
			                                        <div className="form-group">
			                                          <label>Write down 2FA code for withdraw crypto</label>
                                                <input
                                               onChange={this.onChange}
                                               value={this.state.secretcode}
                                               id="secretcode"
                                               type="text"
                                               error={errors.secretcode}
                                               className={classnames("form-control", {
                                                   invalid: errors.secretcode
                                               })}/>

			                                        </div>
			                                      </div>
			                                      <div className="col-12 col-sm-2">
			                                        <div className="form-group">
			                                          <span className="scanCode"><img src={this.state.Factor2AImg} className="img-fluid" /></span>
			                                        </div>
			                                      </div>
			                                    </div>
			                                      <div className="row">
			                                      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
			                                        <div className="form-group">
			                                            <label>Login Password</label>
                                                    <input
                                               onChange={this.onChange}
                                               value={this.state.loginpassword}
                                               id="loginpassword"
                                               type="password"
                                               error={errors.loginpassword}
                                               className={classnames("form-control", {
                                                   invalid: errors.loginpassword
                                               })}/>
			                                         <span className="text-danger">{errors.loginpassword}</span>
			                                        </div>
			                                      </div>
			                                      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
			                                        <div className="form-group">
			                                            <label>Enter 6 Digit Google Authentication Code</label>
                                                  <input
                                               onChange={this.onChange}
                                               value={this.state.onecode}
                                               id="onecode"
                                               type="password"
                                               error={errors.onecode}
                                               className={classnames("form-control", {
                                                   invalid: errors.onecode
                                               })}/>
			                                         <span className="text-danger">{errors.onecode}</span>
			                                        </div>
			                                      </div>
			                                      <div className="col-xl-4 col-lg-4 col-md-2 col-sm-12">
			                                         <div className="form-group">
			                                            <label className="d-none d-md-block invisible">Label Space</label>
			                                           <button className="btn btnDefault px-4" onClick={this.tfaFormsubmit}>{this.state.googleauth!=''?'Disable':'Enable'}</button>
			                                         </div>
			                                       </div>
			                                    </div>
			                                    </div>
			                                  </div>
			                                </div>
			                              </div>
			                            </div>
			                          </div>
			                          <div className="card">
			                            <div className="card-header" id="headingFive">
			                              <h5 className="mb-0">
			                                <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><span className="question">Change Password</span> <i className="fas fa-chevron-down"></i></button>
			                              </h5>
			                            </div>
			                            <div id="collapseFive" className="collapse" aria-labelledby="headingFive" data-parent="#accordionPersonal">
			                              <div className="card-body" id="child1">
                                    <form noValidate onSubmit={this.onChangepasswordUpdate} id="update-Changepassword">
                                      <div className="row">
			                                 <div className="col-lg-3 col-md-4 col-sm-6">
			                                   <div className="form-group">
			                                     <label>Current Password</label>
                                           <input
                                               onChange={this.onChange}
                                               value={this.state.oldpassword}
                                               id="oldpassword"
                                               type="password"
                                               error={errors.oldpassword}
                                               className={classnames("form-control", {
                                                   invalid: errors.oldpassword
                                               })}/>
                                           <span className="text-danger">{errors.oldpassword}</span>
			                                   </div>
			                                 </div>
			                                 <div className="col-lg-3 col-md-4 col-sm-6">
			                                   <div className="form-group">
			                                     <label>New Password</label>
                                           <input
                                                   onChange={this.onChange}
                                                   value={this.state.password}
                                                   id="password"
                                                   type="password"
                                                   error={errors.password}
                                                   className={classnames("form-control", {
                                                       invalid: errors.password
                                                   })}/>
                                               <span className="text-danger">{errors.password}</span>
			                                   </div>
			                                 </div>
			                                 <div className="col-lg-3 col-md-4 col-sm-6">
			                                   <div className="form-group">
			                                     <label>Confirm New Password</label>
                                           <input
                                                   onChange={this.onChange}
                                                   value={this.state.password2}
                                                   id="password2"
                                                   type="password"
                                                   error={errors.password2}
                                                   className={classnames("form-control", {
                                                       invalid: errors.password2
                                                   })}/>
                                               <span className="text-danger">{errors.password2}</span>
			                                   </div>
			                                 </div>
			                                 <div className="col-lg-3 col-md-2 col-sm-12">
			                                   <div className="form-group">
			                                      <label className="d-none d-md-block invisible">Label Space</label>
			                                     <button className="btn btnDefault px-4">Update</button>
			                                   </div>
			                                 </div>
			                               </div>
                                     </form>
			                              </div>
			                            </div>
			                          </div>
			                          <div className="card">
			                            <div className="card-header" id="headingSix">
			                              <h5 className="mb-0">
			                                <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><span className="question">Alerts & Notification</span> <i className="fas fa-chevron-down"></i></button>
			                              </h5>
			                            </div>
			                            <div id="collapseSix" className="collapse" aria-labelledby="headingSix" data-parent="#accordionPersonal">
			                              <div className="card-body" id="child1">
			                                <div className="row">
                                      <form noValidate onSubmit={this.onChangenotificationUpdate} id="update-notification">
                                       <div className="col-lg-12 col-md-12 col-sm-12">
			                                   <div className="form-group settingsCheckList">
			                                     <div className="checkbox">
			                                          <label>
                                                    <input type="checkbox" id="windoworder" onChange={this.checkBoxClicked.bind(this)} checked={this.state.windoworder === "true"}  />
			                                              <span className="cr"><i className="cr-icon fa fa-check"></i></span>
			                                              <span className="listText">Shows Confirmation Window for Orders</span>
			                                          </label>
			                                      </div>
			                                      <div className="checkbox">
			                                          <label>
			                                              <input type="checkbox" id="mobilesite" onChange={this.checkBoxClicked.bind(this)} checked={this.state.mobilesite === "true"}  />
			                                              <span className="cr"><i className="cr-icon fa fa-check"></i></span>
			                                              <span className="listText">Show Confirmation Window for Orders on Mobile Site</span>
			                                          </label>
			                                      </div>
			                                      <div className="checkbox">
			                                          <label>
			                                              <input type="checkbox" id="position" onChange={this.checkBoxClicked.bind(this)} checked={this.state.position === "true"}  />
			                                              <span className="cr"><i className="cr-icon fa fa-check"></i></span>
			                                              <span className="listText">Shows Confirmation Window for changing leverage while having open position</span>
			                                          </label>
			                                      </div>
			                                      <div className="checkbox">
			                                          <label>
			                                              <input type="checkbox" id="animation" onChange={this.checkBoxClicked.bind(this)} checked={this.state.animation === "true"}  />
			                                              <span className="cr"><i className="cr-icon fa fa-check"></i></span>
			                                              <span className="listText">Turn On: Orderbook Animation</span>
			                                          </label>
			                                      </div>
			                                   </div>
                                         <div className="form-group">
			                                      <label className="d-none d-md-block invisible">Label Space</label>
			                                     <button className="btn btnDefault px-4">Update</button>
			                                   </div>
			                                 </div>
                                       </form>
			                               </div>
			                              </div>
			                            </div>
			                          </div>


			                          <div className="card">
			                            <div className="card-header" id="headingSeven">
			                              <h5 className="mb-0">
			                                <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"><span className="question">Login History</span> <i className="fas fa-chevron-down"></i></button>
			                              </h5>
			                            </div>
			                            <div id="collapseSeven" className="collapse" aria-labelledby="headingSeven" data-parent="#accordionPersonal">
			                              <div className="card-body px-0 py-0" id="child1">
			                                 <div className="table-responsive">
                                          <table id="assetsTable" className="table">
			                                          <thead>
			                                              <tr className="wow flipInX" data-wow-delay=".5s;">
			                                                  <th>S.No</th>
			                                                  <th>IP Address</th>
			                                                  <th>Operating System/Browser</th>
			                                                  <th>Country/Region</th>
			                                                  <th>isMobile?</th>
			                                              </tr>
			                                          </thead>
			                                         <tbody>
                                               {this.loginHistory()}

			                                         </tbody>
			                                      </table>
			                                    </div>
			                              </div>
			                            </div>
			                          </div>
			                        </div>
			    </div>
			  </div>
			</section>
			</div>
			<TradeFooter />
    	</div>
    	);
    }
}


Settings.propTypes = {
    profileUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    {profileUser,tfaFormsubmit,CreateApiKey,deleteApiKey}
)(withRouter(Settings));
