const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const AdmiSchema = new Schema({
    name: {
        type: String,
        default: ""
      },
      email: {
        type: String,
        // required: true
      },
      password: {
        type: String,
        required: true
      },
      phonenumber: {
        type: String,
        default: ""
      },
      otp: {
        type: String,
        default: ""
      },
      otptime: {
        type: Date,
        default: ''
      },
})

module.exports = AdmiSchema = mongoose.model("AdmincontrolSchema", AdmiSchema);