var express = require("express");
// var ip           = require('ip');
// const bitcoin_rpc  = require('node-bitcoin-rpc');
var bodyParser = require("body-parser");
const CryptoJS = require("crypto-js");
const Tx = require("ethereumjs-tx").Transaction;

// var myip = ip.address();
// console.log(myip);
var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
const Web3 = require("web3");
const web3 = new Web3("http://localhost:8545");
// const web3  = new Web3('https://ropsten.infura.io/v3/1f45bf089b7f420e9581960cc2cc50a3');
app.use(bodyParser.json());

// var contractAddr = "0xe9f72469b79b3ace278bfd38525cd6ab27d6bf8c";
// var minABI = [
//     {
//       "constant": true,
//       "inputs": [],
//       "name": "name",
//       "outputs": [
//         {
//           "name": "",
//           "type": "string"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "spender",
//           "type": "address"
//         },
//         {
//           "name": "value",
//           "type": "uint256"
//         }
//       ],
//       "name": "approve",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [],
//       "name": "totalSupply",
//       "outputs": [
//         {
//           "name": "",
//           "type": "uint256"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "sender",
//           "type": "address"
//         },
//         {
//           "name": "recipient",
//           "type": "address"
//         },
//         {
//           "name": "amount",
//           "type": "uint256"
//         }
//       ],
//       "name": "transferFrom",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [],
//       "name": "decimals",
//       "outputs": [
//         {
//           "name": "",
//           "type": "uint8"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "spender",
//           "type": "address"
//         },
//         {
//           "name": "addedValue",
//           "type": "uint256"
//         }
//       ],
//       "name": "increaseAllowance",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "account",
//           "type": "address"
//         },
//         {
//           "name": "amount",
//           "type": "uint256"
//         }
//       ],
//       "name": "mint",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "amount",
//           "type": "uint256"
//         }
//       ],
//       "name": "burn",
//       "outputs": [],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [
//         {
//           "name": "account",
//           "type": "address"
//         }
//       ],
//       "name": "balanceOf",
//       "outputs": [
//         {
//           "name": "",
//           "type": "uint256"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "account",
//           "type": "address"
//         },
//         {
//           "name": "amount",
//           "type": "uint256"
//         }
//       ],
//       "name": "burnFrom",
//       "outputs": [],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [],
//       "name": "symbol",
//       "outputs": [
//         {
//           "name": "",
//           "type": "string"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "account",
//           "type": "address"
//         }
//       ],
//       "name": "addMinter",
//       "outputs": [],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [],
//       "name": "renounceMinter",
//       "outputs": [],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "spender",
//           "type": "address"
//         },
//         {
//           "name": "subtractedValue",
//           "type": "uint256"
//         }
//       ],
//       "name": "decreaseAllowance",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "name": "recipient",
//           "type": "address"
//         },
//         {
//           "name": "amount",
//           "type": "uint256"
//         }
//       ],
//       "name": "transfer",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [
//         {
//           "name": "account",
//           "type": "address"
//         }
//       ],
//       "name": "isMinter",
//       "outputs": [
//         {
//           "name": "",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [
//         {
//           "name": "owner",
//           "type": "address"
//         },
//         {
//           "name": "spender",
//           "type": "address"
//         }
//       ],
//       "name": "allowance",
//       "outputs": [
//         {
//           "name": "",
//           "type": "uint256"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "inputs": [
//         {
//           "name": "name",
//           "type": "string"
//         },
//         {
//           "name": "symbol",
//           "type": "string"
//         },
//         {
//           "name": "decimals",
//           "type": "uint8"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "constructor"
//     },
//     {
//       "anonymous": false,
//       "inputs": [
//         {
//           "indexed": true,
//           "name": "account",
//           "type": "address"
//         }
//       ],
//       "name": "MinterAdded",
//       "type": "event"
//     },
//     {
//       "anonymous": false,
//       "inputs": [
//         {
//           "indexed": true,
//           "name": "account",
//           "type": "address"
//         }
//       ],
//       "name": "MinterRemoved",
//       "type": "event"
//     },
//     {
//       "anonymous": false,
//       "inputs": [
//         {
//           "indexed": true,
//           "name": "from",
//           "type": "address"
//         },
//         {
//           "indexed": true,
//           "name": "to",
//           "type": "address"
//         },
//         {
//           "indexed": false,
//           "name": "value",
//           "type": "uint256"
//         }
//       ],
//       "name": "Transfer",
//       "type": "event"
//     },
//     {
//       "anonymous": false,
//       "inputs": [
//         {
//           "indexed": true,
//           "name": "owner",
//           "type": "address"
//         },
//         {
//           "indexed": true,
//           "name": "spender",
//           "type": "address"
//         },
//         {
//           "indexed": false,
//           "name": "value",
//           "type": "uint256"
//         }
//       ],
//       "name": "Approval",
//       "type": "event"
//     }
//   ];
var contractAddr = "0xdac17f958d2ee523a2206206994597c13d831ec7";
var minABI = [
  {
    constant: true,
    inputs: [],
    name: "name",
    outputs: [{ name: "", type: "string" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "_upgradedAddress", type: "address" }],
    name: "deprecate",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      { name: "_spender", type: "address" },
      { name: "_value", type: "uint256" },
    ],
    name: "approve",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "deprecated",
    outputs: [{ name: "", type: "bool" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "_evilUser", type: "address" }],
    name: "addBlackList",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "totalSupply",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      { name: "_from", type: "address" },
      { name: "_to", type: "address" },
      { name: "_value", type: "uint256" },
    ],
    name: "transferFrom",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "upgradedAddress",
    outputs: [{ name: "", type: "address" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [{ name: "", type: "address" }],
    name: "balances",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "decimals",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "maximumFee",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "_totalSupply",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [],
    name: "unpause",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [{ name: "_maker", type: "address" }],
    name: "getBlackListStatus",
    outputs: [{ name: "", type: "bool" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [
      { name: "", type: "address" },
      { name: "", type: "address" },
    ],
    name: "allowed",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "paused",
    outputs: [{ name: "", type: "bool" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [{ name: "who", type: "address" }],
    name: "balanceOf",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [],
    name: "pause",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "getOwner",
    outputs: [{ name: "", type: "address" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "owner",
    outputs: [{ name: "", type: "address" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "symbol",
    outputs: [{ name: "", type: "string" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      { name: "_to", type: "address" },
      { name: "_value", type: "uint256" },
    ],
    name: "transfer",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      { name: "newBasisPoints", type: "uint256" },
      { name: "newMaxFee", type: "uint256" },
    ],
    name: "setParams",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "amount", type: "uint256" }],
    name: "issue",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "amount", type: "uint256" }],
    name: "redeem",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [
      { name: "_owner", type: "address" },
      { name: "_spender", type: "address" },
    ],
    name: "allowance",
    outputs: [{ name: "remaining", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "basisPointsRate",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [{ name: "", type: "address" }],
    name: "isBlackListed",
    outputs: [{ name: "", type: "bool" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "_clearedUser", type: "address" }],
    name: "removeBlackList",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "MAX_UINT",
    outputs: [{ name: "", type: "uint256" }],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "newOwner", type: "address" }],
    name: "transferOwnership",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: false,
    inputs: [{ name: "_blackListedUser", type: "address" }],
    name: "destroyBlackFunds",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      { name: "_initialSupply", type: "uint256" },
      { name: "_name", type: "string" },
      { name: "_symbol", type: "string" },
      { name: "_decimals", type: "uint256" },
    ],
    payable: false,
    stateMutability: "nonpayable",
    type: "constructor",
  },
  {
    anonymous: false,
    inputs: [{ indexed: false, name: "amount", type: "uint256" }],
    name: "Issue",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [{ indexed: false, name: "amount", type: "uint256" }],
    name: "Redeem",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [{ indexed: false, name: "newAddress", type: "address" }],
    name: "Deprecate",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      { indexed: false, name: "feeBasisPoints", type: "uint256" },
      { indexed: false, name: "maxFee", type: "uint256" },
    ],
    name: "Params",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      { indexed: false, name: "_blackListedUser", type: "address" },
      { indexed: false, name: "_balance", type: "uint256" },
    ],
    name: "DestroyedBlackFunds",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [{ indexed: false, name: "_user", type: "address" }],
    name: "AddedBlackList",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [{ indexed: false, name: "_user", type: "address" }],
    name: "RemovedBlackList",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      { indexed: true, name: "owner", type: "address" },
      { indexed: true, name: "spender", type: "address" },
      { indexed: false, name: "value", type: "uint256" },
    ],
    name: "Approval",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      { indexed: true, name: "from", type: "address" },
      { indexed: true, name: "to", type: "address" },
      { indexed: false, name: "value", type: "uint256" },
    ],
    name: "Transfer",
    type: "event",
  },
  { anonymous: false, inputs: [], name: "Pause", type: "event" },
  { anonymous: false, inputs: [], name: "Unpause", type: "event" },
];

let contract = new web3.eth.Contract(minABI, contractAddr);

app.get("/test", function (req, res) {
  console.log("in route function");
  res.json({ status: true });
});
app.post("/ethnode", function (req, res) {
  // console.log(req.body);
  var type = req.body.type;
  if (type == "getnewaddress") {
    var account = web3.eth.accounts.create();
    res.json(account);
  } else if (type == "getbalance") {
    var ethaddress = req.body.ethaddress;
    console.log(ethaddress, "ethaddress");
    web3.eth.getBalance(ethaddress, (err, balance) => {
      console.log(balance, "balance");
      var balance = web3.utils.fromWei(balance, "ether");
      res.json({ result: balance });
    });
  } else if (type == "listtransactions") {
    var argmns = [];
  } else if (type == "sendtoaddress") {
    var account1 = req.body.account1;
    var userprivatekey = req.body.privkey;
    var cryptoPass = req.body.cryptoPass;
    var useraddress = req.body.adminaddress;
    var amount = req.body.amount;

    web3.eth.getBalance(useraddress, (err, balance) => {
      var balance = web3.utils.fromWei(balance, "ether");
      var kjhkhkhkh = amount;
      if (balance >= kjhkhkhkh) {
        web3.eth.getGasPrice(function (err, getGasPrice) {
          web3.eth.getTransactionCount(useraddress, (err, txCount) => {
            var gaslimit = web3.utils.toHex(500000);
            var fee = web3.utils.toHex(getGasPrice) * gaslimit;
            // var amount = balance - fee;

            if (kjhkhkhkh > 0) {
              var updateVal = {};
              // var amount = web3.utils.toWei(kjhkhkhkh.toString(),'hex');
              var amount = web3.utils.toHex(
                web3.utils.toWei(kjhkhkhkh.toString(), "ether")
              );
              console.log(amount, "amountamount");
              const txObject = {
                nonce: web3.utils.toHex(txCount),
                gasLimit: web3.utils.toHex(gaslimit),
                gasPrice: web3.utils.toHex(getGasPrice),
                to: account1.toString(),
                from: useraddress,
                value: amount,
              };

              var userprivatekey1 = Buffer.from(userprivatekey, "hex");

              const tx = new Tx(txObject, { chain: "mainnet" });
              tx.sign(userprivatekey1);
              const serializedTx = tx.serialize();
              console.log(serializedTx);
              const raw1 = "0x" + serializedTx.toString("hex");
              console.log(raw1);
              web3.eth.sendSignedTransaction(raw1, (err, txHash) => {
                console.log(txHash);
                console.log(err);
                res.json({ status: true, txHash: txHash });
              });
            } else {
              console.log("no balance");
            }
          });
        });
      } else {
        res.json({ status: false, message: "Insuffient funds" });
      }
    });
  } else if (type == "movetoadmin") {
    var account1 = req.body.adminaddress;
    var privkey = req.body.privkey;
    var cryptoPass = req.body.cryptoPass;
    var useraddress = req.body.useraddress;
    console.log(useraddress, "useraddress");
    var decrypted = CryptoJS.AES.decrypt(privkey.toString(), cryptoPass);
    var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
    var userprivatekey = decryptedData.substring(2);
    web3.eth.getBalance(useraddress, (err, balance) => {
      // console.log(balance,'fsdsdfds')
      web3.eth.getGasPrice(function (err, getGasPrice) {
        web3.eth.getTransactionCount(useraddress, (err, txCount) => {
          console.log(getGasPrice, "getGasPrice");
          var gaslimit = web3.utils.toHex(21000);
          var fee = web3.utils.toHex(getGasPrice) * gaslimit;
          // console.log(fee,'fee')
          var amount = balance - fee;
          // console.log(amount,'amount')
          if (amount > 0) {
            var updateVal = {};
            const txObject = {
              nonce: web3.utils.toHex(txCount),
              gasLimit: web3.utils.toHex(gaslimit),
              gasPrice: web3.utils.toHex(getGasPrice),
              to: account1.toString(),

              value: amount,
            };

            // console.log(txObject);
            var userprivatekey1 = Buffer.from(userprivatekey, "hex");
            const tx = new Tx(txObject);
            tx.sign(userprivatekey1);
            const serializedTx = tx.serialize();
            // console.log(serializedTx);
            const raw1 = "0x" + serializedTx.toString("hex");
            // console.log(raw1);
            web3.eth.sendSignedTransaction(raw1, (err, txHash) => {
              console.log(err, "err");
              console.log(txHash, "txHash");
              var recamount = web3.utils.fromWei(amount.toString(), "ether");
              res.json({ txHash: txHash, recamount: recamount });
            });
          } else {
            console.log("no balance");
          }
        });
      });
    });
  } else if (type == "usdtsendtoaddress") {
    console.log(req.body, "req.body in sudt send");
    var account1 = req.body.account1;
    var privkey = req.body.privkey;
    var cryptoPass = req.body.cryptoPass;
    var useraddress = req.body.adminaddress;
    var tokenamount = req.body.amount;

    web3.eth.getBalance(useraddress, (err, balance) => {
      console.log(balance, "fsdsdfds");
      web3.eth.getGasPrice(function (err, getGasPrice) {
        web3.eth.getTransactionCount(useraddress, (err, txCount) => {
          console.log(getGasPrice, "getGasPrice");
          var gaslimit = web3.utils.toHex(21000);
          var fee = web3.utils.toHex(getGasPrice) * gaslimit;
          console.log(fee, "fee");
          var amount = balance - fee;
          console.log(amount, "amount");
          if (amount > 0) {
            console.log("in balance");
            // var tokenamount =  web3.utils.toHex(web3.utils.toWei(req.body.amount.toString(),'ether'));
            var tokenamount = parseFloat(req.body.amount) * 1000000;
            console.log(tokenamount, "token amount");
            var data = contract.methods
              .transfer(account1, tokenamount)
              .encodeABI();
            let transactionObject = {
              gasLimit: web3.utils.toHex(200000),
              gasPrice: web3.utils.toHex(getGasPrice),
              // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
              data: data,
              nonce: web3.utils.toHex(txCount),
              from: useraddress,
              to: contractAddr,
              value: "0x0",
              chainId: 1,
              // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
            };
            console.log(privkey, "privkeyprivkey");
            console.log(transactionObject, "transactionObject");

            var userprivatekey1 = Buffer.from(privkey, "hex");

            const tx = new Tx(transactionObject, { chain: "mainnet" });

            tx.sign(userprivatekey1);
            const serializedTx = tx.serialize();
            const raw1 = "0x" + serializedTx.toString("hex");

            web3.eth
              .sendSignedTransaction(raw1)
              .once("receipt", function (receipt) {
                console.log(receipt);
              })
              .then(function (receipt) {
                if (receipt) {
                  res.json({
                    status: true,
                    message: "Succefully transfer",
                    txHash: receipt.transactionHash,
                  });
                } else {
                  console.log("Errorr in sending", err);
                }
              });

            // web3.eth.accounts.signTransaction(transactionObject, privkey, function (error, signedTx) {
            // if (error) {
            // console.log(error);
            // // handle error
            // } else {

            // web3.eth.sendSignedTransaction(signedTx.rawTransaction)
            // .on('receipt', function (receipt) {
            // 	console.log(receipt,'receipt')
            // 	res.json({status:true,message:"Succefully transfer",txHash:receipt.blockHash})
            //     //do something
            // });

            // }
            // });
          } else {
            console.log("no balance");
          }
        });
      });
    });
  } else if (type == "tokenupdation") {
    var currencyAddress = req.body.currencyAddress;
    var privKey = req.body.privKey;
    var cryptoPass = req.body.cryptoPass;
    var userAddress = req.body.userAddress;
    var userprivatekey = req.body.userprivatekey;
    var decimals=req.body.decimals;
    console.log(req.body, "req.body in token updation");
    contract.methods
      .balanceOf(currencyAddress)
      .call(function (err, tokenbalance) {
        console.log(err);
        var muldecimal;
        if(decimals==1){
           muldecimal=10
        }else if (decimals==2) {
          muldecimal=100
        }
        else if (decimals==4) {
          muldecimal=10000
        }
        else if(decimals==8){
            muldecimal=100000000
        }
        else if(decimals==18){
            muldecimal=1000000000000000000
        }
        var realtokenbalance = tokenbalance;
        // var tokenbalnce = web3.utils.fromWei(tokenbalance, "ether");

        var tokenbalnce = parseFloat(tokenbalance) * muldecimal;

        console.log(tokenbalance, "tokenbalnce");
        if (tokenbalance > 0) {
          var account = currencyAddress;

          web3.eth.getBalance(account, (err, balance) => {
            console.log(balance, "jhhk");
            // return false;
            const accountNonce = (
              web3.eth.getTransactionCount(userAddress) + 1
            ).toString(16);
            console.log("in balance");
            web3.eth.getTransactionCount(account, (err, txCount) => {
              web3.eth.getGasPrice(function (err, getGasPrice) {
                var gaslimit = web3.utils.toHex(500000);
                var fee = web3.utils.toHex(getGasPrice) * gaslimit;
                console.log(fee, "feeeeeee");
                if (balance > fee) {
                  var tokenamount = web3.utils.toHex(
                    web3.utils.toWei(tokenbalnce.toString(), "ether")
                  );
                  console.log(tokenamount, "token amount");
                  var data = contract.methods
                    .transfer(userAddress, tokenamount)
                    .encodeABI();
                  let transactionObject = {
                    gasLimit: web3.utils.toHex(50000),
                    gasPrice: web3.utils.toHex(getGasPrice),
                    // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
                    data: data,
                    nonce: txCount,
                    from: account,
                    to: contractAddr,
                    // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
                  };
                  var decrypted = CryptoJS.AES.decrypt(
                    privKey.toString(),
                    cryptoPass
                  );
                  var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
                  var userprivatek = decryptedData.substring(2);
                  web3.eth.accounts.signTransaction(
                    transactionObject,
                    userprivatek,
                    function (error, signedTx) {
                      if (error) {
                        console.log(error);
                        // handle error
                      } else {
                        web3.eth
                          .sendSignedTransaction(signedTx.rawTransaction)
                          .on("receipt", function (receipt) {
                            console.log(receipt, "receipt");
                            res.json({
                              status: true,
                              message: "Succefully transfer",
                              tokenbalnce: realtokenbalance,
                              txHash: receipt.blockHash,
                            });
                            //do something
                          });
                      }
                    }
                  );
                } else {
                  console.log("no balance");
                  web3.eth.getTransactionCount(userAddress, (err, txCount) => {
                    web3.eth.getGasPrice(function (err, getGasPrice) {
                      var gaslimit = web3.utils.toHex(50000);
                      var fee = web3.utils.toHex(getGasPrice) * gaslimit;

                      fee =
                        parseFloat(fee - balance) +
                        parseFloat(web3.utils.toWei("0.00001", "ether"));
                      let transactionObject = {
                        gasLimit: web3.utils.toHex(500000),
                        gasPrice: web3.utils.toHex(getGasPrice),
                        nonce: txCount,
                        to: account,
                        value: fee,
                      };
                      var adminprivkey = req.body.userprivatekey;
                      console.log(adminprivkey, "userprivatekey55");
                      console.log(req.body.cryptoPass, "cryptoPass");

                      var decrypted = CryptoJS.AES.decrypt(
                        adminprivkey.toString(),
                        req.body.cryptoPass
                      );
                      var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
                      var adminprivkey = decryptedData.substring(2);
                      console.log(adminprivkey);
                      var userprivatekey1 = Buffer.from(adminprivkey, "hex");

                      const tx = new Tx(transactionObject, {
                        chain: "mainnet",
                      });
                      tx.sign(userprivatekey1);
                      const serializedTx = tx.serialize();
                      console.log(serializedTx);
                      const raw1 = "0x" + serializedTx.toString("hex");
                      console.log(raw1);

                      web3.eth
                        .sendSignedTransaction(raw1)
                        .on("receipt", function (receipt) {
                          console.log(receipt, "receipt");
                        })
                        .then(function (receipt) {
                          console.log("in balance");
                          web3.eth.getTransactionCount(
                            account,
                            (err, txCount) => {
                              web3.eth.getGasPrice(function (err, getGasPrice) {
                                var tokenamount = web3.utils.toHex(
                                  web3.utils.toWei(
                                    tokenbalnce.toString(),
                                    "ether"
                                  )
                                );
                                console.log(tokenamount, "token amount");
                                var data = contract.methods
                                  .transfer(userAddress, tokenamount)
                                  .encodeABI();
                                let transactionObject = {
                                  gasLimit: web3.utils.toHex(500000),
                                  gasPrice: web3.utils.toHex(getGasPrice),
                                  // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
                                  data: data,
                                  nonce: txCount,
                                  from: account,
                                  to: contractAddr,
                                  // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
                                };
                                var decrypted = CryptoJS.AES.decrypt(
                                  privKey.toString(),
                                  req.body.cryptoPass
                                );
                                var decryptedData = decrypted.toString(
                                  CryptoJS.enc.Utf8
                                );
                                var userprivatekey = decryptedData.substring(2);
                                web3.eth.accounts.signTransaction(
                                  transactionObject,
                                  userprivatekey,
                                  function (error, signedTx) {
                                    if (error) {
                                      console.log(error);
                                      // handle error
                                    } else {
                                      web3.eth
                                        .sendSignedTransaction(
                                          signedTx.rawTransaction
                                        )
                                        .on("receipt", function (receipt) {
                                          console.log(receipt, "receipt");
                                          res.json({
                                            status: true,
                                            message: "Succefully transfer",
                                            tokenbalnce: realtokenbalance,
                                            txHash: receipt.blockHash,
                                          });

                                          //do something
                                        });
                                    }
                                  }
                                );
                              });
                            }
                          );
                        });
                    });
                  });
                }
              });
            });
          });
        } else {
          console.log("else part");
          res.json({ status: false, message: "There is no new deposit" });
        }
      });
  } else if (type == "tokentoadmin") {
    // console.log(req.body, "req.body in move token");
var curminabi=JSON.parse(req.body.curminabi)
    var currencyAddress = req.body.currencyAddress;
    var privKey = req.body.privKey;
    var cryptoPass = req.body.cryptoPass;
    var userAddress = req.body.userAddress;
    var userprivatekey = req.body.userprivatekey;
    var curcontractaddress = req.body.curcontractaddress;
    var decimals=req.body.decimals;

    // var curminabi = req.body.curminabi;
    let contract = new web3.eth.Contract(curminabi, curcontractaddress);
    contract.methods
      .balanceOf(currencyAddress)
      .call(function (err, tokenbalance) {
        console.log(err);
        var realtokenbalance = tokenbalance;
        var muldecimal=2;
        if(decimals==1){
           muldecimal=10
        }else if (decimals==2) {
          muldecimal=100
        }
        else if (decimals==4) {
          muldecimal=10000
        }
        else if (decimals==6){
            muldecimal=1000000
        }
        else if(decimals==8){
            muldecimal=100000000
        }
        console.log("tokenbalance",tokenbalance);
        // var tokenbalnce = web3.utils.fromWei(tokenbalance, "ether");

        var tokenbalnce = parseFloat(tokenbalance) * parseFloat(muldecimal);
        // var tokenbalnce = web3.utils.fromWei(tokenbalance, "ether");
        console.log(tokenbalnce, "tokenbalnce");
        if (tokenbalance > 0) {
          var account = currencyAddress;

          web3.eth.getBalance(account, (err, balance) => {
            console.log(balance, "jhhk");
            // return false;
            const accountNonce = (
              web3.eth.getTransactionCount(userAddress) + 1
            ).toString(16);
            console.log("in balance");
            web3.eth.getTransactionCount(account, (err, txCount) => {
              web3.eth.getGasPrice(function (err, getGasPrice) {
                var gaslimit = web3.utils.toHex(500000);
                var fee = web3.utils.toHex(getGasPrice) * gaslimit;
                console.log(fee, "feeeeeee");
                if (balance > fee) {
                  var tokenamount = web3.utils.toHex(
                    web3.utils.toWei(tokenbalnce.toString(), "ether")
                  );
                  console.log(tokenamount, "token amount");
                  var data = contract.methods
                    .transfer(userAddress, tokenbalance)
                    .encodeABI();
                    console.log("testing on token");
                  let transactionObject = {
                    gasLimit: web3.utils.toHex(500000),
                    gasPrice: web3.utils.toHex(getGasPrice),
                    // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
                    data: data,
                    nonce: txCount,
                    from: account,
                    to: curcontractaddress,
                    // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
                  };
                  var decrypted = CryptoJS.AES.decrypt(
                    privKey.toString(),
                    cryptoPass
                  );
                  var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
                  var userprivatek = decryptedData.substring(2);
                  web3.eth.accounts.signTransaction(
                    transactionObject,
                    userprivatek,
                    function (error, signedTx) {
                      if (error) {
                        console.log(error);
                        // handle error
                      } else {
                        web3.eth
                          .sendSignedTransaction(signedTx.rawTransaction)
                          .on("receipt", function (receipt) {
                            console.log(receipt, "receipt");
                            res.json({
                              status: true,
                              message: "Succefully transfer",
                              tokenbalnce: realtokenbalance,
                              txHash: receipt.blockHash,
                            });
                            //do something
                          });
                      }
                    }
                  );
                } else {
                  console.log("no balance");
                  web3.eth.getTransactionCount(userAddress, (err, txCount) => {
                    web3.eth.getGasPrice(function (err, getGasPrice) {
                      var gaslimit = web3.utils.toHex(500000);
                      var fee = web3.utils.toHex(getGasPrice) * gaslimit;

                      fee =
                        parseFloat(fee - balance) +
                        parseFloat(web3.utils.toWei("0.00001", "ether"));
                      let transactionObject = {
                        gasLimit: web3.utils.toHex(500000),
                        gasPrice: web3.utils.toHex(getGasPrice),
                        nonce: txCount,
                        to: account,
                        value: fee,
                      };
                      var adminprivkey = req.body.userprivatekey;
                      console.log(adminprivkey, "userprivatekey55");
                      // console.log(req.body.cryptoPass, "cryptoPass");

                      var decrypted = CryptoJS.AES.decrypt(
                        adminprivkey.toString(),
                        req.body.cryptoPass
                      );
                      var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
                      var adminprivkey = decryptedData.substring(2);
                      // console.log(adminprivkey);
                      var userprivatekey1 = Buffer.from(adminprivkey, "hex");

                      const tx = new Tx(transactionObject, {
                        chain: "mainnet",
                      });
                      tx.sign(userprivatekey1);
                      const serializedTx = tx.serialize();
                      console.log(serializedTx);
                      const raw1 = "0x" + serializedTx.toString("hex");
                      console.log(raw1);

                      web3.eth
                        .sendSignedTransaction(raw1)
                        .on("receipt", function (receipt) {
                          console.log(receipt, "receipt");
                        })
                        .then(function (receipt) {
                          console.log("in balance");
                          web3.eth.getTransactionCount(
                            account,
                            (err, txCount) => {
                              web3.eth.getGasPrice(function (err, getGasPrice) {
                                var tokenamount = web3.utils.toHex(
                                  web3.utils.toWei(
                                    tokenbalnce.toString(),
                                    "ether"
                                  )
                                );
                                console.log(tokenamount, "token amount");
                                var data = contract.methods
                                  .transfer(userAddress, tokenbalance)
                                  .encodeABI();
                                let transactionObject = {
                                  gasLimit: web3.utils.toHex(500000),
                                  gasPrice: web3.utils.toHex(getGasPrice),
                                  // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
                                  data: data,
                                  nonce: txCount,
                                  from: account,
                                  to: curcontractaddress,
                                  // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
                                };
                                var decrypted = CryptoJS.AES.decrypt(
                                  privKey.toString(),
                                  req.body.cryptoPass
                                );
                                var decryptedData = decrypted.toString(
                                  CryptoJS.enc.Utf8
                                );
                                var userprivatekey = decryptedData.substring(2);
                                web3.eth.accounts.signTransaction(
                                  transactionObject,
                                  userprivatekey,
                                  function (error, signedTx) {
                                    if (error) {
                                      console.log(error);
                                      // handle error
                                    } else {
                                      web3.eth
                                        .sendSignedTransaction(
                                          signedTx.rawTransaction
                                        )
                                        .on("receipt", function (receipt) {
                                          console.log(receipt, "receipt");
                                          res.json({
                                            status: true,
                                            message: "Succefully transfer",
                                            tokenbalnce: realtokenbalance,
                                            txHash: receipt.transactionHash,
                                          });

                                          //do something
                                        });
                                    }
                                  }
                                );
                              });
                            }
                          );
                        });
                    });
                  });
                }
              });
            });
          });
        } else {
          console.log("else part");
          res.json({ status: false, message: "There is no new deposit" });
        }
      });
  } else if (type == "sendtokentouser") {
    // console.log(req.body, "req.body in move token");
var curminabi=JSON.parse(req.body.curminabi)
    var currencyAddress = req.body.currencyAddress;
    var privKey = req.body.privKey;
    var cryptoPass = req.body.cryptoPass;
    var userAddress = req.body.userAddress;
    var userprivatekey = req.body.userprivatekey;
    var curcontractaddress = req.body.curcontractaddress;
    var decimals=req.body.decimals;
    var tokentosend=req.body.amount;

    // var curminabi = req.body.curminabi;
    let contract = new web3.eth.Contract(curminabi, curcontractaddress);
    contract.methods
      .balanceOf(currencyAddress)
      .call(function (err, tokenbalance) {
        console.log(err);
        var realtokenbalance = tokenbalance;
        var muldecimal=2;
        if(decimals==1){
           muldecimal=10
        }else if (decimals==2) {
          muldecimal=100
        }
        else if (decimals==4) {
          muldecimal=10000
        }
        else if (decimals==6){
            muldecimal=1000000
        }
        else if(decimals==8){
            muldecimal=100000000
        }
        console.log("tokentosend",tokentosend);
        console.log("muldecimal",muldecimal);

        // var tokenbalnce = web3.utils.fromWei(tokenbalance, "ether");

        var tokenbalnce = parseFloat(tokentosend) * parseFloat(muldecimal);
        // var tokenbalnce = web3.utils.fromWei(tokenbalance, "ether");
        console.log(tokenbalnce, "tokenbalnce");
        if (tokenbalance > 0) {
          var account = currencyAddress;

          web3.eth.getBalance(account, (err, balance) => {
            console.log(balance, "jhhk");
            // return false;
            const accountNonce = (
              web3.eth.getTransactionCount(userAddress) + 1
            ).toString(16);
            console.log("in balance");
            web3.eth.getTransactionCount(account, (err, txCount) => {
              console.log("txCounttxCount",txCount);
              console.log("txCounttxCount",typeof txCount);
              web3.eth.getGasPrice(function (err, getGasPrice) {
                var gaslimit = web3.utils.toHex(500000);
                var fee = web3.utils.toHex(getGasPrice) * gaslimit;
                console.log(fee, "feeeeeee");
                if (balance > fee) {
                  var tokenamount = web3.utils.toHex(
                    web3.utils.toWei(tokenbalnce.toString(), "ether")
                  );
                  console.log(tokenamount, "token amount");
                  var data = contract.methods
                    .transfer(userAddress, tokenbalnce)
                    .encodeABI();
                    console.log("testing on token");
                  let transactionObject = {
                    gasLimit: web3.utils.toHex(500000),
                    gasPrice: web3.utils.toHex(getGasPrice),
                    // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
                    data: data,
                    nonce: txCount,
                    from: account,
                    to: curcontractaddress,
                    // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
                  };
                  var decrypted = CryptoJS.AES.decrypt(
                    privKey.toString(),
                    cryptoPass
                  );
                  var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
                  var userprivatek = decryptedData.substring(2);




                  var userprivatekey1 = Buffer.from(userprivatek, "hex");

                  const tx = new Tx(transactionObject);
                  tx.sign(userprivatekey1);
                  const serializedTx = tx.serialize();
                  const raw1 = "0x" + serializedTx.toString("hex");
                  web3.eth.sendSignedTransaction(raw1, (err, txHash) => {
                    console.log("txhash ", txHash);
                    res.json({ status: true, txHash: txHash });
                    if (err) {
                      console.log("Errorr in sending", err);
                    }
                  });

                  // web3.eth.accounts.signTransaction(
                  //   transactionObject,
                  //   userprivatek,
                  //   function (error, signedTx) {
                  //     if (error) {
                  //       console.log(error);
                  //       // handle error
                  //     } else {
                  //       web3.eth
                  //         .sendSignedTransaction(signedTx.rawTransaction)
                  //         .on("receipt", function (receipt) {
                  //           console.log(receipt, "receipt");
                  //           res.json({
                  //             status: true,
                  //             message: "Succefully transfer",
                  //             tokenbalnce: realtokenbalance,
                  //             txHash: receipt.transactionHash,
                  //           });
                  //           //do something
                  //         });
                  //
                  //
                  //     }
                  //   }
                  // );
                } else{
                  console.log("no balance");
                }
                //
                // else {
                //   console.log("no balance");
                //   web3.eth.getTransactionCount(userAddress, (err, txCount) => {
                //     web3.eth.getGasPrice(function (err, getGasPrice) {
                //       var gaslimit = web3.utils.toHex(500000);
                //       var fee = web3.utils.toHex(getGasPrice) * gaslimit;
                //
                //       fee =
                //         parseFloat(fee - balance) +
                //         parseFloat(web3.utils.toWei("0.00001", "ether"));
                //       let transactionObject = {
                //         gasLimit: web3.utils.toHex(500000),
                //         gasPrice: web3.utils.toHex(getGasPrice),
                //         nonce: txCount,
                //         to: account,
                //         value: fee,
                //       };
                //       var adminprivkey = req.body.userprivatekey;
                //       console.log(adminprivkey, "userprivatekey55");
                //       // console.log(req.body.cryptoPass, "cryptoPass");
                //
                //       var decrypted = CryptoJS.AES.decrypt(
                //         adminprivkey.toString(),
                //         req.body.cryptoPass
                //       );
                //       var decryptedData = decrypted.toString(CryptoJS.enc.Utf8);
                //       var adminprivkey = decryptedData.substring(2);
                //       // console.log(adminprivkey);
                //       var userprivatekey1 = Buffer.from(adminprivkey, "hex");
                //
                //       const tx = new Tx(transactionObject, {
                //         chain: "mainnet",
                //       });
                //       tx.sign(userprivatekey1);
                //       const serializedTx = tx.serialize();
                //       console.log(serializedTx);
                //       const raw1 = "0x" + serializedTx.toString("hex");
                //       console.log(raw1);
                //
                //       web3.eth
                //         .sendSignedTransaction(raw1)
                //         .on("receipt", function (receipt) {
                //           console.log(receipt, "receipt");
                //         })
                //         .then(function (receipt) {
                //           console.log("in balance");
                //           web3.eth.getTransactionCount(
                //             account,
                //             (err, txCount) => {
                //               web3.eth.getGasPrice(function (err, getGasPrice) {
                //                 var tokenamount = web3.utils.toHex(
                //                   web3.utils.toWei(
                //                     tokenbalnce.toString(),
                //                     "ether"
                //                   )
                //                 );
                //                 console.log(tokenamount, "token amount");
                //                 var data = contract.methods
                //                   .transfer(userAddress, tokenamount)
                //                   .encodeABI();
                //                 let transactionObject = {
                //                   gasLimit: web3.utils.toHex(500000),
                //                   gasPrice: web3.utils.toHex(getGasPrice),
                //                   // gasPrice : web3.utils.toHex(web3.utils.toWei('5','gwei')),
                //                   data: data,
                //                   nonce: txCount,
                //                   from: account,
                //                   to: curcontractaddress,
                //                   // value:web3.utils.toHex(web3.utils.toWei(tokenamount,'ether'))
                //                 };
                //                 var decrypted = CryptoJS.AES.decrypt(
                //                   privKey.toString(),
                //                   req.body.cryptoPass
                //                 );
                //                 var decryptedData = decrypted.toString(
                //                   CryptoJS.enc.Utf8
                //                 );
                //                 var userprivatekey = decryptedData.substring(2);
                //                 web3.eth.accounts.signTransaction(
                //                   transactionObject,
                //                   userprivatekey,
                //                   function (error, signedTx) {
                //                     if (error) {
                //                       console.log(error);
                //                       // handle error
                //                     } else {
                //                       web3.eth
                //                         .sendSignedTransaction(
                //                           signedTx.rawTransaction
                //                         )
                //                         .on("receipt", function (receipt) {
                //                           console.log(receipt, "receipt");
                //                           res.json({
                //                             status: true,
                //                             message: "Succefully transfer",
                //                             tokenbalnce: realtokenbalance,
                //                             txHash: receipt.transactionHash,
                //                           });
                //
                //                           //do something
                //                         });
                //                     }
                //                   }
                //                 );
                //               });
                //             }
                //           );
                //         });
                //     });
                //   });
                // }
              });
            });
          });
        } else {
          console.log("else part");
          res.json({ status: false, message: "There is no new deposit" });
        }
      });
  }
});
app.listen(3000, function () {
  console.log("Example app listening on port 3000!");
});
