const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const async = require("async");
const validateTradeInput = require('../../validation/frontend/trade');
const validatemobRegisterInput = require('../../validation/frontend/mobregister');
const validateLoginInput = require('../../validation/login');
const validatemobLoginInput = require('../../validation/moblogin');
const validateUpdateUserInput = require('../../validation/frontend/updateUser');
const validateEmailtemplateInput = require('../../validation/emailtemplate');
const validateForgotInput = require('../../validation/forgot');
const validateCmsInput = require('../../validation/cms');
const validateFaqInput = require('../../validation/faq');
const validateUpdateSettingsInput = require('../../validation/settings');
const validateResetInput = require('../../validation/frontend/resetpassword');
const validatetfaInput = require('../../validation/frontend/tfainput');
const validateContactInput = require('../../validation/frontend/contact_us');
const spottradeTable = require('../../models/spottradeTable');
const charts = require('../../models/Chart');
const FeeTable = require('../../models/FeeTable');
const Bonus = require('../../models/Bonus');
const Assets = require('../../models/Assets');
const position_table = require('../../models/position_table');
const currency = require('../../models/currency');
const User = require('../../models/User');
const FundingHistory = require('../../models/FundingHistory');
const InterestHistory = require('../../models/InterestHistory');
const Emailtemplates = require('../../models/emailtemplate');
const exchangePrices = require('../../models/exchangePrices');
const spotPrices = require('../../models/spotPrices');
const spotpairs = require('../../models/spotpairs');
const multer = require('multer');
var node2fa = require('node-2fa');
var CryptoJS = require("crypto-js");
var moment = require("moment");
const perpetual = require('../../models/perpetual');
const cryptoRandomString = require('crypto-random-string');
const nodemailer = require('nodemailer');
var fs = require('fs');
const client = require('twilio')(
  keys.TWILIO_ACCOUT_SID,
  keys.TWILIO_AUTH_TOKEN
);
const mongoose = require('mongoose');
const url = require('url');
const ObjectId = mongoose.Types.ObjectId;
var symbolsDatabase = require("../symbols_database"),
  RequestProcessor = require("../request-processor").RequestProcessor;
var requestProcessor = new RequestProcessor(symbolsDatabase);
var schedule = require('node-schedule');

var cron = require('node-cron');

var request = require('request');
const userinfo = [];
var tradeinfo = [];
const rp = require('request-promise');
const perf = require("execution-time")();

//
// //Kraken API section
// const ws = new WebSocket('wss://ws.kraken.com/');
// ws.on('message', function incoming(data) {
//   // console.log(JSON.parse(data),'hsdkfhksdhfkshdfkhsdf');
//   // console.log(JSON.parse(data)[3],'hsdkfhksdhfkshdfkhsdf');
//   var low   = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].l:'';
//   var last  = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].a:'';
//   var high  = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].h:'';
//   var open  = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].o:'';
//   var close = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].c:'';
//   var volume = typeof(JSON.parse(data)[1])!='undefined'?JSON.parse(data)[1].v:'';
//   var pairname = typeof(JSON.parse(data)[3])!='undefined'?JSON.parse(data)[3].replace("/",""):'';
//   if(typeof(JSON.parse(data)[3])!='undefined')
//   {
//     var updatedata = {
//       "low"          : (low)?low[0]:0,
//       "high"         : (high)?high[0]:0,
//       "exchangename" : "Kraken",
//       "last"         : (last)?last[0]:0,
//       "pairname"     : (pairname=='XBTUSD')?'BTCUSD':pairname
//     };
//
//     var pairname = (pairname=='XBTUSD')?'BTCUSD':pairname;
//
//     updatebaldata = {"volume":(volume)?volume[0]:0,}
//
//   exchangePrices.findOneAndUpdate({exchangename:"Kraken","pairname":pairname},{"$set": updatedata ,"$inc": updatebaldata } , {new:true,"fields": {exchangename:1} } ,function(balerr,baldata){
//     // console.log(balerr,'balerr');
//     // console.log(baldata,'baldata');
//   });
//   }
// });
//
// ws.on('open', function open() {
//   ws.send(JSON.stringify({
//   "event": "subscribe",
//   "pair": ["XBT/USD", "ETH/USD", "XRP/USD", "BCH/USD", "LTC/USD"],
//   "subscription": {"name": "ticker"}
// }))
// });
//
// //Coinbase pro API section
// const ws1 = new WebSocket("wss://ws-feed.pro.coinbase.com");
//
// ws1.onopen = function () {
//   ws1.send(JSON.stringify({
//     "type": "subscribe",
//     "product_ids": [
//         "BTC-USD",
//         "ETH-USD",
//         "LTC-USD",
//         "XRP-USD",
//         "BCH-USD",
//     ],
//     "channels": [
//         "level2",
//         "heartbeat",
//         {
//             "name": "ticker",
//             "product_ids": [
//                 "BTC-USD",
//                 "ETH-USD",
//                 "LTC-USD",
//                 "XRP-USD",
//                 "BCH-USD",
//             ]
//         }
//     ]
// }));
// };
//
// ws1.on('message', function incoming(data) {
//   // console.log(JSON.parse(data),'gemini');
//   var result = JSON.parse(data);
//   if(typeof result.type!='undefined' && result.type=='l2update')
//   {
//     var product_id = typeof result.product_id != 'undefined'?result.product_id:'';
//     var changes = typeof result.changes != 'undefined'?result.changes[0]:'';
//     var updatedata = {"exchangename" : "Coinbasepro","last" : (changes[1])?changes[1]:0,"pairname" : product_id?product_id.replace("-",""):''};
//
//     updatebaldata = {"volume":(changes[2])?changes[2]:0}
//
//     exchangePrices.findOneAndUpdate({exchangename:"Coinbasepro","pairname":product_id?product_id.replace("-",""):''},{"$set": updatedata ,"$inc": updatebaldata } , {new:true,"fields": {exchangename:1} } ,function(balerr,baldata){
//       // console.log(baldata,'baldata')
//     });
//   }
//
// });
//
// //Bitstamp API section
// const ws2 = new WebSocket("wss://ws.bitstamp.net");
//
// var subscribeMsg = {
//         "event": "bts:subscribe",
//         "data": {
//             "channel": "live_trades_btcusd"
//         }
//     };
//
// var subscribeMsg1 = {
//         "event": "bts:subscribe",
//         "data": {
//             "channel": "live_trades_ethusd"
//         }
//     };
// var subscribeMsg2 = {
//         "event": "bts:subscribe",
//         "data": {
//             "channel": "live_trades_xrpusd"
//         }
//     };
// var subscribeMsg3 = {
//         "event": "bts:subscribe",
//         "data": {
//             "channel": "live_trades_bchusd"
//         }
//     };
//     var subscribeMsg4 = {
//         "event": "bts:subscribe",
//         "data": {
//             "channel": "live_trades_ltcusd"
//         }
//     };
//
// ws2.onopen = function () {
//   ws2.send(JSON.stringify(subscribeMsg));
//   ws2.send(JSON.stringify(subscribeMsg1));
//   ws2.send(JSON.stringify(subscribeMsg2));
//   ws2.send(JSON.stringify(subscribeMsg3));
//   ws2.send(JSON.stringify(subscribeMsg4));
// };
//
// ws2.on('message', function incoming(data) {
// // console.log(JSON.parse(data),'dddddddddd');
//   var result = JSON.parse(data).data;
//   if(typeof result != 'undefined' && typeof result.amount_str != 'undefined')
//   {
//       var channel = typeof JSON.parse(data).channel!= 'undefined'?JSON.parse(data).channel:'';
//       var splitar = channel.split('_');
//
//       var updatedata = {
//       "exchangename" : "Bitstamp",
//       "last"         : (result.price)?result.price:0,
//       "pairname"     : typeof splitar[2]!='undefined'?splitar[2].toUpperCase():''
//       };
//
//       updatebaldata = {"volume":(result.amount_str)?result.amount_str:0}
//
//       exchangePrices.findOneAndUpdate({exchangename:"Bitstamp","pairname":typeof splitar[2]!='undefined'?splitar[2].toUpperCase():''},{"$set": updatedata ,"$inc": updatebaldata } , {new:true,"fields": {exchangename:1} } ,function(balerr,baldata){
//
//         // console.log(baldata,'baldata')
//     });
//   }
// });


router.post('/getUserIdBYassets', (req, res) => {
  User.find({}, async function(err, UserDEtailsbyid) {
    UserDEtailsbyid.forEach(function (res) {
        var userId =   res._id;

     const newAssets = new Assets({
      userId: userId,
      balance: 0,
      currency: ObjectId("5ef1a7cfd2ea3374feac1b13"),
      currencySymbol: 'USD',
      currencyAddress: "",
    });

    newAssets.save(function (err, data) {
      // userinfo.callBackbuyposTrade();
      if (err) {
        console.log("Error in new assets add", err);
      }
    });

    })
    // console.log()
    if (UserDEtailsbyid) {


      res.json({
        status: true,
        data: UserDEtailsbyid
      })
    } else {
      res.json({
        status: false,
        message: "Something went wrong"
      })

    }
  });
});
async function set(){

}
router.get('/currencydetails', (req, res) => {
  currency.find({}, function(err, currencydetails) {
    if (currencydetails) {
      res.json({
        status: true,
        data: currencydetails
      })
    } else {
      res.json({
        status: false,
        message: "Something went wrong"
      })

    }
  });
});
router.post('/spotloadmoreRescentorder', (req, res) => {
  var pair = req.body.pair;
  var rescentcount = req.body.rescentcount;
  spottradeTable.aggregate([{
      $match: {
        'pairName': pair,
        'status': '1'
      }
    },
    {
      $unwind: "$filled"
    },
    {
      $project: {
        "filled": 1
      }
    },
    {
      $group: {
        _id: {
          "buyuserId": '$filled.buyuserId',
          "selluserId": '$filled.selluserId',
          "sellId": "$filled.sellId",
          "buyId": "$filled.buyId"
        },
        "created_at": {
          $first: "$filled.created_at"
        },
        "Type": {
          $first: "$filled.Type"
        },
        "filledAmount": {
          $first: "$filled.filledAmount"
        },
        "pairname": {
          $first: "$filled.pairname"
        },
        "Price": {
          $first: "$filled.Price"
        },
        "Type": {
          $first: "$filled.Type"
        }
      }
    },
    {
      $sort: {
        'created_at': -1
      }
    },
    // {$skip: rescentcount},
    {
      $limit: rescentcount
    },
  ]).exec(function(err, result) {
    res.json({
      status: true,
      data: result
    });
  });
});
router.get('/balance', (req, res) => {
  // res.json({statue:"success"});
  User.find({}, function(err, userdetails) {
    if (userdetails) {
      userdetails.forEach(function(res) {
        var userId = res._id;
        currency.find({}, function(err, currencydetails) {
          currencydetails.forEach(function(cur) {
            var insertobj = {
              "balance": 0,
              "currency": cur._id,
              "currencySymbol": cur.currencySymbol
            };

            const newContact = new Assets({
              "balance": 0,
              "currency": cur._id,
              "currencySymbol": cur.currencySymbol,
              "userId": userId
            });
            newContact.save(function(err, data) {
              console.log("success");
            });

          });
        });
      });
      res.send('success');

    }
  })
});

function gettradedata(firstCurrency, secondCurrency, io) {
  var findObj = {
    firstCurrency: firstCurrency,
    secondCurrency: secondCurrency
  };
  var pair = firstCurrency + secondCurrency;
  var result = {};
  // tradeTable.find(findObj,function(err,tradeTableAll){
  async.parallel({
    buyOrder: function(cb) {
      var sort = {
        '_id': -1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: firstCurrency,
            secondCurrency: secondCurrency,
            buyorsell: 'buy'
          }
        },
        {
          $group: {
            '_id': '$price',
            'quantity': {
              $sum: '$quantity'
            },
            'filledAmount': {
              $sum: '$filledAmount'
            }
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 10
        },
      ]).allowDiskUse(true).exec(cb)
    },
    sellOrder: function(cb) {
      var sort = {
        '_id': 1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: firstCurrency,
            secondCurrency: secondCurrency,
            buyorsell: 'sell'
          }
        },
        {
          $group: {
            '_id': '$price',
            'quantity': {
              $sum: '$quantity'
            },
            'filledAmount': {
              $sum: '$filledAmount'
            }
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 10
        },
      ]).allowDiskUse(true).exec(cb)
    },
    sellsumvalue: function(cb) {
      var sort = {
        '_id': 1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: firstCurrency,
            secondCurrency: secondCurrency,
            buyorsell: 'sell'
          }
        },
        {
          $group: {
            _id: null,
            'quantity': {
              $sum: '$quantity'
            },
          }
        },
      ]).allowDiskUse(true).exec(cb)
    },
    buysumvalue: function(cb) {
      var sort = {
        '_id': 1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: firstCurrency,
            secondCurrency: secondCurrency,
            buyorsell: 'buy'
          }
        },
        {
          $group: {
            _id: null,
            'orderValue': {
              $sum: '$orderValue'
            },
          }
        },
      ]).allowDiskUse(true).exec(cb)
    },
    contractdetails: function(cb) {
      spotpairs.findOne({
        first_currency: firstCurrency,
        second_currency: secondCurrency
      }, {
        tiker_root: 1,
        maint_margin: 1,
        first_currency: 1,
        second_currency: 1
      }).exec(cb)
    },
    Rescentorder: function(cb) {
      spottradeTable.aggregate([{
          $match: {
            'pairName': pair,
            'status': '1'
          }
        },
        {
          $unwind: "$filled"
        },
        {
          $project: {
            "filled": 1
          }
        },
        {
          $group: {
            _id: {
              "buyuserId": '$filled.buyuserId',
              "selluserId": '$filled.selluserId',
              "sellId": "$filled.sellId",
              "buyId": "$filled.buyId"
            },
            "created_at": {
              $first: "$filled.created_at"
            },
            "Type": {
              $first: "$filled.Type"
            },
            "filledAmount": {
              $first: "$filled.filledAmount"
            },
            "pairname": {
              $first: "$filled.pairname"
            },
            "Price": {
              $first: "$filled.Price"
            }
          }
        },
        {
          $sort: {
            'created_at': -1
          }
        },
        {
          $limit: 20
        },
      ]).exec(cb)
    },
  }, (err, results) => {
    if (err) {
      result.status = false;
      result.message = 'Error occured.';
      result.err = err;
      result.notify_show = 'no';
      // res.json(result);
    } else if (results) {
      var sellOrder = results.sellOrder;
      var buyOrder = results.buyOrder;

      if (buyOrder.length > 0) {
        var sumamount = 0
        for (i = 0; i < buyOrder.length; i++) {
          var quantity = parseFloat(buyOrder[i].quantity) - parseFloat(buyOrder[i].filledAmount);
          var _id = buyOrder[i]._id;
          sumamount = parseFloat(sumamount) + parseFloat(quantity);
          buyOrder[i].total = sumamount;
          buyOrder[i].quantity = quantity;
        }
      }

      if (sellOrder.length > 0) {
        var sumamount = 0
        for (i = 0; i < sellOrder.length; i++) {
          var quantity = parseFloat(sellOrder[i].quantity) - parseFloat(sellOrder[i].filledAmount);
          var _id = sellOrder[i]._id;
          sumamount = parseFloat(sumamount) + parseFloat(quantity);
          sellOrder[i].total = sumamount;
          sellOrder[i].quantity = quantity;
        }
      }

      sellOrder = sellOrder.reverse();

      result.status = true;
      result.message = 'tradeTableAll';
      result.buyOrder = results.buyOrder;
      result.sellOrder = results.sellOrder;
      result.sellsumvalue = results.sellsumvalue;
      result.buysumvalue = results.buysumvalue;
      result.contractdetails = results.contractdetails;
      result.notify_show = 'no';
      result.Rescentorder = results.Rescentorder;
      // res.json(result);
      // console.log(result);
      if (typeof socketio != 'undefined') {
        socketio.emit('SPOTTRADE', result);
      }
    } else {
      result.status = false;
      result.message = 'Error occured.';
      result.err = '';
      result.notify_show = 'no';
      // res.json(result);
    }
  });
}
router.post('/getspotTradeData', (req, res) => {
  var findObj = {
    firstCurrency: req.body.firstCurrency,
    secondCurrency: req.body.secondCurrency
  };
  var pair = req.body.firstCurrency + req.body.secondCurrency;
  var result = {};
  // tradeTable.find(findObj,function(err,tradeTableAll){
  async.parallel({
    buyOrder: function(cb) {
      var sort = {
        '_id': -1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: req.body.firstCurrency,
            secondCurrency: req.body.secondCurrency,
            buyorsell: 'buy'
          }
        },
        {
          $group: {
            '_id': '$price',
            'quantity': {
              $sum: '$quantity'
            },
            'filledAmount': {
              $sum: '$filledAmount'
            }
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 10
        },
      ]).allowDiskUse(true).exec(cb)
    },
    sellOrder: function(cb) {
      var sort = {
        '_id': 1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: req.body.firstCurrency,
            secondCurrency: req.body.secondCurrency,
            buyorsell: 'sell'
          }
        },
        {
          $group: {
            '_id': '$price',
            'quantity': {
              $sum: '$quantity'
            },
            'filledAmount': {
              $sum: '$filledAmount'
            }
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 10
        },
      ]).allowDiskUse(true).exec(cb)
    },
    sellsumvalue: function(cb) {
      var sort = {
        '_id': 1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: req.body.firstCurrency,
            secondCurrency: req.body.secondCurrency,
            buyorsell: 'sell'
          }
        },
        {
          $group: {
            _id: null,
            'quantity': {
              $sum: '$quantity'
            },
          }
        },
      ]).allowDiskUse(true).exec(cb)
    },
    buysumvalue: function(cb) {
      var sort = {
        '_id': 1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: req.body.firstCurrency,
            secondCurrency: req.body.secondCurrency,
            buyorsell: 'buy'
          }
        },
        {
          $group: {
            _id: null,
            'orderValue': {
              $sum: '$orderValue'
            },
          }
        },
      ]).allowDiskUse(true).exec(cb)
    },
    Assetdetails: function(cb) {
      Assets.find({
        userId: ObjectId(req.body.userid)
      }).exec(cb)
    },
    contractdetails: function(cb) {
      spotpairs.findOne({
        first_currency: req.body.firstCurrency,
        second_currency: req.body.secondCurrency
      }, {
        tiker_root: 1,
        first_currency: 1,
        second_currency: 1
      }).exec(cb)
    },
    Rescentorder: function(cb) {
      spottradeTable.aggregate([{
            $match: {
              'pairName': pair,
              'status': '1'
            }
          },
          {
            $unwind: "$filled"
          },
          {
            $project: {
              "filled": 1
            }
          },
          {
            $group: {
              _id: {
                "buyuserId": '$filled.buyuserId',
                "selluserId": '$filled.selluserId',
                "sellId": "$filled.sellId",
                "buyId": "$filled.buyId"
              },
              "created_at": {
                $first: "$filled.created_at"
              },
              "Type": {
                $first: "$filled.Type"
              },
              "filledAmount": {
                $first: "$filled.filledAmount"
              },
              "pairname": {
                $first: "$filled.pairname"
              },
              "Price": {
                $first: "$filled.Price"
              },
              "Type": {
                $first: "$filled.Type"
              }
            }
          },
          {
            $sort: {
              'created_at': -1
            }
          },
          {
            $limit: 20
          },
        ])
        .exec(cb)
    },
  }, (err, results) => {
    // console.log("sda",results)

    if (err) {
      result.status = false;
      result.message = 'Error occured.';
      result.err = err;
      result.notify_show = 'no';
      res.json(result);
    } else if (results) {
      var sellOrder = results.sellOrder;  
      var buyOrder = results.buyOrder;
      if (buyOrder.length > 0) {
        var sumamount = 0
        for (i = 0; i < buyOrder.length; i++) {
          var quantity = parseFloat(buyOrder[i].quantity) - parseFloat(buyOrder[i].filledAmount);
          var _id = buyOrder[i]._id;
          sumamount = parseFloat(sumamount) + parseFloat(quantity);
          buyOrder[i].total = sumamount;
          buyOrder[i].quantity = quantity;
        }
      }

      if (sellOrder.length > 0) {
        var sumamount = 0
        for (i = 0; i < sellOrder.length; i++) {
          var quantity = parseFloat(sellOrder[i].quantity) - parseFloat(sellOrder[i].filledAmount);
          var _id = sellOrder[i]._id;
          sumamount = parseFloat(sumamount) + parseFloat(quantity);
          sellOrder[i].total = sumamount;
          sellOrder[i].quantity = quantity;
        }
      }
      var highestBid = (buyOrder.length > 0) ? buyOrder[0]._id : 0;
      var lowestAsk = (sellOrder.length > 0) ? sellOrder[sellOrder.length - 1]._id : 0;

      spotpairs.findOneAndUpdate({
        "tiker_root": pair
      }, {
        "$set": {
          "highestBid": highestBid,
          "lowestAsk": lowestAsk
        }
      }, {
        multi: true
      }).exec(function(err, resUpdate) {
        if (resUpdate) {
          // console.log(resUpdate,'price update');
        }
      });

      sellOrder = sellOrder.reverse();

      result.status = true;
      result.message = 'tradeTableAll';
      result.buyOrder = results.buyOrder;
      result.sellOrder = results.sellOrder;
      result.contractdetails = results.contractdetails;
      result.sellsumvalue = results.sellsumvalue;
      result.buysumvalue = results.buysumvalue;
      result.notify_show = 'no';
      result.assetdetails = results.Assetdetails;
      result.Rescentorder = results.Rescentorder;
      res.json(result);
    } else {
      result.status = false;
      result.message = 'Error occured.';
      result.err = '';
      result.notify_show = 'no';
      res.json(result);
    }
  });
});

function cancel_trade(tradeid, userid) {
  // console.log("cancel_trade")
  update = {
    status: '3'
  }
  spottradeTable.findOne({
    '_id': ObjectId(tradeid),
    'status': {
      $ne: '3'
    }
  }).exec((tradeerr, tradedata) => {
    if (tradedata) {
      var type = tradedata.buyorsell;
      var trade_ids = tradedata._id;
      var userId = tradedata.userId;
      var filledAmt = tradedata.filledAmount;
      var status = tradedata.status;
      var quantity = tradedata.quantity;
      var price = tradedata.price;
      var t_firstcurrencyId = tradedata.firstCurrency;
      var t_secondcurrencyId = tradedata.secondCurrency;
      var beforeBalance = tradedata.beforeBalance;
      var afterBalance = tradedata.afterBalance;

      quantity = parseFloat(quantity) - parseFloat(filledAmt);

      var order_value = parseFloat(quantity * price).toFixed(8);

      async.parallel({
        // update balance
        data1: function(cb) {
          var updatebaldata = {};

          var currency = (type == 'buy') ? t_secondcurrencyId : t_firstcurrencyId
          updatebaldata["spotwallet"] = (type == 'buy') ? order_value : quantity;
          // console.log(updatebaldata, 'updatebaldata')
          // console.log(currency, 'currency')
          Assets.findOneAndUpdate({
            currencySymbol: currency,
            userId: ObjectId(userId)
          }, {
            "$inc": updatebaldata
          }, {
            new: true,
            "fields": {
              spotwallet: 1
            }
          }, function(balerr, baldata) {
            // console.log(balerr, 'balerriiiii')
            // console.log(baldata, 'baldata')

          });
        },
        data2: function(cb) {
          var updatedata = {
            "status": '3'
          }
          spottradeTable.findOneAndUpdate({
            _id: ObjectId(tradeid)
          }, {
            "$set": updatedata
          }, {
            new: true,
            "fields": {
              _id: 1
            }
          }, function(upErr, upRes) {
            if (upRes) {
              //res.json({status:true,message:"Your Order cancelled successfully.",notify_show:'yes'});
              gettradedata(t_firstcurrencyId, t_secondcurrencyId, socketio)
            } else {
              res.json({
                status: false,
                message: "Due to some error occurred,While Order cancelling"
              });
            }
          });
        }
      }, function(err, results) {

      });
    } else {
      console.log({
        status: false,
        message: "Your Order already cancelled"
      });
    }
  });
}
router.post('/spotcancelTrade', (req, res) => {

  var tradeid = req.body.id;
  var userid = req.body.userid;
  update = {
    status: '3'
  }
  spottradeTable.findOne({
    '_id': ObjectId(tradeid),
    'status': {
      $ne: '3'
    }
  }).exec((tradeerr, tradedata) => {
    if (tradedata) {
      var type = tradedata.buyorsell;
      var trade_ids = tradedata._id;
      var userId = tradedata.userId;
      var filledAmt = tradedata.filledAmount;
      var status = tradedata.status;
      var quantity = tradedata.quantity;
      var price = tradedata.price;
      var t_firstcurrencyId = tradedata.firstCurrency;
      var t_secondcurrencyId = tradedata.secondCurrency;
      var beforeBalance = tradedata.beforeBalance;
      var afterBalance = tradedata.afterBalance;

      quantity = parseFloat(quantity) - parseFloat(filledAmt);

      var order_value = parseFloat(quantity * price).toFixed(8);

      async.parallel({
        // update balance
        data1: function(cb) {
          var updatebaldata = {};

          var currency = (type == 'buy') ? t_secondcurrencyId : t_firstcurrencyId
          updatebaldata["spotwallet"] = (type == 'buy') ? order_value : quantity;

          Assets.findOneAndUpdate({
            currencySymbol: currency,
            userId: ObjectId(userId)
          }, {
            "$inc": updatebaldata
          }, {
            new: true,
            "fields": {
              spotwallet: 1
            }
          }, function(balerr, baldata) {

          });
        },
        data2: function(cb) {
          var updatedata = {
            "status": '3'
          }
          spottradeTable.findOneAndUpdate({
            _id: ObjectId(tradeid)
          }, {
            "$set": updatedata
          }, {
            new: true,
            "fields": {
              _id: 1
            }
          }, function(upErr, upRes) {
            if (upRes) {
              res.json({
                status: true,
                message: "Your Order cancelled successfully.",
                notify_show: 'yes'
              });
              gettradedata(t_firstcurrencyId, t_secondcurrencyId, socketio)
            } else {
              res.json({
                status: false,
                message: "Due to some error occurred,While Order cancelling"
              });
            }
          });
        }
      }, function(err, results) {

      });
    } else {
      res.json({
        status: false,
        message: "Your Order already cancelled"
      });
    }
  });

});
router.post('/getspotPricevalue', (req, res) => {
  console.log("inside");
  perf.start();
  var pair = req.body.firstCurrency + req.body.secondCurrency;
  // var curarray = ["BTCUSDT","ETHUSDT","ETHBTC","XRPUSDT"]
console.log("pair",pair);
  var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD","DASHBTC","TRXBTC","XMRBTC","DASHUSDT","TRXUSDT","XMRUSDT","BNBUSDT","BNBBTC"]
  var pairname = (curarray.includes(pair)) ? pair.replace("USDT", "USD") : pair;
  var result = {};
  async.parallel({
    volumedata: function(cb) {
      var sort = {
        'orderDate': -1
      };
      spottradeTable.aggregate([{
          $match: {
            "orderDate": {
              $gte: new Date(Date.now() - 24 * 60 * 60 * 1000),
              $lte: new Date()
            },
            status: '1',
          }
        },
        {
          $unwind: "$filled"
        },
        {
          $match: {
            'filled.pairname': pair
          }
        },
        {
          $group: {
            _id: "$item",
            low: {
              $min: "$filled.Price"
            },
            high: {
              $max: "$filled.Price"
            },
            volume: {
              $sum: {
                $abs: "$filled.filledAmount"
              }
            },
            secvolume: {
              $sum: "$filled.order_value"
            }
          }
        }

      ]).allowDiskUse(true).exec(cb)
    },
    rates: function(cb) {
      spotPrices.aggregate([{
          $match: {
            "createdAt": {
              $gte: new Date(Date.now() - 24 * 60 * 60 * 1000),
              $lte: new Date()
            },
            pairname: pairname
          }
        },
        {
          $sort: {
            'createdAt': 1
          }
        },
        {
          $limit: 86400
        },
        {
          $group: {
            _id: null,
            pairname: {
              $first: '$pairname'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            high: {
              $max: '$price'
            },
            low: {
              $min: '$price'
            },
          }
        },
        // {
        //     $project: {
        //         _id      : 1,
        //         pairname : 1,
        //         open     : 1,
        //         close    : 1,
        //         low      : 1,
        //         high     : 1,
        //         // change   : { $multiply: [{ $subtract: [ 1, { $divide: [ {$cond: [ { $eq: [ "$open", null ] }, 0, '$open' ]}, {$cond: [ { $eq: [ "$close", null ] }, 0, '$close' ]} ] } ] }, 100]}
        //     }
        // },
      ]).allowDiskUse(true).exec(cb)
    },
    rates1: function(cb) {
      spottradeTable.aggregate([{
          $match: {
            "orderDate": {
              $gte: new Date(Date.now() - 24 * 60 * 60 * 1000),
              $lte: new Date()
            },
            pairName: req.body.firstCurrency + req.body.secondCurrency,
            //status:{'$or' : [{"status" : '0'},{"status" : '2'}]}
          }
        },
        {
          $sort: {
            'orderDate': 1
          }
        },
        {
          $group: {
            _id: null,
            pairname: {
              $first: '$pairName'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            high: {
              $max: '$price'
            },
            low: {
              $min: '$price'
            },
          }
        },
        // {
        //     $project: {
        //         _id      : 1,
        //         pairname : 1,
        //         open     : 1,
        //         close    : 1,
        //         low      : 1,
        //         high     : 1,
        //         // change   : { $multiply: [{ $subtract: [ 1, { $divide: [ {$cond: [ { $eq: [ "$open", null ] }, 0, '$open' ]}, {$cond: [ { $eq: [ "$close", null ] }, 0, '$close' ]} ] } ] }, 100]}
        //     }
        // },
      ]).allowDiskUse(true).exec(cb)
    },
  }, (err, results) => {
    const results1 = perf.stop();
    // console.log(results, "results now comme");
    if (err) {
      result.status = false;
      result.message = 'Error occured.';
      result.err = err;
      result.notify_show = 'no';
      res.json(result);
    } else if (results) {
      var open = 0;
      var last = 0;
      // var curarray = ["BTCUSDT","ETHUSDT","ETHBTC","XRPUSDT"]
      var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD",,"DASHBTC","TRXBTC","XMRBTC","DASHUSDT","TRXUSDT","XMRUSDT","BNBUSDT","BNBBTC"]
      if (curarray.includes(pair)) {
        // console.log("Resultsss",results)
        if (results.rates.length > 0) {
          results.rates[0].volume = results.volumedata.length > 0 ? results.volumedata[0].volume / 2 : 0;
          results.rates[0].secvolume = results.volumedata.length > 0 ? results.volumedata[0].secvolume / 2 : 0;
          var low = results.rates[0].low;
          var high = results.rates[0].high;
          var last = results.rates[0].close;
          var open = results.rates[0].open;
          var volume = results.volumedata.length > 0 ? parseFloat(results.volumedata[0].volume) / 2 : 0;
          var secvolume = results.volumedata.length > 0 ? parseFloat(results.volumedata[0].secvolume) / 2 : 0;
          // var total_volume = results.rates[0].volume;
          var change = (1 - (parseFloat(open) / parseFloat(last))) * 100;
          results.rates[0].change = change;

        }
      } else {
        if (results.rates1.length > 0) {
          results.rates1[0].volume = results.volumedata.length > 0 ? results.volumedata[0].volume / 2 : 0;
          results.rates1[0].secvolume = results.volumedata.length > 0 ? results.volumedata[0].secvolume / 2 : 0;
          var low = results.rates1[0].low;
          var high = results.rates1[0].high;
          var last = results.rates1[0].close;
          var open = results.rates1[0].open;
          var volume = results.volumedata.length > 0 ? parseFloat(results.volumedata[0].volume) / 2 : 0;
          var secvolume = results.volumedata.length > 0 ? parseFloat(results.volumedata[0].secvolume) / 2 : 0;
          // var total_volume = results.rates[0].volume;
          var change = (1 - (parseFloat(open) / parseFloat(last))) * 100;
          results.rates1[0].change = change;
        }
      }
      spotpairs.findOneAndUpdate({
        "tiker_root": req.body.firstCurrency + req.body.secondCurrency
      }, {
        "$set": {
          "low": low,
          "high": high,
          "last": last,
          "volume": volume,
          "secvolume": secvolume,
          "change": change
        }
      }, {
        multi: true
      }).exec(function(err, resUpdate) {
        if (resUpdate) {
          // console.log(resUpdate,'price update');
        }
      });
// console.log("currarray",curarray);
// console.log("pair",pair);
      result.status = true;
      result.message = 'tradeTableAll';
      result.pricedet = (curarray.includes(pair)) ? results.rates : results.rates1;
      // result.lastpricedet = results.lastpricedet;
      // result.change       = results.change;
      result.notify_show = 'no';
      res.json(result);
      // console.log("resulltss",result);
    } else {
      result.status = false;
      result.message = 'Error occured.';
      result.err = '';
      result.notify_show = 'no';
      res.json(result);
    }
  });
});

function getusertradedata(userId, firstCurrency, secondCurrency) {
  // console.log(userId,'getuserdra')
  var userId = userId;
  var result = {};
  async.parallel({
    orderHistory: function(cb) {
      var sort = {
        '_id': -1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: firstCurrency,
            secondCurrency: secondCurrency,
            userId: ObjectId(userId)
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 10
        },
      ]).allowDiskUse(true).exec(cb)
    },
    Histroydetails: function(cb) {
      spottradeTable.find({
        userId: ObjectId(userId),
        firstCurrency: firstCurrency,
        secondCurrency: secondCurrency
      }).sort({
        '_id': -1
      }).limit(10).exec(cb)
    },
    Filleddetails: function(cb) {
      spottradeTable.find({
        status: 1,
        userId: ObjectId(userId),
        firstCurrency: firstCurrency,
        secondCurrency: secondCurrency
      }).sort({
        '_id': -1
      }).limit(10).exec(cb)
    },
    Conditional_details: function(cb) {
      spottradeTable.find({
        status: '4',
        userId: ObjectId(userId),
        firstCurrency: firstCurrency,
        secondCurrency: secondCurrency
      }).sort({
        '_id': -1
      }).limit(10).exec(cb)
    },

    // lastpricedet : function(cb) {
    //   var sort = {'orderDate':-1};
    //   tradeTable.findOne({'$or' : [{"status" : '1'},{"status" : '2'}],
    //         firstCurrency:firstCurrency,
    //         secondCurrency:secondCurrency},{price:1}).sort({'orderDate':-1}).exec(cb)
    //   // tradeTable.aggregate([
    //   //   {
    //   //     $match:
    //   //     {
    //   //       '$or' : [{"status" : '1'},{"status" : '2'}],
    //   //       firstCurrency:firstCurrency,
    //   //       secondCurrency:secondCurrency
    //   //     }
    //   //   },
    //   //   {$sort:sort},
    //   //   {$limit: 1},
    //   // ]).allowDiskUse(true).exec(cb)
    // },
    Assetdetails: function(cb) {
      Assets.find({
        userId: ObjectId(userId)
      }).exec(cb)
    },
    contractdetails: function(cb) {
      spotpairs.findOne({
        first_currency: firstCurrency,
        second_currency: secondCurrency
      }, {
        tiker_root: 1,
        maint_margin: 1,
        first_currency: 1,
        second_currency: 1
      }).exec(cb)
    },

  }, (err, results) => {

    if (err) {
      result.status = false;
      result.message = 'Error occured.';
      result.err = err;
      result.notify_show = 'no';
      // res.json(result);
    } else if (results) {
      result.status = true;
      result.message = 'tradeTableAll';
      result.buyOrder = results.buyOrder;
      result.sellOrder = results.sellOrder;
      result.orderHistory = results.orderHistory;
      result.Histroydetails = results.Histroydetails;
      result.Conditional_details = results.Conditional_details;
      result.Filleddetails = results.Filleddetails;
      // result.lastpricedet         = results.lastpricedet;
      result.assetdetails = results.Assetdetails;
      result.contractdetails = results.contractdetails;

      result.notify_show = 'no';
      if (typeof socketio != 'undefined' && typeof userId != 'undefined') {
        socketio.sockets.in(userId.toString()).emit('SPOTUSERTRADE', result);
      }
    } else {
      result.status = false;
      result.message = 'Error occured.';
      result.err = '';
      result.notify_show = 'no';
      // res.json(result);
    }
  });

}


router.post('/getspotuserTradeData', (req, res) => {
  var userId = req.body.userid;
  var status = req.body.status;
  var firstCurrency = req.body.firstCurrency;
  var secondCurrency = req.body.secondCurrency;
  var result = {};
  // tradeTable.find(findObj,function(err,tradeTableAll){
  async.parallel({
    orderHistory: function(cb) {
      var sort = {
        '_id': -1
      };
      spottradeTable.aggregate([{
          $match: {
            '$or': [{
              "status": '0'
            }, {
              "status": '2'
            }],
            firstCurrency: req.body.firstCurrency,
            secondCurrency: req.body.secondCurrency,
            userId: ObjectId(userId)
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 10
        },
      ]).allowDiskUse(true).exec(cb)
    },
    Histroydetails: function(cb) {
      spottradeTable.find({
        userId: ObjectId(userId),
        firstCurrency: firstCurrency,
        secondCurrency: secondCurrency
      }).sort({
        '_id': -1
      }).limit(20).exec(cb)
    },
    Filleddetails: function(cb) {
      spottradeTable.find({
        status: 1,
        userId: ObjectId(userId),
        firstCurrency: firstCurrency,
        secondCurrency: secondCurrency
      }).sort({
        '_id': -1
      }).limit(20).exec(cb)
    },
    Conditional_details: function(cb) {
      spottradeTable.find({
        status: '4',
        userId: ObjectId(userId),
        firstCurrency: firstCurrency,
        secondCurrency: secondCurrency
      }).sort({
        '_id': -1
      }).limit(20).exec(cb)
    },

    lastpricedet: function(cb) {
      var sort = {
        'orderDate': -1
      };
      spottradeTable.aggregate([{
          $match: {
            status: '1',
            firstCurrency: req.body.firstCurrency,
            secondCurrency: req.body.secondCurrency
          }
        },
        {
          $sort: sort
        },
        {
          $limit: 1
        },
      ]).allowDiskUse(true).exec(cb)
    },
    Assetdetails: function(cb) {
      Assets.find({
        userId: ObjectId(req.body.userid)
      }).exec(cb)
    },
    contractdetails: function(cb) {
      spotpairs.findOne({
        first_currency: req.body.firstCurrency,
        second_currency: req.body.secondCurrency
      }, {
        tiker_root: 1,
        maint_margin: 1,
        first_currency: 1,
        second_currency: 1
      }).exec(cb)
    },

  }, (err, results) => {
    // console.log(results.position_details,'position_details');
    if (err) {
      result.status = false;
      result.message = 'Error occured.';
      result.err = err;
      result.notify_show = 'no';
      res.json(result);
    } else if (results) {
      result.status = true;
      result.message = 'tradeTableAll';
      result.buyOrder = results.buyOrder;
      result.sellOrder = results.sellOrder;
      result.orderHistory = results.orderHistory;
      result.Histroydetails = results.Histroydetails;
      result.Conditional_details = results.Conditional_details;
      result.Filleddetails = results.Filleddetails;
      result.lastpricedet = results.lastpricedet;
      result.assetdetails = results.Assetdetails;
      result.contractdetails = results.contractdetails;
      result.notify_show = 'no';
      res.json(result);
    } else {
      result.status = false;
      result.message = 'Error occured.';
      result.err = '';
      result.notify_show = 'no';
      res.json(result);
    }
  });
});

function order_placing(ordertype, buyorsell, price, quantity, pairname, userid, trigger_price = 0, trigger_type = null, id = 0, typeorder = 'Conditional', trailstopdistance = 0) {
  console.log("orderplacing")
  spotpairs.findOne({
    tiker_root: pairname
  }, {
    tiker_root: 1,
    first_currency: 1,
    second_currency: 1,
    markprice: 1,
    maxquantity: 1,
    minquantity: 1
  }, function(err, contractdetails) {
    var float = (pairname == 'BTCUSDT' || pairname == 'ETHUSDT') ? 2 : 8;
    // console.log(contractdetails, 'contractdetails')
    var markprice = contractdetails.markprice;
    var btcprice = contractdetails.markprice;
    var taker_fees = contractdetails.taker_fees;
    var order_value = parseFloat(quantity) * parseFloat(price);
    var firstcurrency = contractdetails.first_currency;
    var secondcurrency = contractdetails.second_currency;
    var curcurrency = (buyorsell == 'buy') ? secondcurrency : firstcurrency;
    var curvalue = (buyorsell == 'buy') ? order_value : quantity;
    if (err) {
      res.json({
        status: false,
        message: "Error occured.",
        err: err,
        notify_show: 'yes'
      });
    } else {
      Assets.findOne({
        userId: ObjectId(userid),
        currencySymbol: curcurrency
      }, function(err, assetdetails) {
        // console.log(err, 'err')
        // console.log(assetdetails, 'assetdetails')
        if (err) {
          res.json({
            status: false,
            message: "Error occured.",
            err: err,
            notify_show: 'yes'
          });
        } else if (assetdetails) {
          console.log('else    if')
          var curbalance = assetdetails.spotwallet;
          if (parseFloat(curbalance) < parseFloat(curvalue) && userid.toString() != "5e567694b912240c7f0e4299") {
            console.log({
              status: false,
              message: "Due to insuffient balance order cannot be placed",
              notify_show: 'yes'
            })
          } else {

            var before_reduce_bal = curbalance;
            var after_reduce_bal = parseFloat(curbalance) - parseFloat(curvalue);

            var updateObj = {
              spotwallet: after_reduce_bal
            };

            Assets.findByIdAndUpdate(assetdetails._id, updateObj, {
              new: true
            }, function(err, changed) {
              if (err) {
                res.json({
                  status: false,
                  message: "Error occured.",
                  err: err,
                  notify_show: 'yes'
                });
              } else if (changed) {
                // console.log(typeorder,'triggertyrp')
                if (typeorder == 'trailingstop') {
                  const newtradeTable = new spottradeTable({
                    quantity: parseFloat(quantity).toFixed(8),
                    price: parseFloat(price).toFixed(float),
                    trigger_price: trigger_price,
                    orderValue: order_value,
                    userId: userid,
                    pair: contractdetails._id,
                    pairName: pairname,
                    beforeBalance: before_reduce_bal,
                    afterBalance: after_reduce_bal,
                    firstCurrency: firstcurrency,
                    secondCurrency: secondcurrency,
                    orderType: ordertype,
                    trigger_type: trigger_type,
                    stopstatus: '0',
                    buyorsell: buyorsell,
                    pairid: id,
                    trigger_ordertype: typeorder,
                    trailstop: '1',
                    orderDate:new Date(),
                    trailstopdistance: trailstopdistance,
                    status: 4 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                  });
                  newtradeTable
                    .save()
                    .then(curorder => {
                      if (typeof socketio != 'undefined') {
                        socketio.sockets.in(userid.toString()).emit('NOTIFICATION', "Trail stop order created successfully");
                      }
                      tradematching(curorder);
                    }).catch(err => {
                      console.log(err, 'error');
                      res.json({
                        status: false,
                        message: "Your order not placed.",
                        notify_show: 'yes'
                      })
                    });
                  ``
                } else {
                  const newtradeTable = new spottradeTable({
                    quantity: quantity,
                    price: (typeorder == 'stop' || typeorder == 'takeprofit') ? trigger_price : price,
                    trigger_price: trigger_price,
                    orderValue: order_value,
                    userId: userid,
                    pair: contractdetails._id,
                    pairName: pairname,
                    beforeBalance: before_reduce_bal,
                    afterBalance: after_reduce_bal,
                    firstCurrency: firstcurrency,
                    secondCurrency: secondcurrency,
                    orderType: ordertype,
                    trigger_type: trigger_type,
                    stopstatus: (typeorder != 'Conditional') ? '1' : '0',
                    buyorsell: buyorsell,
                    pairid: id,
                    trigger_ordertype: typeorder,
                    status: (trigger_type != null) ? 4 : 0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                  });
                  newtradeTable
                    .save()
                    .then(curorder => {
                      // console.log('curorder', curorder)
                      tradematching(curorder);
                    }).catch(err => {
                      console.log(err, 'error');
                      res.json({
                        status: false,
                        message: "Your order not placed.",
                        notify_show: 'yes'
                      })
                    });
                }

              }
            })
            // insert trade tab
          }
        } else {

        }
      });
    }

  });


}
router.post('/triggerstop', (req, res) => {
  var takeprofitcheck = req.body.takeprofitcheck;
  var stopcheck = req.body.stopcheck;
  var quantity = req.body.quantity;
  var takeprofit = req.body.takeprofit;
  var ordertype = req.body.ordertype;
  var buyorsell = req.body.buyorsell;
  var price = req.body.price;
  var leverage = req.body.leverage;
  var trailingstopdistance = req.body.trailingstopdistance;

  if (takeprofitcheck) {
    var trigger_price = takeprofit;
    var tptrigger_type = "Mark";
    var newbuyorsell = (buyorsell == 'buy') ? 'sell' : 'buy';
    order_placing(ordertype, newbuyorsell, price, quantity, leverage, req.body.pairname, req.body.userid, trigger_price, tptrigger_type, 0, 'takeprofit');
    res.json({
      status: true,
      message: "Your take profit order set successfully.",
      notify_show: 'yes'
    });
  }
  if (stopcheck) {
    var stoptrigger_type = "Mark";
    var trigger_price = stopprice;
    var newbuyorsell = (buyorsell == 'buy') ? 'sell' : 'buy';
    order_placing(ordertype, newbuyorsell, price, quantity, leverage, req.body.pairname, req.body.userid, trigger_price, stoptrigger_type, 0, 'stop');
    res.json({
      status: true,
      message: "Your stop order set successfully.",
      notify_show: 'yes'
    });
  }
  if (trailingstopdistance != '' && trailingstopdistance != 0) {

    var trigger_price = (buyorsell == 'buy') ? parseFloat(price) + parseFloat(trailingstopdistance) : parseFloat(price) - parseFloat(trailingstopdistance);
    // var newbuyorsell = (buyorsell=='buy')?'sell':'buy';
    order_placing(ordertype, buyorsell, price, quantity, leverage, req.body.pairname, req.body.userid, trigger_price, 'Last', 0, 'trailingstop', trailingstopdistance);
    res.json({
      status: true,
      message: "Your trail stop order set successfully.",
      notify_show: 'yes'
    });
  }
});

function write_log(msg) {
  var now = new Date();
  var log_file = 'log/common_log_' + now.getFullYear() + now.getMonth() + now.getDay() + '.txt';
  fs.appendFileSync(log_file, msg);
  //console.log(msg);
  return true;
}


/// 30 miutess     */30 * * * *

//
//
// cron.schedule("*/7 * * * *", (req, res) => {
// console.log("cron working in minutess/*/*/*/");
// var orderType="Limit"
// var buyorsell ="buy"
// generatebot(orderType,buyorsell)
// });
//
// cron.schedule("*/5 * * * *", (req, res) => {
// console.log("cron working in minutess/*/*/*/");
// var orderType="Limit"
// var buyorsell ="sell"
// generatebot(orderType,buyorsell)
// });
//
// cron.schedule("*/2 * * * *", (req, res) => {
// console.log("cron working in minutess/*/*/*/");
// var orderType="Market"
// var buyorsell ="buy"
// generatebot(orderType,buyorsell)
// });
//
// cron.schedule("*/3 * * * *", (req, res) => {
// console.log("cron working in minutess/*/*/*/");
// var orderType="Market"
// var buyorsell ="sell"
// generatebot(orderType,buyorsell)
// });

function generatebot(orderType,buyorsell){
  // console.log("orderType",orderType);
  // console.log("buyorsell",buyorsell);
  spotpairs.find({}).then(spotpairs=>{
    console.log("spotpairss",spotpairs.length);
    if (spotpairs.length > 0) {
      spotpairs[0].orderTypedata=orderType
      spotpairs[0].buyorselldata=buyorsell
      var i = 0;

        // console.log("spotpairsss",spotpairs[0])
      generateBuySpotOrder(spotpairs[0], function () {
        // console.log("length of array",spotpairs.length);
        if (i === spotpairs.length - 1) {
          // console.log("inside the if sss")
          callBackSpotImport();
        } else {
          // console.log("isndie else")
          i += 1;
          spotpairs[i].orderTypedata=orderType
          spotpairs[i].buyorselldata=buyorsell
          if (spotpairs[i]) {
            // console.log("next creatinon token ss",currencytokendetails[i]);
            generateBuySpotOrder(spotpairs[i]);
          } else {
            callBackSpotImport();
          }
        }
      });
    }
  })

}

// cron.schedule("*/3 * * * *", (req, res) => {
// var pairname="BCHBTC"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });


// cron.schedule("*/6 * * * *", (req, res) => {
// var pairname="BCHBUSD"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/9 * * * *", (req, res) => {
// var pairname="BCHUSDT"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/12 * * * *", (req, res) => {
// var pairname="BTCBUSD"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });


// cron.schedule("*/15 * * * *", (req, res) => {
// var pairname="BTCUSDT"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/18 * * * *", (req, res) => {
// var pairname="ETHBTC"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/21 * * * *", (req, res) => {
// var pairname="ETHBUSD"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });


// cron.schedule("*/24 * * * *", (req, res) => {
// var pairname="ETHUSDT"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/27 * * * *", (req, res) => {
// var pairname="LTCBTC"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/30 * * * *", (req, res) => {
// var pairname="LTCBUSD"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/33 * * * *", (req, res) => {
// var pairname="LTCUSDT"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/36 * * * *", (req, res) => {
// var pairname="XRPBTC"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/39 * * * *", (req, res) => {
// var pairname="XRPBUSD"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/42 * * * *", (req, res) => {
// var pairname="XRPUSDT"
// var orderType="Limit"
// var buyorsell ="sell"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });



// cron.schedule("*/3 * * * *", (req, res) => {
// var pairname="BCHBTC"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });


// cron.schedule("*/6 * * * *", (req, res) => {
// var pairname="BCHBUSD"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/9 * * * *", (req, res) => {
// var pairname="BCHUSDT"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/12 * * * *", (req, res) => {
// var pairname="BTCBUSD"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });


// cron.schedule("*/15 * * * *", (req, res) => {
// var pairname="BTCUSDT"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/18 * * * *", (req, res) => {
// var pairname="ETHBTC"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/21 * * * *", (req, res) => {
// var pairname="ETHBUSD"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });


// cron.schedule("*/24 * * * *", (req, res) => {
// var pairname="ETHUSDT"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/27 * * * *", (req, res) => {
// var pairname="LTCBTC"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/30 * * * *", (req, res) => {
// var pairname="LTCBUSD"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/33 * * * *", (req, res) => {
// var pairname="LTCUSDT"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/36 * * * *", (req, res) => {
// var pairname="XRPBTC"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/39 * * * *", (req, res) => {
// var pairname="XRPBUSD"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

// cron.schedule("*/42 * * * *", (req, res) => {
// var pairname="XRPUSDT"
// var orderType="Limit"
// var buyorsell ="buy"
// spotpairs.findOne({tiker_root:pairname}).then(spotpairs=>{
//   if(spotpairs){
//     spotpairs.buyorselldata=buyorsell
//     spotpairs.orderTypedata=orderType
//     generateBuySpotOrder(spotpairs)
//   }
// })
// });

function generateBuySpotOrder(spotpairs) {
  console.log("isndiesss");
  var pairname = spotpairs.tiker_root;
  var highpricetable=spotpairs.high;
  var lowpricetable=spotpairs.low
  var buyorsell=spotpairs.buyorselldata
    var ordertype = spotpairs.orderTypedata;
    // var useridstatic=ObjectId("5f3617da362e0610f8df95d5")
    var useridstatic=ObjectId("5f11301062c7e3584e61ec88")
  var tablename = (pairname == 'BTCUSDT' || pairname == 'ETHUSDT') ? perpetual : spotpairs;
  var pairnn = (pairname == 'BTCUSDT' ||pairname == 'ETHUSDT') ? pairname.replace("USDT", "USD") : pairname;
console.log("pairnameesssssa",pairname);
if(spotpairs.botstatus=="On"){
  // console.log("spotpairs",spotpairs);
  spottradeTable
    .aggregate([
      {
        $match: {
          $or: [{ status: "0" }, { status: "2" }],
          pairName: pairname,
          buyorsell:buyorsell,
        },
      }])
.then(buyresult=>{
console.log("length",buyresult.length);

if(buyresult.length<=9 && buyresult.length>=0){

  var i = 0;
  var  firstcurrency=spotpairs.first_currency
var secondcurrency = spotpairs.second_currency
  generateTenSpotOrder(spotpairs, function () {
    // console.log("i valueesa/*/*/*/*/*",i);
    if (i === 10 - 1) {
      // callBackSpotImport();

  gettradedata(
    firstcurrency,
    secondcurrency,
    socketio
  );

      // userinfo.spotover()
    } else {
      i += 1;
      spotpairs.orderTypedata=ordertype
      spotpairs.buyorselldata=buyorsell
        generateTenSpotOrder(spotpairs);
    }
  });
}
})

}
}

// function generateBuySpotOrder(spotpairs, callBackOne) {
//   console.log("insdie");
//   if (callBackOne) {
//     userinfo.spotover = callBackOne;
//   }
//   var pairname = spotpairs.tiker_root;
//   var highpricetable=spotpairs.high;
//   var lowpricetable=spotpairs.low
//   var buyorsell=spotpairs.buyorselldata
//     var ordertype = spotpairs.orderTypedata;
//     // var useridstatic=ObjectId("5f3617da362e0610f8df95d5")
//     var useridstatic=ObjectId("5f11301062c7e3584e61ec88")
//   var tablename = (pairname == 'BTCUSDT' || pairname == 'ETHUSDT') ? perpetual : spotpairs;
//   var pairnn = (pairname == 'BTCUSDT' ||pairname == 'ETHUSDT') ? pairname.replace("USDT", "USD") : pairname;
// console.log("pairnameesssssa",pairname);
// if(spotpairs.botstatus=="On"){
//   // console.log("spotpairs",spotpairs);
//   spottradeTable
//     .aggregate([
//       {
//         $match: {
//           $or: [{ status: "0" }, { status: "2" }],
//           pairName: pairname,
//           buyorsell:buyorsell,
//         },
//       }])
// .then(buyresult=>{
// console.log("length",buyresult.length);
//
// if(buyresult.length<=9 && buyresult.length>=0){
//
//   var i = 0;
//   generateTenSpotOrder(spotpairs, function () {
//     console.log("i valueesa/*/*/*/*/*",i);
//     if (i === 10 - 1) {
//       // callBackSpotImport();
//       userinfo.spotover()
//     } else {
//       i += 1;
//       spotpairs.orderTypedata=ordertype
//       spotpairs.buyorselldata=buyorsell
//         generateTenSpotOrder(spotpairs);
//     }
//   });
// }else{
//   userinfo.spotover()
// }
// })
//
// }else{
//   userinfo.spotover()
// }
// }

function generateTenSpotOrder(spotpairs, callBackTwo) {
  if (callBackTwo) {
    userinfo.spotplacingover = callBackTwo;
  }

  var pairname = spotpairs.tiker_root;
  console.log("pairname in tenspot",pairname);

  var highpricetable=spotpairs.high;
  var lowpricetable=spotpairs.low
  var buyorsell=spotpairs.buyorselldata
    var ordertype = spotpairs.orderTypedata;
    // var useridstatic=ObjectId("5f3617da362e0610f8df95d5")
    var useridstatic=ObjectId("5f11301062c7e3584e61ec88")

        var contractdetails=spotpairs
        var timeinforcetype = "GoodTillCancelled";
        var trigger_price = 0;
        var trigger_type = null;
        var firstcurrency = contractdetails.first_currency;
        var secondcurrency = contractdetails.second_currency;
        // var randomprice = Math.random() * (+highpricetable - +lowpricetable) + +lowpricetable;
        var dbmarkprice=contractdetails.markprice
        var randommulti = Math.random() * (+0.001 - +0.002) + +0.002
        // console.log("randommultiii",randommulti);
        // console.log("oridigal price",contractdetails.markprice);
        var checkprice= parseFloat(dbmarkprice)  * parseFloat(randommulti)
        var divvaluee
        if(pairname=="BTCUSDT" ){
            divvaluee = parseFloat(checkprice)/ 100
        }else if(pairname=="ETHUSDT"){
          divvaluee= parseFloat(checkprice) * 5
        }
        else {
          divvaluee =randommulti
        }

            if(buyorsell=="buy"){
              // var randomprice = Math.random() * (+highpricetable - +lowpricetable) + +lowpricetable;
              randomprice = parseFloat(dbmarkprice) - parseFloat(divvaluee)

            }else{
              randomprice = parseFloat(dbmarkprice) + parseFloat(divvaluee)
            }
        var randomquantity12 = Math.random() * (+1 - +0.5) + +0.5
        var randomquantity =randomquantity12.toFixed(4)

        // console.log("randomprice",randomprice);
        // console.log("randomquantity",randomquantity);
          var price = randomprice.toFixed(8);
          // var quantity=randomquantity.toFixed(2)

        var markprice = parseFloat(contractdetails.markprice).toFixed(8);
        var maxquantity = contractdetails.maxquantity;
        var minquantity = contractdetails.minquantity;
        var taker_fees = contractdetails.taker_fees;
        var order_value = parseFloat(randomquantity * price).toFixed(8);
        var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD","DASHBTC","TRXBTC","XMRBTC","DASHUSDT","TRXUSDT","XMRUSDT","BNBUSDT","BNBBTC"]

        var minfromdb=contractdetails.minquantity
        var spotpricefromdb= contractdetails.markprice
        var spotquantity =parseFloat(minfromdb)/parseFloat(spotpricefromdb)

            var balcurrency = (buyorsell == 'buy') ? secondcurrency : firstcurrency;
            var order_value1 = (buyorsell == 'buy') ? order_value : randomquantity;
            // console.log(balcurrency, 'balcurrency')
            // console.log(req.body.userid, 'req.body.userid')
            var before_reduce_bal=0
            var after_reduce_bal =0

              var float = (pairname == 'BTCUSDT' || pairname == 'ETHUSDT') ? 2 : 8;
              const newtradeTable = new spottradeTable({
                quantity: parseFloat(randomquantity),
                price: parseFloat(price).toFixed(float),
                trigger_price: trigger_price,
                orderValue: order_value,
                userId: useridstatic,
                pair: contractdetails._id,
                pairName: pairname,
                beforeBalance: before_reduce_bal,
                afterBalance: after_reduce_bal,
                timeinforcetype: timeinforcetype,
                firstCurrency:  contractdetails.first_currency,
                secondCurrency: contractdetails.second_currency,
                orderType: ordertype,
                trigger_type: trigger_type,
                orderDate:new Date(),
                buyorsell: buyorsell,
                status: (trigger_type != null) ? 4 : 0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
              });
              newtradeTable
                .save()
                .then(curorder => {

                  // console.log("order placedd");
                  userinfo.spotplacingover()

                }).catch(err => {
                  console.log(err, 'error');

                });
          // }


      // });


 }


function callBackSpotImport() {
  // tradeinfo.filledamount = 0;
  console.log("spot generatedds");
}

router.post('/spotorderPlacing', (req, res) => {

  const {
    errors,
    isValid
  } = validateTradeInput(req.body);
  if (!isValid) {
    res.json({
      status: false,
      message: "Error occured, please fill all required fields.",
      errors: errors,
      notify_show: 'yes'
    });
  } else {

    var post_only = req.body.post_only;
    var reduce_only = req.body.reduce_only;
    var ordertype = req.body.ordertype;
    var buyorsell = req.body.buyorsell;
    var price = req.body.price;
    var timeinforcetype = req.body.timeinforcetype;
    var trigger_price = req.body.trigger_price;
    var trigger_type = req.body.trigger_type;
    var quantity = req.body.quantity;
    var takeprofitcheck = req.body.takeprofitcheck;
    var stopcheck = req.body.stopcheck;
    var takeprofit = req.body.takeprofit;
    var stopprice = req.body.stopprice;
    var tablename = (req.body.pairname == 'BTCUSDT' || req.body.pairname == 'ETHUSDT') ? perpetual : spotpairs;
    var pairnn = (req.body.pairname == 'BTCUSDT' || req.body.pairname == 'ETHUSDT') ? req.body.pairname.replace("USDT", "USD") : req.body.pairname;
    // console.log(pairnn);
    // console.log(tablename);
    spotpairs.findOne({
      tiker_root: req.body.pairname
    }, {
      tiker_root: 1,
      first_currency: 1,
      second_currency: 1,
      markprice: 1,
      maxquantity: 1,
      minquantity: 1,
      taker_fees: 1
    }, function(err, contractdetails) {
      // console.log(err, 'contractdetails');
      // console.log(contractdetails, 'contractdetails');
      // var firstcurrency  = contractdetails.first_currency;
      //   var secondcurrency = contractdetails.second_currency;

      var firstcurrency = contractdetails.first_currency;
      var secondcurrency = contractdetails.second_currency;


      var markprice = parseFloat(contractdetails.markprice).toFixed(8);
      var maxquantity = contractdetails.maxquantity;
      var minquantity = contractdetails.minquantity;
      var taker_fees = contractdetails.taker_fees;
      var order_value = parseFloat(quantity * price).toFixed(8);
      var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD","DASHBTC","TRXBTC","XMRBTC","DASHUSDT","TRXUSDT","XMRUSDT","BNBUSDT","BNBBTC"]

      var minfromdb=contractdetails.minquantity
      var spotpricefromdb= contractdetails.markprice
      var spotquantity =parseFloat(minfromdb)/parseFloat(spotpricefromdb)

      if (req.body.price < 0.00000001) {
        return res.json({
          status: false,
          message: "Price of contract must not be lesser than 0.00000001",
          notify_show: 'yes'
        });
      } else if (parseFloat(quantity) < parseFloat(spotquantity)) {
        return res.json({
          status: false,
          message: "Quantity of contract must not be lesser than " + spotquantity,
          notify_show: 'yes'
        });
      }
      // else if(parseFloat(quantity) > parseFloat(maxquantity))
      // {
      //     return res.json({
      //     status:false,
      //     message:"Quantity of contract must not be higher than "+maxquantity,
      //     notify_show:'yes'
      //     });
      // }
      else if (ordertype == 'Limit' && buyorsell == "buy" && parseFloat(req.body.price) > parseFloat(markprice) && curarray.includes(req.body.pairname)) {
        return res.json({
          status: false,
          message: "Entry price you set must be lower or equal to " + markprice,
          notify_show: 'yes'
        });
      } else if (ordertype == 'Limit' && buyorsell == "sell" && parseFloat(req.body.price) < parseFloat(markprice) && curarray.includes(req.body.pairname)) {
        return res.json({
          status: false,
          message: "Entry price you set must be higher or equal to " + markprice,
          notify_show: 'yes'
        });
      } else {


        if (err) {
          res.json({
            status: false,
            message: "Error occured.",
            err: err,
            notify_show: 'yes'
          });
        } else {
          var balcurrency = (req.body.buyorsell == 'buy') ? secondcurrency : firstcurrency;
          var order_value1 = (req.body.buyorsell == 'buy') ? order_value : quantity;
          // console.log(balcurrency, 'balcurrency')
          // console.log(req.body.userid, 'req.body.userid')
          Assets.findOne({
            userId: ObjectId(req.body.userid),
            currencySymbol: balcurrency
          }, function(err, assetdetails) {
            // console.log(assetdetails, 'assetdetails')
            if (err) {
              res.json({
                status: false,
                message: "Error occured.",
                err: err,
                notify_show: 'yes'
              });
            } else if (assetdetails) {

              var curbalance = parseFloat(assetdetails.spotwallet).toFixed(8);
              if (parseFloat(curbalance) < parseFloat(order_value1)) {
                res.json({
                  status: false,
                  message: "Due to insuffient balance order cannot be placed",
                  notify_show: 'yes'
                })
              } else {

                var before_reduce_bal = curbalance;
                var after_reduce_bal = parseFloat(curbalance) - parseFloat(order_value1);
                var updateObj = {
                  spotwallet: after_reduce_bal
                };
                var userid = req.body.userid;

                Assets.findByIdAndUpdate(assetdetails._id, updateObj, {
                  new: true
                }, function(err, changed) {
                  if (err) {
                    res.json({
                      status: false,
                      message: "Error occured.",
                      err: err,
                      notify_show: 'yes'
                    });
                  } else if (changed) {
                    var float = (req.body.pairname == 'BTCUSDT' || req.body.pairname == 'ETHUSDT') ? 2 : 8;
                    const newtradeTable = new spottradeTable({
                      quantity: parseFloat(quantity).toFixed(8),
                      price: parseFloat(price).toFixed(float),
                      trigger_price: trigger_price,
                      orderValue: order_value,
                      userId: req.body.userid,
                      pair: contractdetails._id,
                      pairName: req.body.pairname,
                      postOnly: post_only,
                      reduceOnly: reduce_only,
                      beforeBalance: before_reduce_bal,
                      afterBalance: after_reduce_bal,
                      timeinforcetype: timeinforcetype,
                      firstCurrency: firstcurrency,
                      secondCurrency: secondcurrency,
                      orderType: ordertype,
                      trigger_type: trigger_type,
                      orderDate:new Date(),
                      buyorsell: buyorsell,
                      status: (trigger_type != null) ? 4 : 0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                    });
                    newtradeTable
                      .save()
                      .then(curorder => {
                        // write_log("\n"+JSON.stringify({date:new Date(),process:"orderplacing",result:curorder}));
                        var io = req.app.get('socket');
                        if (typeof io != 'undefined') {
                          socketio.sockets.in(req.body.userid.toString()).emit('SPOTTRADE', curorder);
                        }
                        res.json({
                          status: true,
                          message: "Your order placed successfully.",
                          notify_show: 'yes'
                        });
                        if (takeprofitcheck == false) {
                          var trigger_price = takeprofit;
                          var tptrigger_type = "Mark";
                          var newbuyorsell = (buyorsell == 'buy') ? 'sell' : 'buy';
                          order_placing(ordertype, newbuyorsell, price, quantity, req.body.pairname, req.body.userid, trigger_price, tptrigger_type, curorder._id, 'takeprofit');
                        }
                        // console.log(profitnloss,'profitnloss')
                        // console.log(balance_check,'balance_check')
                        if (stopcheck == false) {
                          var stoptrigger_type = "Mark";
                          var trigger_price = stopprice;
                          var newbuyorsell = (buyorsell == 'buy') ? 'sell' : 'buy';
                          order_placing(ordertype, newbuyorsell, price, quantity, req.body.pairname, req.body.userid, trigger_price, stoptrigger_type, curorder._id, 'stop');
                        }
                        // console.log(balance_check,'balance_check')
                        // console.log(profitnloss,'profitnloss')

                        tradematching(curorder, io);
                      }).catch(err => {
                        console.log(err, 'error');
                        res.json({
                          status: false,
                          message: "Your order not placed.",
                          notify_show: 'yes'
                        })
                      });
                    ``
                  }
                })
                // insert trade tab
              }
            } else {
              res.json({
                status: false,
                message: "Error occured.",
                err: 'no res 2',
                notify_show: 'yes'
              });
            }
          });
        }
      }

    });
  }
});

function selldetailsupdate(tempdata, buyorderid, buyUpdate, sellorderid, sellUpdate, selluserid, buyprice, taker_fees, io, sellerforced_liquidation, sellleverage, buyOrder) {
  var buyuserid = tempdata.user_id;
  async.waterfall([
    function(callback) {
      spottradeTable.findOneAndUpdate({
        _id: ObjectId(buyorderid)
      }, {
        "$set": {
          "status": buyUpdate.status,
          "filled": tempdata
        },
        "$inc": {
          "filledAmount": parseFloat(buyUpdate.filledAmt)
        }
      }, {
        new: true,
        "fields": {
          status: 1,
          filled: 1
        }
      }, function(buytemp_err, buytempData1) {
        // console.log(buytemp_err, 'buytemp_err')
        if (buytempData1) {
          var updatebaldata = {};
          updatebaldata['spotwallet'] = parseFloat(tempdata.filledAmount) - parseFloat(tempdata.Fees);
          Assets.findOneAndUpdate({
            currencySymbol: tempdata.firstCurrency,
            userId: ObjectId(tempdata.user_id)
          }, {
            "$inc": updatebaldata
          }, {
            new: true,
            "fields": {
              spotwallet: 1
            }
          }, function(balerr, baldata) {
            // console.log(balerr,'buybalance error');
            // console.log(baldata,'buybaldata');
          });
          callback(null, buytempData1);
        }

      });
    },
    function(data, callback) {
      var order_value = parseFloat(sellUpdate.filledAmt * buyprice).toFixed(8);
      var fee = parseFloat(order_value) * parseFloat(taker_fees) / 100;
      tempdata.Type = "sell";
      tempdata.user_id = ObjectId(selluserid);
      tempdata.Fees = parseFloat(fee).toFixed(8);
      tempdata.Price = buyOrder.price;
      tempdata.filledAmount = +(sellUpdate.filledAmt).toFixed(8);
      tempdata.afterBalance = buyOrder.afterBalance;
      tempdata.beforeBalance = buyOrder.beforeBalance;
      tempdata.order_value = order_value;
      spottradeTable.findOneAndUpdate({
        _id: ObjectId(sellorderid)
      }, {
        "$set": {
          "status": sellUpdate.status,
          "filled": tempdata
        },
        "$inc": {
          "filledAmount": parseFloat(sellUpdate.filledAmt)
        }
      }, {
        new: true,
        "fields": {
          status: 1,
          filled: 1
        }
      }, function(buytemp_err, selltempData) {
        // console.log(buytemp_err, 'buytemp_errsell')
        if (selltempData) {
          var updatebaldata = {};
          updatebaldata['spotwallet'] = parseFloat(tempdata.order_value) - parseFloat(tempdata.Fees);
          Assets.findOneAndUpdate({
            currencySymbol: tempdata.secondCurrency,
            userId: ObjectId(tempdata.user_id)
          }, {
            "$inc": updatebaldata
          }, {
            new: true,
            "fields": {
              spotwallet: 1
            }
          }, function(balerr, baldata) {
            // console.log(balerr,'buybalance error');
            // console.log(baldata,'buybaldata');
          });

          callback(null, selltempData);
        }
      });
    },
  ], function(err, result) {
    // console.log(result, 'selldetailsupdate')

    //Bonus updation
    // FeeTable.findOne({}).exec(function(err, bonusdetails) {
    //   // console.log(bonusdetails, 'bonusdetails')
    //   if (bonusdetails) {
    //     var trade_bonus = bonusdetails.trade_bonus;
    //     var updatebonusdata = {};
    //     updatebonusdata["tempcurrency"] = trade_bonus;
    //     Assets.findOneAndUpdate({
    //       currencySymbol: 'BTC',
    //       userId: ObjectId(selluserid)
    //     }, {
    //       "$inc": updatebonusdata
    //     }, {
    //       new: true,
    //       "fields": {
    //         balance: 1
    //       }
    //     }, function(balerr, baldata) {
    //       // console.log(balerr, 'bale')
    //       // console.log(baldata, 'bale')
    //       const newBonus = new Bonus({
    //         userId: selluserid,
    //         bonus_amount: trade_bonus,
    //         type: '4',
    //       });
    //       newBonus.save(function(err, data) {
    //         // console.log(err,'err')
    //         // console.log(data,'data')
    //       });
    //     });
    //
    //     Assets.findOneAndUpdate({
    //       currencySymbol: 'BTC',
    //       userId: ObjectId(buyuserid)
    //     }, {
    //       "$inc": updatebonusdata
    //     }, {
    //       new: true,
    //       "fields": {
    //         balance: 1
    //       }
    //     }, function(balerr, baldata) {
    //       // console.log(balerr, 'bale')
    //       // console.log(baldata, 'bale')
    //       const newBonus = new Bonus({
    //         userId: buyuserid,
    //         bonus_amount: trade_bonus,
    //         type: '4',
    //       });
    //       newBonus.save(function(err, data) {
    //         // console.log(err,'err')
    //         // console.log(data,'data')
    //       });
    //     });
    //   }
    // });


    //socket call
    setTimeout(function() {
      gettradedata(result.filled[0].firstCurrency, result.filled[0].secondCurrency, socketio);
      getusertradedata(result.filled[0].selluserId, result.filled[0].firstCurrency, result.filled[0].secondCurrency);
      getusertradedata(result.filled[0].buyuserId, result.filled[0].firstCurrency, result.filled[0].secondCurrency);
    }, 3000);


    spottradeTable.findOneAndUpdate({
      pairid: (buyorderid),
      status: '4',
      stopstatus: '1'
    }, {
      "$set": {
        "stopstatus": '2'
      }
    }, {
      new: true,
      "fields": {
        status: 1
      }
    }, function(buytemp_err, buytempData) {

    });

    spottradeTable.findOneAndUpdate({
      pairid: (sellorderid),
      status: '4',
      stopstatus: '1'
    }, {
      "$set": {
        "stopstatus": '2'
      }
    }, {
      new: true,
      "fields": {
        status: 1
      }
    }, function(buytemp_err, buytempData) {

    });

    spottradeTable.find({
      status: '4',
      trigger_type: 'Last'
    }, function(buytemp_err, buytempData) {
      if (buytempData.length) {
        for (var i = 0; i < buytempData.length; i++) {
          var _id = buytempData[i]._id;
          var price = buytempData[i].price;
          var trigger_price = buytempData[i].trigger_price;
          var userId = buytempData[i].userId;
          var pairName = buytempData[i].pairName;
          var leverage = buytempData[i].leverage;
          var quantity = buytempData[i].quantity;
          var buyorsell = buytempData[i].buyorsell;
          var orderType = buytempData[i].orderType;
          var trailstop = buytempData[i].trailstop;
          var different = parseFloat(price) - parseFloat(trigger_price);
          // console.log(trigger_price,'trigger_price');
          // console.log(buyprice,'buyprice');
          if (different > 0) {
            if (trailstop == '0' && parseFloat(trigger_price) > parseFloat(buyprice)) {
              // order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
              spottradeTable.findOneAndUpdate({
                _id: ObjectId(_id),
                status: '4',
                stopstatus: {
                  $ne: '1'
                }
              }, {
                "$set": {
                  "status": '0'
                }
              }, {
                new: true,
                "fields": {
                  status: 1
                }
              }, function(buytemp_err, buytempData) {
                // console.log(buytemp_err,'trigger error');
              });
            }
          } else {
            if (trailstop == '0' && parseFloat(trigger_price) < parseFloat(buyprice)) {
              //order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
              spottradeTable.findOneAndUpdate({
                _id: ObjectId(_id),
                status: '4',
                stopstatus: {
                  $ne: '1'
                }
              }, {
                "$set": {
                  "status": '0'
                }
              }, {
                new: true,
                "fields": {
                  status: 1
                }
              }, function(buytemp_err, buytempData) {

                // console.log(buytemp_err,'trigger error');
              });
            }
          }
          //trailing stop trigger
          if (trailstop == '1' && buyorsell == 'buy' && parseFloat(price) > parseFloat(buyprice)) {
            var addprice = (parseFloat(buyprice) - parseFloat(price))
            var newtriggerprice = parseFloat(trigger_price) + parseFloat(addprice);
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "price": newtriggerprice,
                "trigger_price": newtriggerprice
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
          if (trailstop == '1' && buyorsell == 'buy' && parseFloat(trigger_price) < parseFloat(buyprice)) {
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "status": '0'
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
          if (trailstop == '1' && buyorsell == 'sell' && parseFloat(price) < parseFloat(buyprice)) {
            var addprice = (parseFloat(price) - parseFloat(buyprice))
            var newtriggerprice = parseFloat(trigger_price) - parseFloat(addprice);
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "price": newtriggerprice,
                "trigger_price": newtriggerprice
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
          if (trailstop == '1' && buyorsell == 'sell' && parseFloat(trigger_price) > parseFloat(buyprice)) {
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "status": '0'
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
        }
      }
    });

  });
}

function buydetailsupdate(tempdata, buyorderid, buyUpdate, sellorderid, sellUpdate, selluserid, buyprice, maker_rebate, io, sellforced_liquidation, sellleverage, sellOrder) {
  var buyuserid = tempdata.user_id;
  async.waterfall([
    function(callback) {
      spottradeTable.findOneAndUpdate({
        _id: ObjectId(buyorderid)
      }, {
        "$set": {
          "filled": tempdata,
          "status": buyUpdate.status
        },
        "$inc": {
          "filledAmount": buyUpdate.filledAmt
        }
      }, {
        new: true,
        "fields": {
          filled: 1
        }
      }, function(buytemp_err, buytempData) {
        // console.log(buytemp_err, 'buytemp_err')
        if (buytempData) {
          var updatebaldata = {};
          updatebaldata['spotwallet'] = parseFloat(tempdata.filledAmount) - parseFloat(tempdata.Fees);
          Assets.findOneAndUpdate({
            currencySymbol: tempdata.firstCurrency,
            userId: ObjectId(tempdata.user_id)
          }, {
            "$inc": updatebaldata
          }, {
            new: true,
            "fields": {
              spotwallet: 1
            }
          }, function(balerr, baldata) {
            // console.log(balerr,'buybalance error');
            // console.log(baldata,'buybaldata');
          });
          callback(null, buytempData);
        }
      });
    },
    function(data, callback) {

      var order_value = parseFloat(sellUpdate.filledAmt * buyprice).toFixed(8);
      var fee = parseFloat(order_value) * parseFloat(maker_rebate) / 100;

      tempdata.Type = "sell";
      tempdata.user_id = ObjectId(selluserid);
      tempdata.filledAmount = (sellUpdate.filledAmt);
      tempdata.Fees = parseFloat(fee).toFixed(8);
      tempdata.Price = sellOrder.price;
      tempdata.beforeBalance = sellOrder.beforeBalance;
      tempdata.afterBalance = sellOrder.afterBalance;
      tempdata.order_value = order_value;

      spottradeTable.findOneAndUpdate({
        _id: ObjectId(sellorderid)
      }, {
        "$set": {
          "filled": tempdata,
          "status": sellUpdate.status
        },
        "$inc": {
          "filledAmount": parseFloat(sellUpdate.filledAmt)
        }
      }, {
        new: true,
        "fields": {
          filled: 1
        }
      }, function(selltemp_err, selltempData) {
        // console.log(selltemp_err, 'selltemp_err')
        if (selltempData) {
          var updatebaldata = {};
          updatebaldata['spotwallet'] = parseFloat(tempdata.order_value) - parseFloat(fee);
          Assets.findOneAndUpdate({
            currencySymbol: tempdata.secondCurrency,
            userId: ObjectId(tempdata.user_id)
          }, {
            "$inc": updatebaldata
          }, {
            new: true,
            "fields": {
              spotwallet: 1
            }
          }, function(balerr, baldata) {
            // console.log(balerr,'buybalance error');
            // console.log(baldata,'buybaldata');
          });
          callback(null, selltempData);
        }
      });
    },
  ], function(err, result) {
    // console.log(result, 'buydetailsupdate')

    //Bonus updation
    // FeeTable.findOne({}).exec(function(err, bonusdetails) {
    //   console.log(bonusdetails, 'bonusdetails')
    //   if (bonusdetails) {
    //     var trade_bonus = bonusdetails.trade_bonus;
    //     var updatebonusdata = {};
    //     updatebonusdata["tempcurrency"] = trade_bonus;
    //     Assets.findOneAndUpdate({
    //       currencySymbol: 'BTC',
    //       userId: ObjectId(selluserid)
    //     }, {
    //       "$inc": updatebonusdata
    //     }, {
    //       new: true,
    //       "fields": {
    //         balance: 1
    //       }
    //     }, function(balerr, baldata) {
    //       // console.log(balerr, 'bale')
    //       // console.log(baldata, 'bale')
    //       const newBonus = new Bonus({
    //         userId: selluserid,
    //         bonus_amount: trade_bonus,
    //         type: '4',
    //       });
    //       newBonus.save(function(err, data) {
    //         // console.log(err,'err')
    //         // console.log(data,'data')
    //       });
    //     });
    //
    //     Assets.findOneAndUpdate({
    //       currencySymbol: 'BTC',
    //       userId: ObjectId(buyuserid)
    //     }, {
    //       "$inc": updatebonusdata
    //     }, {
    //       new: true,
    //       "fields": {
    //         balance: 1
    //       }
    //     }, function(balerr, baldata) {
    //       // console.log(balerr, 'bale')
    //       // console.log(baldata, 'bale')
    //       const newBonus = new Bonus({
    //         userId: buyuserid,
    //         bonus_amount: trade_bonus,
    //         type: '4',
    //       });
    //       newBonus.save(function(err, data) {
    //         // console.log(err,'err')
    //         // console.log(data,'data')
    //       });
    //     });
    //   }
    // });

    //socket call
    setTimeout(function() {
      gettradedata(result.filled[0].firstCurrency, result.filled[0].secondCurrency, socketio);
      getusertradedata(result.filled[0].selluserId, result.filled[0].firstCurrency, result.filled[0].secondCurrency);
      getusertradedata(result.filled[0].buyuserId, result.filled[0].firstCurrency, result.filled[0].secondCurrency);
    }, 3000);

    spottradeTable.findOneAndUpdate({
      pairid: (buyorderid),
      status: '4',
      stopstatus: '1'
    }, {
      "$set": {
        "stopstatus": '2'
      }
    }, {
      new: true,
      "fields": {
        status: 1
      }
    }, function(buytemp_err, buytempData) {

    });

    spottradeTable.findOneAndUpdate({
      pairid: (sellorderid),
      status: '4',
      stopstatus: '1'
    }, {
      "$set": {
        "stopstatus": '2'
      }
    }, {
      new: true,
      "fields": {
        status: 1
      }
    }, function(buytemp_err, buytempData) {

    });

    spottradeTable.find({
      status: '4',
      trigger_type: 'Last'
    }, function(buytemp_err, buytempData) {
      if (buytempData.length) {
        for (var i = 0; i < buytempData.length; i++) {
          var _id = buytempData[i]._id;
          var price = buytempData[i].price;
          var trigger_price = buytempData[i].trigger_price;
          var userId = buytempData[i].userId;
          var pairName = buytempData[i].pairName;
          var leverage = buytempData[i].leverage;
          var quantity = buytempData[i].quantity;
          var buyorsell = buytempData[i].buyorsell;
          var orderType = buytempData[i].orderType;
          var trailstop = buytempData[i].trailstop;
          var different = parseFloat(price) - parseFloat(trigger_price);
          // console.log(trigger_price,'trigger_price');
          // console.log(buyprice,'buyprice');
          if (different > 0) {
            if (trailstop == '0' && parseFloat(trigger_price) > parseFloat(buyprice)) {
              // order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
              spottradeTable.findOneAndUpdate({
                _id: ObjectId(_id),
                status: '4',
                stopstatus: {
                  $ne: '1'
                }
              }, {
                "$set": {
                  "status": '0'
                }
              }, {
                new: true,
                "fields": {
                  status: 1
                }
              }, function(buytemp_err, buytempData) {
                // console.log(buytemp_err,'trigger error');
              });
            }
          } else {
            if (spottradeTable == '0' && parseFloat(trigger_price) < parseFloat(buyprice)) {
              //order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
              spottradeTable.findOneAndUpdate({
                _id: ObjectId(_id),
                status: '4',
                stopstatus: {
                  $ne: '1'
                }
              }, {
                "$set": {
                  "status": '0'
                }
              }, {
                new: true,
                "fields": {
                  status: 1
                }
              }, function(buytemp_err, buytempData) {

                // console.log(buytemp_err,'trigger error');
              });
            }
          }
          //trailing stop trigger
          if (trailstop == '1' && buyorsell == 'buy' && parseFloat(price) > parseFloat(buyprice)) {
            var addprice = (parseFloat(buyprice) - parseFloat(price))
            var newtriggerprice = parseFloat(trigger_price) + parseFloat(addprice);
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "price": newtriggerprice,
                "trigger_price": newtriggerprice
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
          if (trailstop == '1' && buyorsell == 'buy' && parseFloat(trigger_price) < parseFloat(buyprice)) {
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "status": '0'
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
          if (trailstop == '1' && buyorsell == 'sell' && parseFloat(price) < parseFloat(buyprice)) {
            var addprice = (parseFloat(price) - parseFloat(buyprice))
            var newtriggerprice = parseFloat(trigger_price) - parseFloat(addprice);
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "price": newtriggerprice,
                "trigger_price": newtriggerprice
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
          if (trailstop == '1' && buyorsell == 'sell' && parseFloat(trigger_price) > parseFloat(buyprice)) {
            spottradeTable.findOneAndUpdate({
              _id: ObjectId(_id),
              status: '4',
              stopstatus: {
                $ne: '1'
              }
            }, {
              "$set": {
                "status": '0'
              }
            }, {
              new: true,
              "fields": {
                status: 1
              }
            }, function(buytemp_err, buytempData) {
              // console.log(buytemp_err,'trigger error');
            });
          }
        }
      }
    });

  });
}
async function buymatchingprocess(curorder, tradedata, pairData, io) {
  // console.log("buy");
  var buyOrder = curorder,
    buyAmt = rounds(buyOrder.quantity),
    buyorderid = buyOrder._id,
    buyuserid = buyOrder.userId,
    buyleverage = buyOrder.leverage,
    buyforced_liquidation = buyOrder.forced_liquidation,
    forceBreak = false,
    buyeramount = rounds(buyOrder.quantity),
    buyeramount1 = rounds(buyOrder.quantity),
    buytempamount = 0;
  buy_res_len = tradedata.length;
  console.log(buy_res_len, 'buy_res_len')
  // tradedata.forEach(function(data_loop){
  for (var i = 0; i < tradedata.length; i++) {
    var data_loop = tradedata[i];
    buyAmt = rounds(buyOrder.quantity - buytempamount);
    if (buyAmt == 0 || forceBreak == true) {
      // console.log("break");
      return;
    } else {
      var ii = i,
        sellOrder = data_loop,
        sellorderid = sellOrder._id,
        selluserid = sellOrder.userId,
        sellleverage = sellOrder.leverage,
        sellforced_liquidation = sellOrder.forced_liquidation,
        sellAmt = rounds(+sellOrder.quantity - +sellOrder.filledAmount),
        silentBreak = false,
        buyUpdate = {},
        sellUpdate = {},
        buyerBal = 0,
        sellerBal = 0,
        orderSocket = {}
      buyeramount = buyeramount - sellAmt;
      // console.log(buyAmt,"buyAmt");
      // console.log(Math.abs(sellAmt),"sellAmt");
      if (Math.abs(buyAmt) == Math.abs(sellAmt)) {
        // console.log("amount eq");
        buyUpdate = {
          status: '1',
          filledAmt: Math.abs(sellAmt)
        }
        sellUpdate = {
          status: '1',
          filledAmt: buyAmt
        }
        forceBreak = true
      } else if (Math.abs(buyAmt) > Math.abs(sellAmt)) {
        // console.log("else buy gt");
        buyUpdate = {
          status: '2',
          filledAmt: Math.abs(sellAmt)
        }
        sellUpdate = {
          status: '1',
          filledAmt: Math.abs(sellAmt)
        }
        buyAmt = rounds(+buyAmt - +sellAmt)
      } else if (Math.abs(buyAmt) < Math.abs(sellAmt)) {
        // console.log("else sell gt");
        buyUpdate = {
          status: '1',
          filledAmt: buyAmt
        }
        sellUpdate = {
          status: '2',
          filledAmt: buyAmt
        }
        forceBreak = true
      } else {
        silentBreak = true
      }

      if (silentBreak == false) {

        var order_value = parseFloat(buyUpdate.filledAmt * buyOrder.price).toFixed(8);
        var fee = parseFloat(buyUpdate.filledAmt) * pairData.taker_fees / 100;

        var tempdata = {
          "pair": ObjectId(pairData._id),
          "firstCurrency": pairData.first_currency,
          "secondCurrency": pairData.second_currency,
          "buyuserId": ObjectId(buyuserid),
          "user_id": ObjectId(buyuserid),
          "selluserId": ObjectId(selluserid),
          "sellId": ObjectId(sellorderid),
          "buyId": ObjectId(buyorderid),
          "filledAmount": +(buyUpdate.filledAmt).toFixed(8),
          "Price": +buyOrder.price,
          "pairname": curorder.pairName,
          "Fees": parseFloat(fee).toFixed(8),
          "status": "filled",
          "Type": "buy",
          "created_at": new Date(),
          "beforeBalance": curorder.beforeBalance,
          "afterBalance": curorder.afterBalance,
          "order_value": order_value,
        }
        buytempamount += +buyUpdate.filledAmt

        await buydetailsupdate(tempdata, buyorderid, buyUpdate, sellorderid, sellUpdate, selluserid, sellOrder.price, pairData.maker_rebate, io, sellforced_liquidation, sellleverage, sellOrder);
        if (tradedata.length == i && forceBreak != true && curorder.timeinforcetype == 'ImmediateOrCancel') {
          cancel_trade(curorder._id, curorder.userId);
        }
        // positionmatching(data_loop);
        if (forceBreak == true) {
          // console.log('forceBreak')
          return true;
        }
      }
    }
  }

}

async function sellmatchingprocess(curorder, tradedata, pairData, io) {
  var sellOrder = curorder,
    sellorderid = sellOrder._id,
    selluserid = sellOrder.userId,
    sellleverage = sellOrder.leverage,
    sellAmt = rounds(Math.abs(sellOrder.quantity)),
    forceBreak = false,
    selleramount = rounds(sellOrder.quantity)
  selleramount1 = rounds(sellOrder.quantity)
  sellerforced_liquidation = '';
  selltempamount = 0;
  sell_res_len = tradedata.length;
  console.log(sell_res_len, 'sell_res_len')
  for (var i = 0; i < tradedata.length; i++) {
    var data_loop = tradedata[i];
    sellAmt = rounds(Math.abs(sellOrder.quantity) - selltempamount);
    // console.log('loop starting',i);
    if (sellAmt == 0 || forceBreak == true)
      return

    var ii = i,
      buyOrder = data_loop,
      buyorderid = buyOrder._id,
      buyuserid = buyOrder.userId,
      buyleverage = buyOrder.leverage,
      buyforced_liquidation = buyOrder.forced_liquidation,
      buyAmt = rounds(buyOrder.quantity - buyOrder.filledAmount),
      silentBreak = false,
      buyUpdate = {},
      sellUpdate = {},
      buyerBal = 0,
      sellerBal = 0,
      orderSocket = {}
    selleramount = selleramount - buyAmt;

    // console.log(Math.abs(sellAmt),'sellamount');
    // console.log(buyAmt,'buyamount');
    if (Math.abs(sellAmt) == Math.abs(buyAmt)) {
      buyUpdate = {
        status: 1,
        filledAmt: Math.abs(sellAmt)
      }
      sellUpdate = {
        status: 1,
        filledAmt: buyAmt
      }
      forceBreak = true
    } else if (Math.abs(sellAmt) > Math.abs(buyAmt)) {
      buyUpdate = {
        status: 1,
        filledAmt: buyAmt
      }
      sellUpdate = {
        status: 2,
        filledAmt: buyAmt
      }
      sellAmt = rounds(+sellAmt - +buyAmt)
    } else if (Math.abs(sellAmt) < Math.abs(buyAmt)) {
      buyUpdate = {
        status: 2,
        filledAmt: Math.abs(sellAmt)
      }
      sellUpdate = {
        status: 1,
        filledAmt: Math.abs(sellAmt)
      }
      forceBreak = true
    } else {
      silentBreak = true
    }
    var returnbalance = 0;
    if (+buyOrder.price > +sellOrder.price) {
      var return_price = +buyOrder.price - +sellOrder.price;
      returnbalance = +buyUpdate.filledAmt * +return_price;
      returnbalance = parseFloat(returnbalance).toFixed(8);
    }

    if (silentBreak == false) {

      var order_value = parseFloat(buyUpdate.filledAmt * sellOrder.price).toFixed(8);
      var fee = parseFloat(buyUpdate.filledAmt) * parseFloat(pairData.maker_rebate) / 100;

      var tempdata = {
        "pair": ObjectId(pairData._id),
        "firstCurrency": pairData.first_currency,
        "secondCurrency": pairData.second_currency,
        "buyuserId": ObjectId(buyuserid),
        "user_id": ObjectId(buyuserid),
        "selluserId": ObjectId(selluserid),
        "sellId": ObjectId(sellorderid),
        "buyId": ObjectId(buyorderid),
        "filledAmount": +(buyUpdate.filledAmt).toFixed(8),
        "Price": +buyOrder.price,
        "pairname": curorder.pairName,
        "status": "filled",
        "Type": "buy",
        "Fees": parseFloat(fee).toFixed(8),
        "created_at": new Date(),
        "beforeBalance": buyOrder.beforeBalance,
        "afterBalance": buyOrder.afterBalance,
        "order_value": order_value,
      }
      selltempamount += +sellUpdate.filledAmt;
      // console.log(tempdata,'before sell update');
      await selldetailsupdate(tempdata, buyorderid, buyUpdate, sellorderid, sellUpdate, selluserid, sellOrder.price, pairData.taker_fees, io, sellerforced_liquidation, sellleverage, curorder);
      if (tradedata.length == i && forceBreak != true && curorder.timeinforcetype == 'ImmediateOrCancel') {
        cancel_trade(curorder._id, curorder.userId);
      }
      if (forceBreak == true) {
        // console.log("true");
        // getrateandorders(curorder.pair,userid);
        return true
      }
    }
    if (forceBreak == true) {
      // getrateandorders(curorder.pair,userid);
      return true
    }
  }
}


router.post("/spottradeorderplacing",(req,res)=>{
  // console.log("req.body in sportt",req.body);
  if(!req.body.pair){
   return res.json({status:true,message:"Please Select the Pair"})
  }
  spotpairs.findOne({tiker_root:req.body.pair.value}).then(spotpairs=>{
      if(spotpairs){
        var pairname = spotpairs.tiker_root;
        var markprice = parseFloat(spotpairs.markprice).toFixed(8);
      var tradeprice = 0
        if(req.body.tradeprice){
          tradeprice=parseFloat(req.body.tradeprice)
        }
        if(!req.body.tradequantity){
          return res.json({status:true,message:"Please Enter the Quantity"})
        }
        if(!req.body.buyorsell){
          return res.json({status:true,message:"Please Select the BUY/SELL"})
        }
        if(!req.body.ordertypechange){
          return res.json({status:true,message:"Please Select the Limit/Market"})
        }
        var buyorsell=req.body.buyorsell.value
        var ordertypechange=req.body.ordertypechange.value
        // tradeprice=ordertypechange=="Market"?markprice:req.body.tradeprice
        var tradequantity =parseFloat(req.body.tradequantity)
        var useridstatic=ObjectId("5f11301062c7e3584e61ec88")
        var timeinforcetype = "GoodTillCancelled";
        var trigger_price = 0;
        var trigger_type = null;
        var firstcurrency = spotpairs.first_currency;
        var secondcurrency = spotpairs.second_currency;
        var order_value = parseFloat(tradequantity * tradeprice).toFixed(8);
        var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD"]
        var balcurrency = (buyorsell == 'buy') ? secondcurrency : firstcurrency;
        var order_value1 = (buyorsell == 'buy') ? order_value : tradequantity;
        var before_reduce_bal=0
        var after_reduce_bal =0
        var float = (pairname == 'BTCUSDT' || pairname == 'ETHUSDT') ? 2 : 6;
        if(buyorsell=="buy"&& parseFloat(tradeprice)>markprice&&ordertypechange=="Limit"){
          return res.json({status:true,message:"Please Enter  the price below "+ markprice})
        }
        if(buyorsell=="sell"&& parseFloat(tradeprice)<markprice&&ordertypechange=="Limit"){
          return res.json({status:true,message:"Please Enter  the price above "+ markprice})
        }
        const newtradeTable = new spottradeTable({
          quantity: parseFloat(tradequantity),
          price: parseFloat(tradeprice).toFixed(float),
          trigger_price: trigger_price,
          orderValue: order_value,
          userId: useridstatic,
          pair: spotpairs._id,
          pairName: pairname,
          beforeBalance: before_reduce_bal,
          afterBalance: after_reduce_bal,
          timeinforcetype: timeinforcetype,
          firstCurrency:  spotpairs.first_currency,
          secondCurrency: spotpairs.second_currency,
          orderType: ordertypechange,
          trigger_type: trigger_type,
          orderDate:new Date(),
          buyorsell: buyorsell,
          status: (trigger_type != null) ? 4 : 0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
        });
        newtradeTable
          .save()
          .then(curorder => {
            console.log("order placedd for the trade");
            var io = req.app.get('socket');

            tradematching(curorder, io);

            return res.json({status:true,message:"Order Placed Succesfully"})

          }).catch(err => {
            console.log(err, 'error');

          });

      }
    })
})


function tradematching(curorder, io) {
  //Fill or kill order type
  if (curorder.timeinforcetype == "FillOrKill") {
    // console.log('filleorkill');
    var datas = {
        '$or': [{
          "status": '2'
        }, {
          "status": '0'
        }],
        'userId': {
          $ne: ObjectId(curorder.userId)
        },
        'pairName': (curorder.pairName)
      },
      sort;

    if (curorder.buyorsell == 'buy') {
      datas['buyorsell'] = 'sell'
      datas['price'] = {
        $lte: curorder.price
      }

      sort = {
        "price": 1
      }
    } else {
      datas['buyorsell'] = 'buy'
      datas['price'] = {
        $gte: curorder.price
      }
      sort = {
        "price": -1
      }
    }

    spottradeTable.aggregate([{
        $match: datas
      },
      {
        $group: {
          "_id": null,
          'quantity': {
            $sum: '$quantity'
          },
          'filledAmount': {
            $sum: '$filledAmount'
          }
        }
      },
      {
        $sort: sort
      },
      {
        $limit: 10
      },
    ]).exec((tradeerr, tradedata) => {
      if (tradedata.length > 0) {
        var quantity = tradedata[0].quantity;
        var filledAmount = tradedata[0].filledAmount;
        var pendingamount = parseFloat(Math.abs(quantity)) - parseFloat(Math.abs(filledAmount));
        if (Math.abs(curorder.quantity) > pendingamount) {
          var quant = parseFloat(Math.abs(curorder.quantity)) - parseFloat(pendingamount);

          var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD","DASHBTC","TRXBTC","XMRBTC","DASHUSDT","TRXUSDT","XMRUSDT","BNBUSDT","BNBBTC"]
          if ((curarray.includes(curorder.pairName))) {
            var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
            var newbuyorsell = (curorder.buyorsell == 'buy') ? 'sell' : 'buy';
            var quant = (curorder.buyorsell == 'buy') ? quant : (quant);
            order_placing(curorder.orderType, newbuyorsell, curorder.price, Math.abs(quant), curorder.leverage, curorder.pairName, oppuser_id);
          } else {
            cancel_trade(curorder._id, curorder.userId);
          }
        }
      } else {
        cancel_trade(curorder._id, curorder.userId);

      }
    });
  }
  var datas = {
      '$or': [{
        "status": '2'
      }, {
        "status": '0'
      }],
      'userId': {
        $ne: ObjectId(curorder.userId)
      },
      'pairName': (curorder.pairName)
    },
    sort;

  if (curorder.buyorsell == 'buy') {
    datas['buyorsell'] = 'sell'
    datas['price'] = {
      $lte: curorder.price
    }
    sort = {
      "price": 1
    }
  } else {
    datas['buyorsell'] = 'buy'
    datas['price'] = {
      $gte: curorder.price
    }
    sort = {
      "price": -1
    }
  }
  if (curorder.userId.toString() == "5e567694b912240c7f0e4299") {
    datas['price'] = curorder.price
  }
  // console.log(datas,'datas');
  spottradeTable.aggregate([{
      $match: datas
    },
    {
      $sort: sort
    },
    {
      $limit: 50
    },
  ]).exec((tradeerr, tradedata) => {
    spotpairs.findOne({
      tiker_root: (curorder.pairName)
    }).exec(function(pairerr, pairData) {
      // console.log('perpetual');
      if (tradeerr)
        console.log({
          status: false,
          message: tradeerr
        });
      else
        // console.log(tradedata, 'tradedata')
      if (tradedata.length > 0) {
        if (curorder.postOnly) {
          cancel_trade(curorder._id, curorder.userId);
        }
        var i = 0;
        if (curorder.buyorsell == 'buy') {
          buyside(curorder, tradedata, pairData, io);
        } else if (curorder.buyorsell == 'sell') {
          sellside(curorder, tradedata, pairData, io);
        }
      } else {
        // console.log('elseepart')
        if (curorder.timeinforcetype == "ImmediateOrCancel") {
          cancel_trade(curorder._id, curorder.userId);
        }
        gettradedata(curorder.firstCurrency, curorder.secondCurrency, socketio);
        var oppuser_id = ObjectId("5e567694b912240c7f0e4299");

        if (curorder.status == '0' && curorder.orderType == 'Market' && curorder.userId.toString() != oppuser_id.toString()) {
          // console.log('ifepart')
          var curarray = ["BTCUSDT", "ETHUSDT", "XRPUSDT", "LTCUSDT", "BCHUSDT", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "LTCETH", "XRPETH", "BCHETH", "BTCBUSD", "ETHBUSD", "LTCBUSD", "XRPBUSD", "BCHBUSD","DASHBTC","TRXBTC","XMRBTC","DASHUSDT","TRXUSDT","XMRUSDT","BNBUSDT","BNBBTC"]
          if ((curarray.includes(curorder.pairName))) {
            // console.log('hereddd');
            var newbuyorsell = (curorder.buyorsell == 'buy') ? 'sell' : 'buy';
            order_placing(curorder.orderType, newbuyorsell, curorder.price, Math.abs(curorder.quantity), curorder.pairName, oppuser_id);
          }
        }

      }
    });
  });
}

function buyside(curorder, tradedata, pairData, io) {

  var tradePos = 0;
  var curtradequan = parseFloat(Math.abs(curorder.quantity)) - parseFloat(Math.abs(curorder.filledAmount));
  var tradequan = parseFloat(Math.abs(tradedata[0].quantity)) - parseFloat(Math.abs(tradedata[0].filledAmount));
  var tradequan1 = parseFloat(Math.abs(tradedata[0].quantity)) - parseFloat(Math.abs(tradedata[0].filledAmount));

  tradeinfo.filledamount = (Math.abs(curtradequan) == Math.abs(tradequan)) ? Math.abs(tradequan) : (Math.abs(curtradequan) > Math.abs(tradequan)) ? Math.abs(tradequan) : (Math.abs(curtradequan) < Math.abs(tradequan)) ? Math.abs(tradequan) : 0;

  tradedata[0].pairData = pairData;
  tradedata[0].quantity = tradequan;
  curorder.quantity = curtradequan;
  tradedata[0].curorder = curorder;
  var tradetails = tradedata[0];

  buymatchingtrade(tradetails, function() {
    if (tradePos === tradedata.length - 1 || parseFloat(tradequan1) == parseFloat(curtradequan)) {
      callBackResponseImport();
    } else {
      tradePos += 1;
      var tradequan1 = parseFloat(tradedata[tradePos].quantity) - parseFloat(tradedata[tradePos].filledAmount);
      curtradequan -= parseFloat(Math.abs(tradeinfo.filledamount));
      tradeinfo.filledamount = (Math.abs(curtradequan) == Math.abs(tradequan1)) ? Math.abs(tradequan1) : (Math.abs(curtradequan) > Math.abs(tradequan1)) ? Math.abs(tradequan1) : (Math.abs(curtradequan) < Math.abs(tradequan1)) ? Math.abs(tradequan1) : 0;

      tradedata[tradePos].pairData = pairData;
      curorder.quantity = curtradequan
      tradedata[tradePos].curorder = curorder;
      tradedata[tradePos].quantity = tradequan1;
      var tradetails = tradedata[tradePos];

      if (tradedata[tradePos]) {
        buymatchingtrade(tradetails);
      } else {
        callBackResponseImport();
      }
    }

  });
}

function sellside(curorder, tradedata, pairData, io) {
  var tradePos = 0;
  var curtradequan = parseFloat(Math.abs(curorder.quantity)) - parseFloat(Math.abs(curorder.filledAmount));
  var tradequan = parseFloat(tradedata[0].quantity) - parseFloat(tradedata[0].filledAmount);
  tradeinfo.tradequan1 = tradequan;

  tradeinfo.filledamount = (Math.abs(curtradequan) == Math.abs(tradequan)) ? Math.abs(tradequan) : (Math.abs(curtradequan) > Math.abs(tradequan)) ? Math.abs(tradequan) : (Math.abs(curtradequan) < Math.abs(tradequan)) ? Math.abs(tradequan) : 0;

  // console.log(tradequan, 'tradequan')
  // console.log(curtradequan, 'curtradequan')
  tradedata[0].pairData = pairData;
  tradedata[0].quantity = tradequan;
  curorder.quantity = curtradequan;
  tradedata[0].curorder = curorder;
  var tradetails = tradedata[0];

  sellmatchingtrade(tradetails, function() {
    if (tradePos === tradedata.length - 1 || parseFloat(tradeinfo.tradequan1) == parseFloat(curtradequan)) {
      // console.log('in');
      callBackResponseImport();
    } else {
      tradePos += 1;

      var tradequan1 = parseFloat(tradedata[tradePos].quantity) - parseFloat(tradedata[tradePos].filledAmount);
      tradeinfo.tradequan1 = tradequan1;
      curtradequan -= parseFloat(Math.abs(tradeinfo.filledamount));

      tradeinfo.filledamount = (Math.abs(curtradequan) == Math.abs(tradequan1)) ? Math.abs(tradequan1) : (Math.abs(curtradequan) > Math.abs(tradequan1)) ? Math.abs(tradequan1) : (Math.abs(curtradequan) < Math.abs(tradequan1)) ? Math.abs(tradequan1) : 0;
      // console.log(tradequan1, 'tradequan1')
      // console.log(curtradequan, 'curtradequan11')
      tradedata[tradePos].pairData = pairData;
      curorder.quantity = curtradequan
      tradedata[tradePos].curorder = curorder;
      tradedata[tradePos].quantity = tradequan1;
      var tradetails = tradedata[tradePos];

      if (tradedata[tradePos]) {
        sellmatchingtrade(tradetails);
      } else {
        callBackResponseImport();
      }
    }

  });
}

function buymatchingtrade(tradedata, callBackOne) {
  var curorder = tradedata.curorder;
  var buyOrder = curorder,
    buyAmt = rounds(buyOrder.quantity),
    buyorderid = buyOrder._id,
    buyuserid = buyOrder.userId,
    buyleverage = buyOrder.leverage,
    buyforced_liquidation = buyOrder.forced_liquidation,
    buyeramount = rounds(buyOrder.quantity),
    buyeramount1 = rounds(buyOrder.quantity),
    buytempamount = 0;

  var data_loop = tradedata;
  buyAmt = rounds(buyOrder.quantity);
  // var ii = i,
  sellOrder = data_loop,
    sellorderid = sellOrder._id,
    selluserid = sellOrder.userId,
    sellleverage = sellOrder.leverage,
    sellforced_liquidation = sellOrder.forced_liquidation,
    sellAmt = rounds(+sellOrder.quantity),
    buyUpdate = {},
    sellUpdate = {};
  if (Math.abs(buyAmt) == Math.abs(sellAmt)) {
    // console.log("amount eq");
    buyUpdate = {
      status: '1',
      filledAmt: Math.abs(sellAmt)
    }
    sellUpdate = {
      status: '1',
      filledAmt: buyAmt
    }
  } else if (Math.abs(buyAmt) > Math.abs(sellAmt)) {
    // console.log("else buy gt");
    buyUpdate = {
      status: '2',
      filledAmt: Math.abs(sellAmt)
    }
    sellUpdate = {
      status: '1',
      filledAmt: Math.abs(sellAmt)
    }
    buyAmt = rounds(+buyAmt - +sellAmt)
  } else if (Math.abs(buyAmt) < Math.abs(sellAmt)) {
    // console.log("else sell gt");
    buyUpdate = {
      status: '1',
      filledAmt: buyAmt
    }
    sellUpdate = {
      status: '2',
      filledAmt: buyAmt
    }
  }
  var order_value = parseFloat(buyUpdate.filledAmt * sellOrder.price).toFixed(8);
  var fee = parseFloat(buyUpdate.filledAmt) * parseFloat(sellOrder.pairData.maker_rebate) / 100;

  var tempdata = {
    "pair": ObjectId(sellOrder.pairData._id),
    "firstCurrency": sellOrder.pairData.first_currency,
    "secondCurrency": sellOrder.pairData.second_currency,
    "buyuserId": ObjectId(buyuserid),
    "user_id": ObjectId(buyuserid),
    "selluserId": ObjectId(selluserid),
    "sellId": ObjectId(sellorderid),
    "buyId": ObjectId(buyorderid),
    "uniqueid": Math.floor(Math.random() * 1000000000),
    "filledAmount": +(buyUpdate.filledAmt).toFixed(8),
    "Price": +buyOrder.price,
    "pairname": curorder.pairName,
    "Fees": parseFloat(fee).toFixed(8),
    "status": "filled",
    "Type": "buy",
    "created_at": new Date(),
    "beforeBalance": curorder.beforeBalance,
    "afterBalance": curorder.afterBalance,
    "order_value": order_value,
  }
  buytempamount += +buyUpdate.filledAmt

  buydetailsupdate(tempdata, buyorderid, buyUpdate, sellorderid, sellUpdate, selluserid, sellOrder.price, sellOrder.pairData.maker_rebate, socketio, sellforced_liquidation, sellleverage, sellOrder, callBackOne);
  // if(tradedata.length==i && forceBreak!=true && curorder.timeinforcetype=='ImmediateOrCancel')
  // {
  //     cancel_trade(curorder._id,curorder.userId);
  // }
}

function sellmatchingtrade(tradedata, callBackOne) {
  var curorder = tradedata.curorder;
  var sellOrder = curorder,
    sellorderid = sellOrder._id,
    selluserid = sellOrder.userId,
    sellleverage = sellOrder.leverage,
    sellAmt = rounds(Math.abs(sellOrder.quantity)),
    selleramount = rounds(sellOrder.quantity)
  selleramount1 = rounds(sellOrder.quantity)
  sellerforced_liquidation = (sellOrder.forced_liquidation)
  selltempamount = 0;
  sell_res_len = tradedata.length;
  var data_loop = tradedata;
  sellAmt = rounds(Math.abs(sellOrder.quantity) - selltempamount);

  // var ii                   = i,
  buyOrder = data_loop,
    buyorderid = buyOrder._id,
    buyuserid = buyOrder.userId,
    buyleverage = buyOrder.leverage,
    buyforced_liquidation = buyOrder.forced_liquidation,
    buyAmt = rounds(buyOrder.quantity),
    buyUpdate = {},
    sellUpdate = {},

    selleramount = selleramount - buyAmt;
  // console.log(sellAmt, 'sellAmt')
  // console.log(buyAmt, 'buyAmt')
  if (Math.abs(sellAmt) == Math.abs(buyAmt)) {
    buyUpdate = {
      status: 1,
      filledAmt: Math.abs(sellAmt)
    }
    sellUpdate = {
      status: 1,
      filledAmt: buyAmt
    }
    forceBreak = true
  } else if (Math.abs(sellAmt) > Math.abs(buyAmt)) {
    buyUpdate = {
      status: 1,
      filledAmt: buyAmt
    }
    sellUpdate = {
      status: 2,
      filledAmt: buyAmt
    }
    sellAmt = rounds(+sellAmt - +buyAmt)
  } else if (Math.abs(sellAmt) < Math.abs(buyAmt)) {
    buyUpdate = {
      status: 2,
      filledAmt: Math.abs(sellAmt)
    }
    sellUpdate = {
      status: 1,
      filledAmt: Math.abs(sellAmt)
    }
  }

  var order_value = parseFloat(buyUpdate.filledAmt * sellOrder.price).toFixed(8);
  var fee = parseFloat(buyUpdate.filledAmt) * parseFloat(buyOrder.pairData.maker_rebate) / 100;


  var tempdata = {
    "pair": ObjectId(buyOrder.pairData._id),
    "firstCurrency": buyOrder.pairData.first_currency,
    "secondCurrency": buyOrder.pairData.second_currency,
    "buyuserId": ObjectId(buyuserid),
    "user_id": ObjectId(buyuserid),
    "selluserId": ObjectId(selluserid),
    "sellId": ObjectId(sellorderid),
    "buyId": ObjectId(buyorderid),
    "filledAmount": +(buyUpdate.filledAmt).toFixed(8),
    "Price": +sellOrder.price,
    "uniqueid": Math.floor(Math.random() * 1000000000),
    "pairname": curorder.pairName,
    "status": "filled",
    "Type": "buy",
    "Fees": parseFloat(fee).toFixed(8),
    "created_at": new Date(),
    "beforeBalance": buyOrder.beforeBalance,
    "afterBalance": buyOrder.afterBalance,
    "order_value": order_value,
  }
  selltempamount += +sellUpdate.filledAmt;

  selldetailsupdate(tempdata, buyorderid, buyUpdate, sellorderid, sellUpdate, selluserid, sellOrder.price, buyOrder.pairData.maker_rebate, socketio, sellerforced_liquidation, sellleverage, curorder, callBackOne);
  // if(tradedata.length==i && forceBreak!=true && curorder.timeinforcetype=='ImmediateOrCancel')
  // {
  //     cancel_trade(curorder._id,curorder.userId);
  // }
}

function callBackResponseImport() {
  tradeinfo.filledamount = 0;
  console.log('fskdmflskmdflskdmflksmdf');
}

function rounds(n) {
  var roundValue = (+n).toFixed(8)
  return parseFloat(roundValue)
}

router.post('/user-activate', (req, res) => {
  var userid = req.body.userid;
  var updateObj = {
    active: "Activated"
  }
  User.findByIdAndUpdate(userid, updateObj, {
    new: true
  }, function(err, user) {
    if (user) {
      return res.status(200).json({
        message: 'Your Account activated successfully'
      });
    }
  })
});

router.post('/spotpair-data/', (req, res) => {
  spotpairs.find({}).sort({
    priority: 1
  }).then(result => {
    if (result) {
var result =result.sort((a, b) => parseFloat(b.markprice) - parseFloat(a.markprice));


      return res.status(200).json({
        status: true,
        data: result,
        type: 'perpetual'
      });
    }
  });
});

router.post('/spotorder-history/', (req, res) => {
  spottradeTable.find({
    userId: ObjectId(req.body.userid)
  }).sort({
    '_id': -1
  }).then(result => {
    if (result) {
      return res.status(200).json({
        status: true,
        data: result,
        type: "orderhistory"
      });
    }
  });
});




router.post('/spotsearchorder-history/', (req, res) => {
  var userid = req.body.userid;
  var contract = req.body.contract;
  var type = req.body.type;
  var startDate = req.body.startDate;
  var endDate = req.body.endDate;
  var match = {};
  match['userId'] = userid;
  if (contract != 'All') {
    match['pairName'] = contract;
  }
  if (type != 'All') {
    match['buyorsell'] = type.toLowerCase();
  }
  if (startDate != '' && endDate != '') {
    match['orderDate'] = {
      $gte: startDate,
      $lte: endDate
    };
  } else if (startDate != '') {
    match['orderDate'] = {
      $gte: startDate
    };
  } else if (endDate != '') {
    match['orderDate'] = {
      $lte: endDate
    };
  }
  spottradeTable.find(match).sort({
    '_id': -1
  }).then(result => {
    if (result) {
      return res.status(200).json({
        status: true,
        data: result,
        type: "orderhistory"
      });
    }
  });
});


router.post('/spotsearchtrade-history/', (req, res) => {
  var userid = req.body.userid;
  var contract = req.body.contract;
  var type = req.body.type;
  var startDate = req.body.startDate;
  var endDate = req.body.endDate;
  var match = {};
  match['userId'] = userid;
  match['status'] = '1';
  if (contract != 'All') {
    match['pairName'] = contract;
  }
  if (type != 'All') {
    match['buyorsell'] = type.toLowerCase();
  }
  if (startDate != '' && endDate != '') {
    match['orderDate'] = {
      $gte: startDate,
      $lte: endDate
    };
  } else if (startDate != '') {
    match['orderDate'] = {
      $gte: startDate
    };
  } else if (endDate != '') {
    match['orderDate'] = {
      $lte: endDate
    };
  }
  spottradeTable.find(match).sort({
    '_id': -1
  }).then(result => {
    if (result) {
      return res.status(200).json({
        status: true,
        data: result,
        type: "orderhistory"
      });
    }
  });
});

router.post('/spottrade-history/', (req, res) => {
  spottradeTable.find({
    status: 1,
    userId: ObjectId(req.body.userid)
  }).sort({
    '_id': -1
  }).then(result => {
    if (result) {
      return res.status(200).json({
        status: true,
        data: result,
        type: "tradehistory"
      });
    }
  });
});




function stopordertrigger(pair) {
// console.log('stopordertrigger');
  spotpairs.findOne({
    tiker_root: pair
  }, {
    tiker_root: 1,
    first_currency: 1,
    second_currency: 1,
    markprice: 1,
    maxquantity: 1,
    minquantity: 1
  }, function(err, contractdetails) {

    if (contractdetails) {
      var markprice = contractdetails.markprice;
      if (markprice != '' && !isNaN(markprice)) {
        //stop order
        spottradeTable.find({
          trigger_ordertype: 'stop',
          pairName: pair,
          status: '4',
          stopstatus: '2',
          trigger_type: 'Mark',
          buyorsell: 'sell',
          trigger_price: {
            $gte: markprice
          },
          userId: {
            $ne: ObjectId("5e567694b912240c7f0e4299")
          }
        }, function(buytemp_err, buytempData) {
          // console.log(markprice,'markprice');
          // console.log(buytempData,'buytempData');
          if (buytempData.length) {

            var i = 0;
            generatestopsellorder(buytempData[0], function () {
              // console.log("buytempData of array",buytempData.length);
              if (i === buytempData.length - 1) {
                // console.log("inside the if sss")
                callBackResponseImport();
              } else {
                // console.log("isndie else")
                i += 1;
                if (buytempData[i]) {
                  // console.log("next creatinon token ss",currencytokendetails[i]);
                  generatestopsellorder(buytempData[i]);
                } else {
                  callBackResponseImport();
                }
              }
            });
          }
        });

        spottradeTable.find({
          trigger_ordertype: 'stop',
          pairName: pair,
          status: '4',
          stopstatus: '2',
          trigger_type: 'Mark',
          buyorsell: 'buy',
          trigger_price: {
            $lte: markprice
          },
          userId: {
            $ne: ObjectId("5e567694b912240c7f0e4299")
          }
        }, function(buytemp_err, buytempData) {
          if (buytempData.length) {
            var i = 0;
            generatestopbuyorder(buytempData[0], function () {
              // console.log("buytempData of array",buytempData.length);
              if (i === buytempData.length - 1) {
                // console.log("inside the if sss")
                callBackResponseImport();
              } else {
                // console.log("isndie else")
                i += 1;
                if (buytempData[i]) {
                  // console.log("next creatinon token ss",currencytokendetails[i]);
                  generatestopbuyorder(buytempData[i]);
                } else {
                  callBackResponseImport();
                }
              }
            });
          }
        });
        //take profit
        spottradeTable.findOneAndUpdate({
          trigger_ordertype: 'takeprofit',
          pairName: pair,
          status: '4',
          stopstatus: '2',
          trigger_type: 'Mark',
          buyorsell: 'sell',
          trigger_price: {
            $lte: markprice
          }
        }, {
          "$set": {
            "status": '0'
          }
        }, {
          new: true,
          "fields": {
            status: 1
          }
        }, function(buytemp_err, buytempData) {

        });
        spottradeTable.findOneAndUpdate({
          trigger_ordertype: 'takeprofit',
          pairName: pair,
          status: '4',
          stopstatus: '2',
          trigger_type: 'Mark',
          buyorsell: 'buy',
          trigger_price: {
            $gte: markprice
          }
        }, {
          "$set": {
            "status": '0'
          }
        }, {
          new: true,
          "fields": {
            status: 1
          }
        }, function(buytemp_err, buytempData) {

        });

        
        spottradeTable.find({
          pairName: pair,
          buyorsell: 'sell',
          userId: {
            $ne: ObjectId("5e567694b912240c7f0e4299")
          },
          price: {
            $lte: (parseFloat(markprice))
          },
          '$or': [{
            "status": '0'
          }, {
            "status": '2'
          }]
        }).limit(100).sort({
          "price": 1
        }).exec(function(buytemp_err, buytempData) {

          if (!buytemp_err && buytempData.length > 0) {

            var i = 0;
            generatespotpairsellorder(buytempData[0], function () {
              // console.log("buytempData of array",buytempData.length);
              if (i === buytempData.length - 1) {
                // console.log("inside the if sss")
                callBackResponseImport();
              } else {
                // console.log("isndie else")
                i += 1;
                if (buytempData[i]) {
                  // console.log("next creatinon token ss",currencytokendetails[i]);
                  generatespotpairsellorder(buytempData[i]);
                } else {
                  callBackResponseImport();
                }
              }
            });
          }
        });

        spottradeTable.find({
          pairName: pair,
          buyorsell: 'buy',
          userId: {$ne : ObjectId("5e567694b912240c7f0e4299")},
          price: {
            $gte: (parseFloat(markprice))
          },
          '$or': [{
            "status": '0'
          }, {
            "status": '2'
          }]
        }).limit(100).sort({
          "price": -1
        }).exec(function(buytemp_err, buytempData) {
          if (buytempData.length) {
            var i = 0;
            generatespotpairbuyorder(buytempData[0], function () {
              // console.log("buytempData of array",buytempData.length);
              if (i === buytempData.length - 1) {
                // console.log("inside the if sss")
                callBackResponseImport();
              } else {
                // console.log("isndie else")
                i += 1;
                if (buytempData[i]) {
                  // console.log("next creatinon token ss",currencytokendetails[i]);
                  generatespotpairbuyorder(buytempData[i]);
                } else {
                  callBackResponseImport();
                }
              }
            });
          }

        });

        spottradeTable.findOneAndUpdate(
        {
          pairName: pair,
          // orderType: "Market",
          userId : ObjectId("5e567694b912240c7f0e4299"),
          buyorsell: "sell",
          price: { $lt: parseFloat(markprice) },
          $or: [{ status: "0" }, { status: "2" }],
        },
        { $set: { status: "3" } },
        { new: true, fields: { status: 1 } },
        function (buytemp_err, buytempData) {});

        spottradeTable.findOneAndUpdate(
        {
            pairName: pair,
            // orderType: "Market",
            buyorsell: "buy",
            userId : ObjectId("5e567694b912240c7f0e4299"),
            price: { $gt: parseFloat(markprice) },
            $or: [{ status: "0" }, { status: "2" }],
        },
        { $set: { status: "3" } },
        { new: true, fields: { status: 1 } },
        function (buytemp_err, buytempData) {});



      }
    }
  });
}


function generatestopsellorder(buytempData, callBackfour)
 {
  if (callBackfour) {
    userinfo.callstopsellorder = callBackfour;
  }
    var _id = buytempData._id;
    var price = buytempData.price;
    var trigger_price = buytempData.trigger_price;
    var userId = buytempData.userId;
    var pairName = buytempData.pairName;
    var leverage = buytempData.leverage;
    var quantity = buytempData.quantity;
    var buyorsell = buytempData.buyorsell;
    var orderType = buytempData.orderType;
    var trailstop = buytempData.trailstop;
    var pos_leverage = buytempData.leverage;
    var pos_liqprice = buytempData.Liqprice;
    var curorder = buytempData;
    curorder.status = '0';
    var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
    spottradeTable.findOneAndUpdate({
      _id: ObjectId(_id),
      status: '4',
      stopstatus: '2'
    }, {
      "$set": {
        "status": '0',
        price: trigger_price
      }
    }, {
      new: true,
      "fields": {
        status: 1
      }
    }, function(buytemp_err, buytempDat) {
      // order_placing('Market','buy',parseFloat(trigger_price).toFixed(2),Math.abs(quantity),pos_leverage,pairName,oppuser_id,trigger_price=0,trigger_type=null,id=0,'',trailstopdistance=0);
      tradematching(curorder);
      userinfo.callstopsellorder()
    });
  }


  function generatestopbuyorder(buytempData, callBackfive)
   {
    if (callBackfive) {
      userinfo.callstopbuyorder = callBackfive;
    }
      var _id = buytempData._id;
      var price = buytempData.price;
      var trigger_price = buytempData.trigger_price;
      var userId = buytempData.userId;
      var pairName = buytempData.pairName;
      var leverage = buytempData.leverage;
      var quantity = buytempData.quantity;
      var buyorsell = buytempData.buyorsell;
      var orderType = buytempData.orderType;
      var trailstop = buytempData.trailstop;
      var pos_leverage = buytempData.leverage;
      var pos_liqprice = buytempData.Liqprice;
      var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
      var curorder = buytempData;
      curorder.status = '0';
      spottradeTable.findOneAndUpdate({
        _id: ObjectId(_id),
        status: '4',
        stopstatus: '2'
      }, {
        "$set": {
          "status": '0',
          price: trigger_price
        }
      }, {
        new: true,
        "fields": {
          status: 1
        }
      }, function(buytemp_err, buytempDat) {
        //order_placing('Market','sell',parseFloat(trigger_price).toFixed(2),Math.abs(quantity),pos_leverage,pairName,oppuser_id,trigger_price=0,trigger_type=null,id=0,'',trailstopdistance=0);
        // console.log(curorder, 'tempdata')
        tradematching(curorder);
        userinfo.callstopbuyorder()
      });

  }


  function generatespotsellorder(buytempData, callBacksix)
   {
    if (callBacksix) {
      userinfo.callsellorder = callBacksix;
    }
    var _id = buytempData._id;
    var userId = buytempData.userId;
    console.log(_id, 'dfsdkfsldkfslkdjflskdjflskdjf');
    cancel_trade(_id, userId)
    userinfo.callsellorder()
  }

  function generatespotpairsellorder(buytempData, callBackseven)
   {
    if (callBackseven) {
      userinfo.callpairsellorder = callBackseven;
    }
    var _id = buytempData._id;
    var userId = buytempData.userId;
    var buyorsell = buytempData.buyorsell;
    var pairName = buytempData.pairName;
    var quantity = buytempData.quantity - buytempData.filledAmount;
    var price = buytempData.price;
    var leverage = buytempData.leverage;
    var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
    var float = (pairName == 'BTCUSDT' || pairName == 'ETHUSDT') ? 2 : 8;
    order_placing('Market', 'buy', parseFloat(price).toFixed(float), Math.abs(quantity), pairName, oppuser_id, trigger_price = 0, trigger_type = null, id = 0, '', trailstopdistance = 0);
userinfo.callpairsellorder()
  }

  function generatespotpairsellsecondorder(buytempData, callBackeight)
   {
    if (callBackeight) {
      userinfo.callsellsecondorder = callBackeight;
    }
    var _id =  buytempData._id;
    var userId =  buytempData.userId;
    var buyorsell =  buytempData.buyorsell;
    var pairName =  buytempData.pairName;
    var quantity =  buytempData.quantity -  buytempData.filledAmount;
    var price =  buytempData.price;
    var leverage =  buytempData.leverage;
    var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
    cancel_trade(_id, userId);
    userinfo.callsellsecondorder()
  }


  function generatespotpairbuyorder(buytempData, callBacknine)
   {
    if (callBacknine) {
      userinfo.callpairbuyorder = callBacknine;
    }
    var _id =  buytempData._id;
    var userId =  buytempData.userId;
    var buyorsell =  buytempData.buyorsell;
    var pairName =  buytempData.pairName;
    var quantity =  buytempData.quantity -  buytempData.filledAmount;
    var price =  buytempData.price;
    var leverage =  buytempData.leverage;
    var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
    //var index         = position_details.findIndex(x => (x._id) === userId);
    //console.log(index,'index')
    var float = (pairName == 'BTCUSDT' || pairName == 'ETHUSDT') ? 2 : 8;
    order_placing('Market', 'sell', parseFloat(price).toFixed(float), Math.abs(quantity), pairName, oppuser_id, trigger_price = 0, trigger_type = null, id = 0, '', trailstopdistance = 0);

    userinfo.callpairbuyorder()
  }


  function generatespotpairbuysecondorder(buytempData, callBackten)
   {
    if (callBackten) {
      userinfo.callpairbuysecondorder = callBackten;
    }
    var _id =  buytempData._id;
    var userId =  buytempData.userId;
    var buyorsell =  buytempData.buyorsell;
    var pairName =  buytempData.pairName;
    var quantity =  buytempData.quantity -  buytempData.filledAmount;
    var price =  buytempData.price;
    var leverage =  buytempData.leverage;
    var oppuser_id = ObjectId("5e567694b912240c7f0e4299");
    //var index         = position_details.findIndex(x => (x._id) === userId);
    //console.log(index,'index')
    cancel_trade(_id, userId);

    userinfo.callpairbuysecondorder()
  }




router.get('/markets', (req, res) => {
  perpetual.aggregate([{
    $project: {
      _id: 0,
      name: "$tiker_root",
      type: "crypto",
      exchange: "Alwin",
    }
  }]).exec(function(err, pairdata) {
    res.json(pairdata);
  });
});

router.get('/chart/:config', (request, response) => {
  var uri = url.parse(request.url, true);
  // console.log(uri.query,'querydfjsldjflsdjflsdkfjldfs');
  var action = uri.pathname;
  // console.log(action,'action');
  switch (action) {
    case '/chart/config':
      action = '/config';
      break;
    case '/chart/time':
      action = '/time';
      break;
    case '/chart/symbols':
      symbolsDatabase.initGetAllMarketsdata();
      action = '/symbols';
      break;
    case '/chart/history':
      action = '/history';
      break;
  }
  return requestProcessor.processRequest(action, uri.query, response);
});

router.get('/chartData', (req, res) => {
  // console.log('callhere chart');
  var url = require('url');
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;


  var pair = req.query.market;
  var start_date = req.query.start_date;
  var end_date = req.query.end_date;
  var resol = req.query.resolution;
  var spl = pair.split("_");
  var first = spl[0];
  var second = spl[1];
  var pattern = /^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/;
  var _trProject;
  var _trGroup;
  var _exProject;
  var _exGroup;
  if (start_date) {
    if (!pattern.test(start_date)) {
      res.json({
        "message": "Start date is not a valid format"
      });
      return false;
    }
  } else {
    res.json({
      "message": "Start date parameter not found"
    });
    return false;
  }
  if (end_date) {
    if (!pattern.test(end_date)) {
      res.json({
        "message": "End date is not a valid format"
      });
      return false;
    }
  } else {
    res.json({
      "message": "End date parameter not found"
    });
    return false;
  }
  charts.findOne({
    type: resol,
    pairname: pair
  }).exec(function(err, result) {
    if (result) {
      res.json(result.data);
    } else {
      res.json([]);
    }
  });

});

router.get('/chartspotupdate', (req, res) => {
  var url = require('url');
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;

  var pair = req.query.market;
  var resol = req.query.resolution;
  chartspotupdate(resol, pair);
});
router.get('/chartData1', (req, res) => {
  // console.log('callhere chart');
  var url = require('url');
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;

  var pair = req.query.market;
  var start_date = req.query.start_date;
  var end_date = req.query.end_date;
  var resol = req.query.resolution;
  var spl = pair.split("_");
  var first = spl[0];
  var second = spl[1];
  var pattern = /^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/;
  var _trProject;
  var _trGroup;
  var _exProject;
  var _exGroup;
  if (start_date) {
    if (!pattern.test(start_date)) {
      res.json({
        "message": "Start date is not a valid format"
      });
      return false;
    }
  } else {
    res.json({
      "message": "Start date parameter not found"
    });
    return false;
  }
  if (end_date) {
    if (!pattern.test(end_date)) {
      res.json({
        "message": "End date is not a valid format"
      });
      return false;
    }
  } else {
    res.json({
      "message": "End date parameter not found"
    });
    return false;
  }
  var sDate = start_date + ' 00:00:0.000Z';
  var eDate = end_date + ' 00:00:0.000Z';

  // console.log(start_date,'start_date');
  // console.log(end_date,'end_date');

  // console.log(sDate,'start_date');
  // console.log(eDate,'end_date');
  if (sDate > eDate) {
    res.json({
      "message": "Please ensure that the End Date is greater than or equal to the Start Date"
    });
  }
  // perpetual.find({tiker_root:pair}).select("_id").select("tiker_root").exec(function(err,pairdata){
  try {
    // if(pairdata.length > 0)
    // {
    //   var pairId   = pairdata[0]._id;
    //   var pairname = pairdata[0].tiker_root;
    // console.log(pairname);
    var limits;
    var project = {
      Date: "$Date",
      pair: "$pair",
      low: "$low",
      high: "$high",
      open: "$open",
      close: "$close",
      volume: "$volume",
      exchange: "GlobalCryptoX"
    };


    if (resol) {
      if (resol != 1 && resol != 5 && resol != 15 && resol != 30 && resol != 60 && resol != '1d' && resol != '2d' && resol != '3d' && resol != 'd' && resol != '1w' && resol != '3w' && resol != 'm' && resol != '6m') {
        res.json({
          "message": "Resolution value is not valid"
        });
        return false;
      } else {
        if (resol == '1d') {
          console.log('1d');
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            // "hour": {
            //     "$hour": "$createdAt"
            // },
            // "minute": { "$minute": "$createdAt" },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                $add: [{
                    $subtract: [{
                        $minute: "$modifiedDate"
                      },
                      {
                        $mod: [{
                          $minute: "$modifiedDate"
                        }, +resol]
                      }
                    ]
                  },
                  +resol
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }

        } else if (resol == 'd') {
          _trProject = {
            "week": {
              "$week": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: '$pairname',
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "week": "$week",
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }
          }

          resol = 10080;
        } else if (resol == '1m') {

          _trProject = {
            "month": {
              "$month": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "month": "$month",
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pairname'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }
          }

          resol = 43200;
        } else if (resol == 1) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": "$minute"
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 5) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 5]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 30) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 30]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 60) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour"
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 15) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 15]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                $add: [{
                    $subtract: [{
                        $minute: "$modifiedDate"
                      },
                      {
                        $mod: [{
                          $minute: "$modifiedDate"
                        }, +resol]
                      }
                    ]
                  },

                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }

        }
      }
    }
    // console.log(end_date,'eDate');
    console.log(moment(end_date).add(1, 'days'), 'eDate');
    // console.log(moment(start_date).format(),'sDate');
    spotPrices.aggregate([{
        $match: {
          "pairname": pair,
          "createdAt": {
            "$lt": new Date(moment(end_date).add('1 days')),
            "$gte": new Date(moment(start_date).format())
          },
        }
      },
      // {$limit: 500000},
      {
        $project: _trProject
      },
      {
        "$group": _trGroup
      },
      {
        $project: project,
      },
      {
        $sort: {
          "Date": 1,

        }
      },
      // {
      //                      allowDiskUse: true
      //                    },
    ]).exec(function(err, result) {
      // console.log(err,'err');
      //console.log(result,'result');
      res.json(result);
    });

    // }
    // else
    // {
    //     // console.log("no pair data");
    //     res.json({"message" : "No Pair Found"});
    // }
  } catch (e) {
    console.log("no pair", e);
  }
  // console.log(pairdata);
  // });

});

cron.schedule('*/5 * * * * *',async (req, res) => {
  var pairdetails = await spotpairs.find({});
  if(pairdetails.length>0)
  {
      for(var ia=0;ia<pairdetails.length;ia++)
      {
          var markprice = pairdetails[ia].markprice;
          // console.log(markprice,'markprice');
          if(markprice!="")
          {
            // console.log("first if")
              var buytempData = await spottradeTable.find({pairName: pairdetails[ia].tiker_root, buyorsell: 'sell', userId: { $ne: ObjectId("5e567694b912240c7f0e4299") },
                price: { $lte: (parseFloat(markprice)) }, '$or': [{  "status": '0'}, { "status": '2'}] }).limit(100).sort({ "price": 1 });
              // console.log(buytempData,'buytempData')
              if(buytempData.length>0)
              {
                // console.log("here goingn")
              for(var is=0;is<buytempData.length;is++)
              {
                // console.log("next goingn")
                  var _id            = buytempData[is]._id;
                  var userId         = buytempData[is].userId;
                  var buyorsell      = buytempData[is].buyorsell;
                  var pairName       = buytempData[is].pairName;
                  var quantity       = buytempData[is].quantity -  buytempData[is].filledAmount;
                  var price          = buytempData[is].price;
                  var leverage       = buytempData[is].leverage;
                  var order_value    = quantity * price;
                  var firstcurrency  = pairdetails[ia].first_currency;
                  var secondcurrency = pairdetails[ia].second_currency;
                  var oppuser_id     = ObjectId("5e567694b912240c7f0e4299");
                  var float = (pairName == 'BTCUSDT' || pairName == 'ETHUSDT') ? 2 : 8;
                  const newtradeTable = new spottradeTable({
                        quantity       : parseFloat(quantity).toFixed(8),
                        price          : parseFloat(price).toFixed(float),
                        trigger_price  : 0,
                        orderValue     : order_value,
                        userId         : oppuser_id,
                        pair           : pairdetails[ia]._id,
                        pairName       : pairName,
                        beforeBalance  : 0,
                        afterBalance   : 0,
                        firstCurrency  : firstcurrency,
                        secondCurrency : secondcurrency,
                        orderType      : "Market",
                        stopstatus     : '0',
                        buyorsell      : "buy",
                        pairid         : _id,
                        trailstop      : '1',
                        orderDate      : new Date(),
                        status         : 1 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                      });
                  var curorder = await newtradeTable.save();
                  
                  var buyfee = parseFloat(order_value) * parseFloat(pairdetails[ia].taker_fees) / 100;

                  var buytempdata = {
                    "pair"           : ObjectId(pairdetails[ia]._id),
                    "firstCurrency"  : firstcurrency,
                    "secondCurrency" : secondcurrency,
                    "buyuserId"      : oppuser_id,
                    "user_id"        : oppuser_id,
                    "selluserId"     : ObjectId(userId),
                    "sellId"         : ObjectId(_id),
                    "buyId"          : ObjectId(curorder._id),
                    "filledAmount"   : +(quantity).toFixed(8),
                    "Price"          : +price,
                    "pairname"       : pairName,
                    "status"         : "filled",
                    "Type"           : "buy",
                    "Fees"           : parseFloat(buyfee).toFixed(8),
                    "created_at"     : new Date(),
                    "beforeBalance"  : 0,
                    "afterBalance"   : 0,
                    "order_value"    : order_value,
                  }

                   var selltempdata = {
                    "pair"           : ObjectId(pairdetails[ia]._id),
                    "firstCurrency"  : firstcurrency,
                    "secondCurrency" : secondcurrency,
                    "buyuserId"      : oppuser_id,
                    "user_id"        : userId,
                    "selluserId"     : ObjectId(userId),
                    "sellId"         : ObjectId(_id),
                    "buyId"          : ObjectId(curorder._id),
                    "filledAmount"   : +(quantity).toFixed(8),
                    "Price"          : +price,
                    "pairname"       : pairName,
                    "status"         : "filled",
                    "Type"           : "sell",
                    "Fees"           : parseFloat(buyfee).toFixed(8),
                    "created_at"     : new Date(),
                    "beforeBalance"  : 0,
                    "afterBalance"   : 0,
                    "order_value"    : order_value,
                  }

                await spottradeTable.findOneAndUpdate({_id: ObjectId(curorder._id)}, {"$set": {"status": "1","filled": buytempdata},"$inc": {"filledAmount":parseFloat(quantity)}}, {new: true,"fields": { status: 1,filled: 1} });
                 
                await spottradeTable.findOneAndUpdate({_id: ObjectId(_id)}, {"$set": {"status": "1","filled": selltempdata},"$inc": {"filledAmount":parseFloat(quantity)}}, {new: true,"fields": { status: 1,filled: 1} });


              }
            }

               var selltempData = await spottradeTable.find({pairName: pairdetails[ia].tiker_root, buyorsell: 'buy', userId: { $ne: ObjectId("5e567694b912240c7f0e4299") },
                price: { $gte: (parseFloat(markprice)) }, '$or': [{  "status": '0'}, { "status": '2'}] }).limit(100).sort({ "price": 1 });
              // console.log(selltempData,'selltempData')
              if(selltempData.length>0)
              {
                // console.log("here goingn")
              for(var is=0;is<selltempData.length;is++)
              {
                // console.log("next goingn")
                  var _id            = selltempData[is]._id;
                  var userId         = selltempData[is].userId;
                  var buyorsell      = selltempData[is].buyorsell;
                  var pairName       = selltempData[is].pairName;
                  var quantity       = selltempData[is].quantity -  selltempData[is].filledAmount;
                  var price          = selltempData[is].price;
                  var leverage       = selltempData[is].leverage;
                  var order_value    = quantity * price;
                  var firstcurrency  = pairdetails[ia].first_currency;
                  var secondcurrency = pairdetails[ia].second_currency;
                  var oppuser_id     = ObjectId("5e567694b912240c7f0e4299");
                  var float = (pairName == 'BTCUSDT' || pairName == 'ETHUSDT') ? 2 : 8;
                  const newtradeTable = new spottradeTable({
                        quantity       : parseFloat(quantity).toFixed(8),
                        price          : parseFloat(price).toFixed(float),
                        trigger_price  : 0,
                        orderValue     : order_value,
                        userId         : oppuser_id,
                        pair           : pairdetails[ia]._id,
                        pairName       : pairName,
                        beforeBalance  : 0,
                        afterBalance   : 0,
                        firstCurrency  : firstcurrency,
                        secondCurrency : secondcurrency,
                        orderType      : "Market",
                        stopstatus     : '0',
                        buyorsell      : "sell",
                        pairid         : _id,
                        trailstop      : '1',
                        orderDate      : new Date(),
                        status         : 1 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                      });
                  var curorder = await newtradeTable.save();
                  
                  var buyfee = parseFloat(order_value) * parseFloat(pairdetails[ia].taker_fees) / 100;

                  var buytempdata = {
                    "pair"           : ObjectId(pairdetails[ia]._id),
                    "firstCurrency"  : firstcurrency,
                    "secondCurrency" : secondcurrency,
                    "buyuserId"      : userId,
                    "user_id"        : userId,
                    "selluserId"     : ObjectId(oppuser_id),
                    "sellId"         : ObjectId(curorder._id),
                    "buyId"          : ObjectId(_id),
                    "filledAmount"   : +(quantity).toFixed(8),
                    "Price"          : +price,
                    "pairname"       : pairName,
                    "status"         : "filled",
                    "Type"           : "buy",
                    "Fees"           : parseFloat(buyfee).toFixed(8),
                    "created_at"     : new Date(),
                    "beforeBalance"  : 0,
                    "afterBalance"   : 0,
                    "order_value"    : order_value,
                  }

                   var selltempdata = {
                    "pair"           : ObjectId(pairdetails[ia]._id),
                    "firstCurrency"  : firstcurrency,
                    "secondCurrency" : secondcurrency,
                    "buyuserId"      : userId,
                    "user_id"        : oppuser_id,
                    "selluserId"     : ObjectId(oppuser_id),
                    "sellId"         : ObjectId(curorder._id),
                    "buyId"          : ObjectId(_id),
                    "filledAmount"   : +(quantity).toFixed(8),
                    "Price"          : +price,
                    "pairname"       : pairName,
                    "status"         : "filled",
                    "Type"           : "sell",
                    "Fees"           : parseFloat(buyfee).toFixed(8),
                    "created_at"     : new Date(),
                    "beforeBalance"  : 0,
                    "afterBalance"   : 0,
                    "order_value"    : order_value,
                  }

                await spottradeTable.findOneAndUpdate({_id: ObjectId(curorder._id)}, {"$set": {"status": "1","filled": buytempdata},"$inc": {"filledAmount":parseFloat(quantity)}}, {new: true,"fields": { status: 1,filled: 1} });
                 
                await spottradeTable.findOneAndUpdate({_id: ObjectId(_id)}, {"$set": {"status": "1","filled": selltempdata},"$inc": {"filledAmount":parseFloat(quantity)}}, {new: true,"fields": { status: 1,filled: 1} });

              }
            }

      }
  }
}
});

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BTCUSDT');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('ETHUSDT');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('LTCUSDT');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BCHUSDT');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('XRPUSDT');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('ETHUSDT');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('ETHBTC');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('DASHBTC');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('DASHUSDT');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('TRXBTC');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('TRXUSDT');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('XMRBTC');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('XMRUSDT');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BNBUSDT');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BNBBTC');
// });
// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('LTCBTC');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('XRPBTC');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BCHBTC');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BTCBUSD');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('ETHBUSD');
// });


// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('XRPBUSD');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('LTCBUSD');
// });

// cron.schedule('*/5 * * * * *', (req, res) => {
//   stopordertrigger('BCHBUSD');
// });

// cron.schedule('*/5 * * * *', (req,res) => {
//   chartupdate('5','BTCUSD');
//   chartupdate('5','ETHUSD');
//   chartupdate('5','LTCUSD');
//   chartupdate('5','BCHUSD');
//   chartupdate('5','XRPUSD');

// //   tradeTable.remove({userId:ObjectId("5e567694b912240c7f0e4299"),'$or' : [{"status" : '0'},{"status" : '2'}]}, function(err) {
// //     if (!err) {
// //             console.log("success");
// //     }
// //     else {

// //             console.log(err);
// //     }
// // });
// });

// cron.schedule('* * * * *', (req,res) => {
//   chartupdate('1','BTCUSD');
//   chartupdate('1','ETHUSD');
//   chartupdate('1','BCHUSD');
//   chartupdate('1','LTCUSD');
//   chartupdate('1','XRPUSD');
// });

// cron.schedule('*/15 * * * *', (req,res) => {
//   chartupdate('15','BTCUSD');
//   chartupdate('15','ETHUSD');
//   chartupdate('15','BCHUSD');
//   chartupdate('15','LTCUSD');
//   chartupdate('15','XRPUSD');
// });

// cron.schedule('0 * * * *', (req,res) => {
//   chartupdate('60','BTCUSD');
//   chartupdate('60','ETHUSD');
//   chartupdate('60','BCHUSD');
//   chartupdate('60','LTCUSD');
//   chartupdate('60','XRPUSD');
// });

// cron.schedule('0 * * * *', (req,res) => {
//   chartupdate('60','BTCUSD');
//   chartupdate('60','ETHUSD');
//   chartupdate('60','BCHUSD');
//   chartupdate('60','LTCUSD');
//   chartupdate('60','XRPUSD');
// });

// cron.schedule('0 0 * * *', (req,res) => {
//   chartupdate('1d','BTCUSD');
//   chartupdate('1d','ETHUSD');
//   chartupdate('1d','BCHUSD');
//   chartupdate('1d','LTCUSD');
//   chartupdate('1d','XRPUSD');
// });

function chartupdate(resol, pair) {

  try {

    var limits;
    var project = {
      Date: "$Date",
      pair: "$pair",
      low: "$low",
      high: "$high",
      open: "$open",
      close: "$close",
      volume: "$volume",
      exchange: "GlobalCryptoX"
    };


    if (resol) {
      var restype = resol;
      if (resol != 1 && resol != 5 && resol != 15 && resol != 30 && resol != 60 && resol != '1d' && resol != '2d' && resol != '3d' && resol != 'd' && resol != '1w' && resol != '3w' && resol != 'm' && resol != '6m') {
        res.json({
          "message": "Resolution value is not valid"
        });
        return false;
      } else {
        if (resol == '1d') {
          console.log('1d');
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            // "hour": {
            //     "$hour": "$createdAt"
            // },
            // "minute": { "$minute": "$createdAt" },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                $add: [{
                    $subtract: [{
                        $minute: "$modifiedDate"
                      },
                      {
                        $mod: [{
                          $minute: "$modifiedDate"
                        }, +resol]
                      }
                    ]
                  },
                  +resol
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }

        } else if (resol == 'd') {
          _trProject = {
            "week": {
              "$week": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: '$pairname',
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "week": "$week",
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }
          }

          resol = 10080;
        } else if (resol == 'm') {

          _trProject = {
            "month": {
              "$month": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "month": "$month",
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pairname'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }
          }

          resol = 43200;
        } else if (resol == 1) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": "$minute"
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 5) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 5]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 30) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 30]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 60) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour"
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 15) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 15]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$createdAt"
            },
            "mn": {
              "$month": "$createdAt"
            },
            "dt": {
              "$dayOfMonth": "$createdAt"
            },
            "hour": {
              "$hour": "$createdAt"
            },
            "minute": {
              "$minute": "$createdAt"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairname",
            modifiedDate: '$createdAt',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                $add: [{
                    $subtract: [{
                        $minute: "$modifiedDate"
                      },
                      {
                        $mod: [{
                          $minute: "$modifiedDate"
                        }, +resol]
                      }
                    ]
                  },

                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }

        }
      }
    }
    // console.log(end_date,'eDate');
    // console.log(moment(end_date).add(1, 'days'),'eDate');

    var d1 = new Date();
    var d2 = new Date(d1);
    d2.setMinutes(d1.getMinutes() - parseFloat(restype));
    // console.log(d2)
    // console.log(d1)
    // console.log(moment(start_date).format(),'sDate');
    spotPrices.aggregate([{
        $match: {
          "pairname": pair,
          "createdAt": {
            "$lt": new Date(),
            "$gte": d2
          },
        }
      },
      // {$limit: 500000},
      {
        $project: _trProject
      },
      {
        "$group": _trGroup
      },
      {
        $project: project,
      },
      {
        $sort: {
          "Date": 1,

        }
      },

    ]).exec(function(err, result) {

      charts.update({
        type: restype,
        pairname: pair
      }, {
        $addToSet: {
          data: result
        }
      }, function(err, ress) {
        // console.log(err)
        // console.log(ress)
      });
    });


  } catch (e) {
    console.log("no pair", e);
  }

}


function chartspotupdate(resol, pair) {

  try {

    var limits;
    var project = {
      Date: "$Date",
      pair: "$pair",
      low: "$low",
      high: "$high",
      open: "$open",
      close: "$close",
      volume: "$volume",
      exchange: "GlobalCryptoX"
    };


    if (resol) {
      var restype = resol;
      if (resol != 1 && resol != 5 && resol != 15 && resol != 30 && resol != 60 && resol != '1d' && resol != '2d' && resol != '3d' && resol != 'd' && resol != '1w' && resol != '3w' && resol != 'm' && resol != '6m') {
        res.json({
          "message": "Resolution value is not valid"
        });
        return false;
      } else {
        if (resol == '1d') {
          console.log('1d');
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            // "hour": {
            //     "$hour": "$createdAt"
            // },
            // "minute": { "$minute": "$createdAt" },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                $add: [{
                    $subtract: [{
                        $minute: "$modifiedDate"
                      },
                      {
                        $mod: [{
                          $minute: "$modifiedDate"
                        }, +resol]
                      }
                    ]
                  },
                  +resol
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }

        } else if (resol == 'd') {
          _trProject = {
            "week": {
              "$week": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: '$pairName',
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "week": "$week",
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }
          }

          resol = 10080;
        } else if (resol == 'm') {

          _trProject = {
            "month": {
              "$month": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "month": "$month",
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pairName'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }
          }

          resol = 43200;
        } else if (resol == 1) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            "hour": {
              "$hour": "$orderDate"
            },
            "minute": {
              "$minute": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": "$minute"
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 5) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            "hour": {
              "$hour": "$orderDate"
            },
            "minute": {
              "$minute": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 5]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 30) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            "hour": {
              "$hour": "$orderDate"
            },
            "minute": {
              "$minute": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 30]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 60) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            "hour": {
              "$hour": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour"
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else if (resol == 15) {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            "hour": {
              "$hour": "$orderDate"
            },
            "minute": {
              "$minute": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                "$subtract": [{
                    "$minute": "$modifiedDate"
                  },
                  {
                    "$mod": [{
                      "$minute": "$modifiedDate"
                    }, 15]
                  }
                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }
        } else {
          resol = 1440;
          _trProject = {
            "yr": {
              "$year": "$orderDate"
            },
            "mn": {
              "$month": "$orderDate"
            },
            "dt": {
              "$dayOfMonth": "$orderDate"
            },
            "hour": {
              "$hour": "$orderDate"
            },
            "minute": {
              "$minute": "$orderDate"
            },
            "filledAmount": 1,
            "price": 1,
            pair: "$pairName",
            modifiedDate: '$orderDate',
          }
          _trGroup = {
            "_id": {
              "year": "$yr",
              "month": "$mn",
              "day": "$dt",
              "hour": "$hour",
              "minute": {
                $add: [{
                    $subtract: [{
                        $minute: "$modifiedDate"
                      },
                      {
                        $mod: [{
                          $minute: "$modifiedDate"
                        }, +resol]
                      }
                    ]
                  },

                ]
              }
            },
            count: {
              "$sum": 1
            },
            Date: {
              $last: "$modifiedDate"
            },
            pair: {
              $first: '$pair'
            },
            low: {
              $min: '$price'
            },
            high: {
              $max: '$price'
            },
            open: {
              $first: '$price'
            },
            close: {
              $last: '$price'
            },
            volume: {
              $sum: '$filledAmount'
            }

          }

        }
      }
    }
    // console.log(end_date,'eDate');
    // console.log(moment(end_date).add(1, 'days'),'eDate');

    var d1 = new Date();
    var d2 = new Date(d1);
    d2.setMinutes(d1.getMinutes() - parseFloat(restype));
    // console.log(d2)
    // console.log(d1)
    // console.log(moment(start_date).format(),'sDate');
    spottradeTable.aggregate([{
        $match: {
          "pairName": pair,
          "orderDate": {
            "$lt": new Date(),
            "$gte": d2
          },
        }
      },
      // {$limit: 500000},
      {
        $project: _trProject
      },
      {
        "$group": _trGroup
      },
      {
        $project: project,
      },
      {
        $sort: {
          "Date": 1,

        }
      },

    ]).exec(function(err, result) {
      console.log(err, 'err')
      console.log(result, 'chartresut')
      // charts.update(
      // { type: restype,pairname: pair },
      // { $addToSet: { data: result } },function(err,ress){
      //   // console.log(err)
      //   // console.log(ress)
      // });
    });


  } catch (e) {
    console.log("no pair", e);
  }

}


cron.schedule("0 0 1 * *", (req, res) => {
  // one minute
  removeChatData("1", "BTCUSD", "Date");
  removeChatData("1", "ETHUSD", "Date");
  removeChatData("1", "BCHUSD", "Date");
  removeChatData("1", "DASHUSD", "Date");
  removeChatData("1", "XMRUSD", "Date");
  removeChatData("1", "LTCUSD", "Date");
  removeChatData("1", "BNBUSD", "Date");
  removeChatData("1", "TRXUSD", "Date");
  removeChatData("1", "ETHBTC", "Date");
  removeChatData("1", "BCHBTC", "Date");
  removeChatData("1", "DASHBTC", "Date");
  removeChatData("1", "XMRBTC", "Date");
  removeChatData("1", "LTCBTC", "Date");
  removeChatData("1", "BNBBTC", "Date");
  removeChatData("1", "XRPBTC", "Date");

  // five minute
  removeChatData("5", "BTCUSD", "Date");
  removeChatData("5", "ETHUSD", "Date");
  removeChatData("5", "BCHUSD", "Date");
  removeChatData("5", "ETHBTC", "Date");
  removeChatData("5", "DASHUSD", "Date");
  removeChatData("5", "LTCUSD", "Date");
  removeChatData("5", "XMRUSD", "Date");
  removeChatData("5", "BNBUSD", "Date");
  removeChatData("5", "TRXUSD", "Date");
  removeChatData("5", "BCHBTC", "Date");
  removeChatData("5", "DASHBTC", "Date");
  removeChatData("5", "XMRBTC", "Date");
  removeChatData("5", "LTCBTC", "Date");
  removeChatData("5", "BNBBTC", "Date");
  removeChatData("5", "XRPBTC", "Date");
});

function removeChatData(type, pairname, format) {
  let date = new Date();
  let toDay = new Date(date.setMonth( date.getMonth() - 1 ));
  // let toDay = `${date.getFullYear()}-${date.getMonth() + 1}-01`
  // console.log("----~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~toDay", toDay)
  // if (format == "Date") {
  //   toDay = new Date(Date.now() - 24 * 60 * 60 * 1000)
  // }
  console.log("----~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~toDay", toDay)
  charts.updateMany(
    {
      "pairname": pairname,
      "type": type
    },
    {
      "$pull": {
        "data": {
          "Date": {
            "$lte": toDay
          }
        }
      }
    },
    function (err, ress) {
      console.log("----",err)
      console.log("----@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", ress)
    }
  )
}




module.exports = router;
