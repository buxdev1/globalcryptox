const express                     = require('express');
const router                      = express.Router();
const bcrypt                      = require('bcryptjs');
const jwt                         = require('jsonwebtoken');
const keys                        = require('../../config/keys');
const async                       = require("async");
const validateTradeInput          = require('../../validation/frontend/trade');
const validatemobRegisterInput    = require('../../validation/frontend/mobregister');
const validateLoginInput          = require('../../validation/login');
const validatemobLoginInput       = require('../../validation/moblogin');
const validateUpdateUserInput     = require('../../validation/frontend/updateUser');
const validateEmailtemplateInput  = require('../../validation/emailtemplate');
const validateForgotInput         = require('../../validation/forgot');
const validateCmsInput            = require('../../validation/cms');
const validateFaqInput            = require('../../validation/faq');
const validateUpdateSettingsInput = require('../../validation/settings');
const validateResetInput          = require('../../validation/frontend/resetpassword');
const validatetfaInput            = require('../../validation/frontend/tfainput');
const validateContactInput        = require('../../validation/frontend/contact_us');
const tradeTable                  = require('../../models/tradeTable');
const Assets                      = require('../../models/Assets');
const position_table              = require('../../models/position_table');
const currency                    = require('../../models/currency');
const User                        = require('../../models/User');
const Emailtemplates              = require('../../models/emailtemplate');
const exchangePrices              = require('../../models/exchangePrices');
const nodemailer                  = require('nodemailer');
const multer                      = require('multer');
var node2fa                       = require('node-2fa');
var CryptoJS                      = require("crypto-js");
const perpetual                   = require('../../models/perpetual');
const cryptoRandomString          = require('crypto-random-string');
const client                      = require('twilio')(
  keys.TWILIO_ACCOUT_SID,
  keys.TWILIO_AUTH_TOKEN
);
const mongoose                    = require('mongoose');
const ObjectId                    = mongoose.Types.ObjectId;

var cron = require('node-cron');

var request = require('request');


const rp = require('request-promise')


let user_list  = [{"Id":1 },{"Id":2 },{"Id":3 },{"Id":4 },{"Id":5 }];
let URL = "https://jsonplaceholder.typicode.com/users/"

function make_api_call(id,exchange){
  if(exchange=='Bitstamp')
  {
    console.log('bit here');
    return rp({
          url : 'https://www.bitstamp.net/api/v2/ticker/'+id.toLowerCase(),
          method : 'GET',
          json : true
      })
  }

  if(exchange=='Kraken')
  {
    var id = (id=='BTCUSD')?'XBTUSD':(id=='ETHUSD')?'ETHUSD':'';
    console.log(id,'pairdetails');
    return rp({
          url : 'https://api.kraken.com/0/public/Ticker?pair='+id,
          method : 'GET',
          json : true
      })
  }

  if(exchange=='Coinbasepro')
  {
    var id = (id=='BTCUSD')?'BTC-USD':(id=='ETHUSD')?'ETH-USD':'';
    console.log(id,'pairdetails');
    return rp({
          url : 'https://api.pro.coinbase.com/products/'+id+'/ticker',
          method : 'GET',
          json : true
      })
  }

  if(exchange=='Gemini')
  {
    return rp({
          url : 'https://api.gemini.com/v1/pubticker/'+id.toLowerCase(),
          method : 'GET',
          json : true
      })
  }

}

async function processUsers(contractdetails,exchange){
  console.log(contractdetails.length,'length of contract');
    if(contractdetails.length>0)
    {
      for(var i=0;i<contractdetails.length;i++)
      {
        if(contractdetails[i].tiker_root=='ETHUSD' || contractdetails[i].tiker_root=='BTCUSD')
        {
          result = await make_api_call(contractdetails[i].tiker_root,exchange);
          if(exchange=='Bitstamp')
          {
              const newrecord = new exchangePrices({
                  "low"          : (result.low)?result.low:0,
                  "high"         : (result.high)?result.high:0,
                  "exchangename" : exchange,
                  "last"         : (result.last)?result.last:0,
                  "volume"       : (result.volume)?result.volume:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                  console.log("success1");
                  });
          }

          if(exchange=='Kraken')
          {
            var result = (contractdetails[i].tiker_root=='ETHUSD')?result.result.XETHZUSD:(contractdetails[i].tiker_root=='BTCUSD')?result.result.XXBTZUSD:[];
              const newrecord = new exchangePrices({
                  "low"          : (result.l[0])?result.l[0]:0,
                  "high"         : (result.h[0])?result.h[0]:0,
                  "exchangename" : exchange,
                  "last"         : (result.p[0])?result.p[0]:0,
                  "volume"       : (result.v[0])?result.v[0]:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                  console.log("success1");
                  });
          }

           if(exchange=='Coinbasepro')
          {
              const newrecord = new exchangePrices({
                  "low"          : (result.ask)?result.ask:0,
                  "high"         : (result.bid)?result.bid:0,
                  "exchangename" : exchange,
                  "last"         : (result.price)?result.price:0,
                  "volume"       : (result.volume)?result.volume:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                  console.log("success1");
                  });
          }

           if(exchange=='Gemini')
          {
              const newrecord = new exchangePrices({
                  "low"          : (result.ask)?result.ask:0,
                  "high"         : (result.bid)?result.bid:0,
                  "exchangename" : exchange,
                  "last"         : (result.last)?result.last:0,
                  "volume"       : (result.volume)?result.volume[contractdetails[i].first_currency]:0,
                  "pair"         : ObjectId(contractdetails[i]._id),
                  "pairname"     : contractdetails[i].tiker_root
                  });
                  newrecord.save(function(err,data) {
                    console.log(err,'error1');
                    console.log(data,'data1');
                    console.log("success1");
                  });
          }

        }
    }
  }

}


router.get('/apitest', (req, res) => {
  perpetual.find({},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){
   
 }).then((contractdetails)=> {
  
processUsers(contractdetails,'Bitstamp');
processUsers(contractdetails,'Kraken');
processUsers(contractdetails,'Coinbasepro');
processUsers(contractdetails,'Gemini');                
  });

});
router.get('/balance', (req, res) => {
  // res.json({statue:"success"});


   User.find({}, function(err, userdetails) {
      if (userdetails) {
        userdetails.forEach(function(res) {
          var userId = res._id;
          currency.find({}, function(err, currencydetails) {
            currencydetails.forEach(function(cur){
              var insertobj = {
                "balance":0,
                "currency":cur._id,
                "currencySymbol":cur.currencySymbol
              };

              const newContact = new Assets({
               "balance":0,
                "currency":cur._id,
                "currencySymbol":cur.currencySymbol,
                "userId":userId
            });
           newContact.save(function(err,data) {
            console.log("success");
           });

            });
          });
        });
        res.send('success');
          
      }
    })
});

router.post('/getTradeData', (req, res) => {
    var findObj = {
        firstCurrency:req.body.firstCurrency,
        secondCurrency:req.body.secondCurrency
    };
    var pair = req.body.firstCurrency + req.body.secondCurrency;
    var result = {};
    // tradeTable.find(findObj,function(err,tradeTableAll){
    async.parallel({
      buyOrder : function(cb) {
        var sort = {'price':-1};
        tradeTable.aggregate([
        {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:req.body.firstCurrency,secondCurrency:req.body.secondCurrency,buyorsell:'buy'}},
        {
          $group : {
            '_id' : '$price',
            'quantity' : { $sum : '$quantity' },
            'filledAmount' : { $sum : '$filledAmount' }
          }
        },
        {$sort:sort},
        {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      sellOrder : function(cb) {
        var sort = {'price':1};
        tradeTable.aggregate([
        {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:req.body.firstCurrency,secondCurrency:req.body.secondCurrency,buyorsell:'sell'}},
        {
          $group : {
            '_id' : '$price',
            'quantity' : { $sum : '$quantity' },
            'filledAmount' : { $sum : '$filledAmount' }
          }
        },
        {$sort:sort},
        {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },

      Assetdetails : function(cb) {
         Assets.find({userId:ObjectId(req.body.userid)}).exec(cb)
      },
      contractdetails : function(cb) {
         perpetual.findOne({first_currency:req.body.firstCurrency,second_currency:req.body.secondCurrency},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1}).exec(cb)
      },
      Rescentorder : function(cb) {
      tradeTable.aggregate([
      {$match:{'pairName': pair,'status':'1'}},
      {$unwind:"$filled"},
      {$project:{"filled":1}},
      {$group:{_id:{"buyuserId":'$filled.buyuserId',"selluserId":'$filled.selluserId',"sellId":"$filled.sellId","buyId":"$filled.buyId"},"created_at":{ $first:"$filled.created_at" },"Type":{$first:"$filled.Type"},"filledAmount":{$first:"$filled.filledAmount"},"pairname":{$first:"$filled.pairname"},"Price":{$first:"$filled.Price"}}},
     {$limit: 50},
     ]).exec(cb)
   },
    },(err,results) => {
        if(err){
            result.status      = false;
            result.message     = 'Error occured.';
            result.err         = err;
            result.notify_show = 'no';
            res.json(result);
        } else if(results){
            result.status       = true;
            result.message      = 'tradeTableAll';
            result.buyOrder     = results.buyOrder;
            result.sellOrder    = results.sellOrder;
            result.contractdetails    = results.contractdetails;
            result.notify_show  = 'no';
            result.assetdetails = results.Assetdetails;
            result.Rescentorder = results.Rescentorder;
             res.json(result);
        } else {
            result.status = false;
            result.message = 'Error occured.';
            result.err = '';
            result.notify_show = 'no';
            res.json(result);
        }
    });
});
function cancel_trade(tradeid,userid)
{
  update      = { status : '3'}
  tradeTable.aggregate([
    {$match:{'_id' : ObjectId(tradeid),'status':{ $ne: '3' }}},
  ]).exec((tradeerr,tradedata) => { 
    if(tradedata.length > 0) {
      var type                =  tradedata[0].buyorsell;
      var trade_ids           =  tradedata[0]._id;
      var userId              =  tradedata[0].userId;
      var filledAmt           =  tradedata[0].filledAmount;
      var status              =  tradedata[0].status;
      var quantity            =  tradedata[0].quantity;
      var price               =  tradedata[0].price;
      var t_firstcurrencyId   =  tradedata[0].firstCurrency;
      var t_secondcurrencyId  =  tradedata[0].secondCurrency;
      var leverage            =  tradedata[0].leverage;

      quantity = parseFloat(quantity) - parseFloat(filledAmt);

      var leverage1       = parseFloat(100/leverage);

      var initial_margin = ((parseFloat(quantity)/parseFloat(price))*leverage1)/100;
      var feetoopen      = (quantity/price)*0.075/100;
      var Bankruptcy     = price * leverage /(parseFloat(leverage) + 1);
      var feetoclose     = (quantity/Bankruptcy) * 0.075/100;
      var order_value    = parseFloat(quantity/price).toFixed(8);
      var order_cost     = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);


  
      async.parallel({
      // update balance 
      data1: function(cb){
      var updatebaldata = {};
        updatebaldata["balance"] = order_cost; 
        console.log(order_cost,'order cost');
        Assets.findOneAndUpdate({currencySymbol:t_firstcurrencyId,userId:ObjectId(userId)},{"$inc": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
          
        }); 
      }, 
      data2: function(cb){ 
        
        var updatedata = {"status": '3' }
        tradeTable.findOneAndUpdate({_id: ObjectId(tradeid)},{"$set":updatedata},{new:true,"fields": {balance:1} },function(upErr,upRes){ 
            if(upRes) { 
              //res.json({status:true,message:"Your Order cancelled successfully.",notify_show:'yes'}); 
              // getrateandorders(pair,userid); 
              // open_ordersdata(pair,userid);
            } 
            else { 
              //res.json({status:false,message:"Due to some error occurred,While Order cancelling"}); 
            }  
        });   
      }     
      },function(err, results){ 

      }); 
    }
    else  {
      console.log({status:false,message:"Your Order already cancelled"});
    }
  });
}
router.post('/cancelTrade', (req, res) => {

  var tradeid = req.body.id;
  var userid  = req.body.userid;
  update      = { status : '3'}
  tradeTable.aggregate([
    {$match:{'_id' : ObjectId(tradeid),'status':{ $ne: '3' }}},
  ]).exec((tradeerr,tradedata) => { 
    if(tradedata.length > 0) {
      var type                =  tradedata[0].buyorsell;
      var trade_ids           =  tradedata[0]._id;
      var userId              =  tradedata[0].userId;
      var filledAmt           =  tradedata[0].filledAmount;
      var status              =  tradedata[0].status;
      var quantity            =  tradedata[0].quantity;
      var price               =  tradedata[0].price;
      var t_firstcurrencyId   =  tradedata[0].firstCurrency;
      var t_secondcurrencyId  =  tradedata[0].secondCurrency;
      var leverage            =  tradedata[0].leverage;

      quantity = parseFloat(quantity) - parseFloat(filledAmt);

      var leverage1       = parseFloat(100/leverage);

      var initial_margin = ((parseFloat(quantity)/parseFloat(price))*leverage1)/100;
      var feetoopen      = (quantity/price)*0.075/100;
      var Bankruptcy     = price * leverage /(parseFloat(leverage) + 1);
      var feetoclose     = (quantity/Bankruptcy) * 0.075/100;
      var order_value    = parseFloat(quantity/price).toFixed(8);
      var order_cost     = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);


  
      async.parallel({
      // update balance 
      data1: function(cb){
      var updatebaldata = {};
        updatebaldata["balance"] = order_cost; 
        console.log(order_cost,'order cost');
        Assets.findOneAndUpdate({currencySymbol:t_firstcurrencyId,userId:ObjectId(userId)},{"$inc": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
          
        }); 
      }, 
      data2: function(cb){ 
        
        var updatedata = {"status": '3' }
        tradeTable.findOneAndUpdate({_id: ObjectId(tradeid)},{"$set":updatedata},{new:true,"fields": {balance:1} },function(upErr,upRes){ 
            if(upRes) { 
              res.json({status:true,message:"Your Order cancelled successfully.",notify_show:'yes'}); 
              // getrateandorders(pair,userid); 
              // open_ordersdata(pair,userid);
            } 
            else { 
              res.json({status:false,message:"Due to some error occurred,While Order cancelling"}); 
            }  
        });   
      }     
      },function(err, results){ 

      }); 
    }
    else  {
      res.json({status:false,message:"Your Order already cancelled"});
    }
  });

});

router.post('/getPricevalue', (req, res) => {
  var result = {};
  async.parallel({
      lastpricedet : function(cb) {
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
          {
            $match:
            {
              status:'1',
              firstCurrency:req.body.firstCurrency,
              secondCurrency:req.body.secondCurrency,
              //"createdAt": { $gt: new Date(Date.now() - 24*60*60 * 1000) }
            }
          },
          {$sort:sort},
          {$limit: 1},
        ]).allowDiskUse(true).exec(cb)
      },
      pricedet : function(cb) {
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
           {
            $match:
            {
              status:'1',
            }
          },
          {$unwind:"$filled"},
          {
            $match:
            {
              'filled.pairname': req.body.firstCurrency+req.body.secondCurrency
            }
          },
          {
            $group:
            {
              _id: "$item",
              low: { $min: "$filled.Price" },
              high: { $max: "$filled.Price" },
              volume :{ $sum: "$filled.order_cost"}
            }
          }
          
        ]).allowDiskUse(true).exec(cb)
      },
      change : function(cb) {
    tradeTable.aggregate([
      {$unwind:"$filled"},

      {$match : { "filled.created_at": {
        $gte: new Date(new Date().setDate(new Date().getDate()-1)),
        $lte: new Date()
      },'filled.pairname': req.body.firstCurrency+req.body.secondCurrency}},
      {
        $sort: {
          "filled.created_at": 1,
        }
      },
      {
        $group: {
          _id: null,
          pairname: {$first:"$filled.pair"},
          lastRate: { $last: '$filled.Price'},
          oldRate: { $first: '$filled.Price'},
        }
      },
      {
        $project: {
          _id: 1,
          pairname: 1,
          oldRate : {$cond: [ { $eq: [ "$oldRate", null ] }, 0, '$oldRate' ]},
          lastRate : {$cond: [ { $eq: [ "$lastRate", null ] }, 0, '$lastRate' ]},
          change  : { $multiply: [{ $subtract: [ 1, { $divide: [ {$cond: [ { $eq: [ "$oldRate", null ] }, 0, '$oldRate' ]}, {$cond: [ { $eq: [ "$lastRate", null ] }, 0, '$lastRate' ]} ] } ] }, 100]}
        }
      },
    ]).allowDiskUse(true).exec(cb)
   },
    },(err,results) => {
  
        if(err){
            result.status       = false;
            result.message      = 'Error occured.';
            result.err          = err;
            result.notify_show  = 'no';
            res.json(result);
        } else if(results){
            result.status       = true;
            result.message      = 'tradeTableAll';
            result.pricedet     = results.pricedet;
            result.lastpricedet = results.lastpricedet;
            result.change       = results.change;
            result.notify_show  = 'no';
            res.json(result);
        } else {
            result.status       = false;
            result.message      = 'Error occured.';
            result.err          = '';
            result.notify_show  = 'no';
            res.json(result);
        }
    });
});


router.post('/getuserTradeData', (req, res) => {
   var userId = req.body.userid;
   var status = req.body.status;
    var result = {};
    // tradeTable.find(findObj,function(err,tradeTableAll){
    async.parallel({
      orderHistory : function(cb) {
        var sort = {'_id':-1};
        tradeTable.aggregate([
          {$match:{'$or' : [{"status" : '0'},{"status" : '2'}],firstCurrency:req.body.firstCurrency,secondCurrency:req.body.secondCurrency,userId:ObjectId(userId)}},
          {$sort:sort},
          {$limit: 10},
        ]).allowDiskUse(true).exec(cb)
      },
      Histroydetails : function(cb) {
        tradeTable.find({userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      Filleddetails : function(cb) {
        tradeTable.find({status:1,userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      Conditional_details : function(cb) {
        tradeTable.find({status:'4',stopstatus:{$ne:'1'},userId:ObjectId(userId)}).sort({'_id':-1}).exec(cb)
      },
      position_details : function(cb) {
        var pair = req.body.firstCurrency+req.body.secondCurrency;
          tradeTable.aggregate([
          { "$match": { status:'1',userId:ObjectId(userId),"pairName": pair } },
          {$unwind:"$filled"},
          { "$match": { "filled.position_status":'1'} },
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
          ]).exec(cb)
      },
      daily_details : function(cb) {
        var pair = req.body.firstCurrency+req.body.secondCurrency;
        var start = new Date();
        start.setHours(0,0,0,0);
        // console.log(start,'start');
        var end = new Date();
        end.setHours(23,59,59,999);
        // console.log(end,'start');
        tradeTable.aggregate([
          { "$match": { status:'1',position_status:'1',userId:ObjectId(userId),"pairName": pair } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$match": {"filled.created_at": {$gte: new Date(start), $lt: new Date(end)}} },
          { "$group": { "_id": null,"price" :{ "$avg": "$filled.Price" },"quantity" :{ "$sum": "$filled.filledAmount" },"Fees" :{ "$sum": "$filled.Fees" },"pairName" :{ "$first": "$filled.pairname" },"leverage" :{ "$first": "$leverage" } } }
          ]).exec(cb)
      },
      lastpricedet : function(cb) { 
        var sort = {'orderDate':-1};
        tradeTable.aggregate([
          {
            $match:
            {
              status:'1',
              firstCurrency:req.body.firstCurrency,
              secondCurrency:req.body.secondCurrency
            }
          },
          {$sort:sort},
          {$limit: 1},
        ]).allowDiskUse(true).exec(cb)
      },
      Assetdetails : function(cb) {
         Assets.find({userId:ObjectId(req.body.userid)}).exec(cb)
      },
      contractdetails : function(cb) {
         perpetual.findOne({first_currency:req.body.firstCurrency,second_currency:req.body.secondCurrency},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1}).exec(cb)
      },
      closed_positions : function(cb) {
         position_table.find({userId:ObjectId(userId),pairname:req.body.firstCurrency+req.body.secondCurrency}).exec(cb)
      },
    },(err,results) => {
      // console.log(results.position_details,'position_details');
        if(err){
            result.status      = false;
            result.message     = 'Error occured.';
            result.err         = err;
            result.notify_show = 'no';
            res.json(result);
        } else if(results){
            result.status               = true;
            result.message              = 'tradeTableAll';
            result.buyOrder             = results.buyOrder;
            result.sellOrder            = results.sellOrder;
            result.orderHistory         = results.orderHistory;
            result.Histroydetails       = results.Histroydetails;
            result.Conditional_details  = results.Conditional_details;
            result.Filleddetails        = results.Filleddetails;
            result.lastpricedet         = results.lastpricedet;
            result.assetdetails         = results.Assetdetails;
            result.contractdetails      = results.contractdetails;
            result.position_details     = results.position_details;
            result.closed_positions     = results.closed_positions;
            result.daily_details        = results.daily_details;
            result.notify_show          = 'no';
            res.json(result);
        } else {
            result.status = false;
            result.message = 'Error occured.';
            result.err = '';
            result.notify_show = 'no';
            res.json(result);
        }
    });
});
function order_placing(ordertype,buyorsell,price,quantity,leverage,pairname,userid,trigger_price=0,trigger_type=null,id=0,typeorder='Conditional')
{
    console.log(trigger_type,'trigger_type');
      var leverage       = parseFloat(100/leverage);
      var initial_margin = ((parseFloat(quantity)/parseFloat(price))*leverage)/100;
      var feetoopen      = (quantity/price)*0.075/100;
      var Bankruptcy     = price * leverage /(parseFloat(leverage) + 1);
      var feetoclose     = (quantity/Bankruptcy) * 0.075/100;
      var order_value    = parseFloat(quantity/price).toFixed(8);
      var order_cost     = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);
      order_cost         = parseFloat(order_cost).toFixed(8);
      
       async.parallel({
      position_details : function(cb) {
          tradeTable.aggregate([  
          { "$match": { status:'1',position_status:'1',userId:ObjectId(userid),"pairName": pairname } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"quantity" :{ "$sum": "$filled.filledAmount" }} }
          ]).exec(cb)
      },
    
    },(err,results) => {
      var position_details = (results.position_details.length>0)?results.position_details[0].quantity:0;
          perpetual.findOne({tiker_root:pairname},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){
                var mainmargin = contractdetails.maint_margin/100;
                var balance_check = true;
            if(buyorsell=='buy')
            {
                if(position_details<0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                var Liqprice = price*leverage/((leverage+1)-(mainmargin*leverage));
            }
            else
            {
               if(position_details>0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                quantity = parseFloat(quantity)*-1;
                var Liqprice = price*leverage/((leverage-1)+(mainmargin*leverage));
            }
            if(err){
              res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
            } 
            else 
            {
              Assets.findOne({userId:ObjectId(userid),currencySymbol:contractdetails.first_currency},function(err,assetdetails){
                if(err){
                  res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                } else if(assetdetails){
                  var firstcurrency  = contractdetails.first_currency;
                  var secondcurrency = contractdetails.second_currency;
                  var curbalance     = assetdetails.balance;
                  if(curbalance<order_cost && balance_check==true)
                  {
                    res.json({status:false,message:"Due to insuffient balance order cannot be placed",notify_show:'yes'})
                  } else {

                    var before_reduce_bal = curbalance;
                    var after_reduce_bal = curbalance-(balance_check)?order_cost:0;

                    var updateObj = {balance:after_reduce_bal};

                    // Assets.findByIdAndUpdate(assetdetails._id, updateObj, {new: true}, function(err, changed) {
                    //   if (err) {
                    //     res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                    //   } else if(changed){

                        const newtradeTable = new tradeTable({
                          quantity          : quantity,
                          price             : price,
                          trigger_price     : trigger_price,
                          orderCost         : order_cost,
                          orderValue        : order_value,
                          leverage          : leverage,
                          userId            : userid,
                          pair              : contractdetails._id,
                          pairName          : pairname,
                          beforeBalance     : before_reduce_bal,
                          afterBalance      : after_reduce_bal,
                          firstCurrency     : firstcurrency,
                          secondCurrency    : secondcurrency,
                          Liqprice          : Liqprice,
                          orderType         : ordertype,
                          trigger_type      : trigger_type,
                          stopstatus        : (typeorder!='Conditional')?'1':'0',
                          buyorsell         : buyorsell,
                          pairid            : id,
                          trigger_ordertype : typeorder,
                          status            : (trigger_type!=null)?4:0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                        });
                        newtradeTable
                        .save()
                        .then(curorder => {
                        io.emit('TRADE', 'test');
                          
                          tradematching(curorder);
                        }).catch(err => { console.log(err,'error'); res.json({status:false,message:"Your order not placed.",notify_show:'yes'})});``
                    //   }
                    // })
                    // insert trade tab
                  }
                } else {
                  
                }
              });
            }

          });
    });
    
  
}
router.post('/orderPlacing', (req, res) => {
  if(req.body.price<1 || req.body.quantity <1)
  {
    return res.json({
      status:false,
      message:"Price or Quantity of contract must not be lesser than 1",
      notify_show:'yes'
    });
  }
    const { errors, isValid } = validateTradeInput(req.body);
    if (!isValid) {
      res.json({
        status:false,
        message:"Error occured, please fill all required fields.",
        errors:errors,
        notify_show:'yes'
      });
    } else {
    
      var post_only       = req.body.post_only;
      var reduce_only     = req.body.reduce_only;
      var ordertype       = req.body.ordertype;
      var buyorsell       = req.body.buyorsell;
      var price           = req.body.price;
      var timeinforcetype = req.body.timeinforcetype;
      var trigger_price   = req.body.trigger_price;
      var trigger_type    = req.body.trigger_type;
      var quantity        = req.body.quantity;
      var takeprofitcheck = req.body.takeprofitcheck;
      var stopcheck       = req.body.stopcheck;
      var takeprofit      = req.body.takeprofit;
      var stopprice       = req.body.stopprice;
      var leverage        = parseFloat(100/req.body.leverage);
      var initial_margin  = ((parseFloat(quantity)/parseFloat(price))*leverage)/100;
      var feetoopen       = (quantity/price)*0.075/100;
      var Bankruptcy      = price * req.body.leverage /(parseFloat(req.body.leverage) + 1);
      var feetoclose      = (quantity/Bankruptcy) * 0.075/100;
      var order_value     = parseFloat(quantity/price).toFixed(8);
      var order_cost      = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);
      order_cost          = parseFloat(order_cost).toFixed(8);
      

       async.parallel({
      position_details : function(cb) {
        var pair = req.body.pairname;
          tradeTable.aggregate([
          { "$match": { status:'1',position_status:'1',userId:ObjectId(req.body.userid),"pairName": pair } },
          {$unwind:"$filled"},
          {$project:{"filled":1,leverage:1}},
          { "$group": { "_id": null,"quantity" :{ "$sum": "$filled.filledAmount" }} }
          ]).exec(cb)
      },
    
    },(err,results) => {
      var position_details = (results.position_details.length>0)?results.position_details[0].quantity:0;
          perpetual.findOne({tiker_root:req.body.pairname},{tiker_root:1,maint_margin:1,first_currency:1,second_currency:1},function(err,contractdetails){
                var mainmargin = contractdetails.maint_margin/100;
                var balance_check = true;
            if(req.body.buyorsell=='buy')
            {
                if(position_details<0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                var Liqprice = price*req.body.leverage/((req.body.leverage+1)-(mainmargin*req.body.leverage));
            }
            else
            {
               if(position_details>0 && Math.abs(position_details)>=quantity)
                {
                  var balance_check = false;
                }
                quantity = parseFloat(quantity)*-1;
                var Liqprice = price*req.body.leverage/((req.body.leverage-1)+(mainmargin*req.body.leverage));
            }
            if(err){
              res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
            } 
            else 
            {
              Assets.findOne({userId:ObjectId(req.body.userid),currencySymbol:contractdetails.first_currency},function(err,assetdetails){
                if(err){
                  res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                } else if(assetdetails){
                  var firstcurrency  = contractdetails.first_currency;
                  var secondcurrency = contractdetails.second_currency;
                  var curbalance     = assetdetails.balance;
                  if(curbalance<order_cost && balance_check==true)
                  {
                    res.json({status:false,message:"Due to insuffient balance order cannot be placed",notify_show:'yes'})
                  } else {

                    var before_reduce_bal = curbalance;
                    var after_reduce_bal = curbalance-order_cost;

                    var userid = req.body.userid;

                    var updateObj = {balance:(balance_check)?after_reduce_bal:0};

                    Assets.findByIdAndUpdate(assetdetails._id, updateObj, {new: true}, function(err, changed) {
                      if (err) {
                        res.json({status:false,message:"Error occured.",err:err,notify_show:'yes'});
                      } else if(changed){

                        const newtradeTable = new tradeTable({
                          quantity        : quantity,
                          price           : price,
                          trigger_price   : trigger_price,
                          orderCost       : order_cost,
                          orderValue      : order_value,
                          leverage        : req.body.leverage,
                          userId          : req.body.userid,
                          pair            : contractdetails._id,
                          pairName        : req.body.pairname,
                          postOnly        : post_only,
                          reduceOnly      : reduce_only,
                          beforeBalance   : before_reduce_bal,
                          afterBalance    : after_reduce_bal,
                          timeinforcetype : timeinforcetype,
                          firstCurrency   : firstcurrency,
                          secondCurrency  : secondcurrency,
                          Liqprice        : Liqprice,
                          orderType       : ordertype,
                          trigger_type    : trigger_type,
                          buyorsell       : buyorsell,
                          status          : (trigger_type!=null)?4:0 // //0-pending, 1-completed, 2-partial, 3- Cancel, 4- Conditional
                        });
                        newtradeTable
                        .save()
                        .then(curorder => {
                          io.sockets.in(req.body.userid).emit('TRADE', curorder);
                          res.json({status:true,message:"Your order placed successfully.",notify_show:'yes'});
                          if(takeprofitcheck)
                          {
                              var trigger_price = takeprofit;
                              var tptrigger_type = req.body.tptrigger_type;
                              order_placing(ordertype,buyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,tptrigger_type,curorder._id,'takeprofit');
                          }
                          if(stopcheck)
                          {
                              var stoptrigger_type = req.body.stoptrigger_type;
                              var trigger_price = stopprice;
                              var newbuyorsell = (buyorsell=='buy')?'sell':'buy';
                              order_placing(ordertype,newbuyorsell,price,quantity,leverage,req.body.pairname,req.body.userid,trigger_price,stoptrigger_type,curorder._id,'stop');
                          }
                          tradematching(curorder);
                        }).catch(err => { console.log(err,'error'); res.json({status:false,message:"Your order not placed.",notify_show:'yes'})});``
                      }
                    })
                    // insert trade tab
                  }
                } else {
                  res.json({status:false,message:"Error occured.",err:'no res 2',notify_show:'yes'});
                }
              });
            }

          });
    });
    }
  });
function selldetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,buyprice,maker_rebate)
{
    var testdata = tempdata;
    tradeTable.findOneAndUpdate({_id:ObjectId(buyorderid)},{"$set":{"status":buyUpdate.status,"filled":tempdata},"$inc":{"filledAmount" : parseFloat(buyUpdate.filledAmt)}},{new:true,"fields": {status:1,filled:1}},function(buytemp_err,buytempData1){
        console.log(buytemp_err,'here');
        console.log(buytempData1,'herefsdfsf');
         
          // positionmatching(tempdata);
        //Position updation process
        //Conditional order trigger process
          tradeTable.find({status:'4',trigger_type:'Last'},function(buytemp_err,buytempData){
              if(buytempData.length)
              {
                  for (var i=0; i < buytempData.length; i++) {
                      var price         = buytempData[i].price;
                      var trigger_price = buytempData[i].trigger_price;
                      var userId        = buytempData[i].userId;
                      var pairName      = buytempData[i].pairName;
                      var leverage      = buytempData[i].leverage;
                      var quantity      = buytempData[i].quantity;
                      var buyorsell     = buytempData[i].buyorsell;
                      var orderType     = buytempData[i].orderType;
                      var different     = parseFloat(price)-parseFloat(trigger_price);
                      if(different>0)
                      {
                          if(trigger_price>buyprice)
                          {
                              // order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
                                tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                                    console.log(buytemp_err,'trigger error');
                                });
                          }
                      }
                      else
                      {
                          if(trigger_price<buyprice)
                          {
                              //order_placing(orderType,buyorsell,price,quantity,leverage,pairName,userId);
                               tradeTable.findOneAndUpdate({_id:ObjectId(_id),status:'4',stopstatus:{$ne:'1'} },{ "$set": {"status":'0'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                               
                                    console.log(buytemp_err,'trigger error');
                                });
                          }
                      }
                  }
              }
          });

          tradeTable.findOneAndUpdate({pairid:(buyorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                  if(buytempData)
                  {
                        
                  }
              });

    });
    var fee_amount        = parseFloat(sellUpdate.filledAmt/buyprice)*parseFloat(maker_rebate)/100;
    testdata.Type         = "sell";
    testdata.user_id      = ObjectId(selluserid);
    testdata.Fees         = parseFloat(fee_amount).toFixed(8);
    testdata.filledAmount = +(sellUpdate.filledAmt).toFixed(8)*-1;
    console.log(sellUpdate.filledAmt * -1,'sellamount');
    tradeTable.findOneAndUpdate({_id:ObjectId(sellorderid)},{"$set":{"status":sellUpdate.status,"filled":testdata},"$inc":{"filledAmount" : parseFloat(sellUpdate.filledAmt * -1)}},{new:true,"fields": {status:1} },function(buytemp_err,selltempData){
            if(selltempData)
            {
              console.log(buytemp_err,'sellhere');
              console.log(selltempData,'sellherefsdfsf');
              // positionmatching(testdata);
              tradeTable.findOneAndUpdate({pairid:(sellorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                  if(buytempData)
                  {
                        
                  }
              });
            }
    });
}
function buydetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,buyprice,maker_rebate)
{
    tradeTable.findOneAndUpdate({_id:ObjectId(buyorderid)},{ "$set": {"filled":tempdata,"status":buyUpdate.status},"$inc":{"filledAmount":buyUpdate.filledAmt}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
          if(buytempData)
          {
              console.log(buytemp_err,'buy');
              console.log(buytempData,'buy');
              tradeTable.findOneAndUpdate({pairid:(buyorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                  if(buytempData)
                  {
                        
                  }
              });
              // positionmatching(tempdata);
          }
          console.log("tmp updated");
    });
    var fee_amount = parseFloat(sellUpdate.filledAmt/buyprice)*parseFloat(maker_rebate)/100;
    tempdata.Type="sell";
    tempdata.user_id=ObjectId(selluserid);
    tempdata.filledAmount=(sellUpdate.filledAmt) * -1;
    tempdata.Fees=parseFloat(fee_amount).toFixed(8);

    tradeTable.findOneAndUpdate({_id:ObjectId(sellorderid)},{ "$set": {"filled":tempdata,"status":sellUpdate.status},"$inc":{"filledAmount":parseFloat(sellUpdate.filledAmt)*-1}},{new:true,"fields": {status:1} },function(selltemp_err,selltempData){
          if(selltempData)
          {
              console.log(selltemp_err,'sell');
              console.log(selltempData,'sell');
              // positionmatching(tempdata);
              tradeTable.findOneAndUpdate({pairid:(sellorderid),status:'4',stopstatus:'1'},{ "$set": {"stopstatus":'2'}},{new:true,"fields": {status:1} },function(buytemp_err,buytempData){
                  if(buytempData)
                  {
                        
                  }
              });
          }
    });
}
async function buymatchingprocess(curorder,tradedata,pairData)
{
    console.log("buy");
    var buyOrder  = curorder,
    buyAmt        = rounds(buyOrder.quantity),
    buyorderid    = buyOrder._id,
    buyuserid     = buyOrder.userId,
    forceBreak    = false,
    buyeramount   = rounds(buyOrder.quantity),
    buyeramount1  = rounds(buyOrder.quantity),
    buytempamount = 0;
    buy_res_len   = tradedata.length;

    // tradedata.forEach(function(data_loop){
        for (var i = 0; i < tradedata.length; i++) {
        var data_loop = tradedata[i];
        buyAmt = rounds(buyOrder.quantity-buytempamount);
        if(buyAmt == 0 || forceBreak == true){
          console.log("break");
          return;
        }
        else{
            var ii = i,
            sellOrder   = data_loop,
            sellorderid = sellOrder._id,
            selluserid  = sellOrder.userId,
            sellAmt     = rounds(+sellOrder.quantity - +sellOrder.filledAmount),
            silentBreak = false,
            buyUpdate   = {},
            sellUpdate  = {},
            buyerBal    = 0,
            sellerBal   = 0,
            orderSocket = {}
            buyeramount = buyeramount-sellAmt;
            console.log(buyAmt,"buyAmt");
            console.log(Math.abs(sellAmt),"sellAmt");
            if (buyAmt == Math.abs(sellAmt)) {
                console.log("amount eq");
                buyUpdate = {
                  status: '1',
                  filledAmt: Math.abs(sellAmt)
                }
                sellUpdate =  {
                  status: '1',
                  filledAmt: buyAmt
                }
                forceBreak = true
            } else if (buyAmt > Math.abs(sellAmt)) {
                console.log("else buy gt");
                buyUpdate = {
                  status: '2',
                  filledAmt: Math.abs(sellAmt)
                }
                sellUpdate = {
                  status: '1',
                  filledAmt: Math.abs(sellAmt)
                }
                buyAmt = rounds(+buyAmt - +sellAmt)
            } else if (buyAmt < Math.abs(sellAmt)) {
                console.log("else sell gt");
                buyUpdate = {
                  status: '1',
                  filledAmt: buyAmt     
                }
                sellUpdate = {
                  status: '2',
                  filledAmt: buyAmt
                }
                forceBreak = true
            } else {
                silentBreak = true
            }

            if (silentBreak == false) {
                console.log("si brk");
                var taker_fee = pairData.taker_fees;
                var fee_amount = parseFloat(buyUpdate.filledAmt/buyOrder.price)*parseFloat(pairData.taker_fees)/100;
                var tempdata = {
                    "pair"         : ObjectId(pairData._id),
                    "firstCurrency": pairData.first_currency,
                    "buyuserId"    : ObjectId(buyuserid),
                    "user_id"      : ObjectId(buyuserid),
                    "selluserId"   : ObjectId(selluserid),
                    "sellId"       : ObjectId(sellorderid),
                    "buyId"        : ObjectId(buyorderid),
                    "filledAmount" : +(buyUpdate.filledAmt).toFixed(8),
                    "Price"        : +buyOrder.price,
                    "pairname"     : curorder.pairName,
                    "order_cost"   : (+(buyUpdate.filledAmt)/+buyOrder.price).toFixed(8),
                    "Fees"         : parseFloat(fee_amount).toFixed(8),
                    "status"       : "filled",
                    "Type"         : "buy",
                    "created_at"   : new Date(),
                }
                buytempamount += +buyUpdate.filledAmt
                
                await buydetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,buyOrder.price,pairData.maker_rebate);
                if(tradedata.length==i && forceBreak!=true && curorder.timeinforcetype=='ImmediateOrCancel')
                {
                    cancel_trade(curorder._id,curorder.userId);
                }
                // positionmatching(data_loop);
                if(forceBreak == true) {
                    console.log('forceBreak')
                    return true;
                }
            }
        }
    }
    
}

async function sellmatchingprocess(curorder,tradedata,pairData)
{
  var sellOrder = curorder,
  sellorderid    = sellOrder._id,
  selluserid     = sellOrder.userId,
  sellAmt        = rounds(Math.abs(sellOrder.quantity)),
  forceBreak     = false,
  selleramount   = rounds(sellOrder.quantity)
  selleramount1  = rounds(sellOrder.quantity)
  selltempamount = 0;
  sell_res_len   = tradedata.length;
  for (var i = 0; i < tradedata.length; i++) {
    var data_loop = tradedata[i];
    sellAmt        = rounds(Math.abs(sellOrder.quantity)-selltempamount);
    console.log('loop starting',i);
    if(sellAmt == 0 || forceBreak == true)
    return

    var ii = i,
    buyOrder     = data_loop,
    buyorderid   = buyOrder._id,
    buyuserid    = buyOrder.userId,
    buyAmt       = rounds(buyOrder.quantity - buyOrder.filledAmount),
    silentBreak  = false,
    buyUpdate    = {},
    sellUpdate   = {},
    buyerBal     = 0,
    sellerBal    = 0,
    orderSocket  = {}
    selleramount = selleramount-buyAmt;

    console.log(Math.abs(sellAmt),'sellamount');
    console.log(buyAmt,'buyamount');
    if(Math.abs(sellAmt) == buyAmt) {
        buyUpdate = {
          status    : 1,
          filledAmt : Math.abs(sellAmt)
        }
        sellUpdate = {
          status    : 1,
          filledAmt : buyAmt
        }
        forceBreak = true
    } else if(Math.abs(sellAmt) > buyAmt) {
        buyUpdate = {
          status    : 1,
          filledAmt : buyAmt
        }
        sellUpdate = {
          status    : 2,
          filledAmt : buyAmt
        }
        sellAmt = rounds(+sellAmt - +buyAmt)  
    } else if(Math.abs(sellAmt) < buyAmt) {
        buyUpdate = {
          status    : 2,
          filledAmt : Math.abs(sellAmt)
        }
        sellUpdate = {
          status    : 1,
          filledAmt : Math.abs(sellAmt)
        }
        forceBreak = true
    } else {
      silentBreak = true
    }
    var returnbalance = 0;
    if(+buyOrder.price > +sellOrder.price)
    {
      var return_price = +buyOrder.price - +sellOrder.price;
      returnbalance    = +buyUpdate.filledAmt* +return_price;
      returnbalance    = parseFloat(returnbalance).toFixed(8);
    }

    if(silentBreak == false) {
      var taker_fee = pairData.taker_fees;
      var fee_amount = parseFloat(buyUpdate.filledAmt/buyOrder.price)*parseFloat(pairData.taker_fees)/100;
        var tempdata = {
        "pair"         : ObjectId(pairData._id),
        "firstCurrency": pairData.first_currency,
        "buyuserId"    : ObjectId(buyuserid),
        "user_id"      : ObjectId(buyuserid),
        "selluserId"   : ObjectId(selluserid),
        "sellId"       : ObjectId(sellorderid),
        "buyId"        : ObjectId(buyorderid),
        "filledAmount" : +(buyUpdate.filledAmt).toFixed(8),
        "Price"        : +sellOrder.price,
        "pairname"     : curorder.pairName,
        "order_cost"   : (+(buyUpdate.filledAmt)/+sellOrder.price).toFixed(8),
        "status"       : "filled",
        "Type"         : "buy",
         "Fees"        : parseFloat(fee_amount).toFixed(8),
        "created_at"   : new Date()
        }
        selltempamount += +sellUpdate.filledAmt;
        console.log(tempdata,'before sell update');
        await selldetailsupdate(tempdata,buyorderid,buyUpdate,sellorderid,sellUpdate,selluserid,sellOrder.price,pairData.maker_rebate);
        if(tradedata.length==i && forceBreak!=true && curorder.timeinforcetype=='ImmediateOrCancel')
        {
            cancel_trade(curorder._id,curorder.userId);
        }
        if(forceBreak == true) {
            console.log("true");
            // getrateandorders(curorder.pair,userid);
            return true
        }
    }
    if(forceBreak == true) {
        // getrateandorders(curorder.pair,userid);
        return true
    }
  }
}
function tradematching(curorder)
{
  //Fill or kill order type
  if(curorder.timeinforcetype=="FillOrKill")
  {
    console.log('filleorkill');
      var datas = {
        '$or' : [{"status" : '2'},{"status" : '0'}],
        'userId' : { $ne : ObjectId(curorder.userId) },
        'pairName': (curorder.pairName)
      },sort;

      if(curorder.buyorsell == 'buy') {
        datas['buyorsell'] = 'sell'
        datas['price'] = { $lte : curorder.price }
        sort = { "price" : 1 }
      } else {
        datas['buyorsell'] = 'buy'
        datas['price'] = { $gte : curorder.price }
        sort = { "price" : -1 }
      }
        tradeTable.aggregate([
            {$match:datas},
            {
                $group : {
                    "_id"          : null,
                    'quantity'     : { $sum : '$quantity' },
                    'filledAmount' : { $sum : '$filledAmount' }
                }
            },
            {$sort:sort},
            {$limit: 10},
        ]).exec((tradeerr,tradedata) => {
          if(tradedata.length>0)
          {
              var quantity      = tradedata[0].quantity;
              var filledAmount  = tradedata[0].filledAmount;
              var pendingamount = parseFloat(quantity) - parseFloat(filledAmount);
              if(curorder.quantity>pendingamount)
              {
                  cancel_trade(curorder._id,curorder.userId);
              }
          }
          else
          {
            cancel_trade(curorder._id,curorder.userId);
          }
        });
  }
  var datas = {
      '$or' : [{"status" : '2'},{"status" : '0'}],
      'userId' : { $ne : ObjectId(curorder.userId) },
      'pairName': (curorder.pairName)
    },sort;

    if(curorder.buyorsell == 'buy') {
      datas['buyorsell'] = 'sell'
      datas['price'] = { $lte : curorder.price }
      sort = { "price" : 1 }
    } else {
      datas['buyorsell'] = 'buy'
      datas['price'] = { $gte : curorder.price }
      sort = { "price" : -1 }
    }
      // console.log(datas,'datas');
    tradeTable.aggregate([
      {$match:datas},
      {$sort:sort},
    ]).exec((tradeerr,tradedata) => {
    perpetual.findOne({_id:ObjectId(curorder.pair)}).exec(function(pairerr,pairData){
        console.log('perpetual');
        if(tradeerr)
            console.log({status:false,message:tradeerr});
        else
            if(tradedata.length>0){
                if(curorder.postOnly)
                {
                  cancel_trade(curorder._id,curorder.userId);
                }
                var i=0;
                if (curorder.buyorsell == 'buy') {
                   buymatchingprocess(curorder,tradedata,pairData);
                } else if (curorder.buyorsell == 'sell') {
                    sellmatchingprocess(curorder,tradedata,pairData);
                }
            }
            else
            {
              if(curorder.timeinforcetype=="ImmediateOrCancel")
              {
                  cancel_trade(curorder._id,curorder.userId);
              }
            }
            // positionmatching(curorder);
    });
  });
}
function positionmatching(curorder)
{
  console.log('positionmatching',curorder);
    var datas = {
        'filled.user_id'         : ObjectId(curorder.user_id) ,
        'filled.pairname'        : (curorder.pairname),
        "filled.position_status" : '1',
        "filled.status"          : 'filled'
    },sort;
    if(curorder.Type=='buy')
    {
      datas['filled.Type'] = 'sell';
    }
    else
    {
      datas['filled.Type'] = 'buy';
    }
    tradeTable.aggregate([
        {$unwind:"$filled"},
        {$project:{"filled":1}},
        {$match:datas},
    ]).exec((tradeerr,tradedata) => {
      
        if(tradedata.length>0)
        {
          var buytemp =0;
          var selltemp =0;
            for(var i=0;i<tradedata.length;i++)
            {
                if(curorder.Type=='buy')
                {
                  console.log(curorder.filledAmount,'curqauntity');
                  var sellorderid   = tradedata[i].filled.sellId;
                  var firstCurrency = curorder.firstCurrency;
                  var buyorderid    = curorder.buyId;
                  var buyFees       = curorder.Fees;
                  var sellFees      = tradedata[i].filled.Fees;
                  var buypairName   = curorder.pairname;
                  var buyuserId     = curorder.buyuserId;
                  var buypair       = curorder.pair;
                  var sellquantity  = Math.abs(tradedata[i].filled.filledAmount);
                  var buyqauntity   = parseFloat(curorder.filledAmount)-buytemp;
                  var buyprice      = curorder.Price;
                  var sellprice     = tradedata[i].filled.Price;
                  var buyupdate     = {};
                  var sellupdate    = {};
                  var forceBreak    = false;
                  if(forceBreak == true) {
                      break;
                  }
                  console.log(buyqauntity,'buyqauntity');
                  console.log(sellquantity,'sellquantity');
                  if(sellquantity==buyqauntity)
                  {
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }
                  else if(sellquantity<buyqauntity)
                  {
                    buyupdate['position_status']  = 2;
                    buyupdate['positionFilled']   = sellquantity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = sellquantity;
                  }
                  else{
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 2;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }

                  buytemp += buyupdate.positionFilled;
                  tradeTable.findOneAndUpdate({'filled.buyId':ObjectId(buyorderid)},{ "$set": {"filled.$.position_status":buyupdate.position_status}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
                    console.log(selltemp_err,'sell error');
                        if(selltempData)
                        {
                            var totalfees   = parseFloat(buyFees)+parseFloat(sellFees);
                            var profitnloss = [ (1/parseFloat(sellprice))  - (1/parseFloat(buyprice)) ] * parseFloat(buyupdate.positionFilled);
                            profitnloss     = parseFloat(profitnloss)-parseFloat(totalfees);

                            const newposition = new position_table({
                            "pairname"          : buypairName,
                            "pair"              : buypair,
                            "userId"            : buyuserId,
                            "closing_direction" : "Closed short",
                            "quantity"          : buyupdate.positionFilled,
                            "exit_price"        : buyprice,
                            "entry_price"       : sellprice,
                            "profitnloss"       : profitnloss,
                            });
                            newposition.save(function(err,data) {
                              console.log("success");
                              var updatebaldata = {};
                             
                             console.log(profitnloss,'profitnloss1');
                              Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(buyuserId)},{"$inc": {"balance":10} } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
                                console.log(balerr,'balance error');
                                console.log(baldata,'baldata');
                              }); 
                              
                            });


                            console.log(selltemp_err,'buy');
                            console.log(selltempData,'buy');
                        }
                  });

                  tradeTable.findOneAndUpdate({'filled.sellId':ObjectId(sellorderid)},{ "$set": {"filled.$.position_status":sellupdate.position_status}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
                        if(selltempData)
                        {
                            console.log(selltemp_err,'buy');
                            console.log(selltempData,'buy');
                        }
                  });

                }
                else
                {
                  var buyorderid    = tradedata[i].filled.buyId;
                  var buyprice      = tradedata[i].filled.Price;
                  var sellorderid   = curorder.sellId;
                  var firstCurrency = curorder.firstCurrency;
                  var selluserId    = curorder.userId;
                  var sellpair      = curorder.pair;
                  var sellprice     = curorder.Price;
                  var buyFees       = curorder.Fees;
                  var sellFees      = tradedata[i].filled.Fees;
                  var sellpairName  = curorder.pairname;
                  var buyqauntity   = Math.abs(tradedata[i].filled.filledAmount);
                  var sellquantity  = Math.abs(curorder.filledAmount)-selltemp;
                  var buyupdate     = {};
                  var sellupdate    = {};
                  var forceBreak    = false;
                  if(forceBreak == true) {
                      break;
                  }
                  console.log(buyqauntity,'buyqauntity1');
                  console.log(sellquantity,'sellquantity1');
                  if(sellquantity==buyqauntity)
                  {
                    console.log('equal');
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }
                  else if(sellquantity<buyqauntity)
                  {
                    buyupdate['position_status']  = 2;
                    buyupdate['positionFilled']   = sellquantity;

                    sellupdate['position_status'] = 0;
                    sellupdate['positionFilled']  = sellquantity;
                  }
                  else{
                    buyupdate['position_status']  = 0;
                    buyupdate['positionFilled']   = buyqauntity;

                    sellupdate['position_status'] = 2;
                    sellupdate['positionFilled']  = buyqauntity;
                    forceBreak                    = true
                  }
                   console.log('after else');
                  buytemp += buyupdate.positionFilled;
                  tradeTable.findOneAndUpdate({'filled.buyId':ObjectId(buyorderid)},{ "$set": {"filled.$.position_status":buyupdate.position_status}},{new:true,"fields": {filled:1} },function(selltemp_err,selltempData){
                    console.log(selltemp_err,'selltemp_err');
                        if(selltempData)
                        {
                            var totalfees   = parseFloat(buyFees)+parseFloat(sellFees);
                            var profitnloss = [ (1/parseFloat(buyprice))  - (1/parseFloat(sellprice)) ] * parseFloat(buyupdate.positionFilled);
                            profitnloss     = parseFloat(profitnloss)-parseFloat(totalfees);

                            const newposition = new position_table({
                            "pairname"          : sellpairName,
                            "pair"              : sellpair,
                            "userId"            : selluserId,
                            "closing_direction" : "Closed long",
                            "quantity"          : buyupdate.positionFilled,
                            "exit_price"        : sellprice,
                            "entry_price"       : buyprice,
                            "profitnloss"       : profitnloss,
                            });
                            newposition.save(function(err,data) {
                            console.log(err);
                            console.log('success');
                            console.log(profitnloss,'profitnloss');    
                                Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(buyuserId)},{"$inc": {"balance":10} } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
                                }); 
                               
                            });
                            console.log(selltemp_err,'sell');
                            console.log(selltempData,'sell');
                        }
                        
                  });

                  tradeTable.findOneAndUpdate({'filled.sellId':ObjectId(sellorderid)},{ "$set": {"filled.$.position_status":sellupdate.position_status}},{new:true,"fields": {position_status:1} },function(selltemp_err,selltempData){
                        if(selltempData)
                        {
                            console.log(selltemp_err,'sell');
                            console.log(selltempData,'sell');
                        }
                  });

                }
            }
        }
    });
}

function rounds(n) {
  var roundValue = (+n).toFixed(8)
  return parseFloat(roundValue)
}

router.post('/user-activate', (req, res) => {
    var userid = req.body.userid;
    var updateObj = {active:"Activated"}
    User.findByIdAndUpdate(userid, updateObj, {new: true}, function(err, user) {
      if (user) {
          return res.status(200).json({ message: 'Your Account activated successfully' });
      }
    })
});


router.post('/order-history/', (req, res) => {   
   tradeTable.find({}).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result});
        } 
    });
});

router.post('/trade-history/', (req, res) => {   
    tradeTable.find({status:1}).then(result => {
        if (result) {
            return res.status(200).json({status:true,data:result});
        } 
    });
});





// cron.schedule('* * * * *', () => {
//   forced_liquidation();
//   indexprice_calculation('BTCUSD');
//   // fundingAmount();
// });

// cron.schedule('0 */8 * * *', () => {
//   fundingAmount();
// });

function forced_liquidation()
{
  var lastprice = 7124;
     tradeTable.find({Liqprice:{$gt:lastprice-1,$lte:lastprice},buyorsell:'buy','$or' : [{"status" : '0'},{"status" : '2'}]}).then(result => {
        if(result) {
            console.log(result);
            if(result.length>0)
            {
                for(var i=0;i<result.length;i++)
                {
                  var quantity  = parseFloat(result[i].quantity) - parseFloat(result[i].filledAmount);
                  var leverage  = result[i].leverage;
                  var pairname  = result[i].pairName;
                  var userId    = result[i].userId;
                  var orderType = "Market";
                  order_placing(orderType,'sell',lastprice,quantity,leverage,pairname,userId);
                }
            }
        } 
    });

    tradeTable.find({Liqprice:{$lt:lastprice+1,$gte:lastprice},buyorsell:'sell',status:{'$or' : [{"status" : '0'},{"status" : '2'}]}}).then(result => {
        if(result) {
            console.log(result);
            if(result.length>0)
            {
                for(var i=0;i<result.length;i++)
                {
                  var quantity  = parseFloat(result[i].quantity) - parseFloat(result[i].filledAmount);
                  var leverage  = result[i].leverage;
                  var pairname  = result[i].pairName;
                  var userId    = result[i].userId;
                  var orderType = "Market";
                  order_placing(orderType,'buy',lastprice,quantity,leverage,pairname,userId);
                }
            }
        } 
    });
}

router.get('/indexprice_calculation/', (req, res) => { 
  // funding_rate();
  // forced_liquidation();
  indexprice_calculation('BTCUSD');
  // fundingAmount();
});
function getimpactbidprice(type,pair)
{
  console.log(pair,'fsdfsdfsdfsdfsdfsdf')
  return tradeTable.aggregate([
    // { "$match": { "orderDate": { $gt:new Date(Date.now() - 8*60*60 * 1000) },"status":'1',"buyorsell":type,"pairname": pair} },
    { "$match": { "status":'1',"buyorsell":type,"pairname": pair } },
    { "$group": { "_id": null,"price" :{ "$avg": "$price" } } }
]);
}

function getavgtradevolume(type,pair)
{
  return exchangePrices.aggregate([
    { "$match": { "createdAt":{
        $gte: (new Date((new Date()).getTime() - (10 * 24 * 60 * 60 * 1000)))
    },"pairname":pair,"exchangename":type } },
    // { "$match": { "status":'1',"buyorsell":type } },
    { "$group": { "_id": null,"volume" :{ "$avg": "$volume" } } }
]);
}

async function indexprice_calculation(pair)
{
    var exc_A_avgtradevolumefor_month = await getavgtradevolume('Bitstamp',pair);
    var exc_B_avgtradevolumefor_month = await getavgtradevolume('Kraken',pair);
    var exc_C_avgtradevolumefor_month = await getavgtradevolume('Coinbasepro',pair);


    exc_A_avgtradevolumefor_month = (typeof exc_A_avgtradevolumefor_month[0].volume != 'undefined')?exc_A_avgtradevolumefor_month[0].volume:0;
    exc_B_avgtradevolumefor_month = (typeof exc_B_avgtradevolumefor_month[0].volume != 'undefined')?exc_B_avgtradevolumefor_month[0].volume:0;
    exc_C_avgtradevolumefor_month = (typeof exc_C_avgtradevolumefor_month[0].volume != 'undefined')?exc_C_avgtradevolumefor_month[0].volume:0;

    console.log(exc_A_avgtradevolumefor_month);
    console.log(exc_B_avgtradevolumefor_month);
    console.log(exc_C_avgtradevolumefor_month);
    // return false;
    
    // var exc_A_avgtradevolumefor_month =  2722988075;
    // var exc_B_avgtradevolumefor_month =  6031867375;
    // var exc_C_avgtradevolumefor_month =  3951095106;

    Totalexcvol = exc_A_avgtradevolumefor_month + exc_B_avgtradevolumefor_month + exc_C_avgtradevolumefor_month;
    //Weightage calculation

    var Wt_A = parseFloat(exc_A_avgtradevolumefor_month) / parseFloat(Totalexcvol);
    var Wt_B = parseFloat(exc_A_avgtradevolumefor_month) / parseFloat(Totalexcvol);
    var Wt_C = parseFloat(exc_A_avgtradevolumefor_month) / parseFloat(Totalexcvol);

    var exchange_A_ticker = await make_api_call(pair,'Bitstamp');
    var exchange_B_ticker = await make_api_call(pair,'Kraken');
    var exchange_C_ticker = await make_api_call(pair,'Coinbasepro');
    // console.log(exchange_B_ticker,'ticker details');
    var exchange_A_spotprice = exchange_A_ticker?exchange_A_ticker.last:0;
    if(pair=='BTCUSD')
    {
      var exchange_B_spotprice = exchange_B_ticker?exchange_B_ticker.result.XXBTZUSD.c[0]:0;
    }
    else
    {
      var exchange_B_spotprice = exchange_B_ticker?exchange_B_ticker.result.XETHZUSD.c[0]:0; 
    }
    var exchange_C_spotprice = exchange_C_ticker?exchange_C_ticker.price:0;

    console.log(exchange_A_spotprice,'A price');
    console.log(exchange_B_spotprice,'B price');
    console.log(exchange_C_spotprice,'C price');

    var total = Wt_A+Wt_B+Wt_C;

    var awei = (Wt_A/total)*100;  
    var bwei = (Wt_B/total)*100;
    var cwei = (Wt_C/total)*100;
    

    var EBTC  =  parseFloat(awei/100) * parseFloat(exchange_A_spotprice) + parseFloat(bwei/100) * parseFloat(exchange_B_spotprice) + parseFloat(cwei/100) * parseFloat(exchange_C_spotprice);

    var pricespreadofA = Math.abs(parseFloat(exchange_A_spotprice) - parseFloat(EBTC));
    var pricespreadofB = Math.abs(parseFloat(exchange_B_spotprice) - parseFloat(EBTC));
    var pricespreadofC = Math.abs(parseFloat(exchange_C_spotprice) - parseFloat(EBTC));


    var asquare = 1/(parseFloat(pricespreadofA) * parseFloat(pricespreadofA));
    var bsquare = 1/(parseFloat(pricespreadofB) * parseFloat(pricespreadofB));
    var csquare = 1/(parseFloat(pricespreadofC) * parseFloat(pricespreadofC));

    var totalsquar = parseFloat(asquare) + parseFloat(bsquare) + parseFloat(csquare);

    var weight_A   =   parseFloat(asquare) / parseFloat(totalsquar);
    var weight_B   =   parseFloat(bsquare) / parseFloat(totalsquar);
    var weight_C   =   parseFloat(csquare) / parseFloat(totalsquar);


    var index_price  =  (parseFloat(weight_A) * parseFloat(exchange_A_spotprice)) + (parseFloat(weight_B) * parseFloat(exchange_B_spotprice)) + (parseFloat(weight_C) * parseFloat(exchange_C_spotprice))
    
      console.log(index_price,'indexprice');
    var Interest_Quote_Index = 1/100;
    var Interest_Base_Index  = 0.25/100;
    var fundingrate          = (Interest_Quote_Index*Interest_Base_Index)/3;

    var timeuntillfunding    = 5;  //need to calculate
    var fundinbasis          = fundingrate * (timeuntillfunding/3);
    var mark_price           = index_price * (1 + fundinbasis);

    console.log(mark_price,'mark price');

    if(typeof index_price != 'undefined' && typeof mark_price != 'undefined')
    {
        perpetual.findOneAndUpdate({tiker_root:pair},{"$set": {"markprice":mark_price,"index_price":index_price} } , {new:true,"fields": {tiker_root:1} } ,function(pererr,perdata){
            console.log(perdata);
        }); 
    }

    var Interest_Quote_Index = 1/100;
    var Interest_Base_Index  = 0.25/100;
    // var mark_price        = 9260
   
    var Interest_Rate        = ((Interest_Quote_Index-Interest_Base_Index)/3) * 100;

    var imapactbidprice      = await getimpactbidprice('buy',pair);
    var imapactaskprice      = await getimpactbidprice('sell',pair);
    console.log(imapactbidprice,'imapactbidprice');

    var exchange_A_ticker    = await make_api_call(pair,'Bitstamp');
    var exchange_A_spotprice = exchange_A_ticker?exchange_A_ticker.last:0;

    if(typeof imapactbidprice=='undefined' && imapactbidprice.length>0 && typeof imapactbidprice[0].price == 'undefined')
    {
      return false;
    }
    imapactbidprice          = (imapactbidprice.length>0)?imapactbidprice[0].price:0;
    imapactaskprice          = (imapactaskprice.length>0)?imapactaskprice[0].price:0;
    
    console.log(imapactbidprice);
    console.log(imapactaskprice);

    var midprice      = Math.round((imapactbidprice+imapactaskprice)/2);
    console.log(midprice,'midprice');
    var premium_index = Math.max(0, parseFloat(imapactbidprice) - parseFloat(mark_price)) -  Math.max(0, parseFloat(mark_price) - parseFloat(imapactaskprice))/exchange_A_spotprice;
    console.log(premium_index,'premium_index');

    var fairbasis     = (parseFloat(midprice)/parseFloat(index_price)-1)/(30/365);
    console.log(fairbasis,'fairbasis');
    premium_index     = parseFloat(premium_index) + fairbasis;
    console.log(premium_index,'premium_index');
    console.log(Interest_Rate,'Interest_Rate');

    const clamp = (min, max) => (value) =>
      value < min ? min : value > max ? max : value;

    var Funding_Rate = premium_index + clamp(0.05, -0.05)(Interest_Rate - premium_index);
    console.log(Funding_Rate,'Funding_Rate');
    if(typeof Funding_Rate != 'undefined' && typeof mark_price != 'undefined')
    {
        perpetual.findOneAndUpdate({tiker_root:pair},{"$set": {"funding_rate":Funding_Rate,"markprice":mark_price,"index_price":index_price} } , {new:true,"fields": {tiker_root:1} } ,function(pererr,perdata){
            console.log(perdata);
        }); 
    }
    
}
function updatefunction(payerid,receiverid,amount,firstCurrency)
{
  updatebaldata["balance"] = amount;
    Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(receiverid)},{"$inc": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){
    }); 

    Assets.findOneAndUpdate({currencySymbol:firstCurrency,userId:ObjectId(payerid)},{"$dec": updatebaldata } , {new:true,"fields": {balance:1} } ,function(balerr,baldata){

    }); 
}
async function calculatingfun(tradeDetails,pairDetails)
{
    if(tradeDetails.length>0)
    { 
      var buyuserIdarr  = [];
      var selluserIdarr = [];
      var sellIdarr     = [];
      var buyIdarr      = [];
      var pairarr       = [];
      for(var i=0; i<tradeDetails.length; i++)
      {
        var quantity      = tradeDetails[i].quantity;
        var price         = tradeDetails[i].price;
        var markprice     = tradeDetails[i].pair.markprice;
        var funding_rate  = tradeDetails[i].pair.funding_rate;
        var filled        = tradeDetails[i].pair.filled;
        var leverage      = tradeDetails[i].pair.leverage;
        var firstCurrency = tradeDetails[i].pair.firstCurrency;

        if(filled)
        {
          for(var j=0; j<filled.length; j++)
          {
              var buyuserId    = filled[j].buyuserId;
              var selluserId   = filled[j].selluserId;
              var sellId       = filled[j].sellId;
              var buyId        = filled[j].buyId;
              var pair         = filled[j].pair;
              var pairname     = filled[j].pairname;
              var filledAmount = filled[j].filledAmount;
              var Price        = filled[j].Price;
              if(sellIdarr.includes(sellId) && buyIdarr.includes(buyId) && pairarr.includes(pair) && buyuserIdarr.includes(buyuserId) && selluserIdarr.includes(selluserId))
              {
                continue;
              }
              else
              {
                  sellIdarr.push(sellId);
                  buyIdarr.push(buyId);
                  pairarr.push(pair);
                  buyuserIdarr.push(buyuserId);
                  selluserIdarr.push(selluserId);
                  var index = pairDetails.findIndex(x => (x.tiker_root) === pairname);
                  if(index!=-1)
                  {
                    var markprice      = pairDetails[index].markprice;
                    var funding_rate   = pairDetails[index].funding_rate;
                    var position_value = quantity / markprice;
                    var fundingamount  = position_value * (funding_rate/100);
                    if(funding_rate>=0)
                    {
                        await updatefunction(buyuserId,selluserId,fundingamount,firstCurrency);
                    }
                    else
                    {
                        await updatefunction(selluserId,buyuserId,fundingamount,firstCurrency);
                    }
                  }

              }


          }

        }
      }
    }
}
async function fundingAmount()
{
    console.log('funding amourn');
    perpetual.find({},{tiker_root,funding_rate,markprice}).populate('pair').
    exec(function (pairerr, pairDetails) {
        if(pairDetails)
        {
            tradeTable.find({status:'1'}).populate('pair').exec(function (err, tradeDetails) {
                calculatingfun(tradeDetails,pairDetails);
            });
        }
    });
}

module.exports = router;
