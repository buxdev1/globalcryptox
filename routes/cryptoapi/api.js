

const express = require("express");
const router = express.Router();
const tradeTable = require("../../models/tradeTable");
const spottradeTable = require("../../models/spottradeTable");
const currency = require("../../models/currency");
const spotpairs = require("../../models/spotpairs");
const perpetual = require("../../models/perpetual");
const mongoose = require("mongoose");
const url = require("url");
const ObjectId = mongoose.Types.ObjectId;
const async = require("async");

router.get("/products", (req, res) => {
    async.parallel(
    {
      pairs: function (cb) {
        spotpairs.find({},{tiker_root:1,first_currency:1,second_currency:1,last:1,lowestAsk:1,highestBid:1,change:1,volume:1,secvolume:1,high:1,low:1}).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            
            var tiker_root = pairs[i].first_currency+"-"+pairs[i].second_currency;
           
            var pairdetails = {
            "id"            : tiker_root,
            "fromSymbol"    : pairs[i].first_currency,
            "toSymbol"    : pairs[i].second_currency,
            }
            data.push(pairdetails);
          }
        }
      
        res.json(data)
      }
    });
});

router.get("/contract_specs", (req, res) => {
   async.parallel(
    {
      pairs: function (cb) {
        perpetual.find({},{tiker_root:1,first_currency:1,second_currency:1,last:1,lowestAsk:1,highestBid:1,markprice:1,volume:1,secvolume:1,high:1,low:1}).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            
            var tiker_root = pairs[i].first_currency+"-"+pairs[i].second_currency;
           
            var pairdetails = {
            "contract_type"            : 'Inverse',
            "contract_price"    : parseFloat(pairs[i].markprice?pairs[i].markprice:0).toFixed(8),
            "contract_price_currency"    : 'USD',
            }
            data.push(pairdetails);
          }
        }
      
        res.json(data)
      }
    });
});
router.get("/contracts", (req, res) => {
  console.log("dffsdsdf")
    async.parallel(
    {
      pairs: function (cb) {
        perpetual.find({},{}).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            
            var tiker_root = pairs[i].first_currency+"-"+pairs[i].second_currency;
           
            var pairdetails = {
              "ticker_id"                   : tiker_root,
              "base_currency"               : pairs[i].first_currency,
              "quote_currency"              : pairs[i].second_currency,
              "last_price"                  : parseFloat(pairs[i].last?pairs[i].last:0),
              "base_volume"                 : parseFloat(pairs[i].volume?pairs[i].volume:0),
              "quote_volume"                : parseFloat(pairs[i].secvolume?pairs[i].secvolume:0),
              "bid"                         : parseFloat(pairs[i].highestBid?pairs[i].highestBid:0),
              "ask"                         : parseFloat(pairs[i].lowestAsk?pairs[i].lowestAsk:0),
              "high"                        : parseFloat(pairs[i].high?pairs[i].high:0),
              "low"                         : parseFloat(pairs[i].low?pairs[i].low:0),
              "product_type"                : "Perpetual",
              "open_interest"               : 0,
              "index_price"                 : parseFloat(pairs[i].markprice?pairs[i].markprice:0),
              "funding_rate"                : parseFloat(pairs[i].funding_rate?pairs[i].funding_rate:0),
              "next_funding_rate_timestamp" : 8,
            }
            data.push(pairdetails);
          }
        }
      
        res.json(data)
      }
    });
});

router.get("/ticker", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query     = url_parts.query;
    var buymatch = {
                $or: [{ status: "0" }, { status: "2" }],
                buyorsell: "buy",
              };
     var sellmatch = {
                $or: [{ status: "0" }, { status: "2" }],
                buyorsell: "sell",
              };
    var limit = 50;
    if(typeof query.ticker_id != 'undefined')
    {
        buymatch['pairName'] = query.ticker_id.replace("-","");
        sellmatch['pairName'] = query.ticker_id.replace("-","");
    }
    else{
      res.json({"error":"Please specify a currency pair."})
    }
    if(typeof query.depth != 'undefined')
    {
        limit = (query.depth/2);
    }
    console.log(limit)
    async.parallel(
    {
      buyOrder: function (cb) {
        var sort = { _id: -1 };
        tradeTable
          .aggregate([
            {
              $match: buymatch,
            },
            {
              $group: {
                _id: "$price",
                quantity: { $sum: "$quantity" },
                // filledAmount: { $sum: "$filledAmount" },
              },
            },
            { $sort: sort },
            { $limit: parseInt(limit) },
          ])
          .exec(cb);
      },
      sellOrder: function (cb) {
        var sort = { _id: 1 };
        tradeTable
          .aggregate([
            {
              $match: sellmatch,
            },
            {
              $group: {
                _id: "$price",
                quantity: { $sum: "$quantity" },
                // filledAmount: { $sum: "$filledAmount" },
              },
            },
            { $sort: sort },
            { $limit: parseInt(limit) },
          ])
          .exec(cb);
      },
    },
    (err, results) => {
        if(results)
        {
            var asks = [];
            var bids = [];

            if(results.sellOrder){
              var sellOrder = results.sellOrder;
              for(i=0;i<results.sellOrder.length;i++){

              var price    = parseFloat(sellOrder[i]._id?sellOrder[i]._id:0).toFixed(8);
              var quantity = parseFloat(sellOrder[i].quantity?sellOrder[i].quantity:0).toFixed(8);
              var arr      = [price,quantity];

              asks.push(arr);
              }
            }
            if(results.buyOrder){
                var buyOrder = results.buyOrder;
                for(i=0;i<results.buyOrder.length;i++){
                var price    = parseFloat(buyOrder[i]._id?buyOrder[i]._id:0).toFixed(8);
                var quantity = parseFloat(buyOrder[i].quantity?buyOrder[i].quantity:0).toFixed(8);
                var arr      = [price,quantity]
                bids.push(arr);
                }
            }
            res.json({ticker_id:query.ticker_id,timestamp:new Date().valueOf(),asks:asks,bids:bids})
        }
        
    });
}); 


router.get("/pairs", (req, res) => {
    async.parallel(
    {
      pairs: function (cb) {
        spotpairs.find({},{tiker_root:1,first_currency:1,second_currency:1,last:1,lowestAsk:1,highestBid:1,change:1,volume:1,secvolume:1,high:1,low:1}).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            
            var tiker_root = pairs[i].first_currency+"-"+pairs[i].second_currency;
           
            var pairdetails = {
            "ticker_id"            : tiker_root,
            "base"    : pairs[i].first_currency,
            "target"    : pairs[i].second_currency,
            }
            data.push(pairdetails);
          }
        }
      
        res.json(data)
      }
    });
});

router.get("/tickers", (req, res) => {
    async.parallel(
    {
      pairs: function (cb) {
        spotpairs.find({},{tiker_root:1,first_currency:1,second_currency:1,last:1,lowestAsk:1,highestBid:1,change:1,volume:1,secvolume:1,high:1,low:1}).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            
            var tiker_root = pairs[i].first_currency+"_"+pairs[i].second_currency;
           
            var pairdetails = {
            "ticker_id"       : tiker_root,
            "base_currency"   : pairs[i].first_currency,
            "target_currency" : pairs[i].second_currency,
            "last_price"      : parseFloat(pairs[i].last?pairs[i].last:0).toFixed(8),
            "base_volume"     : parseFloat(pairs[i].volume?pairs[i].volume:0).toFixed(8),
            "target_volume"   : parseFloat(pairs[i].secvolume?pairs[i].secvolume:0).toFixed(8),
            "bid"             : parseFloat(pairs[i].highestBid?pairs[i].highestBid:0).toFixed(8),
            "ask"             : parseFloat(pairs[i].lowestAsk?pairs[i].lowestAsk:0).toFixed(8),
            "high"            : parseFloat(pairs[i].high?pairs[i].high:0).toFixed(8),
            "low"             : parseFloat(pairs[i].low?pairs[i].low:0).toFixed(8),
            }
            data.push(pairdetails);
          }
        }
      
        res.json(data)
      }
    });
});

router.get("/historical_trades", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query     = url_parts.query;
  var pair   = query.ticker_id;
  var match = {};
  var limit = 100;
  match['status'] = '1';

  if(typeof query.ticker_id != 'undefined')
    {
        match['pairName'] = query.ticker_id.replace("_","").toUpperCase();
    }
    else{
      res.json({"error":"Please specify a currency pair."})
    }

    if(typeof query.type != 'undefined')
    {
        match['buyorsell'] = query.type;
    }
    else{
      res.json({"error":"Please specify a type."})
    }
    if(typeof query.limit != 'undefined')
    {
        limit = parseInt(query.limit);
    }
    if(typeof query.start_time != 'undefined')
    {
        match['orderDate'] = {$gte:new Date(query.start_time)};
    }
    if(typeof query.end_time != 'undefined')
    {
        match['orderDate'] = {$lte:new Date(query.end_time)};
    }


    async.parallel(
    {
      tradesdata: function (cb) {
        spottradeTable.find(match,{price:1,quantity:1,buyorsell:1,orderDate:1,orderValue:1}).limit(limit).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.tradesdata){
          var tradedetails = results.tradesdata;
          for(i=0;i<results.tradesdata.length;i++){
            
            var price          = tradedetails[i].price;
            var quantity       = tradedetails[i].quantity;
            var buyorsell      = tradedetails[i].buyorsell;
            var orderDate      = tradedetails[i].orderDate;
            var orderValue      = tradedetails[i].orderValue;
            var ordertimestamp = new Date(orderDate).valueOf()
            var _id            = tradedetails[i]._id;
            var timestamp      = _id.toString().substring(0,8);
            var _id            = parseInt( timestamp, 16 );

            var pairdetails = {
              "trade_id"        : _id,
              "trade_timestamp" : ordertimestamp,
              "price"     : parseFloat(price?price:0).toFixed(8),
              "base_volume"  : parseFloat(quantity?quantity:0).toFixed(8),
              "target_volume"  : parseFloat(orderValue?orderValue:0).toFixed(8),
              "type"  : buyorsell,
            }
            data.push(pairdetails);
          }
        }
      if(typeof query.ticker_id != 'undefined')
      {

        res.json(data)
      }
      }
    });
});

router.get("/orderbook", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query     = url_parts.query;
    var buymatch = {
                $or: [{ status: "0" }, { status: "2" }],
                buyorsell: "buy",
              };
     var sellmatch = {
                $or: [{ status: "0" }, { status: "2" }],
                buyorsell: "sell",
              };
    var limit = 100;
    if(typeof query.ticker_id != 'undefined')
    {
        buymatch['pairName'] = query.ticker_id.replace("_","");
        sellmatch['pairName'] = query.ticker_id.replace("_","");
    }
    else{
      res.json({"error":"Please specify a currency pair."})
    }
    if(typeof query.depth != 'undefined')
    {
        limit = query.depth;
    }
    console.log(limit)
    async.parallel(
    {
      buyOrder: function (cb) {
        var sort = { _id: -1 };
        spottradeTable
          .aggregate([
            {
              $match: buymatch,
            },
            {
              $group: {
                _id: "$price",
                quantity: { $sum: "$quantity" },
                // filledAmount: { $sum: "$filledAmount" },
              },
            },
            { $sort: sort },
            { $limit: parseInt(limit) },
          ])
          .exec(cb);
      },
      sellOrder: function (cb) {
        var sort = { _id: 1 };
        spottradeTable
          .aggregate([
            {
              $match: sellmatch,
            },
            {
              $group: {
                _id: "$price",
                quantity: { $sum: "$quantity" },
                // filledAmount: { $sum: "$filledAmount" },
              },
            },
            { $sort: sort },
            { $limit: parseInt(limit) },
          ])
          .exec(cb);
      },
    },
    (err, results) => {
        if(results)
        {
            var asks = [];
            var bids = [];

            if(results.sellOrder){
              var sellOrder = results.sellOrder;
              for(i=0;i<results.sellOrder.length;i++){

              var price    = parseFloat(sellOrder[i]._id?sellOrder[i]._id:0).toFixed(8);
              var quantity = parseFloat(sellOrder[i].quantity?sellOrder[i].quantity:0).toFixed(8);
              var arr      = [price,quantity];

              asks.push(arr);
              }
            }
            if(results.buyOrder){
                var buyOrder = results.buyOrder;
                for(i=0;i<results.buyOrder.length;i++){
                var price    = parseFloat(buyOrder[i]._id?buyOrder[i]._id:0).toFixed(8);
                var quantity = parseFloat(buyOrder[i].quantity?buyOrder[i].quantity:0).toFixed(8);
                var arr      = [price,quantity]
                bids.push(arr);
                }
            }
            res.json({asks:asks,bids:bids})
        }
        
    });
});

router.get("/trades", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query     = url_parts.query;
  var pair   = query.pair;
  var match = {};
  var limit = 100;
  match['status'] = '1';

  if(typeof query.pair != 'undefined')
    {
        match['pairName'] = query.pair.replace("-","").toUpperCase();
    }
    else{
      res.json({"error":"Please specify a currency pair."})
    }
    if(typeof query.limit != 'undefined')
    {
        limit = parseInt(query.limit);
    }


    async.parallel(
    {
      tradesdata: function (cb) {
        spottradeTable.find(match,{price:1,quantity:1,buyorsell:1,orderDate:1}).limit(limit).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = [];
          var coins = {};
        if(results.tradesdata){
          var tradedetails = results.tradesdata;
          for(i=0;i<results.tradesdata.length;i++){
            
            var price          = tradedetails[i].price;
            var quantity       = tradedetails[i].quantity;
            var buyorsell      = tradedetails[i].buyorsell;
            var orderDate      = tradedetails[i].orderDate;
            var ordertimestamp = new Date(orderDate).valueOf()
            var _id            = tradedetails[i]._id;
            var timestamp      = _id.toString().substring(0,8);
            var _id            = parseInt( timestamp, 16 );

            var pairdetails = {
              "id"        : _id,
              "timestamp" : ordertimestamp,
              "price"     : parseFloat(price?price:0).toFixed(8),
              "quantity"  : parseFloat(quantity?quantity:0).toFixed(8),
              "type"  : buyorsell,
            }
            data.push(pairdetails);
          }
        }
      if(typeof query.pair != 'undefined')
      {

        res.json(data)
      }
      }
    });
});
router.get("/publicapi", (req, res) => {
  var url       = require("url");
  var url_parts = url.parse(req.url, true);
  var query     = url_parts.query;
  var command   = query.command;
  if(command=="returnTicker")
  {
     async.parallel(
    {
      pairs: function (cb) {
        spotpairs.find({},{tiker_root:1,first_currency:1,second_currency:1,last:1,lowestAsk:1,highestBid:1,change:1,volume:1,secvolume:1,high:1,low:1}).exec(cb);
      },
      currencies: function (cb) {
        currency.find({type:{$ne:'fiat'},status:1},{currencySymbol:1,currencyName:1}).exec(cb);
      }
    },
    (err, results) => {
      if(results)
      {
          var data = {};
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            var _id        = pairs[i]._id;
            var tiker_root = pairs[i].first_currency+"_"+pairs[i].second_currency;
            var timestamp  = _id.toString().substring(0,8);
            var _id        = parseInt( timestamp, 16 );
            var pairdetails = {
            "id"            : _id,
            "last"          : parseFloat(pairs[i].last?pairs[i].last:0).toFixed(8),
            "lowestAsk"     : parseFloat(pairs[i].lowestAsk?pairs[i].lowestAsk:0).toFixed(8),
            "highestBid"    : parseFloat(pairs[i].highestBid?pairs[i].highestBid:0).toFixed(8),
            "percentChange" : parseFloat(pairs[i].change?pairs[i].change:0).toFixed(8),
            "baseVolume"    : parseFloat(pairs[i].volume?pairs[i].volume:0).toFixed(8),
            "quoteVolume"   : parseFloat(pairs[i].secvolume?pairs[i].secvolume:0).toFixed(8),
            "isFrozen"      : "0",
            "high24hr"      : parseFloat(pairs[i].high?pairs[i].high:0).toFixed(8),
            "low24hr"       : parseFloat(pairs[i].low?pairs[i].low:0).toFixed(8)
            }
            data[tiker_root] = pairdetails;
          }
        }
        if(results.currencies){
          var currencies = results.currencies;
          for(i=0;i<results.currencies.length;i++){
            var _id            = currencies[i]._id;
            var currencySymbol = currencies[i].currencySymbol;
            var currencyName   = currencies[i].currencyName;
            var timestamp      = _id.toString().substring(0,8);
            var _id            = parseInt( timestamp, 16 );
            var curdetails = {
              "name": currencyName,
              "withdraw": "ON",
              "deposit": "ON"
            }
            coins[currencySymbol] = curdetails;
          }
          // console.log(coins)
        }
        res.json({"code":"200","msg":"success","data":data,"coins":coins})
      }
    });
  }
  else if(command=="return24hVolume")
  {
     async.parallel(
    {
      pairs: function (cb) {
        spotpairs.find({},{tiker_root:1,first_currency:1,second_currency:1,last:1,lowestAsk:1,highestBid:1,change:1,volume:1,secvolume:1,high:1,low:1}).exec(cb);
      },
    },
    (err, results) => {
      if(results)
      {
          var data = {};
          var coins = {};
        if(results.pairs){
          var pairs = results.pairs;
          for(i=0;i<results.pairs.length;i++){
            var _id        = pairs[i]._id;
            var tiker_root = pairs[i].first_currency+"_"+pairs[i].second_currency;
            var timestamp  = _id.toString().substring(0,8);
            var _id        = parseInt( timestamp, 16 );
            var pairdetails = {
            "id"            : _id,
            "last"          : parseFloat(pairs[i].last?pairs[i].last:0).toFixed(8),
            "baseVolume"    : parseFloat(pairs[i].volume?pairs[i].volume:0).toFixed(8),
            "quoteVolume"   : parseFloat(pairs[i].secvolume?pairs[i].secvolume:0).toFixed(8),
            }
            data[tiker_root] = pairdetails;
          }
        }
      
        res.json(data)
      }
    });
  }
  else if(command=='returnCurrencies')
  {
    async.parallel(
    {
      currencies: function (cb) {
        currency.find({type:{$ne:'fiat'},status:1},{currencySymbol:1,currencyName:1,minimum:1,fee:1}).exec(cb);
      }
    },
    (err, results) => {
      if(results)
      {
          var data = {};
          var coins = {};
      
        if(results.currencies){
          var currencies = results.currencies;
          for(i=0;i<results.currencies.length;i++){
            var _id            = currencies[i]._id;
            var fee = currencies[i].fee;
            var currencySymbol = currencies[i].currencySymbol;
            var currencyName   = currencies[i].currencyName;
            var timestamp      = _id.toString().substring(0,8);
            var _id            = parseInt( timestamp, 16 );
            var curdetails = {
              "id": _id,
              "name": currencyName,
              "txFee": fee,
              "minConf": 1,
              "currencyType":"address",
              "depositAddress": null,
              
            }
            coins[currencySymbol] = curdetails;
          }
          // console.log(coins)
        }
        res.json(coins)
      }
    });
  }
  else if(command=='returnTrades')
  {
    var url_parts = url.parse(req.url, true);
    var query     = url_parts.query;
    var pair   = query.market_pair;
    var match = {};
    var limit = 100;
    match['status'] = '1';

    if(typeof query.market_pair != 'undefined')
      {
          match['pairName'] = query.market_pair.replace("_","").toUpperCase();
      }
      else{
        res.json({"error":"Please specify a currency pair."})
      }
      if(typeof query.limit != 'undefined')
      {
          limit = parseInt(query.limit);
      }


      async.parallel(
      {
        tradesdata: function (cb) {
          spottradeTable.find(match,{price:1,quantity:1,buyorsell:1,orderDate:1,orderValue:1}).limit(limit).exec(cb);
        },
      },
      (err, results) => {
        if(results)
        {
            var data = [];
            var coins = {};
          if(results.tradesdata){
            var tradedetails = results.tradesdata;
            for(i=0;i<results.tradesdata.length;i++){
              
              var price          = tradedetails[i].price;
              var quantity       = tradedetails[i].quantity;
              var buyorsell      = tradedetails[i].buyorsell;
              var orderDate      = tradedetails[i].orderDate;
              var orderValue      = tradedetails[i].orderValue;
              var ordertimestamp = new Date(orderDate).valueOf()
              var _id            = tradedetails[i]._id;
              var timestamp      = _id.toString().substring(0,8);
              var _id            = parseInt( timestamp, 16 );

              var pairdetails = {
                "trade_id"        : _id,
                "timestamp" : ordertimestamp,
                "price"     : parseFloat(price?price:0).toFixed(8),
                "base_volume"  : parseFloat(quantity?quantity:0).toFixed(8),
                "quote_volume"  : parseFloat(orderValue?orderValue:0).toFixed(8),
                "type"  : buyorsell,
              }
              data.push(pairdetails);
            }
          }
        if(typeof query.market_pair != 'undefined')
        {

          res.json(data)
        }
        }
      });
  }
  else if(command=='returnOrderBook')
  {
    console.log("here")
    var buymatch = {
                $or: [{ status: "0" }, { status: "2" }],
                buyorsell: "buy",
              };
     var sellmatch = {
                $or: [{ status: "0" }, { status: "2" }],
                buyorsell: "sell",
              };
    var limit = 100;
    if(typeof query.currencyPair != 'undefined')
    {
        buymatch['pairName'] = query.currencyPair.replace("_","");
        sellmatch['pairName'] = query.currencyPair.replace("_","");
    }
    else{
      res.json({"error":"Please specify a currency pair."})
    }
    if(typeof query.depth != 'undefined')
    {
        limit = query.depth;
    }
    console.log(limit)
    async.parallel(
    {
      buyOrder: function (cb) {
        var sort = { _id: -1 };
        spottradeTable
          .aggregate([
            {
              $match: buymatch,
            },
            {
              $group: {
                _id: "$price",
                quantity: { $sum: "$quantity" },
                // filledAmount: { $sum: "$filledAmount" },
              },
            },
            { $sort: sort },
            { $limit: parseInt(limit) },
          ])
          .exec(cb);
      },
      sellOrder: function (cb) {
        var sort = { _id: 1 };
        spottradeTable
          .aggregate([
            {
              $match: sellmatch,
            },
            {
              $group: {
                _id: "$price",
                quantity: { $sum: "$quantity" },
                // filledAmount: { $sum: "$filledAmount" },
              },
            },
            { $sort: sort },
            { $limit: parseInt(limit) },
          ])
          .exec(cb);
      },
    },
    (err, results) => {
        if(results)
        {
            var asks = [];
            var bids = [];

            if(results.sellOrder){
              var sellOrder = results.sellOrder;
              for(i=0;i<results.sellOrder.length;i++){

              var price    = parseFloat(sellOrder[i]._id?sellOrder[i]._id:0).toFixed(8);
              var quantity = parseFloat(sellOrder[i].quantity?sellOrder[i].quantity:0).toFixed(8);
              var arr      = [price,quantity];

              asks.push(arr);
              }
            }
            if(results.buyOrder){
                var buyOrder = results.buyOrder;
                for(i=0;i<results.buyOrder.length;i++){
                var price    = parseFloat(buyOrder[i]._id?buyOrder[i]._id:0).toFixed(8);
                var quantity = parseFloat(buyOrder[i].quantity?buyOrder[i].quantity:0).toFixed(8);
                var arr      = [price,quantity]
                bids.push(arr);
                }
            }
            res.json({asks:asks,bids:bids})
        }
        
    });
  }
});

module.exports = router;
