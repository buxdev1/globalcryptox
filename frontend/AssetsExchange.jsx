import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Select from "react-select";

import TradeHeader from "./TradeHeader";
import TradeFooter from "./TradeFooter";
import doubleArrow from "../images/doubleArrow.png";

import { getAssetExchangeData, getCurrencyData } from "../actions/authActions";

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px dotted pink",
    color: state.isSelected ? "red" : "blue",
    padding: 20
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return { ...provided, opacity, transition };
  }
};

class AssetsExchange extends Component {
  constructor(props) {
    super(props);

    this.state = {
      AssetsExchangedata: {},
      CurrencyTableData: [],
      CurrencyTableData_optionsfrom: [],
      CurrencyTableData_optionsto: [],
      selectedvalue: "",
      selectedNumber: "",
      from: "",
      to: "",
      pairdetails: 
        {
          pair: "",
          single_min_limit: "",
          single_max_limit: "",
          full_min_limit: "",
          full_max_limit: "",
          trade_fee: ""
        }
    };
    // this.onNumbersChange = this.onNumbersChange.bind(this);
    this.handleChange1 = this.handleChange1.bind(this);
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onNumbersChangefrom = selectedOption => {
    console.log("selectedOption", selectedOption);
    var current_currency = this.state.CurrencyTableData_optionsfrom;

    current_currency.map((item, i) => {
      if (selectedOption.value == current_currency[i].value) {
        current_currency[i].disabled = "yes";
        current_currency[i].className = "hideopt";
      } else {
        current_currency[i].disabled = "no";
        current_currency[i].className = "showopt";
      }
    });
    console.log("ooldd", current_currency);
    this.setState({
      CurrencyTableData_optionsto: current_currency,
      from: selectedOption.value
    });
    this.getpairdetail('from',selectedOption.value);
  };
  onNumbersChangeto = selectedOption => {
    console.log("selectedOption", selectedOption);
    var current_currency = this.state.CurrencyTableData_optionsto;

    current_currency.map((item, i) => {
      if (selectedOption.value == current_currency[i].value) {
        current_currency[i].disabled = "yes";
        current_currency[i].className = "hideopt";
      } else {
        current_currency[i].disabled = "no";
        current_currency[i].className = "showopt";
      }
    });
    console.log("ooldd", current_currency);
    this.setState({
      CurrencyTableData_optionsfrom: current_currency,
      to: selectedOption.value
    });
    this.getpairdetail('to',selectedOption.value);
  };

  getpairdetail(fromorto,value) {
    var from =this.state.from;
    var to =this.state.to;   
    
    if(fromorto=="from"){
      from = value
    }
    else if(fromorto=="to"){
      to = value
    }

    var pair = from + to;
    var AssetsExchangedata= this.state.AssetsExchangedata
    var index = AssetsExchangedata.findIndex(
      x =>
        x.pair.toLowerCase() ===
        pair.toLowerCase()
    );
    if(index!=-1){
      console.log("Index==",index)
      console.log("asset data ",AssetsExchangedata[index])
      this.setState({pairdetails:AssetsExchangedata[index]})

    }
    else{
      var empty= {
        pair: "",
        single_min_limit: "",
        single_max_limit: "",
        full_min_limit: "",
        full_max_limit: "",
        trade_fee: ""
      }
      this.setState({pairdetails:empty})
    }
  }

  handleChange = selectedOption => {
    this.setState({ first_currency: selectedOption.value });
    console.log(`Option selected:`, selectedOption);
    this.setState({ selectedvalue: selectedOption.value });
  };
  handleChange1 = selectedOption => {
    this.setState({ second_currency: selectedOption.value });
    // console.log(`Option selected:`, selectedOption );
  };

  componentWillReceiveProps(nextProps) {
    console.log("nextpropsssProps", nextProps);

    if (nextProps.errors) {
      console.log("Error in next props  ");
      this.setState({
        errors: nextProps.errors
      });
    } else {
      this.setState({
        errors: {}
      });
    }
    console.log(nextProps, "nextProps");
    if (typeof nextProps.auth != "undefined") {
      if (typeof nextProps.auth.trade != "undefined") {
        if (typeof nextProps.auth.trade.data != "undefined") {
          if (
            typeof nextProps.auth.trade.data.AssetsExchangedata != "undefined"
          ) {
            var AssetsExchangedata =
              nextProps.auth.trade.data.AssetsExchangedata;
            console.log("AssetsExchangedat----------");
            console.log(AssetsExchangedata);
            this.setState({
              AssetsExchangedata: AssetsExchangedata
            });
          }
        }

        if (typeof nextProps.auth.trade.data.CurrencyTableData != "undefined") {
          var CurrencyTableData = nextProps.auth.trade.data.CurrencyTableData;
          console.log("CurrencyTableData----------");
          console.log(CurrencyTableData);
          console.log(
            "CurrencyTableData LEngth----------",
            CurrencyTableData.length
          );

          var CurrencyTableData_optionsfrom = [];
          var CurrencyTableData_optionsto = [];

          for (var i = 0; i < CurrencyTableData.length; i++) {
            console.log("Inside i", i);
            var sample = {
              value: CurrencyTableData[i].currencySymbol,
              label: CurrencyTableData[i].currencyName
            };
            CurrencyTableData_optionsfrom.push(sample);
            CurrencyTableData_optionsto.push(sample);

            console.log("Push i value", CurrencyTableData_optionsfrom);
          }

          this.setState({
            CurrencyTableData: CurrencyTableData,
            CurrencyTableData_optionsfrom: CurrencyTableData_optionsfrom,
            CurrencyTableData_optionsto: CurrencyTableData_optionsto
          });
        }
      }
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    let userid = this.props.auth.user.id;
    var dynobj = {};
    dynobj.find = {};
    dynobj.find._id = userid;
    dynobj.table = {};
    dynobj.table.name = "AssetsExchange";
    dynobj.return = {};
    dynobj.return.name = "AssetsExchangedata";
    this.props.getAssetExchangeData(dynobj);

    var dynobj = {};
    dynobj.find = {};
    // dynobj.find.referaluserid= userid;
    dynobj.table = {};
    dynobj.table.name = "CurrencyTable";
    dynobj.return = {};
    dynobj.return.name = "CurrencyTableData";
    this.props.getCurrencyData(dynobj);
  }

  render() {
    const {
      CurrencyTableData_optionsfrom,
      CurrencyTableData_optionsto,
      selectedvalue,
      selectedNumber
    } = this.state;

    return (
      <div>
        <TradeHeader />
        <div className="container-fluid">
          <section className="tradeMain">
            <div className="row">
              <div className="col-md-12">
                <div className="darkBox assetsTable">
                  <div className="tableHead">
                    <h3>Assets Exchange</h3>
                  </div>
                  <div className="tab-content">
                    <div className="assetExchange">
                      <div className="row">
                        <div className="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                          <div className="assetEchangeForm">
                            <div className="row">
                              <div className="col-sm-5 mb-4 mb-sm-0">
                                <div className="form-group">
                                  <label>From Coin</label>

                                  <Select
                                    styles={customStyles}
                                    width="100%"
                                    menuColor="red"
                                    options={CurrencyTableData_optionsfrom}
                                    onChange={this.onNumbersChangefrom}
                                    isOptionDisabled={option =>
                                      option.disabled === "yes"
                                    }
                                  />
                                </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="form-group">
                                  <label className="d-none d-sm-block invisible">
                                    Sa
                                  </label>
                                  <div className="changeSelect">
                                    <img
                                      src={doubleArrow}
                                      className="img-fluid"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-sm-5 mb-4 mb-sm-0">
                                <div className="form-group">
                                  <label>To Coin</label>
                                  <Select
                                    onChange={this.onNumbersChangeto}
                                    styles={customStyles}
                                    width="100%"
                                    menuColor="red"
                                    options={CurrencyTableData_optionsto}
                                    isOptionDisabled={option =>
                                      option.disabled === "yes"
                                    }
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="form-group inputWithText">
                              <label>
                                Amount{" "}
                                <a href="#" className="float-right rightLink">
                                  All Amount
                                </a>
                              </label>
                              <input
                                name=""
                                className="form-control"
                                placeholder=""
                                value=""
                                type="text"
                              />
                              <div className="input-group-append-icon">
                                <small>BTC</small>
                              </div>
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              <div className="form-group">
                                <label>Single Minimum Limit</label>
                                <input
                                  onChange={this.onChange}
                                  value={this.state.pairdetails.single_min_limit}
                                  id="userid"
                                  type="text"
                                  readOnly
                                  className="form-control"
                                />
                              </div>
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              <div className="form-group">
                                <label>Single Maximum Limit</label>
                                <input
                                  onChange={this.onChange}
                                  value={this.state.pairdetails.single_max_limit}
                                  id="userid"
                                  type="text"
                                  readOnly
                                  className="form-control"
                                />
                              </div>
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              <div className="form-group">
                                <label>24 Hrs Minimum Limit</label>
                                <input
                                  onChange={this.onChange}
                                  value={this.state.pairdetails.full_min_limit}
                                  id="userid"
                                  type="text"
                                  readOnly
                                  className="form-control"
                                />
                              </div>
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              <div className="form-group">
                                <label>24 Hrs Maximum Limit</label>
                                <input
                                  onChange={this.onChange}
                                  value={this.state.pairdetails.full_max_limit}
                                  id="userid"
                                  type="text"
                                  readOnly
                                  className="form-control"
                                />
                              </div>
                            </div>  
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              <div className="form-group">
                                <label>Trade Fee</label>
                                <input
                                  onChange={this.onChange}
                                  value={this.state.pairdetails.trade_fee}
                                  id="userid"
                                  type="text"
                                  readOnly
                                  className="form-control"
                                />
                              </div>
                            </div>

                            <div className="form-group">
                              <h3 className="assetTitle">
                                <span>
                                  Receiving Amount{" "}
                                  <small>(including Fee 0.5%)</small>
                                </span>
                                0.00000000 ETH <small>= 0.00 USD</small>
                              </h3>
                            </div>
                            <div className="form-group clearfix">
                              <input
                                className="buttonType1 float-left"
                                type="button"
                                name=""
                                value="Convert Now"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                          <div className="assetExchangeWalletBalance">
                            <p>BTC Wallet Balance</p>
                            <h3>
                              1.53258798 BTC <small>= 0.00 USD</small>
                            </h3>
                            <h6>1 BTC = 49.06 ETH</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tableHead">
                      <h3>Exchange History</h3>
                    </div>
                    <div className="table-responsive">
                      <table id="assetsTable" className="table">
                        <thead>
                          <tr className="wow flipInX" data-wow-delay=".5s;">
                            <th>Time</th>
                            <th>From Coin</th>
                            <th>To Coin</th>
                            <th>Amount</th>
                            <th>Transaction ID</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td
                              colspan="6"
                              height="150"
                              className="text-center"
                            >
                              - No data Available -
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <TradeFooter />
      </div>
    );
  }
}

AssetsExchange.propTypes = {
  getAssetExchangeData: PropTypes.object.isRequired,
  getCurrencyData: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(mapStateToProps, {
  getAssetExchangeData,
  getCurrencyData
})(withRouter(AssetsExchange));
