import React, { Component } from 'react'
import footerLogo from "../images/Logo-small.png"

import footerLogo1 from "../images/footerLogo1.jpg"
import footerLogo2 from "../images/footerLogo2.png"
import footerLogo3 from "../images/footerLogo3.png"
import footerLogo4 from "../images/footerLogo4.jpg"
import footerLogo5 from "../images/footerLogo5.png"

import {Link} from 'react-router-dom';

class Footer extends Component{
	render(){
			return ( <footer className="pageFooter pt-0">
        <div className="partnerMain py-5 mb-5">
          <div class="container">
        <h3>Recognised by</h3>
        <div className="recognisedLogo">
          <a href="#" className="wow zoomIn"><img src={footerLogo1} className="img-fluid" alt="" /></a>
          <a href="#" className="wow zoomIn"><img src={footerLogo2} className="img-fluid" alt="" /></a>
          <a href="#" className="wow zoomIn"><img src={footerLogo3} className="img-fluid" alt="" /></a>
          <a href="#" className="wow zoomIn"><img src={footerLogo4} className="img-fluid" alt="" /></a>
          <a href="#" className="wow zoomIn"><img src={footerLogo5} className="img-fluid" alt="" /></a>
        </div>
      </div>
      </div>
  <div className="container">
    <div className="row">
      <div className="col-md-4 col-sm-6 mb-3 mb-md-0">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <p><a href="#"><img src={footerLogo} className="img-fluid" /></a></p>
          <p>GlobalCryptoX Private Limited<br />
             Register Code  : 14724820<br />
             Address: Rotermanni 8 , Tallinn , Estonia - 10111<br />
             Contact : +3726683102<br />
             Email : <a href="mailto:support@globalcryptox.com">support@globalcryptox.com</a></p>
             <p>&copy; Copyright ©2019 All rights reserved</p>
        </div>
      </div>
      <div className="col-md-4 col-sm-6 mb-4 mb-md-0 pl-xl-0">
        <div className="row mx-0">
          <div className="col-md-6 col-sm-6 px-0">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6>CryptoTradeX</h6>
          <ul>
            <li><Link to="/About">Abut us</Link></li>
            <li><a href="#">Fee</a></li>
            <li><Link to="/Faq">FAQ</Link></li>
          </ul>
        </div>
      </div>
      <div className="col-md-6 col-sm-6 px-0">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6>Support</h6>
          <ul>
            <li><Link to="/SupportTicket">Support Ticket</Link></li>
            <li><a href="#">Terms and Conditions</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
      </div>
      <div className="col-md-12 px-0 mt-4">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6 className="mb-2">Join our community</h6>
          <ul className="socialIcons">
            <li><a href="#"><i className="fab fa-twitter"></i></a></li>
            <li><a href="#"><i className="fab fa-medium-m"></i></a></li>
            <li><a href="#"><i className="fab fa-telegram-plane"></i></a></li>
            <li><a href="#"><i className="fab fa-facebook"></i></a></li>
            <li><a href="#"><i className="fab fa-linkedin-in"></i></a></li>
          </ul>
        </div>
      </div>
        </div>
      </div>
      <div className="col-md-4 col-sm-12">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6>Crypto Exchange Operating licence</h6>
          <p>Number : FVR000901<br />
             Providing services of exchanging a virtual currency against a fiat currency<br />
             Number :FRK000795<br />
             Providing A virtual currency wallet service</p>
          <h6 className="mb-0 footerBottTitle">GlobalCryptoX company information</h6>
          <p><a href="http://www.ariregister.rik.ee/" target="_blank">www.ariregister.rik.ee</a></p>
          <h6 className="mb-0 footerBottTitle">Company Crypto exchange and wallet license information</h6>
          <p><a href="http://www.mtr.mkm.ee/" target="_blank">www.mtr.mkm.ee</a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
);
	};
}

export default Footer;