import React, {Fragment,Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/hover.css';
import './css/animate.css';
import './css/responsive-table.css';
import './css/customScroll.css';
import './css/nice-select.css';
import './css/style.css';
import 'react-notifications-component/dist/theme.css';
import DatatablePage from './components/Table'
import Landing from './components/Landing'
import Maintanance from './components/Maintanance'
import Login from './components/Login'
import Register from './components/Register'
import PaymentCode from './components/PaymentCode'
import ForgotPassword from './components/ForgotPassword'
import Resetpassword from './components/ResetPassword'
import Footer from './components/Footer'
import Settings from './components/Settings'
import  Referral from './components/Referral';
import About_us from './components/About'
import Leverage from './components/Leverage'
import Fee from './components/Fee'
import Contact from './components/Contact'
import Support from './components/SupportTicket'
import Bonus from './components/Bonus'
import Terms from './components/Terms'
import Spot from './components/Spot'
import publicComponent from './components/publicComponent'
import Privacy from './components/Privacy'
import Faq from './components/Faq'
import SupportReply from './components/SupportReply'
import MyAssets from './components/MyAssets'
import AssetsExchange from "./components/AssetsExchange.jsx"
import ManageAddress from "./components/ManageAddress.js"
import ClosedPandL from "./components/ClosedPandL.js"
import Trade from './components/Trade'

import Orderhistory from './components/Orderhistory'
import TradeHistory from './components/TradeHistory'
import BonusHistory from './components/BonusHistory'
import AssetsHistory from './components/AssetsHistory'
import Beginnerguide from './components/BeginnersGuide'
import ReactNotification from 'react-notifications-component'
import withdrawalHistory from './components/withdrawalHistory'
import Helpmain from './components/HelpMain.jsx'
import HelpDetail from './components/HelpDetailPage'
import Apipage from "./components/Api"
import Locker from "./components/Locker"
import Coininfo from "./components/CoinInfo"
import {Redirect,BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Provider } from "react-redux";
import PrivateRoute from "./components/private-route/PrivateRoute";
import store from "./store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import axios from "axios";
import keys from "./actions/config";
import IdleTimer from 'react-idle-timer'
const url = keys.baseUrl;

if (localStorage.jwtToken) {
    const token = localStorage.jwtToken;
    setAuthToken(token);

    const decoded = jwt_decode(token);
    store.dispatch(setCurrentUser(decoded));
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        store.dispatch(logoutUser());
        window.location.href = "./login";
    }
}

class App extends Component {
      constructor(props) {
        super(props);
        this.idleTimer = null
    this.onAction = this._onAction.bind(this)
    this.onActive = this._onActive.bind(this)
    this.onIdle = this._onIdle.bind(this)
        this.state = {
          maintanancecheck : false
        }
        axios
        .get(url+"api/maintanancecheck")
         .then(res => {
           this.setState({maintanancecheck:res.data.data})
          })
         .catch()
       }
    renderRedirect = () => {
      if(this.state.maintanancecheck){
        return <Redirect to='/maintanance' />
      }
    }
    _onAction(e) {
    console.log('user did something', e)
  }

  _onActive(e) {
    console.log('user is active', e)
    console.log('time remaining', this.idleTimer.getRemainingTime())
  }

  _onIdle(e) {
    console.log('user is idle', e)
    console.log('last active', this.idleTimer.getLastActiveTime())
    if(this.idleTimer.getRemainingTime()==0)
    {
      store.dispatch(logoutUser());
        window.location.href = "/login";
    }
  }
  render() {
   return (
    <div>
    <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={1000*60*10} />
    <Provider store={store}>
    <Router basename={'/'}>
      <Fragment>
      <ReactNotification />
        <Route exact path='/' component={Landing} />
        <section className='container-fluid'>
          <Switch>
           <Route exact path='/maintanance' component={Maintanance} />
            <Route exact path='/Payment' component={PaymentCode} />
            <Route exact path='/Register' component={Register} />
            <Route exact path='/Login' component={Login} />
            <Route exact path='/Activate/:id' component={Login} />
            <Route exact path='/ForgotPassword' component={ForgotPassword} />
            <Route exact path='/resetpassword/:id' component={Resetpassword} />
            <Route exact path='/About_us' component={About_us} />
            <Route exact path='/Leverage' component={Leverage} />
            <Route exact path='/Bonus' component={Bonus} />
            <Route exact path='/Contact_us' component={Contact} />
            <Route exact path='/Terms' component={Terms} />
            <Route exact path='/Privacy' component={Privacy} />
            <Route exact path='/Faq' component={Faq} />
            <Route exact path='/Trade' component={Trade} />
            <Route exact path='/ClosedPandL' component={ClosedPandL} />

            <Route exact path='/Trade/:currency' component={Trade} />
            <Route exact path='/Spot/:currency' component={Spot} />
            <Route exact path='/public' component={publicComponent} />

            <Route exact path='/helpcenter' component={Helpmain} />
            <Route exact path='/apidocs' component={Apipage} />
            <Route exact path='/Fee' component={Fee} />
            <Route exact path='/coininfo' component={Coininfo} />


            <Route exact path='/beginnersguide' component={Beginnerguide} />

            <Route exact path='/helpdetails/:aid' component={HelpDetail} />

            <Switch>
              <PrivateRoute exact path='/settings' component={Settings} />
              <PrivateRoute exact path='/MyAssets' component={MyAssets} />
              <PrivateRoute exact path='/Referral_Program' component={Referral} />
              <PrivateRoute exact path='/support' component={Support} />
              <PrivateRoute exact path='/supportreply' component={SupportReply} />
              <PrivateRoute exact path='/withdrawalHistory' component={withdrawalHistory} />      

              {/* <PrivateRoute exact path='/Fee' component={Fee} /> */}
              <PrivateRoute exact path='/Withdraw/:id' component={Landing} />
              // <PrivateRoute exact path='/Trade' component={Trade} />
              <PrivateRoute exact path='/Orderhistory' component={Orderhistory} />
              <PrivateRoute exact path='/BonusHistory' component={BonusHistory} />
              <PrivateRoute exact path='/TradeHistory' component={TradeHistory} />
              <PrivateRoute exact path='/AssetsHistory' component={AssetsHistory} />
              // <PrivateRoute exact path='/Trade/:currency' component={Trade} />
              <PrivateRoute exact path='/AssetsExchange' component={AssetsExchange} />
              <PrivateRoute exact path='/ManageAddress' component={ManageAddress} />
              <PrivateRoute exact path='/Locker' component={Locker} />



            </Switch>
          </Switch>
        </section>
      </Fragment>
      {this.renderRedirect()}
    </Router>
    </Provider>
    </div>
  );
}
}

export default App;
