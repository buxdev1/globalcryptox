import React, {Fragment,Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/hover.css';
import './css/animate.css';
import './css/responsive-table.css';
import './css/customScroll.css';
import './css/nice-select.css';
import './css/style.css';
import 'react-notifications-component/dist/theme.css';

import DatatablePage from './components/Table'
import Landing from './components/Landing'
import Login from './components/Login'
import Register from './components/Register'
import PaymentCode from './components/PaymentCode'
import ForgotPassword from './components/ForgotPassword'
import Resetpassword from './components/ResetPassword'
import Footer from './components/Footer'
import Settings from './components/Settings'
import  Referral from './components/Referral';
import About_us from './components/About'
import Leverage from './components/Leverage'
import Fee from './components/Fee'
import Contact from './components/Contact'
import Support from './components/SupportTicket'
import Bonus from './components/Bonus'
import Terms from './components/Terms'
import Spot from './components/Spot'
import Privacy from './components/Privacy'
import Faq from './components/Faq'
import SupportReply from './components/SupportReply'
import withdrawalHistory from './components/withdrawalHistory'
import MyAssets from './components/MyAssets'
import AssetsExchange from "./components/AssetsExchange.jsx"
import ManageAddress from "./components/ManageAddress.js"
import ClosedPandL from "./components/ClosedPandL.js"
import Trade from './components/Trade'
import Maintanance from './components/Maintanance'
import Orderhistory from './components/Orderhistory'
import TradeHistory from './components/TradeHistory'
import BonusHistory from './components/BonusHistory'
import AssetsHistory from './components/AssetsHistory'
import Beginnerguide from './components/BeginnersGuide'
import ReactNotification from 'react-notifications-component'
import Helpmain from './components/HelpMain.jsx'
import HelpDetail from './components/HelpDetailPage'
import {Redirect,BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Provider } from "react-redux";
import PrivateRoute from "./components/private-route/PrivateRoute";
import store from "./store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import axios from "axios";
import keys from "./actions/config";
const url = keys.baseUrl;


if (localStorage.jwtToken) {
    const token = localStorage.jwtToken;
    setAuthToken(token);

    const decoded = jwt_decode(token);
    store.dispatch(setCurrentUser(decoded));
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        store.dispatch(logoutUser());
        window.location.href = "./login";
    }

}
function App() {
  return (
    <Provider store={store}>
    <Router basename={'/'}>
      <Fragment>
      <ReactNotification />
        <Route exact path='/' component={Landing} />
        <section className='container-fluid'>
          <Switch>
            <Route exact path='/Payment' component={PaymentCode} />
            <Route exact path='/Register' component={Register} />
            <Route exact path='/Login' component={Login} />
            <Route exact path='/Activate/:id' component={Login} />
            <Route exact path='/ForgotPassword' component={ForgotPassword} />
            <Route exact path='/resetpassword/:id' component={Resetpassword} />
            <Route exact path='/Withdraw/:id' component={Landing} />
            <Route exact path='/About_us' component={About_us} />
            <Route exact path='/Leverage' component={Leverage} />
            <Route exact path='/Bonus' component={Bonus} />
            <Route exact path='/Contact_us' component={Contact} />
            <Route exact path='/Terms' component={Terms} />
            <Route exact path='/Privacy' component={Privacy} />
            <Route exact path='/Faq' component={Faq} />
            <Route exact path='/Trade' component={Trade} />
            <Route exact path='/ClosedPandL' component={ClosedPandL} />

            <Route exact path='/Trade/:currency' component={Trade} />

            <Route exact path='/helpcenter' component={Helpmain} />
            <Route exact path='/Fee' component={Fee} />

            <Route exact path='/beginnersguide' component={Beginnerguide} />

            <Route exact path='/helpdetails/:aid' component={HelpDetail} />

            <Switch>
              <PrivateRoute exact path='/settings' component={Settings} />
              <PrivateRoute exact path='/MyAssets' component={MyAssets} />
              <PrivateRoute exact path='/withdrawalHistory' component={withdrawalHistory} />
              <PrivateRoute exact path='/Referral_Program' component={Referral} />
              <PrivateRoute exact path='/support' component={Support} />
              <PrivateRoute exact path='/supportreply' component={SupportReply} />
              // <PrivateRoute exact path='/Trade' component={Trade} />
              <PrivateRoute exact path='/Orderhistory' component={Orderhistory} />
              <PrivateRoute exact path='/BonusHistory' component={BonusHistory} />
              <PrivateRoute exact path='/TradeHistory' component={TradeHistory} />
              <PrivateRoute exact path='/AssetsHistory' component={AssetsHistory} />
              // <PrivateRoute exact path='/Trade/:currency' component={Trade} />
              <PrivateRoute exact path='/AssetsExchange' component={AssetsExchange} />
              <PrivateRoute exact path='/ManageAddress' component={ManageAddress} />

            </Switch>
          </Switch>
        </section>

      </Fragment>
    </Router>
    </Provider>
  );
}

export default App;
