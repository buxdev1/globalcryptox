module.exports = {
    // siteUrl : "http://localhost/",
    // imageUrl : "http://localhost:5000/",
    // baseUrl : "http://localhost:5000/",
    // socketUrl : "http://localhost:5000/",
    // Recaptchakey : "6LdpeoQUAAAAAHwFEDfpcA-W5-leSH8548lZWWeb", //local

    siteUrl     : "https://globalcryptox.com/",
    imageUrl     : "https://api.globalcryptox.com/",
    baseUrl      : "https://api.globalcryptox.com/",
    socketUrl    : "https://api.globalcryptox.com/",
    Recaptchakey : "6Lc_VdwUAAAAADItI2_2GEk6m4fmfxhTcnSC1C7H", //live

    // siteUrl     : "http://139.162.211.153/",
    // imageUrl     : "http://139.162.211.153:5000/",
    // baseUrl      : "http://139.162.211.153:5000/",
    // socketUrl    : "http://139.162.211.153:5000/",
    // Recaptchakey : "6Lc_VdwUAAAAADItI2_2GEk6m4fmfxhTcnSC1C7H", //demo
};

