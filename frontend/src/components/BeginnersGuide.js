import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import AboutImg from "../images/aboutus.png"

class About extends Component {
	render() {
		return (<div>
			<Navbar />
			<section className="innerCMS">
  <div className="container">
    <div className="row">
      <div className="col-md-12 mx-auto">
        <img src={AboutImg} className="img-fluid cmsTopImg mt-1" />
        <div className="darkBox contentPage">
          <div className="tableHead tableHeadBlock helpMainTab">
            <nav>
              <div className="nav nav-tabs" id="navLink-tab" role="tablist">
                <a href="help-main.html" className="nav-item nav-link">Trading Academy</a>
                <a href="beginners-guide.html" className="nav-item nav-link active">Beginner's Guide</a>
                <a href="#" className="nav-item nav-link">Reference</a>
                <a href="#" className="nav-item nav-link">FAQ</a>
                <a href="#" className="nav-item nav-link">News</a>
              </div>
            </nav>
          </div>
          <div className="darkBoxSpace helpPage">
            <div className="wow fadeIn">
              <h3 className="helpPageSubTitle text-center mt-4">Beginner's Guide</h3>
              <div id="accordion">
                <div className="card">
                  <div className="card-header wow flipInX" id="accordianheadingOne">
                    <h5 className="mb-0">
                      <button className="btn btn-link" data-toggle="collapse" data-target="#accordiancollapseOne" aria-expanded="true" aria-controls="accordiancollapseOne"><span className="question">Account Registration</span> <i className="fa fa-plus" aria-hidden="true"></i></button>
                    </h5>
                  </div>

                  <div id="accordiancollapseOne" className="collapse show" aria-labelledby="accordianheadingOne" data-parent="#accordion">
                    <div className="card-body">
                      <ul className="helpList">
                        <li><a href="#">Register a Real Account</a></li>
                        <li><a href="#">Register a Testnet Account</a></li>
                        <li><a href="#">Forgot Password/How to Retrieve Password </a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header wow flipInX" id="accordianheadingTwo">
                    <h5 className="mb-0">
                      <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#accordiancollapseTwo" aria-expanded="false" aria-controls="accordiancollapseTwo"><span className="question">Account and Security</span> <i className="fa fa-plus" aria-hidden="true"></i></button>
                    </h5>
                  </div>
                  <div id="accordiancollapseTwo" className="collapse" aria-labelledby="accordianheadingTwo" data-parent="#accordion">
                    <div className="card-body">
                      <ul className="helpList">
                        <li><a href="#">How to install the Bybit APP on your Smartphone?</a></li>
                        <li><a href="#">Account Overview</a></li>
                        <li><a href="#">Account Details</a></li>
                        <li><a href="#">Security</a></li>
                        <li><a href="#">Set/Change Email</a></li>
                        <li><a href="#">Set/Change Google Authentication</a></li>
                        <li><a href="#">Set/Change Mobile Phone</a></li>
                        <li><a href="#">API Management </a></li>
                        <li><a href="#">Add New API Key</a></li>
                        <li><a href="#">Settings</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header wow flipInX" id="accordianheadingThree">
                    <h5 className="mb-0">
                      <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#accordiancollapseThree" aria-expanded="false" aria-controls="accordiancollapseThree"><span className="question">Deposit and Withdrawal</span> <i className="fa fa-plus" aria-hidden="true"></i></button>
                    </h5>
                  </div>
                  <div id="accordiancollapseThree" className="collapse" aria-labelledby="accordianheadingThree" data-parent="#accordion">
                    <div className="card-body">
                      <ul className="helpList">
                        <li><a href="#">How to Make a Deposit or Withdrawal on Bybit?</a></li>
                        <li><a href="#">How to Submit a Withdrawal Request?</a></li>
                        <li><a href="#">How To Add Your Wallet Address?</a></li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header wow flipInX" id="accordianheadingFour">
                    <h5 className="mb-0">
                      <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#accordiancollapseFour" aria-expanded="false" aria-controls="accordiancollapseFour"><span className="question">Placing an order</span> <i className="fa fa-plus" aria-hidden="true"></i></button>
                    </h5>
                  </div>
                  <div id="accordiancollapseFour" className="collapse" aria-labelledby="accordianheadingFour" data-parent="#accordion">
                    <div className="card-body">
                      <ul className="helpList">
                        <li><a href="#">How to Set a Different Reference Price to Trigger Take Profit/Stop Loss?</a></li>
                        <li><a href="#">Order Placing Overview</a></li>
                        <li><a href="#">Market Order</a></li>
                        <li><a href="#">Limit Order</a></li>
                        <li><a href="#">Conditional Order</a></li>
                        <li><a href="#">How to Partial Close Your Open Position?</a></li>
                        <li><a href="#">Set Take Profit/Stop Loss</a></li>
                        <li><a href="#">Time in Force</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header wow flipInX" id="accordianheadingFive">
                    <h5 className="mb-0">
                      <button className="btn btn-link collapsed" data-toggle="collapse" data-target="#accordiancollapseFive" aria-expanded="false" aria-controls="accordiancollapseFive"><span className="question">My Position</span> <i className="fa fa-plus" aria-hidden="true"></i></button>
                    </h5>
                  </div>
                  <div id="accordiancollapseFive" className="collapse" aria-labelledby="accordianheadingFive" data-parent="#accordion">
                    <div className="card-body">
                      <ul className="helpList">
                        <li><a href="#">Positions</a></li>
                        <li><a href="#">Active Order</a></li>
                        <li><a href="#">Conditional Order</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Position Closing</a></li>
                        <li><a href="#">Set or Adjust Leverage and Margin</a></li>
                        <li><a href="#">Set or Adjust Take Profit/Stop Loss</a></li>
                        <li><a href="#">Set or Adjust Trailing Stop</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
			<Footer />
		</div>
		);
	}
}

export default About