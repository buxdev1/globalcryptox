import React, { Component } from 'react'
import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import DepositeQR from "../images/depositeQR.jpg"
import BtcIcon from "../images/btcIcon.png"
import EthIcon from "../images/ethIcon.png"
import XrpIcon from "../images/xrpIcon.png"
import LtcIcon from "../images/ltcIcon.png"
import BchIcon from "../images/bchIcon.png"
import { store } from 'react-notifications-component';
import keys from "../actions/config";
import Select from 'react-select';
import axios from "axios";
import DatePicker from "react-datepicker";
import {bonushistory,Searchdata} from "../actions/authActions";
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { CSVLink, CSVDownload } from "react-csv";
import { Scrollbars } from 'react-custom-scrollbars';

import "react-datepicker/dist/react-datepicker.css";
const url = keys.baseUrl;



const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: '1px dotted pink',
    color: state.isSelected ? 'red' : 'blue',
    padding: 20,
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';

    return { ...provided, opacity, transition };
  }
}

const options = [
	{ value: 10, label: '10' },
  { value: 20, label: '20' },
  { value: 50, label: '50' },
  { value: 100, label: '100' },
  // { value: "All", label: 'All' },

];

// const options = [{ value: 'All', label: 'All' },];

class BonusHistory extends Component {
	constructor(props) {
    super(props);
    this.state = {
      historydetails  : [],
      duplicaterecords:[],
			records:[],
			csvData:[],
			contract:{value:10,label:'10'},
			type:{value:'All',label:'All'},
			startDate:'',
      endDate:'',
      limit:20,
      rescentcount:10
    };
  }

  componentDidMount() {
            this.getData()
        };
    componentWillReceiveProps(nextProps) {

        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
        console.log(nextProps.auth.trade,'resultkjljflsjdfsdf')
				if (nextProps.auth !== undefined
            && nextProps.auth.trade !== undefined
            && nextProps.auth.trade.data !== undefined
            && nextProps.auth.trade.data.data !== undefined
            && nextProps.auth.trade.data.type !== undefined
            && nextProps.auth.trade.data.type == 'bonushistory'
            ) {
          // console.log(nextProps.auth.trade.data.data,'daajdflskdjflskdjf')
						        this.setState({historydetails:nextProps.auth.trade.data.data});
                    this.buildarray(nextProps.auth.trade.data.data);
        }
    }
		changecontract = contract => {
      this.setState({ contract });
      this.setState({ limit:contract.value });
      var input = {"userid":this.props.auth.user.id, "limit":contract.value}
      axios.post(url + "cryptoapi/bonus-historywithlimit", input).then((res) => {
        // console.log("ress invested arra", res.data);
        this.setState({ historydetails: res.data.data });
      });

	  };

		changetype = type => {
	    this.setState({ type });
	  };

		getData() {
      // var input = {"userid":this.props.auth.user.id}
        // this.props.bonushistory(input);
        var input = {"userid":this.props.auth.user.id, "limit":10,rescentcount:this.state.rescentcount}
        axios.post(url + "cryptoapi/bonus-historywithlimit", input).then((res) => {
          // console.log("ress invested arra", res.data);
          this.setState({ historydetails: res.data.data });
          this.setState({duplicaterecords:res.data.data})
        });
  
		}

    buildarray = (historydata) =>{
    //   var csvdata = [];
    //   var titledata = ["Contracts","Qty","Filled remaining","Exec price","Trigger price","Order price","Order value","Trading fess","Type","Status","Order#","Order Time"];
    //   console.log(csvdata,'test');
    //   csvdata.push(titledata);
    //     console.log('if');
				// historydata.map((item,i)=>{
    //       var pairName = item.pairName?item.pairName:0;
    //       var quantity = item.quantity?item.quantity:0;
    //       var price = item.quantity?item.price:0;
    //       var buyorsell = item.buyorsell?item.buyorsell:0;
    //       var filledAmount = item.filledAmount?item.filledAmount:0;
    //       var orderValue = item.orderValue?item.orderValue:0;
    //       var orderType = item.orderType?item.orderType:0;
    //       var orderDate = item.orderDate?item.orderDate:0;
    //       var _id = item._id?item._id:0;
    //       var status1 = item.status;
    //       var e_price = item.filled.length>0?item.filled[0].Price:0;
    //       var tradingfees = item.filled.length>0?item.filled[0].Fees:0;
    //       var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
    //       var data1 = new Date(orderDate);
    //       let date12 = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();

    //       var status = (status1 == 0)?"New":(status1 == 1)?"Completed":(status1 == 2)?"Partial":(status1 == 3)?"Cancel":''
    //       var newarr = [];
    //       newarr.push(pairName);
    //       newarr.push(quantity);
    //       newarr.push(filledAmount+'/'+Remaining);
    //       newarr.push(e_price);
    //       newarr.push(price);
    //       newarr.push('-/-');
    //       newarr.push(orderValue);
    //       newarr.push(parseFloat(tradingfees).toFixed(8));
    //       newarr.push(buyorsell);
    //       newarr.push(status);
    //       newarr.push(_id);
    //       newarr.push(date12);
    //       csvdata.push(newarr);
				// });
    //     console.log(csvdata,'csvdata');
    //     this.setState({csvData:csvdata})

    }

      loadmoreRescentorder = () => {
         var input = {"userid":this.props.auth.user.id, "limit":10,rescentcount:this.state.rescentcount+10}
    axios
      .post(url+"cryptoapi/bonus-historywithlimit",input)
      .then(res => {
        this.setState({historydetails:res.data.data,rescentcount:(this.state.rescentcount+10)})
      });
  }

		Searchdata = () => {
      var postdetails = {
        "contract"  : this.state.contract.value,
        "type"      : this.state.type.value,
        "startDate" : this.state.startDate,
        "endDate"   : this.state.endDate,
        "userid"    : this.props.auth.user.id
      }
			this.props.Searchdata(postdetails);
		}

		// pairdatachange = data =>{
		// 	this.setState({records:data});
		// 	if(this.state.records.length==0)
		// 	{
    //     if(data.length>0){
  	// 			data.map((item,i)=>{
    //         if(item.first_currency != '' && item.second_currency != '')
    //         {
    //           var one = {"value":item.first_currency+item.second_currency,"label":item.first_currency+item.second_currency}
    //           options.push(one);
    //         }
  	// 			});
    //     }
		// 	}
    // }

		handleChange = date => {
			this.setState({
				startDate: date
			});
		};


		handleChange1 = date => {
			this.setState({
				endDate: date
			});
		};

    onChange = (e) => {
      this.setState({ [e.target.id]: e.target.value });
      // var term = e.target.value
      // var newarray = {}
      // newarray=this.state.duplicaterecords.filter(function(tag){
      //   return  tag.tiker_root.toLowerCase().indexOf(term.toLowerCase())>=0
      // })
      // this.setState({historydetails:newarray})
    };
  

  render() {
  	 const {historydetails,timeinforcetype,contract,type} = this.state
    return (
			<div>
			<div className="modal fade" id="deposite" tabindex="-1" role="dialog" aria-labelledby="deposite" aria-hidden="true">
  <div className="modal-dialog modal-md modal-dialog-centered" role="document">
    <div className="modal-content depositeContent dwCont">
      <div className="modal-body">
        <div className="popHead">
          <h4>BTC Deposit</h4>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="popUpSpace">
          <div className="depositeQr">
            <img src={DepositeQR} className="img-fluid" />
          </div>
          <div className="walletKey">
            <div className="row">
              <div className="col-md-12">
                <div className="form-group inputWithText">
                    <input name="" className="form-control" placeholder="" value="3CMCRgEm8HVz3DrWaCCid3vAANE42jcEv9" type="text" />
                    <button className="input-group-append-icon"><i className="far fa-copy"></i></button>
                </div>
              </div>
            </div>
          </div>
          <div className="noteCont">
            <h6>Notes:</h6>
            <p>1. Do not deposit any non- BTC assets to the above address. Otherwise, the assets will lost permanently.</p>
            <p>2. Please provide the correct GlobalCryptoX wallet address to ensure a deposit.</p>
            <p>3. Your deposit will be reflected inside your account after receiving 1 confirmation on the blockchain.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <div className="modal fade" id="Withdraw" tabindex="-1" role="dialog" aria-labelledby="Withdraw" aria-hidden="true">
  <div className="modal-dialog modal-md modal-dialog-centered" role="document">
    <div className="modal-content WithdrawContent dwCont">
      <div className="modal-body">
        <div className="popHead">
          <h4>BTC Withdraw</h4>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="popUpSpace withdrawPopupForm">
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <label>Receiver’s Wallet Address <a href="#" className="float-right">Add Wallet</a></label>
                <input type="text" name="" className="form-control" value="3CMCRgEm8HVz3DrWaCCid3vAANE42jcEv9" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label>BTC Amount <span className="float-right">Balance: <small>10.23569878 BTC</small> | <a href="#">Send All</a></span></label>
                <input type="text" name="" className="form-control" value="0.00000000" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label>Final Amount <span className="float-right">+ Fee: 0.00005 BTC</span></label>
                <input type="text" name="" className="form-control" value="0.00000000" disabled="" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label>Enter 2FA Authentication Code</label>
                <input type="text" name="" className="form-control" value="123456" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <input type="submit" name="" value="Withdraw" className="btn buttonType1" />
              </div>
            </div>
          </div>
          <div className="noteCont">
            <h6>Notes:</h6>
            <p>1. Minimum withdrawal:0.002BTC.</p>
            <p>2. Your withdraw will be reflected inside your account after receiving 1 confirmation  on the blockchain.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
				<TradeHeader/>
				<div className="container-fluid">
    		<section className="tradeMain">
			  <div className="row">
			    <div className="col-md-12">
			      <div className="darkBox tradeLimitMarket assetsTable">
			        <nav>
			          <div className="nav nav-tabs" id="navLink-tab" role="tablist">
			            {/* <a href="/myAssets" className="nav-item nav-link">My Assets</a> */}
            <a href="/closedPandL" className="nav-item nav-link ">Closed P&L</a>
            <a href="/AssetsHistory" className="nav-item nav-link">Deposit History</a>
            <a href="/OrderHistory" className="nav-item nav-link">Order History</a>
            <a href="/TradeHistory" className="nav-item nav-link">Trade History</a>
            <a href="/withdrawalHistory" className="nav-item nav-link">Withdrawal History</a>
            <a href="/BonusHistory" className="nav-item nav-link active">Bonus History</a>
            {/* <a href="/Locker" className="nav-item nav-link">Finance</a> */}

			          </div>
			        </nav>
              <div className="assetHistoryTableTop clearfix">
                      <div className="row">
                        <div className="col-lg-10 col-md-9 col-sm-8">
                          <div className="row">
                            

                            {/* <div className="col-lg-2 col-md-4 col-sm-6">
                                <div className="form-group">
                                  <label>End Date</label>
                                  <input
                                        onChange={this.onChange}
                                        value={this.state.lockeramount}
                                        id="lockeramount"
                                        type="text"
                                        className="form-control"
                                        placeholder="Enter the Locking Amount"
                                      />
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-4 col-sm-6">
                              <div className="form-group">
                                <label className="d-none d-md-block invisible">s</label>
                                <input type="button" name="" value="Search" onClick={this.Searchdata} className="buttonType1" />
                              </div>
                            </div> */}
                           
                           
                          </div>
                        </div>
                 
                      </div>
                    </div>

              
			        <div className="tab-content">

			           <div className="table-responsive">
                {this.state.historydetails?
                   <Scrollbars style={{ width: '100%', height: 400 }} renderTrackVertical={props => <div className="track-vertical"/>}>
			          <table id="assetsTable" className="table">
			                                            <thead>
			                                                <tr className="wow flipInX" data-wow-delay=".5s;">
			                                                    <th>Type</th>
			                                                    <th>Bonus</th>
                                                          <th>Deposit amount</th>
			                                                    <th>Date</th>
                                                          <th>Referreduser</th>
			                                                   
			                                                </tr>
			                                            </thead>
			                                            <tbody>
                                                   {
                                                    historydetails.map((item,i)=>{ console.log(item,'itemitem')                                             
                                                 var type = item.type?item.type:'';
                                                var depositamount = item.depositamount?item.depositamount:0;
                                                var bonus_amount = item.bonus_amount?item.bonus_amount:0;
                                                var createdDate = item.createdDate?item.createdDate:0;
                                                var referuser = item.referId?item.referId.email:'';
                                                
                                                var data1 = new Date(createdDate);
                                                let date12 = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
			                                              return (
                                                    <tr >
			                                                    <td>{(type=='0')?'Signup':(type=='1')?'Referral':(type=='2')?'Deposit':(type=='3')?'Social media promotion':(type=='4')?"Trade":''}</td>
			                                                   
			                                                     <td className="">{bonus_amount}</td>
			                                                    <td>{depositamount}</td>
                                                          <td>{date12}</td>
                                                          <td>{referuser}</td>
			                                                </tr>
                                                    )
                                                       })
                                                }
                                                {(this.state.historydetails.length>=10)?
          <tr><td colspan="5">
          <a onClick={this.loadmoreRescentorder} class="d-block text-center mx-auto"><i class="fas fa-sort-amount-down-alt"></i> Load More</a></td></tr>:''}
			                                            </tbody>
			                                        </table></Scrollbars>:''}
			          </div>

			        </div>
			      </div>
			    </div>
			  </div>
			</section>
			</div>
    	<TradeFooter />
			</div>
			);
    }
}

BonusHistory.propTypes = {
  Searchdata    : PropTypes.func.isRequired,
  bonushistory    : PropTypes.func.isRequired,
  auth             : PropTypes.object.isRequired,
  errors           : PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(

  mapStateToProps,
  {
    bonushistory,
    Searchdata,
  }
)(withRouter(BonusHistory));
