import React, { Component } from 'react'
import BannerX from "../images/xicon.png"
import CaptchaImg from "../images/captchaImg.jpg"
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { store } from 'react-notifications-component';
import { forgotUser,mobforgotUser,mobresetPassword } from "../actions/authActions";
import classnames from "classnames";
import PhoneInput from 'react-phone-number-input';
import ReCAPTCHA from "react-google-recaptcha";
import keys from "../actions/config";
const url = keys.baseUrl;
const Recaptchakey = keys.Recaptchakey;
const recaptchaRef = React.createRef();

const grecaptchaObject = window.grecaptcha

class ForgotPassword extends Component {

	constructor() {
			super();
			this.state = {
					email: "",
					phone: "",
					otpver: "",
					timer: "",
					resend: "",
					recaptcha: "",
					mobrecaptcha: "",
					errors: {},
					time: {}, seconds: 120
			};
		this.timer = 0;
		this.startTimer = this.startTimer.bind(this);
		this.countDown = this.countDown.bind(this);
	}

	componentDidMount() {
			if (this.props.auth.isAuthenticated) {
					this.props.history.push("/settings");
			}
			 let timeLeftVar = this.secondsToTime(this.state.seconds);
    	this.setState({ time: timeLeftVar });
	}

	captchaChange = e => {
			this.setState({ recaptcha : e });
	};

	captchaChange1 = e => {
			this.setState({ mobrecaptcha : e });
	};

	componentWillReceiveProps(nextProps) {
		console.log(nextProps.auth.forgotuser);
			if (nextProps.errors) {
					this.setState({
							errors: nextProps.errors
					});
			}

			if (nextProps.auth.forgotuser !== undefined
					&& nextProps.auth.forgotuser.data !== undefined
					&& nextProps.auth.forgotuser.data.message !== undefined ) {
					store.addNotification({
					  title: "Wonderful!",
					  message: nextProps.auth.forgotuser.data.message,
					  type: "success",
					  insert: "top",
					  container: "top-right",
					  animationIn: ["animated", "fadeIn"],
					  animationOut: ["animated", "fadeOut"],
					  dismiss: {
					    duration: 5000,
					    onScreen: true
					  }
					});
					this.setState({
						otpver : (this.state.phone != '')?'true':''
					});
					if(nextProps.auth.forgotuser.data.message=='Your password has been reset successfully')
					{
						nextProps.auth.forgotuser = "";
								this.props.history.push("/Login");
					}
					else
					{
							if(this.state.phone != '')
							{
								this.setState({timer:'true',seconds:120});
								this.state.seconds = 120;
								this.timer = 0
								this.startTimer();
								nextProps.auth.forgotuser = "";
							}
							else
							{
								nextProps.auth.forgotuser = "";
								this.props.history.push("/Login");

							}
					}
			}
	}

	onChange = e => {
			this.setState({ [e.target.id]: e.target.value });
	};

	onChangeCheckbox = e => {
			this.setState({ [e.target.id]: e.target.checked });
	};


	onSubmit = e => {
			e.preventDefault();
			const forgotUser = {
					email: this.state.email,
					recaptcha: this.state.recaptcha,
			};
			console.log(forgotUser);	
			this.props.forgotUser(forgotUser, this.props.history);
	};

	startTimer() {
		if (this.timer == 0 && this.state.seconds > 0) {
		this.timer = setInterval(this.countDown, 1000);
		}
	}

	countDown() {
		// Remove one second, set state so a re-render happens.
		let seconds = this.state.seconds - 1;
		this.setState({
		time: this.secondsToTime(seconds),
		seconds: seconds,
		});

		// Check if we're at zero.
		if (seconds == 0) { 
		clearInterval(this.timer);
		this.setState({timer:'',resend:'true'})
		}
	}

	mobileSubmit = e => {
			e.preventDefault();
			const forgotUser = {
					phone: this.state.phone,
					mobrecaptcha: this.state.mobrecaptcha,
			};
			this.props.mobforgotUser(forgotUser, this.props.history);
	};

	mobileSubmit1 = e => {
		this.setState({resend:''})
			e.preventDefault();
			const forgotUser = {
					phone: this.state.phone,
			};
			this.props.mobforgotUser(forgotUser, this.props.history);
	};

	resetSubmit = e => {
			e.preventDefault();
			const forgotUser = {
					phone: this.state.phone,
					resetpassword: this.state.resetpassword,
					password2: this.state.password2,
					otpcode: this.state.otpcode,
					action:'reset'
			};
			this.props.mobresetPassword(forgotUser, this.props.history);
	};

	secondsToTime(secs){
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    return obj;
  }



	render() {
		const { errors } = this.state;
		return <div><header className="loginBanner">
  <div className="container">
    <div className="row">
      <div className="col-md-6 mx-auto">
        <div className="formBox wow flipInY mt-3 mt-md-5 mb-5 mb-md-0" data-wow-delay=".15s;">
         <h2>Forgot Password</h2>
         
        
        <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
          <div className="tab-pane fade show active" id="nav-emailLogin" role="tabpanel" aria-labelledby="nav-emailLogin-tab">
            <form id="" name="" className="stoForm landingForm" noValidate onSubmit={this.onSubmit} >
            <div className="form-group fadeIn second mb-5 mt-3">
            <label>Email address</label>
						<input
								onChange={this.onChange}
								value={this.state.email}
								error={errors.email}
								id="email"
								type="email"
								className={classnames("form-control", {
										invalid: errors.email
								})}
						/>
						<span className="text-danger">{errors.email}</span>
          </div>
           <div className="form-group clearfix fadeIn third recaptchaMain">
			<ReCAPTCHA
			ref={(r) => this.recaptcha = r}
			sitekey={Recaptchakey}
			grecaptcha={grecaptchaObject}
			onChange={this.captchaChange}
			/>
			<span className="text-danger">{errors.recaptcha}</span>
			</div>               
            <div className="form-group mb-4">
            <input type="submit" className="btn-block fadeIn fourth" value="Submit" />
          </div>
          </form>
          <div id="formFooter" className="fadeIn fourth">
            <h6 className="text-center mb-4">Not a registered user? <Link to="/Register">Register Now!</Link></h6>
            <h6 className="text-center">If you have lost your 2FA token, please <a href="#">email us</a> with the subject "Lost 2FA Device". GlobalCryptoX support will help you recover your account.</h6>
          </div>
          </div>
          <div className="tab-pane fade" id="nav-mobileLogin" role="tabpanel" aria-labelledby="nav-mobileLogin-tab">
          { this.state.otpver==''?
             <form id="" name="" className="stoForm landingForm" noValidate onSubmit={this.mobileSubmit} >
             <div className="form-group input-group mobNumber mb-5 mt-3">
                                        <label>Mobile Number</label>
										<PhoneInput
										placeholder="Enter phone number"
										country={'IN'}
										value={this.state.phone}
										onChange={phone => this.setState({ phone })}
										/>
										<span className="text-danger">{errors.phone}</span>
                                    </div>
              <div className="form-group clearfix fadeIn third recaptchaMain">
			<ReCAPTCHA
			ref={(r) => this.recaptcha = r}
			sitekey={Recaptchakey}
			grecaptcha={grecaptchaObject}
			onChange={this.captchaChange1}
			/>
			<span className="text-danger">{errors.mobrecaptcha}</span>
			</div>                       
            <div className="form-group mb-4">
            <input type="submit" className="btn-block fadeIn fourth" value="Submit" />
          </div>
          </form>
          :
          <form id="" name="" className="stoForm landingForm" noValidate onSubmit={this.resetSubmit} >
            <div className="form-group fadeIn second mb-5 mt-3">
            <label>New Password</label>
						<input
								onChange={this.onChange}
								value={this.state.resetpassword}
								error={errors.resetpassword}
								id="resetpassword"
								type="password"
								className={classnames("form-control", {
										invalid: errors.resetpassword
								})}
						/>
						<span className="text-danger">{errors.resetpassword}</span>
          </div>
          <div className="form-group fadeIn second mb-5 mt-3">
             <label>Confirm Password</label>
             <input
                 onChange={this.onChange}
                 value={this.state.password2}
                 error={errors.password2}
                 id="password2"
                 type="password"
                 className={classnames("form-control", {
                     invalid: errors.password2
                 })}
             />
             <span className="text-danger">{errors.password2}</span>
          </div>

           <div className="form-group fadeIn second mb-5 mt-3">
             <label>OTP Code</label>
             <input
                 onChange={this.onChange}
                 value={this.state.otpcode}
                 error={errors.otpcode}
                 id="otpcode"
                 type="password"
                 className={classnames("form-control", {
                     invalid: errors.otpcode
                 })}
             />
             <span className="text-danger">{errors.otpcode}</span>
          </div>
          { this.state.timer!=''?
          <center><p>m: {this.state.time.m} s: {this.state.time.s}</p></center>
          :''}
           { this.state.resend!=''?
          <center><p>No SMS reaceived? <a onClick={this.mobileSubmit1}>Resend otp</a></p></center>
          :''}
            <div className="form-group mb-4">
            <input type="submit" className="btn-block fadeIn fourth" value="Submit" />
          </div>
          </form>
      }
          <div id="formFooter" className="fadeIn fourth">
            <h6 className="text-center mb-4">Not a registered user? <Link to="/Register">Register Now!</Link></h6>
            <h6 className="text-center">If you have lost your 2FA token, please <a href="#">email us</a> with the subject "Lost 2FA Device". GlobalCryptoX support will help you recover your account.</h6>
          </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</header>
		</div>
	}
}





ForgotPassword.propTypes = {
    forgotUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { forgotUser,mobforgotUser,mobresetPassword }
)(withRouter(ForgotPassword));
