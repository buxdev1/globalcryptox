import React, { Component } from 'react'
import '../css/style-api.css';
import {Link} from 'react-router-dom';
import Logo from "../images/Logo-small.png"


class Api extends Component{
 state = { showing: true };
  render () {
   const { showing } = this.state;
    return (
      <div>
         <button id="nav-button" className="d-block d-md-none" onClick={() => this.setState({ showing: !showing })}><i class="fas fa-bars"></i> Menu</button>
      <div className="container-fluid">
         { showing
                    ?
        <div className="apiSidebarLeft deshHide">
      <a className="nav-brand ml-2" href="/"><img src={Logo} className="img-fluid" alt="" /></a>
      <div className="input-group mb-3 ml-2">
        <div className="input-group-prepend">
          <span className="input-group-text"><i className="fas fa-search"></i></span>
        </div>
        <input type="text" className="form-control" placeholder="Search" />
      </div>
    <nav id="myScrollspy">
      <ul className="nav nav-pills flex-column">
        <li className="nav-item">
          <a className="nav-link active" href="#section1" onClick={() => this.setState({ showing: !showing })}>Introduction</a>
        </li>

        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" onClick={() => this.setState({ showing: !showing })}>Getting Started</a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#section21" onClick={() => this.setState({ showing: !showing })}>Sign Up</a>
            <a className="dropdown-item" href="#section22" onClick={() => this.setState({ showing: !showing })}>Create an API Key</a>
            <a className="dropdown-item" href="#section23" onClick={() => this.setState({ showing: !showing })}>Authenticate</a>
            <a className="dropdown-item" href="#section24" onClick={() => this.setState({ showing: !showing })}>Minimize Latency</a>
          </div>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#section3" onClick={() => this.setState({ showing: !showing })}>HTTP API</a>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Public HTTP API Methods</a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#section41" onClick={() => this.setState({ showing: !showing })}>returnTicker</a>
            <a className="dropdown-item" href="#section42" onClick={() => this.setState({ showing: !showing })}>return24hVolume</a>
            <a className="dropdown-item" href="#section43" onClick={() => this.setState({ showing: !showing })}>returnOrderBook</a>

            <a className="dropdown-item" href="#section44" onClick={() => this.setState({ showing: !showing })}>returnTrades</a>
            <a className="dropdown-item" href="#section45" onClick={() => this.setState({ showing: !showing })}>returnCurrencies</a>
            <a className="dropdown-item" href="#section46" onClick={() => this.setState({ showing: !showing })}>products</a>
            <a className="dropdown-item" href="#section47" onClick={() => this.setState({ showing: !showing })}>trades</a>
            <a className="dropdown-item" href="#section48" onClick={() => this.setState({ showing: !showing })}>pairs</a>
            <a className="dropdown-item" href="#section49" onClick={() => this.setState({ showing: !showing })}>tickers</a>
            <a className="dropdown-item" href="#section50" onClick={() => this.setState({ showing: !showing })}>orderbook</a>
            <a className="dropdown-item" href="#section51" onClick={() => this.setState({ showing: !showing })}>historical_trades</a>
           {/* <a className="dropdown-item" href="#section52" onClick={() => this.setState({ showing: !showing })}>contracts</a>
            <a className="dropdown-item" href="#section53" onClick={() => this.setState({ showing: !showing })}>contract_specs</a>
            <a className="dropdown-item" href="#section54" onClick={() => this.setState({ showing: !showing })}>ticker</a> */}


          </div>
        </li>
      </ul>
    </nav>
  </div>
   : null
  }
  <div className="row">

    <div className="col-md-2 px-0 d-none d-md-block">
      <div className="apiSidebarLeft">
      <a className="nav-brand ml-2" href="/"><img src={Logo} className="img-fluid" alt="" /></a>
      <div className="input-group mb-3 ml-2">
        <div className="input-group-prepend">
          <span className="input-group-text"><i className="fas fa-search"></i></span>
        </div>
        <input type="text" className="form-control" placeholder="Search" />
      </div>
    <nav id="myScrollspy">
      <ul className="nav nav-pills flex-column">
        <li className="nav-item">
          <a className="nav-link active" href="#section1" onClick={() => this.setState({ showing: !showing })}>Introduction</a>
        </li>

        {/* <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Getting Started</a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#section21">Sign Up</a>
            <a className="dropdown-item" href="#section22">Create an API Key</a>
            <a className="dropdown-item" href="#section23">Authenticate</a>
            <a className="dropdown-item" href="#section24">Minimize Latency</a>
          </div>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#section3">HTTP API</a>
        </li> */}
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Public HTTP API Methods</a>
          <div className="dropdown-menu">
            <a className="dropdown-item" href="#section41">returnTicker</a>
            <a className="dropdown-item" href="#section42">return24hVolume</a>
            <a className="dropdown-item" href="#section43">returnOrderBook</a>
            <a className="dropdown-item" href="#section44">returnTrades</a>
            <a className="dropdown-item" href="#section45">returnCurrencies</a>
            <a className="dropdown-item" href="#section46">products</a>
            <a className="dropdown-item" href="#section47">trades</a>
            <a className="dropdown-item" href="#section48">pairs</a>
            <a className="dropdown-item" href="#section49">tickers</a>
            <a className="dropdown-item" href="#section50">orderbook</a>
            <a className="dropdown-item" href="#section51">historical_trades</a>
            { /*<a className="dropdown-item" href="#section52">contracts</a>
            <a className="dropdown-item" href="#section53">contract_specs</a>
            <a className="dropdown-item" href="#section54">ticker</a> */ }



          </div>
        </li>
      </ul>
    </nav>
  </div>
  </div>

    <div className="col-md-10 px-0">
      <div className="apiSecCommon" id="section1">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h2>Introduction</h2>
              <p>Globalcryptox provides HTTP api for interacting with the exchange. Its allow read access to public market data.</p>

            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <blockquote><p className="mb-0 py-2">Setup</p></blockquote>
              <pre className="highlight shell tab-shell">
                <code>
                  <span className="c"># make sure you have curl installed</span>
                </code>
              </pre>
            </div>
          </div>
        </div>
      </div>
      <div className="apiSecCommon" id="section2">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h2>Getting Started</h2>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
            </div>
          </div>
        </div>
      </div>
      <div className="apiSecCommon" id="section21">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>Sign Up</h4>
              <p>If you do not have a Globalcryptox account yet, use the button below to sign up.</p>
              <p><a href="/Register" className="btn btnapi1">Sign Up</a></p>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
            </div>
          </div>
        </div>
      </div>
      {/* <div className="apiSecCommon" id="section22">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>Create an API Key</h4>
              <p>Once you are verified and have an account, you can create an API Key.</p>
              <p>Enabling IP address restrictions for API keys is strongly recommended. Withdrawals are disabled by default and must be enabled on a per key basis.</p>
              <p>As the name implies, your secret must remain private! If you suspect your key has been compromised, immediately disable that key and generate a new one.</p>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
            </div>
          </div>
        </div>
      </div> */}
      {/* <div className="apiSecCommon" id="section23">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>Authenticate</h4>
              <aside className="info">TLS 1.2 or greater is required.</aside>
              <aside className="info">The public endpoint does not require API keys or nonces. So if you are only interested in the public endpoints, skip ahead <a href="#">here.</a></aside>
              <p>Private HTTP endpoints are authenticated using HMAC-SHA512 signed POST request.</p>
              <p>Private HTTP endpoints also require a nonce, which must be an integer greater than the previous nonce used. There is no requirement that nonces increase by a specific amount, so the current epoch time in milliseconds is an easy choice. As each API key has its own nonce tracking, using a different key for each client process can greatly simplify nonce management.</p>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
                <pre className="highlight shell tab-shell"><code><span className="c"># Find the HMAC-SHA512 signature of your POST parameters</span><br />
                  <span className="c"># using your secret key. Set the nonce to the current</span><br />
                  <span className="c"># milliseconds. (available with date +%s00000)</span><br />
                  <span className="nb">echo</span> -n <span className="s2">"command=returnBalances&amp;nonce=154264078495300"</span> | <span className="se">\</span><br />
                  openssl sha512 -hmac <span className="nv">$API_SECRET</span><br />
                  <span className="c"># You will use this signature as a header in your request.</span><br />
                  <span className="c"># For example:</span><br />
                   curl -X POST <span className="se">\</span><br />
                       -d <span className="s2">"command=returnBalances&amp;nonce=154264078495300"</span> <span className="se">\</span><br />
                       -H <span className="s2">"Key: 7BCLAZQZ-HKLK9K6U-3MP1RNV9-2LS1L33J"</span> <span className="se">\</span><br />
                       -H <span className="s2">"Sign: 2a7849ecf...ae71161c8e9a364e21d9de9"</span> <span className="se">\</span><br />
                       https://Globalcryptox.com/tradingApi
                  </code></pre>
            </div>
          </div>
        </div>
      </div>       */}
      {/* <div className="apiSecCommon" id="section24">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>Minimize Latency</h4>
              <p>If you will be performing high-frequency trading, you may wish to locate your bots as close to our servers as possible. As Globalcryptox uses Cloudflare for all requests, you can minimize network latency by positioning your client near the Cloudflare gateway in Dublin, Ireland. You can identify which Cloudflare gateway your client is accessing by running this command on the same machine as your bot:</p>
              <p><code>curl -s https://www.cloudflare.com/cdn-cgi/trace</code></p>
              <p>Cloudflare’s Dublin data center will return a “colo” field of “DUB”. If you get a different “colo” value, you can look up the location at <a href="https://www.cloudflarestatus.com" target="_blank">https://www.cloudflarestatus.com</a>.</p>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
            </div>
          </div>
        </div>
      </div> */}
      <div className="apiSecCommon" id="section3">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h2>HTTP API</h2>
              <p>The HTTP API allows read access to public market data through the public endpoint and read / write access to your private account via the private endpoint.</p>
              <ul>
                <li>Public HTTP Endpoint: <code>https://api.globalcryptox.com/v1/</code></li>
                {/* <li>Private HTTP Endpoint: <code>https://Globalcryptox.com/tradingApi</code></li> */}
              </ul>
              <p>Please note that there is a default limit of 6 calls per second. </p>
              <p>Making more than 6 calls per second to the API, or repeatedly and needlessly fetching excessive amounts of data, can result in rate limit. Please be careful.</p>
              <aside className="notice">If your account's volume is over $5 million in 30 day volume, you may be eligible for an API rate limit increase. Please email api-support@Globalcryptox.com.</aside>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
            </div>
          </div>
        </div>
      </div>
      <div className="apiSecCommon" id="section4">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h2>Public HTTP API Methods</h2>
              <p>The public HTTP API allows read access to public market data.</p>
              <aside className="notice">Public HTTP URL: `https://api.globalcryptox.com/v1/`</aside>
              <p>There are seven public methods, all of which take HTTP GET requests and return output in JSON format. No authentication is necessary but you must not excessively use any API endpoint.</p>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode">
            </div>
          </div>
        </div>
      </div>
      <div className="apiSecCommon" id="section41">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>publicapi?command=returnTicker</h4>
              <p>Retrieves summary information for each currency pair listed on the exchange. Fields include:</p>
              <div className="table-responsive">
              <table className="table"><thead>
                <tr>
                <th>Field</th>
                <th>Description</th>
                </tr>
                </thead><tbody>
                <tr>
                <td>id</td>
                <td>Id of the <a href="#currency-pair-ids">currency pair</a>.</td>
                </tr>
                <tr>
                <td>last</td>
                <td>Execution price for the most recent trade for this pair.</td>
                </tr>
                <tr>
                <td>lowestAsk</td>
                <td>Lowest current purchase price for this asset.</td>
                </tr>
                <tr>
                <td>highestBid</td>
                <td>Highest current sale price for this asset.</td>
                </tr>
                <tr>
                <td>percentChange</td>
                <td>Price change percentage.</td>
                </tr>
                <tr>
                <td>baseVolume</td>
                <td>Base units traded in the last 24 hours.</td>
                </tr>
                <tr>
                <td>quoteVolume</td>
                <td>Quoted units traded in the last 24 hours.</td>
                </tr>
                <tr>
                <td>isFrozen</td>
                <td>Indicates if this market is currently trading or not.</td>
                </tr>
                <tr>
                <td>high24hr</td>
                <td>The highest execution price for this pair within the last 24 hours.</td>
                </tr>
                <tr>
                <td>low24hr</td>
                <td>The lowest execution price for this pair within the last 24 hours.</td>
                </tr>
                </tbody></table>
              </div>
            </div>
          </div>

          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-4">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/publicapi?command=returnTicker"</span>
</code></pre>
              <blockquote><p className="py-3">Example output:</p></blockquote>


<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">BTC_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1589547268</span><span className="p">,</span><span className="w">
     </span><span className="err">last</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11362.69722340</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">lowestAsk</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">highestBid</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">percentChange</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.95161161</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">baseVolume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.07272800</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quoteVolume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">825.35724560</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">isFrozen</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">high</span><span className="mi">24</span><span className="err">hr</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11362.69722340</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">low</span><span className="mi">24</span><span className="err">hr</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11032.93529963</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span><span className="w">

  </span><span className="err">ETH_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1589540019</span><span className="p">,</span><span className="w">
     </span><span className="err">last</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">391.50900216</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">lowestAsk</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">highestBid</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">percentChange</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">-0.40803887</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">baseVolume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1.25000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quoteVolume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">489.38750000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">isFrozen</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">high</span><span className="mi">24</span><span className="err">hr</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">394.48354977</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">low</span><span className="mi">24</span><span className="err">hr</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">381.09562140</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span><span className="w">
</span><span className="err">...</span><span className="w">
</span></code></pre>


            </div>
          </div>
        </div>
      </div>


      <div className="apiSecCommon" id="section42">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>publicapi?command=return24hVolume</h4>
              <p>Returns the 24-hour volume for all markets as well as totals for primary currencies.</p>
              <p>Primary currencies include <code>BTC</code>, <code>ETH</code>, <code>USD</code> and show the total amount of those tokens that have traded within the last 24 hours.</p>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/publicapi?command=return24hVolume"</span>
</code></pre>
              <blockquote><p className="py-3">Example output:</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ETH_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1589540019</span><span className="p">,</span><span className="w">
     </span><span className="err">last</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">397.56877815</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">baseVolume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11.02260000</span><span className="err">'</span><span className="p">,</span><span className="w">

     </span><span className="err">quote_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">4374.29612600</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="err">BTC_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1589547268</span><span className="p">,</span><span className="w">
     </span><span className="err">last</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11634.76676175</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">baseVolume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.79718400</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">9262.09019030</span><span className="err">'</span><span className="p">,</span><span className="w">

     </span><br /><span className="p">},</span>
     <span className="w">


  </span>
</code></pre>
            </div>
          </div>
        </div>
      </div>



      <div className="apiSecCommon" id="section43">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>publicapi?command=returnOrderBook</h4>
              <p>Returns the order book for a given market, as well as a sequence number used by websockets for synchronization of book updates and an indicator specifying whether the market is frozen. You may set currencyPair to "all" to get the order books of all markets.</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full order book depth.</aside> */}
              <div className="table-responsive">
              <table className="table"><thead>
              <tr>
              <th>Request Parameter</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>currencyPair</td>
              <td>A pair like <code>BTC_ETH</code> or <code>all</code></td>
              </tr>
              <tr>
              <td>depth (optional)</td>
              <td>Default depth is <code>50</code>. Max depth is <code>100</code>.</td>
              </tr>
              </tbody></table>
            </div>
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>asks</td>
              <td>An array of price aggregated offers in the book ordered from low to high price.</td>
              </tr>
              <tr>
              <td>bids</td>
              <td>An array of price aggregated bids in the book ordered from high to low price.</td>
              </tr>
              <tr>
              <td>isFrozen</td>
              <td>Indicates if trading the market is currently disabled or not.</td>
              </tr>
              <tr>
              <td>seq</td>
              <td>An always-incrementing sequence number for this market.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/publicapi?command=returnOrderBook&amp;currencyPair=BTC_USD&amp;depth=10"</span></code></pre>
              <blockquote><p className="py-3">Example output for a selected market:</p></blockquote>
              <pre className="highlight json tab-json"><code><span className="p">&#123;</span><span className="w"> </span><span className="err">asks</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11389.90000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.11019000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11490.89000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.26566000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11500.30000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.19000000</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11510.10000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.21819000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11520.90000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.07282700</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
  </span><br /><span className="err">bids</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11350.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.02620000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11000.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.04560000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10920.35000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.25660000</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10820.45000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.05269000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10784.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.11700000</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
  </span><br /><span className="err">isFrozen</span><span className="p">:</span><span className="w"> </span><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
  </span><br /><span className="err">seq</span><span className="p">:</span><span className="w"> </span><span className="mi">595100792</span><span className="w"> </span><span className="p">}</span><span className="w">
</span></code></pre>
              {/* <blockquote><p className="py-3">Example output for all markets:</p></blockquote> */}
              {/* <pre className="highlight json tab-json"><code><span className="p">&#123;</span><span className="w"> </span><span className="err">BTC_ETH</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">&#123;</span><span className="w"> </span><span className="err">asks</span><span className="p">:</span><span className="w">
      </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.03143500</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">46.84591041</span><span className="w"> </span><span className="p">],</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.03144000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">100.086388</span><span className="w"> </span><span className="p">],</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.03144865</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">6.01683252</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.03132669</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.01619218</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="err">isFrozen</span><span className="p">:</span><span className="w"> </span><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><br /><span className="err">seq</span><span className="p">:</span><span className="w"> </span><span className="mi">130962406</span><span className="w"> </span><span className="p">},</span><span className="w">
  </span><br /><span className="err">BTC_LTC</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">&#123;</span><span className="w"> </span><span className="err">asks</span><span className="p">:</span><span className="w">
      </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.00812000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">6.82726987</span><span className="w"> </span><span className="p">],</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.00812253</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">6.6911383</span><span className="w"> </span><span className="p">],</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">0.00812500</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">84.1323</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">1.06900000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.0162</span><span className="w"> </span><span className="p">],</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">1.06800000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.0162</span><span className="w"> </span><span className="p">],</span><span className="w">
        </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">1.06700000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.0162</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="err">isFrozen</span><span className="p">:</span><span className="w"> </span><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><br /><span className="err">seq</span><span className="p">:</span><span className="w"> </span><span className="mi">51055117</span><span className="w"> </span><span className="p">}</span><span className="w"> </span><span className="p">}</span><span className="w">
</span></code></pre> */}
            </div>
          </div>
        </div>
      </div>

      <div className="apiSecCommon" id="section44">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>publicapi?command=returnTrades</h4>
              <p>Returns the Trade data for a given market, as well as a sequence number used by websockets for synchronization of book updates and an indicator specifying whether the market is frozen. You may set currencyPair to "all" to get the order books of all markets.</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside> */}
              <div className="table-responsive">
              <table className="table"><thead>
              <tr>
              <th>Request Parameter</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>market_pair</td>
              <td>A pair like <code>BTC_USD</code> or <code>all</code></td>
              </tr>
              <tr>
              <td>depth (optional)</td>
              <td>Default depth is <code>50</code>. Max depth is <code>100</code>.</td>
              </tr>
              </tbody></table>
            </div>
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>trade_id</td>
              <td>The id of the trade .</td>
              </tr>
              <tr>
              <td>timestamp</td>
              <td>The timestatmp for the trade.</td>
              </tr>
              <tr>
              <td>price</td>
              <td>The price for the Trade.</td>
              </tr>
              <tr>
              <td>base_volume</td>
              <td>The base volume of the Trade.</td>
              </tr>
              <tr>
              <td>quote_volume</td>
              <td>The Qouted volume of the Trade.</td>
              </tr>
              <tr>
              <td>type</td>
              <td>The Type of the Trade.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/publicapi?command=returnTrades&amp;currencyPair=BTC_USD"</span></code></pre>
              <blockquote><p className="py-3">Example output for a selected market:</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">BTC_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">trade_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592058724</span><span className="p">,</span><span className="w">
     </span><span className="err">timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1592058703912</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">9444.53000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.01000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">94.44530000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">type</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">sell</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="err">ETH_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">trade_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592378730</span><span className="p">,</span><span className="w">
     </span><span className="err">timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1592341583418</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">240.45000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.01465855</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">3.52464835</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">type</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">sell</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>



      <div className="apiSecCommon" id="section45">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>publicapi?command=returnCurrencies</h4>
              <p>Returns the Currencies that are used</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside> */}

            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>id</td>
              <td>The id of the Currency .</td>
              </tr>
              <tr>
              <td>name</td>
              <td>The name for the Currency.</td>
              </tr>
              <tr>
              <td>txFee</td>
              <td>The Transaction fee  of the Currency.</td>
              </tr>
              <tr>
              <td>minConf</td>
              <td>.</td>
              </tr>
              <tr>
              <td>currencyType</td>
              <td>The type of the Currency.</td>
              </tr>
              <tr>
              <td>depositAddress</td>
              <td>The deposit Address for the currency.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/publicapi?command=returnCurrencies"</span></code></pre>
              <blockquote><p className="py-3">Example output :</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">BTC</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1579858076</span><span className="p">,</span><span className="w">
     </span><span className="err">name</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">Bitcoin</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">txFee</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.0005</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">minConf</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">currencyType</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">address</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">depositAddress</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">null</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="err">USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592047745</span><span className="p">,</span><span className="w">
     </span><span className="err">name</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">TetherUSD</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">txFee</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">minConf</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">currencyType</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">address</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">depositAddress</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">null</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>



      <div className="apiSecCommon" id="section46">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>products</h4>
              <p>Returns the Currencies that are used</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside> */}

            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>id</td>
              <td>The id of the Currency Pair.</td>
              </tr>
              <tr>
              <td>fromSymbol</td>
              <td>The From Currency Symbol.</td>
              </tr>
              <tr>
              <td>toSymbol</td>
              <td>The To Currency Symbol.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/products"</span></code></pre>
              <blockquote><p className="py-3">Example output :</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">ETH-USD</span><span className="p">,</span><span className="w">
     </span><span className="err">fromSymbol</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">ETH</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">toSymbol</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">BTC-USD</span><span className="p">,</span><span className="w">
     </span><span className="err">fromSymbol</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">BTC</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">toSymbol</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <span className="p">}</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>

      <div className="apiSecCommon" id="section47">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>trades</h4>
              <p>Returns the Trade that have been Completed</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside> */}
              <div className="table-responsive">
              <table className="table"><thead>
              <tr>
              <th>Request Parameter</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>pair</td>
              <td>A pair like <code>BTC-USD</code> or <code>all</code></td>
              </tr>
              <tr>
              <td>limit</td>
              <td>Default depth is <code>10</code>. Max depth is <code>100</code>.</td>
              </tr>
              </tbody></table>
            </div>

            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>id</td>
              <td>The id of the Trade.</td>
              </tr>
              <tr>
              <td>timestamp</td>
              <td>The timestatmp of the Trade.</td>
              </tr>
              <tr>
              <td>price</td>
              <td>The price for the Trade.</td>
              </tr>
              <tr>
              <td>quantity</td>
              <td>The quantity in the Trade.</td>
              </tr>
              <tr>
              <td>type</td>
              <td>The type of the Trade.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/trades?pair=BTC-USD&limit=10"</span></code></pre>
              <blockquote><p className="py-3">Example output :</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592058724</span><span className="p">,</span><span className="w">
     </span><span className="err">timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1592058703912</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">9444.53000000</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">quantity</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.01000000</span><span className="err">'</span><span className="p">,</span><br />
     <span className="err">type</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">sell</span><span className="err">'</span><span className="p">,</span><br />
     <span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592371758</span><span className="p">,</span><span className="w">
     </span><span className="err">timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1592341583418</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">9145.27000000</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">quantity</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.02132000</span><span className="err">'</span><span className="p">,</span><br />
     <span className="err">type</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">buy</span><span className="err">'</span><span className="p">,</span><br />
     <span className="p">},</span>
     <span className="w">


  </span>
  <span className="p">}</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>


      <div className="apiSecCommon" id="section48">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>pairs</h4>
              <p>Returns the pairs list</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside> */}

            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>ticker_id</td>
              <td>The tickerroot of the pair.</td>
              </tr>
              <tr>
              <td>base</td>
              <td>The base of the pair.</td>
              </tr>
              <tr>
              <td>target</td>
              <td>The target of the pair.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/pairs"</span></code></pre>
              <blockquote><p className="py-3">Example output :</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">ETH-USD</span><span className="p">,</span><span className="w">
     </span><span className="err">base</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">ETH</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">target</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">BTC-USD</span><span className="p">,</span><span className="w">
     </span><span className="err">base</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">BTC</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">target</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <span className="p">}</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>


      <div className="apiSecCommon" id="section49">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>tickers</h4>
              <p>Returns the tickers data that are used</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside>
             */}
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>ticker_id</td>
              <td>The ticker id of the pair .</td>
              </tr>
              <tr>
              <td>base_currency</td>
              <td>The from currency in the pair.</td>
              </tr>
              <tr>
              <td>target_currency</td>
              <td>The to currencyin the pair.</td>
              </tr>
              <tr>
              <td>last_price</td>
              <td>The last price of the pair</td>
              </tr>
              <tr>
              <td>base_volume</td>
              <td>The base volume  of the pair.</td>
              </tr>
              <tr>
              <td>target_volume</td>
              <td>The target volume of the pair</td>
              </tr>
              <tr>
              <td>bid</td>
              <td>The bid for the pair.</td>
              </tr>
              <tr>
              <td>ask</td>
              <td>The asking price of the pair.</td>
              </tr>
              <tr>
              <td>high</td>
              <td>The high price of the pair.</td>
              </tr>
              <tr>
              <td>low</td>
              <td>The low price of the pair.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>


          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/tickers"</span></code></pre>
              <blockquote><p className="py-3">Example output :</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">ETH_USD</span><span className="p">,</span><span className="w">
     </span><span className="err">base_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">ETH</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">target_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">last_price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">391.50900216</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">4.82260000</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">target_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1888.09612600</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">bid</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">ask</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span>


     <span className="w">
     </span><span className="err">high</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">394.48354977</span><span className="err">'</span><span className="p">,</span>


     <span className="w">
     </span><span className="err">low</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">381.09562140</span><span className="err">'</span><span className="p">,</span>




     <br /><span className="p">},</span>
     <span className="w">


  </span>
  <br />
   <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">BTC_USD</span><span className="p">,</span><span className="w">
     </span><span className="err">base_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">BTC</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">target_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">last_price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11362.69722340</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.24888900</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">target_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">2827.02184030</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">bid</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span>

     <span className="w">
     </span><span className="err">ask</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.00000000</span><span className="err">'</span><span className="p">,</span>


     <span className="w">
     </span><span className="err">high</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11362.69722340</span><span className="err">'</span><span className="p">,</span>


     <span className="w">
     </span><span className="err">low</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11032.93529963</span><span className="err">'</span><span className="p">,</span>
     <br /><span className="p">},</span>
  <span className="p">}</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>


      <div className="apiSecCommon" id="section50">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>orderbook</h4>
              <p>Returns the order book for a given market, as well as a sequence number used by websockets for synchronization of book updates and an indicator specifying whether the market is frozen. You may set currencyPair to "all" to get the order books of all markets.</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full order book depth.</aside> */}
              <div className="table-responsive">
              <table className="table"><thead>
              <tr>
              <th>Request Parameter</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>ticker_id</td>
              <td>A pair like <code>BTC_USD</code> or <code>all</code></td>
              </tr>
              <tr>
              <td>depth (optional)</td>
              <td>Default depth is <code>50</code>. Max depth is <code>100</code>.</td>
              </tr>
              </tbody></table>
            </div>
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>asks</td>
              <td>An array of price aggregated offers in the book ordered from low to high price.</td>
              </tr>
              <tr>
              <td>bids</td>
              <td>An array of price aggregated bids in the book ordered from high to low price.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/orderbook?ticker_id=BTC_USD&depth=200"</span></code></pre>
              <blockquote><p className="py-3">Example output for a selected market:</p></blockquote>
              <pre className="highlight json tab-json"><code><span className="p">&#123;</span><span className="w"> </span><span className="err">asks</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11389.90000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.11019000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11490.89000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.26566000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11500.30000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.19000000</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11510.10000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.21819000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11520.90000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.07282700</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
  </span><br /><span className="err">bids</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11350.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.02620000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11000.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.04560000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10920.35000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.25660000</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10820.45000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.05269000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10784.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.11700000</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
  </span><br /><span className="p">}</span><span className="w">
</span></code></pre>

            </div>
          </div>
        </div>
      </div>


      <div className="apiSecCommon" id="section51">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>historical_trades</h4>
              <p>Returns the  Historical Trade data for a given market, as well as a sequence number used by websockets for synchronization of book updates and an indicator specifying whether the market is frozen. You may set currencyPair to "all" to get the order books of all markets.</p>
              {/* <aside className="notice">Consider using the <a href="#websocket-api">Websocket API</a> over HTTP if you are looking for fresh and full trade data depth.</aside> */}
              <div className="table-responsive">
              <table className="table"><thead>
              <tr>
              <th>Request Parameter</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>ticker_id</td>
              <td>A pair like <code>BTC_USD</code> or <code>all</code></td>
              </tr>
              <tr>
              <td>limit </td>
              <td>Default limit is <code>10</code>. Max depth is <code>100</code>.</td>
              </tr>
              <tr>
              <td>type </td>
              <td>The type of trade, buy or sell.</td>
              </tr>
              </tbody></table>
            </div>
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>trade_id</td>
              <td>The id of the trade .</td>
              </tr>
              <tr>
              <td>trade_timestamp</td>
              <td>The timestatmp for the trade.</td>
              </tr>
              <tr>
              <td>price</td>
              <td>The price for the Trade.</td>
              </tr>
              <tr>
              <td>base_volume</td>
              <td>The base volume of the Trade.</td>
              </tr>
              <tr>
              <td>target_volume</td>
              <td>The Qouted volume of the Trade.</td>
              </tr>
              <tr>
              <td>type</td>
              <td>The Type of the Trade.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/historical_trades?ticker_id=BTC_USD&limit=10&type=sell"</span></code></pre>
              <blockquote><p className="py-3">Example output for a selected market:</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">BTC_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">trade_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592058724</span><span className="p">,</span><span className="w">
     </span><span className="err">trade_timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1592058703912</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">9444.53000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.01000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">target_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">94.44530000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">type</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">sell</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="err">ETH_USD</span><span className="p">:</span><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">trade_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">1592378730</span><span className="p">,</span><span className="w">
     </span><span className="err">trade_timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1592341583418</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">240.45000000</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0.01465855</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">target_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">3.52464835</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">type</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">sell</span><span className="err">'</span><span className="w"> </span><br /><span className="p">},</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>

{/*
      <div className="apiSecCommon" id="section52">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>contracts</h4>
              <p>Retrieves summary information of the contracts. Fields include:</p>
              <div className="table-responsive">
              <table className="table"><thead>
                <tr>
                <th>Field</th>
                <th>Description</th>
                </tr>
                </thead><tbody>
                <tr>
                <td>ticker_id</td>
                <td>Id of the <a href="#currency-pair-ids">contracts</a>.</td>
                </tr>
                <tr>
                <td>base_currency</td>
                <td>from currency of the contract.</td>
                </tr>
                <tr>
                <td>quote_currency</td>
                <td>to currency of the contract.</td>
                </tr>
                <tr>
                <td>last_price</td>
                <td>last price of the contract .</td>
                </tr>
                <tr>
                <td>base_volume</td>
                <td>base volume of the contract.</td>
                </tr>
                <tr>
                <td>quote_volume</td>
                <td>qoute volume of the contract.</td>
                </tr>
                <tr>
                <td>bid</td>
                <td>bid for the contract.</td>
                </tr>
                <tr>
                <td>ask</td>
                <td>asking price for the contract</td>
                </tr>
                <tr>
                <td>high</td>
                <td>highest price of the contract</td>
                </tr>
                <tr>
                <td>low</td>
                <td>lowest price of the contract.</td>
                </tr>

                <tr>
                <td>product_type</td>
                <td>type of contract .</td>
                </tr>

                <tr>
                <td>open_interest</td>
                <td>open interest of the contract.</td>
                </tr>

                <tr>
                <td>index_price</td>
                <td>The index price of the contract.</td>
                </tr>

                <tr>
                <td>funding_rate</td>
                <td>The funding price of the contract.</td>
                </tr>

                <tr>
                <td>next_funding_rate_timestamp</td>
                <td>The next funding  timestamp.</td>
                </tr>
                </tbody></table>
              </div>
            </div>
          </div>

          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-4">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/contracts"</span>
</code></pre>
              <blockquote><p className="py-3">Example output:</p></blockquote>


<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />

   <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">BTC-USD</span><span className="p">,</span><span className="w">
     </span><span className="err">base_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">BTC</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">last_price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11413.278018419194</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">19.3615</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">bid</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">ask</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">high</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11420.675442221043</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">low</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11032.935299634943</span><span className="err">'</span><span className="w"> </span>

     <span className="w">
     </span><span className="err">product_type</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">Perpetual</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">open_interest</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">index_price</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11585.957396608863</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">funding_rate</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">null</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">next_funding_rate_timestamp</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">8</span><span className="err">'</span><span className="p">,</span>

     <br /><span className="p">},</span><span className="w">

  </span>
  <span className="p">&#123;</span><span className="w"> </span><br /><span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="mi">BTC-USD</span><span className="p">,</span><span className="w">
     </span><span className="err">base_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">BTC</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">last_price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11413.278018419194</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">base_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">19.3615</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">quote_volume</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">bid</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">ask</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mi">0</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">high</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11420.675442221043</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">low</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11032.935299634943</span><span className="err">'</span><span className="w"> </span>

     <span className="w">
     </span><span className="err">product_type</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">Perpetual</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">open_interest</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">0</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">index_price</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11585.957396608863</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">funding_rate</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">null</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><span className="err">next_funding_rate_timestamp</span><span className="mi"></span><span className="err"></span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">8</span><span className="err">'</span><span className="p">,</span>
     <br /><span className="p">},</span><span className="w">
     <br /><span className="p">}</span><span className="w">

</span>
  </span>

  </code></pre>


            </div>
          </div>
        </div>
      </div>

      <div className="apiSecCommon" id="section53">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>contract_specs</h4>
              <p>Returns the contract specifications</p>
             
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>contract_type</td>
              <td>Type of the contract.</td>
              </tr>
              <tr>
              <td>contract_price</td>
              <td>The price of the contract.</td>
              </tr>
              <tr>
              <td>contract_price_currency</td>
              <td>The currency of the contract.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/contract_specs
"</span></code></pre>
              <blockquote><p className="py-3">Example output :</p></blockquote>

<pre className="highlight json tab-json"><code><span className="err">...</span><br />
                <span className="w"> </span><br />
                <span className="p">&#123;</span><span className="w"> </span><br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">contract_type</span><span className="p">:</span><span className="w"> </span><br /><span className="mf">Inverse</span><span className="p">,</span><span className="w">
     </span><span className="err">contract_price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">11601.24169070</span><span className="err">'</span><span className="p">,</span><span className="w">

     </span><span className="err">contract_price_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span><br />
     <span className="p">},</span>
     <span className="w">


  </span>
  <br /><span className="w">
   </span><span className="p">&#123;</span><span className="w"> </span><br /><span className="err">contract_type</span><span className="p">:</span><span className="w"> </span><br /><span className="mf">Inverse</span><span className="p">,</span><span className="w">
     </span><span className="err">contract_price</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">399.97355874</span><span className="err">'</span><span className="p">,</span><span className="w">
     </span><span className="err">contract_price_currency</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">USD</span><span className="err">'</span><span className="p">,</span>
     <span className="w">
     </span><br />
     <span className="p">},</span>
     <span className="w">


  </span>
  <span className="p">}</span>
     <span className="w">


  </span>
</code></pre>

            </div>
          </div>
        </div>
      </div>


      <div className="apiSecCommon" id="section54">
        <div className="row mx-0">
          <div className="col-md-6 flexColumn px-0">
            <div className="apiContent">
              <h4>ticker</h4>
              <p>Returns the contracts biding and asking pricess.</p>
              
              <div className="table-responsive">
              <table className="table"><thead>
              <tr>
              <th>Request Parameter</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>ticker_id</td>
              <td>A pair like <code>BTCUSD</code> or <code>all</code></td>
              </tr>
              <tr>
              <td>depth</td>
              <td>Default depth is <code>100</code>. Max depth is <code>100</code>.</td>
              </tr>
              </tbody></table>
            </div>
            <div className="table-responsive mt-4">
              <table className="table"><thead>
              <tr>
              <th>Field</th>
              <th>Description</th>
              </tr>
              </thead><tbody>
              <tr>
              <td>ticker_id</td>
              <td>An id of the contract.</td>
              </tr>
              <tr>
              <td>timestamp</td>
              <td>An timestamp of the contract.</td>
              </tr>
              <tr>
              <td>asks</td>
              <td>An array of price aggregated offers in the book ordered from low to high price.</td>
              </tr>
              <tr>
              <td>bids</td>
              <td>An array of price aggregated bids in the book ordered from high to low price.</td>
              </tr>
              </tbody></table>
            </div>
            </div>
          </div>
          <div className="col-md-6 flexColumn px-0">
            <div className="apiCode pt-5">
              <pre className="highlight shell tab-shell"><code>curl <span className="s2">"https://api.globalcryptox.com/v1/ticker?ticker_id=BTCUSD&depth=100"</span></code></pre>
              <blockquote><p className="py-3">Example output for a selected market:</p></blockquote>
              <pre className="highlight json tab-json"><code><span className="p">&#123;</span><span className="w"> </span>
              <span className="err">ticker_id</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">BTCUSD</span><span className="err">'</span><span className="p">,</span>
              <span className="err">timestamp</span><span className="p">:</span><span className="w"> </span><br /><span className="err">'</span><span className="mf">1596634401435</span><span className="err">'</span><span className="p">,</span>
              <span className="err">asks</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11389.90000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.11019000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11490.89000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.26566000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11500.30000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.19000000</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11510.10000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.21819000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11520.90000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.07282700</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
  </span><br /><span className="err">bids</span><span className="p">:</span><span className="w">
   </span><br /><span className="p">[</span><span className="w"> </span><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11350.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.02620000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">11000.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.04560000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10920.35000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.25660000</span><span className="w"> </span><span className="p">],</span><span className="w">
</span><br /><span className="err">...</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10820.45000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.05269000</span><span className="w"> </span><span className="p">],</span><span className="w">
     </span><br /><span className="p">[</span><span className="w"> </span><span className="err">'</span><span className="mf">10784.50000000</span><span className="err">'</span><span className="p">,</span><span className="w"> </span><span className="mf">0.11700000</span><span className="w"> </span><span className="p">]</span><span className="w"> </span><span className="p">],</span><span className="w">
  </span><br /><span className="p">}</span><span className="w">
</span></code></pre>

            </div>
          </div>
        </div>
      </div>
*/}


    </div>
  </div>
</div>
      </div>
    );
  }
}

export default Api
