import React from 'react';
import { MDBDataTable } from 'mdbreact';

const DatatablePage = () => {
  const data = {
    columns: [
      {
        label: 'Pair',
        field: 'pair',
        sort: 'asc'
      },
      {
        label: 'Max Leverage',
        field: 'maxLeverage',
        sort: 'asc'
      },
      {
        label: 'Last Price',
        field: 'lastPrice',
        sort: 'asc'
      },
      {
        label: '24H Change',
        field: 'hChange',
        sort: 'asc'
      },
      {
        label: '24H Volume',
        field: 'hVolume',
        sort: 'asc'
      }
    ],
    rows: [
      {
        pair: 'BTC/USD',
        maxLeverage: '100x',
        lastPrice: '8450 USD',
        hChange: '+5.25%',
        hVolume: '15450 USD'
      },
      {
        pair: 'BTC/INR',
        maxLeverage: '100x',
        lastPrice: '6057254 INR',
        hChange: '+5.21%',
        hVolume: '55057254 INR'
      },
      {
       pair: 'ETH/BTC',
        maxLeverage: '100x',
        lastPrice: '0.02256587 BTC',
        hChange: '+5.25%',
        hVolume: '15450 USD'
      },
      {
        pair: 'BTC/USD',
        maxLeverage: '100x',
        lastPrice: '8450 USD',
        hChange: '-2.25%',
        hVolume: '5.6985 BTC'
      },
      {
        pair: 'LTC/BTC',
        maxLeverage: '90x',
        lastPrice: '0.00253265 BTC',
        hChange: '-5.21%',
        hVolume: '12.3265 BTC'
      },
      {
        pair: 'EOS/BTC',
        maxLeverage: '90x',
        lastPrice: '0.00003265 BTC',
        hChange: '+0.25%',
        hVolume: '48.2356 BTC'
      }
    ]
  };

  return (
    <MDBDataTable
      striped
      bordered
      hover
      data={data}
    />
  );
}

export default DatatablePage;