
/* eslint-disable */

import React, { Component } from 'react'
import '../css/style-trade.css';
// import '../js/main.js';

import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import Logo from "../images/Logo-small.png"
import Selicon1 from "../images/selicon1.png"
import Selicon2 from "../images/selicon2.png"
import Selicon4 from "../images/selicon4.png"
import Selicon5 from "../images/selicon5.png"
import Selicon6 from "../images/selicon6.png"

import { getPertual,orderPlacing,getTradeData,getuserTradeData,cancelTrade,logoutUser,getPricevalue,triggerstop,changeopenpositions } from "../actions/authActions";
import * as moment from "moment";

import GreenPinkDot from "../images/btn-green-pink-dot.png"
import PinkDot from "../images/btn-pink-dot.png"
import GreenDot from "../images/btn-green-dot.png"
import TradeChart from "../images/tradeChart.jpg"
import Slider, { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';

import axios from "axios";
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { store } from 'react-notifications-component';
import {Modal,Button} from 'react-bootstrap/';
import Select from 'react-select';
import io from "socket.io-client";
import { Scrollbars } from 'react-custom-scrollbars';
import { TVChartContainer } from './TVChartContainer';
import keys from "../actions/config";

const url = keys.baseUrl;

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: '1px dotted pink',
    color: state.isSelected ? 'red' : 'blue',
    padding: 20,
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';

    return { ...provided, opacity, transition };
  }
}

const options1 = [
  { value: 'Beshare', label: 'Beshare' },
  { value: '3Commas', label: '3Commas' },
  { value: 'AIcoin', label: 'AIcoin' },
  { value: 'alpha-algo', label: 'alpha-algo' },
  { value: 'Cornix', label: 'Cornix' },
  { value: '合约帝', label: '合约帝' },
];

const options = [
  { value: 'GoodTillCancelled', label: 'GoodTillCancelled' },
  { value: 'ImmediateOrCancel', label: 'ImmediateOrCancel' },
  { value: 'FillOrKill', label: 'FillOrKill' },
];



class Trade extends Component{

  constructor(props) {

    super(props);
    //console.log('typeof this.props.match.params.currency');
   // console.log(typeof this.props.match.params.currency);
    if(typeof this.props.match.params.currency=='undefined'){
      var currency = 'BTC-USD';
    } else {
      var currency = this.props.match.params.currency;
      localStorage.setItem('curpair',this.props.match.params.currency)
    }
    // alert(currency);
    currency = currency.toUpperCase();

    var date              = new Date();
    var curhour           = date.getHours();
    var timeuntillfunding = (curhour>0 && curhour<8)?(8-curhour):(curhour>8 && curhour<16)?(16-curhour):(curhour>16 && curhour<24)?(24-curhour):0;

    this.state = {
      value                : (currency=='BTC-USD')?150:50,
      dummyleverage        : 10,
      curcontract          : currency,
      chartpair            : currency.replace("-", ""),
      secondcurrency       : currency.split('-')[1],
      firstcurrency        : currency.split('-')[0],
      chartpair            : currency.replace("-", ""),
      price                : 7800,
      instantprice         : 7800,
      index_price          : 7800,
      rescentcount         : 50,
      markprice            : 0,
      quantity             : 0.00000000,
      order_value          : 0.00000000,
      order_cost           : 0.00000000,
      btcprice             : 0,
      balance              : 0,
      last_price           : 0,
      maxquantity          : 0,
      minquantity          : 0,
      minmargin            : 0,
      close_quantity       : 0,
      btcvolume            : 0,
      close_price          : 0,
      prevoiusprice        : 0,
      maxleverage          : 0,
      floating             : (currency=='XRP-USD')?4:2,
      taker_fee            : 0.075,
      maker_fee            : 0.075,
      disable              : true,
      buydisable           : true,
      selldisable          : true,
      readOnly             : false,
      records              : [],
      assetOverview        : [],
      tradeTableAll        : [],
      buyOrder             : [],
      prevbuyOrder         : [],
      sellOrder            : [],
      prevsellOrder        : [],
      orderHistory         : [],
      Rescentorder         : [],
      Histroydetails       : [],
      position_details     : [],
      allposition_details  : [],
      closed_positions     : [],
      Conditional_details  : [],
      Filleddetails        : [],
      lastpricedet         : "",
      daily_details        : [],
      errors               : {},
      spotpricecolor       : 'greenText',
      orderbookshow        : 'greenpink',
      ordertype            : 'Market',
      triggerordertype     : 'limit',
      theme                : 'Dark',
      limit                : false,
      conditional          : false,
      reduce_only          : false,
      post_only            : false,
      show                 : false,
      tpshow               : false,
      show1                : false,
      tpreadOnly           : true,
      trailreadOnly        : true,
      stopreadOnly         : true,
      readOnly1            : true,
      update               : true,
      timeinforcetype      : {value:'GoodTillCancelled',label:'GoodTillCancelled'},
      perpetual            : '',
      username             : '',
      message              : '',
      trailingstopdistance : 0,
      alertmessage         : '',
      TradingView          : '',
      messages             : [],
      Liqprice             : 0,
      takeprofit           : 0,
      stopprice            : 0,
      stopper              : 0,
      tptrigger_type       : null,
      stoptrigger_type     : null,
      blocktime            : '',
      change               : 0,
      buylimit             : 0,
      selllimit             : 0,
      timeuntillfunding    : timeuntillfunding,
      buyorsell            : 'buy',
      alertmessage1        : '',
      alertmessage2        : '',

    };

    // this.socket = io(keys.socketUrl,{secure: true,transports:['polling'],jsonp:false,rejectUnauthorized:false}); //live
    this.socket = io(keys.socketUrl);

    this.socket.on('RECEIVE_MESSAGE', function(data){
      console.log(data);
        addMessage(data);
    });

    this.socket.on('TRADE', function(data){
        console.log(data,'orderPlacing');
        getPricevalue();
        // if( data.contractdetails !== undefined && data.contractdetails.first_currency !== undefined &&  data.contractdetails.first_currency==this.state.first_currency)
        // {
          appenddata(data);
        // }
    });

    this.socket.on('USERTRADE', function(data){
        console.log(data,'userddata');
        // let userid = this.props.auth.user.id;
        userappenddata(data);
    });

    this.socket.on('PRICEDETAILS', function(data){
      console.log(data,'PRICEDETAILS');
        priceappenddata(data);
    });

    this.socket.on('NOTIFICATION', function(data){
          store.addNotification({
          title: "Wonderful!",
          message: data,
          type: "success",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
          duration: 2000,
          onScreen: true
          }
          });
    });

    const appenddata = data => {
       this.setState({prevbuyOrder:this.state.buyOrder,prevsellOrder:this.state.sellOrder});
        if(data.message=='tradeTableAll' && data.buyOrder !== undefined && data.sellOrder !== undefined  && data.contractdetails !== undefined && data.Rescentorder !== undefined){
              console.log('insideifffffffff')
              console.log(data.contractdetails.first_currency)
              console.log(this.state.first_currency)
            if(data.contractdetails.first_currency==this.state.firstcurrency)
            {
              console.log('insideif')
                    var assetdetails    = data.assetdetails;
                    var contractdetails = data.contractdetails;

                    var buyOrder        = data.buyOrder;
                    var sellOrder       = data.sellOrder;
                    var Rescentorder    = data.Rescentorder;

                    this.setState({
                      buyOrder      : buyOrder,
                      sellOrder     : sellOrder,
                      Rescentorder  : Rescentorder,
                      buylimit      : (buyOrder.length>0)?buyOrder[0]._id:this.state.markprice,
                      selllimit     : (sellOrder.length>0)?sellOrder[sellOrder.length-1]._id:this.state.markprice,
                      mainmargin    : (contractdetails.maint_margin!=null?contractdetails.maint_margin/100:0),
                    });
              }


          }
    };


    const priceappenddata = data => {
      if(data && data.tiker_root=='BTCUSD')
      {
        this.setState({btcprice:parseFloat(data.markprice).toFixed(2)})
      }
     if(data && (data.tiker_root.replace("USDT", "USD")==this.state.chartpair || data.tiker_root==this.state.chartpair))
      {
        var floating = (this.state.chartpair=='XRPUSD')?4:2;
        this.setState({markprice:parseFloat(data.markprice).toFixed(floating),last_price:data.last});
        if(this.state.prevoiusprice!=0 && this.state.prevoiusprice < data.markprice )
        {
            this.setState({spotpricecolor:"greenText"});
        }
        else
        {
            this.setState({spotpricecolor:"pinkText"});
        }
            this.setState({prevoiusprice:this.state.markprice,floating:floating});
      }
    }
    const userappenddata = data => {
        if(data.message=='tradeTableAll' && data.orderHistory !== undefined && data.assetdetails !== undefined && data.Conditional_details !== undefined && data.position_details !== undefined  && data.daily_details !== undefined  && data.closed_positions !== undefined ){

               var orderHistory        = data.orderHistory;
               var Histroydetails      = data.Histroydetails;
               var Filleddetails       = data.Filleddetails;
               // var lastpricedet        = data.lastpricedet;
               var assetdetails        = data.assetdetails;
               var Conditional_details = data.Conditional_details;
               var position_details    = data.position_details;
               var allposition_details    = data.allposition_details;
               var daily_details       = data.daily_details;
               var closed_positions    = data.closed_positions;

                this.setState({
                  orderHistory        : orderHistory,
                  Histroydetails      : Histroydetails,
                  // lastpricedet        : lastpricedet,
                  position_details    : position_details,
                  Filleddetails       : Filleddetails,
                  assetOverview       : assetdetails,
                  Conditional_details : Conditional_details,
                  allposition_details : allposition_details,
                  daily_details       : (daily_details.length>0)?daily_details:[],
                  // instantprice        : (lastpricedet)?lastpricedet.price:0,
                  closed_positions    : closed_positions
                });
             }
    };


    //get message
    axios
      .get(url+"cryptoapi/chat-data")
      .then(res => {
          if (res.status === 200) {
            // console.log("RESRESRESR");
            // console.log(res.data);
            let filterrecords = [];
            for (var i = 0; i < res.data.length; i++) {
               var chatdata = {};
               chatdata.message = res.data[i].message;
               if(res.data[i].userId != null && res.data[i].userId.name!=''){
                 chatdata.author = res.data[i].userId.name;
               }else{
                if(res.data[i].userId != null)
                {
                 var split_name = res.data[i].userId.email.split("@");
                 chatdata.author = split_name[0];
                }
               }
               chatdata.moderator = (typeof res.data[i].userId.moderator != 'undefined')?res.data[i].userId.moderator:0;
               filterrecords.push(chatdata);
            }
            this.setState({ messages: filterrecords})
            // console.log(this.state.messages);
          }
      })
      .catch();
    //get message

    const addMessage = data => {
        this.setState({messages: [...this.state.messages, data]});
    };
    const userTradeFun = data => {

        this.setState({userTrade : [...this.state.userTrade, data]});

    };

    this.sendMessage = ev => {
        ev.preventDefault();
      //save Chat Message to database
      if(this.state.message!='')
      {
        var chatdata = {};
        chatdata.message = this.state.message;
        chatdata.userId = this.props.auth.user.id;
        axios
            .post(url+"cryptoapi/chat-add", chatdata)
            .then(res => {
                if (res.status === 200) {
                  console.log(res.data.message);
                }
            })
            .catch();
            var name = "";
            if(this.props.auth.user.name != ""){
              this.state.username = this.props.auth.user.name;
            }else{
              var split_name      = this.props.auth.user.email.split("@");
              this.state.username = split_name[0];
            }
            var fmoderator = (typeof this.props.auth.user.moderator != 'undefined')?this.props.auth.user.moderator:0;
            this.socket.emit('SEND_MESSAGE', {
                author: this.state.username,
                message: this.state.message,
                moderator: fmoderator,
            })
           this.setState({message: ''});
      }
      else
      {
          store.addNotification({
          title        : "Warning!",
          message      : "You cannot post empty message",
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss         : {
            duration     : 1500,
            onScreen     : true
          }
        });
      }
    }
    this.sendTradeMessage = ev => {
      ev.preventDefault();
      this.socket.emit('username',this.props.auth.user.id);
      this.socket.emit('SEND_TRADE', {
          toid : this.props.auth.user.id,
          id: this.props.auth.user.id,
          message: this.state.userTradedata
      })
      this.socket.emit('disconnect', {
          id: this.props.auth.user.id,
      })
    }
  }

    toggleMode = e => {

    if(document.body.classList == ""){
      document.body.classList.add('themeLight');
      this.setState({theme:"White"})
    }
    else{
      document.body.classList.remove('themeLight');
      this.setState({theme:"Dark"})
    }
  }
   onLogoutClick = e => {
    e.preventDefault();
    store.addNotification({
      title: "Wonderful!",
      message: "Loggedout Successfully",
      type: "success",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 2000,
        onScreen: true
      }
    });
    this.props.logoutUser();
    window.location.href="";
  };

  loadmoreRescentorder = () => {
    axios
      .post(url+"cryptoapi/loadmoreRescentorder",{pair:this.state.firstcurrency+this.state.secondcurrency,rescentcount:this.state.rescentcount+20})
      .then(res => {
        this.setState({Rescentorder:res.data.data,rescentcount:(this.state.rescentcount+20)})
      });
  }
  handleChangeStart = () => {
    console.log('Change event started')
  };

  handleChange = value => {
    this.setState({dummyleverage:value});
  };

  // afterchange = value => {
  //   alert('test');
  // }
  handleChangeComplete = value => {
    // alert(this.state.update);
    console.log(value,'valueeeeeeeeeeee');
    if(this.state.value!=value)
    {
      if(this.state.position_details.length>0)
      {
        this.setState({update:false})
         // if(window.confirm('Each contract can only use one leverage consistently across all active orders and open positions. Please note that changing the leverage while holding a position will affect the liquidation price.'))
         // {
            this.setState({
              value: value
            });
            this.calculation(value);
            this.changeopenpositions(value);
          // }
          // else
          // {
          //   return false;
          // }

      }
      else
      {
         this.setState({
            value: value
          });
          this.calculation(value);
      }

    }
  };

  balanceperc = e => {

    var orderprice = this.state.markprice;
    var balanceperc     = e.target.innerHTML.replace("%", "");

    var initial_balance = parseFloat(this.state.balance) * parseFloat(balanceperc)/100;

    var leveragebal     = parseFloat(this.state.value) * parseFloat(this.state.balance);

    var openfee         = parseFloat(leveragebal) * parseFloat(this.state.taker_fee)/100;

    var closefee        = parseFloat(openfee) * parseFloat(this.state.value) / parseFloat(this.state.value+1);

    var btcval          = parseFloat(initial_balance) - (parseFloat(openfee)+parseFloat(closefee));
    var quanti          = parseFloat(btcval)*parseFloat(orderprice);

    this.setState({quantity: parseFloat(quanti).toFixed()});
    this.calculation();

  };
   quantitydivide = e => {
        var quantperc     = e.target.innerHTML.replace("%", "");

        var est_quant = parseFloat((this.state.position_details.length>0 && this.state.position_details[0].quantity>0)?this.state.position_details[0].quantity:0) * parseFloat(quantperc)/100;

        this.setState({close_quantity: parseFloat(est_quant).toFixed()});
        this.calculation();

  };

  triggerPrice = (param) => {
      var type     = param.target.innerHTML;
      // console.log(type,'params');
      var trigger_price =  (type=='Last')?this.state.last_price:(type=='Index')?this.state.index_price:this.state.markprice;
      // console.log(trigger_price);
      this.setState({trigger_price:trigger_price,trigger_type:type});
  }
  triggertp = (param) => {
      var type     = param.target.innerHTML;
      // console.log(type,'params');
      var trigger_price =  (type=='Last')?this.state.last_price:(type=='Index')?this.state.index_price:this.state.markprice;
      // console.log(trigger_price);
      this.setState({takeprofit:trigger_price,tptrigger_type:type});
  }
  triggerstop = (param) => {
      var type     = param.target.innerHTML;
      // console.log(type,'params');
      var trigger_price =  (type=='Last')?this.state.last_price:(type=='Index')?this.state.index_price:this.state.markprice;
      // console.log(trigger_price);
      this.setState({stopprice:trigger_price,stoptrigger_type:type});
  }

  conditionalPrice = (param) => {
     var type     = param.target.innerHTML;
     (type=='Limit')?this.setState({readOnly:false,triggerordertype:type}):this.setState({readOnly:true,triggerordertype:type});
  }
  ordertype_changes = (param) => {
    // console.log('fhjsdkjhfskjdhfksdhjf');
    var type     = param.target.innerHTML;
    // console.log(type);
    if(type=='Limit')
    {
      this.setState({limit:true,conditional:false,ordertype: type,price:this.state.markprice});
    }
    else if(type=='Conditional')
    {
      this.setState({conditional:true,ordertype: type,limit:false});
    }
    else
    {
      this.setState({limit:false,conditional:false,ordertype: type});
    }
  }
  tpClose= (param) =>{
    this.setState({tpshow:false});
  }
   handleClose1= (param) =>{
    this.setState({show1:false});
  }
  handleShow1 = (param) =>{
    var type     = param.target.innerHTML;
    this.setState({show1:true,popuptype:type,close_quantity:(this.state.position_details.length>0 && this.state.position_details[0].quantity)?Math.abs(this.state.position_details[0].quantity):0,close_price:(this.state.position_details.length>0 && this.state.position_details[0].price>0)?this.state.position_details[0].price:0});
  }

  tpshow = (param) =>{
    var type     = param.target.innerHTML;
    this.setState({tpshow:true});
  }

  handleShow = (e) => {
    var type = e.target.innerHTML;
    this.setState({buyorsell:type.toLowerCase(),price:this.state.markprice,alertmessage:""});
  }
  handleShow22 = (type) => {
    if(this.state.buyorsell=='buy')
    {
        var buyselltype = "buy";
        var orderprice = this.state.ordertype=='Market'?this.state.markprice:this.state.price;
        var Liqprice = (+orderprice* +this.state.value)/(( +this.state.value+1)-(+this.state.mainmargin* +this.state.value));
    }
    else
    {
        var orderprice = this.state.ordertype=='Market'?this.state.markprice:this.state.price;
        var buyselltype = "sell";
        var Liqprice = (+orderprice* +this.state.value)/((+this.state.value-1)+(+this.state.mainmargin* +this.state.value));
    }

    if(this.state.ordertype=='Limit' && buyselltype=="buy" && parseFloat(this.state.price) > parseFloat(this.state.markprice))
    {
        this.setState({alertmessage:"Entry price you set must be lower or equal to "+this.state.markprice})
        return false;
    }
    else if(this.state.ordertype=='Limit' && buyselltype=="sell" && parseFloat(this.state.price) < parseFloat(this.state.markprice))
    {
      this.setState({alertmessage:"Entry price you set must be higher or equal to "+this.state.markprice})
      return false;
    }
    // if(this.state.ordertype=='Market' && buyselltype=="sell" && parseFloat(this.state.buyOrder.length) < 1 && this.state.value>20)
    // {
    //   this.setState({alertmessage:"There is no taker for this order, try again later"})
    //   return false;
    // }
    // else if(this.state.ordertype=='Market' && buyselltype=="buy" && parseFloat(this.state.sellOrder.length) < 1 && this.state.value>20)
    // {
    //   this.setState({alertmessage:"There is no taker for this order, try again later"})
    //   return false;
    // }
    if(this.state.ordertype=='Limit' && buyselltype=="buy" && parseFloat(Liqprice) > parseFloat(this.state.price))
    {
      this.setState({alertmessage:"Opening this position may cause immediate liquidation as the system predicts that the position's Liquidation price will be above Mark Price if the order is fulfilled."})
      return false;
    }
     else if(this.state.ordertype=='Limit' && buyselltype=="sell" && parseFloat(Liqprice) < parseFloat(this.state.price))
    {
      this.setState({alertmessage:"Opening this position may cause immediate liquidation as the system predicts that the position's Liquidation price will be below Mark Price if the order is fulfilled."})
      return false;
    }
    else{
      this.setState({alertmessage:""})
    }
    if(parseFloat(this.state.quantity) < parseFloat(this.state.minquantity))
    {
       store.addNotification({
          title        : "Wonderful!",
          message      : "Quantity of contract must not be lesser than "+this.state.minquantity,
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss      : {
            duration     : 1500,
            onScreen     : true
          }
        });
       return false;
    }
    if(parseFloat(this.state.quantity) > parseFloat(this.state.maxquantity))
    {
       store.addNotification({
          title        : "Wonderful!",
          message      : "Quantity of contract must not be higher than "+this.state.maxquantity,
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss      : {
            duration     : 1500,
            onScreen     : true
          }
        });
       return false;
    }
    if(parseFloat(this.state.order_cost) < parseFloat(this.state.minmargin))
    {
       store.addNotification({
          title        : "Wonderful!",
          message      : "Minimum margin impact is "+this.state.minmargin,
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss      : {
            duration     : 1500,
            onScreen     : true
          }
        });
       return false;
    }
    if(isNaN(orderprice))
    {
       store.addNotification({
          title        : "Wonderful!",
          message      : "Price is invalid",
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss      : {
            duration     : 1500,
            onScreen     : true
          }
        });
       return false;
    }
    if(orderprice<0.001)
    {
       store.addNotification({
          title        : "Wonderful!",
          message      : "Price of contract must not be lesser than 0.001",
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss      : {
            duration     : 1500,
            onScreen     : true
          }
        });
       return false;
    }
    else if(this.state.balance<this.state.order_cost)
    {
       store.addNotification({
          title        : "Warning!",
          message      : "Due to insuffient balance order cannot be placed",
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss         : {
            duration     : 1500,
            onScreen     : true
          }
        });
       return false;
    }
    else
    {
      if(this.state.ordertype=='Conditional' && this.state.trigger_price<1)
      {
        store.addNotification({
          title        : "Warning!",
          message      : "Trigger Price of contract must not be lesser than 1",
          type         : "danger",
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss         : {
            duration     : 1500,
            onScreen     : true
          }
        });
        return false;
      }



      this.setState({show:true,buyorsell:buyselltype,Liqprice:Liqprice});
    }
  }

  handleClose = () => {
      this.setState({show:false});
    }

  orderPlacing = (type) => {

    if(this.state.stopreadOnly==false)
    {
        if(this.state.buyorsell=='buy' && parseFloat(this.state.markprice)<=parseFloat(this.state.stopprice))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be lower than "+this.state.markprice});
          return false;
        }
        else if(this.state.buyorsell=='buy' && parseFloat(this.state.Liqprice)>=parseFloat(this.state.stopprice))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be higher than "+parseFloat(this.state.Liqprice).toFixed(this.state.floating)});
          return false;
        }
        else if(this.state.buyorsell=='sell' && parseFloat(this.state.markprice)>=parseFloat(this.state.stopprice))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be higher than "+this.state.markprice});
          return false;
        }
        else if(this.state.buyorsell=='sell' && parseFloat(this.state.Liqprice)<=parseFloat(this.state.stopprice))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be lower than "+parseFloat(this.state.Liqprice).toFixed(this.state.floating)});
          return false;
        }
        else
        {
          this.setState({alertmessage1:''});
        }
      }
      if(this.state.tpreadOnly==false)
      {
        if(this.state.buyorsell=='buy' && parseFloat(this.state.markprice)>=parseFloat(this.state.takeprofit))
        {
          this.setState({alertmessage2:"Stop Loss price you set must be higher than "+this.state.markprice});
          return false;
        }
        else if(this.state.buyorsell=='sell' && parseFloat(this.state.markprice)>=parseFloat(this.state.takeprofit))
        {
          this.setState({alertmessage2:"Stop Loss price you set must be lower than "+this.state.markprice});
          return false;
        }
        else
        {
          this.setState({alertmessage2:''});
        }
      }
    // return false;
    let userid = this.props.auth.user.id;
    var check1 = type.target.className.includes("btnBuy");
    if(this.state.buyorsell=='buy'){
      this.setState({buydisable:false,show:false});
    } else {
      this.setState({selldisable:false,show:false});
    }
    var orderprice = this.state.ordertype=='Market'?this.state.markprice:this.state.price;

    let orderData = {
      "quantity"         : this.state.quantity,
      "price"            : orderprice,
      "userid"           : userid,
      "pair"             : this.state.firstcurrency+this.state.secondcurrency,
      "pairname"         : this.state.firstcurrency+this.state.secondcurrency,
      "firstcurrency"    : this.state.firstcurrency,
      "secondcurrency"   : this.state.secondcurrency,
      "timeinforcetype"  : this.state.timeinforcetype.value,
      // "leverage"         : 180,
      "leverage"         : this.state.value,
      "buyorsell"        : this.state.buyorsell,
      "ordertype"        : (this.state.ordertype=='Conditional')?this.state.triggerordertype:this.state.ordertype,
      "trigger_price"    : (this.state.ordertype=='Conditional')?this.state.trigger_price:0,
      "trigger_type"     : (this.state.ordertype=='Conditional')?this.state.trigger_type:null,
      "tptrigger_type"   : this.state.tptrigger_type,
      "stoptrigger_type" : this.state.stoptrigger_type,
      "post_only"        : this.state.post_only,
      "reduce_only"      : this.state.reduce_only,
      "stopcheck"        : this.state.stopreadOnly,
      "takeprofitcheck"  : this.state.tpreadOnly,
      "takeprofit"       : this.state.takeprofit,
      "stopprice"        : this.state.stopprice,
    }
    this.setState({disable:false});
    this.setState({update:false})
    this.props.orderPlacing(orderData);
  };

  orderPlacing1 = (type) => {
    this.setState({show1:false});
    let userid = this.props.auth.user.id;

    var buyselltype = (this.state.position_details.length>0 && this.state.position_details[0].quantity>0)?'sell':'buy';
    var orderprice = (buyselltype=='buy')?this.state.selllimit:this.state.buylimit;
    let orderData = {
      "quantity"       : this.state.close_quantity,
      "price"          : orderprice,
      "userid"         : userid,
      "pair"           : this.state.firstcurrency+this.state.secondcurrency,
      "pairname"       : this.state.firstcurrency+this.state.secondcurrency,
      "firstcurrency"  : this.state.firstcurrency,
      "secondcurrency" : this.state.secondcurrency,
      "leverage"       : this.state.value,
      "buyorsell"      : buyselltype,
      "ordertype"      : this.state.popuptype,
      "trigger_price"  : 0,
      "trigger_type"   : null,
      "post_only"      : this.state.post_only,
      "reduce_only"    : this.state.reduce_only,
    }
    this.setState({update:false})
    this.setState({disable:false});
    this.props.orderPlacing(orderData);
  };


triggerstoporder = (type) => {

    let userid = this.props.auth.user.id;

    var orderprice = this.state.markprice;
    var buyselltype = (this.state.position_details.length>0 && this.state.position_details[0].quantity>0)?'sell':'buy';
    var close_quantity = (this.state.position_details.length>0 && this.state.position_details[0].quantity!=0)?this.state.position_details[0].quantity:0;
    let orderData = {
      "quantity"             : close_quantity,
      "price"                : this.state.instantprice,
      "userid"               : userid,
      "pair"                 : this.state.firstcurrency+this.state.secondcurrency,
      "pairname"             : this.state.firstcurrency+this.state.secondcurrency,
      "firstcurrency"        : this.state.firstcurrency,
      "secondcurrency"       : this.state.secondcurrency,
      "leverage"             : this.state.value,
      "buyorsell"            : buyselltype,
      "ordertype"            : "Market",
      "stopprice"            : this.state.stopprice,
      "takeprofit"           : this.state.takeprofit,
      "trailingstopdistance" : this.state.trailingstopdistance,
      "tptrigger_type"       : this.state.tptrigger_type,
      "stoptrigger_type"     : this.state.stoptrigger_type,
      "takeprofitcheck"      : this.state.takeprofitcheck,
      "stopcheck"            : this.state.stopcheck,
    }
    this.setState({disable:false,tpshow:false});
    this.props.triggerstop(orderData);
  };


  orderType = (e) => {
    if(e.target.id == 'nav-tradeLimit-tab')
    {
      this.setState({limit:true});
    }
    else
    {
      this.setState({limit:false});
    }
  };

  cancelFunction = (e) => {
    if(window.confirm('Are you confirm to cancel this order?.'))
    {
        let userid = this.props.auth.user.id;
        var iddetails = {id:e.target.id,userid:userid};
        this.props.cancelTrade(iddetails);

    }
  };

  upvalue = (field) => {
    if(field=='quantity'){
      var newval = this.state.quantity+0.5;
      this.setState({quantity: newval});
    }
  };

 updownvalue = (one,two,three) => {

    var newval = 0;
    if(one=='quantity'){
      if(two=='plus'){
        newval = parseFloat(this.state.quantity?this.state.quantity:0)+1;
      } else if(two=='minus'){
        newval = parseFloat(this.state.quantity?this.state.quantity:0)-1;
      }
      if(newval >= 0){
        this.setState({quantity: newval});
          var orderprice      = this.state.ordertype=='Market'?this.state.markprice:this.state.price;
          var leverage        = this.state.value;

          var order_value1    = parseFloat(newval*orderprice).toFixed(8);

          var order_value     = parseFloat(order_value1/this.state.btcprice).toFixed(8);

          var required_margin = parseFloat(order_value1)/leverage;

          var fee             = parseFloat(order_value1)*this.state.taker_fee/100;

          var margininbtc     = parseFloat(required_margin)/parseFloat(this.state.btcprice);
          var feeinbtc        = parseFloat(fee)/parseFloat(this.state.btcprice);

          var order_cost      = parseFloat(margininbtc)+parseFloat(feeinbtc);

          this.setState({order_cost:parseFloat(order_cost).toFixed(8),order_value:parseFloat(order_value).toFixed(8)});
      }
    }

    if(one=='price'){
      if(two=='plus'){
        newval = parseFloat(this.state.price?this.state.price:0)+0.5;
      } else if(two=='minus'){
        newval = parseFloat(this.state.price?this.state.price:0)-0.5;
      }
      if(newval >= 0){
        this.setState({price: newval});
      }
    }

    if(one=='trigger_price'){
      if(two=='plus'){
        newval = parseFloat(this.state.trigger_price?this.state.trigger_price:0)+0.5;
      } else if(two=='minus'){
        newval = parseFloat(this.state.trigger_price?this.state.trigger_price:0)-0.5;
      }
      if(newval >= 0){
        this.setState({trigger_price: newval});
      }
    }
     if(one=='takeprofit'){
      if(two=='plus'){
        newval = parseFloat(this.state.takeprofit?this.state.takeprofit:0)+0.5;
      } else if(two=='minus'){
        newval = parseFloat(this.state.takeprofit?this.state.takeprofit:0)-0.5;
      }
      if(newval >= 0 && this.state.takeprofitcheck){
        this.setState({takeprofit: newval});
      }
    }

    if(one=='stopprice'){
      if(two=='plus'){
        newval = parseFloat(this.state.stopprice?this.state.stopprice:0)+0.5;
      } else if(two=='minus'){
        newval = parseFloat(this.state.stopprice?this.state.stopprice:0)-0.5;
      }
      if(newval >= 0 && this.state.stopcheck){
        this.setState({stopprice: newval});
      }
    }
    if(one=='trailingstopdistance'){
      if(two=='plus'){
        newval = parseFloat(this.state.trailingstopdistance?this.state.trailingstopdistance:0)+0.5;
      } else if(two=='minus'){
        newval = parseFloat(this.state.trailingstopdistance?this.state.trailingstopdistance:0)-0.5;
      }
      if(newval >= 0 && this.state.trailcheck){
        this.setState({trailingstopdistance: newval});
      }
    }

    if(one=='close_quantity'){
      if(two=='plus'){
        newval = parseFloat(this.state.close_quantity?this.state.close_quantity:0)+0.5;
      } else if(two=='minus'){
        newval = parseFloat(this.state.close_quantity?this.state.close_quantity:0)-0.5;
      }
      if(newval >= 0 ){
        this.setState({close_quantity: newval});
      }
    }
  };

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    if(this.props.auth.user.blocktime!='' && this.props.auth.user.blockhours)
    {
      var _date1 = new Date(this.props.auth.user.blocktime);
      this.setState({blocktime:(new Date( _date1.getTime() + 1000 * 60 * 60 * this.props.auth.user.blockhours ))});
    }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
    else
    {
      this.setState({
        errors: {}
      });
    }
    // console.log(nextProps.auth,'nextProps');
    if (nextProps.auth.trade !== undefined
      && nextProps.auth.trade.data !== undefined
      && nextProps.auth.trade.data.data !== undefined
      ) {

          var pair = this.state.curcontract;
          var res = pair.split("_");
          var temp = pair.replace("-","");
          var index = nextProps.auth.trade.data.data.findIndex(x => (x.tiker_root) === temp);
          var btcindex = nextProps.auth.trade.data.data.findIndex(x => (x.tiker_root) === 'BTCUSD');
      if(index!=-1){
          var perparray = nextProps.auth.trade.data.data;
            var indexarr = [0, 1, 2,3,4];
          var outputarr = indexarr.map(i => perparray[i]);
         // alert(nextProps.auth.trade.data.data[index].markprice);

          this.setState({
            records   : outputarr,
            perpetual : nextProps.auth.trade.data.data[0],
            markprice : parseFloat(nextProps.auth.trade.data.data[index].markprice).toFixed(this.state.floating),
            value : parseFloat(nextProps.auth.trade.data.data[index].leverage).toFixed(),
            maxleverage : parseFloat(nextProps.auth.trade.data.data[index].leverage).toFixed(),
            //price     : parseFloat(nextProps.auth.trade.data.data[index].markprice).toFixed(this.state.floating),
            btcprice  : parseFloat(nextProps.auth.trade.data.data[btcindex].markprice).toFixed(2),
            maxquantity  : parseFloat(nextProps.auth.trade.data.data[index].maxquantity),
            minquantity  : parseFloat(nextProps.auth.trade.data.data[index].minquantity),
            minmargin  : parseFloat(nextProps.auth.trade.data.data[index].minmargin),
          });
      }
    }
 // console.log(nextProps.auth.trade,'tradeTableAlltradeTableAll');
    if (nextProps.auth.trade !== undefined && nextProps.auth.trade.data !== undefined && nextProps.auth.trade.data.message=='tradeTableAll' && nextProps.auth.trade !== undefined
    && nextProps.auth.trade.data !== undefined && nextProps.auth.trade.data.pricedet !== undefined && nextProps.auth.trade.data.pricedet.length>0) {
      console.log(nextProps.auth.trade.data.pricedet,'tradeTableAlltradeTableAll for the tradeheader');
      var btcvalue = 0;
      if(nextProps.auth.trade.data.pricedet.length>0)
      {
        var usdvalue = parseFloat(this.state.markprice)*parseFloat(nextProps.auth.trade.data.pricedet[0].volume);
        var btcvalue = parseFloat(usdvalue)/parseFloat(this.state.btcprice);
      }
      this.setState({
        //price         : nextProps.auth.trade.data.pricedet[0].last,
        trigger_price : nextProps.auth.trade.data.pricedet[0].last,
        last_price    : nextProps.auth.trade.data.pricedet[0].last,
        pricedet      : nextProps.auth.trade.data.pricedet,
        // btcvolume     : isNaN(parseFloat(btcvalue))?0:btcvalue,
                btcvolume     :usdvalue,
        change        : (nextProps.auth.trade.data.pricedet.length>0)?nextProps.auth.trade.data.pricedet[0].change:0,
        instantprice  : (nextProps.auth.trade.data.pricedet.length>0)?nextProps.auth.trade.data.pricedet[0].last:0
      });
    }


    if (nextProps.auth.trade !== undefined
        && nextProps.auth.trade.data !== undefined
        && nextProps.auth.trade.data.message !== undefined ) {

      var message = nextProps.auth.trade.data.message;
      //console.log(nextProps.auth.trade,'tradedetails');
      if(message=='tradeTableAll' && nextProps.auth.trade.data.buyOrder !== undefined && nextProps.auth.trade.data.sellOrder !== undefined && nextProps.auth.trade.data.assetdetails !== undefined && nextProps.auth.trade.data.contractdetails !== undefined && nextProps.auth.trade.data.Rescentorder !== undefined){
        var assetdetails    = nextProps.auth.trade.data.assetdetails;
        console.log(assetdetails,'assetdetails');
        var contractdetails = nextProps.auth.trade.data.contractdetails;
        var index           = assetdetails.findIndex(x => (x.currencySymbol).toLowerCase() === 'btc');
        if(index!=-1){
          var baldet          = parseFloat(assetdetails[index].balance);
        }
        else
        {
          var balance = 0;
        }
          var buyOrder        = nextProps.auth.trade.data.buyOrder;
          var sellOrder       = nextProps.auth.trade.data.sellOrder;
          var Rescentorder    = nextProps.auth.trade.data.Rescentorder;
          // console.log("Rescentorder",Rescentorder);

          var val = (sellOrder.length<6)?0:((sellOrder.length>=6)&&(sellOrder.length<9))?50:(buyOrder.length<3)?500:150
          this.setState({
            buyOrder      : buyOrder,
            sellOrder     : sellOrder,
            buylimit      : (buyOrder.length>0)?buyOrder[0]._id:this.state.markprice,
            selllimit     : (sellOrder.length>0)?sellOrder[sellOrder.length-1]._id:this.state.markprice,
            Rescentorder  : Rescentorder,
            balance       : baldet,
            mainmargin    : (contractdetails!=null?contractdetails.maint_margin/100:0),

          }, () => {
            // alert(val);
          const {scrollbar} = this.refs;  // scrollbar has access to the internal API
            scrollbar.view.scrollTop = val;
        });

      }

       if(message=='tradeTableAll' && nextProps.auth.trade.data.orderHistory !== undefined && nextProps.auth.trade.data.assetdetails !== undefined && nextProps.auth.trade.data.Conditional_details !== undefined && nextProps.auth.trade.data.position_details !== undefined && nextProps.auth.trade.data.daily_details !== undefined  && nextProps.auth.trade.data.closed_positions !== undefined   && nextProps.auth.trade.data.allposition_details !== undefined ){
        console.log('here')
         var orderHistory        = nextProps.auth.trade.data.orderHistory;
         var Histroydetails      = nextProps.auth.trade.data.Histroydetails;
         var Filleddetails       = nextProps.auth.trade.data.Filleddetails;
         var assetdetails        = nextProps.auth.trade.data.assetdetails;
         var Conditional_details = nextProps.auth.trade.data.Conditional_details;
         var position_details    = nextProps.auth.trade.data.position_details;
         var daily_details       = nextProps.auth.trade.data.daily_details;
         var closed_positions    = nextProps.auth.trade.data.closed_positions;
         var allposition_details = nextProps.auth.trade.data.allposition_details;
         console.log(Conditional_details,'Conditional_details');
          this.setState({
            orderHistory        : orderHistory,
            Histroydetails      : Histroydetails,
            Filleddetails       : Filleddetails,
            assetOverview       : assetdetails,
            Conditional_details : Conditional_details,
            allposition_details : allposition_details,
            position_details    : position_details,
            daily_details       : (daily_details.length>0)?daily_details:[],
            value               : (position_details.length>0 && typeof position_details[0].leverage  != 'undefined' && this.state.update == true)?position_details[0].leverage:this.state.value,
            closed_positions    : closed_positions,
            update              : false
          });
       }

      if(nextProps.auth.trade.data.notify_show !==undefined && nextProps.auth.trade.data.notify_show=='yes' && nextProps.auth.trade.data.status !== undefined){
        if(nextProps.auth.trade.data.status){
          var type = 'success';
        } else {
          var type = 'danger';
        }

        store.addNotification({
          title        : "Wonderful!",
          message      : message,
          type         : type,
          insert       : "top",
          container    : "top-right",
          animationIn  : ["animated", "fadeIn"],
          animationOut : ["animated", "fadeOut"],
          dismiss      : {
            duration     : 1500,
            onScreen     : true
          }
        });
        this.setState({disable:true,selldisable:true,buydisable:true});
        this.getTradeData();
        this.getuserTradeData();
      }
      nextProps.auth.trade = "";
    }
  }



  pairChange = (e) => {
    let userid = this.props.auth.user.id;

    var pair = e.target.id;
    var res = pair.split("_");
    var temp = pair.replace("_","");
    var loctemp = pair.replace("_","-");
    var index = this.state.records.findIndex(x => (x.tiker_root) === temp);
    var mainmargin = (index>-1)?this.state.records[index].maint_margin:0.5/100;
    //console.log(index)
    this.setState({
      curcontract    : e.target.id,
      chartpair      : temp,
      secondcurrency : res[1],
      chartpair      : temp,
      firstcurrency  : res[0],
      price          : (index>-1)?this.state.records[index].mark_price:this.state.price,
      perpetual      : this.state.records[index],
      mainmargin     : mainmargin,
    });
    localStorage.setItem('curpair', loctemp);
    window.location.href="/Trade/"+loctemp;
    // this.getData();
    var findObj = {
      userid:userid,
      firstCurrency:res[0],
      secondCurrency:res[1]
    };
    this.props.getTradeData(findObj);
    this.props.getuserTradeData(findObj);
    this.props.getPricevalue(findObj);

  };

  fillprice = (a,b) => {
    this.setState({quantity:a,price:b}, () => {
    this.calculation();
  });
  };

  fillquantity = e => {
    this.setState({quantity:e.target.innerHTML});
  };
  onChange = e => {

    this.setState({ [e.target.id]: e.target.value });
    if(e.target.id=='takeprofit' && e.target.value)
    {
         if(this.state.buyorsell=='buy' && parseFloat(this.state.markprice)>=parseFloat(e.target.value))
        {
          this.setState({alertmessage2:"Stop Loss price you set must be higher than "+this.state.markprice});
          return false;
        }
        else if(this.state.buyorsell=='sell' && parseFloat(this.state.markprice)>=parseFloat(e.target.value))
        {
          this.setState({alertmessage2:"Stop Loss price you set must be lower than "+this.state.markprice});
          return false;
        }
        else
        {
          this.setState({alertmessage2:''});
          return true;
        }
    }
    if(e.target.id=='trailcheck' && e.target.value)
    {
        this.setState({trailreadOnly:(this.state.trailreadOnly)?false:true});
    }
    if(e.target.id=='stopcheck' && e.target.value)
    {
        this.setState({stopreadOnly:(this.state.stopreadOnly)?false:true});
    }
     if(e.target.id=='takeprofitcheck' && e.target.value)
    {
        this.setState({tpreadOnly:(this.state.tpreadOnly)?false:true});
    }
    if(e.target.id=='stopprice' && e.target.value)
    {
        if(this.state.buyorsell=='buy' && parseFloat(this.state.markprice)<=parseFloat(e.target.value))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be lower than "+this.state.markprice});
          return false;
        }
        else if(this.state.buyorsell=='buy' && parseFloat(this.state.Liqprice)>=parseFloat(e.target.value))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be higher than "+parseFloat(this.state.Liqprice).toFixed(this.state.floating)});
          return false;
        }
        else if(this.state.buyorsell=='sell' && parseFloat(this.state.markprice)>=parseFloat(e.target.value))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be higher than "+this.state.markprice});
          return false;
        }
        else if(this.state.buyorsell=='sell' && parseFloat(this.state.Liqprice)<=parseFloat(e.target.value))
        {
          this.setState({alertmessage1:"Stop Loss price you set must be lower than "+parseFloat(this.state.Liqprice).toFixed(this.state.floating)});
          return false;
        }
        else
        {
          this.setState({alertmessage1:''});
          return true;
        }

    }
    if(e.target.id=='reduce_only' || e.target.id=='post_only'){
      var checked = e.target.checked;

      if(e.target.id=='post_only'){
        this.setState({post_only: checked});
      } else if(e.target.id=='reduce_only'){
        this.setState({reduce_only: checked});
      }
    }
     else if(e.target.id=='quantity' || e.target.id=='price')
    {
      var leverage = parseFloat(100/this.state.value);
      console.log(this.state.ordertype,'ordertype')
      if(e.target.id=='quantity')
      {
        var quantity = e.target.value;
        var orderprice = this.state.ordertype=='Market'?this.state.markprice:this.state.price;
      }
      if(e.target.id=='price')
      {
        var buyselltype = this.state.buyorsell;
        var quantity = this.state.quantity;
        var orderprice = this.state.ordertype=='Market'?this.state.markprice:e.target.value;

          // if(this.state.ordertype=='Limit' && buyselltype=="buy" && parseFloat(e.target.value) > parseFloat(this.state.selllimit))
          // {
          //     this.setState({alertmessage:"Entry price you set must be lower or equal to "+parseFloat(this.state.selllimit)})
          //     return false;
          // }
          // else if(this.state.ordertype=='Limit' && buyselltype=="sell" && parseFloat(e.target.value) < parseFloat(this.state.buylimit))
          // {
          //   this.setState({alertmessage:"Entry price you set must be higher or equal to "+this.state.buylimit})
          //   return false;
          // }
          // else{
          //    this.setState({alertmessage:""})
          // }
      }




      // var initial_margin = ((parseFloat(quantity)/parseFloat(orderprice))*leverage)/100;

      // var feetoopen = (quantity/orderprice)*this.state.taker_fee/100;

      // var Bankruptcy = orderprice * this.state.value /(parseFloat(this.state.value) + 1);

      // var feetoclose = (quantity/Bankruptcy) * this.state.taker_fee/100;
      // var order_cost = parseFloat(initial_margin) +  parseFloat(feetoopen) +  parseFloat(feetoclose);

      var order_value1 = parseFloat(quantity*orderprice).toFixed(8);

      var order_value = parseFloat(order_value1/this.state.btcprice).toFixed(8);

      var required_margin = parseFloat(order_value1)/this.state.value;

      var fee = parseFloat(order_value1)*this.state.taker_fee/100;

      var margininbtc = parseFloat(required_margin)/parseFloat(this.state.btcprice);
      var feeinbtc = parseFloat(fee)/parseFloat(this.state.btcprice);

      var order_cost = parseFloat(margininbtc)+parseFloat(feeinbtc);


      this.setState({order_cost:parseFloat(order_cost).toFixed(8),order_value:order_value});
    }
  };

  changetimeinforce = timeinforcetype => {
    this.setState({ timeinforcetype });
    console.log(`Option selected:`, timeinforcetype);
  };

  orderbookshow = e => {
    this.setState({orderbookshow:e.target.id})
     };

  calculation = (value=0) => {
    // alert('tefsdf');
    var orderprice = this.state.ordertype=='Market'?this.state.markprice:this.state.price;
    var leverage = value!=0?value:this.state.value;

    var order_value1 = parseFloat(this.state.quantity*orderprice).toFixed(8);

    var order_value = parseFloat(order_value1/this.state.btcprice).toFixed(8);

    var required_margin = parseFloat(order_value1)/leverage;

    var fee = parseFloat(order_value1)*this.state.taker_fee/100;

    var margininbtc = parseFloat(required_margin)/parseFloat(this.state.btcprice);
    var feeinbtc = parseFloat(fee)/parseFloat(this.state.btcprice);

    var order_cost = parseFloat(margininbtc)+parseFloat(feeinbtc);
console.log(order_cost,'order_cost');
console.log(order_value,'order_value');
    this.setState({order_cost:parseFloat(order_cost).toFixed(8),order_value:parseFloat(order_value).toFixed(8)});
  };

  componentDidMount() {

    console.log('component');
    this.getData();
    if(!localStorage.getItem('curpair'))
    {
      localStorage.setItem('curpair','BTC-USD')
    }
  };

  getData() {
    this.props.getPertual();
    this.getTradeData();
    this.getuserTradeData();
    console.log(this.props.auth.user.id,'createroom')
    this.socket.emit('CREATEROOM',this.props.auth.user.id);

  }

  changeopenpositions(value){

    this.props.changeopenpositions({leverage:value,user_id:this.props.auth.user.id});
  }

  getTradeData() {
    let userid = this.props.auth.user.id;
    var findObj = {
      userid:userid,
      firstCurrency:this.state.firstcurrency,
      secondCurrency:this.state.secondcurrency
    };
    this.props.getTradeData(findObj);
  }
  getPricevalue(){
    var findObj = {
      firstCurrency:this.state.firstcurrency,
      secondCurrency:this.state.secondcurrency
    };
    this.props.getPricevalue(findObj);
  }

  getuserTradeData() {
    let userid = this.props.auth.user.id;
    var findObj = {
      userid:userid,
      firstCurrency:this.state.firstcurrency,
      secondCurrency:this.state.secondcurrency
    };
    this.props.getuserTradeData(findObj);
    this.props.getPricevalue(findObj);
  }


  render () {
    const { user } = this.props.auth;
    const { value,records,assetOverview,timeinforcetype,errors,orderHistory,Histroydetails,Filleddetails,perpetual,Conditional_details,position_details,daily_details,closed_positions } = this.state

    if(Conditional_details.length>0)
    {
       var cindex = Conditional_details.findIndex(x => (x.trigger_ordertype) === 'takeprofit');
       var tpprice = (cindex==-1)?'-':Conditional_details[cindex].trigger_price;

       var sindex = Conditional_details.findIndex(x => (x.trigger_ordertype) === 'stop');
       var stprice = (sindex==-1)?'-':Conditional_details[sindex].trigger_price;
    }



    if(position_details.length>0)
    {
      var pos_pairName       = (position_details[0].pairName)?position_details[0].pairName:0;
      var pos_quantity       = (position_details[0].quantity)?position_details[0].quantity:0;
      var pos_price          = (position_details[0].price)?position_details[0].price:0;
      var pos_leverage       = (position_details[0].leverage)?position_details[0].leverage:0;

      //calculate the initial margin

      var order_value1 = parseFloat(pos_quantity*pos_price).toFixed(8);

      var order_value = parseFloat(order_value1/this.state.btcprice).toFixed(8);

      var required_margin = parseFloat(order_value1)/pos_leverage;

      var fee = parseFloat(order_value1)*this.state.taker_fee/100;

      var margininbtc = parseFloat(required_margin)/parseFloat(this.state.btcprice);
      var feeinbtc = parseFloat(fee)/parseFloat(this.state.btcprice);

      var pos_initial_margin = parseFloat(margininbtc)+parseFloat(feeinbtc);


      // var pos_initial_margin = (parseFloat(pos_quantity)/parseFloat(pos_price))*((100/parseFloat(pos_leverage))/100);
      var mainmarginwithleverage = parseFloat(this.state.mainmargin)*parseFloat(pos_leverage);

      if(pos_quantity>0)
      {
        var difference = parseFloat(this.state.markprice) - parseFloat(pos_price);
          var pos_liqprice       = (parseFloat(pos_price)*parseFloat(pos_leverage))/((parseFloat(pos_leverage)+1)-parseFloat(mainmarginwithleverage));
      }
      else
      {
        var difference = parseFloat(pos_price) -  parseFloat(this.state.markprice);
          var pos_liqprice       = (parseFloat(pos_price)*parseFloat(pos_leverage))/((parseFloat(pos_leverage)-1)+parseFloat(mainmarginwithleverage));
      }
      var profitnlossusd = parseFloat(difference)*parseFloat(Math.abs(pos_quantity));
      var profitnloss    = parseFloat(profitnlossusd)/parseFloat(pos_price);
      var profitnlossbtc = parseFloat(profitnlossusd)/parseFloat(this.state.btcprice);
      var profitnlossper = parseFloat(profitnloss)*100;
      var roe            = parseFloat(profitnlossper)*10;
      roe = (profitnlossbtc<0)?roe:"+"+(roe);

      // console.log(pos_liqprice,'pos_liqprice')
    }
    if(daily_details.length>0)
    {
      var daily_quantity       = (daily_details[0].quantity)?daily_details[0].quantity:0;
      var daily_price          = (daily_details[0].price)?daily_details[0].price:0;
      var daily_leverage       = (daily_details[0].leverage)?daily_details[0].leverage:0;
      var daily_fees           = (daily_details[0].Fees)?daily_details[0].Fees:0;

      //calculate the initial margin
      var daily_initial_margin = (parseFloat(daily_quantity)/parseFloat(daily_price))*((100/parseFloat(daily_leverage))/100);
      if(daily_quantity>0)
      {
        var difference = parseFloat(this.state.markprice) - parseFloat(daily_price);
      }
      else
      {
        var difference = parseFloat(daily_price) -  parseFloat(this.state.markprice);
      }

      var dailyprofitnlossusd = parseFloat(difference)*parseFloat(Math.abs(daily_quantity));
      var dailyprofitnloss    = parseFloat(profitnlossusd)/parseFloat(daily_price);
      var dailyprofitnlossbtc = parseFloat(profitnlossusd)/parseFloat(this.state.btcprice);
      var dailyprofitnlossper = parseFloat(profitnloss)*100;


      // var dailyprofitnloss        = [ (1/parseFloat(daily_price))  - (1/parseFloat(this.state.last_price)) ] * parseFloat(daily_quantity);
      // dailyprofitnloss            = parseFloat(dailyprofitnloss)-parseFloat(daily_fees)
      // var dailyprofitnlossper     = (parseFloat(dailyprofitnloss)/parseFloat(daily_initial_margin))*100;
      // var dailyprofitnlossusd     = (dailyprofitnloss*parseFloat(this.state.last_price));
      // var dailyprofitnlossbtc =  parseFloat(dailyprofitnlossusd)/parseFloat(this.state.btcprice);

    }
    else
    {
      var daily_quantity      = 0;
      var daily_price         = 0;
      var daily_leverage      = 0;
      var daily_fees          = 0;
      var dailyprofitnlossper = 0;
      var dailyprofitnloss    = 0;
      var dailyprofitnlossusd = 0;
      var dailyprofitnlossbtc = 0;
    }
     // Rc Slider - https://react-component.github.io/slider/

    const marks = {
      1: '1x',
      25: '25x',
      50: '50x',
      75: '75x',
      100: '100x',
      125: '125x',
      150: {
      style: {
      color: '#00b5eb',
      },
      label: <strong>150x</strong>,
      },
    };

    const marks1 = {
      1: '1x',
      10: '10x',
      20: '20x',
      30: '30x',
      40: '40x',
      50: {
      style: {
      color: '#00b5eb',
      },
      label: <strong>50x</strong>,
      },
    };

    return (
      <div>
      <div className="container-fluid">
      <nav className="navbar navbar-expand-md fixed-top customNavTrade wow fadeInDown" data-wow-delay=".2s">
          <Link to="/" className="navbar-brand"><img src={Logo} alt="GlobalCryptoX" /></Link>
          <div className="header-overview">
            <div className="selectCoinType">
              <div className="btn-group">
                <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {localStorage.getItem('curpair')} Perpetual
                </button>
                <ul className="dropdown-menu">
                  <li className="dropdown-header">
                    <ul>
                      { records.map((item,i)=>{
                       if (item.first_currency == 'BTC') {
                      var icon = Selicon1;
                      } else if(item.first_currency == 'XRP') {
                      var icon = Selicon4;
                      }else if(item.first_currency == 'LTC') {
                      var icon = Selicon5;
                      }else if(item.first_currency == 'BCH') {
                      var icon = Selicon6;
                      }else if(item.first_currency == 'ETH') {
                      var icon = Selicon2;
                      }

                      return <li className={(this.state.firstcurrency==item.first_currency && this.state.secondcurrency==item.second_currency)?"active":""} id={item.first_currency+"_"+item.second_currency} onClick={this.pairChange.bind(this)}><img src={icon} />{item.first_currency+item.second_currency}</li>

                      })
                      }
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div className="headerOverviewGroup">

             <div className="headerOverviewStatus">
               <h5><small>Spot Price</small><span className={this.state.spotpricecolor}>{ ((typeof this.state.markprice != 'undefined')?parseFloat(this.state.markprice).toFixed(this.state.floating):0)+' $' }</span></h5>
              </div>
              <div className="headerOverviewStatus">
               <h5><small>Last Price</small>{ ((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(this.state.pricedet[0].close).toFixed(this.state.floating):0)+' $' }</h5>
              </div>
              <div className="headerOverviewStatus">
              {
                (this.state.change<0)?
               <h5 className="pinkText"><small>24H Change</small>{ ((this.state.change!='')?parseFloat(this.state.change).toFixed(2):0) }%</h5>:
                <h5 className="greenText"><small>24H Change</small>{ ((this.state.change!='')?parseFloat(this.state.change).toFixed(2):0) }%</h5>

              }
              </div>
              <div className="headerOverviewStatus">
               <h5><small>24H High</small>{ ((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(this.state.pricedet[0].high).toFixed(this.state.floating):0)+' $' }</h5>
              </div>
              <div className="headerOverviewStatus">
               <h5><small>24H Low</small>{ ((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(this.state.pricedet[0].low).toFixed(this.state.floating):0)+' $' }</h5>
              </div>
              <div className="headerOverviewStatus">
               <h5><small>24H Turnover</small>{ (typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(Math.abs(this.state.btcvolume)).toFixed(3):0 } {'$'}</h5>
              </div>
          </div>
          </div>

          <div className="collapse navbar-collapse" id="navbarResponsive">
         { (typeof user.email != 'undefined')?
            <ul className="navbar-nav ml-auto">
             <li class="nav-item">
                <a class="nav-link themeChangeLink" onClick={this.toggleMode}><span class="bg-sunIcon iconNav"></span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#"><span className="bg-mobileIcon iconNav"></span></a>
              </li>
              {/*<li className="nav-item d-none d-md-block">
                <a className="nav-link" href="#"><span className="bg-bellIcon iconNav"></span></a>
              </li>*/}

             <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Trade
                  </a>
                  <div className="dropdown-menu sm-menu">
                   <Link to={"/Spot/"+localStorage.getItem('curpair1')} className="dropdown-item nav-link">Spot</Link>
                   <Link to={"/Trade/"+localStorage.getItem('curpair')} className="dropdown-item nav-link">Derivatives</Link>
                  </div>
              </li>
              <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Finance
                  </a>
                  <div className="dropdown-menu sm-menu">
                    <Link to="/Locker" className="dropdown-item">Finance </Link>
                  </div>
              </li>


              <li className="nav-item">
                <Link to="/MyAssets" className="nav-link">Wallets</Link>
              </li>

			        <li className="nav-item">
			          <Link to="/AssetsHistory" className="nav-link">History</Link>
			        </li>
              <li className="nav-item dropdown dmenu">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  {(typeof user.email != 'undefined')?(user.email.split('@')[0].length<8)?user.email.split('@')[0]:user.email.split('@')[0].substring(0, 7):''}
                  </a>
                  <div className="dropdown-menu sm-menu">
                    <Link  to="/Settings" className="dropdown-item">Settings</Link>
                    <Link  to="/Bonus" className="dropdown-item">Bonus</Link>
                    <Link  to="/Leverage" className="dropdown-item">Leverage</Link>
                    <Link  to="/Referral_Program" className="dropdown-item">Referral Program</Link>

                    <a className="dropdown-item" onClick={this.onLogoutClick} href="#">Logout</a>
                  </div>
                </li>
            </ul> :
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <Link to="/Login" className="nav-link">Login</Link>
                </li>
                <li className="nav-item">
                    <Link to="/Register" className="nav-link">Register</Link>
                </li>
            </ul>
            }
          </div>
      </nav>
      <section className="tradeMain">
  <div className="row">
    <div className="col-9">
      <div className="row">

    <div className="col-md-8 pr-xl-0">
      <div className="chartTradeSection">

        <TVChartContainer className={ 'chartcontainer' } theme={this.state.theme} pair={this.state.chartpair} pairdatachange={this.pairdatachange}/>

      </div>
    </div>

      <div className="col-md-4 pr-md-0">
      <div className="tradeTableLeftSide darkBox orderBook">
        <div className="tableHead">
          <h4>Order Book</h4>
          <div className="inputGroup">
            <button className="btn"><img src={GreenPinkDot} id="greenpink" onClick={this.orderbookshow.bind(this)}/></button>
            <button className="btn"><img src={PinkDot} id="pink" onClick={this.orderbookshow.bind(this)}/></button>
            <button className="btn"><img src={GreenDot} id="green" onClick={this.orderbookshow.bind(this)}/></button>
          </div>
        </div>
        <div className="tradeTableTitle row mx-auto">
          <span className="col-4">Price</span>
          <span className="col-4">Qty</span>
          <span className="col-4">Total</span>
        </div>
        <Scrollbars ref='scrollbar' style={{ width: '100%', height: 200 }} renderTrackVertical={props => <div className="track-vertical"/>}>
        <div>
          {
            (this.state.orderbookshow=='greenpink' || this.state.orderbookshow=='pink')?
            this.state.sellOrder.map((item,i)=>{
              var toam = this.state.sellOrder[0].total;
              var precentage = item.total/toam*100;
              var classs       = (i%2)?'tradeTableBodyRow odd row mx-auto':'tradeTableBodyRow even row mx-auto';
              var ordvalue     = parseFloat(item.quantity)/parseFloat(item._id);
              var index        = (this.state.prevsellOrder).findIndex(x => (x._id) === item._id);
              index            = (this.state.prevsellOrder.length>0)?index:-2;
              var prevquantity = (this.state.prevsellOrder.length>0 && index!=-1 )?this.state.prevsellOrder[index].quantity:0;
              var highLightcls = (index==-1)?"highlight":(prevquantity!=0 && prevquantity!=item.quantity)?"highlight":'';
              return <div className={classs+' '+highLightcls} >
                  <span className="col-4 pinkText" onClick={this.fillprice.bind("test",Math.abs(item.quantity),item._id)}>{parseFloat(item._id).toFixed(5)}</span>
                  <span className="col-4" onClick={this.fillprice.bind("test",Math.abs(item.quantity),item._id)}>{parseFloat(Math.abs(item.quantity)).toFixed(5)}</span>
                  <span className="col-4"> <span class="bgProgress1" style={{width : precentage+'%'}}></span> <span className="bgProgressText1">{parseFloat(Math.abs(item.total)).toFixed(5)}</span></span>
                </div>;
            })
            :''
          }



        {
        (typeof this.state.pricedet != 'undefined' && this.state.pricedet.length > 0)?(

          <div className="tradeTableBodyRow even highLight row mx-auto" >
            <span className={(this.state.pricedet[0].close)?('col-6 pl-5 pinkText'):('col-6 pl-5 yellowText')}><i className="fas fa-caret-down "></i>{parseFloat(this.state.pricedet[0].close).toFixed(this.state.floating)+" $"}</span>
            <span className={this.state.spotpricecolor+' col-6 pl-5 text-center'} data-toggle="tooltip" data-placement="right" data-html="true" title="Spot price">{this.state.markprice+" $"}</span>

        </div>
        ):('')}


          {
            (this.state.orderbookshow=='greenpink' || this.state.orderbookshow=='green')?
            this.state.buyOrder.map((item,i)=>{
              var lastindex = this.state.buyOrder.length;
              var toam = this.state.buyOrder[lastindex-1].total;
              var precentage = item.total/toam*100;
              var classs       = (i%2)?'tradeTableBodyRow even row mx-auto':'tradeTableBodyRow odd row mx-auto';
              var ordvalue     = parseFloat(item.quantity)/parseFloat(item._id);
              var index        = (this.state.prevbuyOrder).findIndex(x => (x._id) === item._id);
              index            = (this.state.prevbuyOrder.length>0)?index:-2;
              var prevquantity = (this.state.prevbuyOrder.length>0 && index!=-1 )?this.state.prevbuyOrder[index].quantity:0;
              var highLightcls = (index==-1)?"highlight":(prevquantity!=0 && prevquantity!=item.quantity)?"highlight":'';

              return <div className={classs+' '+highLightcls}>
                  <span className=" col-4 greenText" onClick={this.fillprice.bind("test",item.quantity,item._id)}>{parseFloat(item._id).toFixed(5)}</span>
                  <span className="col-4" onClick={this.fillprice.bind("test",item.quantity,item._id)}>{parseFloat(item.quantity).toFixed(5)}</span>
                  <span className="col-4"> <span class="bgProgress" style={{width : precentage+'%'}}></span> <span className="bgProgressText">{parseFloat(item.total).toFixed(5)}</span></span>
                </div>;
            })
            :''
          }
       </div>
        </Scrollbars>
      </div>
      <div className="tradeTableLeftSide darkBox recentTrades">
        <div className="tableHead">
          <h4>Recent Trades</h4>
        </div>
        <div className="tradeTableTitle row mx-auto">
        <span className="col-3">Type</span>
          <span className="col-3">Price</span>
          <span className="col-3">Qty</span>
          <span className="col-3">Time</span>
        </div>
         <Scrollbars style={{ width: '100%' , height: 200 }}>
        <div className="tradeTableBody customScroll">

            {
            this.state.Rescentorder.map((item,i)=>{
               var data = new Date(item.created_at);
               let date1 =  data.getHours() +':'+ data.getMinutes() + ':'+data.getSeconds();
               var classs = (i%2)?'tradeTableBodyRow even row mx-auto':'tradeTableBodyRow odd row mx-auto';
               var typeclass = item.Type=="buy"?" col-3 greenText":" col-3 pinkText"
                return <div className={classs} data-toggle="tooltip">
                   <span className={typeclass} >
                          {item.Type=="buy"?"Buy":item.Type=="sell"?"Sell":""}

                        </span>
                        <span className="col-3">{parseFloat(item.Price).toFixed(5)}</span>
                        <span className="col-3">{parseFloat(Math.abs(item.filledAmount)).toFixed(5)}</span>
                        <span className="col-3">
                          {/* {date1} */}
                          {moment(item.created_at).format(
                                    " k:mm "
                                  )}
                        </span>

                      </div>;

            })
          }
          {(this.state.Rescentorder.length>20)?
          <div class="tradeTableBodyRow odd row mx-auto ">
          <a onClick={this.loadmoreRescentorder} class="d-block text-center mx-auto"><i class="fas fa-sort-amount-down-alt"></i> Load More</a></div>:''}

        </div>

        </Scrollbars>
      </div>
    </div>

    <div className="col-md-12 pr-xl-0">


      <div className="darkBox tradeFulltabbedTable">
          <nav>
          <div className="nav nav-tabs" id="nav-tab" role="tablist">
            <a className="nav-item nav-link active" id="nav-positions-tab" data-toggle="tab" href="#nav-positions" role="tab" aria-controls="nav-positions" aria-selected="true">Positions({this.state.curcontract.replace("_", "")})</a>
            <a className="nav-item nav-link" id="nav-closedPL-tab" data-toggle="tab" href="#nav-closedPL" role="tab" aria-controls="nav-closedPL" aria-selected="false">Closed P&L</a>
            <a className="nav-item nav-link" id="nav-active0-tab" data-toggle="tab" href="#nav-active0" role="tab" aria-controls="nav-active0" aria-selected="false">Pending {orderHistory.length}</a>

            <a className="nav-item nav-link" id="nav-filledOrder-tab" data-toggle="tab" href="#nav-filledOrder" role="tab" aria-controls="nav-filledOrder" aria-selected="false">Filled Order</a>
            <a className="nav-item nav-link" id="nav-tradeHistory-tab" data-toggle="tab" href="#nav-tradeHistory" role="tab" aria-controls="nav-tradeHistory" aria-selected="false">History</a>
          </div>
        </nav>
        <div className="tab-content px-xl-3" id="nav-tabContent">
          <div className="tab-pane fade show active" id="nav-positions" role="tabpanel" aria-labelledby="nav-positions-tab">
             <div className="table-responsive">
            <table id="positionsTable" className="table table-striped">
                                           <thead>
                                                <tr>
                                                    <th>Contracts</th>
                                                    <th>Qty</th>
                                                    <th>Type</th>
                                                    <th className="text-right">Value</th>
                                                    <th className="text-right">Price</th>
                                                    <th className="text-right"><span className="yellowText">Liq.Price</span></th>
                                                    <th className="text-right">Initial Margin</th>
                                                    <th className="text-right stdWidth">Unrealized P&L</th>
                                                    <th className="text-right">Daily Realized P&L</th>
                                                    <th className="text-right">TP/SL</th>
                                                    <th className="text-right">Close By</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                           {(position_details.length>0 && pos_quantity!=0)?
                                              <tr>
                                                <td><span className="yellowText">{pos_pairName}</span></td>
                                                <td><span className={(pos_quantity<0)?"pinkText":"greenText"}>{parseFloat(pos_quantity).toFixed(8)}</span></td>
                                                <td><span className="yellowText">{(pos_quantity>0)?"Buy":"Sell"}</span></td>
                                                <td className="text-right">{parseFloat((pos_quantity/pos_price)).toFixed(8)}</td>
                                                <td className="text-right">{parseFloat(pos_price).toFixed(this.state.floating)}</td>
                                                <td className="text-right"><span className="yellowText">{parseFloat(pos_liqprice).toFixed(this.state.floating)}</span></td>
                                                <td className="text-right">{parseFloat(pos_initial_margin).toFixed(8)+' BTC'+' ('+pos_leverage+'X)'} </td>
                                                <td className="text-right"><span className={(profitnlossbtc<0)?'pinkText':'greenText'}><div>{parseFloat(profitnlossbtc).toFixed(8)+' BTC'} <br /> {' ≈ '+profitnlossusd.toFixed(2)+' USD'}</div><div>{(profitnlossbtc<0)?'('+parseFloat(roe).toFixed(2)+'%)':'(+'+parseFloat(roe).toFixed(2)+'%)'}</div></span></td>

                                                <td className="text-right">{parseFloat(dailyprofitnlossbtc).toFixed(8)+' BTC'}<br/><span className="greyText">{' ≈ ' + dailyprofitnlossusd.toFixed(2)+' USD'}</span></td>
                                                <td>{tpprice}/{stprice} &nbsp; <a href="#" onClick={this.tpshow.bind(this)}><i class="far fa-edit yellowText"></i></a></td>
                                                <td>
                   <button className="btn btnGroupBorderBtn btn-sm px-1 py-0 btn-block fontSM" onClick={this.handleShow1.bind(this)}>Market</button></td>
                                              </tr>
                                              :
                                              <tr><td colspan="10" className="text-center">No data Available</td></tr>
                                           }

                                            </tbody>
                                        </table>
                                      </div>
          </div>
          <div className="tab-pane fade" id="nav-closedPL" role="tabpanel" aria-labelledby="nav-closedPL-tab">
             <div className="table-responsive">
                                             <Scrollbars style={{ width: '100%' , height: 200 }} renderTrackVertical={props => <div className="track-vertical"/>}>
             <table id="closedPLTable" className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Contracts</th>
                                                    <th>Closing Direction</th>
                                                    <th>Quantity</th>
                                                    <th>Entry Price</th>
                                                    <th>Exit Price</th>
                                                    <th>Closed P&L(Excl Fee)</th>
                                                    <th>Exit Type</th>
                                                    <th>Close Position Time</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              {
                                                closed_positions.map((item,i)=>{
                                                var pairName     = item.pairname?item.pairname:'';
                                                var closing_direction     = item.closing_direction?item.closing_direction:0;
                                                var quantity        = item.quantity?item.quantity:0;
                                                var entry_price        = item.entry_price?item.entry_price:0;
                                                var exit_price        = item.exit_price?item.exit_price:0;
                                                var profitnloss = item.profitnloss?item.profitnloss:0;
                                                var exit_type   = item.exit_type?item.exit_type:0;
                                                var createdDate    = item.createdDate?item.createdDate:0;
                                                var classs = (profitnloss>0)?"greenText":'pinkText';

                                                var data1        = new Date(createdDate);
                                                let date12       = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();


                                               // console.log(date12,'datezxc');
                                                return <tr>
                                                  <td className="yellowText">{pairName}</td>
                                                  <td>{closing_direction}</td>
                                                  <td>{parseFloat(quantity).toFixed(8)}</td>
                                                  <td>{parseFloat(entry_price).toFixed(this.state.floating)}</td>
                                                  <td>{parseFloat(exit_price).toFixed(this.state.floating)}</td>
                                                  <td className={classs}>{parseFloat(profitnloss).toFixed(8)+" BTC"}</td>
                                                  <td>{exit_type}</td>
                                                  <td>{date12}</td>
                                                  </tr>
                                                })
                                                }
                                            </tbody>
                                        </table>
                                            </Scrollbars>
                                      </div>
          </div>
          <div className="tab-pane fade" id="nav-active0" role="tabpanel" aria-labelledby="nav-active0-tab">
            <div className="table-responsive">
             <Scrollbars style={{ width: '100%' , height: 200 }} renderTrackVertical={props => <div className="track-vertical"/>}>
            <table id="active0Table" className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Contracts</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th>Filled Remaining</th>
                                                    <th>Order Value</th>
                                                    <th>Tp/SL</th>
                                                    <th>Type</th>
                                                    <th>Status</th>
                                                    <th>Order#</th>
                                                    <th>Order Time</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             {
                                                orderHistory.map((item,i)=>{
                                                var pairName = item.pairName?item.pairName:'';
                                                var quantity = item.quantity?item.quantity:0;
                                                var price = item.quantity?item.price:0;
                                                var filledAmount = item.filledAmount?item.filledAmount:0;
                                                var orderValue = item.orderValue?item.orderValue:0;
                                                var orderType = item.orderType?item.orderType:0;
                                                var orderDate = item.orderDate?item.orderDate:0;
                                                var classs = (quantity>0)?'greenText':'pinkText';
                                                var _id = item._id?item._id:0;
                                                var status = item.status=='0'?'New':'Partial';
                                                var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                               // console.log(orderDate,'orderDate');
                                                var data1 = new Date(orderDate);
                                                let date12 = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();


                                               // console.log(date12,'datezxc');
                                                return <tr>
                                                  <td className="yellowText">{pairName}</td>
                                                  <td><span className={classs}>{parseFloat(quantity).toFixed(8)}</span></td>
                                                  <td>{parseFloat(price).toFixed(this.state.floating)}</td>
                                                  <td>{parseFloat(filledAmount).toFixed(8)+'/'+parseFloat(Remaining).toFixed(8)}</td>
                                                  <td>{parseFloat(orderValue).toFixed(8)}</td>
                                                  <td>{'-/-'}</td>
                                                  <td>{orderType}</td>
                                                  <td>{status}</td>
                                                  <td>{_id}</td>
                                                  <td>{date12}</td>
                                                  <td>
                                                    <button className="btn btn-borderButton mr-1" onClick={this.cancelFunction.bind(this)} id={_id}> Cancel </button>
                                                  </td>
                                                  </tr>
                                                })
                                                }
                                            </tbody>
                                        </table>
                                       </Scrollbars>
                                      </div>
          </div>
          <div className="tab-pane fade" id="nav-conditional0" role="tabpanel" aria-labelledby="nav-conditional0-tab">
            <div className="table-responsive">
             <Scrollbars style={{ width: '100%' , height: 200 }} renderTrackVertical={props => <div className="track-vertical"/>}>
            <table id="conditional0Table" className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Contracts</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th>Trigger Price</th>
                                                    <th>Order Value</th>
                                                    <th>Price (Distance)</th>
                                                    <th>Type</th>
                                                    <th>Status</th>
                                                    <th>Order#</th>
                                                    <th>Order Time</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                             <tbody>
                                             {
                                                Conditional_details.map((item,i)=>{
                                                var pairName       = item.pairName?item.pairName:'';
                                                var quantity       = item.quantity?item.quantity:0;
                                                var price          = item.quantity?item.price:0;
                                                var filledAmount   = item.filledAmount?item.filledAmount:0;
                                                var orderValue     = item.orderValue?item.orderValue:0;
                                                var orderType      = item.orderType?item.orderType:0;
                                                var trigger_price  = item.orderType?item.trigger_price:0;
                                                var trigger_type   = item.orderType?item.trigger_type:0;
                                                var orderDate      = item.orderDate?item.orderDate:0;
                                                var classs         = (quantity>0)?'greenText':'pinkText';
                                                var _id            = item._id?item._id:0;
                                                var status         = item.status=='0'?'New':'Partial';
                                                var Remaining      = parseFloat(quantity) - parseFloat(filledAmount);
                                                var price_distance = parseFloat(this.state.last_price) - parseFloat(price);
                                               // console.log(orderDate,'orderDate');
                                                var data1          = new Date(orderDate);
                                                let date12         = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
                                               // console.log(date12,'datezxc');
                                                return <tr>
                                                  <td className="yellowText">{pairName}</td>
                                                  <td><span className={classs}>{parseFloat(quantity).toFixed(8)}</span></td>
                                                  <td>{parseFloat(price).toFixed(this.state.floating)}</td>
                                                  <td>{(price_distance>0)?'<='+trigger_price+' ('+trigger_type+')':'>='+trigger_price+' ('+trigger_type+')'}</td>
                                                  <td>{parseFloat(orderValue).toFixed(8)}</td>
                                                  <td>{this.state.last_price+'('+price_distance+')'}</td>
                                                  <td>{orderType}</td>
                                                  <td>{status}</td>
                                                  <td>{_id}</td>
                                                  <td>{date12}</td>
                                                  <td><button className="btn btn-borderButton mr-1" onClick={this.cancelFunction.bind(this)} id={_id}> Cancel </button></td>
                                                  </tr>
                                                })
                                                }
                                            </tbody>
                                        </table>
                                        </Scrollbars>
                                      </div>
          </div>
          <div className="tab-pane fade" id="nav-filledOrder" role="tabpanel" aria-labelledby="nav-filledOrder-tab">
            <div className="table-responsive">
             <Scrollbars style={{ width: '100%' , height: 200 }} renderTrackVertical={props => <div className="track-vertical"/>}>
            <table id="filledOrderTable" className="table table-striped rembordor">
                                            <thead>
                                                <tr>
                                                    <th>Contracts</th>
                                                    <th>Quantity</th>
                                                    <th>Filled Remaining</th>
                                                    <th>Exec Price</th>
                                                    <th>Price</th>
                                                    <th>Type</th>
                                                    <th>Order#</th>
                                                    <th>Time</th>
                                                </tr>
                                            </thead>

                                             {
                                                Filleddetails.map((item,i)=>{
                                                 // console.log(item,'itemsss');
                                                 var quantity     = item.quantity?item.quantity:0;
                                                 var price        = item.price?item.price:0;
                                                 var _id          = item._id?item._id:0;
                                                 var classs       = (quantity>0)?'greenText':'pinkText';
                                                 var filledAmount = item.filledAmount?item.filledAmount:0;
                                                 var Remaining    = parseFloat(quantity) - parseFloat(filledAmount);
                                                 return ( <tbody>
                                                      { item.filled.map((itemdata,i)=>{
                                                        var created_at = itemdata.created_at?itemdata.created_at:'';
                                                        var data1      = new Date(created_at);
                                                        let date12     = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
                                                      return (<tr>
                                                        <td className="yellowText">{itemdata.pairname}</td>
                                                        <td>{parseFloat(quantity).toFixed(8)}</td>
                                                         <td>{parseFloat(filledAmount).toFixed(8)+'/'+parseFloat(Remaining).toFixed(8)}</td>
                                                        <td>{parseFloat(itemdata.Price).toFixed(this.state.floating)}</td>
                                                        <td>{parseFloat(price).toFixed(this.state.floating)}</td>
                                                        <td>{itemdata.Type}</td>
                                                        <td>{_id}</td>
                                                        <td>{date12}</td>
                                                        </tr>
                                                      );
                                                    })
                                                    }
                                                     </tbody>);
                                                })
                                                }



                                        </table>
                                        </Scrollbars>
                                      </div>
          </div>
          <div className="tab-pane fade" id="nav-tradeHistory" role="tabpanel" aria-labelledby="nav-tradeHistory-tab">
            <div className="table-responsive">
             <Scrollbars style={{ width: '100%' , height: 200 }} renderTrackVertical={props => <div className="track-vertical"/>}>
            <table id="tradeHistoryTable" className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Contracts</th>
                                                    <th>Quantity</th>
                                                    <th>Filled Remaining</th>
                                                    <th>Exec Price</th>
                                                    <th>Price</th>
                                                    <th>Trigger Price</th>
                                                    <th>Order Value</th>
                                                    <th>Type</th>
                                                    <th>Status</th>
                                                    <th>Order#</th>
                                                    <th>Order Time</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             {
                                                Histroydetails.map((item,i)=>{
                                               var pairName     = item.pairName?item.pairName:'';
                                               var quantity     = item.quantity?item.quantity:0;
                                               var filledAmount = item.filledAmount?item.filledAmount:0;
                                               var e_price      = item.filled.length>0?item.filled[0].Price:0;
                                               var price        = item.quantity?item.price:0;
                                               var orderValue   = item.orderValue?item.orderValue:0;
                                               var orderType    = item.orderType?item.orderType:0;
                                               var orderDate    = item.orderDate?item.orderDate:0;
                                               var classs       = item.buyorsell=='buy'?'greenText':'pinkText';
                                               var _id          = item._id?item._id:0;
                                               var status1      = item.status;
                                               var Remaining    = parseFloat(quantity) - parseFloat(filledAmount);
                                               var data         = new Date(orderDate);
                                               let date1        = data.getFullYear() + '-' + (data.getMonth() +1) + '-' + data.getDate() + ' ' + data.getHours() +':'+ data.getMinutes() + ':'+data.getSeconds();
                                                return <tr>
                                                  <td>{pairName}</td>
                                                  <td><span className={classs}>{parseFloat(quantity).toFixed(8)}</span></td>
                                                  <td>{parseFloat(filledAmount).toFixed(8)+'/'+parseFloat(Remaining).toFixed(8)}</td>
                                                  <td>{e_price}</td>
                                                  <td>{price}</td>
                                                  <td>{'-/-'}</td>
                                                  <td>{orderValue}</td>
                                                  <td>{orderType}</td>
                                                 {(status1 == 0)?
                                                  <td >New</td>
                                                   :''}
                                                   {(status1 == 1)?
                                                  <td >Completed</td>
                                                   :''}
                                                   {(status1 == 2)?
                                                  <td >Partial</td>
                                                   :''}
                                                   {(status1 == 3)?
                                                  <td >Cancel</td>
                                                   :''}
                                                   {(status1 == 4)?
                                                  <td >Conditional</td>
                                                   :''}
                                                  <td>{_id}</td>
                                                  <td>{date1}</td>
                                                  </tr>
                                                })
                                                }
                                            </tbody>

                                        </table>
                                        </Scrollbars>
                                      </div>
          </div>
        </div>

      </div>
       <div className="darkBox tradeFulltabbedTable">
      <div className="tableHead"><h4>Assets Overview</h4></div>
      <div className="table-responsive">
       {this.state.assetOverview?
           <table id="positionsTable" className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Coin</th>
                                                    <th>Equity</th>
                                                    <th>Available Balance</th>
                                                    <th>Position Margin</th>
                                                    <th>Order Margin</th>
                                                    <th>Total Realized P&L</th>
                                                    <th>Unrealized P&L</th>
                                                    <th>Risk Limit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               {
                                                this.state.assetOverview.map((item,i)=>{
                                                    var currencySymbol     = item.currencySymbol?item.currencySymbol:'';
                                                    var assetbalance       = item.balance?item.balance:'0';
                                                    var index              = this.state.allposition_details.findIndex(x => (x.firstCurrency) === currencySymbol);

                                                    var riskindex              = this.state.records.findIndex(x => (x.first_currency) === currencySymbol);

                                                    var parposquantity     = (index!=-1)?this.state.allposition_details[index].quantity:0;
                                                    var parposprice        = (index!=-1)?this.state.allposition_details[index].price:0;
                                                    var parposleverage     = (index!=-1)?this.state.allposition_details[index].leverage:0;

                                                    var pos_initial_margin = (parseFloat(parposquantity)/parseFloat(parposprice))*((100/parseFloat(parposleverage))/100);
                                                    var profitnloss        = [ (1/parseFloat(parposprice))  - (1/parseFloat(this.state.markprice)) ] * parseFloat(parposquantity);
                                                    var unprofitnloss        = [ (1/parseFloat(parposprice))  - (1/parseFloat(this.state.last_price)) ] * parseFloat(parposquantity);
                                                    var profitnlossper     = (parseFloat(profitnloss)/parseFloat(pos_initial_margin))*100;
                                                    var profitnlossusd     = (profitnloss*parseFloat(this.state.last_price));

                                                    var equitybal          = +assetbalance+ (+pos_initial_margin);

                                                    var risk_limit = (riskindex!=-1)?this.state.records[riskindex].risk_limit:0;

                                                    return <tr>
                                                      <td>{currencySymbol}</td>
                                                      <td>{parseFloat(isNaN(equitybal)?0:equitybal).toFixed(8)}</td>
                                                      <td>{parseFloat(assetbalance).toFixed(8)}</td>
                                                      <td>{parseFloat(isNaN(pos_initial_margin)?0:pos_initial_margin).toFixed(8)}</td>
                                                      <td>{parseFloat(isNaN(pos_initial_margin)?0:pos_initial_margin).toFixed(8)}</td>
                                                      <td>{parseFloat(isNaN(unprofitnloss)?0:profitnloss).toFixed(8)}</td>
                                                      <td>{parseFloat(isNaN(profitnloss)?0:profitnloss).toFixed(8)}</td>
                                                      <td>{risk_limit}</td>


                                                      </tr>
                                                })
                                                }

                                            </tbody>
                                        </table>:''}
                                        </div>
      </div>
    </div>
      </div>
    </div>
    <div className="col-3">


      <div className="darkBox tradeLimitMarket">

   <nav>
      <div className="nav nav-tabs" id="nav-tab" role="tablist">
         <a className="nav-item nav-link active" id="nav-tradeLimit-tab" data-toggle="tab" href="#nav-tradeLimit" role="tab" aria-controls="nav-tradeLimit" aria-selected="true" onClick={this.ordertype_changes.bind(this)}>Market</a>

         <a className="nav-item nav-link " id="nav-tradeLimit-tab" data-toggle="tab" href="#nav-tradeLimit" role="tab" aria-controls="nav-tradeLimit" aria-selected="true" onClick={this.ordertype_changes.bind(this)}>Limit</a>


      </div>
   </nav>
   <div className="tab-content" id="nav-tabContent-tradeLimitMarket">
    <div className="">
    { this.state.limit?
      <Select
         styles={customStyles}
         width='100%'
         menuColor='red'
         options={options}
         value={timeinforcetype}
         onChange={this.changetimeinforce}
         />
         :''}
   </div>
      <div className="form-group">
      <div className="mt-3 mb-4">
      <Slider min={1} max={this.state.maxleverage} marks={(this.state.firstcurrency=='BTC')?marks:marks1} onChange={this.handleChangeComplete} onAfterChange={this.afterchange} included={false} value={this.state.value} />
      </div>
         <p id="leverageCurrentSliderValLabel" className="text-center">Leverage <span id="leverageSliderVal">{value} %</span></p>
      </div>
      <div className="form-group">
            <div className="row">
               <div className="col-md-3 d-none">

               </div>
               <div className="col-md-9 mx-auto">
                  <div className="def-number-input safari_only colorBlue">
                     <div class="btn-group">
      {(this.state.buyorsell=='buy')?
      <button class="btn btnGroupBorderBtn btnBuyBlue" onClick={this.handleShow.bind(this)}>Buy</button>:
      <button class="btn btnGroupBorderBtn btnBuyYellow" onClick={this.handleShow.bind(this)}>Buy</button>
      }
      {(this.state.buyorsell=='sell')?
      <button class="btn btnGroupBorderBtn btnSellBlue" onClick={this.handleShow.bind(this)}>Sell</button>:
      <button class="btn btnGroupBorderBtn btnSellYellow" onClick={this.handleShow.bind(this)}>Sell</button>
      }
      </div>

                  </div>
               </div>
            </div>
         </div>

      <div className="tab-pane fade show active" id="nav-tradeLimit" role="tabpanel" aria-labelledby="nav-tradeLimit-tab">
         { this.state.limit?
          <div>
         <div className="form-group">
            <div className="row">
               <div className="col-md-4">
                  <label className="marginTopLabel">Price - <span>{this.state.secondcurrency}</span></label>
               </div>
               <div className="col-md-8">
                  <div className="def-number-input safari_only colorBlue">
                     <input className="quantity" min="0" name="price" id="price" value="1" type="text" value={this.state.price} onChange={this.onChange}/>

                  </div>
               </div>
            </div>
         </div>
         <div className="form-group">
            <div className="row">
               <div className="col-md-6">
                  <label>Current Price - <span>{this.state.secondcurrency}</span></label>
               </div>
               <div className="col-md-6">
                  <div className="def-number-input safari_only colorBlue">
                     {this.state.markprice+" $"}

                  </div>
               </div>
            </div>
         </div>
         </div>
         :
           <div className="form-group">
            <div className="row">
               <div className="col-md-6">
                  <label>Current Price - <span>{this.state.secondcurrency}</span></label>
               </div>
               <div className="col-md-6">
                  <div className="def-number-input safari_only colorBlue">
                     {this.state.markprice+" $"}

                  </div>
               </div>
            </div>
         </div>
          }

         {  this.state.conditional?
          <div>
           <div className="form-group">
              <div className="row">
                <div className="col-md-5">
                 <div className="btn-group">
                   <button className="btn btnGroupBorderBtn" onClick={this.conditionalPrice.bind(this)}>Limit</button>
                   <button className="btn btnGroupBorderBtn" onClick={this.conditionalPrice.bind(this)}>Market</button>
                 </div>
                </div>
                <div className="col-md-7">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" readOnly={this.state.readOnly} name="price" id="price" value="1" type="number" value={this.state.price} onChange={this.onChange} />
                   <button onClick={this.updownvalue.bind('pri','price','plus')} className="2 price plus"></button>
                     <button onClick={this.updownvalue.bind('pri','price','minus')} className="2 price minus"></button>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="row">
                <div className="col-md-5">
                  <label>Trigger Price <span>{this.state.second_currency}</span></label>
                </div>
                <div className="col-md-7">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="trigger_price" id="trigger_price" type="number" value={this.state.trigger_price} onChange={this.onChange} />
                    <button onClick={this.updownvalue.bind('pri','trigger_price','plus')} className="2 price plus"></button>
                     <button onClick={this.updownvalue.bind('pri','trigger_price','minus')} className="2 price minus"></button>
                  </div>
                </div>
                <div class="col-md-12">
                  <div className="btn-group btn-block mt-3">
                    <button className="btn btn-borderButton mr-1" onClick={this.triggerPrice.bind(this)}>Last</button>
                    <button className="btn btn-borderButton mr-1" onClick={this.triggerPrice.bind(this)}>Index</button>
                    <button className="btn btn-borderButton mr-1" onClick={this.triggerPrice.bind(this)}>Mark</button>
                  </div>
                </div>
              </div>
            </div>
            </div>
            :'' }
         <div className="form-group">
            <div className="row">
               <div className="col-md-4">
                  <label>Qty - <span>{this.state.firstcurrency}</span></label>
               </div>
               <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                     <input className="quantity" min="0" name="quantity" id="quantity" value={this.state.quantity} onChange={this.onChange} type="number" />
                     <button onClick={this.updownvalue.bind('quan','quantity','plus')} className="2 plus"></button>
                     <button onClick={this.updownvalue.bind('quan','quantity','minus')} className="2 minus"></button>
                  </div>
                 {/* <div className="input-group mt-2">
                     <button className="btn btn-borderButton mr-1" onClick={this.balanceperc}>25%</button>
                     <button className="btn btn-borderButton mr-1" onClick={this.balanceperc}>50%</button>
                     <button className="btn btn-borderButton mr-1" onClick={this.balanceperc}>75%</button>
                     <button className="btn btn-borderButton" onClick={this.balanceperc}>100%</button>
                  </div>*/}
               </div>
            </div>
         </div>
         <ul className="list2ColumnJustify">
            <li>
               <label>Order Value</label>
               <span>{parseFloat(this.state.order_value).toFixed(8)+' BTC'}</span>
            </li>
            <li>
               <label>Balance</label>
               <span>{parseFloat(isNaN(this.state.balance)?0:this.state.balance).toFixed(8)+' BTC'}</span>
            </li>
            <li>
               <label>Margin impact</label>
               <span>{parseFloat(this.state.order_cost).toFixed(8)+' BTC'}</span>
            </li>
           { this.state.limit?
            <li>
               <div className="checkbox"><label><input name="post_only" id="post_only" value="1" onChange={this.onChange} type="checkbox" /><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText">Post only</span></label></div>
               <div className="checkbox"><label><input name="reduce_only" id="reduce_only" value="1" onChange={this.onChange} type="checkbox" /><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText">Reduce only</span></label></div>
            </li>:''}
         </ul>
         {this.state.alertmessage!=''?
          <div class="quadrat">{this.state.alertmessage}</div>:''
          }
          { (typeof user.email != 'undefined')?
         <div className="form-group input-group buySellBtn">
            { (!this.state.buydisable)?
            <button className="btn buttonType2 mr-2" onClick={this.handleShow22.bind(this)} disabled={!this.state.disable}>
            <span className="spinner-border spinner-border-sm"></span> Send order
            </button>:
            <button className="btn buttonType2 mr-2" onClick={this.handleShow22.bind(this)} disabled={!this.state.disable}>
            Send order
            </button>
            }

         </div>
         :
         <div className="form-group input-group buySellBtn">
            <Link to="/Login" class="btn btnBlue hvr-shadow float-right btn-block mb-3 py-2">Login</Link>

         </div>
        }
      </div>

      <div className="tab-pane fade" id="nav-conditional" role="tabpanel" aria-labelledby="nav-conditional-tab">
        <div className="form-group">
              <div className="row">
                <div className="col-md-5">
                 <div className="btn-group">
                   <button className="btn btnGroupBorderBtn">Limit</button>
                   <button className="btn btnGroupBorderBtn">Market</button>
                 </div>
                </div>
                <div className="col-md-7">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="quantity" value="1" type="number" />
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="row">
                <div className="col-md-5">
                  <label>Trigger Price <span>USD</span></label>
                </div>
                <div className="col-md-7">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="quantity" value="0.25658748" type="number" />
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                  </div>
                </div>
                <div class="col-md-12">
                  <div className="btn-group btn-block mt-3">
                    <button className="btn btn-borderButton mr-1">Last</button>
                    <button className="btn btn-borderButton mr-1">Index</button>
                    <button className="btn btn-borderButton mr-1">Mark</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <div className="row">
                <div className="col-md-4">
                  <label>Qty - <span>BTC</span></label>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="quantity" value="0.25658748" type="number" />
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                  </div>
                  {/*<div className="input-group mt-2">
                    <button className="btn btn-borderButton mr-1">25%</button>
                    <button className="btn btn-borderButton mr-1">50%</button>
                    <button className="btn btn-borderButton mr-1">75%</button>
                    <button className="btn btn-borderButton">100%</button>
                  </div>*/}
                </div>
              </div>
            </div>
            <ul className="list2ColumnJustify">
            <li>
               <label>Order Value</label>
               <span>{parseFloat(this.state.order_value).toFixed(8)+' '+this.state.firstcurrency}</span>
            </li>
            <li>
               <label>Balance</label>
               <span>{parseFloat(this.state.balance).toFixed(8)+' '+this.state.firstcurrency}</span>
            </li>
            <li>
               <label>Margin impact</label>
               <span>{parseFloat(this.state.order_cost).toFixed(8)+' '+this.state.firstcurrency}</span>
            </li>
            { this.state.limit?
            <li>
               <div className="checkbox"><label><input name="post_only" value="1" type="checkbox" /><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText">Post only</span></label></div>
               <div className="checkbox"><label><input name="reduce_only" value="1" type="checkbox" /><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText">Reduce only</span></label></div>
            </li>:''}
         </ul>
         <div className="form-group input-group buySellBtn">
            { (!this.state.buydisable)?
            <button className="btn btn-btnBuy mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
            <span className="spinner-border spinner-border-sm"></span> Buy
            </button>:
            <button className="btn btn-btnBuy mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
            Buy
            </button>
            }
            { (!this.state.selldisable)?
            <button className="btn btn-btnSell mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
            <span className="spinner-border spinner-border-sm"></span> Sell
            </button>:
            <button className="btn btn-btnSell mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
            Sell
            </button>
            }
         </div>
      </div>

   </div>
</div>





      <div className="darkBox contractDetailsBTCUSDT tradeFulltabbedTable">
         <nav>
          <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <a className="nav-item nav-link active" id="nav-trollBox-tab" data-toggle="tab" href="#nav-trollBox" role="tab" aria-controls="nav-trollBox" aria-selected="true">Troll Box</a>
            <a className="nav-item nav-link" id="nav-contractDetails-tab" data-toggle="tab" href="#nav-contractDetails" role="tab" aria-controls="nav-contractDetails" aria-selected="false">Contract Details</a>
          </div>
        </nav>
        <div className="tab-content px-3 px-sm-0" id="nav-tabContent">
          <div className="tab-pane fade show active" id="nav-trollBox" role="tabpanel" aria-labelledby="nav-trollBox-tab">
          <div className="trollBox">


            <div className="messages">
              <Scrollbars style={{ width: '100%', height: 500 }}>
                  <ul className="customScroll">
                    {this.state.messages.map(message => {
                      var modclass = (message.moderator==0)?"nonmoderatorName":"moderatorName";
                        return (
                            <li><span className={modclass}>{message.author} :</span> {message.message}</li>
                        )
                    })}
                </ul>
                </Scrollbars>

            </div>
            { (typeof user.email != 'undefined' && this.state.blocktime<new Date())?
             <div className="input-group bottomFixed trollBoxForm">
              <input className="form-control" type="text" name="Message" value={this.state.message} onChange={ev => this.setState({message: ev.target.value})}/>
              <input type="submit" onClick={this.sendMessage} name="" value="Send" />
            </div>
            : '' }
          </div>
        </div>
          <div className="tab-pane fade" id="nav-contractDetails" role="tabpanel" aria-labelledby="nav-contractDetails-tab">
            <div>
             <Scrollbars style={{ width: '100%', height: 500 }}>
          <ul className="list2ColumnJustify">
            <li>
              <label>Contract Details</label>
              <span>{(perpetual.tiker_root)?perpetual.tiker_root:7050.00}</span>
            </li>
            <li>
              <label>Last Traded Price</label>
              <span>{parseFloat(this.state.last_price).toFixed(this.state.floating)}</span>
            </li>
            <li>
              <label>Spot Price</label>
              <span>{this.state.markprice}</span>
            </li>
             <li>
              <label>Funding rate</label>
              <span>{(perpetual.funding_rate)?parseFloat(perpetual.funding_rate).toFixed(4):0.01}{" in "+this.state.timeuntillfunding+" hours"}</span>
            </li>

            <li>
              <label>24hr Volume</label>
              <span>{(perpetual.volume)?perpetual.volume:0}</span>
            </li>

            <li>
              <label>Contract Value</label>
              <span>{(perpetual.contract_size)?perpetual.contract_size:1} USD</span>
            </li>

            <li>
              <label>Risk Limit</label>
              <span>{(perpetual.risk_limit)?perpetual.risk_limit:50}BTC</span>
            </li>
            <li>
              <label>Risk Step</label>
              <span>{(perpetual.risk_step)?perpetual.risk_step:50}BTC</span>
            </li>

            <li>
              <label>Settlement</label>
              <span>This contract does not settle.</span>
            </li>
            <li>
              <label>Commission</label>
              <span><a href="/Fee">Click to see more details</a></span>
            </li>
            <li>
              <label>Minimum Price Increment</label>
              <span>{(perpetual.minpriceincrement)?perpetual.minpriceincrement:0.5} USD</span>
            </li>
            <li>
              <label>Max Price</label>
              <span>{(perpetual.maxpriceincrement)?perpetual.maxpriceincrement:100000000} USD</span>
            </li>
            <li>
              <label>Lot Size</label>
              <span>{(perpetual.lotsize)?perpetual.lotsize:1}</span>
            </li>
          </ul>
           </Scrollbars>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


      <Modal show={this.state.show} onHide={this.handleClose}  aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
                  <Modal.Title>Order Details <small>{this.state.ordertype} {this.state.buyorsell}</small></Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className="form-group">
              <div className="row">
                <div className="col-md-4">
                  <div className="checkbox pt-2"><label><input name="takeprofitcheck" id="takeprofitcheck" onChange={this.onChange} value={this.state.takeprofitcheck} type="checkbox" value="1"/><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText checkConfirmationText">Take Profit USD</span></label></div>
                </div>

                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" readOnly={this.state.tpreadOnly} name="takeprofit" id="takeprofit" onChange={this.onChange} value={this.state.takeprofit} type="number" />
                    <button onClick={this.updownvalue.bind('tak','takeprofit','plus')} className="plus"></button>
                    <button onClick={this.updownvalue.bind('tak','takeprofit','minus')} className="minus"></button>
                  </div>

                </div>
              </div>

            </div>
            <div className="form-group">
              <div className="row">
               <div className="col-md-4">
                  <div className="checkbox pt-2"><label><input name="stopcheck" id="stopcheck" onChange={this.onChange} value={this.state.stopcheck} type="checkbox" value="1"/><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText checkConfirmationText">Stop Loss USD</span></label></div>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="stopprice" readOnly={this.state.stopreadOnly} id="stopprice" onChange={this.onChange} value={this.state.stopprice} type="number" />
                    <button onClick={this.updownvalue.bind('sto','stopprice','plus')}  className="plus"></button>
                    <button onClick={this.updownvalue.bind('sto','stopprice','minus')}  className="minus"></button>
                  </div>

                </div>
              </div>

            </div>
         <div className="table-responsive">
            <table className="table table-bordered tradeBuy">
                <tr>
                  <td align="right" width="50%">Order Price</td>
                  <td><span>{(this.state.ordertype=='Market')?this.state.ordertype+' '+this.state.secondcurrency:this.state.price+' '+this.state.secondcurrency}</span></td>
                </tr>
                <tr>
                  <td align="right" width="50%">Order Value</td>
                  <td>{this.state.order_value+' BTC'}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Order Cost</td>
                  <td>{this.state.order_cost+' BTC'}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Available Balance</td>
                  <td>{parseFloat(this.state.balance).toFixed(8)+' BTC'}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Position Leverage</td>
                  <td>{this.state.value+'x'}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Total Position Qty after execution</td>
                  <td>{this.state.quantity}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Take Profit</td>
                  <td>{this.state.takeprofit+' '+this.state.secondcurrency}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Stop Loss</td>
                  <td>{this.state.stopprice+' '+this.state.secondcurrency}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Mark Price</td>
                  <td>{this.state.markprice+' '+this.state.secondcurrency}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Estimated Liq. Price</td>
                  <td>{parseFloat(this.state.Liqprice).toFixed(4)+' '+this.state.secondcurrency} </td>
                </tr>
                <tr>
                  <td align="right" width="50%">Mark Price to Liq. Price distance</td>
                  <td> {Math.abs((parseFloat(this.state.markprice) - parseFloat(this.state.Liqprice)).toFixed(2)) +' '+this.state.secondcurrency}</td>
                </tr>
                <tr>
                  <td align="right" width="50%">Time in Force</td>
                  <td>GoodTillCancelled</td>
                </tr>
            </table>
          </div>

           {(this.state.alertmessage1!='' && this.state.stopreadOnly==false)?
          <div class="quadrat">{this.state.alertmessage1}</div>:''
          }
           {(this.state.alertmessage2!='' && this.state.tpreadOnly==false)?
          <div class="quadrat">{this.state.alertmessage2}</div>:''
          }

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary btnDefaultNewBlue" onClick={this.handleClose}>
            Cancel
          </Button>
          <Button variant="primary btnDefaultNew" onClick={this.orderPlacing}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>

       <Modal show={this.state.show1} onHide={this.handleClose1}  aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title>Close By <small>{this.state.popuptype} {(position_details.length>0 && position_details[0].quantity>0)?'Sell':'Buy'}</small></Modal.Title>
        </Modal.Header>
        <Modal.Body>

       { (this.state.popuptype=='Limit')?
        <div className="form-group">
              <div className="row">
                <div className="col-md-4">
                  <div className="checkbox pt-2"><label>Close Price {this.state.firstcurrency}</label></div>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="close_price" id="close_price" value={this.state.close_price} type="number" onChange={this.onChange}/>
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                  </div>
                </div>
              </div>
            </div>
            : '' }

            <div className="form-group">
              <div className="row">
                <div className="col-md-4">
                  <div className="checkbox pt-2"><label>Number of Positions Closed {this.state.secondßcurrency}</label></div>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" name="close_quantity" id="close_quantity" value={this.state.close_quantity} type="number" onChange={this.onChange}/>
                    <button onClick={this.updownvalue.bind('clo','close_quantity','plus')} className="plus"></button>
                    <button onClick={this.updownvalue.bind('clo','close_quantity','minus')} className="minus"></button>
                  </div>
                  <div className="input-group mt-2">
                    <button className="btn btn-borderButton mr-1" onClick={this.quantitydivide}>100%</button>
                    <button className="btn btn-borderButton mr-1" onClick={this.quantitydivide}>50%</button>
                    <button className="btn btn-borderButton mr-1" onClick={this.quantitydivide}>75%</button>
                    <button className="btn btn-borderButton" onClick={this.quantitydivide}>25%</button>
                  </div>
                </div>
              </div>
            </div>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary btnDefaultNewBlue" onClick={this.handleClose1}>
            Cancel
          </Button>
          <Button variant="primary btnDefaultNew" onClick={this.orderPlacing1}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>

       <Modal show={this.state.tpshow} onHide={this.tpClose}  aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title>Close By <small>{this.state.popuptype} {(position_details.length>0 && position_details[0].quantity>0)?'Sell':'Buy'}</small></Modal.Title>
        </Modal.Header>
        <Modal.Body>


        <div className="form-group">
              <div className="row">
               <div className="col-md-4">
                  <div className="checkbox pt-2"><label><input name="takeprofitcheck" id="takeprofitcheck" onChange={this.onChange} value={this.state.takeprofitcheck} type="checkbox" value="1"/><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText checkConfirmationText">Take Profit USD</span></label></div>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" readOnly={this.state.tpreadOnly} name="takeprofit" id="takeprofit" onChange={this.onChange} value={this.state.takeprofit} type="number" />
                    <button onClick={this.updownvalue.bind('tak','takeprofit','plus')} className="plus"></button>
                    <button onClick={this.updownvalue.bind('tak','takeprofit','minus')} className="minus"></button>
                  </div>
                </div>
              </div>
              {/*<div className="row">
                  <div className="col-md-12 col-offset-2">
                      <div className="input-group mt-2">
                          <button className="btn btn-borderButton mr-1">25%</button>
                          <button className="btn btn-borderButton mr-1">50%</button>
                          <button className="btn btn-borderButton mr-1">75%</button>
                          <button className="btn btn-borderButton">100%</button>
                          <button className="btn btn-borderButton">150%</button>
                          <button className="btn btn-borderButton">200%</button>
                      </div>
                  </div>
              </div>
              <div className="row">
                  <div class="col-md-12">
                      <div className="btn-group btn-block mt-3">
                          <button className="btn btn-borderButton mr-1" onClick={this.triggertp.bind(this)}>Last</button>
                          <button className="btn btn-borderButton mr-1" onClick={this.triggertp.bind(this)}>Index</button>
                          <button className="btn btn-borderButton mr-1" onClick={this.triggertp.bind(this)}>Mark</button>
                      </div>
                  </div>
              </div>*/}
            </div>

             <div className="form-group">
              <div className="row">
               <div className="col-md-4">
                  <div className="checkbox pt-2"><label><input name="stopcheck" id="stopcheck" onChange={this.onChange} value={this.state.stopcheck} type="checkbox" value="1"/><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText checkConfirmationText">Stop Loss USD</span></label></div>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" readOnly={this.state.stopreadOnly} name="stopprice" id="stopprice" onChange={this.onChange} value={this.state.stopprice} type="number" />
                    <button onClick={this.updownvalue.bind('sto','stopprice','plus')} className="plus"></button>
                    <button onClick={this.updownvalue.bind('sto','stopprice','plus')} className="minus"></button>
                  </div>
                </div>
              </div>
              {/*<div className="row">
                  <div className="col-md-12 col-offset-2">
                      <div className="input-group mt-2">
                          <button className="btn btn-borderButton mr-1">25%</button>
                          <button className="btn btn-borderButton mr-1">50%</button>
                          <button className="btn btn-borderButton mr-1">75%</button>
                          <button className="btn btn-borderButton">100%</button>
                          <button className="btn btn-borderButton">150%</button>
                          <button className="btn btn-borderButton">200%</button>
                      </div>
                  </div>
              </div>
               <div className="row">
                  <div class="col-md-12">
                      <div className="btn-group btn-block mt-3">
                          <button className="btn btn-borderButton mr-1" onClick={this.triggerstop.bind(this)}>Last</button>
                          <button className="btn btn-borderButton mr-1" onClick={this.triggerstop.bind(this)}>Index</button>
                          <button className="btn btn-borderButton mr-1" onClick={this.triggerstop.bind(this)}>Mark</button>
                      </div>
                  </div>
              </div>*/}
            </div>


           <div className="form-group">
              <div className="row">
               <div className="col-md-4">
                  <div className="checkbox pt-2"><label><input name="trailcheck" id="trailcheck" onChange={this.onChange} value={this.state.trailcheck} type="checkbox" value="1"/><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText checkConfirmationText">Trailing Stop USD</span></label></div>
                </div>
                <div className="col-md-8">
                  <div className="def-number-input number-input safari_only">
                    <input className="quantity" min="0" readOnly={this.state.trailreadOnly} name="trailingstopdistance" id="trailingstopdistance" onChange={this.onChange} value={this.state.trailingstopdistance} type="number" />
                    <button onClick={this.updownvalue.bind('tri','trailingstopdistance','plus')} className="plus"></button>
                    <button onClick={this.updownvalue.bind('tri','trailingstopdistance','minus')} className="minus"></button>
                  </div>
                  {
                    (this.state.trailingstopdistance!='' && this.state.trailingstopdistance!=0)?
                      <div>If optimal Last Traded Price retraces by {this.state.trailingstopdistance} USD, it will trigger Stop-Loss with Market Order</div>
                      :''
                  }
                </div>
              </div>
            </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary btnDefaultNewBlue" onClick={this.tpClose}>
            Cancel
          </Button>
          <Button variant="primary btnDefaultNew" onClick={this.triggerstoporder}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>


<TradeFooter />
</div>
      </div>
    );
  }
}

Trade.propTypes = {
  changeopenpositions    : PropTypes.func.isRequired,
  triggerstop    : PropTypes.func.isRequired,
  getPricevalue    : PropTypes.func.isRequired,
  logoutUser       : PropTypes.func.isRequired,
  cancelTrade      : PropTypes.func.isRequired,
  getPertual       : PropTypes.func.isRequired,
  getTradeData     : PropTypes.func.isRequired,
  getuserTradeData : PropTypes.func.isRequired,
  orderPlacing     : PropTypes.func.isRequired,
  auth             : PropTypes.object.isRequired,
  errors           : PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(

  mapStateToProps,
  {
    changeopenpositions,
    getPertual,
    cancelTrade,
    orderPlacing,
    getTradeData,
    getuserTradeData,
    logoutUser,
    triggerstop,
    getPricevalue
  }
)(withRouter(Trade));
