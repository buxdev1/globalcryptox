import React, { Component } from "react";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import axios from "axios";
import keys from "../actions/config";

import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2
} from "react-html-parser";

import Footer from "./Footer";
import DeposiImg from "../images/depositScreen.png";
import WithdrawImg from "../images/withdraeScreen.png";

const url = keys.baseUrl;

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articledata: [],
      subcatdata: [],
      subcatname:""
    };
  }
  componentDidMount() {
    this.getData();
  }
  getData() {
    const articleid = this.props.match.params.aid;

    console.log("ARTICICICICC  Id", articleid);

    axios
      .get(url + "api/article/" + articleid)
      .then(result => {
        console.log(
          "article detail in ssas",
          result.data.articledata.subcategoryId
        );
        this.setState({ articledata: result.data.articledata });
        this.setState({ subcatdata: result.data.subcatdata });
        this.setState({subcatname:result.data.articledata.subcategoryId.subcategoryName
        })
        // this.setState({ userselectcategoryid: category.data[0]._id });
      })
      .catch();
  }

  render() {
    const { articledata, subcatdata,subcatname } = this.state;
    return (
      <div>
        <Navbar />
        <section className="innerCMS">
          <div className="container">
            <div className="helpDetailPage">
              <div className="row mt-4">
                <div className="col-md-3 px-md-0">
                  <div className="darkBox contentPage">
                    <div className="tableHead tableHeadBlock">
                      <h3>
                        
                      {subcatname}
                      </h3>
                    </div>
                    <div className="darkBoxSpace helpPage">
                      <div className="wow fadeIn">
                        <ul className="helpList">
                          {subcatdata.map((item, i) => {
                            var subname = item.Articlename;
                            var articleid = item._id;
                            return (
                              <li>
                                <a href={"/helpdetails/" + articleid}>
                                  {" "}
                                  {subname}
                                </a>
                              </li>
                            );
                          })}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-9">
                  <div className="darkBox contentPage">
                    <div className="tableHead tableHeadBlock">
                      <h2>
                        
                        {articledata.Articlename}
                      </h2>
                    </div>
                    <div className="darkBoxSpace helpPage helpDetailContent">
                      <div className="wow fadeIn pt-3">
                        {ReactHtmlParser(articledata.content)}
                        {/* {articledata.content} */}
                        {/* <h3>Making a deposit:</h3>
                    <p>Currently, GlobalCryptoX only accepts the deposits of BTC, ETH, EOS, and XRP, with plans to accept other currencies (e.g. LTC, TRX, etc.) in the near future.</p>
                    <p>To make a deposit, please follow these instructions:</p>
                    <ul>
                      <li>Click on 'Assets' at the top left of the page. In 'My Assets' section, select the Coin to deposit, then click on 'Deposit'.</li>
                      <li>Use the wallet address or scan the QR code to proceed with the transaction.</li>
                      <li><strong>Do not deposit other cryptocurrencies into the address; otherwise, your assets might be lost forever</strong></li>
                    </ul>
                    <h6>Note:</h6>
                     <p><strong>- For ETH deposits, Please be aware that GlobalCryptoX only accepts ETH direct transfers. Please do not use ETH smart contracts.</strong></p>
                     <p><strong>- For EOS deposits, please be sure to write your UID under 'Optional notes' &' Tag'</strong></p>
                     <p><strong>- For XRP deposits, please be sure to write your UID under 'Memo'</strong></p>
                     <p><strong>- In order to obtain testnet coins, please click here.</strong></p>
                     <img src={DeposiImg} className="img-fluid mx-auto" />
                     <h3>Making a Withdrawal:</h3>
                    <p>Currently, GlobalCryptoX only accepts the deposits of BTC, ETH, EOS, and XRP, with plans to accept other currencies (e.g. LTC, TRX, etc.) in the near future.</p>
                    <p>To make a deposit, please follow these instructions:</p>
                    <ul>
                      <li>Click on 'Assets' at the top left of the page. In 'My Assets' section, select the Coin to deposit, then click on 'Deposit'.</li>
                      <li>Use the wallet address or scan the QR code to proceed with the transaction.</li>
                      <li><strong>Do not deposit other cryptocurrencies into the address; otherwise, your assets might be lost forever.</strong></li>
                    </ul>
                    <h6>Note:</h6>
                     <p><strong>- For ETH deposits, Please be aware that GlobalCryptoX only accepts ETH direct transfers. Please do not use ETH smart contracts.</strong></p>
                     <p><strong>- For EOS deposits, please be sure to write your UID under 'Optional notes' &' Tag'</strong></p>
                     <p><strong>- For XRP deposits, please be sure to write your UID under 'Memo'</strong></p>
                     <p><strong>- In order to obtain testnet coins, please click here.</strong></p>
                     <img src={WithdrawImg} className="img-fluid mx-auto" /> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default About;
