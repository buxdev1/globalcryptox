import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import AboutImg from "../images/aboutus.png"
import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;
class About extends Component {

  constructor(props) {
        super(props);
        this.state = {  
            subject:"",
            subject:"",
            content: "",
            errors: {},
            privacy : {}, 
        };

        console.log(this.state,'state');
        }

        componentDidMount() {
            this.getData()  
        };
    createMarkup = () => {
          return { __html: this.state.privacy.data.content };
        }   
    getData() {
           axios
           .get(url+"cryptoapi/privacy")
            .then(res => {   
              this.setState({privacy:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }
   
    
	render() {
		return (<div>
			<Navbar />
			<section className="innerCMS">
			 {this.state.privacy.data?
			  <div className="container">
			    <div className="row">
			      <div className="col-md-10 mx-auto">
			       <img src={keys.baseUrl + 'cms_images/' + this.state.privacy.data.image[0]}  className="img-fluid cmsTopImg mt-1" />
			        <div className="darkBox contentPage">
			          <div className="tableHead tableHeadBlock">
			            <h2>Privacy Policy</h2>
			          </div>
			          <div className="darkBoxSpace">
			            <h4>{this.state.privacy.data.subject}</h4>
			            <p><div dangerouslySetInnerHTML={this.createMarkup()} className='editor'></div> </p>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			   :''}
			</section>
			<Footer />
		</div>
		);
	}
}

export default About