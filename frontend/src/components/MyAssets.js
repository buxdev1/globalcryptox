import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import Select from "react-select";


import DepositeQR from "../images/depositeQR.jpg"
import BtcIcon from "../images/btcIcon.png"
import EthIcon from "../images/ethIcon.png"
import XrpIcon from "../images/xrpIcon.png"
import LtcIcon from "../images/ltcIcon.png"
import BchIcon from "../images/bchIcon.png"
import { Modal, Button } from 'react-bootstrap/';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import classnames from "classnames";
import { store } from 'react-notifications-component';
import axios from "axios";
import keys from "../actions/config";
import doubleArrow from "../images/doubleArrow.png";
const url = keys.baseUrl;
const imageurl = keys.imageUrl;
var QRCode = require('qrcode.react');

const customStyles = {
	option: (provided, state) => ({
		...provided,
		borderBottom: "1px dotted pink",
		color: state.isSelected ? "red" : "blue",
		padding: 20,
		display: state.isDisabled || state.isSelected ? "none" : "block"
	}),
	control: () => ({
		// none of react-select's styles are passed to <Control />
	}),
	singleValue: (provided, state) => {
		const opacity = state.isDisabled ? 0.5 : 1;
		const transition = "opacity 300ms";

		return { ...provided, opacity, transition };
	}
};
const CurrencyTableData_optionsfrom = [{ "label": "Derivatives", "value": "Derivatives" }, { "label": "Spot", "value": "Spot" }]
const CurrencyTableData_optionsto = [{ "label": "Derivatives", "value": "Derivatives" }, { "label": "Spot", "value": "Spot" }]
const CurrencyTableData_from = [{ "label": "TRX", "value": "TRX" }, { "label": "USD", "value": "USD" }]
const CurrencyTableData_to = [{ "label": "TRX", "value": "TRX" }, { "label": "USD", "value": "USD" }]
class MyAssets extends Component {
	constructor() {
		super();
		this.state = {
			exchangeshow: false,
			withdrawshow: false,
			tranfershow: false,
			depositshow: false,
			cryptoAddress: '',
			cryptoSymbol: '',
			cryptoBalance: 0,
			cryptoFee: 0,
			btcbalance: 0,
			usdbalance: 0,
			copied: false,
			copied1: false,
			usddepositshow: false,
			usdwithdrawshow: false,
			getBalanceDetails: [],
			getAddressDetails: [],
			allAddressDetails: [],
			pairdata: [],
			allposition_details: [],
			checkSecurity: false,
			tagid: '',
			receiveraddress: '',
			toaddress: '',
			transferamount: '',
			pmamount:"",
			finalamount: '',
			amount: '',
			twofa: '',
			bonusbalance: 0,
			convertedamount: 0,
			currencyamount: 0,
			btcprice: 0,
			ethprice: 0,
			errors: {},
			xrpdetails: {},
			withdrawshow1: true,
			destinationtag: '',
			fromwallet: { value: 'Derivatives', label: 'Derivatives' },
			towallet: { value: 'Derivatives', label: 'Derivatives' },
			fromcurrency: { value: 'TRX', label: 'TRX' },
			tocurrency: { value: 'TRX', label: 'TRX' },
			CurrencyTableData_optionsfrom: [],
			CurrencyTableData_optionsto: [],
			minbtc: 0,
			minusd: 0
		}
	}
	componentDidMount() {
		this.generateAddress();
		this.generatetokenaddress()
		this.getpositiondetails();
		this.updateBalance();
		// this.updateusdtBalance();
		this.xrpupdateBalance();
		this.getfeedata();
		this.getBalance();
		this.getAddress();
		// this.gettokenupdate();
		axios
			.post(url + "cryptoapi/spotpair-data")
			.then(res => {
				var btcindex = res.data.data.findIndex(x => (x.tiker_root) === 'TRXUSDT');

				if (btcindex != -1) {
					this.setState({ btcprice: res.data.data[btcindex].markprice, pairdata: res.data.data });
				}

			})
			.catch()
	}
	getfeedata() {
		axios.get(url + "cryptoapi/feesettingsdata").then((res) => {
			this.setState({
				minbtc: res.data.minbtc,
				minusd: res.data.minusd
			});
		});
	}

	gettokenupdate() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/tokenupdate/" + userid)
			.then(res => {
			})
			.catch()
	}
	generateAddress() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/addresscreate/" + userid)
			.then(res => {
			});
	}
	generatetokenaddress() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/tokenaddresscreate/" + userid)
			.then(res => {
			});
	}
	getAddress() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/getaddress/" + userid)
			.then(res => {
				if (res.data.data.length > 0) {
					var addressselect = [];
					var getAllAddress = res.data.data;

					for (var i = 0; i < getAllAddress.length; i++) {
						var from_val = {
							value: getAllAddress[i]._id,
							tagid: getAllAddress[i].tagid,
							currency: getAllAddress[i].currency,
							label: getAllAddress[i].address + " - " + getAllAddress[i].name
						};
						addressselect.push(from_val);
					}
					this.setState({ getAddressDetails: addressselect });
					this.setState({ allAddressDetails: addressselect });
				}
			});
	}
	updateBalance() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/updateBalance/" + userid)
			.then(res => {
			});
	}

	// updateusdtBalance() {
	//  let userid = this.props.auth.user.id;
	// axios
	// 		.get(url+"cryptoapi/updateusdtBalance/"+userid)
	// 		.then(res => {
	// 		});
	// }

	xrpupdateBalance() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/xrpupdateBalance/" + userid)
			.then(res => {
			});
	}
	getBalance() {
		let userid = this.props.auth.user.id;
		axios
			.get(url + "cryptoapi/getBalance/" + userid)
			.then(res => {
				var xrpdetails = res.data.xrpdetails;
				console.log(res.data,'res.data')
				var index = res.data.data.findIndex(x => (x.currencySymbol) === "TRX");
				var usdindex = res.data.data.findIndex(x => (x.currencySymbol) === "USD");
				if (usdindex != -1) {
					var perparray = res.data.data;
					var indexarr = [2, 0, 4,1,3];
					var outputarr = indexarr.map(i => perparray[i]);
					this.setState({ getBalanceDetails: res.data.data, bonusbalance: res.data.data[index].tempcurrency, btcbalance: res.data.data[index].spotwallet, xrpdetails: xrpdetails });

				}
				if (usdindex != -1) {
					this.setState({ usdbalance: res.data.data[usdindex].spotwallet })
				}
			});
	}

	getpositiondetails() {
		let userid = this.props.auth.user.id;
		axios
			.post(url + "cryptoapi/allposition_details/", { userId: userid })
			.then(res => {
				this.setState({ allposition_details: res.data.data });
			});
	}
	//  submitWithdraw(){
	// 	setValidateError({});
	// 	  let formData = { ...formValue, ...{ userid: _id }, ...{ from: "pm" }  }
	// 	let otpverify =  postMethod(url+"api/mailotp-generate",formData);
	// 	if(otpverify && otpverify.data && otpverify.data.errors){
	// 	  setValidateError(otpverify.data.errors)
	// 	}else{
	// 	console.log(otpverify,"=================otpverift")
	// 	setotpmail(otpverify.data.otp);
	// 	setshowDetails(true);
	//   }
	//   }
	onChange = e => {
		this.setState({ [e.target.id]: e.target.value });
	};
	onChangeSelect = selectedOption => {
		this.setState({ receiveraddress: selectedOption.value });
	};
	onChangeAmount = (e) => {
		this.setState({ transferamount: e.target.value });
		if (e.target.value >= 0) {
			let finalamountbal = parseFloat(e.target.value) + parseFloat(this.state.cryptoFee)
			this.setState({ finalamount: finalamountbal });
			if (finalamountbal > this.state.cryptoBalance) {
				this.setState({ transferamount: 0 });
				store.addNotification({
					title: "Error!",
					message: "Balance is low",
					type: "danger",
					insert: "top",
					container: "top-right",
					animationIn: ["animated", "fadeIn"],
					animationOut: ["animated", "fadeOut"],
					dismiss: {
						duration: 1500,
						onScreen: true
					}
				});
			}
		} else {
			this.setState({ finalamount: 0 });
			this.setState({ transferamount: 0 });
			store.addNotification({
				title: "Error!",
				message: "Enter the amount greater than 0",
				type: "danger",
				insert: "top",
				container: "top-right",
				animationIn: ["animated", "fadeIn"],
				animationOut: ["animated", "fadeOut"],
				dismiss: {
					duration: 1500,
					onScreen: true
				}
			});
		}
	}
	onChange = e => {
		if (e.target.id == 'currencyamount') {
			if ((this.state.fromcurrency.value == 'TRX' && this.state.tocurrency.value == 'TRX') || (this.state.fromcurrency.value == 'USD' && this.state.tocurrency.value == 'USD')) {
				var convertedamount = 0;
			}
			else if (this.state.fromcurrency.value == 'TRX' && e.target.value != '' && !isNaN(e.target.value)) {
				var convertedamount = parseFloat(e.target.value) * parseFloat(this.state.btcprice);
			}
			else if (e.target.value != '' && !isNaN(e.target.value)) {
				var convertedamount = parseFloat(e.target.value) / parseFloat(this.state.btcprice)
			}
			else {
				var convertedamount = 0;
			}
			this.setState({ [e.target.id]: e.target.value, convertedamount: parseFloat(convertedamount).toFixed(8) });
		}
		else {
			this.setState({ [e.target.id]: e.target.value });
		}
	};

	handleUSDDepositShow = (details) => {



		this.setState({ usddepositshow: true })
		//    if(details.currencySymbol=='USD')
		//    {
		// 	 this.setState({cryptoAddress:this.state.xrpdetails.address})
		// 	 this.setState({destinationtag:details.currencyAddress})
		//    }
		//    else {
		// 	 this.setState({cryptoAddress:details.currencyAddress})
		//    }
		// 		 this.setState({cryptoSymbol:details.currencySymbol})
		// 		 this.setState({cryptoBalance:details.balance})
		// if(details.currencySymbol == "BTC" ||
		// details.currencySymbol == "LTC" ||
		// details.currencySymbol == "BCH" ||details.currencySymbol =="DASH"||details.currencySymbol =="BNB"|| details.currencySymbol =="TRX"||
		// details.currencySymbol =="XMR"){
		//   var doublefee =parseFloat(details.currency.fee)*2
		//   this.setState({cryptoFee:doublefee})
		// }else{
		//  this.setState({cryptoFee:details.currency.fee})

		// }
		//    this.setState({depositshow:true});
	}
	handleUSDWithdrawShow = (details) => {



		this.setState({ usdwithdrawshow: true })
		//    if(details.currencySymbol=='USD')
		//    {
		// 	 this.setState({cryptoAddress:this.state.xrpdetails.address})
		// 	 this.setState({destinationtag:details.currencyAddress})
		//    }
		//    else {
		// 	 this.setState({cryptoAddress:details.currencyAddress})
		//    }
		// 		 this.setState({cryptoSymbol:details.currencySymbol})
		// 		 this.setState({cryptoBalance:details.balance})
		// if(details.currencySymbol == "BTC" ||
		// details.currencySymbol == "LTC" ||
		// details.currencySymbol == "BCH" ||details.currencySymbol =="DASH"||details.currencySymbol =="BNB"|| details.currencySymbol =="TRX"||
		// details.currencySymbol =="XMR"){
		//   var doublefee =parseFloat(details.currency.fee)*2
		//   this.setState({cryptoFee:doublefee})
		// }else{
		//  this.setState({cryptoFee:details.currency.fee})

		// }
		//    this.setState({depositshow:true});
	}

	handleDepositShow = (details) => {
		if (details.currencyAddress == '') {
			alert("Please refresh the page")
			window.location.href = "";
		}
		if (details.currencySymbol == 'XRP') {
			this.setState({ cryptoAddress: this.state.xrpdetails.address })
			this.setState({ destinationtag: details.currencyAddress })
		}
		else {
			this.setState({ cryptoAddress: details.currencyAddress })
		}
		this.setState({ cryptoSymbol: details.currencySymbol })
		this.setState({ cryptoBalance: details.balance })
		// if(details.currencySymbol == "BTC" ||
		// details.currencySymbol == "LTC" ||
		// details.currencySymbol == "BCH" ||details.currencySymbol =="DASH"||details.currencySymbol =="BNB"|| details.currencySymbol =="TRX"||
		// details.currencySymbol =="XMR"){
		//   var doublefee =parseFloat(details.currency.fee)*2
		//   this.setState({cryptoFee:doublefee})
		// }else{
		this.setState({ cryptoFee: details.currency.fee })

		// }
		this.setState({ depositshow: true });
	}

	movetoSettings = () => {
		this.props.history.push("/Settings");
	}
	manageAddress = () => {
		this.props.history.push("/ManageAddress");
	}

	withdrawClick = (e) => {
		this.setState({ withdrawshow1: false });
	}
	backtoWithdraw = (e) => {
		this.setState({ withdrawshow1: true });
	}
	exchangeshow = (e) => {
		this.setState({ exchangeshow: true });
	}

	convertnow = (e) => {
		var index = this.state.getBalanceDetails.findIndex(x => (x.currencySymbol) === 'TRX');
		var balance = 0;
		if (index != -1) {
			var balance = this.state.fromwallet.value == "Derivatives" ? this.state.getBalanceDetails[index].spotwallet : this.state.getBalanceDetails[index].spotwallet;
		}
		if (this.state.fromwallet.value == this.state.towallet.value) {
			store.addNotification({
				title: "Error!",
				message: "From wallet and to wallet should be different",
				type: "danger",
				insert: "top",
				container: "top-right",
				animationIn: ["animated", "fadeIn"],
				animationOut: ["animated", "fadeOut"],
				dismiss: {
					duration: 1500,
					onScreen: true
				}
			});
			return false
		}
		else {
			if (this.state.inputamount == '' || this.state.inputamount < 0 || isNaN(this.state.inputamount)) {
				store.addNotification({
					title: "Error!",
					message: "Enter valid amount to convert",
					type: "danger",
					insert: "top",
					container: "top-right",
					animationIn: ["animated", "fadeIn"],
					animationOut: ["animated", "fadeOut"],
					dismiss: {
						duration: 1500,
						onScreen: true
					}
				});
			}
			else if ((balance < this.state.inputamount)) {
				store.addNotification({
					title: "Error!",
					message: "Insuffient balance in the wallet",
					type: "danger",
					insert: "top",
					container: "top-right",
					animationIn: ["animated", "fadeIn"],
					animationOut: ["animated", "fadeOut"],
					dismiss: {
						duration: 1500,
						onScreen: true
					}
				});
			}
			else {

				const transferData = {
					inputamount: this.state.inputamount,
					fromwallet: this.state.fromwallet,
					towallet: this.state.towallet,
					currency: "TRX",
					userId: this.props.auth.user.id
				};
				axios
					.post(url + "cryptoapi/convertamount", transferData)
					.then(res => {
						if (!res.data.status) {
							store.addNotification({
								title: "Error!",
								message: res.data.message,
								type: "danger",
								insert: "top",
								container: "top-right",
								animationIn: ["animated", "fadeIn"],
								animationOut: ["animated", "fadeOut"],
								dismiss: {
									duration: 1500,
									onScreen: true
								}
							});
						}
						else {
							this.setState({ inputamount: 0, tranfershow: false });
							store.addNotification({
								title: "Wonderful!",
								message: res.data.message,
								type: "success",
								insert: "top",
								container: "top-right",
								animationIn: ["animated", "fadeIn"],
								animationOut: ["animated", "fadeOut"],
								dismiss: {
									duration: 1500,
									onScreen: true
								}
							});
							setTimeout(function () {
								window.location.href = "/MyAssets";
							}, 2000);
						}
					});
			}
		}
	}
	currencyconvertnow = (e) => {
		var index = this.state.getBalanceDetails.findIndex(x => (x.currencySymbol) === this.state.fromcurrency.value);
		var balance = 0;
		if (index != -1) {
			var balance = this.state.fromcurrency.value == "TRX" ? this.state.getBalanceDetails[index].spotwallet : this.state.getBalanceDetails[index].spotwallet;
		}
		if (this.state.fromcurrency.value == this.state.tocurrency.value) {
			store.addNotification({
				title: "Error!",
				message: "From Currency and to Currency should be different",
				type: "danger",
				insert: "top",
				container: "top-right",
				animationIn: ["animated", "fadeIn"],
				animationOut: ["animated", "fadeOut"],
				dismiss: {
					duration: 1500,
					onScreen: true
				}
			});
			return false
		}
		else {
			if (this.state.currencyamount == '' || this.state.currencyamount < 0 || isNaN(this.state.currencyamount)) {
				store.addNotification({
					title: "Error!",
					message: "Enter valid amount to convert",
					type: "danger",
					insert: "top",
					container: "top-right",
					animationIn: ["animated", "fadeIn"],
					animationOut: ["animated", "fadeOut"],
					dismiss: {
						duration: 1500,
						onScreen: true
					}
				});
			}
			else if ((balance < this.state.currencyamount)) {
				store.addNotification({
					title: "Error!",
					message: "Insuffient balance in the wallet",
					type: "danger",
					insert: "top",
					container: "top-right",
					animationIn: ["animated", "fadeIn"],
					animationOut: ["animated", "fadeOut"],
					dismiss: {
						duration: 1500,
						onScreen: true
					}
				});
			}
			else {
				var check = this.state.fromcurrency.value == "TRX" ? this.state.minbtc : this.state.minusd
				if (this.state.currencyamount > check) {

					this.setState({ exchangeshow: false });
					const transferData = {
						currencyamount: this.state.currencyamount,
						fromcurrency: this.state.fromcurrency,
						tocurrency: this.state.tocurrency,
						userId: this.props.auth.user.id
					};
					axios
						.post(url + "cryptoapi/convertcurrency", transferData)
						.then(res => {
							if (!res.data.status) {
								store.addNotification({
									title: "Error!",
									message: res.data.message,
									type: "danger",
									insert: "top",
									container: "top-right",
									animationIn: ["animated", "fadeIn"],
									animationOut: ["animated", "fadeOut"],
									dismiss: {
										duration: 1500,
										onScreen: true
									}
								});
							}
							else {
								this.setState({ inputamount: 0, tranfershow: false });
								store.addNotification({
									title: "Wonderful!",
									message: res.data.message,
									type: "success",
									insert: "top",
									container: "top-right",
									animationIn: ["animated", "fadeIn"],
									animationOut: ["animated", "fadeOut"],
									dismiss: {
										duration: 1500,
										onScreen: true
									}
								});
								setTimeout(function () {
									window.location.href = "/MyAssets";
								}, 2000);
							}
						});
				} else {
					store.addNotification({
						title: "Error!",
						message: "The minimum Conversion amount for " + this.state.fromcurrency.value + " is " + check,
						type: "danger",
						insert: "top",
						container: "top-right",
						animationIn: ["animated", "fadeIn"],
						animationOut: ["animated", "fadeOut"],
						dismiss: {
							duration: 1500,
							onScreen: true
						}
					});
				}
			}

		}
	}

	otpSubmit = (e) => {
		if (this.state.twofa != "") {
			//check 2fa
			console.log(this.state.receiveraddress, 'receiveraddress')
			console.log(this.state.toaddress,'dfdfdghjk59')


			const withdrawData = {
				transferamount: this.state.transferamount,
				finalamount: this.state.finalamount,
				tagid: this.state.tagid,
				requestType: 'Withdraw',
				cryptoType: this.state.cryptoSymbol,
				twofa: this.state.twofa,
				id: this.props.auth.user.id
			};


if(this.state.cryptoSymbol == 'USD'){
				 withdrawData.toaddress = this.state.toaddress;

			}
			else if(this.state.cryptoSymbol != 'USD'){
								withdrawData.receiveraddress =  this.state.receiveraddress;

			}

			// console.log(withdrawData,'withdrawData');
			// return false;
			let userid = this.props.auth.user.id;
			axios
				.post(url + "cryptoapi/check2fa", withdrawData)
				.then(res => {
					if (res.data.status) {
						store.addNotification({
							title: "Wonderful!",
							message: res.data.message,
							type: "success",
							insert: "top",
							container: "top-right",
							animationIn: ["animated", "fadeIn"],
							animationOut: ["animated", "fadeOut"],
							dismiss: {
								duration: 1500,
								onScreen: true
							}
						});
						this.setState({ withdrawshow: false });
						this.setState({ withdrawshow1: true });
						window.location.href = "/MyAssets";
					} else {
						store.addNotification({
							title: "Error!",
							message: res.data.message,
							type: "danger",
							insert: "top",
							container: "top-right",
							animationIn: ["animated", "fadeIn"],
							animationOut: ["animated", "fadeOut"],
							dismiss: {
								duration: 1500,
								onScreen: true
							}
						});
					}
					//this.setState({getBalanceDetails:res.data.data});
				});
		} else {
			store.addNotification({
				title: "Error!",
				message: "Authentication Code is missing",
				type: "danger",
				insert: "top",
				container: "top-right",
				animationIn: ["animated", "fadeIn"],
				animationOut: ["animated", "fadeOut"],
				dismiss: {
					duration: 1500,
					onScreen: true
				}
			});
		}
	}
	withdrawSubmit = (e) => {
		e.preventDefault();
		const withdrawData = {
			toaddress:this.state.toaddress,
			receiveraddress: this.state.receiveraddress,
			tagid: this.state.tagid,
			transferamount: this.state.transferamount,
			finalamount: this.state.finalamount,
			requestType: 'Withdraw',
			requestType: this.state.cryptoSymbol,
			id: this.props.auth.user.id
		};
		axios
			.post(url + "cryptoapi/withdrawRequest", withdrawData)
			.then(res => {
				if (!res.data.status) {
					console.log(res);
					this.setState({ errors: res.data.errors });
				}
			});

	}

	submitPmWithdraw = (e) => {
		e.preventDefault();
		const withdrawData = {
			accountno: this.state.accountno,
			pmamount: this.state.pmamount,
			id: this.props.auth.user.id
		};
		axios
			.post(url + "cryptoapi/withdrawRequest", withdrawData)
			.then(res => {
				if (!res.data.status) {
					console.log(res);
					this.setState({ errors: res.data.errors });
				}
			});
	}

	handleDepositClose = (type) => {

		this.setState({ depositshow: false });
	}

	handleUsdClose = (type) => {
		this.setState({ usddepositshow: false });

	}
	handleUsdwithdrawClose = (type) => {

		this.setState({ usdwithdrawshow: false });
	}
	handletransferClose = (type) => {

		this.setState({ tranfershow: false });
	}
	handleexchangeClose = (type) => {

		this.setState({ exchangeshow: false });
	}
	fromwalletchange = selectedOption => {
		this.setState({ fromwallet: selectedOption })
	}
	towalletchange = selectedOption => {
		this.setState({ towallet: selectedOption })
	}
	fromcurrencychange = selectedOption => {
		if ((selectedOption.value == 'TRX' && this.state.tocurrency.value == 'TRX') || (selectedOption.value == 'USD' && this.state.tocurrency.value == 'USD')) {
			var convertedamount = 0;
		}
		else if (selectedOption.value == 'TRX' && this.state.currencyamount != '' && !isNaN(this.state.currencyamount)) {
			var convertedamount = parseFloat(this.state.currencyamount) * parseFloat(this.state.btcprice);
		}
		else if (this.state.currencyamount != '' && !isNaN(this.state.currencyamount)) {
			var convertedamount = parseFloat(this.state.currencyamount) / parseFloat(this.state.btcprice)
		}
		else {
			var convertedamount = 0;
		}
		this.setState({ fromcurrency: selectedOption, convertedamount: parseFloat(convertedamount).toFixed(8) })
	}
	tocurrencychange = selectedOption => {
		if ((selectedOption.value == 'TRX' && this.state.fromcurrency.value == 'TRX') || (selectedOption.value == 'USD' && this.state.fromcurrency.value == 'USD')) {
			var convertedamount = 0;
		}
		else if (selectedOption.value == 'TRX' && this.state.currencyamount != '' && !isNaN(this.state.currencyamount)) {
			var convertedamount = parseFloat(this.state.currencyamount) / parseFloat(this.state.btcprice)
		}
		else if (this.state.currencyamount != '' && !isNaN(this.state.currencyamount)) {
			var convertedamount = parseFloat(this.state.currencyamount) * parseFloat(this.state.btcprice);
		}
		else {
			var convertedamount = 0;
		}
		this.setState({ tocurrency: selectedOption, convertedamount: parseFloat(convertedamount).toFixed(8) })
	}

	handleWithdrawShow = (details) => {

		if (details.currencySymbol == "BTC" ||
			details.currencySymbol == "LTC" ||
			details.currencySymbol == "BCH" || 
			details.currencySymbol == "DASH" ||
			 details.currencySymbol == "BNB" ||
			  details.currencySymbol == "TRX" ||
			  details.currencySymbol == "USD"||
			details.currencySymbol == "XMR") {
			var doublefee = parseFloat(details.currency.fee) / 2
			var sumfee = parseFloat(doublefee) + parseFloat(details.currency.fee)

			this.setState({ cryptoFee: sumfee })
		} else {
			this.setState({ cryptoFee: details.currency.fee })
		}
		this.setState({ cryptoAddress: details.currencyAddress })
		this.setState({ cryptoSymbol: details.currencySymbol })
		this.setState({ cryptoBalance: details.currencySymbol == 'BTC' ? details.spotwallet : details.spotwallet })
		// this.setState({cryptoFee:details.currency.fee})
		var emptyarr = []
		for (var i = 0; i < this.state.allAddressDetails.length; i++) {
			if (this.state.allAddressDetails[i].currency == details.currencySymbol) {
				emptyarr.push(this.state.allAddressDetails[i]);
			}
			if (i == (this.state.allAddressDetails.length - 1)) {
				this.setState({ getAddressDetails: emptyarr })
			}
		}
		if (typeof details.userId.google != "undefined" && details.userId.google == "Enabled") {
			this.setState({ checkSecurity: true })
		}
		this.setState({ withdrawshow: true });
	}

	handletransferShow = (details) => {
		// this.setState({cryptoAddress:details.currencyAddress})
		// this.setState({cryptoSymbol:details.currencySymbol})
		// this.setState({cryptoBalance:details.balance})
		// this.setState({cryptoFee:details.currency.fee})
		// if(typeof details.userId.google !="undefined" && details.userId.google =="Enabled" ){
		// 	this.setState({checkSecurity:true})
		// }
		this.setState({ tranfershow: true });
	}


	handleWithdrawClose = (type) => {
		this.setState({ withdrawshow: false });
	}

	renderBalanceData() {
		let icon;
		console.log("this.state.getBalanceDetails", this.state.getBalanceDetails);
		return this.state.getBalanceDetails.map((details, index) => {
			var index = this.state.allposition_details.findIndex(x => (x.firstCurrency) === details.currencySymbol);
			var pairindex = this.state.pairdata.findIndex(x => (x.first_currency) === details.currencySymbol);
			var price = 1;
			if (pairindex != -1) {
				price = this.state.pairdata[pairindex].markprice;
			}


			var parposquantity = (index != -1) ? this.state.allposition_details[index].quantity : 0;
			var parposprice = (index != -1) ? this.state.allposition_details[index].price : 0;
			var parposleverage = (index != -1) ? this.state.allposition_details[index].leverage : 0;

			var pos_initial_margin = (parseFloat(parposquantity) / parseFloat(parposprice)) * ((100 / parseFloat(parposleverage)) / 100);
			var profitnloss = [(1 / parseFloat(parposprice)) - (1 / parseFloat(price))] * parseFloat(parposquantity);
			profitnloss = isNaN(profitnloss) ? 0 : profitnloss;
			var unprofitnloss = [(1 / parseFloat(parposprice)) - (1 / parseFloat(price))] * parseFloat(parposquantity);
			unprofitnloss = isNaN(unprofitnloss) ? 0 : unprofitnloss;
			var profitnlossper = (parseFloat(profitnloss) / parseFloat(pos_initial_margin)) * 100;
			var profitnlossusd = (profitnloss * parseFloat(this.state.btcprice));

			pos_initial_margin = isNaN(pos_initial_margin) ? 0 : pos_initial_margin;
			var equitybal = +details.balance + (+pos_initial_margin);


			// console.log(details,'detailsdetails')
			//   icon = <td ><img src={BtcIcon} /> {details.currencySymbol} </td>;
			icon =
				<td>
					{/* <img src={BtcIcon} /> {details.currencySymbol}{" "} */}
					{details.currency.currencyimage != null ? (
						<img style={{ width: '23px' }} src={
							imageurl + "currency/" + details.currency.currencyimage
						} />
					) : (<img src={BtcIcon} />)}
					{details.currencySymbol}
				</td>

			return (
				<tr className="wow flipInX" data-wow-delay=".1s;">
					{icon}
					<td>{parseFloat(equitybal).toFixed(8)} {details.currencySymbol}<small>= {parseFloat(equitybal * price).toFixed(2)} USD</small></td>
					<td>{parseFloat(details.balance + details.spotwallet).toFixed(8)} {details.currencySymbol}<small>= {parseFloat(details.balance * price).toFixed(2)} USD</small></td>
					<td>{parseFloat(details.spotwallet).toFixed(8)} {details.currencySymbol}<small>= {parseFloat(details.balance * price).toFixed(2)} USD</small></td>
					<td>{parseFloat(details.balance).toFixed(8)} {details.currencySymbol}<small>= {parseFloat(details.balance * price).toFixed(2)} USD</small></td>
					<td>{parseFloat(unprofitnloss).toFixed(8)} {details.currencySymbol}<small>=  {parseFloat(unprofitnloss * price).toFixed(2)} USD</small></td>

					<td>{parseFloat(pos_initial_margin).toFixed(8)} {details.currencySymbol}<small>= {parseFloat(pos_initial_margin * price).toFixed(2)} USD</small></td>
					<td>{parseFloat(pos_initial_margin).toFixed(8)} {details.currencySymbol}<small>= {parseFloat(pos_initial_margin * price).toFixed(2)} USD</small></td>
					<td>
						{(details.currencySymbol != 'USD') ? 
						
						<div>
							<div className="assetDownMain"  >
								<div className="linkclass" onClick={() => { this.handleDepositShow(details) }} title="Deposit / Receive">Deposit</div>
							</div>
							<div className="assetUpMain"  >
								<div className="linkclass" onClick={() => { this.handleWithdrawShow(details) }} data-toggle="tooltip" data-placement="bottom" title="Withdraw / Send">Withdraw</div>
							</div>



						</div> 
						
						:
							<div className="assetUpMain"  >
								<div className="linkclass" onClick={() => { this.handleUSDDepositShow(details) }} data-toggle="tooltip" data-placement="bottom" title="Deposit / Recieve">Perfect Money Deposit</div>
								<div className="linkclass" onClick={() => { this.handleWithdrawShow(details) }} data-toggle="tooltip" data-placement="bottom" title="Withdraw / Send"> Withraw</div>

							</div>
						}

					</td>
				</tr>
			)
		})
	}

	render() {
		const { errors } = this.state;

		return (
			<div>


				<Modal show={this.state.usddepositshow} onHide={this.handleUsdClose}
					aria-labelledby="contained-modal-title-vcenter" centered>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.cryptoSymbol} Deposit</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="assetEchangeForm">
							<form action="https://perfectmoney.com/api/step1.asp" method="POST">
								<div className="d-flex">
									{/* <div className="text-center mt-4 mr-4">
                                <img src={require("../assets/images/qr_code.png")} alt="QR Code" className="img-fluid"/>
                              </div> */}
									<div className="w-100">
										<input type="hidden" name="PAYEE_ACCOUNT" value="U25557132" />
										<input type="hidden" name="PAYEE_NAME" value="GlobalCryptox" />
										<input type="hidden" name="V2_HASH" value="" />
										<label class="d-block">Amount (In USD)</label>
										{/* <input type="text" className="dash_inp" name="PAYMENT_AMOUNT" value={amount} onBlur={checkamount} onChange={onchange}/> */}

										<input type="text" className="dash_inp" name="PAYMENT_AMOUNT" id="amount" onChange={this.onChange} value={this.state.amount} />
										<input type="hidden" name="PAYMENT_UNITS" value="USD" />
										<input type="hidden" name="STATUS_URL" value="globalcryptox@gmail.com" />
										<input type="hidden" name="PAYMENT_URL" value="https://api.globalcryptox.com/cryptoapi/pamentsuccess" />
										<input type="hidden" name="PAYMENT_URL_METHOD" value="POST" />
										<input type="hidden" name="NOPAYMENT_URL" value="https://api.globalcryptox.com/cryptoapi/paymentfailed" />


										<input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST" />
										<input type="hidden" name="SUGGESTED_MEMO" value="" />
										<input type="hidden" name="userid" value={this.props.auth.user.id} />
										<input type="hidden" name="BAGGAGE_FIELDS" value="userid" />
										{/* {/ <label class="d-block mt-4">Address</label> /} */}
										{/* <div className="input-group">
                                  <input type="text" className="dash_inp" name="PAYMENT_AMOUNT" value="VGFDTYjushfshfj659879sfs6sfsfsgfygf54" onChange={onchange}/>
                                  <Button className="deposit_copy_btn">Copy</Button>
                                </div> */}
									</div>
								</div>

								<div className="text-center">
									<Button className="banner_btn mt-3" type="submit" name="PAYMENT_METHOD" value="Pay Now!">Deposit</Button>
								</div>
							</form>
						</div>
					</Modal.Body>
					<Modal.Footer>

					</Modal.Footer>
				</Modal>



				<Modal show={this.state.usdwithdrawshow} onHide={this.handleUsdwithdrawClose}
					aria-labelledby="contained-modal-title-vcenter" centered>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.cryptoSymbol} Withdraw</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="assetEchangeForm">
							{/* <form action="https://perfectmoney.com/api/step1.asp" method="POST"> */}
							{/* <div className="d-flex"> */}
							{/* <div className="text-center mt-4 mr-4">
                                <img src={require("../assets/images/qr_code.png")} alt="QR Code" className="img-fluid"/>
                              </div> */}
							{/* <div className="w-100">
										<input type="hidden" name="PAYEE_ACCOUNT" value="U25557132" />
										<input type="hidden" name="PAYEE_NAME" value="GlobalCryptox" />
										<input type="hidden" name="V2_HASH" value="" />
										<label class="d-block">Amount (In USD)</label>
										{/* <input type="text" className="dash_inp" name="PAYMENT_AMOUNT" value={amount} onBlur={checkamount} onChange={onchange}/> */}

							{/* <input type="text" className="dash_inp" name="PAYMENT_AMOUNT" id="amount" onChange={this.onChange} value={this.state.amount} />
										<input type="hidden" name="PAYMENT_UNITS" value="USD" />
										<input type="hidden" name="STATUS_URL" value="globalcryptox@gmail.com" />
										<input type="hidden" name="PAYMENT_URL" value="https://api.globalcryptox.com/cryptoapi/pamentsuccess" />
										<input type="hidden" name="PAYMENT_URL_METHOD" value="POST" />
										<input type="hidden" name="NOPAYMENT_URL" value="https://api.globalcryptox.com/cryptoapi/paymentfailed" />


										<input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST" />
										<input type="hidden" name="SUGGESTED_MEMO" value="" />
										<input type="hidden" name="userid" value={this.props.auth.user.id} />
										<input type="hidden" name="BAGGAGE_FIELDS" value="userid" /> */}
							{/* {/ <label class="d-block mt-4">Address</label> /} */}
							{/* <div className="input-group">
                                  <input type="text" className="dash_inp" name="PAYMENT_AMOUNT" value="VGFDTYjushfshfj659879sfs6sfsfsgfygf54" onChange={onchange}/>
                                  <Button className="deposit_copy_btn">Copy</Button>
                                </div> */}
							{/* </div> */}
							{/* </div> */}

							{/* <div className="text-center">
									<Button className="banner_btn mt-3" type="submit" name="PAYMENT_METHOD" value="Pay Now!">Deposit</Button>
								</div> */}

							{/* </form> */}

							<div>
								<div className="form-group col-md-12">

									<label>Perfect Money Account number</label>
									<input
										type="text"
										className="dash_inp"
										value=""
										name="accountno"
										id="accountno"
										onChange={this.onChange}
										error={errors.accountno}
										className={classnames("form-control", {
											invalid: errors.accountno
										})} />

								</div>
								<div className="form-group col-md-12">
									<label>Amount</label>

									<input
										type="number"
										className="dash_inp"
										value={this.state.pmamount}
										name="pmamount"
										id="pmamount"
										onChange={this.onChange}
										error={errors.pmamount}
										className={classnames("form-control", {
											invalid: errors.pmamount
										})} />

									{/* <label>Fees : {usdfeeuseramount}% <label>Amount</label></label><br/>
                                    <label>you will get : ${usdamountafterwithdraw} <label>Amount</label></label> */}
									{/* {validateError && validateError.pmamount && <p>{validateError.pmamount}</p>} */}
								</div>
								{/*    <div className="form-group col-md-12">
                                    <label>2FA</label>
                                    <input
                                      type="text"
                                      className="dash_inp"
                                      value={formValue.twofactor}
                                      id="twofactor"
                                      name="twofactor"
                                      onChange={pmwithdraw}
                                    />
                                    {validateError && validateError.twofactor && <p>{validateError.twofactor}</p>}
                                  </div> */}

								<div className="text-center">
									<Button
										className="banner_btn"
										type="button"
									//   onClick={}
									>Withdraw</Button>
								</div>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer>

					</Modal.Footer>
				</Modal>




				<Modal show={this.state.depositshow} onHide={this.handleDepositClose} aria-labelledby="contained-modal-title-vcenter" centered>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.cryptoSymbol} Deposit / Receive</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="popUpSpace">
							<div className="depositeQr">
								<QRCode value={this.state.cryptoAddress} />
							</div>
							<div className="walletKey">
								<div className="row">
									<div className="col-md-12">
										<div className="form-group inputWithText">
											<div className="address">{this.state.cryptoAddress}</div>
											<CopyToClipboard text={this.state.cryptoAddress}
												onCopy={() => this.setState({ copied: true })}>
												<button className="input-group-append-icon"><i className="far fa-copy"></i></button>
											</CopyToClipboard>
											{this.state.copied ? <span style={{ color: 'green' }}>Copied.</span> : null}
										</div>
									</div>
									{
										this.state.cryptoSymbol == 'XRP' ?
											<div className="col-md-12">
												<div className="form-group inputWithText">
													<div className="address">{this.state.destinationtag}</div>
													<CopyToClipboard text={this.state.destinationtag}
														onCopy={() => this.setState({ copied1: true })}>
														<button className="input-group-append-icon"><i className="far fa-copy"></i></button>
													</CopyToClipboard>
													{this.state.copied1 ? <span style={{ color: 'green' }}>Copied.</span> : null}
												</div>
											</div> : ''
									}

								</div>
							</div>
							<div className="noteCont">
								<h6>Notes:</h6>
								<p>1. Do not deposit any non- BTC assets to the above address. Otherwise, the assets will lost permanently.</p>
								<p>2. Please provide the correct GlobalCryptoX wallet address to ensure a deposit.</p>
								<p>3. Your deposit will be reflected inside your account after receiving 1 confirmation on the blockchain.</p>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer>
						{/*<Button variant="secondary btnDefaultNewBlue" onClick={this.handleDepositClose}>
						Cancel
					</Button>
					<Button variant="primary btnDefaultNew" >
						Confirm
					</Button>*/}
					</Modal.Footer>
				</Modal>

				<Modal show={this.state.tranfershow} onHide={this.handletransferClose} aria-labelledby="contained-modal-title-vcenter" centered>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.cryptoSymbol} Wallet conversion</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="assetEchangeForm">
							<div className="row">
								<div className="col-sm-5 mb-4 mb-sm-0">
									<div className="form-group">
										<label>From Wallet</label>

										<Select
											width="100%"
											menuColor="red"
											options={CurrencyTableData_optionsfrom}
											value={this.state.fromwallet}
											onChange={this.fromwalletchange}
											styles={customStyles}
										/>
									</div>
								</div>
								<div className="col-sm-2">
									<div className="form-group">
										<label className="d-none d-sm-block invisible">
											Sa
                                  </label>
										<div className="changeSelect">
											<img
												src={doubleArrow}
												className="img-fluid"
											/>
										</div>
									</div>
								</div>
								<div className="col-sm-5 mb-4 mb-sm-0">
									<div className="form-group">
										<label>To Wallet</label>
										<Select
											options={CurrencyTableData_optionsto}
											onChange={this.towalletchange}
											value={this.state.towallet}
											styles={customStyles}
											width="100%"
											menuColor="red"

										/>
									</div>
								</div>
							</div>
							<div className="form-group inputWithText">
								<label>
									Amount{" "}

								</label>
								<input
									name="inputamount"
									className="form-group"
									placeholder=""
									type="text"
									id="inputamount"
									value={this.state.inputamount}
									onChange={this.onChange}
								/>
								<div className="input-group-append-icon">
									<small>
										BTC
                                </small>
								</div>
							</div>

							<div className="form-group clearfix">
								<input
									className="buttonType1 float-left"
									type="button"
									name=""
									value="Convert Now"
									onClick={this.convertnow}
								/>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer>

					</Modal.Footer>
				</Modal>

				<Modal show={this.state.exchangeshow} onHide={this.handleexchangeClose} aria-labelledby="contained-modal-title-vcenter" centered>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.cryptoSymbol} TRX-USD conversion</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="assetEchangeForm">
							<div className="row">
								<div className="col-sm-7 mb-4 mb-sm-0">
									TRX balance : {parseFloat(this.state.btcbalance).toFixed(8)}
								</div>
								<div className="col-sm-5 mb-4 mb-sm-2">
									USD balance : {parseFloat(this.state.usdbalance).toFixed(2)}
								</div>
							</div>
						</div>

						<div className="assetEchangeForm clearfix mt-2">
							<div className="row">
								<div className="col-sm-5 mb-4 mb-sm-0">
									<div className="form-group">
										<label>From Currency</label>

										<Select
											width="100%"
											menuColor="red"
											options={CurrencyTableData_from}
											value={this.state.fromcurrency}
											onChange={this.fromcurrencychange}
											styles={customStyles}
										/>
									</div>
								</div>
								<div className="col-sm-2">
									<div className="form-group">
										<label className="d-none d-sm-block invisible">
											Sa
                                  </label>
										<div className="changeSelect">
											<img
												src={doubleArrow}
												className="img-fluid"
											/>
											<span style={{ "marginLeft": "-20px" }}>{parseFloat(this.state.btcprice).toFixed(8)}</span>

										</div>
									</div>
								</div>
								<div className="col-sm-5 mb-4 mb-sm-0">
									<div className="form-group">
										<label>To Currency</label>
										<Select
											options={CurrencyTableData_to}
											onChange={this.tocurrencychange}
											value={this.state.tocurrency}
											styles={customStyles}
											width="100%"
											menuColor="red"

										/>
									</div>
								</div>
							</div>
							<div className="form-group inputWithText">
								<label>
									Amount{" "}

								</label>
								<input
									name="currencyamount"
									className="form-group"
									placeholder=""
									type="text"
									id="currencyamount"
									value={this.state.currencyamount}
									onChange={this.onChange}
								/>
								<div className="input-group-append-icon">
									<small>
										{this.state.fromcurrency.value}
									</small>
								</div>
							</div>

							<div className="form-group inputWithText">
								<label>
									You will get : {this.state.convertedamount + " " + this.state.tocurrency.value}

								</label>


							</div>

							<div className="form-group clearfix">
								<input
									className="buttonType1 float-left"
									type="button"
									name=""
									value="Convert Now"
									onClick={this.currencyconvertnow}
								/>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer>

					</Modal.Footer>
				</Modal>


				<Modal show={this.state.withdrawshow} onHide={this.handleWithdrawClose} aria-labelledby="contained-modal-title-vcenter" centered>
					<Modal.Header closeButton>
						<Modal.Title>{this.state.cryptoSymbol} Withdraw</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{(() => {
							if (this.state.checkSecurity) {
								return (
									<div className="popUpSpace withdrawPopupForm">
										{
											(this.state.withdrawshow1) ?
												<div>
													<div className="row">
																										{ this.state.cryptoSymbol != "USD" && 
														<div className="col-md-12">
															<div className="form-group">
																<label>Receiver’s Wallet Address <a href="javascript:void(0)" onClick={this.manageAddress} className="float-right">Add Wallet</a></label>
																<Select
																	width="100%"
																	menuColor="red"
																	options={this.state.getAddressDetails}
																	onChange={this.onChangeSelect}
																	styles={customStyles}
																/>
															</div>
														</div>
													}
												{this.state.cryptoSymbol == "USD" && 
															<div className="col-md-12">
															<div className="form-group">
																<label> TO Address </label>
																<input
																	onChange={this.onChange}
																	value={this.state.toaddress}
																	id="toaddress"
																	type="text"
																	error={errors.toaddress}
																	className={classnames("form-control", {
																		invalid: errors.toaddress
																	})} />
															</div>
														</div>
													}
														<div className="col-md-12">
															<div className="form-group">
																<label>{this.state.cryptoSymbol} Amount <span className="float-right">Balance: <small>{this.state.cryptoBalance} {this.state.cryptoSymbol}</small> | <a href="#">Send All</a></span></label>
																<input
																	onChange={this.onChangeAmount}
																	value={this.state.transferamount}
																	id="transferamount"
																	type="text"
																	error={errors.transferamount}
																	className={classnames("form-control", {
																		invalid: errors.transferamount
																	})} />
															</div>
														</div>
														<div className="col-md-12">
															<div className="form-group">
																<label>Final Amount <span className="float-right">+ Fee: {this.state.cryptoFee} {this.state.cryptoSymbol}</span></label>
																<input
																	value={this.state.finalamount}
																	id="finalamount"
																	type="text"
																	readOnly
																	error={errors.finalamount}
																	className={classnames("form-control", {
																		invalid: errors.finalamount
																	})} />
															</div>
														</div>
														<div className="col-md-12">
															<div className="form-group">
																<input type="submit" onClick={this.withdrawClick} name="" value="Withdraw" className="btn buttonType1" />
															</div>
														</div>
													</div>
													<div className="noteCont">
														<h6>Notes:</h6>
														<p>1. Minimum withdrawal:{this.state.cryptoFee} {this.state.cryptoSymbol}.</p>
														<p>2. Your withdraw will be reflected inside your account after receiving 1 confirmation  on the blockchain.</p>
													</div>
												</div>
												:
												<div className="row">
													<div className="col-md-12">
														<div className="form-group">
															<label>Enter 2FA Authentication Code</label>
															<input
																onChange={this.onChange}
																value={this.state.twofa}
																id="twofa"
																type="text"
																className={classnames("form-control")} />
														</div>
													</div>
													<div className="col-md-12">
														<div className="form-group">
															<input type="button" onClick={this.otpSubmit} name="" value="Submit" className="btn buttonType1" />
															<input type="button" onClick={this.backtoWithdraw} name="" value="Back" className="btn" />
														</div>
													</div>
												</div>
										}
									</div>


								)
							} else {
								return (
									<div className="popUpSpace withdrawPopupForm">
										<div className="row">
											<div class="security-setting-content">
												<ul>
													<li><span>Registered email</span> <small class="textVerifyGreen"><i class="far fa-check-circle"></i> Verified</small></li>
													<li><span>Google Authentication</span> <small class="textUnVerifyRed"><i class="far fa-times-circle"></i> Disabled</small></li>
												</ul>
											</div>
										</div>
										<div className="col-md-12">
											<div className="form-group">
												<input type="button" onClick={this.movetoSettings} name="" value="Settings" className="btn buttonType1" />
											</div>
										</div>
									</div>

								)
							}
						})()}
					</Modal.Body>
				</Modal>
				<TradeHeader />
				<div className="container-fluid">
					<section className="tradeMain">
						<div className="row">
							<div className="col-md-12">
								<div className="darkBox tradeLimitMarket assetsTable">
									<nav>
										<div className="nav nav-tabs" id="navLink-tab" role="tablist">
											<a href="/myAssets" className="nav-item nav-link active">My Assets</a>
											<a href="/ManageAddress" className="nav-item nav-link">Manage Address</a>
											{/* <a href="/BonusHistory" className="nav-item nav-link">BTC-USD convert</a> */}
											<Link onClick={this.exchangeshow} to="#" className="nav-item nav-link">TRX-USD convert</Link>


											{/* <a href="/closedPandL" className="nav-item nav-link ">Closed P&L</a> */}
											{/* <a href="/AssetsHistory" className="nav-item nav-link">Deposit History</a>
						<a href="/OrderHistory" className="nav-item nav-link">Order History</a>
						<a href="/TradeHistory" className="nav-item nav-link">Trade History</a>
						<a href="/withdrawalHistory" className="nav-item nav-link">Withdrawal History</a>
						<a href="/BonusHistory" className="nav-item nav-link">Bonus History</a>
						<a href="/Locker" className="nav-item nav-link">Finance</a> */}

										</div>
									</nav>
									<div className="tab-content">
										<div className="assetTableTop">
											<h3 className="assetTitle">Wallet balance</h3>
											{/* <ul className="assetTableRightLink">
			              <li><Link to="/ManageAddress">Manage Address</Link> </li>
			              <li><Link onClick={this.exchangeshow}>BTC-USD convert</Link> </li>
			            </ul> */}
										</div>
										<div className="table-responsive">
											<table id="assetsTable" className="table">
												<thead>
													<tr className="wow flipInX" data-wow-delay=".5s;">
														<th>Wallet</th>
														<th>Equity</th>
														<th>Wallet Balance</th>
														<th>Spot Balance</th>
														<th>Derivatives Balance</th>
														<th>Unrealized P&L</th>
														<th>Position Margin</th>
														<th>Order Margin</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													{this.renderBalanceData()}
													<tr className="wow flipInX" data-wow-delay=".1s;">
														<td>Bonus USD</td>
														<td>0.00000000 USD<small>= 0.00 USD</small></td>
														<td>{parseFloat(this.state.bonusbalance).toFixed(8)} USD</td>
														<td>0.00000000 USD<small>= 0.00 USD</small></td>
														<td>0.00000000 USD<small>= 0.00 USD</small></td>
														<td>0.00000000 USD</td>
														<td>0.00000000 USD</td>
														<td>0.00000000 USD</td>
														<td>

														</td>
													</tr>
												</tbody>
											</table>
										</div>

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<TradeFooter />
			</div>
		);
	}
}

MyAssets.propTypes = {
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
	auth: state.auth,
	errors: state.errors
});
export default connect(
	mapStateToProps,
)(withRouter(MyAssets));
