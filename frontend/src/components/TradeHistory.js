import React, { Component } from 'react'
import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import DepositeQR from "../images/depositeQR.jpg"
import BtcIcon from "../images/btcIcon.png"
import EthIcon from "../images/ethIcon.png"
import XrpIcon from "../images/xrpIcon.png"
import LtcIcon from "../images/ltcIcon.png"
import BchIcon from "../images/bchIcon.png"
import { store } from 'react-notifications-component';
import keys from "../actions/config";
import axios from "axios";
import DatePicker from "react-datepicker";
import {tradehistory,tradeSearchdata} from "../actions/authActions";
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { CSVLink, CSVDownload } from "react-csv";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { Scrollbars } from 'react-custom-scrollbars';
const url = keys.baseUrl;


const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: '1px dotted pink',
    color: state.isSelected ? 'red' : 'blue',
    padding: 20,
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';

    return { ...provided, opacity, transition };
  }
}

const options1 = [
	{ value: 'All', label: 'All' },
  { value: 'Buy', label: 'Buy' },
  { value: 'Sell', label: 'Sell' },
];

const options = [{ value: 'All', label: 'All' },];

class TradeHistory extends Component {
	constructor(props) {
    super(props);
		this.state = {
		tradehistory : [],
		records 	 : [],
		csvData      : [],
		contract     : {value:'All',label:'All'},
		type 		 : {value:'All',label:'All'},
		startDate 	 : '',
		endDate      : '',
		exchangetype : 'Futures',
		};
	}

		componentDidMount() {
		this.getData()
		};

		componentWillReceiveProps(nextProps) {
				if (nextProps.errors) {
						this.setState({
						errors: nextProps.errors
						});
				}

				if (nextProps.auth !== undefined
				&& nextProps.auth.trade !== undefined
				&& nextProps.auth.trade.data !== undefined
				&& nextProps.auth.trade.data.data !== undefined
				&& nextProps.auth.trade.data.type !== undefined) {
						this.setState({tradehistory:nextProps.auth.trade.data.data});
						this.buildarray(nextProps.auth.trade.data.data);
				}
		}

	getData() {
		var input = {"userid":this.props.auth.user.id,"exchangetype":this.state.exchangetype}
		this.pairdatachanges();
		this.props.tradehistory(input);
	}

	 changecontract = contract => {
		 this.setState({ contract });
	 };

	 changetype = type => {
		 this.setState({ type });
	 };
	exchangetype = e => {
		var type     = e.target.innerHTML;
		this.setState({exchangetype:type,records:[]}
		, () => {
		this.getData();
		});
	};

 	 buildarray = (historydata) =>{
 		 var csvdata = [];

 		 var titledata = ["Contracts","Filled Type","Filled Qty","Exec price","Value","Fee Rate","Fees Paid","Order Price","Unfilled Qty","Type","Transaction ID","Order Time"];
 		 csvdata.push(titledata);
 		  if(historydata.length>0)
        {
			 historydata.map((item,i)=>{
				var quantity 		= item.quantity?item.quantity:0;
				var price 			= item.price?item.price:0;
				var orderValue 	= item.orderValue?item.orderValue:0;
				var orderType 	= item.buyorsell?item.buyorsell:0;
				var _id 				= item._id?item._id:0;
				var classs 			= item.buyorsell=='buy'?'greenText':'pinkText';
				var filledAmount = item.filledAmount?item.filledAmount:0;
				var Remaining 	= parseFloat(quantity) - parseFloat(filledAmount);
				var newarr = [];
				if(typeof item.filled!='undefined')
				{
						 item.filled.map((itemdata,i)=>{
							 console.log(itemdata,'itemdatasss');
							 var created_at = itemdata.created_at?itemdata.created_at:'';
							 var Price = itemdata.Price?itemdata.Price:0;
							 var pairname = itemdata.pairname?itemdata.pairname:0;
								var data1 = new Date(created_at);
								let date = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
								newarr.push(pairname);
								newarr.push("Trade");
								newarr.push(filledAmount);
								newarr.push(Price);
								newarr.push(orderValue);
								newarr.push(orderValue);
								newarr.push(orderValue);
								newarr.push(orderValue);
								newarr.push(Remaining);
								newarr.push(orderType);
								newarr.push(_id);
								newarr.push(date);
								csvdata.push(newarr);
					 })
						}
			 })
			}
 			 this.setState({csvData:csvdata})
 	 }

 	 Searchdata = () => {
 		 var postdetails = {
 			 "contract"  : this.state.contract.value,
 			 "type"      : this.state.type.value,
 			 "startDate" : this.state.startDate,
 			 "endDate"   : this.state.endDate,
 			 "userid"    : this.props.auth.user.id,
 			 "exchangetype" : this.state.exchangetype
 		 }
 		 this.props.tradeSearchdata(postdetails);
 	 }

 	 pairdatachanges = data =>{
      axios
      .post(url+"cryptoapi/pair-data",{exchangetype:this.state.exchangetype})
      .then(res => {
          if (res.data.status) {
            options.splice(1);
            // console.log(res.data.data,'ressssss');
            this.setState({records:res.data.data});
            res.data.data.map((item,i)=>{
          if(item.first_currency != '' && item.second_currency != '')
          {
            var one = {"value":item.first_currency+item.second_currency,"label":item.first_currency+item.second_currency}
            options.push(one);
          }
        });
          }
      })
      .catch();

    }

 	 handleChange = date => {
 		 this.setState({
 			 startDate: date
 		 });
 	 };


 	 handleChange1 = date => {
 		 this.setState({
 			 endDate: date
 		 });
 	 };


  render() {
  	 const {tradehistory,contract,type} = this.state
    return (
			<div>
			<div className="modal fade" id="deposite" tabindex="-1" role="dialog" aria-labelledby="deposite" aria-hidden="true">
  <div className="modal-dialog modal-md modal-dialog-centered" role="document">
    <div className="modal-content depositeContent dwCont">
      <div className="modal-body">
        <div className="popHead">
          <h4>BTC Deposit</h4>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="popUpSpace">
          <div className="depositeQr">
            <img src={DepositeQR} className="img-fluid" />
          </div>
          <div className="walletKey">
            <div className="row">
              <div className="col-md-12">
                <div className="form-group inputWithText">
                    <input name="" className="form-control" placeholder="" value="3CMCRgEm8HVz3DrWaCCid3vAANE42jcEv9" type="text" />
                    <button className="input-group-append-icon"><i className="far fa-copy"></i></button>
                </div>
              </div>
            </div>
          </div>
          <div className="noteCont">
            <h6>Notes:</h6>
            <p>1. Do not deposit any non- BTC assets to the above address. Otherwise, the assets will lost permanently.</p>
            <p>2. Please provide the correct GlobalCryptoX wallet address to ensure a deposit.</p>
            <p>3. Your deposit will be reflected inside your account after receiving 1 confirmation on the blockchain.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <div className="modal fade" id="Withdraw" tabindex="-1" role="dialog" aria-labelledby="Withdraw" aria-hidden="true">
  <div className="modal-dialog modal-md modal-dialog-centered" role="document">
    <div className="modal-content WithdrawContent dwCont">
      <div className="modal-body">
        <div className="popHead">
          <h4>BTC Withdraw</h4>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="popUpSpace withdrawPopupForm">
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <label>Receiver’s Wallet Address <a href="#" className="float-right">Add Wallet</a></label>
                <input type="text" name="" className="form-control" value="3CMCRgEm8HVz3DrWaCCid3vAANE42jcEv9" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label>BTC Amount <span className="float-right">Balance: <small>10.23569878 BTC</small> | <a href="#">Send All</a></span></label>
                <input type="text" name="" className="form-control" value="0.00000000" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label>Final Amount <span className="float-right">+ Fee: 0.00005 BTC</span></label>
                <input type="text" name="" className="form-control" value="0.00000000" disabled="" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label>Enter 2FA Authentication Code</label>
                <input type="text" name="" className="form-control" value="123456" />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <input type="submit" name="" value="Withdraw" className="btn buttonType1" />
              </div>
            </div>
          </div>
          <div className="noteCont">
            <h6>Notes:</h6>
            <p>1. Minimum withdrawal:0.002BTC.</p>
            <p>2. Your withdraw will be reflected inside your account after receiving 1 confirmation  on the blockchain.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
				<TradeHeader pairdatachange={this.pairdatachange}/>
				<div className="container-fluid">
    		<section className="tradeMain">
			  <div className="row">
			    <div className="col-md-12">
			      <div className="darkBox tradeLimitMarket assetsTable">
			        <nav>
			          <div className="nav nav-tabs" id="navLink-tab" role="tablist">
			           {/* <a href="/myAssets" className="nav-item nav-link">My Assets</a> */}
						<a href="/closedPandL" className="nav-item nav-link ">Closed P&L</a>
						<a href="/AssetsHistory" className="nav-item nav-link">Deposit History</a>
						<a href="/OrderHistory" className="nav-item nav-link">Order History</a>
						<a href="/TradeHistory" className="nav-item nav-link active">Trade History</a>
						<a href="/withdrawalHistory" className="nav-item nav-link">Withdrawal History</a>
						<a href="/BonusHistory" className="nav-item nav-link">Bonus History</a>
            {/* <a href="/Locker" className="nav-item nav-link">Finance</a> */}

			          </div>
			        </nav>

              <nav>
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                <a className="nav-item nav-link active" id="nav-USD-tab" data-toggle="tab" href="#nav-USD" role="tab" aria-controls="nav-USD" aria-selected="false" onClick={this.exchangetype}>Futures</a>
                <a className="nav-item  nav-link" id="nav-INR-tab" data-toggle="tab" href="#nav-INR" role="tab" aria-controls="nav-INR" aria-selected="false" onClick={this.exchangetype}>Spot</a>
                </div>
              </nav>
              <div className="tab-content" id="nav-tabContent">
              <div className="tab-pane fade show active" id="nav-USD" role="tabpanel" aria-labelledby="nav-USD-tab">
							<div className="tab-content">
								<div className="assetHistoryTableTop clearfix">
									<div className="row">
										<div className="col-lg-10 col-md-9 col-sm-8">
											<div className="row">
												<div className="col-lg-2 col-md-4 col-sm-6">
														<div className="form-group clearfix">
															<label>Contacts</label>

															<Select
																	 styles={customStyles}
																	 width='100%'
																	 menuColor='red'
																	 options={options}
																	 value={contract}
																	 onChange={this.changecontract}
																	 />
														</div>
												</div>
												<div className="col-lg-2 col-md-4 col-sm-6">
														<div className="form-group clearfix">
															 <label>Type</label>
															 <Select
																	 styles={customStyles}
																	 width='100%'
																	 menuColor='red'
																	 options={options1}
																	 value={type}
																	 onChange={this.changetype}
																	 />
														</div>
												</div>
												<div className="col-lg-2 col-md-4 col-sm-6">
														<div className="form-group">
															<label>Start Date</label>
																<DatePicker
																		selected={this.state.startDate}
																		onChange={this.handleChange}
																		showYearDropdown
																		showMonthDropdown
																/>
														</div>
												</div>
												<div className="col-lg-2 col-md-4 col-sm-6">
														<div className="form-group">
															<label>End Date</label>
															<DatePicker
																	selected={this.state.endDate}
																	onChange={this.handleChange1}
																	showYearDropdown
																	showMonthDropdown
															/>
														</div>
												</div>
												<div className="col-lg-2 col-md-4 col-sm-6">
													<div className="form-group">
														<label className="d-none d-md-block invisible">s</label>
														<input type="button" name="" value="Search" onClick={this.Searchdata} className="buttonType1" />
													</div>
												</div>
											</div>
										</div>
										<div className="col-lg-2 col-md-3 col-sm-4">
											 <div className="downLoadBTN float-right">
												 <div className="form-group">
													<label className="d-none d-sm-block invisible">s</label>

													{(this.state.csvData.length>0)?
														<CSVLink data={this.state.csvData} className="btn transYellowButton">Download.csv</CSVLink> : ''
													}
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
			        <div className="tab-content">
			           <div className="table-responsive">
                {this.state.tradehistory?
                	<Scrollbars style={{ width: '100%', height: 400 }} renderTrackVertical={props => <div className="track-vertical"/>}>
			          <table id="assetsTable" className="table table-striped rembordor">
			                                            <thead>
			                                                <tr className="wow flipInX" data-wow-delay=".5s;">
			                                                    <th>Contracts</th>
			                                                    <th> Filled Type</th>
			                                                    <th>Filled Qty</th>
			                                                    <th>Exec price</th>
			                                                    <th>Value</th>
			                                                    <th>Fee Rate</th>
			                                                    <th>Fees Paid</th>
			                                                    <th>Order Price</th>
                                                          <th>Unfilled Qty</th>
                                                          <th>Type</th>
                                                          <th>Transaction ID</th>
                                                          <th>Time</th>
			                                                </tr>
			                                            </thead>

                                                  {
                                                tradehistory.map((item,i)=>{
                                                  console.log(item,'itemsss');
                                                 var quantity = item.quantity?item.quantity:0;
                                                 var price = item.price?item.price:0;
                                                 var orderValue = item.orderValue?item.orderValue:0;
                                                 var orderType = item.buyorsell?item.buyorsell:0;
                                                 var taker_fees = item.taker_fees?item.taker_fees:0;

                                                 var _id = item._id?item._id:0;
                                                 var classs = item.buyorsell=='buy'?'greenText':'pinkText';
                                                 var filledAmount = item.filledAmount?item.filledAmount:0;
                                                 var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                                     return ( <tbody>
                                                      { (typeof item.filled != 'undefined')?item.filled.map((itemdata,i)=>{
                                                        console.log(itemdata,'itemdatasss');
                                                        var created_at = itemdata.created_at?itemdata.created_at:'';
                                                        var Fees = itemdata.Fees?itemdata.Fees:0;
                                                        var Price = itemdata.Price?itemdata.Price:0;
                                                        var pairname = itemdata.pairname?itemdata.pairname:0;
                                                         var data1 = new Date(created_at);
                                                         let date = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
                                                      return (<tr>
                                                        <td  className="text-center">{pairname}</td>
                                                        <td className="text-center">Trade</td>
                                                        <td className="text-center">{parseFloat(filledAmount).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(Price).toFixed(2)}</td>
                                                        <td className="text-center">{parseFloat(orderValue).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(taker_fees).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(Fees).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(Price).toFixed(2)}</td>
                                                        <td className="text-center">{parseFloat(Remaining).toFixed(8)}</td>
                                                        <td className="text-center">{orderType}</td>
                                                        <td className="text-center">{_id}</td>
                                                        <td className="text-center">{date}</td>
                                                        </tr>
                                                      );
                                                    }):''
                                                    }
                                                     </tbody>);

                                                })
                                                }

			                                        </table></Scrollbars>:''}
			          </div>

			        </div>
              </div>
              <div className="tab-pane fade" id="nav-INR" role="tabpanel" aria-labelledby="nav-INR-tab">
              <div className="tab-content">
                <div className="assetHistoryTableTop clearfix">
                  <div className="row">
                    <div className="col-lg-10 col-md-9 col-sm-8">
                      <div className="row">
                        <div className="col-lg-2 col-md-4 col-sm-6">
                            <div className="form-group clearfix">
                              <label>Contacts</label>

                              <Select
                                   styles={customStyles}
                                   width='100%'
                                   menuColor='red'
                                   options={options}
                                   value={contract}
                                   onChange={this.changecontract}
                                   />
                            </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-6">
                            <div className="form-group clearfix">
                               <label>Type</label>
                               <Select
                                   styles={customStyles}
                                   width='100%'
                                   menuColor='red'
                                   options={options1}
                                   value={type}
                                   onChange={this.changetype}
                                   />
                            </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-6">
                            <div className="form-group">
                              <label>Start Date</label>
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    showYearDropdown
                                    showMonthDropdown
                                />
                            </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-6">
                            <div className="form-group">
                              <label>End Date</label>
                              <DatePicker
                                  selected={this.state.endDate}
                                  onChange={this.handleChange1}
                                  showYearDropdown
                                  showMonthDropdown
                              />
                            </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-6">
                          <div className="form-group">
                            <label className="d-none d-md-block invisible">s</label>
                            <input type="button" name="" value="Search" onClick={this.Searchdata} className="buttonType1" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-md-3 col-sm-4">
                       <div className="downLoadBTN float-right">
                         <div className="form-group">
                          <label className="d-none d-sm-block invisible">s</label>

                          {(this.state.csvData.length>0)?
                            <CSVLink data={this.state.csvData} className="btn transYellowButton">Download.csv</CSVLink> : ''
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div className="tab-content">
                 <div className="table-responsive">
                {this.state.tradehistory?
                  <Scrollbars style={{ width: '100%', height: 400 }} renderTrackVertical={props => <div className="track-vertical"/>}>
                <table id="assetsTable" className="table table-striped rembordor">
                                                  <thead>
                                                      <tr className="wow flipInX" data-wow-delay=".5s;">
                                                          <th>Contracts</th>
                                                          <th> Filled Type</th>
                                                          <th>Filled Qty</th>
                                                          <th>Exec price</th>
                                                          <th>Value</th>
                                                          <th>Fee Rate</th>
                                                          <th>Fees Paid</th>
                                                          <th>Order Price</th>
                                                          <th>Unfilled Qty</th>
                                                          <th>Type</th>
                                                          <th>Transaction ID</th>
                                                          <th>Time</th>
                                                      </tr>
                                                  </thead>

                                                  {
                                                tradehistory.map((item,i)=>{
                                                  console.log(item,'itemsss');
                                                 var quantity = item.quantity?item.quantity:0;
                                                 var price = item.price?item.price:0;
                                                 var orderValue = item.orderValue?item.orderValue:0;
                                                 var orderType = item.buyorsell?item.buyorsell:0;
                                                 var taker_fees = item.taker_fees?item.taker_fees:0;

                                                 var _id = item._id?item._id:0;
                                                 var classs = item.buyorsell=='buy'?'greenText':'pinkText';
                                                 var filledAmount = item.filledAmount?item.filledAmount:0;
                                                 var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                                     return ( <tbody>
                                                      { (typeof item.filled != 'undefined')?item.filled.map((itemdata,i)=>{
                                                        console.log(itemdata,'itemdatasss');
                                                        var created_at = itemdata.created_at?itemdata.created_at:'';
                                                        var Fees = itemdata.Fees?itemdata.Fees:0;
                                                        var Price = itemdata.Price?itemdata.Price:0;
                                                        var pairname = itemdata.pairname?itemdata.pairname:0;
                                                         var data1 = new Date(created_at);
                                                         let date = data1.getFullYear() + '-' + (data1.getMonth() +1) + '-' + data1.getDate() + ' ' + data1.getHours() +':'+ data1.getMinutes() + ':'+data1.getSeconds();
                                                      return (<tr>
                                                        <td  className="text-center">{pairname}</td>
                                                        <td className="text-center">Trade</td>
                                                        <td className="text-center">{parseFloat(filledAmount).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(Price).toFixed(2)}</td>
                                                        <td className="text-center">{parseFloat(orderValue).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(taker_fees).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(Fees).toFixed(8)}</td>
                                                        <td className="text-center">{parseFloat(Price).toFixed(2)}</td>
                                                        <td className="text-center">{parseFloat(Remaining).toFixed(8)}</td>
                                                        <td className="text-center">{orderType}</td>
                                                        <td className="text-center">{_id}</td>
                                                        <td className="text-center">{date}</td>
                                                        </tr>
                                                      );
                                                    }):''
                                                    }
                                                     </tbody>);

                                                })
                                                }

                                              </table></Scrollbars>:''}
                </div>

              </div>
              </div>
              </div>

			      </div>
			    </div>
			  </div>
			</section>
			</div>
    	<TradeFooter />
			</div>
			);
    }
}

TradeHistory.propTypes = {
  tradeSearchdata    : PropTypes.func.isRequired,
  tradehistory    : PropTypes.func.isRequired,
  auth             : PropTypes.object.isRequired,
  errors           : PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(

  mapStateToProps,
  {
    tradehistory,
    tradeSearchdata,
  }
)(withRouter(TradeHistory));
