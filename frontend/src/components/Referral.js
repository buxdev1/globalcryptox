import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import TradeHeader from './TradeHeader'
import PropTypes from "prop-types";
import {CopyToClipboard} from 'react-copy-to-clipboard';


import { connect } from "react-redux";
import {withRouter} from 'react-router-dom';
import { getTableDataDynamic } from "../actions/authActions";
import keys from "../actions/config";



import TradeFooter from './TradeFooter'
import ReferralImg from "../images/referral.png"
import ReferIcon1 from "../images/referIcon1.png"
import ReferIcon2 from "../images/referIcon2.png"
import ReferIcon3 from "../images/socialIcon1.jpg"
import ReferIcon4 from "../images/referIcon4.png"
import ReferIcon5 from "../images/referIcon5.png"
import ReferIcon6 from "../images/referIcon6.png"
import ReferIcon7 from "../images/referIcon7.png"

import axios from "axios";

const url = keys.baseUrl;
const siteUrl = keys.siteUrl;




class Referral extends Component {

	constructor(props) {
		super(props);

		this.state = {
			user:{},
			referraldata:[],
			referencecode_link:'',
			value: '',
			settings: '',
			social_link1: '',
			social_link3: '',
			social_link4: '',
			referralcontent: '',
			bonus: 0,
			bonus_balance: 0,
			copied: false,
		}
	}



	componentWillReceiveProps(nextProps) {
    
		if (nextProps.errors) {
		  this.setState({
			errors: nextProps.errors
		  });
		}
		else
		{
		  this.setState({
			errors: {}
		  });
		}
		console.log(nextProps,'nextProps');
		if(typeof nextProps.auth!='undefined'){
			if(typeof nextProps.auth.trade!='undefined'){
				if(typeof nextProps.auth.trade.data!='undefined'){

					if(typeof nextProps.auth.trade.data.loginuserdata!='undefined'){
						if(typeof nextProps.auth.trade.data.loginuserdata.referencecode!='undefined'){
							var loginuserdata = nextProps.auth.trade.data.loginuserdata;
							console.log('loginuserdata----------');
							console.log(loginuserdata);
							var link =siteUrl+"register?ref="+loginuserdata.referencecode;
							this.setState({
								user: loginuserdata,
								referencecode_link:link
							});
						}
					}

					if(typeof nextProps.auth.trade.data.referraldata!='undefined'){
						var referraldata = nextProps.auth.trade.data.referraldata;
						console.log('referraldata----------');
						console.log(referraldata);
						this.setState({
							referraldata: referraldata
						});
					}
				}
			}
		}
	}

	componentDidMount() {
		this.getUser();
		this.getsettingData();
		this.getbonusData();
		this.getbalanceData();
		this.getcmsData();
	};	


	
	getUser() {
		this.setState({user: this.props.auth.user});

		let userid = this.props.auth.user.id;
		var dynobj = {}
		dynobj.find = {};
		dynobj.find._id = userid;
		dynobj.table = {};
		dynobj.table.name = 'users';
		dynobj.return = {};
		dynobj.return.name = 'loginuserdata';
		this.props.getTableDataDynamic(dynobj);


		var dynobj = {}
		dynobj.find = {};
		dynobj.find.referaluserid= userid;
		dynobj.table = {};
		dynobj.table.name = 'users';
		dynobj.return = {};
		dynobj.return.name = 'referraldata';
		this.props.getTableDataDynamic(dynobj);
	}
	 getcmsData() {
          axios
          .get(url+"cryptoapi/getcms/referral")
           .then(res => {
            
             this.setState({referralcontent:res.data.content});
            })
           .catch()
           console.log(this.setState,'this.setState');
   }
	getsettingData() {
					axios
					.get(url+"cryptoapi/contact")
					 .then(res => {
						 this.setState({settings:res,social_link1:res.data.social_link1,social_link4:res.data.social_link4,social_link3:res.data.social_link3});
						})
					 .catch()
	 }
	 getbonusData() {
          axios
          .get(url+"cryptoapi/getbonusdetails")
           .then(res => {
            if(res.data.status)
            {
             this.setState({bonus:res.data.data.firstlevel});
            }
            })
           .catch()
   }
   getbalanceData() {
   	let userid = this.props.auth.user.id;
          axios
          .get(url+"cryptoapi/getbonusbalance/"+userid)
           .then(res => {
            if(res.data.status)
            {
             this.setState({bonus_balance:res.data.data.tempcurrency});
            }
            })
           .catch()
   }
    createMarkup = () => {
  return { __html: this.state.referralcontent };
  }



	renderTableData() {
		return this.state.referraldata.map((details, index) => {
		   const {  email ,date} = details //destructuring
		   return (
			  <tr>
				 <td>{email}</td>
				 <td>{date}</td>
			  </tr>
		   )
		})
	 }

  render() {
	const { referraldata } = this.state

    return (
			<div>
				<TradeHeader />
				<div className="container-fluid">
				<section className="tradeMain">
				  <div className="row">
				    <div className="col-xl-8 col-lg-10 col-md-12 mx-auto">
				      <img src={ReferralImg} className="img-fluid cmsTopImg mbMinus" />
				      <div className="darkBox supportTicket">
				        <div className="tableHead tableHeadBlock">
				          <h2>Referral  Program</h2>
				        </div>
				        <div className="darkBoxSpace">
				          <form name="supportTicket" method="">
				            <div className="row">
				              <div className="col-md-4 col-sm-4">
				                <div className="form-group">
				                  <label>Referral Code</label>
				                  <input name="" className="form-control text-center noBorder" placeholder="" value={this.state.user.referencecode} type="text" id="myWalletAddress" />
				                </div>
				              </div>
				              <div className="col-md-8 col-sm-8">
				                <div className="form-group input-group referralLinkBox">
				                    <label>Referral Link</label>
				                    <input name="" className="form-control" placeholder="" value={this.state.referencecode_link} type="text" />
				                    <div className="input-group-append">
				                        {/* <input className="btn buttonType1 px-4" type="button" value="Copy Link" /> */}
										<CopyToClipboard text={this.state.referencecode_link}
          onCopy={() => this.setState({copied: true})}>
          <button type="button" className="btn buttonType1 px-4">Copy Link</button>
        </CopyToClipboard>
				                    </div>
				                </div>
				              </div>
				              <div className="col-md-12">
				                <div className="referralSocial">
				                  <h6>Refer from our social media links</h6>
				                  <ul>
				                    <li><a href={this.state.social_link4}><img src={ReferIcon1} /></a></li>
				                    <li><a href={this.state.social_link1}><img src={ReferIcon2} /></a></li>
				                    <li><a href={this.state.social_link3}><img src={ReferIcon3} /></a></li>
				                    
				                  </ul>
				                </div>
				              </div>
				              <div className="col-md-4 col-sm-4">
				                <div className="form-group">
				                  <label>Registered Referrals</label>
				                  <input type="text" name="" className="form-control" value={referraldata.length} />
				                </div>
				              </div>
				              
				              <div className="col-md-4 col-sm-4">
				                <div className="form-group inputWithText">
				                      <label>Balance</label>
				                      <input name="" className="form-control" placeholder="" value={parseFloat(this.state.bonus_balance).toFixed(8)} type="text" />
				                      
				                  </div>
				              </div>
				            <div className="col-md-12">
				                <div className="form-group">
				                 
				                </div>
				              </div>
				            </div>
				          </form>
				          <div className="tableHead">
				            <h3 className="ml-0">Referral  History</h3>
				          </div>
				          <div className="table-responsive">
				            <table id="assetsTable" className="table">
				              <thead>
				                  <tr className="wow flipInX" data-wow-delay=".5s;">
				                      <th>Mobile/Email</th>
				                      <th>Registration Time</th>
				                     
				                  </tr>
				              </thead>
				             <tbody>
							{this.renderTableData()}
									  

				               {/* <tr><td colspan="4" height="150" className="text-center">- You have no created  payment codes -</td></tr> */}
				             </tbody>
				          </table>
				          </div>
				          <div className="noteText">
				            <h3 className="ml-0">Referral Program</h3>
				            <div dangerouslySetInnerHTML={this.createMarkup()} className='editor'></div>
				          </div>
				        </div>
				      </div>
				    </div>
				  </div>
				</section>
				</div>
				<TradeFooter />
			</div>
			);
    }
}

Referral.propTypes = {
	getTableDataDynamic: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
	mapStateToProps,
	{
		getTableDataDynamic
	}
)(withRouter(Referral));
