import React, { Component } from "react";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import Footer from "./Footer";
import AboutImg from "../images/aboutus.png";
import keys from "../actions/config";
import axios from "axios";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

const url = keys.baseUrl;

class Fee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      records: [],
      fbcontent: "",
      currencydetails: [],
      subcategory1: [],
      articledata: [],
      spotdata: [],
    };
  }
  componentDidMount() {
    this.getData();
  }

  getData() {
    axios
      .post(url + "cryptoapi/perpetual-data")
      .then((category) => {
        var perparray = category.data.data;
        var indexarr = [0, 1, 2, 3, 4];
        var outputarr = indexarr.map((i) => perparray[i]);
        // console.log("records", category);

        this.setState({ records: outputarr });
      })
      .catch();

    axios
      .post(url + "cryptoapi/spot-data")
      .then((category) => {
        this.setState({ spotdata: category.data.data });
        // console.log("spot",this.state.spotdata)
      })
      .catch();

    axios
      .get(url + "cryptoapi/currencydetails")
      .then((currencydetails) => {
        if (currencydetails.data.status) {
          var perparray = currencydetails.data.data;
          // var indexarr = [2, 0, 4, 1, 3];
          // var outputarr = indexarr.map((i) => perparray[i]);
          this.setState({ currencydetails: perparray });
        }
      })
      .catch();

    axios
      .get(url + "cryptoapi/getcms/feepage")
      .then((res) => {
        this.setState({ fbcontent: res.data.content });
      })
      .catch();
    console.log(this.setState, "this.setState");
  }
  categoryselect = (event) => {
    var userselectcategoryid1 = event.currentTarget.dataset.item;
    console.log("userselectcategoryid", userselectcategoryid1);
    this.setState({ userselectcategoryid: userselectcategoryid1 });
  };
  createMarkup = () => {
    return { __html: this.state.fbcontent };
  };

  render() {
    const {
      category,
      articledata,
      subcategory,
      userselectcategoryid,
    } = this.state;
    return (
      <div>
        <Navbar />
        <section className="innerCMS">
          <div className="container">
            <div className="row">
              <div className="col-md-10 mx-auto">
                <img src={AboutImg} className="img-fluid cmsTopImg mt-1" />
                <div className="darkBox contentPage">
                  <div className="tableHead tableHeadBlock">
                    <h2>Fees</h2>
                  </div>
                  <div className="darkBoxSpace">
                  <h2>Spot Fee</h2>

<div className="table-responsive valuesTable">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Pair</th>
                            {/* <th>Leverage</th> */}
                            <th>Maker Fee</th>
                            <th>Taker Fee</th>
                            {/* <th>Daily interest</th> */}
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.spotdata.map((item, i) => {
                            var classs =
                              item.change < 0 ? "redText" : "greenText";
                            var floating = item.first_currency == "XRP" ? 4 : 2;
                            return (
                              <tr>
                                <td>{item.tiker_root}</td>
                                {/* <td>150x</td> */}
                                <td>
                                  {item.maker_rebate ? item.maker_rebate : 0}
                                </td>
                                <td>{item.taker_fees ? item.taker_fees : 0}</td>
                                {/* <td>
                                  {item.dailyinterest ? item.dailyinterest : 0}
                                </td> */}
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>


{/*}

<h2>Derivative Fee</h2>
                    <div className="table-responsive valuesTable">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Pair</th>
                            <th>Leverage</th>
                            <th>Maker Fee</th>
                            <th>Taker Fee</th>
                            <th>Daily interest</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.records.map((item, i) => {
                            var classs =
                              item.change < 0 ? "redText" : "greenText";
                            var floating = item.first_currency == "XRP" ? 4 : 2;
                            return (
                              <tr>
                                <td>{item.tiker_root}</td>
                                <td>150x</td>
                                <td>
                                  {item.maker_rebate ? item.maker_rebate : 0}
                                </td>
                                <td>{item.taker_fees ? item.taker_fees : 0}</td>
                                <td>
                                  0.{item.dailyinterest ? item.dailyinterest : 0}%
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                    <p>
                      <div
                        dangerouslySetInnerHTML={this.createMarkup()}
                        className="editor"
                      ></div>
                    </p>

                    <div className="table-responsive valuesTable">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Coin</th>
                            <th>Minimum deposit</th>
                            <th>Minimum withdrawal</th>
                            <th>Miner Fee</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.currencydetails.map((item, i) => {
                            if (item.currencySymbol != "USD") {
                              return (
                                <tr>
                                  <td>{item.currencySymbol}</td>
                                  <td>No Minimum</td>
                                  <td>{item.minimum ? item.minimum : 0}</td>
                                  <td>{item.fee ? item.fee : 0}</td>
                                </tr>
                              );
                            }
                          })}
                        </tbody>
                      </table>
                    </div>
                  */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Fee;
