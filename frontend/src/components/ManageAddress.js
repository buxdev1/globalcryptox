import React, { Component } from 'react'
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import DepositeQR from "../images/depositeQR.jpg"
import Select from "react-select";
import BtcIcon from "../images/btcIcon.png"
import EthIcon from "../images/ethIcon.png"
import XrpIcon from "../images/xrpIcon.png"
import LtcIcon from "../images/ltcIcon.png"
import BchIcon from "../images/bchIcon.png"
import {Modal,Button} from 'react-bootstrap/';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import classnames from "classnames";
import { store } from 'react-notifications-component';
import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px dotted pink",
    color: state.isSelected ? "red" : "blue",
    padding: 20,
    display: state.isDisabled || state.isSelected ? "none" : "block"
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return { ...provided, opacity, transition };
  }
};

class ManageAddress extends Component {
	constructor() {
			super();
			this.state = {
				addAddress :  false,
				tagshow :  false,
				currencyData :  [],
				getAddressDetails :  [],
				cryptoAddress : '',
				currencyName : '',
				name : '',
				tagid : '',
				errors          : {},
			}
		}
		componentDidMount() {
				this.getAddress();
				this.getCurrencyData();
		}

		getAddress() {
			let userid = this.props.auth.user.id;
				axios
						.get(url+"cryptoapi/getaddress/"+userid)
						.then(res => {
              this.setState({getAddressDetails:res.data.data});
						});
			}
      getCurrencyData() {
        axios
            .get(url+"cryptoapi/getcurrency")
            .then(res => {
              var currencyselect = [];
              var getAllCurrency = res.data.data;
              console.log(getAllCurrency);
              for (var i = 0; i < getAllCurrency.length; i++) {
                // if(getAllCurrency[i].currencySymbol=='BTC' || getAllCurrency[i].currencySymbol=='ETH' || getAllCurrency[i].currencySymbol=='USDT')
                // {
                 var from_val = {
                   value: getAllCurrency[i].currencySymbol,
                   label: getAllCurrency[i].currencyName
                 };
                 currencyselect.push(from_val);

                }
                // }
              this.setState({currencyData:currencyselect});
            });
       }
	    handleDeleteaddress = (details) => {
          var id = details._id;
          var passVal = {id:id}
          axios
              .post(url+"cryptoapi/deleteaddress",passVal)
              .then(res => {
                console.log(res);
                 this.getAddress();
              })

      }

	    addAddressSubmit = () => {
        var message = "";
      if(this.state.cryptoAddress == ""){
        message = "Address Should Not be empty";
      }
      if(this.state.currencyName == ""){
        message = "Currency Name Should Not be empty";
      }
      if(this.state.name == ""){
        message = "Name Should Not be empty";
      }


      if(message != ""){
        store.addNotification({
          title: "Error!",
          message: message,
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
        return false;
      }
      let userid = this.props.auth.user.id;
      var passData = {};
      passData.userId = userid;
      passData.address = this.state.cryptoAddress;
      passData.currency = this.state.currencyName;
      passData.name = this.state.name;
      passData.tagid = this.state.tagid;
      axios
					.post(url+"cryptoapi/addAddress",passData)
					.then(res => {
            if(res.data.status){
              store.addNotification({
                title: "Wonderful!",
                message: res.data.message,
                type: "success",
                insert: "top",
                container: "top-right",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                  duration: 1500,
                  onScreen: true
                }
              });
              this.setState({addAddress:false})
              this.getAddress();
            }
					});
			}
      openaddAddress = () => {
        this.setState({addAddress:true});
      }

      handleCloseAddress = () => {
        this.setState({addAddress:false});
      }

			onChange = e => {
				 	this.setState({ [e.target.id]: e.target.value });
			};
			onChangeSelect = selectedOption => {
          var tagshow = selectedOption.value=='XRP'?true:false;
				 	this.setState({ currencyName: selectedOption.value,tagshow:tagshow });
			};

      renderAddressData() {
         return this.state.getAddressDetails.map((details, index) => {
           return (
               <tr className="wow flipInX" data-wow-delay=".1s;">
                  <td>{details.currency} </td>
                  <td>{details.address}</td>
                  <td>{details.name} </td>
                  <td>{details.tagid} </td>
                  <td>
                    <div className="assetDownMain"  >
                      <div className="assetDown" onClick={()=>{this.handleDeleteaddress(details)}} title="Delete"></div>
                    </div>
                  </td>
              </tr>
             )
            })
      }

  render() {
		const { errors } = this.state;
    return (
			<div>
			<Modal show={this.state.addAddress} onHide={this.handleCloseAddress}  aria-labelledby="contained-modal-title-vcenter" centered>
				<Modal.Header closeButton>
				<Modal.Title>Add Address</Modal.Title>
				</Modal.Header>
				<Modal.Body>
				<div className="popUpSpace">
        <div className="row">
        <div className="col-md-12">
          <div className="form-group">
            <label>Currency</label>
            <Select
              width="100%"
              menuColor="red"
              options={this.state.currencyData}
              onChange={this.onChangeSelect}
              styles={customStyles}
            />
          </div>
        </div>
           <div className="col-md-12">
             <div className="form-group">
               <label>Address</label>
               <input
                  onChange={this.onChange}
                  value={this.state.cryptoAddress}
                  id="cryptoAddress"
                  type="text"
                  className={classnames("form-control")}/>
             </div>
           </div>
           {
             (this.state.tagshow)?
             <div className="col-md-12">
             <div className="form-group">
             <label>Tag (Optional)</label>
             <input
             onChange={this.onChange}
             value={this.state.tagid}
             id="tagid"
             type="text"
             className={classnames("form-control")}/>
             </div>
             </div>: ''
           }
           <div className="col-md-12">
             <div className="form-group">
               <label>Name</label>
               <input
                  onChange={this.onChange}
                  value={this.state.name}
                  id="name"
                  type="text"
                  className={classnames("form-control")}/>
             </div>
           </div>
          </div>
      	</div>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary btnDefaultNewBlue" onClick={this.handleCloseAddress}>
						Cancel
					</Button>
					<Button onClick={this.addAddressSubmit} variant="primary btnDefaultNew" >
						Confirm
					</Button>
				</Modal.Footer>
			</Modal>
				<TradeHeader />
				<div className="container-fluid">
    		<section className="tradeMain">
			  <div className="row">
			    <div className="col-md-12">
			      <div className="darkBox tradeLimitMarket assetsTable">
			        <div className="tab-content">
			          <div className="assetTableTop">
                 <h4>Manage Address</h4>

                </div>
			           <div className="table-responsive">
                 <Button className="float-right" onClick={()=>{this.openaddAddress()}} variant="primary btnDefaultNew" >
                 Add
                 </Button>
               <table id="assetsTable" className="table">
                      <thead>
                          <tr className="wow flipInX" data-wow-delay=".5s;">
                              <th>Coin</th>
                              <th>Wallet Address</th>
                              <th>Name</th>
                              <th>Memo/Tag</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
										   	{this.renderAddressData()}
                      </tbody>
                  </table>
			          </div>

			        </div>
			      </div>
			    </div>
			  </div>
			</section>
			</div>
    	<TradeFooter />
			</div>
			);
    }
}

ManageAddress.propTypes = {
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
)(withRouter(ManageAddress));
