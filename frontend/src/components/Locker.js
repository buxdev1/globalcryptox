import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import TradeHeader from "./TradeHeader";
import TradeFooter from "./TradeFooter";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { store } from "react-notifications-component";
import classnames from "classnames";
import keys from "../actions/config";
import axios from "axios";
import { Button, Modal } from "react-bootstrap";
import * as moment from "moment";
import { Scrollbars } from 'react-custom-scrollbars';
import Select from "react-select";
import doubleArrow from "../images/doubleArrow.png";


const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px dotted pink",
    color: state.isSelected ? "red" : "blue",
    padding: 20,
    display: state.isDisabled || state.isSelected ? "none" : "block"
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return { ...provided, opacity, transition };
  }
};

const url = keys.baseUrl;

const CurrencyTableData_from = [{"label":"TRX","value":"TRX"},{"label":"USD","value":"USD"}]
const CurrencyTableData_to = [{"label":"TRX","value":"TRX"},{"label":"USD","value":"USD"}]
class Locker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      records: [],
      show: false,
      lockerid: "",
      lockername: "",
      lockerduration: "",
      termpercentage: "",
      lockeramount: 0,
      convertedamount: 0,
      history: [],
      investedarray: [],
      getBalanceDetails: [],
      usdbalance: 0,
      bonusbalance: 0,
      mininvestment: 0,
      maxinvestment: 0,
      weekshow: false,
      lockertype:"",
      weekarray:[],
      exchangeshow                  : false,
      btcbalance                    : 0,
      usdbalance                    : 0,
      fromcurrency                  : {value:'TRX',label:'TRX'},
      tocurrency                    : {value:'TRX',label:'TRX'},
      btcprice                      : 0,
      currencyamount                : 0,
      minbtc:0,
      minusd:0

    };
  }

  componentDidMount() {
    this.getinvested();
    this.getLockerData();
    this.gethistory();
    this.getbalance();
    this.getfeedata();
    axios
				.post(url+"cryptoapi/spotpair-data")
				.then(res => {
				var btcindex = res.data.data.findIndex(x => (x.tiker_root) === 'TRXUSDT');

				if(btcindex!=-1)
				{
					this.setState({btcprice:res.data.data[btcindex].markprice,pairdata:res.data.data});
				}

				})
				.catch()
  }
  getfeedata() {
    axios.get(url + "cryptoapi/feesettingsdata").then((res) => {
      this.setState({
        mininvestment: res.data.minfinance,
        maxinvestment: res.data.maxfinance,
        minbtc:res.data.minbtc,
        minusd:res.data.minusd
      });
    });
  }
  getbalance() {
    let userid = this.props.auth.user.id;
    axios.get(url + "cryptoapi/getBalance/" + userid).then((res) => {
      var index = res.data.data.findIndex(x => (x.currencySymbol) === "TRX");
						var usdindex = res.data.data.findIndex(x => (x.currencySymbol) === "USD");
      // this.setState({ getBalanceDetails: res.data.data });
      // for (var i = 0; i < res.data.data.length; i++) {
      //   if (res.data.data[i].currencySymbol == "USD") {
      //     var usdbalance = res.data.data[i].spotwallet;
      //     this.setState({ usdbalance: usdbalance });
      //   }
      //   if (res.data.data[i].currencySymbol == "BTC") {
      //     var bonusbalance = res.data.data[i].tempcurrency;
      //     this.setState({ bonusbalance: bonusbalance });
      //   }
      // }

      if(index!=-1)
						{
							var perparray = res.data.data;
							// var indexarr = [2, 0, 4,1,3];
							// var outputarr = indexarr.map(i => perparray[i]);
							this.setState({getBalanceDetails:res.data.data,bonusbalance:res.data.data[index].tempcurrency,btcbalance:res.data.data[index].spotwallet});

						}
						if(usdindex != -1)
						{
							this.setState({usdbalance:res.data.data[usdindex].spotwallet})
						}
    });
  }
  getinvested() {
    const id = this.props.auth.user.id;
    axios.get(url + "cryptoapi/investedlocker/" + id).then((res) => {
      this.setState({ investedarray: res.data });
    });
  }
  gethistory() {
    const id = this.props.auth.user.id;
    axios.get(url + "cryptoapi/lockerhistory/" + id).then((res) => {
      this.setState({ history: res.data });
    });
  }
  getLockerData() {
    axios
      .post(url + "api/locker-data")
      .then((res) => {
        // console.log("ress records arra",res.data)

        this.setState({ records: res.data });
      })
      .catch();
  }
  onChange = (e) => {
    // this.setState({ [e.target.id]: e.target.value });
    if(e.target.id=='currencyamount')
				{
					if((this.state.fromcurrency.value=='TRX' && this.state.tocurrency.value=='TRX') || (this.state.fromcurrency.value=='USD' && this.state.tocurrency.value=='USD'))
					{
						var convertedamount = 0;
					}
					else if(this.state.fromcurrency.value=='TRX' && e.target.value!='' && !isNaN(e.target.value))
					{
						var convertedamount = parseFloat(e.target.value) * parseFloat(this.state.btcprice);
					}
					else if(e.target.value!='' && !isNaN(e.target.value))
					{
						var convertedamount = parseFloat(e.target.value)/parseFloat(this.state.btcprice)
					}
					else
					{
						var convertedamount = 0;
					}
				 	this.setState({ [e.target.id]: e.target.value,convertedamount:parseFloat(convertedamount).toFixed(8) });
				}
				else
				{
				 	this.setState({ [e.target.id]: e.target.value });
				 }
  };

  handleModal = (e) => {
    this.setState({ show: !this.state.show });
  };
  handleweekclose = (e) =>{
    this.setState({weekshow:false})
  }
  handleweeksshow = (answer) => {
    this.setState({ weekshow: true });
    console.log("asnwer",answer)
    var retamount=answer.returndates[0].returnamount
    var dur=answer.projectduration
    var totalreturn=parseFloat(retamount) *parseFloat(dur)
    this.setState({weekarray:answer.returndates})
    this.setState({totalreturn:totalreturn})
  };
  exchangeshow = (e) => {
    this.setState({exchangeshow : true});
  }


  currencyconvertnow = (e) => {
    var index              = this.state.getBalanceDetails.findIndex(x => (x.currencySymbol) === this.state.fromcurrency.value);
    var balance = 0;
    if(index!=-1)
    {
      var balance = this.state.fromcurrency.value=="BTC"?this.state.getBalanceDetails[index].spotwallet:this.state.getBalanceDetails[index].spotwallet;
    }
    if(this.state.fromcurrency.value == this.state.tocurrency.value)
    {
      store.addNotification({
      title: "Error!",
      message: "From Currency and to Currency should be different",
      type: "danger",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 1500,
        onScreen: true
      }
      });
      return false
    }
    else
    {
      if(this.state.currencyamount== '' || this.state.currencyamount<0 || isNaN(this.state.currencyamount))
      {
         store.addNotification({
               title: "Error!",
               message: "Enter valid amount to convert",
               type: "danger",
               insert: "top",
               container: "top-right",
               animationIn: ["animated", "fadeIn"],
               animationOut: ["animated", "fadeOut"],
               dismiss: {
                 duration: 1500,
                 onScreen: true
               }
             });
      }
      else if((balance<this.state.currencyamount))
      {
        store.addNotification({
               title: "Error!",
               message: "Insuffient balance in the wallet",
               type: "danger",
               insert: "top",
               container: "top-right",
               animationIn: ["animated", "fadeIn"],
               animationOut: ["animated", "fadeOut"],
               dismiss: {
                 duration: 1500,
                 onScreen: true
               }
             });
      }
      else
      {
        var check=this.state.fromcurrency.value=="TRX"?this.state.minbtc:this.state.minusd
        if(this.state.currencyamount>check){
          this.setState({exchangeshow:false});
          const transferData = {
              currencyamount : this.state.currencyamount,
              fromcurrency  : this.state.fromcurrency,
              tocurrency    : this.state.tocurrency,
              userId      : this.props.auth.user.id
          };
          axios
          .post(url+"cryptoapi/convertcurrency",transferData)
          .then(res => {
            if(!res.data.status){
              store.addNotification({
                 title: "Error!",
                 message: res.data.message,
                 type: "danger",
                 insert: "top",
                 container: "top-right",
                 animationIn: ["animated", "fadeIn"],
                 animationOut: ["animated", "fadeOut"],
                 dismiss: {
                   duration: 1500,
                   onScreen: true
                 }
               });
             }
             else
             {
               this.setState({inputamount:0,tranfershow:false});
               this.getbalance();

               store.addNotification({
                 title: "Wonderful!",
                        message: res.data.message,
                        type: "success",
                 insert: "top",
                 container: "top-right",
                 animationIn: ["animated", "fadeIn"],
                 animationOut: ["animated", "fadeOut"],
                 dismiss: {
                   duration: 1500,
                   onScreen: true
                 }
               });
             }
          });
        }else{
          store.addNotification({
            title: "Error!",
            message: "The minimum Conversion amount for "+ this.state.fromcurrency.value+ " is " +check,
            type: "danger",
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 1500,
              onScreen: true
            }
          });
        }

      }
    }
  }

  fromcurrencychange = selectedOption => {
    if((selectedOption.value=='TRX' && this.state.tocurrency.value=='TRX') || (selectedOption.value=='USD' && this.state.tocurrency.value=='USD'))
    {
      var convertedamount = 0;
    }
    else if(selectedOption.value=='TRX' && this.state.currencyamount!='' && !isNaN(this.state.currencyamount))
    {
      var convertedamount = parseFloat(this.state.currencyamount) * parseFloat(this.state.btcprice);
    }
    else if(this.state.currencyamount!='' && !isNaN(this.state.currencyamount))
    {
      var convertedamount = parseFloat(this.state.currencyamount)/parseFloat(this.state.btcprice)
    }
    else
    {
      var convertedamount = 0;
    }
      this.setState({fromcurrency:selectedOption,convertedamount:parseFloat(convertedamount).toFixed(8)})
  }
  tocurrencychange = selectedOption => {
    if((selectedOption.value=='TRX' && this.state.fromcurrency.value=='TRX') || (selectedOption.value=='USD' && this.state.fromcurrency.value=='USD'))
    {
      var convertedamount = 0;
    }
    else if(selectedOption.value=='TRX' && this.state.currencyamount!='' && !isNaN(this.state.currencyamount))
    {
      var convertedamount = parseFloat(this.state.currencyamount)/parseFloat(this.state.btcprice)
    }
    else if(this.state.currencyamount!='' && !isNaN(this.state.currencyamount))
    {
      var convertedamount = parseFloat(this.state.currencyamount) * parseFloat(this.state.btcprice);
    }
    else
    {
      var convertedamount = 0;
    }
      this.setState({tocurrency:selectedOption,convertedamount:parseFloat(convertedamount).toFixed(8)})

  }
  handleexchangeClose = (type) => {

    this.setState({exchangeshow:false});
}


  LockerModal = (item) => {
    this.setState({ show: !this.state.show });
    this.setState({ lockerid: item._id });
    this.setState({ lockername: item.name });
    this.setState({ lockerduration: item.returnterm });
    this.setState({ termpercentage: item.returnpercentagemonth });
    this.setState({lockertype:item.Returntype})
  };
  movetolocker() {
    if (
      typeof this.state.lockeramount != "undefined" &&
      this.state.lockeramount != ""
    ) {
      // if (
      //   this.state.lockeramount >= this.state.mininvestment &&
      //   this.state.lockeramount <= this.state.maxinvestment
      // ) {
        const Data = {
          userid: this.props.auth.user.id,
          lockeramount: this.state.lockeramount,
          lockerid: this.state.lockerid,
          lockername: this.state.lockername,
          lockerduration: this.state.lockerduration,
          termpercentage: this.state.termpercentage,
        };
        axios
          .post(url + "cryptoapi/lockeradd", Data)
          .then((res) => {
            if (res.data.status) {
              store.addNotification({
                title: "Wonderful!",
                message: res.data.message,
                type: "success",
                insert: "top",
                container: "top-right",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                  duration: 1500,
                  onScreen: true,
                },
              });
              this.handleModal();
              this.gethistory();
              this.getinvested();
              this.getLockerData();
              this.getbalance();
              this.setState({ lockeramount: 0 });
            } else {
              store.addNotification({
                title: "Error!",
                message: res.data.message,
                type: "danger",
                insert: "top",
                container: "top-right",
                animationIn: ["animated", "fadeIn"],
                animationOut: ["animated", "fadeOut"],
                dismiss: {
                  duration: 1500,
                  onScreen: true,
                },
              });
            }
          })
          .catch();
      // } else {
      //   store.addNotification({
      //     title: "Warning!",
      //     message:
      //       "Please Invest amount is between" +
      //       this.state.mininvestment +
      //       "$ and " +
      //       this.state.maxinvestment +
      //       "$!!!!",
      //     type: "danger",
      //     insert: "top",
      //     container: "top-right",
      //     animationIn: ["animated", "fadeIn"],
      //     animationOut: ["animated", "fadeOut"],
      //     dismiss: {
      //       duration: 1500,
      //       onScreen: true,
      //     },
      //   });
      // }
    } else {
      store.addNotification({
        title: "Error!",
        message: "Please Enter the Amount",
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1500,
          onScreen: true,
        },
      });
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div>
        <TradeHeader />
        <div className="container-fluid">
          <section className="tradeMain">
            <div className="row">
              <div className="col-md-12">
                <div className="darkBox assetsTable">
                  <nav>
                    <div
                      className="nav nav-tabs"
                      id="navLink-tab"
                      role="tablist"
                    >


                      <Link to="/Locker" className="nav-item nav-link active">
                        Finance
                      </Link>
                      <Link onClick={this.exchangeshow} className="nav-item nav-link">TRX-USD convert</Link>

                    </div>
                  </nav>
                  <div className="tab-content">
                    <div className="lockerContent">
                      <div className="row mx-0">
                        <div className="col-md-12 mt-4">
                          <h3 className="assetTitle mb-3">
                            <span>Finance</span>
                          </h3>

                          <Modal
                            show={this.state.show}
                            onHide={() => {
                              this.handleModal();
                            }}
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                          >
                            <Modal.Header closeButton>
                              <h4>Finance</h4>
                              {/* <h7>USD balance : {this.state.usdbalance}</h7> */}
                            </Modal.Header>
                            <Modal.Body>
                              <div className="row">
                                <div className="col-md-12 mx-auto">

                                  <div className="row">
                                  <div className=" col-md-4">
                                      <div className="form-group">
                                        <label>Bonus balance</label>
                                        {this.state.bonusbalance}{" "}$
                                      </div>
                                    </div>

                                    <div className=" col-md-4">
                                      <div className="form-group">
                                        <label>USD balance</label>
                                        {this.state.usdbalance.toFixed(2)} {" "}$
                                      </div>
                                    </div>

                                    <div className=" col-md-4">
                                      <div className="form-group">
                                        <label>Total balance</label>
                                        {(this.state.bonusbalance + this.state.usdbalance).toFixed(2)} {" "}$
                                      </div>
                                    </div>

                                  </div>
                                  <div className="buyAmt">


                                    <div className="input-group form-group">
                                      {/* <span className="float-right">Balance: <small>{this.state.usdbalance} </small></span> */}
                                      <label>
                                        Amount to Invest
                                        {/* <span className="float-right">
                                          Balance:{" "}
                                          <small>
                                            {this.state.usdbalance} USD{" "}
                                          </small>{" "}
                                        </span> */}
                                      </label>
                                      <input
                                        onChange={this.onChange}
                                        value={this.state.lockeramount}
                                        id="lockeramount"
                                        type="number"
                                        className="form-control"
                                        placeholder="Enter the Locking Amount"
                                      />

                                      <div className="input-group-append">
                                        <span className="input-group-text">
                                          USD
                                        </span>
                                      </div>
                                    </div>

                                    <div className="form-group">
                                      <div className="form-group">
                                        <label>Duration - {this.state.lockertype}</label>
                                        {/* <input
                                          type="text"
                                          className="form-control"
                                          readOnly
                                          value={this.state.lockerduration}
                                        /> */}
                                        {this.state.lockerduration}
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <div className="form-group">
                                        <label>Weekly /{this.state.lockertype}</label>
                                        {/* <input
                                          type="text"
                                          readOnly
                                          className="form-control"
                                          value={this.state.termpercentage}
                                        /> */}
                                        {this.state.termpercentage}{" "} %
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                              <Button
                                variant="primary btnDefaultNew"
                                onClick={() => {
                                  this.handleModal();
                                }}
                              >
                                Cancel
                              </Button>
                              <Button
                                variant="primary btnDefaultNew"
                                onClick={() => {
                                  this.movetolocker();
                                }}
                              >
                                Invest
                              </Button>
                            </Modal.Footer>
                          </Modal>

                          <Modal
                            show={this.state.weekshow}
                            onHide={() => {
                              this.handleweekclose();
                            }}
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                            size="lg"
                          >
                            <Modal.Header closeButton>
                              <h4>Return Data Table</h4>
                            </Modal.Header>
                            <Modal.Body>
                              <div className="row">
                                <div className="col-md-12 mx-auto">
                                <div className="table-responsive">
                                <Scrollbars style={{ width: '100%', height: 400 }} >

                      <table id="assetsTable" className="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Return Amount</th>
                            <th>Status</th>

                          </tr>
                        </thead>

                        <tbody>
                          {this.state.weekarray.map((answer, i) => {

                            return (
                              <tr>
                                <td>Return {" "}{i+1}</td>
                                <td>
                                  {moment(answer.returnweekdate).format(
                                    "DD/MM/YYYY"
                                  )}
                                </td>
                                 <td>{answer.returnamount} $</td>
                                <td>{answer.returnstatus}</td>


                              </tr>
                            );
                          })}

                        </tbody>

                      </table>
                    </Scrollbars>
                    </div>
                                </div>
                              </div>
                            </Modal.Body>
                            <Modal.Footer>
                        <h5> Total Return Amount : {this.state.totalreturn}{""}$</h5>
                              <Button
                                variant="primary btnDefaultNew"
                                onClick={() => {
                                  this.handleweekclose();
                                }}
                              >
                                Close
                              </Button>

                            </Modal.Footer>
                          </Modal>


                          <Modal show={this.state.exchangeshow} onHide={this.handleexchangeClose}  aria-labelledby="contained-modal-title-vcenter" centered>
				<Modal.Header closeButton>
				<Modal.Title>{this.state.cryptoSymbol} TRX-USD conversion</Modal.Title>
				</Modal.Header>
				<Modal.Body>
						<div className="assetEchangeForm">
							<div className="row">
								<div className="col-sm-7 mb-4 mb-sm-0">
								TRX balance : {parseFloat(this.state.btcbalance).toFixed(8)}
								</div>
								<div className="col-sm-5 mb-4 mb-sm-2">
								USD balance : {parseFloat(this.state.usdbalance).toFixed(2)}
								</div>
							</div>
						</div>

                               <div className="assetEchangeForm clearfix mt-2">
                            <div className="row">
                              <div className="col-sm-5 mb-4 mb-sm-0">
                                <div className="form-group">
                                  <label>From Currency</label>

                                  <Select
                                    width="100%"
                                    menuColor="red"
                                    options={CurrencyTableData_from}
                                    value={this.state.fromcurrency}
                                    onChange={this.fromcurrencychange}
                                    styles={customStyles}
                                  />
                                </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="form-group">
                                  <label className="d-none d-sm-block invisible">
                                    Sa
                                  </label>
                                  <div className="changeSelect">
                                    <img
                                      src={doubleArrow}
                                      className="img-fluid"
                                    />
                                    <span style={{"marginLeft": "-20px"}}>{parseFloat(this.state.btcprice).toFixed(8)}</span>
                                    
                                  </div>
                                </div>
                              </div>
                              <div className="col-sm-5 mb-4 mb-sm-0">
                                <div className="form-group">
                                  <label>To Currency</label>
                                  <Select
                                    options={CurrencyTableData_to}
                                    onChange={this.tocurrencychange}
                                    value={this.state.tocurrency}
                                    styles={customStyles}
                                    width="100%"
                                    menuColor="red"

                                  />
                                </div>
                              </div>
                            </div>
                            <div className="form-group inputWithText">
                              <label>
                                Amount{" "}

                              </label>
                              <input
                                name="currencyamount"
                                className="form-group"
                                placeholder=""
                                type="text"
                                id="currencyamount"
                                value={this.state.currencyamount}
                                onChange={this.onChange}
                              />
                              <div className="input-group-append-icon">
                                <small>
                                 {this.state.fromcurrency.value}
                                </small>
                              </div>
                            </div>

                              <div className="form-group inputWithText">
                              <label>
                                You will get : {this.state.convertedamount+" "+this.state.tocurrency.value}

                              </label>


                            </div>

                            <div className="form-group clearfix">
                              <input
                                className="buttonType1 float-left"
                                type="button"
                                name=""
                                value="Convert Now"
                                onClick={this.currencyconvertnow}
                              />
                            </div>
                          </div>
				</Modal.Body>
				<Modal.Footer>

				</Modal.Footer>
			</Modal>

                          <div class="row">
                            {this.state.records.map((item, i) => {
                              // console.log("lockerrecords",item);
                              var hash = item._id;
                              var totalinvest = 0;

                              var index = this.state.investedarray.findIndex(
                                (x) => x.projectId === hash
                              );
                              if (index != -1) {
                                totalinvest = this.state.investedarray[index]
                                  .totalinvest;
                              }
                              var typeofreturn= item.Returntype=="Week"?"Weeks":item.Returntype=="Day"?"Days":""

                              return (
                                <div class="col-md-4">
                                  <div>
                                    <div class="lockerProjectCard">
                                      <h3>{item.name}</h3>
                                      <br />
                                      {/* <h4>Invested- {totalinvest}</h4> */}
                                      <p>
                                        <span>Description</span>
                                        :{" "}{item.description}
                                      </p>
                                      <p>
                                        <span> Duration</span>
                                        :{" "}{item.returnterm}{" "} {typeofreturn}
                                      </p>
                                      <p>
                                        <span>Return  </span>
                                        :{" "}{item.returnpercentagemonth}%  per {item.Returntype}
                                      </p>
                                      <p>
                                        <span>Min Amount  </span>
                                        :{" "}{item.mininvestment} $
                                      </p>
                                      <p>
                                        <span>Max Amount  </span>
                                        :{" "}{item.maxinvestment} $
                                      </p>
                                      <div class="text-center">
                                        <button
                                          class="btn buttonType1 py-2 px-4 font-weight-bold"
                                          onClick={() => {
                                            this.LockerModal(item);
                                          }}
                                        >
                                          Invest Now
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mx-0">
                      <div className="col-md-12">
                        <h3 className="assetTitle mb-3 mt-5">
                          <span>Finance History</span>
                        </h3>
                      </div>
                    </div>
                    <div className="table-responsive">
                      <table id="assetsTable" className="table">
                        <thead>
                          <tr className="wow flipInX" data-wow-delay=".5s;">
                            <th>Date</th>
                            <th>Finance ID</th>
                            <th>Amount</th>
                            <th>% Value</th>
                            <th>Term Duration</th>
                            <th>Return Table</th>
                            <th>Finance End</th>
                            {/* <th>Type</th> */}
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.history.map((answer, i) => {
                            var length = answer.returndates.length;
                            var len = parseFloat(length) - 1;
                            var termend = answer.returndates[len].returnweekdate;
                            // console.log("resslength",length)
                            // console.log("resstermend",termend)


                            return (
                              <tr>
                                <td>
                                  {moment(answer.createddate).format(
                                    "DD/MM/YYYY  h:mm a "
                                  )}
                                </td>
                                <td>{answer.investid}</td>
                                {/* <td>{answer.projectname}</td> */}
                                  <td>{answer.Investamount}{" "}$</td>
                                  <td>{answer.termpercentage}{" "}%</td>
                                <td>{answer.projectduration} {answer.projectId.Returntype}</td>
                                <td>
                                  {/* {answer.noofreturn} */}
                                  <div
                                    className="linkclass"
                                    onClick={() => {
                                      this.handleweeksshow(answer);
                                    }}
                                  >
                                    View  Table
                                  </div>
                                </td>
                                <td>
                                  {" "}
                                  {moment(termend).format("DD/MM/YYYY ")}
                                </td>
                                {/* <td>{answer.type}</td> */}
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <TradeFooter />
      </div>
    );
  }
}

Locker.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, {})(withRouter(Locker));
