import React, { Component } from 'react'
import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;

class TradeFooter extends Component {

   
   constructor(props) {
        super(props);
        this.state = {  
            email:"",
            contact_person:"",
            sitename: "",
            site_description:"",
            favorite_image:"",
            sitelogo: "",
            phone_number:"",
            mobile_number:"",
            address: "",
            tax_amount:"",
            social_link1:"",
            social_link2:"",
            social_link3:"",
            social_link4:"",
            social_link5:"",
            copyright_text: "",
            working_day:"",
            working_hours:"",
            company_info_link:"",
            license_info_link:"",
            errors: {},
            settings : {},
        };

        console.log(this.state,'state');
        }

        componentDidMount() {
            this.getData()  
        };
    
    getData() {
           axios
           .get(url+"cryptoapi/contact")
            .then(res => { 
              this.setState({settings:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }
  

  render() {
    return (
    	<div>
         {this.state.settings.data?
    	<div className="container-fluid">
    	<section className="tradeFooter">
		  <ul className="tradeFooterLinks">
		    <li><a href="/">Home</a></li>
		    <li><a href="/Fee">Fee</a></li>
		    <li><a href="/support">Support Ticket</a></li>
		    <li><a href="/Terms">Terms & Conditions</a></li>
		    <li><a href="/privacy">Privacy Policy</a></li>
		  </ul>
		  <ul className="tradeSocialLinks">
		     <li><a href={this.state.settings.data.social_link1} target="_blank"><i className="fab fa-twitter"></i></a></li>  
		   
		    <li><a href={this.state.settings.data.social_link3} target="_blank"><i className="fab fa-telegram-plane"></i></a></li>
		    <li><a href={this.state.settings.data.social_link4} target="_blank"><i className="fab fa-facebook"></i></a></li>
		    
		  </ul>
		</section>
		</div>
        :''}
    	</div>
    	);
    }
}

export default TradeFooter