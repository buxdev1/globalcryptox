import React, { Component } from 'react'
import footerLogo from "../images/Logo-small.png"
import footerLogo1 from "../images/footerLogo1.jpg"
import footerLogo2 from "../images/footerLogo2.png"
import footerLogo3 from "../images/footerLogo3.png"
import footerLogo4 from "../images/footerLogo4.jpg"
import footerLogo5 from "../images/footerLogo5.png"

import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;
class Footer extends Component{

   constructor(props) {
        super(props);
        this.state = {
            email:"",
            contact_person:"",
            sitename: "",
            site_description:"",
            favorite_image:"",
            sitelogo: "",
            phone_number:"",
            mobile_number:"",
            address: "",
            tax_amount:"",
            social_link1:"",
            social_link2:"",
            social_link3:"",
            social_link4:"",
            social_link5:"",
            copyright_text: "",
            working_day:"",
            working_hours:"",
            subject:"",
            subject:"",
            content: "",
            company_info_link:"",
            license_info_link:"",
            errors: {},
            settings : {},
            licence :{},


        };

        console.log(this.state,'state');
        }

        componentDidMount() {
            this.getData()
            this.getData1()

        };


  createMarkup = () => {
          return { __html: this.state.licence.data.content };
        }
    getData() {
           axios
           .get(url+"cryptoapi/contact")
            .then(res => {
            console.log(res,'res_contactzzzzzzzzzzzzzzzzzzzz');
              this.setState({settings:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }
   getData1() {
           axios
           .get(url+"cryptoapi/licence")
            .then(res => {
            console.log(res,'res_contact');
              this.setState({licence:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }


	render(){
			return <footer className="pageFooter pt-5">

  <div className="container">
    <div className="row">
      <div className="col-md-4 col-sm-6 mb-3 mb-md-0">
    {this.state.settings.data?
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <p><a href="#"><img src={footerLogo} className="img-fluid" /></a></p>
          <p>{this.state.settings.data.sitename}<br />
             Register Code  : {this.state.settings.data.reg_code}<br />
             Address: {this.state.settings.data.address}<br />
             Contact : {this.state.settings.data.phone_number}<br />
             Email : <a href={"mailto:" + this.state.settings.data.email}>{this.state.settings.data.email}</a></p>
             <p>{this.state.settings.data.copyright_text}</p>
        </div>
          :''}
      </div>
      <div className="col-md-5 col-sm-12 mb-4 mb-md-0 pl-xl-0">
        <div className="row mx-0">
          <div className="col-lg-4 col-md-6 col-sm-4 px-0">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6>GlobalCryptoX</h6>
          <ul>
            <li><a href="/About_us">About us</a></li>
            <li><a href="/Contact_us">Contact us</a></li>
            <li><a href="/Fee">Fee</a></li>
            <li><a href="/Faq">FAQ</a></li>
            <li><a href="/apidocs">API</a></li>
            <li><a href="/coininfo">Coin Info</a></li>
          </ul>
        </div>
      </div>
      <div className="col-lg-4 col-md-6 col-sm-4 px-0 mt-4 mt-sm-0">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6>Support</h6>
          <ul>
          <li><a href="/helpcenter">Help Center</a></li>
            <li><a href="/support">Support Ticket</a></li>
            <li><a href="/Terms">Terms and Conditions</a></li>
            <li><a href="/privacy">Privacy Policy</a></li>
          </ul>
        </div>
      </div>
      <div className="col-lg-4 col-md-12 col-sm-4 mt-4 mt-sm-0 mt-md-4 mt-lg-0 px-0">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
        <h6>Recognised by</h6>
          <ul>
            <li><a href="https://coinmarketcap.com/exchanges/globalcryptox/" target="_blank">CoinMarketCap</a></li>
            <li><a href="https://www.coingecko.com/" target="_blank">Coingecko</a></li>
            <li><a href="https://coinpaprika.com/exchanges/globalcryptox/" target="_blank">Coinpaprika</a></li>
            <li><a href="https://coincodex.com/exchange/globalcryptox/" target="_blank">Coincodex</a></li>
            <li><a href="https://www.cryptocompare.com/" target="_blank">Cryptocompare</a></li>
            <li><a href="https://www.livecoinwatch.com/exchange/globalcryptox" target="_blank">Live Coin Watch</a></li>
            <li><a href="https://www.worldcoinindex.com/" target="_blank">Worldcoinindex</a></li>
            <li><a href="https://coinranking.com/exchange/kLOPGoUk_+Globalcryptox" target="_blank">Coinranking</a></li>

          </ul>
        </div>
      </div>
       {this.state.settings.data?
      <div className="col-md-12 px-0 mt-4">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6 className="mb-2">Join our community</h6>
          <ul className="socialIcons">
            <li><a href={this.state.settings.data.social_link1} target="_blank"><i className="fab fa-twitter"></i></a></li>

            <li><a href={this.state.settings.data.social_link3} target="_blank"><i className="fab fa-telegram-plane"></i></a></li>
            <li><a href={this.state.settings.data.social_link4} target="_blank"><i className="fab fa-facebook"></i></a></li>

          </ul>
        </div>
      </div>
      :''}
        </div>
      </div>
      {this.state.licence.data?
      <div className="col-md-3 col-sm-12">
        <div className="footerCont wow fadeInUp" data-wow-delay=".25s">
          <h6>{this.state.licence.data.subject}</h6>
          <p><div dangerouslySetInnerHTML={this.createMarkup()} className='editor'></div></p>
           {this.state.settings.data?
             <div>
          <h6 className="mb-0 footerBottTitle">GlobalCryptoX company information</h6>
          <p><a href={this.state.settings.data.company_info_link} target="_blank">www.ariregister.rik.ee</a></p>
          <h6 className="mb-0 footerBottTitle">Company Crypto exchange and wallet license information</h6>
          <p><a href={this.state.settings.data.license_info_link} target="_blank">www.mtr.mkm.ee</a></p>
          </div>
          :''}
        </div>
      </div>
      :''}
    </div>
  </div>
</footer>
	};
}

export default Footer;
