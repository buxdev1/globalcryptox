import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import AboutImg from "../images/aboutus.png"
import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;
class About extends Component {

  constructor(props) {
        super(props);
        this.state = {  
            identifier:"",
            subject:"",
            content: "",
            errors: {},
            terms : {},
        };

        console.log(this.state,'state');
        }

        componentDidMount() {
            this.getData()   
        };
    createMarkup = () => {
          return { __html: this.state.terms.data.content };
        }    
    getData() {
           axios
           .get(url+"cryptoapi/terms")
            .then(res => {   
              this.setState({terms:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }
   
 
    
	render() {
		return (<div>
			<Navbar />
			<section className="innerCMS">
			 {this.state.terms.data?
			  <div className="container">
			    <div className="row">
			      <div className="col-md-10 mx-auto">
			       <img src={keys.baseUrl + 'cms_images/' + this.state.terms.data.image[0]}  className="img-fluid cmsTopImg mt-1" />
			        <div className="darkBox contentPage">
			          <div className="tableHead tableHeadBlock">
			            <h2>Terms and Conditions</h2>
			          </div>
			          <div className="darkBoxSpace">
			            <h4>{this.state.terms.data.identifier}</h4>
			            <p><div dangerouslySetInnerHTML={this.createMarkup()} className='editor'></div> </p>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			   :''}
			</section>
			<Footer />
		</div>
		);
	}
}

export default About