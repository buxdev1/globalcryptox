import React, { Component } from 'react'
import BannerX from "../images/xicon.png"
import CaptchaImg from "../images/captchaImg.jpg"
import {Link,withRouter} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import PropTypes from "prop-types";
import { connect } from "react-redux";
import queryString from 'query-string'
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import ReCAPTCHA from "react-google-recaptcha";
import { store } from 'react-notifications-component';
import { registerUser,mobregisterUser,otptoUser } from "../actions/authActions";
import classnames from "classnames";
import keys from "../actions/config";
const url = keys.baseUrl;
const Recaptchakey = keys.Recaptchakey;
const recaptchaRef = React.createRef();

const grecaptchaObject = window.grecaptcha
class Register extends Component {
	constructor(props) {
			super(props);
			this.state = {
					email             : "",
					password          : "",
					password2          : "",
					referalcode       : "",
					errors            : {},
					phone             : "",
					accept            : "",
					phone             : "",
					mobilepassword    : "",
					mobilereferalcode : "",
					recaptcha         : "",
					mobrecaptcha      : "",
					mobileaccept      : "",
					otpver            : "",
					readOnly          : false,
					disable           : false,
			};
			console.log("this.props.location.query",this.props)
	}

	componentDidMount() {
		if (this.props.auth.isAuthenticated) {
				this.props.history.push("/settings");
		}

		const values = queryString.parse(this.props.location.search)
		console.log("filter values",values)
		if(typeof values.ref!= "undefined"){
			this.setState({referalcode:values.ref,readOnly:true})
		}
		 
	}

	componentWillReceiveProps(nextProps) {
		console.log(nextProps,'nextprops');
			if (nextProps.errors) {
					this.setState({
							errors: nextProps.errors,
							disable:false,
					});
			}
			if (nextProps.auth.newuser !== undefined
					&& nextProps.auth.newuser.data !== undefined
					&& nextProps.auth.newuser.data.message !== undefined ) {
					store.addNotification({
					  title: "Wonderful!",
					  message: nextProps.auth.newuser.data.message,
					  type: "success",
					  insert: "top",
					  container: "top-right",
					  animationIn: ["animated", "fadeIn"],
					  animationOut: ["animated", "fadeOut"],
					  dismiss: {
					    duration: 5000,
					    onScreen: true
					  }
					});
					this.setState({
							email: "",
							password: "",
							referalcode: "",
							errors: {},
							accept : "",
							disable:false,
							otpver : (this.state.phone != '')?'true':''
					});
					if(nextProps.auth.newuser.data.message=='OTP verification successfully completed, You can login now')
					{
						this.props.history.push("/Login");
						nextProps.auth.newuser = "";
					}
					else
					{
						this.props.history.push("/Login");
						nextProps.auth.newuser = "";
					}
			}

	}

	onChange = e => {
			this.setState({ [e.target.id]: e.target.value });
	};

	captchaChange = e => {
			this.setState({ recaptcha : e });
	};

	captchaChange1 = e => {
			this.setState({ mobrecaptcha : e });
	};

	onChangeCheckbox = e => {
			this.setState({ [e.target.id]: e.target.checked });
	};


	onSubmit = e => {
		this.setState({disable:true})
			e.preventDefault();
			const newUser = {
					email       : this.state.email,
					password    : this.state.password,
					password2    : this.state.password2,
					referalcode : this.state.referalcode,
					recaptcha   : this.state.recaptcha,
					accept      : this.state.accept.toString()
			};
			this.props.registerUser(newUser, this.props.history);
	};

	mobileSubmit = e => {
			e.preventDefault();
			const newUser = {
					phone             : this.state.phone,
					mobilepassword    : this.state.mobilepassword,
					mobilereferalcode : this.state.mobilereferalcode,
					mobrecaptcha      : this.state.mobrecaptcha,
					mobileaccept      : this.state.mobileaccept.toString()
			};
			this.props.mobregisterUser(newUser, this.props.history);
	};

	otpSubmit = e => {
			e.preventDefault();
			const newUser = {
					otpcode: this.state.otpcode,
					phone: this.state.phone,
			};
			this.props.otptoUser(newUser, this.props.history);
	};


	render() {


		const { errors } = this.state;
		return(
		<div>
		<Navbar />
		<header className="loginBanner">
  <div className="container">
    <div className="row">
      <div className="col-md-6">
        <div className="homeBannerText wow fadeInUp" data-wow-delay=".25s">
          <h3 className="mb-4">Exchange Top Cryptos with</h3>
          {/*<h2><span>150</span> <img src={BannerX} /> <small>Leverage</small></h2>*/}
          <ul>
            <li><i className="fas fa-check"></i> Advanced Trade Orders</li>
            {/*<li><i className="fas fa-check"></i> More Leverage</li>*/}
            <li><i className="fas fa-check"></i> Safe And Secured Wallets</li>
            <li><i className="fas fa-check"></i> Technical Expertise</li>
          </ul>
        </div>
      </div>
      <div className="col-md-6">
        <div className="formBox wow flipInY mt-3 mt-md-5 mb-5 mb-md-0" data-wow-delay=".15s;">
         <h2>Register</h2>
        <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
         <div className="tab-pane fade show active" id="nav-emailLogin" role="tabpanel" aria-labelledby="nav-emailLogin-tab">
          <form className="stoForm landingForm" noValidate onSubmit={this.onSubmit} >
            <div className="form-group fadeIn second">
            <label>Email address</label>
						<input
								onChange={this.onChange}
								value={this.state.email}
								error={errors.email}
								id="email"
								type="email"
								className={classnames("form-control", {
										invalid: errors.email
								})}
						/>
						<span className="text-danger">{errors.email}</span>
          </div>
          <div className="form-group fadeIn third">
             <label>Password</label>
						 <input
								 onChange={this.onChange}
								 value={this.state.password}
								 error={errors.password}
								 id="password"
								 type="password"
								 className={classnames("form-control", {
										 invalid: errors.password
								 })}
						 />
						 <span className="text-danger">{errors.password}</span>
          </div>
           <div className="form-group fadeIn third">
             <label>Confirm Password</label>
						 <input
								 onChange={this.onChange}
								 value={this.state.password2}
								 error={errors.password2}
								 id="password2"
								 type="password"
								 className={classnames("form-control", {
										 invalid: errors.password2
								 })}
						 />
						 <span className="text-danger">{errors.password2}</span>
          </div>
          <div className="form-group fadeIn third">
             <label>Referral Code (Optional)</label>
						 <input
 								onChange={this.onChange}
 								value={this.state.referalcode}
 								error={errors.referalcode}
 								id="referalcode"
 								type="text"
 								readOnly={this.state.readOnly}
 								className={classnames("form-control", {
 										invalid: errors.referalcode
 								})}
 						/>
 						<span className="text-danger">{errors.referalcode}</span>
          </div>

          <div className="form-group clearfix fadeIn third">
            <div className="float-left"><div className="checkbox">
            <label>
						<input
						 	 onChange={this.onChangeCheckbox}
							 value={this.state.accept}
							 error={errors.accept}
							 id="accept"
							 type="checkbox"
							 className={classnames("form-control", {
									 invalid: errors.accept
							 })}
					   />
                <span className="cr"><i className="cr-icon fa fa-check"></i></span>
                <span>I accept and agree to the <a href="#" className="regTerms">Terms & Conditions</a></span>
            </label>
						<span className="text-danger">{errors.accept}</span>
        </div></div>
          </div>
          <div className="form-group">
          <div className="form-group clearfix fadeIn third recaptchaMain">
			<ReCAPTCHA
			ref={(r) => this.recaptcha = r}
			sitekey={Recaptchakey}
			grecaptcha={grecaptchaObject}
			onChange={this.captchaChange}
			/>
			<span className="text-danger">{errors.recaptcha}</span>
			</div>
          </div>
            <div className="form-group">
            <input type="submit" className="btn-block fadeIn fourth" disabled={this.state.disable} value="Register" />
          </div>
          </form>
          <div id="formFooter" className="fadeIn fourth" >
            <h6 className="text-center">Already registered user? <Link to="/Login" className="underlineHover">Login!</Link></h6>
          </div>
          </div>
          <div className="tab-pane fade" id="nav-mobileLogin" role="tabpanel" aria-labelledby="nav-mobileLogin-tab">
         { this.state.otpver==''?
            <form className="stoForm landingForm" noValidate onSubmit={this.mobileSubmit}>
			<div className="form-group input-group mobNumber">
				<label>Mobile Number</label>
				<PhoneInput
				placeholder="Enter phone number"
				country={'IN'}
				value={this.state.phone}
				onChange={phone => this.setState({ phone })}
				/>
				<span className="text-danger">{errors.phone}</span>
			</div>
          <div className="form-group fadeIn third">
             <label>Password</label>
				<input
					onChange={this.onChange}
					value={this.state.mobilepassword}
					error={errors.mobilepassword}
					id="mobilepassword"
					type="password"
					className={classnames("form-control", {
					invalid: errors.mobilepassword
				})}
				/>
				<span className="text-danger">{errors.mobilepassword}</span>
          </div>
          <div className="form-group fadeIn third">
             <label>Referral Code (Optional)</label>
            <input
					onChange={this.onChange}
					value={this.state.mobilereferalcode}
					error={errors.mobilereferalcode}
					id="mobilereferalcode"
					type="text"
					className="form-control"
					/>
          </div>
          <div className="form-group clearfix fadeIn third">
            <div className="float-left"><div className="checkbox">
            <label>
                <input
						 	 onChange={this.onChangeCheckbox}
							 value={this.state.mobileaccept}
							 error={errors.mobileaccept}
							 id="mobileaccept"
							 type="checkbox"
							 className={classnames("form-control", {
									 invalid: errors.mobileaccept
							 })}
					   />
                <span className="cr"><i className="cr-icon fa fa-check"></i></span>
                <span>I accept and agree to the <a href="#" className="regTerms">Terms & Conditions</a></span>
            </label>
				<span className="text-danger">{errors.mobileaccept}</span>
        </div></div>
          </div>
          <div className="form-group clearfix fadeIn third recaptchaMain">
			<ReCAPTCHA
			ref={(r) => this.recaptcha = r}
			sitekey={Recaptchakey}
			grecaptcha={grecaptchaObject}
			onChange={this.captchaChange1}
			/>
			<span className="text-danger">{errors.mobrecaptcha}</span>
			</div>
            	<div className="form-group">
            <input type="submit" className="btn-block fadeIn fourth" value="Register" />
          </div>
          </form> : 

          <form className="stoForm landingForm" noValidate onSubmit={this.otpSubmit}>
          	 <div className="form-group fadeIn third">
          	 <p>Please enter the verification code that has been sent to {this.state.phone}</p>
             <label>SMS verification code</label>
            <input
					onChange={this.onChange}
					value={this.state.otpcode}
					error={errors.otpcode}
					id="otpcode"
					type="text"
					className="form-control"
					/>
          </div>
          <div className="form-group">
            <input type="submit" className="btn-block fadeIn fourth" value="Submit" />
          </div>
          <div className="form-group">
          <h6 className="text-center">
          <p style={{textAlign:'center'}}>No SMS received？<a onClick={this.mobileSubmit} className="underlineHover" style={{color:'#d6b032 !important'}}>Resend otp</a></p>
          </h6>
          </div>
          </form>
      }
          <div id="formFooter" className="fadeIn fourth">
            <h6 className="text-center">Already registered user? <Link to="/Login" className="underlineHover">Login!</Link></h6>
          </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</header>
<Footer />
		</div>
	);
	}
}



Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    mobregisterUser: PropTypes.func.isRequired,
    otptoUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { registerUser,mobregisterUser,otptoUser }
)(withRouter(Register));
