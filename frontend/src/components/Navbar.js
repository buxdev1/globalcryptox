import React, { Component } from 'react'
import Logo from "../images/Logo-small.png"
// import {Link} from 'react-router-dom';
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";

import TradeHeader from './TradeHeader'
import Navbar_login from './Navbar_login'

class Navbar extends Component {

componentDidMount() {
    this.getData()
}
getData() {
  let userid = this.props.auth.user.id;
  console.log('userid');
  console.log(userid);
  console.log('typeof this.props.auth.user.id');
  console.log(typeof this.props.auth.user.id);
}

render() {
    const { user } = this.props.auth;
    return <div>
    {(typeof this.props.auth.user.id!='undefined')?<TradeHeader />:<Navbar_login />}
    </div>
  }spot
}

Navbar.propTypes = {
    profileUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps
)(withRouter(Navbar));
// export default Navbar
