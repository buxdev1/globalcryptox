import React, { Component } from 'react'
import Logo from "../images/Logo-small.png"
// import {Link} from 'react-router-dom';
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";

import TradeHeader from './TradeHeader'

class Navbar extends Component {
  constructor(props) {
      super(props);
      this.state = {
          navpair               : '',
      }
  }
componentDidMount() {
    this.getData();
    if(!localStorage.getItem('curpair'))
    {
      localStorage.setItem('curpair','BTC-USD');
      this.setState({navpair:'BTC-USD'});
    }
    else
    {
      this.setState({navpair:localStorage.getItem('curpair')});
    }

    if(!localStorage.getItem('curpair1'))
    {
      localStorage.setItem('curpair1','BTC-USD');
      this.setState({navpair:'BTC-USD'});
    }
    else
    {
      this.setState({navpair:localStorage.getItem('curpair1')});
    }
}
getData() {
  let userid = this.props.auth.user.id;
}

render() {
    const { user } = this.props.auth;

    return <div>
      <nav className="navbar navbar-expand-md fixed-top customNav wow fadeInDown" data-wow-delay=".2s">
        <div className="container">
          <Link to="/" className="navbar-brand"><img src={Logo} alt="Crypto Trade X Exchange" /></Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i className="fas fa-bars"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
            <li className="nav-item">
                 <Link to={"/Spot/"+localStorage.getItem('curpair1')} className="dropdown-item nav-link">Trade</Link>
              </li>

               {/* <li className="nav-item"> */}
                 {/* <a href="https://unlimitedmoney.io/" target="_blank" className="dropdown-item nav-link">Unlimited Money</a> */}
              {/* </li> */}
              {/* <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Trade
                  </a>
                  <div className="dropdown-menu sm-menu">
                   <Link to={"/Spot/"+localStorage.getItem('curpair1')} className="dropdown-item nav-link">Spot</Link>
                  <Link to={"/Trade/"+localStorage.getItem('curpair')} className="dropdown-item nav-link">Derivatives</Link>
                  </div>
              </li>*/}
              {/* <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Finance
                  </a>
                  <div className="dropdown-menu sm-menu">
                    <Link to="#" className="dropdown-item">Finance (Comming Soon)</Link>
                  </div>
              </li> 

              
              <li className="nav-item active mr-2">
                <Link to="/Leverage" className="nav-link">Leverage</Link>
              </li>*/}
              <li className="nav-item">
                <Link to="/Bonus" className="nav-link">Bonus</Link>
              </li>
              
              <li className="nav-item">
                <Link to="/Login" className="nav-link">Login</Link>
              </li>
              <li className="nav-item">
                <Link to="/Register" className="nav-link">Register</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  }
}




Navbar.propTypes = {
    profileUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps
)(withRouter(Navbar));
// export default Navbar
