import React, { Component } from "react";
import BannerX from "../images/xicon.png";
import BannerImg from "../images/bannerImg.png";
import BannerImg2 from "../images/BannerImg2.jpg"
import BannerImg3 from "../images/BannerImg3.png"
import BannerImg4 from "../images/BannerImg4.png"
import BannerImg5 from "../images/BannerImg5.png"
import BannerImg6 from "../images/BannerImg6.jpg"
import Trademonitor from "../images/tradeMonitor.png";
import MobilePhones from "../images/mobilePhones.png";
import MacAppIcon from "../images/macAppIcon.png";
import GooglePlayIcon from "../images/googlePlayIcon.png";
import { store } from "react-notifications-component";
import Navbar from "./Navbar";
import Footer from "./Footer";
import axios from "axios";
import { getPertual } from "../actions/authActions";
import { Link, withRouter } from "react-router-dom";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import keys from "../actions/config";
const url = keys.baseUrl;

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      identifier: "",
      subject: "",
      content: "",
      errors: {},
      banner: {},
      banner_sec: {},
      banner_sec_1: {},
      features: {},
      features_1: {},
      features_2: {},
      features_3: {},
      features_4: {},
      start: {},
      start_1: {},
      trading: {},
      coming: {},
      records: [],
      spotrecords: [],
      faq: [],
      btcprice: 0,
      ordertype: "",
      btcshowarray: [],
      ethshowarray: [],
      usdtshowarray: [],
      busdshowarray:[],
      derivativeordertype: "",
      derivativebtcshowarray: [],
      derivativeethshowarray: [],
      derivativeusdtshowarray: [],
    };

    // console.log(this.state, "state");
  }

  componentDidMount() {
    this.getData();
    this.getData1();
    this.getData2();
    this.getData3();
    this.getData4();
    this.getData5();
    this.getData6();
    this.getData7();
    this.getData8();
    this.getData9();
    this.getData10();
    this.getData11();
    this.getData12();
    this.getData13();
    this.getData14();
    let withdraw_id = this.props.match.params.id;
    if (typeof withdraw_id != "undefined" && withdraw_id != "") {
      let passdata = { withdrawid: withdraw_id };
      axios
        .post(url + "cryptoapi/withdrawrequest", passdata)
        .then((res) => {
          if (res.data.status) {
            store.addNotification({
              title: "Wonderful!",
              message: res.data.message,
              type: "success",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 2000,
                onScreen: true,
              },
            });
          } else {
            store.addNotification({
              title: "Warning!",
              message: res.data.message,
              type: "danger",
              insert: "top",
              container: "top-right",
              animationIn: ["animated", "fadeIn"],
              animationOut: ["animated", "fadeOut"],
              dismiss: {
                duration: 2000,
                onScreen: true,
              },
            });
          }
          this.props.history.push("/Login");
        })
        .catch();
    }
  }
  createMarkup = () => {
    return { __html: this.state.banner.data.content };
  };
  createMarkup1 = () => {
    return { __html: this.state.banner_sec.data.content };
  };
  createMarkup2 = () => {
    return { __html: this.state.banner_sec_1.data.content };
  };
  createMarkup3 = () => {
    return { __html: this.state.features.data.content };
  };
  createMarkup4 = () => {
    return { __html: this.state.features_1.data.content };
  };
  createMarkup5 = () => {
    return { __html: this.state.features_2.data.content };
  };
  createMarkup6 = () => {
    return { __html: this.state.features_3.data.content };
  };
  createMarkup7 = () => {
    return { __html: this.state.features_4.data.content };
  };

  createMarkup8 = () => {
    return { __html: this.state.start.data.content };
  };
  createMarkup9 = () => {
    return { __html: this.state.start_1.data.content };
  };
  createMarkup10 = () => {
    return { __html: this.state.coming.data.content };
  };
  createMarkup11 = () => {
    return { __html: this.state.trading.data.content };
  };

  getData() {
    axios
      .get(url + "cryptoapi/banner")
      .then((res) => {
        this.setState({ banner: res });
      })
      .catch();
    console.log(this.setState, "this.setState");
    console.log(this.props, "authget1");
  }

  getData1() {
    axios
      .get(url + "cryptoapi/banner_1")
      .then((res) => {
        this.setState({ banner_sec: res });
      })
      .catch();
    console.log(this.setState, "this.setState");
  }

  getData2() {
    axios
      .get(url + "cryptoapi/banner_2")
      .then((res) => {
        console.log("banner s1",res);
        this.setState({ banner_sec_1: res });
      })
      .catch();
  }
  getData3() {
    axios
      .get(url + "cryptoapi/features")
      .then((res) => {
        this.setState({ features: res });
      })
      .catch();
  }

  getData4() {
    axios
      .get(url + "cryptoapi/features_1")
      .then((res) => {
        this.setState({ features_1: res });
      })
      .catch();
  }
  getData4() {
    axios
      .get(url + "cryptoapi/features_1")
      .then((res) => {
        this.setState({ features_1: res });
      })
      .catch();
  }
  getData5() {
    axios
      .get(url + "cryptoapi/features_2")
      .then((res) => {
        this.setState({ features_2: res });
      })
      .catch();
  }

  getData6() {
    axios
      .get(url + "cryptoapi/features_3")
      .then((res) => {
        // console.log(res, "rezzz12");
        this.setState({ features_3: res });
      })
      .catch();
    console.log(this.setState, "this.setState");
  }
  getData7() {
    axios
      .get(url + "cryptoapi/features_4")
      .then((res) => {
        this.setState({ features_4: res });
      })
      .catch();
  }

  getData8() {
    axios
      .get(url + "cryptoapi/start")
      .then((res) => {
        this.setState({ start: res });
      })
      .catch();
  }

  getData9() {
    axios
      .get(url + "cryptoapi/start_1")
      .then((res) => {
        console.log(res, "rezzz12");
        this.setState({ start_1: res });
      })
      .catch();
    console.log(this.setState, "this.setState");
  }
  getData10() {
    axios
      .get(url + "cryptoapi/coming")
      .then((res) => {
        this.setState({ coming: res });
      })
      .catch();
  }
  getData11() {
    axios
      .get(url + "cryptoapi/trading")
      .then((res) => {
        this.setState({ trading: res });
      })
      .catch();
  }
  getData12() {
    axios
      .get(url + "cryptoapi/faq_1")
      .then((res) => {
        this.setState({ faq: res.data });
      })
      .catch();
  }
  getData13() {
    axios
      .post(url + "cryptoapi/perpetual-data")
      .then((res) => {
        console.log(res, "reskdhfksdfhksdhfksdhfkhsdf");
        if (res.data.status) {
          var btcprice = 0;
          var perparray = res.data.data;
          var indexarr = [0, 1, 2, 3, 4];
          var outputarr = indexarr.map((i) => perparray[i]);
          var btcindex = res.data.data.findIndex(
            (x) => x.tiker_root === "BTCUSD"
          );
          if (btcindex != -1) {
            var btcprice = res.data.data[btcindex].markprice;
          }
          this.setState({ records: outputarr, btcprice: btcprice });
        }
      })
      .catch();
  }
  getData14() {
    axios
      .post(url + "cryptoapi/spotpair-data")
      .then((res) => {
        console.log("spotdetails", res.data);
        if (res.data.status) {
          var btcprice = 0;
          var perparray = res.data.data;
          var indexarr = [1, 0, 2, 3];
          var outputarr = indexarr.map((i) => perparray[i]);

          this.setState({ spotrecords: perparray });
            var dash = [];
            var btcarray = perparray;
            for (var i = 0; i < btcarray.length; i++) {
            if (btcarray[i].second_currency == "USD") {
            dash.push(btcarray[i]);
            var arr = dash.sort(this.sort_by("secvolume",false));
            this.setState({ usdtshowarray: arr });
            // console.log("btc array ",this.state.btcshowarray)
            }
            }
            this.setState({
            ETHshow: false,
            USDTshow: true,
            ordertype: "USD",
            BTCshow: false,
            });
        }
      })
      .catch();
  }

  sort_by(field, reverse, primer) {

    var key = primer ?
    function(x) {console.log(x[field],'x[field]'); return primer(x[field])} :
    function(x) {return x[field]};

    reverse = [-1, 1][+!!reverse];

    return function (a, b) {
        return a = key(a), b = key(b), reverse * (+(parseFloat(a) > parseFloat(b)) - +(parseFloat(b) > parseFloat(a)));
    }
}

  ordertype_changes = (param) => {
    // console.log('fhjsdkjhfskjdhfksdhjf');
    var type = param.target.innerHTML;
    var dash = [];
    console.log("typeadasdas", type);
    if (type == "BTC") {
      var btcarray = this.state.spotrecords;
      for (var i = 0; i < btcarray.length; i++) {
        if (btcarray[i].second_currency == "BTC") {
          dash.push(btcarray[i]);
          // this.setState({ btcshowarray: dash });
          // console.log("btc array ",this.state.btcshowarray)
          console.log(dash,'dashdashdash')
           var arr = dash.sort(this.sort_by("secvolume",false));
          this.setState({ btcshowarray: arr });
        }

      }
      this.setState({
        ETHshow: false,
        USDTshow: false,
        ordertype: type,
        BTCshow: true,
        BUSDshow:false
      });
    } else if (type == "ETH") {
      var etharray = this.state.spotrecords;
      for (var i = 0; i < etharray.length; i++) {
        if (etharray[i].second_currency == "ETH") {
          dash.push(etharray[i]);
          this.setState({ ethshowarray: dash });
          // console.log("btc array ",this.state.btcshowarray)
        }
      }
      this.setState({
        ETHshow: true,
        USDTshow: false,
        ordertype: type,
        BTCshow: false,
        BUSDshow:false
      });
    }else if(type=="BUSD"){
      var busdarray = this.state.spotrecords;
      for (var i = 0; i < busdarray.length; i++) {
        if (busdarray[i].second_currency == "BUSD") {
          dash.push(busdarray[i]);
          this.setState({ busdshowarray: dash });
          // console.log("btc array ",this.state.btcshowarray)
        }
      }
      this.setState({
        ETHshow: false,
        USDTshow: false,
        ordertype: type,
        BTCshow: false,
        BUSDshow:true
      });
    }
    else {
      var usdtarray = this.state.spotrecords;
      for (var i = 0; i < usdtarray.length; i++) {
        if (usdtarray[i].second_currency == "USD") {
          dash.push(usdtarray[i]);
          var arr = dash.sort(this.sort_by("secvolume",false,parseInt));
          this.setState({ usdtshowarray: arr });
          // console.log("btc array ",this.state.btcshowarray)
        }
      }
      this.setState({
        ETHshow: false,
        USDTshow: true,
        ordertype: type,
        BTCshow: false,
        BUSDshow:false
      });
    }
  };

  derivativeordertype_changes = (param) => {
    // console.log('fhjsdkjhfskjdhfksdhjf');
    var derivtype = param.target.innerHTML;
    var derivdash = [];
    console.log("derivtyp", derivtype);
    if (derivtype == "BTC") {
      var derivbtcarray = this.state.records;
      for (var i = 0; i < derivbtcarray.length; i++) {
        if (derivbtcarray[i].second_currency == "BTC") {
          derivdash.push(derivbtcarray[i]);
          // console.log("btc array ",this.state.btcshowarray)
        }

      }
      this.setState({
        derivativeETHshow: false,
        derivativeUSDTshow: false,
        derivativeordertype: derivtype,
        derivativeBTCshow: true,
      });
    } else if (derivtype == "ETH") {
      var derivetharray = this.state.records;
      for (var i = 0; i < derivetharray.length; i++) {
        if (derivetharray[i].second_currency == "ETH") {
          derivdash.push(derivetharray[i]);
          this.setState({ derivativeethshowarray: derivdash });
          // console.log("btc array ",this.state.btcshowarray)
        }
      }
      this.setState({
        derivativeETHshow: true,
        derivativeUSDTshow: false,
        derivativeordertype: derivtype,
        derivativeBTCshow: false,
      });
    } else {
      var derivusdtarray = this.state.records;
      for (var i = 0; i < derivusdtarray.length; i++) {
        if (derivusdtarray[i].second_currency == "USD") {
          derivdash.push(derivusdtarray[i]);
          this.setState({ derivativeusdtshowarray: derivdash });
          // console.log("btc array ",this.state.btcshowarray)
        }
      }
      this.setState({
        derivativeETHshow: false,
        derivativeUSDTshow: true,
        derivativeordertype: derivtype,
        derivativeBTCshow: false,
      });
    }
  };
  exchangetypespot = e => {
    // this.setState({ordertype:"BTC"})
  }
  exchangetypederivative = e => {
    this.setState({derivativeordertype:"derivative"})
  }


  render() {
    return (
      <div>
        <Navbar />

        <header class="homeBanner">
          {this.state.banner.data ? (
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <div
                    class="homeBannerText wow fadeInUp"
                    data-wow-delay=".25s"
                  >
                    <div
                      dangerouslySetInnerHTML={this.createMarkup()}
                      className="editor"
                    ></div>

                    <ul>
                      {this.state.banner_sec.data ? (
                        <li>
                          <span class="bannerIcon"></span>{" "}
                          <div
                            dangerouslySetInnerHTML={this.createMarkup1()}
                            className="editor"
                          ></div>
                        </li>
                      ) : (
                        ""
                      )}
                      {this.state.banner_sec_1.data ? (
                        <li>
                          <span class="bannerIcon"></span>{" "}
                          <div
                            dangerouslySetInnerHTML={this.createMarkup2()}
                            className="editor"
                          ></div>
                        </li>
                      ) : (
                        ""
                      )}
                    </ul>
                    {typeof this.props.auth.user.id == "undefined" ? (
                      <h6>
                        Register in our platform with <span>FREE</span> bonus
                        coins
                      </h6>
                    ) : (
                      ""
                    )}
                    <p>
                      {" "}
                      {typeof this.props.auth.user.id != "undefined" ? (
                        <Link
                          to="/trade/BTC-USD"
                          className="btn btnBlue hvr-shadow"
                        >
                          Start Exchange
                        </Link>
                      ) : (
                        <Link to="/Register" className="btn btnBlue hvr-shadow">
                          Start Exchange
                        </Link>
                      )}
                    </p>
                  </div>
                </div>
                <div class="col-md-6">
                  {this.state.banner_sec_1.data ? (
                    <div
                      class="homeBannerImg wow flipInY"
                      data-wow-delay=".15s;"
                    >
                      <img
                        src={
                          keys.imageUrl +
                          "cms_images/" +
                          this.state.banner_sec_1.data.image[0]
                        }
                        className="img-fluid"
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </header>

        <section className="coinTable">
          <div className="container">
            <div className="row">
              <div className="col-md-12 ">
                <nav>
                  <div className="nav nav-tabs" id="nav-tab" role="tablist">
                    <a
                      className="nav-item  nav-link active"
                      id="nav-INR-tab"
                      data-toggle="tab"
                      href="#nav-INR"
                      role="tab"
                      aria-controls="nav-INR"
                      aria-selected="false"
                      onClick={this.exchangetypespot}
                    >
                      Spot
                    </a>
                   {/* <a
                      className="nav-item nav-link "
                      id="nav-USD-tab"
                      data-toggle="tab"
                      href="#nav-USD"
                      role="tab"
                      aria-controls="nav-USD"
                      aria-selected="false"
                      onClick={this.exchangetypederivative}
                    >
                      Derivatives
                    </a> */ }
                  </div>
                </nav>
                <div className="tab-content" id="nav-tabContent">
                  <div
                    className="tab-pane fade  "
                    id="nav-USD"
                    role="tabpanel"
                    aria-labelledby="nav-USD-tab"
                  >
                    <div className="selectPairCard">

                    </div>
                    <div className="table-responsive valuesTable">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Pair</th>
                            <th>Max Leverage</th>
                            <th>Spot Price</th>
                            <th>Last Price</th>
                            <th>24H Change</th>
                        {/*}    <th>24H Volume</th>*/}
                          </tr>
                        </thead>
                        <tbody>
                           {this.state.records.map((item, i) => {
                            var classs =
                              item.change < 0 ? "redText" : "greenText";
                            var floating = item.first_currency == "XRP" ? 4 : 2;
                            var usdvalue =
                              item.first_currency != "BTC"
                                ? item.volume * item.markprice
                                : 0;
                            var volume =
                              item.first_currency != "BTC"
                                ? usdvalue / this.state.btcprice
                                : item.volume;
                            return (
                              <tr>
                                <td>{item.tiker_root}</td>
                                <td>{item.leverage + "X"}</td>
                                <td>
                                  {(item.markprice
                                    ? parseFloat(item.markprice).toFixed(
                                        floating
                                      )
                                    : 0) + " $"}
                                </td>
                                <td>
                                  {(item.last
                                    ? parseFloat(item.last).toFixed(floating)
                                    : 0) + " $"}
                                </td>
                                <td>
                                  <span className={classs}>
                                    {item.change
                                      ? parseFloat(item.change).toFixed(2)
                                      : 0}
                                    %
                                  </span>
                                </td>
                                {/*}<td>
                                  {(item.volume
                                    ? parseFloat(Math.abs(volume)).toFixed(8)
                                    : 0) + " BTC"}
                                </td>*/}
                              </tr>
                            );
                          })}


                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade show active"
                    id="nav-INR"
                    role="tabpanel"
                    aria-labelledby="nav-INR-tab"
                  >
                    <div className="selectPairCard">

                      <a
                        href="javascript:void(0)"
                        className={(this.state.ordertype == 'USD')?"btn  mr-3 btnBlue px-4":"btn  mr-3 px-4" }
                        onClick={this.ordertype_changes.bind(this)}
                      >
                        USD
                      </a>
                      <a
                        href="javascript:void(0)"
                        className= {(this.state.ordertype == 'BTC')?"btn  mr-3 btnBlue px-4":"btn  mr-3 px-4" }
                        onClick={this.ordertype_changes.bind(this)}
                      >
                        BTC
                      </a>
                     
                    
                    </div>
                    <div className="table-responsive valuesTable">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Pair</th>
                            <th>Spot Price</th>
                            <th>Last Price</th>
                            <th>24H Change</th>
                            {/*}<th>24H Volume</th> */}
                          </tr>
                        </thead>
                        <tbody>
                          {/* {this.state.spotrecords.map((item, i) => {
                            var classs =
                              item.change < 0 ? "redText" : "greenText";
                            var volume = item.volume;
                            var floating =
                              item.tiker_root == "BTCUSDT" ||
                              item.tiker_root == "ETHUSDT" ||
                              item.tiker_root == "BTCUSD"
                                ? 2
                                : 8;
                            //var volume   = (item.first_currency!='BTC')?usdvalue/this.state.btcprice:item.volume;
                            var coinicon =
                              item.second_currency == "BTC" ? "BTC" : "$";
                            return (
                              <tr>
                                <td>{item.tiker_root}</td>
                                <td>
                                  {((item.markprice && item.markprice!=null)
                                    ? parseFloat(item.markprice).toFixed(
                                        floating
                                      )
                                    : item.last
                                    ? parseFloat(item.last).toFixed(floating)
                                    : 0) +
                                    " " +
                                    coinicon}
                                </td>
                                <td>
                                  {(item.last
                                    ? parseFloat(item.last).toFixed(floating)
                                    : parseFloat(item.mark_price).toFixed(
                                        floating
                                      )) +
                                    " " +
                                    coinicon}
                                </td>
                                <td>
                                  <span className={classs}>
                                    {item.change
                                      ? parseFloat(item.change).toFixed(2)
                                      : 0}
                                    %
                                  </span>
                                </td>
                                <td>
                                  {(item.volume
                                    ? parseFloat(Math.abs(volume)).toFixed(8)
                                    : 0) +
                                    " " +
                                    item.first_currency}
                                </td>
                              </tr>
                            );
                          })} */}

                          {this.state.ordertype == "BTC"
                            ? this.state.btcshowarray.map((item, i) => {
                                var classs =
                                  item.change < 0 ? "redText" : "greenText";
                                var volume = item.volume;
                                var floating =
                                  (item.second_currency == "BTC" ||
                                  item.second_currency == "ETH" )
                                    ? 8
                                    : 2;
                                //var volume   = (item.first_currency!='BTC')?usdvalue/this.state.btcprice:item.volume;
                                var coinicon =
                                  item.second_currency != "USD" ? "BTC" : "$";
                                return (
                                  <tr>
                                    <td>{item.tiker_root}</td>
                                    <td>
                                      {(item.markprice && item.markprice!=''
                                        ? parseFloat(item.markprice).toFixed(
                                            floating
                                          )
                                        : item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      {(item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : parseFloat(item.mark_price).toFixed(
                                            floating
                                          )) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      <span className={classs}>
                                        {item.change
                                          ? parseFloat(item.change).toFixed(2)
                                          : 0}
                                        %
                                      </span>
                                    </td>
                                    {/*}<td>
                                      {(item.secvolume
                                        ? parseFloat(Math.abs(item.secvolume)).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>*/}
                                  </tr>
                                );
                              })
                            : this.state.ordertype == "ETH"
                            ? this.state.ethshowarray.map((item, i) => {
                                var classs =
                                  item.change < 0 ? "redText" : "greenText";
                                var volume = item.volume;
                               var floating =
                                  (item.second_currency == "BTC" ||
                                  item.second_currency == "ETH" )
                                    ? 8
                                    : 2;
                                //var volume   = (item.first_currency!='BTC')?usdvalue/this.state.btcprice:item.volume;
                                var coinicon =
                                  item.second_currency != "USD" ? "BTC" : "$";
                                return (
                                  <tr>
                                    <td>{item.tiker_root}</td>
                                    <td>
                                      {(item.markprice
                                        ? parseFloat(item.markprice).toFixed(
                                            floating
                                          )
                                        : item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      {(item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : parseFloat(item.mark_price).toFixed(
                                            floating
                                          )) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      <span className={classs}>
                                        {item.change
                                          ? parseFloat(item.change).toFixed(2)
                                          : 0}
                                        %
                                      </span>
                                    </td>
                                {/*}   <td>
                                      {(item.secvolume
                                        ? parseFloat(Math.abs(item.secvolume)).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>*/}
                                  </tr>
                                );
                              })
                            : this.state.ordertype == "BUSD"
                            ? this.state.busdshowarray.map((item, i) => {
                              console.log("busdshowarray",this.state.busdshowarray)
                                var classs =
                                  item.change < 0 ? "redText" : "greenText";
                                var volume = item.volume;
                               var floating =
                                  (item.second_currency == "BTC" ||
                                  item.second_currency == "ETH" )
                                    ? 8
                                    : 2;
                                //var volume   = (item.first_currency!='BTC')?usdvalue/this.state.btcprice:item.volume;
                                var coinicon =
                                  item.second_currency != "USD" ? "BUSD" : "$";
                                return (
                                  <tr>
                                    <td>{item.tiker_root}</td>
                                    <td>
                                      {(item.markprice
                                        ? parseFloat(item.markprice).toFixed(
                                            floating
                                          )
                                        : item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      {(item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : parseFloat(item.mark_price).toFixed(
                                            floating
                                          )) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      <span className={classs}>
                                        {item.change
                                          ? parseFloat(item.change).toFixed(2)
                                          : 0}
                                        %
                                      </span>
                                    </td>
                                  {/*} <td>
                                      {(item.secvolume
                                        ? parseFloat(Math.abs(item.secvolume)).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>*/}
                                  </tr>
                                );
                              })
                            :
                            this.state.ordertype == "USD"
                            ? this.state.usdtshowarray.map((item, i) => {
                                var classs =
                                  item.change < 0 ? "redText" : "greenText";
                                var volume = item.volume;
                               var floating =
                                  (item.second_currency == "BTC" ||
                                  item.second_currency == "ETH" )
                                    ? 8
                                    : 2;
                                //var volume   = (item.first_currency!='BTC')?usdvalue/this.state.btcprice:item.volume;
                                 var coinicon =
                                  item.second_currency != "USD" ? "BTC" : "$";
                                return (
                                  <tr>
                                    <td>{item.tiker_root}</td>
                                    <td>
                                      {(item.markprice
                                        ? parseFloat(item.markprice).toFixed(
                                            floating
                                          )
                                        : item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      {(item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : parseFloat(item.mark_price).toFixed(
                                            floating
                                          )) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      <span className={classs}>
                                        {item.change
                                          ? parseFloat(item.change).toFixed(2)
                                          : 0}
                                        %
                                      </span>
                                    </td>
                                  {/*} <td>
                                      {(item.secvolume
                                        ? parseFloat(Math.abs(item.secvolume)).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +coinicon
                                        }
                                    </td> */}
                                  </tr>
                                );
                              })
                            :

                            this.state.spotrecords.map((item, i) => {
                                var classs =
                                  item.change < 0 ? "redText" : "greenText";
                                var volume = item.volume;
                                var floating =
                                  (item.second_currency == "BTC" ||
                                  item.second_currency == "ETH")
                                    ? 8
                                    : 2;
                                //var volume   = (item.first_currency!='BTC')?usdvalue/this.state.btcprice:item.volume;
                                 var coinicon =
                                  item.second_currency != "USD" ? "BTC" : "$";
                                return (
                                  <tr>
                                    <td>{item.tiker_root}</td>
                                    <td>
                                      {(item.markprice
                                        ? parseFloat(item.markprice).toFixed(
                                            floating
                                          )
                                        : item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : 0) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      {(item.last
                                        ? parseFloat(item.last).toFixed(
                                            floating
                                          )
                                        : parseFloat(item.mark_price).toFixed(
                                            floating
                                          )) +
                                        " " +
                                        coinicon}
                                    </td>
                                    <td>
                                      <span className={classs}>
                                        {item.change
                                          ? parseFloat(item.change).toFixed(2)
                                          : 0}
                                        %
                                      </span>
                                    </td>
                                  {/*} <td>
                                      {(item.secvolume
                                        ? parseFloat(Math.abs(item.secvolume)).toFixed(
                                            8
                                          )
                                        : 0) +
                                        " " +
                                        item.second_currency}
                                    </td> */}
                                  </tr>
                                );
                              })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="cryptoFeatures">
          {this.state.features.data ? (
            <div className="container">
              <h3 className="pageTitle">
                <small>{this.state.features.data.identifier}</small>Features
              </h3>
              <p className="text-center mb-5">
                <div
                  dangerouslySetInnerHTML={this.createMarkup3()}
                  className="editor"
                ></div>
              </p>
              <div className="cryptoFeaturesMain">
                <div className="row">
                  <div className="col-md-6">
                    <div className="cryptoFeaturesCont mb-4 wow flipInX">
                      <div className="row">
                        <div className="col-lg-3 col-md-4 col-sm-3">
                          <div className="cryptoFeaturesContIcon">
                            <span className="featuresIcon1"></span>
                          </div>
                        </div>
                        {this.state.features_1.data ? (
                          <div className="col-lg-9 col-md-8 col-sm-9">
                            <div className="cryptoFeaturesContText">
                              <h4>{this.state.features_1.data.identifier}</h4>
                              <p>
                                {" "}
                                <div
                                  dangerouslySetInnerHTML={this.createMarkup4()}
                                  className="editor"
                                ></div>
                              </p>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    {/*<div className="cryptoFeaturesCont mb-4 wow flipInX">
                      <div className="row">
                        <div className="col-lg-3 col-md-4 col-sm-3">
                          <div className="cryptoFeaturesContIcon">
                            <span className="featuresIcon2"></span>
                          </div>
                        </div>
                        {this.state.features_2.data ? (
                          <div className="col-lg-9 col-md-8 col-sm-9">
                            <div className="cryptoFeaturesContText">
                              <h4>{this.state.features_2.data.identifier}</h4>
                              <p>
                                {" "}
                                <div
                                  dangerouslySetInnerHTML={this.createMarkup5()}
                                  className="editor"
                                ></div>
                              </p>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div> */}
                    <div className="cryptoFeaturesCont mb-4 wow flipInX">
                      <div className="row">
                        <div className="col-lg-3 col-md-4 col-sm-3">
                          <div className="cryptoFeaturesContIcon">
                            <span className="featuresIcon3"></span>
                          </div>
                        </div>
                        {this.state.features_3.data ? (
                          <div className="col-lg-9 col-md-8 col-sm-9">
                            <div className="cryptoFeaturesContText">
                              <h4>{this.state.features_3.data.identifier}</h4>
                              <p>
                                <div
                                  dangerouslySetInnerHTML={this.createMarkup6()}
                                  className="editor"
                                ></div>
                              </p>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="cryptoFeaturesCont mb-4 wow flipInX">
                      <div className="row">
                        <div className="col-lg-3 col-md-4 col-sm-3">
                          <div className="cryptoFeaturesContIcon">
                            <span className="featuresIcon4"></span>
                          </div>
                        </div>
                        {this.state.features_4.data ? (
                          <div className="col-lg-9 col-md-8 col-sm-9">
                            <div className="cryptoFeaturesContText">
                              <h4>{this.state.features_4.data.identifier}</h4>
                              <p>
                                <div
                                  dangerouslySetInnerHTML={this.createMarkup7()}
                                  className="editor"
                                ></div>
                              </p>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div
                      className="cryptoFeaturesImg wow bounceIn"
                      data-wow-delay=".5s"
                    >
                      <img
                        src={
                          keys.imageUrl +
                          "cms_images/" +
                          this.state.features.data.image[0]
                        }
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </section>
        <section className="homeCTA">
          {this.state.trading.data ? (
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="homeCTACont wow fadeIn" data-wow-delay=".5s">
                    <h6>{this.state.trading.data.identifier}</h6>
                    <h3>
                      <div
                        dangerouslySetInnerHTML={this.createMarkup11()}
                        className="editor"
                      ></div>
                    </h3>
                    <p className="text-center">
                      <a href="#" className="btn btnBlue">
                        Start earn cryptos Today!
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </section>
        <section className="getStarted">
          {this.state.start.data ? (
            <div className="container">
              <h3 className="pageTitle">{this.state.start.data.identifier}</h3>
              <div className="row">
                <div className="col-md-10 mx-auto">
                  <siv className="row mt-5">
                    <div className="col-sm-4">
                      <div className="getStartedCont wow bounceIn">
                        <div className="getStartIcon">
                          <span className="getStartIcon1"></span>
                        </div>
                        <h4>
                          {" "}
                          <div
                            dangerouslySetInnerHTML={this.createMarkup8()}
                            className="editor"
                          ></div>
                        </h4>
                      </div>
                    </div>
                    <div className="col-sm-4">
                      {this.state.start_1.data ? (
                        <div className="getStartedCont wow bounceIn">
                          <div className="getStartIcon">
                            <span className="getStartIcon2"></span>
                          </div>
                          <h4>{this.state.start_1.data.identifier}</h4>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="col-sm-4">
                      {this.state.start_1.data ? (
                        <div className="getStartedCont noarrow wow bounceIn">
                          <div className="getStartIcon">
                            <span className="getStartIcon3"></span>
                          </div>
                          <h4>
                            <div
                              dangerouslySetInnerHTML={this.createMarkup9()}
                              className="editor"
                            ></div>
                          </h4>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </siv>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </section>
        <section className="secFAQ">
          {this.state.faq ? (
            <div className="container">
              <div className="row">
                <div className="col-xl-10 col-lg-10 col-sm-12 ml-auto mr-auto">
                  <div className="faqCont">
                    <div className="homeAccordian wow fadeIn">
                      <h3 className="pageTitle">FAQ</h3>
                      <div id="accordion">
                        {this.state.faq.map((answer, i) => {
                          console.log(i, "iiii");
                          var view = i == 0 ? "show" : "";
                          var expand = i == 0 ? "true" : "false";
                          var btn =
                            i == 0 ? "btn btn-link" : "btn btn-link collapsed";
                          return (
                            <div className="card">
                              <div
                                className="card-header wow flipInX"
                                id={"heading" + i}
                              >
                                <h5 className="mb-0">
                                  <button
                                    className={btn}
                                    data-toggle="collapse"
                                    data-target={"#collapse" + i}
                                    aria-expanded={expand}
                                    aria-controls={"collapse" + i}
                                  >
                                    <span className="question">
                                      {i + 1}.{answer.question}
                                    </span>{" "}
                                    <i
                                      className="fa fa-plus"
                                      aria-hidden="true"
                                    ></i>
                                  </button>
                                </h5>
                              </div>

                              <div
                                id={"collapse" + i}
                                className={"collapse" + view}
                                aria-labelledby={"heading" + i}
                                data-parent="#accordion"
                              >
                                <div className="card-body">
                                  <p>{answer.answer}</p>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                      <a
                        className="btn btnBlue hvr-shadow float-right"
                        href="/Faq"
                      >
                        Read More
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            ""
          )}
        </section>
        <section className="AppComingSoonSection">
          <div className="container">
            {this.state.coming.data ? (
              <div className="row">
                <div className="col-lg-4 col-md-2 d-none d-md-block"></div>
                <div className="col-lg-4 col-md-6 col-sm-7 col-12">
                  <div
                    className="appComingSoonText wow fadeIn"
                    data-wow-delay=".5s"
                  >
                    <h3>{this.state.coming.data.identifier}</h3>
                    <p>
                      <div
                        dangerouslySetInnerHTML={this.createMarkup10()}
                        className="editor"
                      ></div>
                    </p>
                    <img src={MacAppIcon} className="img-fluid" />
                    <img src={GooglePlayIcon} className="img-fluid" />
                  </div>
                </div>

                <div className="col-lg-4 col-md-4 col-sm-5 d-none d-sm-block wow flipInX">
                  <img
                    src={
                      keys.imageUrl +
                      "cms_images/" +
                      this.state.coming.data.image[0]
                    }
                    className="img-fluid"
                  />
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

Landing.propTypes = {
  getPertual: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});
export default connect(mapStateToProps, {
  getPertual,
})(withRouter(Landing));
