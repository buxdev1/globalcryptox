import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import LeverageImg from "../images/leverage.png"
import LeverageChartImg from "../images/leverageBarChart.jpg"
import Slider, { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import io from "socket.io-client";


import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;
// Rc Slider - https://react-component.github.io/slider/
const marks = {
  1: '1x',
  25: '25x',
  50: '50x',
  75: '75x',
  100: '100x',
  125: '125x',
  150: {
    style: {
      color: '#00b5eb',
    },
    label: <strong>150x</strong>,
  },
};
class Leverage extends Component {

   constructor(props) {
        super(props);
        this.state = {
            subject:"",
            identifier:"",
            content: "",
            errors: {},
            Leverage : {},
            leveragevalue: 150,
            quantity: '',
            open_price: '',
            close_price: '',
            leverage: 0,
            initial_margin: 0,
            profitnloss: 0,
            profitnlossper: 0,
            order_cost: 0,
            btcprice: 0,
            order_value: 0,
            taker_fee: 0,
            buyorsell: "Buy",
            ROE: 0,
            records:[]
        };
        this.socket = io(keys.socketUrl,{secure: true,transports:['polling'],jsonp:false,rejectUnauthorized:false}); //live

        // this.socket = io(keys.socketUrl,{transports:['polling']});

        this.socket.on('PRICEDETAILS', function(data){
            priceappenddata(data);
        });
          const priceappenddata = data => {
              if(data.tiker_root=='BTCUSD' || data.tiker_root=='BTCUSDT')
              {
                  var floating = (this.state.chartpair=='XRPUSD')?4:2;
                  this.setState({markprice:parseFloat(data.markprice).toFixed(floating),last_price:data.last});
                  if(this.state.prevoiusprice!=0 && this.state.prevoiusprice < data.markprice )
                  {
                      this.setState({spotpricecolor:"greenText"});
                  }
                  else
                  {
                      this.setState({spotpricecolor:"pinkText"});
                  }
                  this.setState({prevoiusprice:this.state.markprice,floating:floating});
              }
          }
        }

      handleChangeComplete = value => {
          this.setState({
          leveragevalue: value
      });
      }

      handleShow1 = (param) =>{
    var type     = param.target.innerHTML;
    this.setState({buyorsell:type})
  }

      onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
      }

      calculation = () => {
        console.log(this.state.quantity);
        console.log(this.state.open_price);
        console.log(this.state.close_price);
        console.log(this.state.buyorsell);
        if(this.state.quantity!='' && this.state.open_price!='' && this.state.close_price!='')
        {

          var orderprice = this.state.open_price;

          var order_value1 = parseFloat(this.state.quantity*orderprice).toFixed(8);
      
          var order_value = parseFloat(order_value1/orderprice).toFixed(8);

          var initial_margin = parseFloat(order_value1)/this.state.leveragevalue;

          var fee = parseFloat(order_value1)*this.state.taker_fee/100;

          var margininbtc = parseFloat(initial_margin)/parseFloat(orderprice);
          var feeinbtc = parseFloat(fee)/parseFloat(orderprice);

          var order_cost = parseFloat(margininbtc)+parseFloat(feeinbtc);

          var profitnlossusd = (parseFloat(this.state.close_price)) - (parseFloat(this.state.open_price));
          var profitnlossusd = parseFloat(profitnlossusd)*parseFloat(this.state.quantity);
          var profitnloss    = parseFloat(profitnlossusd)/parseFloat(this.state.open_price);
          var open_value     = parseFloat(this.state.quantity)/parseFloat(this.state.open_price);
          var profitnlossper = (parseFloat(profitnloss)*100);
          var roe            = profitnlossper*10;
          if(this.state.buyorsell=='Buy')
          {
              this.setState({order_cost:parseFloat(order_cost).toFixed(8),order_value:parseFloat(order_value).toFixed(8),initial_margin:parseFloat(margininbtc).toFixed(8),profitnloss:parseFloat(profitnloss).toFixed(8),profitnlossper:parseFloat(profitnlossper).toFixed(2),ROE:parseFloat(roe).toFixed(2)});
          }
          else
          {
            this.setState({initial_margin:parseFloat(margininbtc).toFixed(8),profitnloss:parseFloat(profitnloss*-1).toFixed(8),profitnlossper:parseFloat(profitnlossper*-1).toFixed(2),ROE:parseFloat(roe*-1).toFixed(2)});
          }
        }
      }


        componentDidMount() {
            this.getData()
        };
    createMarkup = () => {
          return { __html: this.state.Leverage.data.content };
        }

    getData() {
           axios
           .get(url+"cryptoapi/leverage")
            .then(res => {
              this.setState({Leverage:res});
             })
            .catch()

             axios
           .post(url+"cryptoapi/perpetual-data")
            .then(res => {
              var btcindex = res.data.data.findIndex(x => (x.tiker_root) === 'BTCUSD');
              this.setState({records:res.data.data,btcprice:res.data.data[btcindex].markprice,taker_fee:res.data.data[btcindex].taker_fees});
             })
            .catch()
    }




	render() {
		return (<div>
			<Navbar />

			<section className="innerCMS">
  <div className="container">
    <div className="row">
      <div className="col-md-10 mx-auto">
        <img src={LeverageImg} className="img-fluid cmsTopImg mb-1" />
        <div className="darkBox contentPage leverageContent">
          <div className="tableHead tableHeadBlock">
            <h2>Leverage Trading</h2>
            <h6>Leverage enables you to get a much larger exposure to the market you’re trading than the amount you deposited to open the trade.</h6>
          </div>
          <div className="darkBoxSpace">
            {this.state.Leverage.data?
            <div>
                <p><div dangerouslySetInnerHTML={this.createMarkup()} className='editor'></div></p>
            </div>
            :''}
            <h3 className="text-center mb-4">Leverage Calculator</h3>

            <div className="row">
              <div className="col-md-6">
                <div className="CalculaterPart">
                <div className="mt-4 mb-5">
               {/* <Slider min={1} max={150} marks={marks} included={false} onChange={this.handleChangeComplete} value={this.state.leveragevalue} />*/}

                </div>
                <h6 className="text-center mb-4">Leverage : <span> {this.state.leveragevalue} </span></h6>

                   <div className="form-group inputWithText">
                      <label></label>
                     <div class="btn-group"><button class="btn btnGroupBorderBtn" onClick={this.handleShow1.bind(this)}>Buy</button><button class="btn btnGroupBorderBtn" onClick={this.handleShow1.bind(this)}>Sell</button></div>
                  </div>
                  <div className="form-group inputWithText">
                      <label>Entry Price</label>
                      <input name="open_price" id="open_price" onChange={this.onChange} className="form-control" value={this.state.open_price} type="text" />
                      <div className="input-group-append-icon"><small></small></div>
                  </div>
                  <div className="form-group inputWithText">
                      <label>Exit Price</label>
                      <input name="close_price" id="close_price" onChange={this.onChange} className="form-control" value={this.state.close_price} type="text" />
                      <div className="input-group-append-icon"><small></small></div>
                  </div>
                  <div className="form-group inputWithText">
                      <label>Quantity</label>
                      <input name="quantity" id="quantity" className="form-control" onChange={this.onChange} value={this.state.quantity} type="text" />
                      <div className="input-group-append-icon"><small></small></div>
                  </div>
                  <h6 className="text-center mb-4">Spot Price : <span className={this.state.spotpricecolor}>$ {this.state.markprice} </span></h6>
                  <div className="form-group">
                    <p className="text-center"><input type="submit" className="inputcenter btn buttonType2" onClick={this.calculation} value="Calculate" /></p>
                  </div>
                </div>
              </div>
               <div className="col-md-6">
                 <div className="calcResultBox">
                   <h4>Result</h4>
                   <p> <p>Leverage calculator allows you to calculate the total amount of buying power that you will get on the GlobalCryptoX platform based on your capital</p></p>
                   <ul className="calcResultList">
                     <li>
                       <p>Order Value</p>
                       <p><span>{this.state.order_value}</span> - BTC</p>
                     </li>
                     <li>
                       <p>Order Cost</p>
                       <p><span>{this.state.order_cost}</span> - BTC</p>
                     </li>
                     <li>
                       <p>Initial Margin</p>
                       <p><span>{this.state.initial_margin}</span> - BTC</p>
                     </li>
                     <li>
                       <p>PNL</p>
                       <p><span>{this.state.profitnloss}</span>- BTC</p>
                     </li>
                     <li>
                       <p>PNL %</p>
                       <p><span>{this.state.profitnlossper}</span>- %</p>
                     </li>
                     <li>
                       <p>ROE</p>
                       <p><span>{this.state.ROE}</span>- %</p>
                     </li>
                   </ul>
                 </div>
               </div>
            </div>
            <p className="text-center mt-4">
             <Link to={"/Trade/BTC-USD"} className="btn buttonType2">Start trading</Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
			<Footer />
		</div>
		);
	}
}

export default Leverage
