
/* eslint-disable */

import React, { Component } from 'react'
import '../css/style-trade.css';
// import '../js/main.js';

import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import Logo from "../images/Logo-small.png"
import Selicon1 from "../images/selicon1.png"
import Selicon2 from "../images/selicon2.png"
import Selicon4 from "../images/selicon4.png"
import Selicon5 from "../images/selicon5.png"
import Selicon6 from "../images/selicon6.png"
import Selicon7 from "../images/Icon.png"
import binanceicon from "../images/BNBicon.png"
import dashicon from "../images/DASHicon.png"
import trxicon from "../images/TRXicon.png"
import xmricon from "../images/XMRicon.png"
import silvericon from "../images/silver.jpg"
import * as moment from "moment";

import { getspotPertual, spotorderPlacing, getspotTradeData, getspotuserTradeData, spotcancelTrade, logoutUser, getspotPricevalue, triggerstop, changeopenpositions } from "../actions/authActions";

import GreenPinkDot from "../images/btn-green-pink-dot.png"
import PinkDot from "../images/btn-pink-dot.png"
import GreenDot from "../images/btn-green-dot.png"
import TradeChart from "../images/tradeChart.jpg"
import Slider, { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';

import axios from "axios";
import { Link, withRouter } from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { store } from 'react-notifications-component';
import { Modal, Button } from 'react-bootstrap/';
import Select from 'react-select';
import io from "socket.io-client";
import { Scrollbars } from 'react-custom-scrollbars';
import { TVChartContainer } from './TVChartContainer';
import keys from "../actions/config";

const url = keys.baseUrl;

const customStyles = {
  option: (provided, state) => ({
    ...provided,
    borderBottom: '1px dotted pink',
    color: state.isSelected ? 'red' : 'blue',
    padding: 20,
  }),
  control: () => ({
    // none of react-select's styles are passed to <Control />
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = 'opacity 300ms';

    return { ...provided, opacity, transition };
  }
}

const options1 = [
  { value: 'Beshare', label: 'Beshare' },
  { value: '3Commas', label: '3Commas' },
  { value: 'AIcoin', label: 'AIcoin' },
  { value: 'alpha-algo', label: 'alpha-algo' },
  { value: 'Cornix', label: 'Cornix' },
  { value: '合约帝', label: '合约帝' },
];

const options = [
  { value: 'GoodTillCancelled', label: 'GoodTillCancelled' },
  { value: 'ImmediateOrCancel', label: 'ImmediateOrCancel' },
  { value: 'FillOrKill', label: 'FillOrKill' },
];



class Trade extends Component {

  constructor(props) {

    super(props);
    //console.log('typeof this.props.match.params.currency');
    // console.log(typeof this.props.match.params.currency);
    if (typeof this.props.match.params.currency == 'undefined') {
      var currency = 'BTC-USD';
    }
    else {
      var currency = this.props.match.params.currency;
      localStorage.setItem('curpair1', this.props.match.params.currency)
      console.log(this.props.match.params, 'checkcurrency');
    }
    // alert(currency);
    currency = currency.toUpperCase();

    var date = new Date();
    var curhour = date.getHours();
    var timeuntillfunding = (curhour > 0 && curhour < 8) ? (8 - curhour) : (curhour > 8 && curhour < 16) ? (16 - curhour) : (curhour > 16 && curhour < 24) ? (24 - curhour) : 0;

    this.state = {
      value: (currency == 'BTC-USD') ? 195 : 50,
      dummyleverage: 10,
      curcontract: currency,
      chartpair: currency.replace("-", ""),
      secondcurrency: currency.split('-')[1],
      firstcurrency: currency.split('-')[0],
      chartpair: currency.replace("-", ""),
      price: 7800,
      instantprice: 7800,
      index_price: 7800,
      rescentcount: 50,
      markprice: 0,
      quantity: 0.00000000,
      order_value: 0.00000000,
      order_cost: 0.00000000,
      curarray: ["BTC-USDT", "ETH-USDT", "XRP-USDT", "LTC-USDT", "BCH-USDT", "ETH-BTC", "XRP-BTC", "LTC-BTC", "BCH-BTC", "LTC-ETH", "XRP-ETH", "BCH-ETH", "BTC-BUSD", "ETH-BUSD", "LTC-BUSD", "XRP-BUSD", "BCH-BUSD", "DASH-BTC", "TRX-BTC", "XMR-BTC", "DASH-USDT", "TRX-USDT", "XMR-USDT", "BNB-USDT", "BNB-BTC"
      ],
      floatcurarray: ["BTCUSDT", "ETHUSDT", "LTCUSDT", "BCHUSDT"],
      btcprice: 0,
      quanfloat: (["BTCUSDT", "ETHUSDT", "LTCUSDT", "BCHUSDT"].includes(currency.replace("-", ""))) ? 5 : (currency.replace("-", "") == 'XRPBTC') ? 0 : 3,
      pricefloat: (["BTCUSDT", "ETHUSDT", "LTCUSDT", "BCHUSDT"].includes(currency.replace("-", ""))) ? 4 : (currency.replace("-", "") == 'XRPBTC') ? 8 : 6,
      balance: 0,
      firstbalance: 0,
      secondbalance: 0,
      last_price: 0,
      maxquantity: 0,
      minquantity: 0,
      minmargin: 0,
      close_quantity: 0,
      btcvolume: 0,
      close_price: 0,
      prevoiusprice: 0,
      searchpair: "",
      selllimit: 0,
      buylimit: 0,
      buysumvalue: 0,
      sellsumvalue: 0,
      maxleverage: 0,
      floating: (currency == 'BTC-USDT' || currency == 'ETH-USDT' || currency == 'BTC-BUSD' || currency == 'ETH-BUSD' || currency == 'LTC-BUSD' || currency == 'XRP-BUSD' || currency == 'BCH-BUSD') ? 2 : 8,
      taker_fee: 0.075,
      maker_fee: 0.075,
      disable: true,
      buydisable: true,
      selldisable: true,
      readOnly: false,
      records: [],
      allrecords: [],
      assetOverview: [],
      tradeTableAll: [],
      buyOrder: [],
      prevbuyOrder: [],
      sellOrder: [],
      prevsellOrder: [],
      orderHistory: [],
      Rescentorder: [],
      Histroydetails: [],
      position_details: [],
      allposition_details: [],
      closed_positions: [],
      Conditional_details: [],
      Filleddetails: [],
      lastpricedet: "",
      daily_details: [],
      errors: {},
      spotpricecolor: 'greenText',
      orderbookshow: 'greenpink',
      ordertype: 'Market',
      triggerordertype: 'limit',
      theme: 'Dark',
      limit: false,
      conditional: false,
      reduce_only: false,
      post_only: false,
      show: false,
      tpshow: false,
      show1: false,
      tpreadOnly: true,
      trailreadOnly: true,
      stopreadOnly: true,
      readOnly1: true,
      update: true,
      timeinforcetype: { value: 'GoodTillCancelled', label: 'GoodTillCancelled' },
      perpetual: '',
      username: '',
      message: '',
      trailingstopdistance: 0,
      alertmessage: '',
      TradingView: '',
      messages: [],
      Liqprice: 0,
      takeprofit: 0,
      stopprice: 0,
      stopper: 0,
      tptrigger_type: null,
      stoptrigger_type: null,
      blocktime: '',
      change: 0,
      timeuntillfunding: timeuntillfunding,
      buyorsell: 'buy',
      alertmessage1: '',
      alertmessage2: '',

    };

    this.socket = io(keys.socketUrl, { secure: true, transports: ['polling'], jsonp: false, rejectUnauthorized: false }); //live
    // this.socket = io(keys.socketUrl,{transports:['polling']});

    this.socket.on('RECEIVE_MESSAGE', function (data) {
      console.log(data);
      addMessage(data);
    });

    this.socket.on('SPOTTRADE', function (data) {
      console.log(data, 'spotorderPlacing');

      // if( data.contractdetails !== undefined && data.contractdetails.first_currency !== undefined &&  data.contractdetails.first_currency==this.state.first_currency)
      // {
      appenddata(data);
      // }
    });

    this.socket.on('SPOTUSERTRADE', function (data) {
      getPricevalue();
      console.log(data, 'userddata');
      // let userid = this.props.auth.user.id;
      userappenddata(data);
    });

    this.socket.on('PRICEDETAILS', function (data) {
      console.log(data, 'PRICEDETAILS');
      priceappenddata(data);
    });

    this.socket.on('NOTIFICATION', function (data) {
      store.addNotification({
        title: "Wonderful!",
        message: data,
        type: "success",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 2000,
          onScreen: true
        }
      });
    });

    const appenddata = data => {
      this.setState({ prevbuyOrder: this.state.buyOrder, prevsellOrder: this.state.sellOrder });
      if (data.message == 'tradeTableAll' && data.buyOrder !== undefined && data.sellOrder !== undefined && data.contractdetails !== undefined && data.Rescentorder !== undefined) {
        console.log('insideifffffffff')
        console.log(data.contractdetails.first_currency)
        console.log(this.state.first_currency)
        if (data.contractdetails.first_currency == this.state.firstcurrency) {
          console.log('insideif')
          var assetdetails = data.assetdetails;
          var contractdetails = data.contractdetails;

          var buyOrder = data.buyOrder;
          var sellOrder = data.sellOrder;
          var Rescentorder = data.Rescentorder;
          this.setState({
            buyOrder: buyOrder,
            sellOrder: sellOrder,
            Rescentorder: Rescentorder,
            mainmargin: contractdetails.maint_margin / 100,
          });
        }


      }
    };


    const priceappenddata = data => {
      // if(data.tiker_root=='BTCUSD')
      // {
      //   this.setState({btcprice:parseFloat(data.markprice).toFixed(2)})
      // }

      // if(data && (data.tiker_root==this.state.chartpair("USD") || data.tiker_root==this.state.chartpair))

      if (data && (data.tiker_root == this.state.chartpair.replace("USDT", "USD") || data.tiker_root == this.state.chartpair)) {
        var floating = (this.state.chartpair == ' ' || this.state.chartpair == 'ETHUSD') ? 2 : 8;
        this.setState({ markprice: parseFloat(data.markprice).toFixed(floating), last_price: data.last });
        if (this.state.prevoiusprice != 0 && this.state.prevoiusprice < data.markprice) {
          this.setState({ spotpricecolor: "greenText" });
        }
        else {
          this.setState({ spotpricecolor: "pinkText" });
        }
        this.setState({ prevoiusprice: this.state.markprice, floating: floating });
      }
    }

    const getPricevalue = () => {
      var findObj = {
        firstCurrency: this.state.firstcurrency,
        secondCurrency: this.state.secondcurrency
      };
      this.props.getspotPricevalue(findObj);
    }


    const userappenddata = data => {
      if (data.message == 'tradeTableAll' && data.orderHistory !== undefined && data.assetdetails !== undefined && data.Conditional_details !== undefined) {

        var orderHistory = data.orderHistory;
        var Histroydetails = data.Histroydetails;
        var Filleddetails = data.Filleddetails;
        var lastpricedet = data.lastpricedet;
        var assetdetails = data.assetdetails;
        var Conditional_details = data.Conditional_details;


        this.setState({
          orderHistory: orderHistory,
          Histroydetails: Histroydetails,
          // lastpricedet        : lastpricedet,
          Filleddetails: Filleddetails,
          assetOverview: assetdetails,
          Conditional_details: Conditional_details,
          // instantprice        : (lastpricedet.length?lastpricedet.price:0,
        });
      }
    };


    //get message
    axios
      .get(url + "cryptoapi/chat-data")
      .then(res => {
        if (res.status === 200) {
          // console.log("RESRESRESR");
          // console.log(res.data);
          let filterrecords = [];
          for (var i = 0; i < res.data.length; i++) {
            var chatdata = {};
            chatdata.message = res.data[i].message;
            if (res.data[i].userId != null && res.data[i].userId.name != '') {
              chatdata.author = res.data[i].userId.name;
            } else {
              if (res.data[i].userId != null) {
                var split_name = res.data[i].userId.email.split("@");
                chatdata.author = split_name[0];
              }
            }
            chatdata.moderator = (typeof res.data[i].userId.moderator != 'undefined') ? res.data[i].userId.moderator : 0;
            filterrecords.push(chatdata);
          }
          this.setState({ messages: filterrecords })
          // console.log(this.state.messages);
        }
      })
      .catch();
    //get message

    const addMessage = data => {
      this.setState({ messages: [...this.state.messages, data] });
    };
    const userTradeFun = data => {

      this.setState({ userTrade: [...this.state.userTrade, data] });

    };

    this.sendMessage = ev => {
      ev.preventDefault();
      //save Chat Message to database
      if (this.state.message != '') {
        var chatdata = {};
        chatdata.message = this.state.message;
        chatdata.userId = this.props.auth.user.id;
        axios
          .post(url + "cryptoapi/chat-add", chatdata)
          .then(res => {
            if (res.status === 200) {
              console.log(res.data.message);
            }
          })
          .catch();
        var name = "";
        if (this.props.auth.user.name != "") {
          this.state.username = this.props.auth.user.name;
        } else {
          var split_name = this.props.auth.user.email.split("@");
          this.state.username = split_name[0];
        }
        var fmoderator = (typeof this.props.auth.user.moderator != 'undefined') ? this.props.auth.user.moderator : 0;
        this.socket.emit('SEND_MESSAGE', {
          author: this.state.username,
          message: this.state.message,
          moderator: fmoderator,
        })
        this.setState({ message: '' });
      }
      else {
        store.addNotification({
          title: "Warning!",
          message: "You cannot post empty message",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
      }
    }
    this.sendTradeMessage = ev => {
      ev.preventDefault();
      this.socket.emit('username', this.props.auth.user.id);
      this.socket.emit('SEND_TRADE', {
        toid: this.props.auth.user.id,
        id: this.props.auth.user.id,
        message: this.state.userTradedata
      })
      this.socket.emit('disconnect', {
        id: this.props.auth.user.id,
      })
    }
  }

  toggleMode = e => {

    if (document.body.classList == "") {
      document.body.classList.add('themeLight');
      this.setState({ theme: "White" })
    }
    else {
      document.body.classList.remove('themeLight');
      this.setState({ theme: "Dark" })
    }
  }
  onLogoutClick = e => {
    e.preventDefault();
    store.addNotification({
      title: "Wonderful!",
      message: "Loggedout Successfully",
      type: "success",
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 2000,
        onScreen: true
      }
    });
    this.props.logoutUser();
    window.location.href = "";
  };

  loadmoreRescentorder = () => {
    axios
      .post(url + "cryptoapi/spotloadmoreRescentorder", { pair: this.state.firstcurrency + this.state.secondcurrency, rescentcount: this.state.rescentcount + 20 })
      .then(res => {
        this.setState({ Rescentorder: res.data.data, rescentcount: (this.state.rescentcount + 20) })
        console.log("response", this.state.Rescentorder)

      });
  }
  handleChangeStart = () => {
    console.log('Change event started')
  };

  handleChange = value => {
    this.setState({ dummyleverage: value });
  };


  balanceperc = e => {

    var orderprice = this.state.markprice;
    var balanceperc = e.target.innerHTML.replace("%", "");

    var initial_balance = parseFloat(this.state.balance) * parseFloat(balanceperc) / 100;

    var leveragebal = parseFloat(this.state.value) * parseFloat(this.state.balance);

    var openfee = parseFloat(leveragebal) * parseFloat(this.state.taker_fee) / 100;

    var closefee = parseFloat(openfee) * parseFloat(this.state.value) / parseFloat(this.state.value + 1);

    var btcval = parseFloat(initial_balance) - (parseFloat(openfee) + parseFloat(closefee));
    var quanti = parseFloat(btcval) * parseFloat(orderprice);

    this.setState({ quantity: parseFloat(quanti).toFixed() });
    this.calculation();

  };


  triggerPrice = (param) => {
    var type = param.target.innerHTML;
    // console.log(type,'params');
    var trigger_price = (type == 'Last') ? this.state.last_price : (type == 'Index') ? this.state.index_price : this.state.markprice;
    // console.log(trigger_price);
    this.setState({ trigger_price: trigger_price, trigger_type: type });
  }
  triggertp = (param) => {
    var type = param.target.innerHTML;
    // console.log(type,'params');
    var trigger_price = (type == 'Last') ? this.state.last_price : (type == 'Index') ? this.state.index_price : this.state.markprice;
    // console.log(trigger_price);
    this.setState({ takeprofit: trigger_price, tptrigger_type: type });
  }
  triggerstop = (param) => {
    var type = param.target.innerHTML;
    // console.log(type,'params');
    var trigger_price = (type == 'Last') ? this.state.last_price : (type == 'Index') ? this.state.index_price : this.state.markprice;
    // console.log(trigger_price);
    this.setState({ stopprice: trigger_price, stoptrigger_type: type });
  }

  conditionalPrice = (param) => {
    var type = param.target.innerHTML;
    (type == 'Limit') ? this.setState({ readOnly: false, triggerordertype: type }) : this.setState({ readOnly: true, triggerordertype: type });
  }
  ordertype_changes = (param) => {
    // console.log('fhjsdkjhfskjdhfksdhjf');
    var type = param.target.innerHTML;
    // console.log(type);
    if (type == 'Limit') {
      this.setState({ limit: true, conditional: false, ordertype: type, price: '' });
    }
    else if (type == 'Conditional') {
      this.setState({ conditional: true, ordertype: type, limit: false });
    }
    else {
      this.setState({ limit: false, conditional: false, ordertype: type });
    }
  }
  tpClose = (param) => {
    this.setState({ tpshow: false });
  }
  handleClose1 = (param) => {
    this.setState({ show1: false });
  }


  tpshow = (param) => {
    var type = param.target.innerHTML;
    this.setState({ tpshow: true });
  }

  handleShow = (e) => {
    var type = e.target.innerHTML;
    this.setState({ buyorsell: type.toLowerCase(), price: this.state.markprice, alertmessage: "" });
  }
  handleShow22 = (type) => {
    var curarray = ["BTC-USDT", "ETH-USDT", "XRP-USDT", "LTC-USDT", "BCH-USDT", "ETH-BTC", "XRP-BTC", "LTC-BTC", "BCH-BTC", "LTC-ETH", "XRP-ETH", "BCH-ETH", "BTC-BUSD", "ETH-BUSD", "LTC-BUSD", "XRP-BUSD", "BCH-BUSD", "DASH-BTC", "TRX-BTC", "XMR-BTC", "DASH-USDT", "TRX-USDT", "XMR-USDT", "BNB-USDT", "BNB-BTC"]
    if (curarray.includes(this.state.curcontract)) {
      var orderprice = this.state.ordertype == 'Market' ? this.state.markprice : this.state.price;
    }
    else {
      var orderprice = this.state.ordertype == 'Market' ? (this.state.buyorsell == 'buy') ? this.state.selllimit : this.state.buylimit : this.state.price;
    }
    var balance = (this.state.buyorsell == 'buy') ? this.state.secondbalance : this.state.firstbalance;
    var order_value = (this.state.buyorsell == 'buy') ? this.state.order_value : this.state.quantity;
    if (this.state.ordertype == 'Limit' && this.state.buyorsell == "buy" && parseFloat(this.state.price) > parseFloat(this.state.markprice) && curarray.includes(this.state.curcontract)) {
      this.setState({ alertmessage: "Entry price you set must be lower or equal to " + this.state.markprice })
      return false;
    }
    else if (this.state.ordertype == 'Limit' && this.state.buyorsell == "sell" && parseFloat(this.state.price) < parseFloat(this.state.markprice) && curarray.includes(this.state.curcontract)) {
      this.setState({ alertmessage: "Entry price you set must be higher or equal to " + this.state.markprice })
      return false;
    }
    else {
      this.setState({ alertmessage: "" })
    }
    if (parseFloat(this.state.quantity) < parseFloat(this.state.minquantity)) {
      store.addNotification({
        title: "Wonderful!",
        message: "Quantity must not be lesser than " + this.state.minquantity,
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1500,
          onScreen: true
        }
      });
      return false;
    }
    // if(parseFloat(this.state.quantity) > parseFloat(this.state.maxquantity))
    // {
    //    store.addNotification({
    //       title        : "Wonderful!",
    //       message      : "Quantity must not be higher than "+this.state.maxquantity,
    //       type         : "danger",
    //       insert       : "top",
    //       container    : "top-right",
    //       animationIn  : ["animated", "fadeIn"],
    //       animationOut : ["animated", "fadeOut"],
    //       dismiss      : {
    //         duration     : 1500,
    //         onScreen     : true
    //       }
    //     });
    //    return false;
    // }

    if (isNaN(orderprice)) {
      store.addNotification({
        title: "Wonderful!",
        message: "Price is invalid",
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1500,
          onScreen: true
        }
      });
      return false;
    }

    if (orderprice < 0.00000001) {
      store.addNotification({
        title: "Wonderful!",
        message: "Price of contract must not be lesser than 0.00000001",
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1500,
          onScreen: true
        }
      });
      return false;
    }
    else if (parseFloat(balance) < parseFloat(order_value)) {
      store.addNotification({
        title: "Warning!",
        message: "Due to insuffient balance order cannot be placed",
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 1500,
          onScreen: true
        }
      });
      return false;
    }
    else {
      if (this.state.ordertype == 'Conditional' && this.state.trigger_price < 1) {
        store.addNotification({
          title: "Warning!",
          message: "Trigger Price of contract must not be lesser than 1",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
        return false;
      }



      this.setState({ show: true });
    }
  }

  handleClose = () => {
    this.setState({ show: false });
  }

  orderPlacing = (type) => {
    if (this.state.stopreadOnly == false) {
      if (this.state.buyorsell == 'buy' && parseFloat(this.state.markprice) <= parseFloat(this.state.stopprice)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be lower than " + this.state.markprice });
        return false;
      }
      else if (this.state.buyorsell == 'buy' && parseFloat(this.state.Liqprice) >= parseFloat(this.state.stopprice)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be higher than " + parseFloat(this.state.Liqprice).toFixed(this.state.floating) });
        return false;
      }
      else if (this.state.buyorsell == 'sell' && parseFloat(this.state.markprice) >= parseFloat(this.state.stopprice)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be higher than " + this.state.markprice });
        return false;
      }
      else if (this.state.buyorsell == 'sell' && parseFloat(this.state.Liqprice) <= parseFloat(this.state.stopprice)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be lower than " + parseFloat(this.state.Liqprice).toFixed(this.state.floating) });
        return false;
      }
      else {
        this.setState({ alertmessage1: '' });
      }
    }
    if (this.state.tpreadOnly == false) {
      if (this.state.buyorsell == 'buy' && parseFloat(this.state.markprice) >= parseFloat(this.state.takeprofit)) {
        this.setState({ alertmessage2: "Stop Loss price you set must be higher than " + this.state.markprice });
        return false;
      }
      else if (this.state.buyorsell == 'sell' && parseFloat(this.state.markprice) >= parseFloat(this.state.takeprofit)) {
        this.setState({ alertmessage2: "Stop Loss price you set must be lower than " + this.state.markprice });
        return false;
      }
      else {
        this.setState({ alertmessage2: '' });
      }
    }
    // return false;
    let userid = this.props.auth.user.id;
    var check1 = type.target.className.includes("btnBuy");
    if (this.state.buyorsell == 'buy') {
      this.setState({ buydisable: false, show: false });
    } else {
      this.setState({ selldisable: false, show: false });
    }
    var curarray = ["BTC-USDT", "ETH-USDT", "XRP-USDT", "LTC-USDT", "BCH-USDT", "ETH-BTC", "XRP-BTC", "LTC-BTC", "BCH-BTC", "LTC-ETH", "XRP-ETH", "BCH-ETH", "BTC-BUSD", "ETH-BUSD", "LTC-BUSD", "XRP-BUSD", "BCH-BUSD", "DASH-BTC", "TRX-BTC", "XMR-BTC", "DASH-USDT", "TRX-USDT", "XMR-USDT", "BNB-USDT", "BNB-BTC"]
    // alert(this.state.curcontract)
    // alert(curarray.includes(this.state.curcontract))
    if (curarray.includes(this.state.curcontract)) {
      var orderprice = this.state.ordertype == 'Market' ? this.state.markprice : this.state.price;
    }
    else {
      var orderprice = this.state.ordertype == 'Market' ? (this.state.buyorsell == 'buy') ? this.state.selllimit : this.state.buylimit : this.state.price;
    }

    let orderData = {
      "quantity": this.state.quantity,
      "price": orderprice,
      "userid": userid,
      "pair": this.state.firstcurrency + this.state.secondcurrency,
      "pairname": this.state.firstcurrency + this.state.secondcurrency,
      "firstcurrency": this.state.firstcurrency,
      "secondcurrency": this.state.secondcurrency,
      "timeinforcetype": this.state.timeinforcetype.value,
      "buyorsell": this.state.buyorsell,
      "ordertype": (this.state.ordertype == 'Conditional') ? this.state.triggerordertype : this.state.ordertype,
      "trigger_price": (this.state.ordertype == 'Conditional') ? this.state.trigger_price : 0,
      "trigger_type": (this.state.ordertype == 'Conditional') ? this.state.trigger_type : null,
      "tptrigger_type": this.state.tptrigger_type,
      "stoptrigger_type": this.state.stoptrigger_type,
      "post_only": this.state.post_only,
      "reduce_only": this.state.reduce_only,
      "stopcheck": this.state.stopreadOnly,
      "takeprofitcheck": this.state.tpreadOnly,
      "takeprofit": this.state.takeprofit,
      "stopprice": this.state.stopprice,
    }
    this.setState({ disable: false });
    this.setState({ update: false })
    this.props.spotorderPlacing(orderData);
  };


  orderType = (e) => {
    if (e.target.id == 'nav-tradeLimit-tab') {
      this.setState({ limit: true });
    }
    else {
      this.setState({ limit: false });
    }
  };

  cancelFunction = (e) => {
    if (window.confirm('Are you confirm to cancel this order?.')) {
      let userid = this.props.auth.user.id;
      var iddetails = { id: e.target.id, userid: userid };
      this.props.spotcancelTrade(iddetails);

    }
  };

  upvalue = (field) => {
    if (field == 'quantity') {
      var newval = this.state.quantity + 0.5;
      this.setState({ quantity: newval });
    }
  };

  updownvalue = (one, two, three) => {

    var newval = 0;
    if (one == 'quantity') {
      if (two == 'plus') {
        newval = parseFloat(this.state.quantity ? this.state.quantity : 0) + 1;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.quantity ? this.state.quantity : 0) - 1;
      }
      if (newval >= 0) {
        this.setState({ quantity: newval });
        var orderprice = this.state.ordertype == 'Market' ? this.state.markprice : this.state.price;
        var order_value = parseFloat(newval * orderprice).toFixed(8);
        this.setState({ order_value: parseFloat(order_value).toFixed(8) });
      }
    }

    if (one == 'price') {
      if (two == 'plus') {
        newval = parseFloat(this.state.price ? this.state.price : 0) + 0.5;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.price ? this.state.price : 0) - 0.5;
      }
      if (newval >= 0) {
        this.setState({ price: newval });
      }
    }

    if (one == 'trigger_price') {
      if (two == 'plus') {
        newval = parseFloat(this.state.trigger_price ? this.state.trigger_price : 0) + 0.5;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.trigger_price ? this.state.trigger_price : 0) - 0.5;
      }
      if (newval >= 0) {
        this.setState({ trigger_price: newval });
      }
    }
    if (one == 'takeprofit') {
      if (two == 'plus') {
        newval = parseFloat(this.state.takeprofit ? this.state.takeprofit : 0) + 0.5;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.takeprofit ? this.state.takeprofit : 0) - 0.5;
      }
      if (newval >= 0 && this.state.takeprofitcheck) {
        this.setState({ takeprofit: newval });
      }
    }

    if (one == 'stopprice') {
      if (two == 'plus') {
        newval = parseFloat(this.state.stopprice ? this.state.stopprice : 0) + 0.5;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.stopprice ? this.state.stopprice : 0) - 0.5;
      }
      if (newval >= 0 && this.state.stopcheck) {
        this.setState({ stopprice: newval });
      }
    }
    if (one == 'trailingstopdistance') {
      if (two == 'plus') {
        newval = parseFloat(this.state.trailingstopdistance ? this.state.trailingstopdistance : 0) + 0.5;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.trailingstopdistance ? this.state.trailingstopdistance : 0) - 0.5;
      }
      if (newval >= 0 && this.state.trailcheck) {
        this.setState({ trailingstopdistance: newval });
      }
    }

    if (one == 'close_quantity') {
      if (two == 'plus') {
        newval = parseFloat(this.state.close_quantity ? this.state.close_quantity : 0) + 0.5;
      } else if (two == 'minus') {
        newval = parseFloat(this.state.close_quantity ? this.state.close_quantity : 0) - 0.5;
      }
      if (newval >= 0) {
        this.setState({ close_quantity: newval });
      }
    }

  };

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    if (this.props.auth.user.blocktime != '' && this.props.auth.user.blockhours) {
      var _date1 = new Date(this.props.auth.user.blocktime);
      this.setState({ blocktime: (new Date(_date1.getTime() + 1000 * 60 * 60 * this.props.auth.user.blockhours)) });
    }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
    else {
      this.setState({
        errors: {}
      });
    }
    // console.log(nextProps.auth,'nextProps');
    if (nextProps.auth.trade !== undefined
      && nextProps.auth.trade.data !== undefined
      && nextProps.auth.trade.data.data !== undefined
    ) {
      console.log(nextProps.auth, 'dfdfdff')

      var pair = this.state.curcontract;
      var res = pair.split("_");
      var temp = pair.replace("-", "");
      var index = nextProps.auth.trade.data.data.findIndex(x => (x.tiker_root.toUpperCase()) === temp);
      var btcindex = nextProps.auth.trade.data.data.findIndex(x => (x.tiker_root) === 'BTCUSD');
      if (index != -1) {
        var perparray = nextProps.auth.trade.data.data;
        var indexarr = [1, 0, 2, 3];
        if (perparray.length)
          var outputarr = indexarr.map(i => perparray[i]);
        this.setState({
          records: perparray,
          allrecords: perparray,
          perpetual: nextProps.auth.trade.data.data[0],
          markprice: (nextProps.auth.trade.data.data[index].markprice) ? parseFloat(nextProps.auth.trade.data.data[index].markprice) : (nextProps.auth.trade.data.data[index].last == null) ? 0 : nextProps.auth.trade.data.data[index].last,
          value: parseFloat(nextProps.auth.trade.data.data[index].leverage).toFixed(),
          maxleverage: parseFloat(nextProps.auth.trade.data.data[index].leverage).toFixed(),
          price: parseFloat(nextProps.auth.trade.data.data[index].markprice).toFixed(this.state.floating),
          btcprice: 7800,
          maxquantity: parseFloat(nextProps.auth.trade.data.data[index].maxquantity),
          minquantity: parseFloat(nextProps.auth.trade.data.data[index].minquantity),
          minmargin: parseFloat(nextProps.auth.trade.data.data[index].minmargin),
        });
      }
    }
    // console.log(nextProps.auth.trade,'tradeTableAlltradeTableAll');
    if (nextProps.auth.trade !== undefined && nextProps.auth.trade.data !== undefined && nextProps.auth.trade.data.message == 'tradeTableAll' && nextProps.auth.trade !== undefined
      && nextProps.auth.trade.data !== undefined && nextProps.auth.trade.data.pricedet !== undefined && nextProps.auth.trade.data.pricedet.length > 0) {
      console.log(nextProps.auth.trade.data.pricedet, 'tradeTableAlltradeTableAll');
      var btcvalue = 0;
      if (nextProps.auth.trade.data.pricedet.length > 0) {
        var usdvalue = parseFloat(this.state.markprice) * parseFloat(nextProps.auth.trade.data.pricedet[0].volume);
        var btcvalue = parseFloat(usdvalue) / parseFloat(this.state.btcprice);
      }
      this.setState({
        price: nextProps.auth.trade.data.pricedet[0].last,
        trigger_price: nextProps.auth.trade.data.pricedet[0].last,
        last_price: nextProps.auth.trade.data.pricedet[0].last,
        pricedet: nextProps.auth.trade.data.pricedet,
        btcvolume: isNaN(parseFloat(nextProps.auth.trade.data.pricedet[0].volume)) ? 0 : nextProps.auth.trade.data.pricedet[0].volume,
        change: (nextProps.auth.trade.data.pricedet.length > 0) ? nextProps.auth.trade.data.pricedet[0].change : 0,
        instantprice: (nextProps.auth.trade.data.pricedet.length > 0) ? nextProps.auth.trade.data.pricedet[0].last : 0
      });
    }


    if (nextProps.auth.trade !== undefined
      && nextProps.auth.trade.data !== undefined
      && nextProps.auth.trade.data.message !== undefined) {

      var message = nextProps.auth.trade.data.message;
      //console.log(nextProps.auth.trade,'tradedetails');
      if (message == 'tradeTableAll' && nextProps.auth.trade.data.buyOrder !== undefined && nextProps.auth.trade.data.sellOrder !== undefined && nextProps.auth.trade.data.assetdetails !== undefined && nextProps.auth.trade.data.contractdetails !== undefined && nextProps.auth.trade.data.Rescentorder !== undefined && nextProps.auth.trade.data.sellsumvalue !== undefined && nextProps.auth.trade.data.buysumvalue !== undefined) {
        var assetdetails = nextProps.auth.trade.data.assetdetails;
        var contractdetails = nextProps.auth.trade.data.contractdetails;
        var index = assetdetails.findIndex(x => (x.currencySymbol).toLowerCase() === this.state.firstcurrency.toLowerCase());
        var index1 = assetdetails.findIndex(x => (x.currencySymbol).toLowerCase() === this.state.secondcurrency.toLowerCase());

        var firstbalance = (index != -1) ? parseFloat(assetdetails[index].spotwallet).toFixed(8) : 0;
        var secondbalance = (index1 != -1) ? parseFloat(assetdetails[index1].spotwallet).toFixed(8) : 0;
        var buyOrder = nextProps.auth.trade.data.buyOrder;
        var sellOrder = nextProps.auth.trade.data.sellOrder;
        var Rescentorder = nextProps.auth.trade.data.Rescentorder;
        // console.log("Rescentorder",Rescentorder);

        var buysumvalue = nextProps.auth.trade.data.buysumvalue.length > 0 ? nextProps.auth.trade.data.buysumvalue[0].orderValue : 0;
        var sellsumvalue = nextProps.auth.trade.data.sellsumvalue.length > 0 ? nextProps.auth.trade.data.sellsumvalue[0].quantity : 0;
        var val = (sellOrder.length < 6) ? 0 : ((sellOrder.length >= 6) && (sellOrder.length < 9)) ? 50 : (buyOrder.length < 3) ? 500 : 150
        this.setState({
          buyOrder: buyOrder,
          sellOrder: sellOrder,
          Rescentorder: Rescentorder,
          firstbalance: firstbalance,
          secondbalance: secondbalance,
          buylimit: (buyOrder.length > 0) ? buyOrder[0]._id : 0,
          selllimit: (sellOrder.length > 0) ? sellOrder[sellOrder.length - 1]._id : 0,
          buysumvalue: buysumvalue,
          sellsumvalue: sellsumvalue,
        }, () => {
          // alert(val);
          const { scrollbar } = this.refs;  // scrollbar has access to the internal API
          scrollbar.view.scrollTop = val;
        });





      }

      if (message == 'tradeTableAll' && nextProps.auth.trade.data.orderHistory !== undefined && nextProps.auth.trade.data.assetdetails !== undefined && nextProps.auth.trade.data.Conditional_details !== undefined) {
        console.log('here')
        var orderHistory = nextProps.auth.trade.data.orderHistory;
        var Histroydetails = nextProps.auth.trade.data.Histroydetails;
        var Filleddetails = nextProps.auth.trade.data.Filleddetails;
        var assetdetails = nextProps.auth.trade.data.assetdetails;
        var Conditional_details = nextProps.auth.trade.data.Conditional_details;

        console.log(Conditional_details, 'Conditional_details');
        this.setState({
          orderHistory: orderHistory,
          Histroydetails: Histroydetails,
          Filleddetails: Filleddetails,
          assetOverview: assetdetails,
          Conditional_details: Conditional_details,
        });
      }

      if (nextProps.auth.trade.data.notify_show !== undefined && nextProps.auth.trade.data.notify_show == 'yes' && nextProps.auth.trade.data.status !== undefined) {
        if (nextProps.auth.trade.data.status) {
          var type = 'success';
        } else {
          var type = 'danger';
        }

        store.addNotification({
          title: "Wonderful!",
          message: message,
          type: type,
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 1500,
            onScreen: true
          }
        });
        this.setState({ disable: true, selldisable: true, buydisable: true });
        this.getTradeData();
        this.getuserTradeData();
      }
      nextProps.auth.trade = "";
    }
  }



  pairChange = (e) => {
    let userid = this.props.auth.user.id;

    var pair = e.target.id;
    var res = pair.split("_");
    var temp = pair.replace("_", "");
    var loctemp = pair.replace("_", "-");
    var index = this.state.records.findIndex(x => (x.tiker_root) === temp);
    var mainmargin = (index > -1) ? this.state.records[index].maint_margin : 0.5 / 100;
    //console.log(index)
    this.setState({
      curcontract: e.target.id,
      chartpair: temp,
      secondcurrency: res[1],
      chartpair: temp,
      firstcurrency: res[0],
      price: (index > -1) ? this.state.records[index].mark_price : this.state.price,
      perpetual: this.state.records[index],
      mainmargin: mainmargin,
    });
    localStorage.setItem('curpair1', loctemp);
    window.location.href = "/Spot/" + loctemp;
    // this.getData();
    var findObj = {
      userid: userid,
      firstCurrency: res[0],
      secondCurrency: res[1]
    };
    this.props.getspotTradeData(findObj);
    this.props.getspotuserTradeData(findObj);

    this.props.getspotPricevalue(findObj);

  };

  fillprice = e => {
    this.setState({ price: e.target.innerHTML });
  };

  fillquantity = e => {
    this.setState({ quantity: e.target.innerHTML });
  };
  searchpair = e => {
    this.setState({ [e.target.id]: e.target.value });
    var searcharr = [];
    for (var i = 0; i < this.state.allrecords.length; i++) {
      if (this.state.allrecords[i].tiker_root.indexOf(e.target.value.toUpperCase()) !== -1) {
        searcharr.push(this.state.allrecords[i]);
      }
      if (i == this.state.allrecords.length - 1) {
        this.setState({ records: searcharr })
      }
    }
  }
  onChange = e => {

    this.setState({ [e.target.id]: e.target.value });
    if (e.target.id == 'takeprofit' && e.target.value) {
      if (this.state.buyorsell == 'buy' && parseFloat(this.state.markprice) >= parseFloat(e.target.value)) {
        this.setState({ alertmessage2: "Stop Loss price you set must be higher than " + this.state.markprice });
        return false;
      }
      else if (this.state.buyorsell == 'sell' && parseFloat(this.state.markprice) >= parseFloat(e.target.value)) {
        this.setState({ alertmessage2: "Stop Loss price you set must be lower than " + this.state.markprice });
        return false;
      }
      else {
        this.setState({ alertmessage2: '' });
        return true;
      }
    }
    if (e.target.id == 'trailcheck' && e.target.value) {
      this.setState({ trailreadOnly: (this.state.trailreadOnly) ? false : true });
    }
    if (e.target.id == 'stopcheck' && e.target.value) {
      this.setState({ stopreadOnly: (this.state.stopreadOnly) ? false : true });
    }
    if (e.target.id == 'takeprofitcheck' && e.target.value) {
      this.setState({ tpreadOnly: (this.state.tpreadOnly) ? false : true });
    }
    if (e.target.id == 'stopprice' && e.target.value) {
      if (this.state.buyorsell == 'buy' && parseFloat(this.state.markprice) <= parseFloat(e.target.value)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be lower than " + this.state.markprice });
        return false;
      }
      else if (this.state.buyorsell == 'buy' && parseFloat(this.state.Liqprice) >= parseFloat(e.target.value)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be higher than " + parseFloat(this.state.Liqprice).toFixed(this.state.floating) });
        return false;
      }
      else if (this.state.buyorsell == 'sell' && parseFloat(this.state.markprice) >= parseFloat(e.target.value)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be higher than " + this.state.markprice });
        return false;
      }
      else if (this.state.buyorsell == 'sell' && parseFloat(this.state.Liqprice) <= parseFloat(e.target.value)) {
        this.setState({ alertmessage1: "Stop Loss price you set must be lower than " + parseFloat(this.state.Liqprice).toFixed(this.state.floating) });
        return false;
      }
      else {
        this.setState({ alertmessage1: '' });
        return true;
      }

    }
    if (e.target.id == 'reduce_only' || e.target.id == 'post_only') {
      var checked = e.target.checked;

      if (e.target.id == 'post_only') {
        this.setState({ post_only: checked });
      } else if (e.target.id == 'reduce_only') {
        this.setState({ reduce_only: checked });
      }
    }
    else if (e.target.id == 'quantity' || e.target.id == 'price') {
      var curarray = ["BTC-USDT", "ETH-USDT", "XRP-USDT", "LTC-USDT", "BCH-USDT", "ETH-BTC", "XRP-BTC", "LTC-BTC", "BCH-BTC", "LTC-ETH", "XRP-ETH", "BCH-ETH", "BTC-BUSD", "ETH-BUSD", "LTC-BUSD", "XRP-BUSD", "BCH-BUSD", "DASH-BTC", "TRX-BTC", "XMR-BTC", "DASH-USDT", "TRX-USDT", "XMR-USDT", "BNB-USDT", "BNB-BTC"]
      if (e.target.id == 'quantity') {
        var quantity = e.target.value;
        //var orderprice = this.state.ordertype=='Market'?this.state.markprice:this.state.price;
        if (curarray.includes(this.state.curcontract)) {
          var orderprice = this.state.ordertype == 'Market' ? this.state.markprice : this.state.price;
        }
        else {
          var orderprice = this.state.ordertype == 'Market' ? (this.state.buyorsell == 'buy') ? this.state.selllimit : this.state.buylimit : this.state.price;
        }
      }
      if (e.target.id == 'price') {
        var buyselltype = this.state.buyorsell;
        var quantity = this.state.quantity;
        var orderprice = e.target.value;
        // alert(e.target.value)

        if (this.state.ordertype == 'Limit' && buyselltype == "buy" && curarray.includes(this.state.curcontract) && parseFloat(e.target.value) > parseFloat(this.state.markprice)) {
          this.setState({ alertmessage: "Entry price you set must be lower or equal to " + this.state.markprice })
        }
        else if (this.state.ordertype == 'Limit' && curarray.includes(this.state.curcontract) && buyselltype == "sell" && parseFloat(e.target.value) < parseFloat(this.state.markprice)) {
          this.setState({ alertmessage: "Entry price you set must be higher or equal to " + this.state.markprice })
        }
        else {
          this.setState({ alertmessage: "" })
        }
      }


      var order_value = parseFloat(quantity * orderprice).toFixed(8);


      this.setState({ order_value: order_value });
    }
  };

  changetimeinforce = timeinforcetype => {
    this.setState({ timeinforcetype });
    console.log(`Option selected:`, timeinforcetype);
  };

  orderbookshow = e => {
    this.setState({ orderbookshow: e.target.id })
  };

  calculation = (value = 0) => {
    var orderprice = this.state.ordertype == 'Market' ? this.state.markprice : this.state.price;
    var order_value = parseFloat(this.state.quantity * orderprice).toFixed(8);
    this.setState({ order_value: parseFloat(order_value).toFixed(8) });
  };

  componentDidMount() {

    console.log('component');
    this.getData();
    if (!localStorage.getItem('curpair1')) {
      localStorage.setItem('curpair1', 'BTC-USD')
    }
  };

  getData() {
    this.props.getspotPertual();

    this.getTradeData();
    this.getuserTradeData();

    console.log(this.props.auth.user.id, 'createroom')
    this.socket.emit('CREATEROOM', this.props.auth.user.id);

  }


  getTradeData() {
    let userid = this.props.auth.user.id;
    var findObj = {
      userid: userid,
      firstCurrency: this.state.firstcurrency,
      secondCurrency: this.state.secondcurrency
    };
    this.props.getspotTradeData(findObj);
  }


  getuserTradeData() {
    let userid = this.props.auth.user.id;
    var findObj = {
      userid: userid,
      firstCurrency: this.state.firstcurrency,
      secondCurrency: this.state.secondcurrency
    };
    this.props.getspotuserTradeData(findObj);

    this.props.getspotPricevalue(findObj);
  }


  render() {
    const { user } = this.props.auth;
    const { value, records, assetOverview, timeinforcetype, errors, orderHistory, Histroydetails, Filleddetails, perpetual, Conditional_details } = this.state

    if (Conditional_details.length > 0) {
      var cindex = Conditional_details.findIndex(x => (x.trigger_ordertype) === 'takeprofit');
      var tpprice = (cindex == -1) ? '-' : Conditional_details[cindex].trigger_price;

      var sindex = Conditional_details.findIndex(x => (x.trigger_ordertype) === 'stop');
      var stprice = (sindex == -1) ? '-' : Conditional_details[sindex].trigger_price;
    }


    return (
      <div>
        <div className="container-fluid">
          <nav className="navbar navbar-expand-md fixed-top customNavTrade wow fadeInDown" data-wow-delay=".2s">
            <Link to="/" className="navbar-brand"><img src={Logo} alt="GlobalCryptoX" /></Link>
            <div className="header-overview">
              <div className="selectCoinType">
                <div className="btn-group">
                  <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {localStorage.getItem('curpair1')}
                  </button>
                  <ul className="dropdown-menu">
                    <Scrollbars style={{ width: '100%', height: 400 }} renderTrackVertical={props => <div className="track-vertical" />}>
                      <li>
                        <div className="form-group mt-1 mb-2 mx-2">
                          <input type="text" placeholder="Search Pair" id="searchpair" value={this.state.searchpair} onChange={this.searchpair} className="form-control" />
                        </div>
                      </li>

                      {/*}  <li className="dropdown-header"><h6>ETH Market</h6>
                            <ul>
                            { records.map((item,i)=>{
                            if (item.first_currency == 'BTC') {
                            var icon = Selicon1;
                            } else if(item.first_currency == 'XRP') {
                            var icon = Selicon4;
                            }else if(item.first_currency == 'LTC') {
                            var icon = Selicon5;
                            }else if(item.first_currency == 'BCH') {
                            var icon = Selicon6;
                            }else if(item.first_currency == 'ETH') {
                            var icon = Selicon2;
                            }
                            else if(item.first_currency == '💲PC') {
                            var icon = Selicon7;
                            }
                            else if(item.first_currency=="BNB"){
                              var icon =binanceicon
                            }
                            else if(item.first_currency=="XMR"){
                              var icon =xmricon
                            }
                            else if(item.first_currency=="TRX"){
                              var icon =trxicon
                            }
                            else if(item.first_currency=="DASH"){
                              var icon =dashicon
                            }
                            if(item.second_currency == 'ETH')
                            return   <li className={(this.state.firstcurrency==item.first_currency && this.state.secondcurrency==item.second_currency)?"active":""} id={item.first_currency+"_"+item.second_currency} onClick={this.pairChange.bind(this)}><img width="20" src={icon} />{item.first_currency+item.second_currency}</li>
                            })

                            }
                            </ul>
                    </li>*/}
                      <li className="dropdown-header"><h6>USD Market</h6>
                        <ul>
                          {records.map((item, i) => {
                            if (item.first_currency == 'BTC') {
                              var icon = Selicon1;
                            } else if (item.first_currency == 'XRP') {
                              var icon = Selicon4;
                            } else if (item.first_currency == 'LTC') {
                              var icon = Selicon5;
                            } else if (item.first_currency == 'BCH') {
                              var icon = Selicon6;
                            } else if (item.first_currency == 'ETH') {
                              var icon = Selicon2;
                            }
                            else if (item.first_currency == '💲PC') {
                              var icon = Selicon7;
                            }
                            else if (item.first_currency == "SCN") {
                              var icon = silvericon
                            }
                            else if (item.first_currency == "BNB") {
                              var icon = binanceicon
                            }
                            else if (item.first_currency == "XMR") {
                              var icon = xmricon
                            }
                            else if (item.first_currency == "TRX") {
                              var icon = trxicon
                            }
                            else if (item.first_currency == "DASH") {
                              var icon = dashicon
                            }

                            if (item.second_currency == 'USD')
                              return (
                                <li
                                  className={(this.state.firstcurrency == item.first_currency && this.state.secondcurrency == item.second_currency) ? "active" : ""}
                                  id={item.first_currency + "_" + item.second_currency}
                                  onClick={this.pairChange.bind(this)}
                                >
                                  <img width="20" src={icon} />{item.first_currency + item.second_currency}
                                </li>
                              )
                          })

                          }
                        </ul>
                      </li>

                      {
                        (records.findIndex(x => (x.second_currency) === 'BTC') != -1) ?
                          <li className="dropdown-header"><h6>BTC Market</h6>
                            <ul>
                              {records.map((item, i) => {
                                if (item.first_currency == 'BTC') {
                                  var icon = Selicon1;
                                } else if (item.first_currency == 'XRP') {
                                  var icon = Selicon4;
                                } else if (item.first_currency == 'LTC') {
                                  var icon = Selicon5;
                                } else if (item.first_currency == 'BCH') {
                                  var icon = Selicon6;
                                } else if (item.first_currency == 'ETH') {
                                  var icon = Selicon2;
                                }
                                else if (item.first_currency == '💲PC') {
                                  var icon = Selicon7;
                                }
                                else if (item.first_currency == "SCN") {
                                  var icon = silvericon
                                } else if (item.first_currency == "BNB") {
                                  var icon = binanceicon
                                }
                                else if (item.first_currency == "XMR") {
                                  var icon = xmricon
                                }
                                else if (item.first_currency == "TRX") {
                                  var icon = trxicon
                                }
                                else if (item.first_currency == "DASH") {
                                  var icon = dashicon
                                }
                                if (item.second_currency == 'BTC')
                                  return <li className={(this.state.firstcurrency == item.first_currency && this.state.secondcurrency == item.second_currency) ? "active" : ""} id={item.first_currency + "_" + item.second_currency} onClick={this.pairChange.bind(this)}><img width="20" src={icon} />{item.first_currency + item.second_currency}</li>
                              })

                              }
                            </ul>
                          </li> : ""
                      }

                      {
                        (records.findIndex(x => (x.second_currency) === 'BUSD') != -1) ?
                          <li className="dropdown-header"><h6>BUSD Market</h6>
                            <ul>
                              {records.map((item, i) => {
                                if (item.first_currency == 'BTC') {
                                  var icon = Selicon1;
                                } else if (item.first_currency == 'XRP') {
                                  var icon = Selicon4;
                                } else if (item.first_currency == 'LTC') {
                                  var icon = Selicon5;
                                } else if (item.first_currency == 'BCH') {
                                  var icon = Selicon6;
                                } else if (item.first_currency == 'ETH') {
                                  var icon = Selicon2;
                                }
                                else if (item.first_currency == '💲PC') {
                                  var icon = Selicon7;
                                }
                                else if (item.first_currency == "SCN") {
                                  var icon = silvericon
                                }
                                else if (item.first_currency == "BNB") {
                                  var icon = binanceicon
                                }
                                else if (item.first_currency == "XMR") {
                                  var icon = xmricon
                                }
                                else if (item.first_currency == "TRX") {
                                  var icon = trxicon
                                }
                                else if (item.first_currency == "DASH") {
                                  var icon = dashicon
                                }
                                if (item.second_currency == 'BUSD')
                                  return <li className={(this.state.firstcurrency == item.first_currency && this.state.secondcurrency == item.second_currency) ? "active" : ""} id={item.first_currency + "_" + item.second_currency} onClick={this.pairChange.bind(this)}><img width="20" src={icon} />{item.first_currency + item.second_currency}</li>
                              })

                              }
                            </ul>
                          </li> : ""
                      }
                    </Scrollbars>

                  </ul>
                </div>
              </div>
              <div className="headerOverviewGroup">

                <div className="headerOverviewStatus">
                  <h5><small>Spot Price</small><span className={this.state.spotpricecolor}>{((typeof this.state.markprice != 'undefined') ? parseFloat(this.state.markprice).toFixed(this.state.floating) : 0) + ' '}</span></h5>
                </div>
                <div className="headerOverviewStatus">
                  <h5><small>Last Price</small>{((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length > 0) ? parseFloat(this.state.pricedet[0].close).toFixed(this.state.floating) : 0) + ' '}</h5>
                </div>
                <div className="headerOverviewStatus">
                  {
                    (this.state.change < 0) ?
                      <h5 className="pinkText"><small>24H Change</small>{((this.state.change != '') ? parseFloat(this.state.change).toFixed(2) : 0)}%</h5> :
                      <h5 className="greenText"><small>24H Change</small>{((this.state.change != '') ? parseFloat(this.state.change).toFixed(2) : 0)}%</h5>

                  }
                </div>
                <div className="headerOverviewStatus">
                  <h5><small>24H High</small>{((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length > 0) ? parseFloat(this.state.pricedet[0].high).toFixed(this.state.floating) : 0) + ' '}</h5>
                </div>
                <div className="headerOverviewStatus">
                  <h5><small>24H Low</small>{((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length > 0) ? parseFloat(this.state.pricedet[0].low).toFixed(this.state.floating) : 0) + ' '}</h5>
                </div>
                {/* <div className="headerOverviewStatus">
               <h5><small>24H Turnover</small>{ (typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(Math.abs(this.state.btcvolume)).toFixed(3):0 } {this.state.firstcurrency}</h5>
              </div> */}
                <div className="headerOverviewStatus">
                  <h5><small>24H Turnover</small>{(typeof this.state.pricedet != 'undefined' && this.state.pricedet.length > 0) ? parseFloat(Math.abs(this.state.pricedet[0].secvolume)).toFixed(3) : 0} {this.state.secondcurrency}</h5>
                </div>
              </div>
            </div>

            <div className="collapse navbar-collapse" id="navbarResponsive">
              {(typeof user.email != 'undefined') ?
                <ul className="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link themeChangeLink" onClick={this.toggleMode}><span class="bg-sunIcon iconNav"></span></a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#"><span className="bg-mobileIcon iconNav"></span></a>
                  </li>
                  {/*<li className="nav-item d-none d-md-block">
                <a className="nav-link" href="#"><span className="bg-bellIcon iconNav"></span></a>
              </li>*/}

                  <li className="nav-item">
                    <Link to={"/Spot/" + localStorage.getItem('curpair1')} className="dropdown-item nav-link">Trade</Link>
                  </li>
                  {/* <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Trade
                  </a>
                  <div className="dropdown-menu sm-menu">
                   <Link to={"/Spot/"+localStorage.getItem('curpair1')} className="dropdown-item nav-link">Spot</Link>
                  <Link to={"/Trade/"+localStorage.getItem('curpair')} className="dropdown-item nav-link">Derivatives</Link> 
                  </div>
              </li>*/}
                  <li className="nav-item dropdown dmenu mr-2">
                    <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                      Finance
                  </a>
                    <div className="dropdown-menu sm-menu">
                      <Link to="/Locker" className="dropdown-item">Finance</Link>
                    </div>
                  </li>


                  <li className="nav-item">
                    <Link to="/MyAssets" className="nav-link">Wallets</Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/AssetsHistory" className="nav-link">History</Link>
                  </li>
                  <li className="nav-item dropdown dmenu">
                    <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                      {(typeof user.email != 'undefined') ? (user.email.split('@')[0].length < 8) ? user.email.split('@')[0] : user.email.split('@')[0].substring(0, 7) : ''}
                    </a>
                    <div className="dropdown-menu sm-menu">
                      <Link to="/Settings" className="dropdown-item">Settings</Link>
                      <Link to="/Bonus" className="dropdown-item">Bonus</Link>
                      {/* <Link  to="/Leverage" className="dropdown-item">Leverage</Link> */}
                      <Link to="/Referral_Program" className="dropdown-item">Referral Program</Link>

                      <a className="dropdown-item" onClick={this.onLogoutClick} href="#">Logout</a>
                    </div>
                  </li>
                </ul> :
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link to="/Login" className="nav-link">Login</Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/Register" className="nav-link">Register</Link>
                  </li>
                </ul>
              }
            </div>
          </nav>
          <section className="tradeMain">
            <div className="row">
              <div className="col-9">
                <div className="row">

                  <div className="col-md-8 pr-xl-0">
                    <div className="chartTradeSection">

                      <TVChartContainer className={'chartcontainer'} theme={this.state.theme} pair={this.state.curarray.includes(this.state.curcontract) ? this.state.chartpair.replace('USDT', 'USD') : this.state.chartpair} pairdatachange={this.pairdatachange} />

                    </div>
                  </div>
                  <div className="col-md-4 pr-md-0">
                    <div className="tradeTableLeftSide darkBox orderBook">
                      <div className="tableHead">
                        <h4>Order Book</h4>
                        <div className="inputGroup">
                          <button className="btn"><img src={GreenPinkDot} id="greenpink" onClick={this.orderbookshow.bind(this)} /></button>
                          <button className="btn"><img src={PinkDot} id="pink" onClick={this.orderbookshow.bind(this)} /></button>
                          <button className="btn"><img src={GreenDot} id="green" onClick={this.orderbookshow.bind(this)} /></button>
                        </div>
                      </div>
                      <div class="tradeInfoText">
                        <small className="pinkText"> Total {this.state.firstcurrency + " " + parseFloat(this.state.sellsumvalue).toFixed(4)}</small>
                        <small className="greenText"> Total {this.state.secondcurrency + " " + parseFloat(this.state.buysumvalue).toFixed((this.state.secondcurrency != "USDT") ? 4 : 0)}</small>
                      </div>
                      <div className="tradeTableTitle row mx-auto">
                        <span className="col-4">Price</span>
                        <span className="col-4">Qty</span>
                        <span className="col-4">Total</span>
                      </div>
                      <Scrollbars ref='scrollbar' style={{ width: '100%', height: 200 }} renderTrackVertical={props => <div className="track-vertical" />}>
                        <div>
                          {
                            (this.state.orderbookshow == 'greenpink' || this.state.orderbookshow == 'pink') ?
                              this.state.sellOrder.map((item, i) => {
                                var toam = this.state.sellOrder[0].total;
                                var precentage = item.total / toam * 100;
                                var classs = (i % 2) ? 'tradeTableBodyRow odd row mx-auto' : 'tradeTableBodyRow even row mx-auto';
                                var ordvalue = parseFloat(item.quantity) / parseFloat(item._id);
                                var index = (this.state.prevsellOrder).findIndex(x => (x._id) === item._id);
                                index = (this.state.prevsellOrder.length > 0) ? index : -2;
                                var prevquantity = (this.state.prevsellOrder.length > 0 && index != -1) ? this.state.prevsellOrder[index].quantity : 0;
                                var quanfloat = (this.state.floatcurarray.includes(this.state.chartpair)) ? 5 : (this.state.chartpair == 'XRPBTC') ? 0 : 3;
                                var pricefloat = (this.state.floatcurarray.includes(this.state.chartpair)) ? 4 : (this.state.chartpair == 'XRPBTC') ? 8 : 6;
                                var highLightcls = (index == -1) ? "highlight" : (prevquantity != 0 && prevquantity != item.quantity) ? "highlight" : '';
                                return <div className={classs + ' ' + highLightcls} >
                                  <span className="col-4 pinkText" onClick={this.fillprice.bind(this)}>{parseFloat(item._id).toFixed(this.state.pricefloat)}</span>
                                  <span className="col-4" onClick={this.fillquantity.bind(this)}>{parseFloat(Math.abs(item.quantity)).toFixed(this.state.quanfloat)}</span>
                                  <span className="col-4"> <span class="bgProgress1" style={{ width: precentage + '%' }}></span> <span className="bgProgressText1">{parseFloat(Math.abs(item.total)).toFixed(5)}</span></span>
                                </div>;
                              })
                              : ''
                          }



                          {
                            (typeof this.state.pricedet != 'undefined' && this.state.pricedet.length > 0) ? (

                              <div className="tradeTableBodyRow even highLight row mx-auto" >
                                <span className={(this.state.pricedet[0].close) ? ('col-6 pl-4 pinkText') : ('col-6 pl-5 yellowText')}><i className="fas fa-caret-down "></i>{parseFloat(this.state.pricedet[0].close).toFixed(this.state.pricefloat)} {(this.state.secondcurrency == 'USDT') ? '$' : this.state.secondcurrency}</span>
                                <span className={this.state.spotpricecolor + ' col-6 pl-4 text-center'} data-toggle="tooltip" data-placement="right" data-html="true" title="Spot price">{parseFloat(this.state.markprice).toFixed(this.state.pricefloat)} {(this.state.secondcurrency == 'USDT') ? '$' : this.state.secondcurrency}</span>

                              </div>
                            ) : ('')}


                          {
                            (this.state.orderbookshow == 'greenpink' || this.state.orderbookshow == 'green') ?
                              this.state.buyOrder.map((item, i) => {
                                var lastindex = this.state.buyOrder.length;
                                var toam = this.state.buyOrder[lastindex - 1].total;
                                var precentage = item.total / toam * 100;
                                var classs = (i % 2) ? 'tradeTableBodyRow even row mx-auto' : 'tradeTableBodyRow odd row mx-auto';
                                var ordvalue = parseFloat(item.quantity) / parseFloat(item._id);
                                var index = (this.state.prevbuyOrder).findIndex(x => (x._id) === item._id);
                                var quanfloat = (this.state.floatcurarray.includes(this.state.chartpair)) ? 5 : (this.state.chartpair == 'XRPBTC') ? 0 : 3;
                                var pricefloat = (this.state.floatcurarray.includes(this.state.chartpair)) ? 4 : (this.state.chartpair == 'XRPBTC') ? 8 : 6;
                                index = (this.state.prevbuyOrder.length > 0) ? index : -2;
                                var prevquantity = (this.state.prevbuyOrder.length > 0 && index != -1) ? this.state.prevbuyOrder[index].quantity : 0;
                                var highLightcls = (index == -1) ? "highlight" : (prevquantity != 0 && prevquantity != item.quantity) ? "highlight" : '';

                                return <div className={classs + ' ' + highLightcls}>
                                  <span className=" col-4 greenText" onClick={this.fillprice.bind(this)}>{parseFloat(item._id).toFixed(this.state.pricefloat)}</span>
                                  <span className="col-4" onClick={this.fillquantity.bind(this)}>{parseFloat(item.quantity).toFixed(this.state.quanfloat)}</span>
                                  <span className="col-4"> <span class="bgProgress" style={{ width: precentage + '%' }}></span> <span className="bgProgressText">{parseFloat(item.total).toFixed(5)}</span></span>

                                </div>;
                              })
                              : ''
                          }
                        </div>
                      </Scrollbars>
                    </div>
                    <div className="tradeTableLeftSide darkBox recentTrades">
                      <div className="tableHead">
                        <h4>Recent Trades</h4>
                      </div>
                      <div className="tradeTableTitle row mx-auto">
                        <span className="col-3">Type</span>
                        <span className="col-3">Price</span>
                        <span className="col-3">Qty</span>
                        <span className="col-3">Time</span>
                      </div>
                      <Scrollbars style={{ width: '100%', height: 200 }}>
                        <div className="tradeTableBody customScroll">

                          {
                            this.state.Rescentorder.map((item, i) => {
                              var data = new Date(item.created_at);
                              let date1 = data.getHours() + ':' + data.getMinutes() + ':' + data.getSeconds();
                              var classs = (i % 2) ? 'tradeTableBodyRow even row mx-auto' : 'tradeTableBodyRow odd row mx-auto';
                              var typeclass = item.Type == "buy" ? " col-3 greenText" : " col-3 pinkText"
                              var quanfloat = (this.state.floatcurarray.includes(this.state.chartpair)) ? 5 : (this.state.chartpair == 'XRPBTC') ? 0 : 3;
                              var pricefloat = (this.state.floatcurarray.includes(this.state.chartpair)) ? 4 : (this.state.chartpair == 'XRPBTC') ? 8 : 6;
                              var priceclass = pricefloat == 8 ? "col-4" : "col-3";
                              var quantclass = pricefloat == 8 ? "col-2" : "col-3";
                              return <div className={classs} data-toggle="tooltip">
                                <span className={typeclass} >
                                  {item.Type == "buy" ? "Buy" : item.Type == "sell" ? "Sell" : ""}

                                </span>
                                <span className={priceclass}>{parseFloat(item.Price).toFixed(this.state.pricefloat)}</span>
                                <span className={quantclass}>{parseFloat(Math.abs(item.filledAmount)).toFixed(this.state.quanfloat)}</span>
                                <span className="col-3">
                                  {/* {date1}
                          // {moment(item.created_at).format(
                          //           "DD/MM/YYYY h:mm a"
                          //         )} */}

                                  {moment(item.created_at).format(
                                    "k:mm "
                                  )}
                                </span>

                              </div>;

                            })
                          }
                          {(this.state.Rescentorder.length > 15) ?
                            <div class="tradeTableBodyRow odd row mx-auto ">
                              <a onClick={this.loadmoreRescentorder} class="d-block text-center mx-auto"><i class="fas fa-sort-amount-down-alt"></i> Load More</a></div> : ''}

                        </div>

                      </Scrollbars>
                    </div>
                  </div>
                  <div className="col-md-12 pr-xl-0">


                    <div className="darkBox tradeFulltabbedTable">
                      <nav>
                        <div className="nav nav-tabs" id="nav-tab" role="tablist">

                          <a className="nav-item nav-link active show" id="nav-active0-tab" data-toggle="tab" href="#nav-active0" role="tab" aria-controls="nav-active0" aria-selected="false">Pending {orderHistory.length}</a>

                          <a className="nav-item nav-link" id="nav-filledOrder-tab" data-toggle="tab" href="#nav-filledOrder" role="tab" aria-controls="nav-filledOrder" aria-selected="false">Filled Order</a>
                          <a className="nav-item nav-link" id="nav-tradeHistory-tab" data-toggle="tab" href="#nav-tradeHistory" role="tab" aria-controls="nav-tradeHistory" aria-selected="false">History</a>
                        </div>
                      </nav>
                      <div className="tab-content px-xl-3" id="nav-tabContent">

                        <div className="tab-pane fade active show" id="nav-active0" role="tabpanel" aria-labelledby="nav-active0-tab">
                          <div className="table-responsive">
                            <Scrollbars style={{ width: '100%', height: 200 }} renderTrackVertical={props => <div className="track-vertical" />}>
                              <table id="active0Table" className="table table-striped">
                                <thead>
                                  <tr>
                                    <th>Contracts</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Filled Remaining</th>
                                    <th>Order Value</th>
                                    <th>Tp/SL</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Order#</th>
                                    <th>Order Time</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {
                                    orderHistory.map((item, i) => {
                                      var pairName = item.pairName ? item.pairName : '';
                                      var quantity = item.quantity ? item.quantity : 0;
                                      var price = item.quantity ? item.price : 0;
                                      var filledAmount = item.filledAmount ? item.filledAmount : 0;
                                      var orderValue = item.orderValue ? item.orderValue : 0;
                                      var orderType = item.orderType ? item.orderType : 0;
                                      var orderDate = item.orderDate ? item.orderDate : 0;
                                      var classs = (quantity > 0) ? 'greenText' : 'pinkText';
                                      var _id = item._id ? item._id : 0;
                                      var status = item.status == '0' ? 'New' : 'Partial';
                                      var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                      // console.log(orderDate,'orderDate');
                                      var data1 = new Date(orderDate);
                                      let date12 = data1.getFullYear() + '-' + (data1.getMonth() + 1) + '-' + data1.getDate() + ' ' + data1.getHours() + ':' + data1.getMinutes() + ':' + data1.getSeconds();


                                      // console.log(date12,'datezxc');
                                      return <tr>
                                        <td className="yellowText">{pairName}</td>
                                        <td><span className={classs}>{parseFloat(quantity).toFixed(8)}</span></td>
                                        <td>{parseFloat(price).toFixed(this.state.floating)}</td>
                                        <td>{parseFloat(filledAmount).toFixed(8) + '/' + parseFloat(Remaining).toFixed(8)}</td>
                                        <td>{parseFloat(orderValue).toFixed(8)}</td>
                                        <td>{'-/-'}</td>
                                        <td>{orderType}</td>
                                        <td>{status}</td>
                                        <td>{_id}</td>
                                        <td>{date12}</td>
                                        <td>
                                          <button className="btn btn-borderButton mr-1" onClick={this.cancelFunction.bind(this)} id={_id}> Cancel </button>
                                        </td>
                                      </tr>
                                    })
                                  }
                                </tbody>
                              </table>
                            </Scrollbars>
                          </div>
                        </div>
                        <div className="tab-pane fade" id="nav-conditional0" role="tabpanel" aria-labelledby="nav-conditional0-tab">
                          <div className="table-responsive">
                            <Scrollbars style={{ width: '100%', height: 200 }} renderTrackVertical={props => <div className="track-vertical" />}>
                              <table id="conditional0Table" className="table table-striped">
                                <thead>
                                  <tr>
                                    <th>Contracts</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Trigger Price</th>
                                    <th>Order Value</th>
                                    <th>Price (Distance)</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Order#</th>
                                    <th>Order Time</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {
                                    Conditional_details.map((item, i) => {
                                      var pairName = item.pairName ? item.pairName : '';
                                      var quantity = item.quantity ? item.quantity : 0;
                                      var price = item.quantity ? item.price : 0;
                                      var filledAmount = item.filledAmount ? item.filledAmount : 0;
                                      var orderValue = item.orderValue ? item.orderValue : 0;
                                      var orderType = item.orderType ? item.orderType : 0;
                                      var trigger_price = item.orderType ? item.trigger_price : 0;
                                      var trigger_type = item.orderType ? item.trigger_type : 0;
                                      var orderDate = item.orderDate ? item.orderDate : 0;
                                      var classs = (quantity > 0) ? 'greenText' : 'pinkText';
                                      var _id = item._id ? item._id : 0;
                                      var status = item.status == '0' ? 'New' : 'Partial';
                                      var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                      var price_distance = parseFloat(this.state.last_price) - parseFloat(price);
                                      // console.log(orderDate,'orderDate');
                                      var data1 = new Date(orderDate);
                                      let date12 = data1.getFullYear() + '-' + (data1.getMonth() + 1) + '-' + data1.getDate() + ' ' + data1.getHours() + ':' + data1.getMinutes() + ':' + data1.getSeconds();
                                      // console.log(date12,'datezxc');
                                      return <tr>
                                        <td className="yellowText">{pairName}</td>
                                        <td><span className={classs}>{parseFloat(quantity).toFixed(8)}</span></td>
                                        <td>{parseFloat(price).toFixed(this.state.floating)}</td>
                                        <td>{(price_distance > 0) ? '<=' + trigger_price + ' (' + trigger_type + ')' : '>=' + trigger_price + ' (' + trigger_type + ')'}</td>
                                        <td>{parseFloat(orderValue).toFixed(8)}</td>
                                        <td>{this.state.last_price + '(' + price_distance + ')'}</td>
                                        <td>{orderType}</td>
                                        <td>{status}</td>
                                        <td>{_id}</td>
                                        <td>{date12}</td>
                                        <td><button className="btn btn-borderButton mr-1" onClick={this.cancelFunction.bind(this)} id={_id}> Cancel </button></td>
                                      </tr>
                                    })
                                  }
                                </tbody>
                              </table>
                            </Scrollbars>
                          </div>
                        </div>
                        <div className="tab-pane fade" id="nav-filledOrder" role="tabpanel" aria-labelledby="nav-filledOrder-tab">
                          <div className="table-responsive">
                            <Scrollbars style={{ width: '100%', height: 200 }} renderTrackVertical={props => <div className="track-vertical" />}>
                              <table id="filledOrderTable" className="table table-striped rembordor">
                                <thead>
                                  <tr>
                                    <th>Contracts</th>
                                    <th>Quantity</th>
                                    <th>Filled Remaining</th>
                                    <th>Exec Price</th>
                                    <th>Price</th>
                                    <th>Type</th>
                                    <th>Order#</th>
                                    <th>Time</th>
                                  </tr>
                                </thead>

                                {
                                  Filleddetails.map((item, i) => {
                                    // console.log(item,'itemsss');
                                    var quantity = item.quantity ? item.quantity : 0;
                                    var price = item.price ? item.price : 0;
                                    var _id = item._id ? item._id : 0;
                                    var classs = (quantity > 0) ? 'greenText' : 'pinkText';
                                    var filledAmount = item.filledAmount ? item.filledAmount : 0;
                                    var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                    return (<tbody>
                                      { item.filled.map((itemdata, i) => {
                                        var created_at = itemdata.created_at ? itemdata.created_at : '';
                                        var data1 = new Date(created_at);
                                        let date12 = data1.getFullYear() + '-' + (data1.getMonth() + 1) + '-' + data1.getDate() + ' ' + data1.getHours() + ':' + data1.getMinutes() + ':' + data1.getSeconds();
                                        return (<tr>
                                          <td className="yellowText">{itemdata.pairname}</td>
                                          <td>{parseFloat(quantity).toFixed(8)}</td>
                                          <td>{parseFloat(filledAmount).toFixed(8) + '/' + parseFloat(Remaining).toFixed(8)}</td>
                                          <td>{parseFloat(itemdata.Price).toFixed(this.state.floating)}</td>
                                          <td>{parseFloat(price).toFixed(this.state.floating)}</td>
                                          <td>{itemdata.Type}</td>
                                          <td>{_id}</td>
                                          <td>{date12}</td>
                                        </tr>
                                        );
                                      })
                                      }
                                    </tbody>);
                                  })
                                }



                              </table>
                            </Scrollbars>
                          </div>
                        </div>
                        <div className="tab-pane fade" id="nav-tradeHistory" role="tabpanel" aria-labelledby="nav-tradeHistory-tab">
                          <div className="table-responsive">
                            <Scrollbars style={{ width: '100%', height: 200 }} renderTrackVertical={props => <div className="track-vertical" />}>
                              <table id="tradeHistoryTable" className="table table-striped">
                                <thead>
                                  <tr>
                                    <th>Contracts</th>
                                    <th>Quantity</th>
                                    <th>Filled Remaining</th>
                                    <th>Exec Price</th>
                                    <th>Price</th>
                                    <th>Trigger Price</th>
                                    <th>Order Value</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Order#</th>
                                    <th>Order Time</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {
                                    Histroydetails.map((item, i) => {
                                      var pairName = item.pairName ? item.pairName : '';
                                      var quantity = item.quantity ? item.quantity : 0;
                                      var filledAmount = item.filledAmount ? item.filledAmount : 0;
                                      var e_price = item.filled.length > 0 ? item.filled[0].Price : 0;
                                      var price = item.quantity ? item.price : 0;
                                      var orderValue = item.orderValue ? item.orderValue : 0;
                                      var orderType = item.orderType ? item.orderType : 0;
                                      var orderDate = item.orderDate ? item.orderDate : 0;
                                      var classs = item.buyorsell == 'buy' ? 'greenText' : 'pinkText';
                                      var _id = item._id ? item._id : 0;
                                      var status1 = item.status;
                                      var Remaining = parseFloat(quantity) - parseFloat(filledAmount);
                                      var data = new Date(orderDate);
                                      let date1 = data.getFullYear() + '-' + (data.getMonth() + 1) + '-' + data.getDate() + ' ' + data.getHours() + ':' + data.getMinutes() + ':' + data.getSeconds();
                                      return <tr>
                                        <td>{pairName}</td>
                                        <td><span className={classs}>{parseFloat(quantity).toFixed(8)}</span></td>
                                        <td>{parseFloat(filledAmount).toFixed(8) + '/' + parseFloat(Remaining).toFixed(8)}</td>
                                        <td>{e_price}</td>
                                        <td>{price}</td>
                                        <td>{'-/-'}</td>
                                        <td>{orderValue}</td>
                                        <td>{orderType}</td>
                                        {(status1 == 0) ?
                                          <td >New</td>
                                          : ''}
                                        {(status1 == 1) ?
                                          <td >Completed</td>
                                          : ''}
                                        {(status1 == 2) ?
                                          <td >Partial</td>
                                          : ''}
                                        {(status1 == 3) ?
                                          <td >Cancel</td>
                                          : ''}
                                        {(status1 == 4) ?
                                          <td >Conditional</td>
                                          : ''}
                                        <td>{_id}</td>
                                        <td>{date1}</td>
                                      </tr>
                                    })
                                  }
                                </tbody>

                              </table>
                            </Scrollbars>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
              <div className="col-3">


                <div className="darkBox tradeLimitMarket">

                  <nav>
                    <div className="nav nav-tabs" id="nav-tab" role="tablist">
                      <a className="nav-item nav-link active" id="nav-tradeLimit-tab" data-toggle="tab" href="#nav-tradeLimit" role="tab" aria-controls="nav-tradeLimit" aria-selected="true" onClick={this.ordertype_changes.bind(this)}>Market</a>

                      <a className="nav-item nav-link " id="nav-tradeLimit-tab" data-toggle="tab" href="#nav-tradeLimit" role="tab" aria-controls="nav-tradeLimit" aria-selected="true" onClick={this.ordertype_changes.bind(this)}>Limit</a>


                    </div>
                  </nav>
                  <div className="tab-content" id="nav-tabContent-tradeLimitMarket">
                    <div className="">
                      { /*this.state.limit?
      <Select
         styles={customStyles}
         width='100%'
         menuColor='red'
         options={options}
         value={timeinforcetype}
         onChange={this.changetimeinforce}
         />
         :'' */}
                    </div>

                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-3 d-none">

                        </div>
                        <div className="col-md-9 mx-auto">
                          <div className="def-number-input safari_only colorBlue">
                            <div class="btn-group">
                              {(this.state.buyorsell == 'buy') ?
                                <button class="btn btnGroupBorderBtn btnBuyBlue" onClick={this.handleShow.bind(this)}>Buy</button> :
                                <button class="btn btnGroupBorderBtn btnBuyYellow" onClick={this.handleShow.bind(this)}>Buy</button>
                              }
                              {(this.state.buyorsell == 'sell') ?
                                <button class="btn btnGroupBorderBtn btnSellBlue" onClick={this.handleShow.bind(this)}>Sell</button> :
                                <button class="btn btnGroupBorderBtn btnSellYellow" onClick={this.handleShow.bind(this)}>Sell</button>
                              }
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="tab-pane fade show active" id="nav-tradeLimit" role="tabpanel" aria-labelledby="nav-tradeLimit-tab">
                      {this.state.limit ?
                        <div>
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-4">
                                <label className="marginTopLabel">Price - <span>{this.state.secondcurrency.replace('USDT', 'USD')}</span></label>
                              </div>
                              <div className="col-md-8">
                                <div className="def-number-input safari_only colorBlue">
                                  <input className="quantity" min="0" name="price" id="price" value="1" type="text" value={this.state.price} onChange={this.onChange} />

                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-6">
                                <label>Current Price - <span>{this.state.secondcurrency}</span></label>
                              </div>
                              <div className="col-md-6">
                                <div className="def-number-input safari_only colorBlue">
                                  {parseFloat(this.state.markprice).toFixed(this.state.floating) + " " +
                                    this.state.secondcurrency}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        :
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-6">
                              <label>Current Price - <span>{this.state.secondcurrency}</span></label>
                            </div>
                            <div className="col-md-6">
                              <div className="def-number-input safari_only colorBlue">
                                {parseFloat(this.state.markprice).toFixed(this.state.floating) + " " +
                                  this.state.secondcurrency}

                              </div>
                            </div>
                          </div>
                        </div>
                      }

                      {this.state.conditional ?
                        <div>
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-5">
                                <div className="btn-group">
                                  <button className="btn btnGroupBorderBtn" onClick={this.conditionalPrice.bind(this)}>Limit</button>
                                  <button className="btn btnGroupBorderBtn" onClick={this.conditionalPrice.bind(this)}>Market</button>
                                </div>
                              </div>
                              <div className="col-md-7">
                                <div className="def-number-input number-input safari_only">
                                  <input className="quantity" min="0" readOnly={this.state.readOnly} name="price" id="price" value="1" type="number" value={this.state.price} onChange={this.onChange} />
                                  <button onClick={this.updownvalue.bind('pri', 'price', 'plus')} className="2 price plus"></button>
                                  <button onClick={this.updownvalue.bind('pri', 'price', 'minus')} className="2 price minus"></button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-5">
                                <label>Trigger Price <span>{this.state.second_currency}</span></label>
                              </div>
                              <div className="col-md-7">
                                <div className="def-number-input number-input safari_only">
                                  <input className="quantity" min="0" name="trigger_price" id="trigger_price" type="number" value={this.state.trigger_price} onChange={this.onChange} />
                                  <button onClick={this.updownvalue.bind('pri', 'trigger_price', 'plus')} className="2 price plus"></button>
                                  <button onClick={this.updownvalue.bind('pri', 'trigger_price', 'minus')} className="2 price minus"></button>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div className="btn-group btn-block mt-3">
                                  <button className="btn btn-borderButton mr-1" onClick={this.triggerPrice.bind(this)}>Last</button>
                                  <button className="btn btn-borderButton mr-1" onClick={this.triggerPrice.bind(this)}>Index</button>
                                  <button className="btn btn-borderButton mr-1" onClick={this.triggerPrice.bind(this)}>Mark</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        : ''}
                      <div className="form-group">
                        <div className="row">
                          <div className="col-md-4">
                            <label>Qty - <span>{this.state.firstcurrency}</span></label>
                          </div>
                          <div className="col-md-8">
                            <div className="def-number-input number-input safari_only">
                              <input className="quantity" min="0" name="quantity" id="quantity" value={this.state.quantity} onChange={this.onChange} type="number" />
                              <button onClick={this.updownvalue.bind('quan', 'quantity', 'plus')} className="2 plus"></button>
                              <button onClick={this.updownvalue.bind('quan', 'quantity', 'minus')} className="2 minus"></button>
                            </div>
                            {/* <div className="input-group mt-2">
                     <button className="btn btn-borderButton mr-1" onClick={this.balanceperc}>25%</button>
                     <button className="btn btn-borderButton mr-1" onClick={this.balanceperc}>50%</button>
                     <button className="btn btn-borderButton mr-1" onClick={this.balanceperc}>75%</button>
                     <button className="btn btn-borderButton" onClick={this.balanceperc}>100%</button>
                  </div>*/}
                          </div>
                        </div>
                      </div>
                      <ul className="list2ColumnJustify">
                        <li>
                          <label>Order Value</label>
                          <span>{parseFloat(this.state.order_value).toFixed(8) + ' ' + this.state.secondcurrency}</span>
                        </li>

                        <li>
                          <label>Balance </label>
                          {this.state.buyorsell == 'buy' ?
                            <span>{parseFloat(this.state.secondbalance).toFixed(8) + ' ' + this.state.secondcurrency}</span> :
                            <span>{parseFloat(this.state.firstbalance).toFixed(8) + ' ' + this.state.firstcurrency}</span>
                          }
                        </li>


                      </ul>
                      {this.state.alertmessage != '' ?
                        <div class="quadrat">{this.state.alertmessage}</div> : ''
                      }
                      {(typeof user.email != 'undefined') ?
                        <div className="form-group input-group buySellBtn">
                          {(!this.state.buydisable) ?
                            <button className="btn buttonType2 mr-2" onClick={this.handleShow22.bind(this)} disabled={!this.state.disable}>
                              <span className="spinner-border spinner-border-sm"></span> Send order
            </button> :
                            <button className="btn buttonType2 mr-2" onClick={this.handleShow22.bind(this)} disabled={!this.state.disable}>
                              Send order
            </button>
                          }

                        </div>
                        :
                        <div className="form-group input-group buySellBtn">
                          <Link to="/Login" class="btn btnBlue hvr-shadow float-right btn-block mb-3 py-2">Login</Link>
                        </div>}
                    </div>

                    <div className="tab-pane fade" id="nav-conditional" role="tabpanel" aria-labelledby="nav-conditional-tab">
                      <div className="form-group">
                        <div className="row">
                          <div className="col-md-5">
                            <div className="btn-group">
                              <button className="btn btnGroupBorderBtn">Limit</button>
                              <button className="btn btnGroupBorderBtn">Market</button>
                            </div>
                          </div>
                          <div className="col-md-7">
                            <div className="def-number-input number-input safari_only">
                              <input className="quantity" min="0" name="quantity" value="1" type="number" />
                              <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                              <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-md-5">
                            <label>Trigger Price <span>USD</span></label>
                          </div>
                          <div className="col-md-7">
                            <div className="def-number-input number-input safari_only">
                              <input className="quantity" min="0" name="quantity" value="0.25658748" type="number" />
                              <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                              <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div className="btn-group btn-block mt-3">
                              <button className="btn btn-borderButton mr-1">Last</button>
                              <button className="btn btn-borderButton mr-1">Index</button>
                              <button className="btn btn-borderButton mr-1">Mark</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-md-4">
                            <label>Qty - <span>BTC</span></label>
                          </div>
                          <div className="col-md-8">
                            <div className="def-number-input number-input safari_only">
                              <input className="quantity" min="0" name="quantity" value="0.25658748" type="number" />
                              <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" className="plus"></button>
                              <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" className="minus"></button>
                            </div>
                            {/*<div className="input-group mt-2">
                    <button className="btn btn-borderButton mr-1">25%</button>
                    <button className="btn btn-borderButton mr-1">50%</button>
                    <button className="btn btn-borderButton mr-1">75%</button>
                    <button className="btn btn-borderButton">100%</button>
                  </div>*/}
                          </div>
                        </div>
                      </div>
                      <ul className="list2ColumnJustify">
                        <li>
                          <label>Order Value</label>
                          <span>{parseFloat(this.state.order_value).toFixed(8) + ' ' + this.state.firstcurrency}</span>
                        </li>
                        <li>
                          <label>Balance</label>
                          <span>{parseFloat(this.state.balance).toFixed(8) + ' ' + this.state.firstcurrency}</span>
                        </li>
                        <li>
                          <label>Margin impact</label>
                          <span>{parseFloat(this.state.order_cost).toFixed(8) + ' ' + this.state.firstcurrency}</span>
                        </li>
                        {this.state.limit ?
                          <li>
                            <div className="checkbox"><label><input name="post_only" value="1" type="checkbox" /><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText">Post only</span></label></div>
                            <div className="checkbox"><label><input name="reduce_only" value="1" type="checkbox" /><span className="cr"><i className="cr-icon fa fa-check"></i></span><span className="listText">Reduce only</span></label></div>
                          </li> : ''}
                      </ul>
                      <div className="form-group input-group buySellBtn">
                        {(!this.state.buydisable) ?
                          <button className="btn btn-btnBuy mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
                            <span className="spinner-border spinner-border-sm"></span> Buy
            </button> :
                          <button className="btn btn-btnBuy mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
                            Buy
            </button>
                        }
                        {(!this.state.selldisable) ?
                          <button className="btn btn-btnSell mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
                            <span className="spinner-border spinner-border-sm"></span> Sell
            </button> :
                          <button className="btn btn-btnSell mr-2" onClick={this.handleShow.bind(this)} disabled={!this.state.disable}>
                            Sell
            </button>
                        }
                      </div>
                    </div>

                  </div>
                </div>





                <div className="darkBox contractDetailsBTCUSDT tradeFulltabbedTable">
                  <nav>
                    <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a className="nav-item nav-link active" id="nav-trollBox-tab" data-toggle="tab" href="#nav-trollBox" role="tab" aria-controls="nav-trollBox" aria-selected="true">Troll Box</a>

                    </div>
                  </nav>
                  <div className="tab-content px-3 px-sm-0" id="nav-tabContent">
                    <div className="tab-pane fade show active" id="nav-trollBox" role="tabpanel" aria-labelledby="nav-trollBox-tab">
                      <div className="trollBox">


                        <div className="messages">
                          <Scrollbars style={{ width: '100%', height: 500 }}>
                            <ul className="customScroll">
                              {this.state.messages.map(message => {
                                var modclass = (message.moderator == 0) ? "nonmoderatorName" : "moderatorName";
                                return (
                                  <li><span className={modclass}>{message.author} :</span> {message.message}</li>
                                )
                              })}
                            </ul>
                          </Scrollbars>

                        </div>
                        {(typeof user.email != 'undefined' && this.state.blocktime < new Date()) ?
                          <div className="input-group bottomFixed trollBoxForm">
                            <input className="form-control" type="text" name="Message" value={this.state.message} onChange={ev => this.setState({ message: ev.target.value })} />
                            <input type="submit" onClick={this.sendMessage} name="" value="Send" />
                          </div>
                          : ''}
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </section>


          <Modal show={this.state.show} onHide={this.handleClose} aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
              <Modal.Title>Order Details <small>{this.state.ordertype} {this.state.buyorsell}</small></Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="table-responsive">
                <table className="table table-bordered tradeBuy">
                  <tr>
                    <td align="right" width="50%">Order Price</td>
                    <td><span>{(this.state.ordertype == 'Market') ? parseFloat(this.state.markprice).toFixed(this.state.floating) + ' ' + this.state.secondcurrency : this.state.price + ' ' + this.state.secondcurrency.replace('USDT', 'USD')}</span></td>
                  </tr>
                  <tr>
                    <td align="right" width="50%">Order Value</td>
                    <td>{this.state.order_value + ' ' + this.state.secondcurrency}</td>
                  </tr>

                  <tr>
                    <td align="right" width="50%">Available Balance</td>
                    {(this.state.buyorsell == 'buy') ?
                      <td>{parseFloat(this.state.secondbalance).toFixed(8) + ' ' + this.state.secondcurrency.replace('USDT', 'USD')}</td> :
                      <td>{parseFloat(this.state.firstbalance).toFixed(8) + ' ' + this.state.firstcurrency}</td>
                    }
                  </tr>

                  <tr>
                    <td align="right" width="50%">Mark Price</td>
                    <td>{parseFloat(this.state.markprice).toFixed(this.state.floating) + ' ' + this.state.secondcurrency.replace('USDT', 'USD')}</td>
                  </tr>

                  {/*<tr>
                  <td align="right" width="50%">Time in Force</td>
                  <td>GoodTillCancelled</td>
                </tr>*/}
                </table>
              </div>

              {(this.state.alertmessage1 != '' && this.state.stopreadOnly == false) ?
                <div class="quadrat">{this.state.alertmessage1}</div> : ''
              }
              {(this.state.alertmessage2 != '' && this.state.tpreadOnly == false) ?
                <div class="quadrat">{this.state.alertmessage2}</div> : ''
              }

            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary btnDefaultNewBlue" onClick={this.handleClose}>
                Cancel
          </Button>
              <Button variant="primary btnDefaultNew" onClick={this.orderPlacing}>
                Confirm
          </Button>
            </Modal.Footer>
          </Modal>



          <TradeFooter />
        </div>
      </div>
    );
  }
}

Trade.propTypes = {
  changeopenpositions: PropTypes.func.isRequired,
  triggerstop: PropTypes.func.isRequired,
  getspotPricevalue: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  spotcancelTrade: PropTypes.func.isRequired,
  getspotPertual: PropTypes.func.isRequired,
  getspotTradeData: PropTypes.func.isRequired,
  getspotuserTradeData: PropTypes.func.isRequired,
  spotorderPlacing: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(

  mapStateToProps,
  {
    changeopenpositions,
    getspotPertual,
    spotcancelTrade,
    spotorderPlacing,
    getspotTradeData,
    getspotuserTradeData,
    logoutUser,
    triggerstop,
    getspotPricevalue
  }
)(withRouter(Trade));
