import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import FaqImg from "../images/aboutus.png"
import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;

class Faq extends Component {
  constructor(props) {
        super(props);
        this.state = {  
            question:"",
            answer:"",
            faq:[],
            errors: {}, 
        };
        console.log(this.state,'state');
        }

        componentDidMount() {
            this.getData()   
        };
    
    getData() {
           axios
           .get(url+"cryptoapi/faq")
             .then(res => { 
             console.log(res.data,'ressssssdata');  
                   this.setState({faq:res.data});
             })
            .catch()
            //console.log(this.state.faq,'this.setState');
    }
   
	render() {
		return (<div>
			<Navbar />
				<section className="innerCMS">
        {this.state.faq?
  <div className="container">
    <div className="row">
      <div className="col-md-10 mx-auto">
        <img src={FaqImg} className="img-fluid img-fluid cmsTopImg mt-1" />
        <div className="darkBox contentPage">
          <div className="tableHead tableHeadBlock">
            <h2>FAQ</h2>
          </div>
          <div className="darkBoxSpace">
            <div className="faqCont">
          <div className="homeAccordian wow fadeIn">
          
              <div id="accordion">
               {this.state.faq.map((answer, i) => {
               console.log(i,'iiii'); 
                  var view = (i==0)?'show':''; 
                  var expand = (i==0)?'true':'false'; 
                  var btn = (i==0)?'btn btn-link':'btn btn-link collapsed'; 
               console.log(view,'iiiixxx'); 
           return (  <div className="card">
                  <div className="card-header wow flipInX" id={"heading"+i}>
                    <h5 className="mb-0">
                      <button className={btn} data-toggle="collapse" data-target={"#collapse"+i} aria-expanded={expand} aria-controls={"collapse"+i}><span className="question">{i+1}.{answer.question}</span> <i className="fa fa-plus" aria-hidden="true"></i></button>
                    </h5>
                  </div>

                  <div id={"collapse"+i} className={"collapse"+view} aria-labelledby={"heading"+i} data-parent="#accordion">
                    <div className="card-body">
                      <p>{answer.answer}</p>
                    </div>
                  </div>
                </div>)
             })}
              </div>
              
             
            </div>
        </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   :''}
</section>
			<Footer />
		</div>
		);
	}
}

export default Faq