import React, { Component } from 'react'
import BannerX from "../images/xicon.png"
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import { toast } from 'react-toastify';
import { store } from 'react-notifications-component';
import $ from 'jquery';
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { contact } from "../actions/authActions";
import classnames from "classnames";
import keys from "../actions/config";
import axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";
const url = keys.baseUrl;


const Recaptchakey = keys.Recaptchakey;
const recaptchaRef = React.createRef();
const grecaptchaObject = window.grecaptcha
class Contact extends Component {

  constructor() {
        super();
        this.state = {
            email            : "",
            email            : "",
            contact_person   : "",
            sitename         : "",
            site_description : "",
            favorite_image   : "",
            sitelogo         : "",
            phone_number     : "",
            mobile_number    : "",
            address          : "",
            tax_amount       : "",
            social_link1     : "",
            social_link2     : "",
            social_link3     : "",
            social_link4     : "",
            social_link5     : "",
            copyright_text   : "",
            working_day      : "",
            working_hours    : "",
            recaptcha        : "",
            name             : "",
            message          : "",
            settings         : {},
            errors           : {},
        };
    }
componentDidMount() {
            this.getData()  
           
           
        };
    
    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
        
        if (nextProps.auth !== undefined
            && nextProps.auth.contact_us !== undefined
            && nextProps.auth.contact_us.data !== undefined
            && nextProps.auth.contact_us.data.message !== undefined) {
           store.addNotification({
            title: "Wonderful!",
            message: nextProps.auth.contact_us.data.message,
            type: "success",
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 5000,
              onScreen: true
            }
          });
            nextProps.auth.contact_us = undefined;
            
        }
    }

    captchaChange = e => {
      this.setState({ recaptcha : e });
  };

getData() {
           axios
           .get(url+"cryptoapi/contact")
            .then(res => { 
            console.log(res,'res_contact');  
              this.setState({settings:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };

    onSubmit = e => {
        e.preventDefault();
        const Newcontact = {
            email: this.state.email,
            name: this.state.name,
            message: this.state.message,
            recaptcha: this.state.recaptcha,
        };
        console.log(Newcontact);
        this.props.contact(Newcontact);
    };

	render() {
      const { errors } = this.state;
		return (<div>
			<Navbar />
			<header className="loginBanner">
  <div className="container">
    <div className="row">
      <div className="col-md-6">
         {this.state.settings.data?
        <div className="homeBannerText wow fadeInUp" data-wow-delay=".25s">
          <h3>Exchange Top Cryptos with</h3>
          <h2><span>150</span> <img src={BannerX} /> <small>Leverage</small></h2>
         <p>{this.state.settings.data.sitename}<br />
             Register Code  : {this.state.settings.data.reg_code}<br />
             Address: {this.state.settings.data.address}<br />
             Contact : {this.state.settings.data.phone_number}<br />
             Email :<a href={"mailto:" + this.state.settings.data.email}>{this.state.settings.data.email}</a></p>
             <p>{this.state.settings.data.copyright_text}</p>
        </div>
        :''}
      </div>
      <div className="col-md-6">
        <div className="formBox wow flipInY mt-3 mt-md-5 mb-5 mb-md-0" data-wow-delay=".15s;">
         <h2>Contact us</h2>
         <nav>
          
        </nav>
        <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
          <div className="tab-pane fade show active" id="nav-emailLogin" role="tabpanel" aria-labelledby="nav-emailLogin-tab">
            <form className="stoForm" noValidate onSubmit={this.onSubmit}>
            <div className="form-group fadeIn second">
            <label>Name</label>
            <input
                onChange={this.onChange}
                value={this.state.name}
                error={errors.name}
                id="name"
                type="text"
                className={classnames("form-control", {
                    invalid: errors.name
                })}
            />
            <span className="text-danger">{errors.name}</span>
          </div>
            	<div className="form-group fadeIn second">
            <label>Email address</label>
            <input
                onChange={this.onChange}
                value={this.state.email}
                error={errors.email}
                id="email"
                type="email"
                className={classnames("form-control", {
                    invalid: errors.email
                })}
            />
            <span className="text-danger">{errors.email}</span>
          </div>
		          <div className="form-group fadeIn third">
            <label>Message</label>
            <textarea
                onChange={this.onChange}
                value={this.state.message}
                error={errors.message}
                id="message"
                type="text"
                className={classnames("form-control", {
                    invalid: errors.message
                })}
            />
            <span className="text-danger">{errors.message}</span>
          </div>

           <div className="form-group fadeIn third">
            
                     <ReCAPTCHA
                ref={(r) => this.recaptcha = r}
                sitekey={Recaptchakey}
                grecaptcha={grecaptchaObject}
                onChange={this.captchaChange}
                />
            <span className="text-danger">{errors.recaptcha}</span>
          </div>
            <div className="form-group">
            <input type="submit" className="btn-block fadeIn fourth" value="Submit" />
          </div>
            </form>
      
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>  
</header>
<Footer />
		</div>
		);
	}
}


Contact.propTypes = {
    contact: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { contact }
)(withRouter(Contact));
