import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import TradeHeader from './TradeHeader'
import TradeFooter from './TradeFooter'
import SupportTicketImg from "../images/supportTicket.png"
import $ from 'jquery';
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { store } from 'react-notifications-component';
import { connect } from "react-redux";
import { support } from "../actions/authActions";
import classnames from "classnames";
import keys from "../actions/config";
import axios from "axios";
import { UploaderComponent } from '@syncfusion/ej2-react-inputs';
const url = keys.baseUrl;

class SupportTicket extends Component {
	 constructor() {
        super();
        this.state = {
             id:"",
            email_add: "",
            subject:"",
            description: "",
            attachment:"",
            validation:{},
            errors: {},
            
        };
         this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
        	console.log(nextProps,'nextPropsasadxsdsf');
            this.setState({
                errors: nextProps.errors
            });
        }
        
        if (nextProps.auth !== undefined
            && nextProps.auth.support !== undefined
            && nextProps.auth.support.data !== undefined
            && nextProps.auth.support.data.message !== undefined) {
           store.addNotification({
            title: "Wonderful!",
            message: nextProps.auth.support.data.message,
            type: "success",
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 5000,
              onScreen: true
            }
          });
            nextProps.auth.support = undefined;
              this.props.history.push("/supportreply");
              console.log(this.state,'thissssssssssssssssssssssssstate');
            
        }
    }
 
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
 handleChange = (event) => {
      this.setState({
      //  profileurl: URL.createObjectURL(event.target.files[0]),
        attachment: event.target.files[0]
      })
    }
    onSubmit = e => {
      e.preventDefault();
      console.log(this.props.errors,'errrrrrrr');
      if (this.validateForm()) {
        let fields = {};
        fields["email_add"] = "";
        fields["subject"] = "";
        fields["description"] = "";
        this.setState({validation:fields});
        //alert("Form submitted");  
        const Newsupport = {
          email_add: this.state.email_add,
          subject: this.state.subject,
          description: this.state.description,
          attachment: this.state.attachment
        };
        const data = new FormData();
        data.append('email_add', this.state.email_add);
        data.append('subject', this.state.subject);
        data.append('description', this.state.description);
        data.append('file', this.state.attachment);
        console.log(Newsupport);
        this.props.support(data);
      }
    };

    validateForm() {

      let fields = this.state.validation;
      let errors = {};
      errors["email_add"] = '';
      errors["subject"] = '';
      errors["description"] = '';
      let formIsValid = true;

      if (this.state.email_add=='') {
        formIsValid = false;
        errors["email_add"] = "*Please enter your email-ID.";
      } else if (typeof fields["email_add"] !== "undefined") {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (!pattern.test(fields["email_add"])) {
          formIsValid = false;
          errors["email_add"] = "*Please enter valid email-ID.";
        }
      }
      if (this.state.subject=='') {
        formIsValid = false;
        errors["subject"] = "*Please enter your Subject";
      }
      if (this.state.description=='') {
        formIsValid = false;
        errors["description"] = "*Please enter your Description";
      }

      this.setState({
        errors: errors
      });
      return formIsValid;

    }


  render() {
 
  	const { errors } = this.state;
    return (
			<div>
				<TradeHeader />
				<div className="container-fluid">
				<section className="tradeMain">
				  <div className="row">
				    <div className="col-xl-8 col-lg-10 col-md-12 mx-auto">
				      <img src={SupportTicketImg} className="img-fluid cmsTopImg mbMinus" />
				      <div className="darkBox supportTicket">
				        <div className="tableHead tableHeadBlock">
				          <h2>Support Ticket</h2>
				        </div>
				        <div className="formWhite darkBoxSpace">
				          <form name="supportTicket" onSubmit={this.onSubmit}>
				            <div className="row">
				              <div className="col-md-6">
				                <div className="form-group">
				                  <label>Your email address<sup>*</sup></label>
							                  <input name="email_add"
			                onChange={this.onChange}
			                value={this.state.email_add}
			                error={errors.email_add}
			                id="email_add"
			                type="email"
			                name="email_add"
			                className={classnames("form-control", {
			                    invalid: errors.email_add
			                })}
			            />
			     
            <span className="text-danger">{errors.email_add}</span>
				                </div>
				              </div>
				              <div className="col-md-6">
				                <div className="form-group clearfix">
				                  <label>Subject</label>
				                 <input name="subject"
                onChange={this.onChange}
                value={this.state.subject}
                error={errors.subject}
                id="subject"
                type="text"
                name="subject"
                className={classnames("form-control", {
                    invalid: errors.subject
                })}
            />
           <span className="text-danger">{errors.subject}</span>
				                </div>
				              </div>
				              <div className="col-md-12">
				                <div className="form-group">
				                  <label>Description <sup>*</sup></label>
				                   <textarea name="description"
                onChange={this.onChange}
                value={this.state.description}
                error={errors.description}
                id="description"
                type="text"
                name="description"
                className={classnames("form-control", {
                    invalid: errors.description
                })}
            />
            
             <span className="text-danger">{errors.description}</span>

				                </div>
				              </div>

				              <div className="col-md-6">
				                <div className="form-group">
				                   <label>Attachments if any</label>
				                   <label className="custom-file form-control" id="customFile">
				                      <input type="file"  onChange={this.handleChange} name="file1" className="custom-file-input" id="exampleInputFile" aria-describedby="fileHelp" />
				                      <span className="custom-file-control form-control-file"></span>
				                  </label>
				                </div>
				              </div>
				              <div className="col-md-12 mt-3 mb-3">
				                <div className="form-group">
				                  <input type="submit" name="" value="Submit" className="btn buttonType1" />
				                </div>
				              </div>
				              <div className="col-md-12">
				                <div className="noteText">
				                  <h6>Notes</h6>
				                  <p>1. Please enter your 6 digit account UID if you have already registered with us. This field is compulsory if you registered account with your mobile number.</p>
				                  <p>2. Please enter the details of your request. For trade related inquiries, please indicate the affected order# and describe the issue you encountered in details. 
				                  A member of our support staff will respond as soon as possible.</p>
				                </div>
				              </div>
				            </div>
				          </form>
				        </div>
				      </div>
				    </div>
				  </div>
				</section>
				</div>
				<TradeFooter />
			</div>
			);
    }
}
SupportTicket.propTypes = {
    support: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { support }
)(withRouter(SupportTicket));

