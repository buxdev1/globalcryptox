import React, { Component } from "react";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import Footer from "./Footer";
import LeverageImg from "../images/leverage.png";
import LeverageChartImg from "../images/leverageBarChart.jpg";

class CoinInfo extends Component {
  constructor() {
    super();
    this.state = {
      showcontent: "BTC",
    };
  }
  componentDidMount() {}

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  Contentchange = (item) => {
    console.log("responseitem", item);
    this.setState({ showcontent: item });
  };

  render() {
    return (
      <div>
        <Navbar />

        <section className="innerCMS">
          <div className="container">
            <div className="row mt-4">
              <div className="col-md-3 flexColumn">
                <div class="CoinInfoSideBar">
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fas fa-search"></i>
                      </span>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Search"
                    />
                  </div>
                  <ul>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("BTC");
                        }}
                      >
                        BTC（Bitcoin)
                      </a>
                    </li>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("ETH");
                        }}
                      >
                        ETH（Ethereum)
                      </a>
                    </li>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("BCH");
                        }}
                      >
                        BCH(Bitcoin Cash)
                      </a>
                    </li>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("LTC");
                        }}
                      >
                        LTC（Litecoin）
                      </a>
                    </li>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("USDT");
                        }}
                      >
                        USDT（TetherUS）
                      </a>
                    </li>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("XRP");
                        }}
                      >
                        XRP(Ripple)
                      </a>
                    </li>
                    <li>
                      <i class="fas fa-angle-right"></i>{" "}
                      <a
                        href="#"
                        onClick={() => {
                          this.Contentchange("BUSD");
                        }}
                      >
                        BUSD
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-9 flexColumn">
                {this.state.showcontent=="BTC"?(
                  <div className="darkBox contentPage leverageContent">
                  <div className="tableHead tableHeadBlock">
                    <h2>BTC（Bitcoin）</h2>
                    <h6>2018-02-28</h6>
                  </div>
                  <div className="darkBoxSpace">
                    <div class="coinInfoCard mb-4">
                      <h4>1.Project Description</h4>
                      <ul class="pdListContent">
                        <li>
                          <span>Project Name:</span> Bitcoin
                        </li>
                        <li>
                          <span>Token name:</span> BTC
                        </li>
                      </ul>
                    </div>
                    <div class="coinInfoCard mb-4">
                      <h4>2.Project Introduction</h4>
                      <p>
                      Bitcoin is a cryptocurrency and a digital payment system invented by an unknown programmer, or a group of programmers, under the name Satoshi Nakamoto.It was released as open-source software in 2009.The system is peer-to-peer, and transactions take place between users directly, without an intermediary. These transactions are verified by network nodes and recorded in a public distributed ledger called a blockchain. Since the system works without a central repository or single administrator, bitcoin is called the first decentralized digital currency.Besides being created as a reward for mining, bitcoin can be exchanged for other currencies, products, and services in legal or black markets.As of February 2015, over 100,000 merchants and vendors accepted bitcoin as payment. According to research produced by Cambridge University in 2017, there are 2.9 to 5.8 million unique users using a cryptocurrency wallet, most of them using bitcoin.
                      </p>
                    </div>
                    <div class="coinInfoCard mb-4">
                      <h4>3.Token overview</h4>
                      <ul class="pdListContent">
                        <li>
                          <span>Total circulation:</span>21 million
                        </li>
                      </ul>
                    </div>
                    <div class="coinInfoCard">
                      <h4>4.Support links</h4>
                      <ul class="pdListContent">
                        <li>
                          <span>Official Website:</span>{" "}
                          <a href="https://bitcoin.org/zh_CN/" target="_blank">
                          https://bitcoin.org/zh_CN/
                          </a>
                        </li>
                        <li>
                          <span>Whitepaper:</span>{" "}
                          <a
                            href="https:https://bitcoin.org/bitcoin.pdf"
                            target="_blank"
                          >
                            https:https://bitcoin.org/bitcoin.pdf
                          </a>
                        </li>


                      </ul>
                    </div>
                  </div>
                </div>)
              :this.state.showcontent=="ETH"?(
                <div className="darkBox contentPage leverageContent">
                <div className="tableHead tableHeadBlock">
                  <h2>ETH（Ethereum）</h2>
                  <h6>2018-02-28</h6>
                </div>
                <div className="darkBoxSpace">
                  <div class="coinInfoCard mb-4">
                    <h4>1.Project Description</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Project Name:</span> Ethereum
                      </li>
                      <li>
                        <span>Token name:</span> ETH
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>2.Project Introduction</h4>
                    <p>
                    Ethereum (ETH) is an open-source, public, blockchain-based distributed computing platform featuring smart contract (scripting) functionality, which facilitates online contractual agreements. Ethereum also provides a cryptocurrency token called "ether", which can be transferred between accounts and used to compensate participant nodes for computations performed. "Gas", an internal transaction pricing mechanism, is used to mitigate spamand allocate resources on the network.The value token of the Ethereum blockchain is called ether. It is listed under the diminutive ETH and traded on cryptocurrency exchanges. It is also used to pay for transaction fees and computational services on the Ethereum network.
                    </p>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>3.Token overview</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Total Supply:</span> 107,100,935
                      </li>
                      <li>
                        <span>Circulation:</span> 107,100,935
                      </li>
                    </ul>
                  </div>
                  <div class="coinInfoCard">
                    <h4>4.Support links</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Official Website:</span>{" "}
                        <a href=":https://www.ethereum.org/" target="_blank">
                        :https://www.ethereum.org/
                        </a>
                      </li>
                      <li>
                        <span>Whitepaper:</span>{" "}
                        <a
                          href="https://github.com/ethereum/wiki/wiki/White-Paper"
                          target="_blank"
                        >
                          https://github.com/ethereum/wiki/wiki/White-Paper
                        </a>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>

              ):this.state.showcontent=="BCH"?(
                <div className="darkBox contentPage leverageContent">
                <div className="tableHead tableHeadBlock">
                  <h2>BCH（Bitcoin Cash ）</h2>
                  <h6>2018-02-28</h6>
                </div>
                <div className="darkBoxSpace">
                  <div class="coinInfoCard mb-4">
                    <h4>1.Project Description</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Project Name:</span> Bitcoin Cash
                      </li>
                      <li>
                        <span>Token name:</span> BCH
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>2.Project Introduction</h4>
                    <p>
                    Bitcoin Cash (BCH) is an altcoin version of the popular Bitcoin cryptocurrency. Bitcoin Cash is the result of a hard fork in blockchain technology. One of the most significant changes from Bitcoin to Bitcoin Cash is the size of the coin. Previously, Bitcoin’s 1MB limitation caused transaction delays, so Bitcoin Cash increased the potential block size to enable a greater number of transactions and help the cryptocurrency scale as it grew and competed with more traditional cryptocurrency platforms.

After its creation, Bitcoin Cash quickly became the 3rd most successful cryptocurrency, following Bitcoin and Etherium. Although Bitcoin Cash has a higher transaction rate, the currency is not accepted in as many places as Bitcoin or Etherium, and disagreements in the developer community have caused Bitcoin Cash to be promoted as more of an investment tool than a transactional currency.
                    </p>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>3.Token overview</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Total Supply:</span> 21,000,000
                      </li>
                    </ul>
                  </div>
                  <div class="coinInfoCard">
                    <h4>4.Support links</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Official Website:</span>{" "}
                        <a href="https://www.bitcoincash.org/" target="_blank">
                        https://www.bitcoincash.org/
                        </a>
                      </li>
                      <li>
                        <span>Whitepaper:</span>{" "}
                        <a
                          href="https://www.bitcoincash.org/bitcoin.pdf"
                          target="_blank"
                        >
                          https://www.bitcoincash.org/bitcoin.pdf
                        </a>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>

              ):this.state.showcontent=="LTC"?(
                <div className="darkBox contentPage leverageContent">
                <div className="tableHead tableHeadBlock">
                  <h2>LTC（Litecoin）</h2>
                  <h6>2018-02-28</h6>
                </div>
                <div className="darkBoxSpace">
                  <div class="coinInfoCard mb-4">
                    <h4>1.Project Description</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Project Name:</span> Litecoin
                      </li>
                      <li>
                        <span>Token name:</span> LTC
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>2.Project Introduction</h4>
                    <p>
                    Litecoin (LTC ) is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. Creation and transfer of coins is based on an open source cryptographic protocol and is not managed by any central authority. While inspired by, and in most regards technically nearly identical to Bitcoin (BTC), Litecoin has some technical improvements over Bitcoin, and most other major cryptocurrencies, such as the adoption of Segregated Witness, and the Lightning Network. These effectively allow a greater amount of transactions to be processed by the network in a given time, reducing potential bottlenecks, as seen with Bitcoin. Litecoin also has almost zero payment cost and facilitates payments approximately four times faster than Bitcoin
                    </p>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>3.Token overview</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Total Circulation:</span>  2 billion
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard">
                    <h4>4.Support links</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Official Website:</span>{" "}
                        <a href="https://litecoin.org " target="_blank">
                        https://litecoin.org
                        </a>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>

              ):this.state.showcontent=="USDT"?(
                <div className="darkBox contentPage leverageContent">
                <div className="tableHead tableHeadBlock">
                  <h2>USDT (TetherUS)</h2>
                  <h6>2018-02-28</h6>
                </div>
                <div className="darkBoxSpace">
                  <div class="coinInfoCard mb-4">
                    <h4>1.Project Description</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Project Name:</span> TetherUS
                      </li>
                      <li>
                        <span>Token name:</span> USDT
                      </li>
                      <li>
                        <span>Contract Address:</span>{" "}
                        <a
                          href="https://etherscan.io/token/0xdac17f958d2ee523a2206206994597c13d831ec7"
                          target="_blank"
                        >
                          0xdac17f958d2ee523a2206206994597c13d831ec7
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>2.Project Introduction</h4>
                    <p>
                    USDT is a crypto-currency asset issued on the Bitcoin blockchain via the Omni Layer Protocol. Each USDT unit is backed by a U.S Dollar held in the reserves of the Tether Limited and can be redeemed through the Tether Platform. USDT can be transferred, stored, spent, just like Bitcoins or any other crypto-currency.USDT and other Tether currencies were created to facilitate the transfer of national currencies, to provide users with a stable alternative to Bitcoin and to provide an alternative for exchange and wallet audits which are currently unreliable. USDT provides an alternative to Proof of Solvency methods by introducing a Proof of Reserves Process. In the Tether Proof of Reserves system, the amount of USDT in circulations can be easily checked on the Bitcoin blockchain via the tools provided at Omnichest.info, while the corresponding total amount of USD held reserves is proved by publishing the bank balance and undergoing periodic audits by professionals.
                    </p>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>3.Token overview</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Total circulation:</span> 2.83 billion
                      </li>
                      <li>
                        <span>Flow circulation:</span> 2.507 billion
                      </li>
                    </ul>
                  </div>
                  <div class="coinInfoCard">
                    <h4>4.Support links</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Official Website:</span>{" "}
                        <a href="https://tether.to/" target="_blank">
                        https://tether.to/
                        </a>
                      </li>
                      <li>
                        <span>Whitepaper:</span>{" "}
                        <a
                          href="https: //tether.to/wp-content/uploads/2016/06/TetherWhitePaper.pdf"
                          target="_blank"
                        >
                          https: //tether.to/wp-content/uploads/2016/06/TetherWhitePaper.pdf
                        </a>
                      </li>

                      <li>
                        <span>Twitter:</span>{" "}
                        <a
                          href="https: //twitter.com/Tether_to/"
                          target="_blank"
                        >
                         https: //twitter.com/Tether_to/
                        </a>
                      </li>
                      <li>
                        <span>Facebook:</span>{" "}
                        <a
                          href="https: //www.facebook.com/tether.to"
                          target="_blank"
                        >
                          https: //www.facebook.com/tether.to
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              ):this.state.showcontent=="XRP"?(
                <div className="darkBox contentPage leverageContent">
                <div className="tableHead tableHeadBlock">
                  <h2>XRP (Ripple)</h2>
                  <h6>2018-02-28</h6>
                </div>
                <div className="darkBoxSpace">
                  <div class="coinInfoCard mb-4">
                    <h4>1.Project Description</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Project Name:</span> Ripple
                      </li>
                      <li>
                        <span>Token name:</span> XRP
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>2.Project Introduction</h4>
                    <p>
                    Ripple (XRP) is an independent digital asset that is native to the Ripple Consensus Ledger. With proven governance and the fastest transaction confirmation of its kind, XRP is said to be the most efficient settlement option for financial institutions and liquidity providers seeking global reach, accessibility and fast settlement finality for interbank flows.
                    </p>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>3.Token overview</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Total Circulation:</span> 99,991,850,794
                      </li>
                      <li>
                        <span>Supply:</span> 41,432,141,931
                      </li>
                      <li>
                        <span>Issue date:</span>  2011/4/18
                      </li>
                      <li>
                        <span>Consensus Protocol:</span>  RPCA
                      </li>
                      <li>
                        <span>Private Offering Price:</span> 1XRP=$0.031
                      </li>
                    </ul>
                  </div>
                  <div class="coinInfoCard">
                    <h4>4.Support links</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Official Website:</span>{" "}
                        <a href="https://ripple.com/" target="_blank">
                        https://ripple.com/
                        </a>
                      </li>
                      <li>
                        <span>Whitepaper:</span>{" "}
                        <a
                          href="https://ripple.com/files/ripple_consensus_whitepaper.pdf"
                          target="_blank"
                        >
                          https://ripple.com/files/ripple_consensus_whitepaper.pdf
                        </a>
                      </li>
                      <li>
                        <span>Twitter:</span>{" "}
                        <a
                          href="https://twitter.com/Ripple"
                          target="_blank"
                        >
                          https://twitter.com/Ripple
                        </a>
                      </li>

                      <li>
                        <span>Facebook:</span>{" "}
                        <a
                          href="https://www.facebook.com/ripplepay"
                          target="_blank"
                        >
                        https://www.facebook.com/ripplepay
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              ):
              this.state.showcontent=="BUSD"?(
                <div className="darkBox contentPage leverageContent">
                <div className="tableHead tableHeadBlock">
                  <h2>BUSD</h2>
                  <h6>2018-02-28</h6>
                </div>
                <div className="darkBoxSpace">
                  <div class="coinInfoCard mb-4">
                    <h4>1.Project Description</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Project Name:</span> Binance USD
                      </li>
                      <li>
                        <span>Token name:</span> BUSD
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>2.Project Introduction</h4>
                    <p>
                  Binance USD (BUSD) is a new USD-denominated stablecoin approved by the New York State Department of Financial Services (NYDFS) that will be launched in partnership with Paxos and Binance.
BUSD is a U.S.-regulated stablecoin, fully backed by U.S. dollars: 1 BUSD = $1.00 USD. BUSD offers faster ways to fund your trades and is acceptable as a medium of exchange, store of value, and method of payment across the global crypto ecosystem.
                    </p>
                  </div>
                  <div class="coinInfoCard mb-4">
                    <h4>3.Token overview</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Total Circulation:</span>  $200 million
                      </li>

                    </ul>
                  </div>
                  <div class="coinInfoCard">
                    <h4>4.Support links</h4>
                    <ul class="pdListContent">
                      <li>
                        <span>Official Website:</span>{" "}
                        <a href="https://www.binance.com/en/busd" target="_blank">
                        https://www.binance.com/en/busd
                        </a>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>

              ):
              ("")
                }

              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default CoinInfo;
