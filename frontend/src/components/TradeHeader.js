import React, { Component } from 'react'
import '../css/style-trade.css';
import Logo from "../images/Logo-small.png"
import Selicon1 from "../images/selicon1.png"
import Selicon2 from "../images/selicon2.png"
import Selicon4 from "../images/selicon4.png"
import Selicon5 from "../images/selicon5.png"
import Selicon6 from "../images/selicon6.png"
import {Link,withRouter} from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import { store } from 'react-notifications-component';
import { logoutUser,getPertual,getspotPricevalue } from "../actions/authActions";
import classnames from "classnames";
import io from "socket.io-client";
import keys from "../actions/config";
const url = keys.baseUrl;

class TradeHeader extends Component {
  constructor(props) {
    super(props);
    if(!localStorage.getItem('curpair'))
    {
      localStorage.setItem('curpair','BTC-USD')
    }
    if(!localStorage.getItem('curpair1'))
    {
      localStorage.setItem('curpair1','BTC-USD')
    }
    this.state = {
      email          : "",
      password       : "",
      username       : "",
      errors         : {},
      records        : [],
      secondcurrency : localStorage.getItem('curpair1').split('-')[1],
      firstcurrency  : localStorage.getItem('curpair1').split('-')[0],
      prevoiusprice  : 0,
      floating       : 2,
      change         : 0,
      btcvolume      : 0,
      btcprice       :0,
      spotpricecolor : 'greenText',
    };
    this.getData();

    this.socket = io(keys.socketUrl,{secure: true,transports:['polling'],jsonp:false,rejectUnauthorized:false}); //live
    // this.socket = io(keys.socketUrl,{transports:['polling']});
     this.socket.on('TRADE', function(data){
       this.getData();
    });
    this.socket.on('PRICEDETAILS', function(data){
      //console.log(data,'PRICEDETAILS');
        priceappenddata(data);
    });

    const priceappenddata = data => {
      console.log(data,'data')
      // if(data && data.tiker_root=='BTCUSD')
      // {
      //   this.setState({btcprice:parseFloat(data.markprice).toFixed(2)})
      // }
      // var curpair = localStorage.getItem('curpair1').replace("USDT", "USD");
      // console.log(curpair,'curpair')
      if(data && data.tiker_root == localStorage.getItem('curpair1').replace("-", ""))
      {
        var floating = (data.tiker_root=='XRPUSD')?4:2;
        this.setState({markprice:parseFloat(data.markprice).toFixed(floating),last_price:data.last,change:data.change});
        if(this.state.prevoiusprice!=0 && this.state.prevoiusprice < data.markprice )
        {
            this.setState({spotpricecolor:"greenText"});
        }
        else
        {
            this.setState({spotpricecolor:"pinkText"});
        }
            this.setState({prevoiusprice:this.state.markprice,floating:floating});
      }

    }


  }
  onLogoutClick = e => {
    e.preventDefault();
    store.addNotification({
      title        : "Wonderful!",
      message      : "Loggedout Successfully",
      type         : "success",
      insert       : "top",
      container    : "top-right",
      animationIn  : ["animated", "fadeIn"],
      animationOut : ["animated", "fadeOut"],
      dismiss      : {
      duration     : 2000,
      onScreen     : true
      }
    });
    this.props.logoutUser();
  };

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      // this.props.history.push("/settings");
    }
  };

  componentWillReceiveProps(nextProps) {

    if (nextProps.auth.isAuthenticated) {
    }

    if (nextProps.errors) {
      this.setState({errors: nextProps.errors});
    }
    else
    {
      this.setState({errors: {}});
    }
    if (nextProps.auth.trade !== undefined
    && nextProps.auth.trade.data !== undefined
    && nextProps.auth.trade.data.data !== undefined
    && nextProps.auth.trade.data.type !== undefined
    && nextProps.auth.trade.data.type == 'perpetual'
    && nextProps.auth.trade.data.data.length>0
    ) {
          if(nextProps.auth.trade.data.data.length>0)
          {
            console.log(nextProps.auth.trade.data.type,'type')
            console.log(nextProps.auth.trade.data.data,'Perpetual data');
            var index     = nextProps.auth.trade.data.data.findIndex(x => (x.tiker_root) === this.state.firstcurrency+this.state.secondcurrency);
            var btcindex  = nextProps.auth.trade.data.data.findIndex(x => (x.tiker_root) === 'BTCUSD');
              var perparray = nextProps.auth.trade.data.data;
              var indexarr  = [0, 1, 2,3,4];
              var outputarr = indexarr.map(i => perparray[i]);

              this.setState({
                records:outputarr,markprice:(index!=-1)?nextProps.auth.trade.data.data[index].markprice:0,
                btcprice  : (btcindex!=-1)?parseFloat(nextProps.auth.trade.data.data[btcindex].markprice).toFixed(2):0,
              });
              console.log(this.props.location.pathname,'pathname')
              if( this.props.location.pathname=='/closedPandL')
              {
                console.log(index,'index')
                  if(index!='-1')
                  {
                    this.props.pairdatachange(nextProps.auth.trade.data.data);
                  }
              }
          }
    }

    if (nextProps.auth.trade !== undefined
    && nextProps.auth.trade.data !== undefined
    && nextProps.auth.trade.data.pricedet !== undefined && nextProps.auth.trade.data.pricedet.length > 0) {
      var floating = (localStorage.getItem('curpair').replace("-", "")=='XRPUSD')?4:2;
      var btcvalue = 0;
      console.log(nextProps.auth.trade.data.pricedet,'pricedet')
      if(nextProps.auth.trade.data.pricedet.length>0)
      {
        var usdvalue = parseFloat(this.state.markprice)*parseFloat(nextProps.auth.trade.data.pricedet[0].volume);
        var btcvalue = parseFloat(usdvalue)/parseFloat(this.state.btcprice);
      console.log(btcvalue,'btcvalue')
      console.log(usdvalue,'usdvalue')
      console.log(this.state.markprice,'markprice')
      console.log(parseFloat(this.state.btcprice),'btcprice')
      }
      this.setState({
        pricedet : nextProps.auth.trade.data.pricedet,
        change   : nextProps.auth.trade.data.pricedet[0].change,
        floating : floating,
        btcvolume: isNaN(parseFloat(btcvalue))?0:btcvalue,
      });
    }
  }

  getData() {
    var pairdata = {
      "firstCurrency" : this.state.firstcurrency,
      "secondCurrency" : this.state.secondcurrency,
    }
    this.props.getPertual();
    console.log(pairdata,'pairdatapairdata')
    this.props.getspotPricevalue(pairdata);
  }

  render() {
    const { user } = this.props.auth;
    const { records } = this.state;

    return (
    	<div>
    		<nav className="navbar navbar-expand-md fixed-top customNavTrade wow fadeInDown" data-wow-delay=".2s">
			    <Link to="/" className="navbar-brand"><img src={Logo} alt="GlobalCryptoX" /></Link>
			    <div className="header-overview">
            <div className="selectCoinType">
            	{/*<div className="btn-group">
            		<button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            		{localStorage.getItem('curpair')} Perpetual
            		</button>
            		<ul className="dropdown-menu">
            		  <li className="dropdown-header">
            				<ul>
            					{ (records.length>0)?
                        records.map((item,i)=>{
                      if ((typeof item !='undefined') && item.first_currency == 'BTC') {
                      var icon = Selicon1;
                      } else if((typeof item !='undefined') && item.first_currency == 'XRP') {
                      var icon = Selicon4;
                      }else if( (typeof item !='undefined') && item.first_currency == 'LTC') {
                      var icon = Selicon5;
                      }else if( (typeof item !='undefined') && item.first_currency == 'BCH') {
                      var icon = Selicon6;
                      }else if( (typeof item !='undefined') && item.first_currency == 'ETH') {
                      var icon = Selicon2;
                      }
            					// var image = (i==0)?Selicon1:((i==1)?Selicon2:"");
            					if((typeof item !='undefined')){
            					return <Link to={'/trade/'+item.first_currency+'-'+item.second_currency}><li className={(this.state.firstcurrency==item.first_currency && this.state.secondcurrency==item.second_currency)?"active":""} id={item.first_currency+"_"+item.second_currency} ><img src={icon} /><span>{item.first_currency+item.second_currency}</span></li></Link>
            					}
            					})
                      :''
            					}
            				</ul>
            			</li>
            		</ul>
            	</div>*/}
            </div>
			      <div className="headerOverviewGroup">

			        <div className="headerOverviewStatus">
			         <h5><small>Spot Price</small><span className={this.state.spotpricecolor}>{ ((typeof this.state.markprice != 'undefined')?parseFloat(this.state.markprice).toFixed(this.state.floating):0)+' $' }</span></h5>
			        </div>

              <div className="headerOverviewStatus">
               <h5><small>Last Price</small>{ ((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(this.state.pricedet[0].close).toFixed(this.state.floating):0)+' $' }</h5>
              </div>

              <div className="headerOverviewStatus">
              {
                (this.state.change<0)?
			         <h5 className="pinkText"><small>24H Change</small>{ ((this.state.change != '')?parseFloat(this.state.change).toFixed(2):0) }%</h5>:
               <h5 className="greenText"><small>24H Change</small>{ ((this.state.change != '')?parseFloat(this.state.change).toFixed(2):0) }%</h5>
              }
			        </div>
			        <div className="headerOverviewStatus">
			         <h5><small>24H High</small>{ ((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(this.state.pricedet[0].high).toFixed(this.state.floating):0)+' $' }</h5>
			        </div>
			        <div className="headerOverviewStatus">
			         <h5><small>24H Low</small>{ ((typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(this.state.pricedet[0].low).toFixed(this.state.floating):0)+' $' }</h5>
			        </div>
			        <div className="headerOverviewStatus">
			         <h5><small>24H Turnover</small>{ (typeof this.state.pricedet != 'undefined' && this.state.pricedet.length>0)?parseFloat(Math.abs(this.state.pricedet[0].secvolume)).toFixed(3):0 } {this.state.secondcurrency}</h5>
			        </div>
			    </div>
			    </div>
			    <div className="mobileNavAction d-block d-md-none ml-auto">
			    <ul className="navbar-nav">
			        <li className="nav-item">
			          <a className="nav-link" href="#"><span className="bg-mobileIcon iconNav"></span></a>
			        </li>
			        {/*<li className="nav-item">
			          <a className="nav-link" href="#"><span className="bg-bellIcon iconNav"></span></a>
			        </li>*/}
			        <li className="nav-item">
			          <button className="nav-link navbar-toggler customNavbarToggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			          <i className="fas fa-bars"></i>
			    </button>
			        </li>
			    </ul>
			  </div>
			    <div className="collapse navbar-collapse" id="navbarResponsive">
			      <ul className="navbar-nav ml-auto">
			        <li className="nav-item d-none d-md-block">
			          <a className="nav-link" href="#"><span className="bg-mobileIcon iconNav"></span></a>
			        </li>
			        {/*<li className="nav-item d-none d-md-block">
			          <a className="nav-link" href="#"><span className="bg-bellIcon iconNav"></span></a>
			        </li>*/}
              <li className="nav-item">
                 <Link to={"/Spot/"+localStorage.getItem('curpair1')} className="dropdown-item nav-link">Trade</Link>
              </li>
              {/* <li className="nav-item"> */}
                 {/* <a href="" target="_blank" className="dropdown-item nav-link">Unlimited Money</a> */}
              {/* </li> */}
              {/* <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Trade
                  </a>
                  <div className="dropdown-menu sm-menu">
                   <Link to={"/Spot/"+localStorage.getItem('curpair1')} className="dropdown-item nav-link">Spot</Link>
                   <Link to={"/Trade/"+localStorage.getIteName="dropdown-item nav-link">Derivatives</Link> 
                  </div>
              </li>*/}

              <li className="nav-item dropdown dmenu mr-2">
                  <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  Finance
                  </a>
                  <div className="dropdown-menu sm-menu">
                    <Link to="/Locker"  className="dropdown-item">Finance</Link>
                  </div>
              </li>

			        <li className="nav-item">
			          <Link to="/MyAssets" className="nav-link">Wallets</Link>
			        </li>

			        <li className="nav-item">
			          <Link to="/closedPandL" className="nav-link">History</Link>
			        </li>
			        <li className="nav-item dropdown dmenu">
			            <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                  {(typeof user.email != 'undefined')?(user.email.split('@')[0].length<8)?user.email.split('@')[0]:user.email.split('@')[0].substring(0, 7):''}
			            </a>
			            <div className="dropdown-menu sm-menu">
			              <Link  to="/Settings" className="dropdown-item">Settings</Link>
			              {/* <a className="dropdown-item" href="#">Referral Program</a> */}
						        <Link  to="/Referral_Program" className="dropdown-item">Referral Program</Link>
						        <Link  to="/Bonus" className="dropdown-item">Bonus</Link>
						        {/* <Link  to="/Leverage" className="dropdown-item">Leverage</Link> */}

			              <a className="dropdown-item" onClick={this.onLogoutClick} href="#">Logout</a>
			            </div>
			          </li>
			      </ul>
			    </div>
			</nav>
    	</div>
    	);
    }
}


TradeHeader.propTypes = {
    getspotPricevalue: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    getPertual: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    {logoutUser,getPertual,getspotPricevalue}
)(withRouter(TradeHeader));
