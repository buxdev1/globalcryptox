import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Navbar from './Navbar'
import Footer from './Footer'
import LeverageImg from "../images/leverage.png"
import axios from "axios";
import keys from "../actions/config";
const url = keys.baseUrl;
class Leverage extends Component {

	 constructor(props) {
        super(props);
        this.state = {  
            subject:"",
            identifier:"",
            content: "",
            errors: {},
            Leverage : {},
           
        };

        console.log(this.state,'state');
        }

        componentDidMount() {
            this.getData()  
             
        };
    createMarkup = () => {
          return { __html: this.state.Leverage.data.content };
        }
 
    getData() {
           axios
           .get(url+"cryptoapi/leverage")
            .then(res => {   
              this.setState({Leverage:res});
             })
            .catch()
            console.log(this.setState,'this.setState');
    }
   

	render() {
		return (<div>
			<Navbar />
			<section className="innerCMS">
			 {this.state.Leverage.data?
			  <div className="container">
			    <div className="row">
			      <div className="col-md-10 mx-auto">
			        <img src={keys.baseUrl + 'cms_images/' + this.state.Leverage.data.image[0]} className="img-fluid cmsTopImg mb-1" /> 
			        <div className="darkBox contentPage">
			          <div className="tableHead tableHeadBlock">
			            <h2>Leverage</h2>
			          </div>
			          <div className="darkBoxSpace">
                {this.state.Leverage.data?
                  <div>
			           
			            <p><div dangerouslySetInnerHTML={this.createMarkup()} className='editor'></div></p>
			            </div>
                  :''}
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			    :''}
			</section>
			<Footer />
		</div>
		);
	}
}

export default Leverage
