const express       = require('express');
const mongoose      = require('mongoose');
const router        = express.Router();
const settings      = require('../models/settings');
const avocms     = require('../models/avocms');
const session       = require('express-session')
const async         = require('async');
var fs              = require('fs');
const multer        = require('multer');
const path          = require('path');
 
var sess            = '';
var assigndata      = {};
var app = express(); 
var ObjectId        = mongoose.Types.ObjectId;

/********************* sub admin *****************************/
function checksubadmin_roles(req,res) { 

  var roledat = res.locals.ms_data.role.split(",").map(Number); 
  var role_chkcnd = roledat.contains(req.singlerole);  
  if(role_chkcnd==true || res.locals.ms_data.type =='main' ) { 
    //next(); 
  } else{
    res.redirect('/admin/ad_dashboard');
  }
}
/********************* sub admin *****************************/

 /********************** manage Blog category*************************/  
// view blog
router.get('/avo_links', function(req,res) {
  assigndata['page']    = "bk_avo_links";
  assigndata['page_id'] = "view";
  assigndata['singlerole']= 2;
  assigndata['res']     = {};
  if( req.session.adminid_b==null) {
    res.redirect('/admin/zxcvbnm');
  } else {
    checksubadmin_roles(assigndata,res);
    async.parallel({
        settings: function(cb){ 
                settings.findOne({} , {_id:0 } ).exec(cb)
            },
      avodata: function(cb){
        avocms.find({page:"avo_details"} , {} ).sort({_id: 'desc'}).exec(cb)
      }
    }, function(err, results){
      if (err)  res.json({"error":err});
      try {
        assigndata['avobyte'] = results.avodata;
        assigndata['settings1'] = results.settings;
      res.json({data:assigndata});
      /*console.log('zsdcfvgbh');*/
      //console.log(community);
      //res.render("admin/bk_avo_details",{data:assigndata} );
      } catch(err) {
        res.json({"error":err});
      }
    }); 
  }
});      






module.exports = router; 