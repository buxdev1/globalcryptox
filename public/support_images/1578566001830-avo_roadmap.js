const express       = require('express');
const mongoose      = require('mongoose');
const router        = express.Router();
const settings      = require('../models/settings');
const avocms     = require('../models/avocms');
const session       = require('express-session')
const async         = require('async');
var fs              = require('fs');
const multer        = require('multer');
const path          = require('path');
 
var sess            = '';
var assigndata      = {};
var app = express(); 
var ObjectId        = mongoose.Types.ObjectId;

/********************* sub admin *****************************/
function checksubadmin_roles(req,res) { 

  var roledat = res.locals.ms_data.role.split(",").map(Number); 
  var role_chkcnd = roledat.contains(req.singlerole);  
  if(role_chkcnd==true || res.locals.ms_data.type =='main' ) { 
    //next(); 
  } else{
    res.redirect('/admin/ad_dashboard');
  }
}
/********************* sub admin *****************************/

 /********************** manage Blog category*************************/  
// view blog
router.get('/avo_roadmap', function(req,res) {
  assigndata['page']    = "bk_avo_roadmap";
  assigndata['page_id'] = "view";
  assigndata['singlerole']= 2;
  assigndata['res']     = {};
  if( req.session.adminid_b==null) {
    res.redirect('/admin/zxcvbnm');
  } else {
    checksubadmin_roles(assigndata,res);
    async.parallel({
      avodata: function(cb){
        avocms.find({pageid:"avo_roadmap"} , {} ).sort({_id: 'desc'}).exec(cb)
      }
    }, function(err, results){
      if (err)  res.json({"error":err});
      try {
        assigndata['avobyte'] = results.avodata;
      //res.json({data:assigndata});
      /*console.log('zsdcfvgbh');*/
      //console.log(community);
      res.render("admin/bk_avo_roadmap",{data:assigndata} );
      } catch(err) {
        res.json({"error":err});
      }
    }); 
  }
});      


app.use(express.static(path.join(__dirname, 'public')))
 var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/assets/images/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname) )
  }
});

var upload = multer({ storage: storage }); 
//var upload = multer({ storage: storage }); 
//multer({ storage: storage, limits: {fileSize: 100000000} });
//var load = multer({storage: storage}).any('image_sec1');


var cpUpload = upload.fields([{ name: 'image_sec1', maxCount: 5 }]);


router.get('/editavo_roadmap/:ids', function(req,res){    
    assigndata['page']    = "bk_avo_roadmap";
    assigndata['page_id'] = "edit";
    assigndata['res']     = {};
    assigndata['singlerole']= 2;     
    if( req.session.adminid_b==null) { 
      res.redirect('/admin/zxcvbnm');
    } else {  
      checksubadmin_roles(assigndata,res);
        var id = req.params.ids; 
        async.parallel({ 
            settings: function(cb){ 
                settings.findOne({} , {_id:0 } ).exec(cb)
            }, avodata: function(cb){ 
                avocms.findOne({_id:ObjectId(req.params.ids)} , {created_date:0} ).populate().exec(cb)
            }       
        }, function(err, results){ 

        if (err) throw err;
        try {
            
            assigndata['avobyte']    = results.avodata;
            assigndata['settings']    = results.settings;
            //res.json({data:assigndata})

            res.render("admin/bk_avo_roadmap",{data:assigndata} );    
        } catch(err) { 
          console.log(err);
        } 
        }); 
    }

}); 

router.post('/editavo_roadmap', cpUpload, function(req,res) {

  assigndata['page']="bk_avo_roadmap";
  assigndata['page_id']="edit";
  assigndata['singlerole']=2;
  //assigndata['res']  ={}; 

  if( req.session.adminid_b==null) { 
       res.json({'status':false, 'message':"You must login" });
        
  } else {  
    checksubadmin_roles(assigndata,res);

    var pdata = req.body;
    var filesdata = req.files;
    console.log(filesdata);

    if(pdata.page!="" || pdata.title!="" || pdata.avo_sdate_1!="" ) {

      req.checkBody('title').notEmpty().withMessage('Title is required');
      req.checkBody('avo_sdate_1').notEmpty().withMessage('Title is required');
      
      

      const errors = req.validationErrors();

      if (errors) {
        res.json({'status':'2',message:errors});
      }
      else {

        async.parallel({
          avodata: function(cb) {
            avocms.findOne({_id:pdata.avoid} , {_id:0 } ).exec(cb);
          }
        }, function(err, results){

          if (err) {
            res.json({'status':'error',message:err});
            return false;
          }

          console.log(filesdata.image_sec1);
          console.log('258');
        

          var updatedata = {
                    
                    "title": pdata.title,
                    "avo_sdate_1":pdata.avo_sdate_1,
                  };
          avocms.findOneAndUpdate({"_id": pdata.avoid},{"$set": updatedata},{new: true, "fields":{"status":1 } }).exec(function(uperr, resUpdate) {
            if (!uperr) {
              res.json({pdata:pdata,'status':true,message:"avobyte details are updated successfully"})
            }
            else {
              res.json({pdata:pdata,'status':false,message:"Some error was occurred while updating details"})
            }  
          })


        });


      }
    } else {
      res.json({'status':false,message:"Some error was occurred while updating details"})
    }
  }
});

router.post('/delavo_roadmap', function(req,res){     
  assigndata['page']="bk_avo_roadmap";   
  assigndata['res']  ={}; 
  assigndata['singlerole']= 2;     
 if( req.session.adminid_b==null) { 
       res.json({'status':false, 'message':"You must login"});
  } else {   
    checksubadmin_roles(assigndata,res); 
    var pdata = req.body; 
      avocms.remove({_id: pdata.id }, function(err) { 
      if (!err) {
         res.json({'status':true,message:"avobyte roadmap data removed successfully"})
      }
      else {
          res.json({'status':false,message:"Some error was occurred while deleting avobyte  invest data"})
      }
      });

  }

});  


router.post('/changestatus_avo_roadmap', function(req,res){    
  assigndata['page']="bk_avo_roadmap";   
  assigndata['res']  ={}; 
  assigndata['singlerole']= 2;      
 if( req.session.adminid_b==null) { 
      res.json({'status':false, 'message':"You must login" });
    
  } else {  
    checksubadmin_roles(assigndata,res); 
    var pdata =  req.body;     
       async.parallel({  
        avodata: function(cb){ 
         avocms.findOne({_id:pdata.id},{status:1,_id:1}).exec(cb) 
        }       
      }, function(err, results){   
    
      if (err) { 
           res.json({'status':false,message:"Some error was occurred while changing avobyte status"})
       } 
      try { 
            if(results.avodata.status==1) {  
             var newstatus = 0; 
             var title = 'Deactivated'   
          }
          else { 
             var newstatus = 1; 
             var title = 'Activated'
          } 
           avocms.findOneAndUpdate({ "_id": pdata.id },{"$set": {"status": newstatus}},{new: true, "fields":{ "status":1 }}).exec(function(uperr, resUpdate){         
              if (!uperr) {
                 res.json({'status':true,message:"avobyte status "+title+" successfully"})
              }
              else { 
                  res.json({'status':false,message:"Some error was occurred while updating avobyte status"})
              } 
     });
      } catch(err) { 
         res.json({'status':false,message:"Some error was occurred while updating status"+err})
      }

      }); 
  } 
});   



module.exports = router; 