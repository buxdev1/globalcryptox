const express       = require('express');
const mongoose      = require('mongoose');
const router        = express.Router();
const settings      = require('../models/settings');
const avocms     = require('../models/avocms');
const session       = require('express-session')
const async         = require('async');
var fs              = require('fs');
const multer        = require('multer');
const path          = require('path');
 
var sess            = '';
var assigndata      = {};
var app = express(); 
var ObjectId        = mongoose.Types.ObjectId;

/********************* sub admin *****************************/
function checksubadmin_roles(req,res) { 

  var roledat = res.locals.ms_data.role.split(",").map(Number); 
  var role_chkcnd = roledat.contains(req.singlerole);  
  if(role_chkcnd==true || res.locals.ms_data.type =='main' ) { 
    //next(); 
  } else{
    res.redirect('/admin/ad_dashboard');
  }
}
/********************* sub admin *****************************/

 /********************** manage Blog category*************************/  
// view blog
router.get('/avo_homebanner', function(req,res) {
  assigndata['page']    = "bk_avo_homebanner";
  assigndata['page_id'] = "view";
  assigndata['singlerole']= 2;
  assigndata['res']     = {};
  if( req.session.adminid_b==null) {
    res.redirect('/admin/zxcvbnm');
  } else {
    checksubadmin_roles(assigndata,res);
    async.parallel({
      avodata: function(cb){
        avocms.find({page:"avo_homebanner"} , {} ).sort({_id: 'desc'}).exec(cb)
      }
    }, function(err, results){
      if (err)  res.json({"error":err});
      try {
        assigndata['avobyte'] = results.avodata;
      //res.json({data:assigndata});
      res.render("admin/bk_avo_homebanner",{data:assigndata} );
      } catch(err) {
        res.json({"error":err});
      }
    }); 
  }
});      


app.use(express.static(path.join(__dirname, 'public')))
 var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/assets/images/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname) )
  }
});

var upload = multer({ storage: storage }); 
var cpUpload = upload.fields([{ name: 'image_sec1', maxCount: 5 }]);


router.get('/editavo_homebanner/:ids', function(req,res){    
    assigndata['page']    = "avo_homebanner";
    assigndata['page_id'] = "edit";
    assigndata['res']     = {};
    assigndata['singlerole']= 2;     
    if( req.session.adminid_b==null) { 
      res.redirect('/admin/zxcvbnm');
    } else {  
      checksubadmin_roles(assigndata,res);
        var id = req.params.ids; 
        async.parallel({ 
            settings: function(cb){ 
                settings.findOne({} , {_id:0 } ).exec(cb)
            }, avodata: function(cb){ 
                avocms.findOne({_id:ObjectId(req.params.ids)} , {created_date:0} ).populate().exec(cb)
            }       
        }, function(err, results){ 

        if (err) throw err;
        try {
            
            assigndata['avobyte']    = results.avodata;
            assigndata['settings']    = results.settings;
            //res.json({data:assigndata})

            res.render("admin/bk_avo_homebanner",{data:assigndata} );    
        } catch(err) { 
          console.log(err);
        } 
        }); 
    }

}); 

router.post('/editavo_homebanner', cpUpload, function(req,res) {

  assigndata['page']="bk_avo_homebanner";
  assigndata['page_id']="edit";
  assigndata['singlerole']=2;
  //assigndata['res']  ={}; 

  if( req.session.adminid_b==null) { 
       res.json({'status':false, 'message':"You must login" });
        
  } else {  
    checksubadmin_roles(assigndata,res);

    var pdata = req.body;
    var filesdata = req.files;
    console.log(filesdata);

    if(pdata.token!="" || pdata.title!="" || pdata.sec_title!="" || pdata.description!="" || pdata.sec_description!="" ) {

      req.checkBody('token').notEmpty().withMessage('token is required');
      req.checkBody('title').notEmpty().withMessage('Title is required');
      req.checkBody('sec_title').notEmpty().withMessage('Sec Title is required');
      req.checkBody('description').isLength({ min: 1 }).notEmpty().withMessage('Description is required');
      req.checkBody('sec_description').isLength({ min: 1 }).notEmpty().withMessage('Sec Description is required');

      const errors = req.validationErrors();

      if (errors) {
        res.json({'status':'2',message:errors});
      }
      else {

        async.parallel({
          avodata: function(cb) {
            avocms.findOne({_id:pdata.avoid} , {_id:0 } ).exec(cb);
          }
        }, function(err, results){

          if (err) {
            res.json({'status':'error',message:err});
            return false;
          }

          console.log(filesdata.image_sec1);
          console.log('258');

          var filename_imagesarr = [];
          if(!filesdata || typeof filesdata.image_sec1=="undefined") {
            var filename_imagesarr = results.avodata.image_sec1;
            console.log('if');
          } else {
            console.log('else');
            for(var i=0;i<filesdata.image_sec1.length;i++)
            {
               filename_imagesarr.push(filesdata.image_sec1[i].filename);
            }

            if(results.avodata.image_sec1 && results.avodata.image_sec1!="undefined") {
              console.log(results);
              console.log(results.avodata.image_sec1);
              for(var i=0;i<results.avodata.image_sec1.length;i++)
              {
                var image_old = "./public/assets/images/"+results.avodata.image_sec1[i];
                fs.unlink(image_old, (unlinkerr) => {
                  if (unlinkerr) {
                  }
                  else{
                  }
                });
              }
            }

          }


        
          console.log('filesdata : ');
          console.log(filesdata);
          console.log('filename_imagesarr : ');

          var updatedata = {
                    
                    "token": pdata.token,
                    "title": pdata.title,
                    "sec_title": pdata.hidesec_title,
                    "description":pdata.description,
                    "sec_description":pdata.hidesec_description,
                    "image_sec1": filename_imagesarr,
                  };
          avocms.findOneAndUpdate({"_id": pdata.avoid},{"$set": updatedata},{new: true, "fields":{"status":1 } }).exec(function(uperr, resUpdate) {
            if (!uperr) {
              res.json({pdata:pdata,'status':true,message:"avobyte details are updated successfully"})
            }
            else {
              res.json({pdata:pdata,'status':false,message:"Some error was occurred while updating details"})
            }  
          })


        });


      }
    } else {
      res.json({'status':false,message:"Some error was occurred while updating details"})
    }
  }
});


router.post('/editavo_homebanner_induv', function(req, res) {
  var pdata = req.body;
  var updatedata = {};
  updatedata[pdata.gname] = pdata.gvalue;
  avocms.findOneAndUpdate({"_id": pdata.avoid},{"$set": updatedata},{new: true}).exec(function(uperr, resUpdate) {
    if (!uperr) {
      res.json({
        uperr:uperr,
        updatedata:updatedata,
        'status':true,
        message:"details are updated successfully"
      });
    }
    else {
      res.json({
        uperr:uperr,
        updatedata:updatedata,
        'status':false,
        message:"Some error was occurred while updating details"
      });
    }
  })
});
router.post('/delavobyte_homebanner', function(req,res){     
  assigndata['page']="bk_avo_homebanner";   
  assigndata['res']  ={}; 
  assigndata['singlerole']= 2;     
 if( req.session.adminid_b==null) { 
       res.json({'status':false, 'message':"You must login"});
  } else {   
    checksubadmin_roles(assigndata,res); 
    var pdata = req.body; 
      avocms.remove({_id: pdata.id }, function(err) { 
      if (!err) {
         res.json({'status':true,message:"avobyte data removed successfully"})
      }
      else {
          res.json({'status':false,message:"Some error was occurred while deleting avobyte  data"})
      }
      });

  }

});  


router.post('/changestatus_avobyte', function(req,res){    
  assigndata['page']="bk_avo_homebanner";   
  assigndata['res']  ={}; 
  assigndata['singlerole']= 2;      
 if( req.session.adminid_b==null) { 
      res.json({'status':false, 'message':"You must login" });
    
  } else {  
    checksubadmin_roles(assigndata,res); 
    var pdata =  req.body;     
       async.parallel({  
        avodata: function(cb){ 
         avocms.findOne({_id:pdata.id},{status:1,_id:1}).exec(cb) 
        }       
      }, function(err, results){   
    
      if (err) { 
           res.json({'status':false,message:"Some error was occurred while changing avobyte status"})
       } 
      try { 
            if(results.avodata.status==1) {  
             var newstatus = 0; 
             var title = 'Deactivated'   
          }
          else { 
             var newstatus = 1; 
             var title = 'Activated'
          } 
           avocms.findOneAndUpdate({ "_id": pdata.id },{"$set": {"status": newstatus}},{new: true, "fields":{ "status":1 }}).exec(function(uperr, resUpdate){         
              if (!uperr) {
                 res.json({'status':true,message:"avobyte status "+title+" successfully"})
              }
              else { 
                  res.json({'status':false,message:"Some error was occurred while updating avobyte status"})
              } 
     });
      } catch(err) { 
         res.json({'status':false,message:"Some error was occurred while updating status"+err})
      }

      }); 
  } 
});   



module.exports = router; 