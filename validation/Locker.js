const Validator = require("validator");
const isEmpty = require("is-empty");
module.exports = function validateUpdateUserInput(data,type) {
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : "";
    data.description = !isEmpty(data.description) ? data.description : "";
    data.returnpercentage = !isEmpty(data.returnpercentage) ? data.returnpercentage : "";
    data.lockerduration = !isEmpty(data.lockerduration) ? data.lockerduration : "";
    data.mininvestment = !isEmpty(data.mininvestment) ? data.mininvestment : "";
    data.maxinvestment = !isEmpty(data.maxinvestment) ? data.maxinvestment : "";


    if (Validator.isEmpty(data.name)) {
        errors.name = "Name field is required";
    }
    if (Validator.isEmpty(data.description)) {
        errors.description = "Description field is required";
    }
    if (Validator.isEmpty(data.returnpercentage)) {
        errors.returnpercentage = "Return Percentage  field is required";
    }
    if (!Validator.isEmpty(data.returnpercentage)) {
        if(data.returnpercentage<1){
            errors.returnpercentage = "Enter the Correct value";
        }


        // var options ={force_decimal: false, decimal_digits: '1,', locale: 'en-US'}



        // if (Validator. isDecimal(data.returnpercentage [ options])) {
        //     console.log("is integer eroror")
        // }
        // var integer =Number.isInteger(parseFloat( data.returnpercentage))
        // console.log("integer",integer)
        // console.log("typeof",typeof integer)
        // if(integer==false){
        //     errors.returnpercentage = "Enter an Integer value";
        // }

    //   if(Number.isInteger(parseFloat( data.returnpercentage))){
    //         console.log("is integer")
    //         // errors.returnpercentage = "Enter an Integer value";
    //     }
    }

    if (Validator.isEmpty(data.lockerduration)) {
        errors.lockerduration = "Return Term field is required";
    }
    if (!Validator.isEmpty(data.lockerduration)) {
        if(data.lockerduration<1){
            errors.lockerduration = "Enter the Correct value";
        }
        var lockinteger=Number.isInteger(parseFloat( data.lockerduration))
        if(lockinteger==false){
            errors.lockerduration = "Enter an Integer value";
        }
        // if(Number.isInteger(parseFloat( data.lockerduration))){
        //     console.log("is integer")
        //     // errors.lockerduration = "Enter an Integer value";
        // }
    }
    if (Validator.isEmpty(data.mininvestment.toString())) {
        errors.mininvestment = "Minimum Investment field is required";
    }
    if (Validator.isEmpty(data.maxinvestment.toString())) {
        errors.maxinvestment = "Maximum Investment field is required";
    }

    if (!Validator.isEmpty(data.mininvestment.toString())) {
        if(data.mininvestment<0){
            errors.mininvestment = "Enter the Correct Amount";
        }

    }

    if (!Validator.isEmpty(data.maxinvestment.toString())) {
        if(data.maxinvestment<1){
            errors.maxinvestment = "Enter the Correct Amount";
        }

    }



    return {
        errors,
        isValid: isEmpty(errors)
    };
};
